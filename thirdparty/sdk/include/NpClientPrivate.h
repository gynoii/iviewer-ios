#ifndef _Np_CLIENT_PRIVATE_H_
#define _Np_CLIENT_PRIVATE_H_

#include "NpClient.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

enum Np_ManualRecordStatus 
{
    kMR_START = 0,
    kMR_STOP = 1,
};

SDK_EXPORT Np_Result_t Info_GetManualRecordStatus(void* handle, Np_ManualRecordStatus& status);

SDK_EXPORT Np_Result_t Control_EnableManualRecord(void* handle, bool enable);

SDK_EXPORT Np_Result_t Create_PrivateHandle(void** handle, Np_ServerType type, const wchar_t* username, 
                                            const wchar_t* passwd, const wchar_t* ipaddress, WORD port);

SDK_EXPORT Np_Result_t Destroy_PrivateHandle(void** handle);

SDK_EXPORT Np_Result_t PlayBack_GetIDList(void* player, Np_IDList& IdList);

SDK_EXPORT Np_Result_t PlayBack_ReleaseIDList(Np_IDList& IdList);

SDK_EXPORT Np_Result_t PlayBack_NextMilliseconds(void* player, int milliseconds);

SDK_EXPORT Np_Result_t PlayBack_PreviousMilliseconds(void* player, int milliseconds);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif


