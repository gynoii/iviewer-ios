#ifndef _SDKEXPORT_H_
#define _SDKEXPORT_H_

#ifdef SDK_STATIC
#	define SDK_EXPORT
#else // DLL

#	ifdef WIN32
#		ifdef SDK_SHARED
#			define SDK_EXPORT __declspec(dllexport)
#		else
#			define SDK_EXPORT __declspec(dllimport)
#		endif
#       define MAINCONSOLESDK
#	else
#		define SDK_EXPORT
#	endif
#endif


#endif /* __SDKEXPORT_H__ */

