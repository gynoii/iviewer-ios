#ifndef _Np_CLIENT_H_
#define _Np_CLIENT_H_

#include "SDKExport.h"
#include "NpClientDefine.h"
#include <string>
#include <vector>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#ifndef BYTE
typedef unsigned char BYTE;
#endif // BYTE

#ifndef WORD
typedef unsigned short WORD;
#endif // WORD

#ifndef DWORD
#if defined(__APPLE__) || defined(__linux__) || defined(__linux)
typedef unsigned int DWORD;
#else
typedef unsigned long DWORD;
#endif
#endif // DWORD

#define MAX_PATROL_POINT 80
#define MAX_GROUP_AMOUNT 4
#define MAX_GROUP_NAME_LENGTH 32
#define MAX_METADATALOG_WAITINGTIME 10000
#define EVENT_TIMER 0

enum Np_ServerType 
{ 
    kMainConsoleLiveview = 1, 
    kMainConsolePlayback = 2, 
    kCorporate = 3, 
    kTitan = 4, 
    kCrystal = 5 
};

enum Np_VideoCodec
{
    kVideoCodecNone = 0,
    kVideoCodecMJPEG = 1,
    kVideoCodecMPEG4 = 2,
    kVideoCodecH264 = 3,
    kVideoCodecMXPEG = 4,
};

enum Np_AudioCodec
{    
    kAudioCodecNone = 0,
    kAudioCodecAAC = 1,
    kAudioCodecACM = 2,
    kAudioCodecMSADPCM = 3,
    kAudioCodecG711Alaw = 4,
    kAudioCodecG711Mulaw = 5,
    kAudioCodecG726 = 6,
    kAudioCodecGSM_AMR = 7,
    kAudioCodecPCM = 8,
    kAudioCodecG7221 = 9,
    kAudioCodecGSM610 = 10,    
};

enum Np_PixelFormat
{
    kPixelFormatYUV420P = 0,
    kPixelFormatRGB24 = 1,
    kPixelFormatBGR24 = 2,
};

enum Np_ExportError 
{
    kExportSuccess = 0,
    kExportUpdateProgress = 1,
    kExportCallbackError = 2,
    kExportChannelEmpty = 3,
    kExportCueTimeError = 4,
    kExportDimesionError = 5,
    kExportFail = 6,
    kExportNoData = 7,
    kExportMJpegMac = 8,
    kExportSonyFormatMac = 9,
    kExportFormatChange = 10,
    kExportSizeOver4G = 11,
    kExportContentFail = 12,
    kExportNetworkFail = 13
};

enum Np_FrameType
{
    kFrame = 0x00000000,
    kIFrame = 0x00000001,
    kClosestFrame = 0x00000002,    
};

enum Np_BackupStatus 
{
    kBackupStart = 0,
    kBackupSuccess = 1,
    kBackupUpdateFilePayload = 2,
    kBackupCreateNewFile = 3,
    kBackupNetworkError = 4,
    kBackupFail = 5
};

enum Np_GetRawFrameStatus
{
    kGetRawFrameStart = 0,
    kGetRawFrameSuccess = 1,
    kGetRawFrameFailed = 2,
    kGetRawFrameChannelEmpty = 3,
    kGetRawFrameTimeError = 4,
};

enum Np_BackuperState 
{
    kBackuping = 0,
    kBackupSuccessed = 1,
    kBackupFailed = 2
};

enum Np_LogUpdateStatus   
{ 
    kLogUpdating = 0, 
    kLogUpdateSuccess = 1, 
    kLogUpdateFailed = 2 
};

typedef struct ST_Np_DataTime
{
	WORD year;
	WORD month;
	WORD day;
	WORD hour;
	WORD minute;
	WORD second;
	WORD millisecond;
} Np_DateTime;

typedef int Np_Result_t;

typedef int Np_Error;

typedef struct ST_Np_Frame
{
    Np_DateTime time;
    char* buffer;
    int len;
    int width; 
    int height;
} Np_Frame;

typedef struct ST_Np_Rectangle
{
    int topLeftX;
    int topLeftY;
    int bottomRightX;
    int bottomRightY;
} Np_Rectangle;

typedef struct ST_Np_BackupItem
{    
    wchar_t* name;    
} Np_BackupItem;

typedef struct ST_Np_BackupItemList
{
    int size;
    Np_BackupItem* items;
} Np_BackupItemList;

//////////////////////////////////////////////////////////////////
/*
 * ID 
 */
//////////////////////////////////////////////////////////////////

typedef struct ST_Np_ID
{
	int centralID;
	int localID;
} Np_ID;

enum Np_StreamProfile 
{
	kProfileNormal = 0,
	kProfileOriginal = 1,
    kProfileLow = 2,
    kProfileMinimum = 3
};

enum Np_CrystalStream
{
	kFirstStream = 0,
	kSecondStream = 1,
	kThirdStream = 2
};

enum Np_ScheduleType 
{
    kScheduleNone = 0,
    kScheduleRecordOnly	= 1,
    kScheduleMotionDetectOnly = 2,
    kScheduleRecordAndMotionDetect = 3,
    kScheduleRecordMovingOnly = 4,
    kScheduleRecordOnEvent = 5,
    kScheduleUndefined = 6, //undefined
    kScheduleRecordBoost = 7
};

enum Np_MetadataSourceType 
{
    kMetadataNone = 0,
    kMetadataPOS = 1,
    kMetadataAccessControl = 2,
    kMetadataLPR = 3
};

enum Np_SourceType
{
    kSourceCamera = 0,
    kSourceDigitalInput = 1,
    kSourceMetaData = 2,
    kSourceServer = 3,
    kSourceALL = 4
};

enum Np_DIOStatus 
{
    kDIO_OFF = 0,
    kDIO_ON = 1,
};

enum Np_AudioStatus 
{
    kAUDIO_OFF = 0,
    kAUDIO_ON = 1,
};

enum Np_PlayerState 
{
    kStateStopped = 0,
    kStatePaused = 1,
    kStateRunning = 2
};

enum Np_DeviceCapability 
{
    kDeviceNone	= 0x0000,
    kDeviceAudio = 0x0001,
    kDeviceTalk	= 0x0002,
    kDevicePTZ = 0x0004,
    kDeviceDIO = 0x0008,
    kDeviceProfile = 0x0010
};

enum Np_PNStatus 
{
    kPN_UNREG = 0,
    kPN_REG,
};

enum Np_PTZCommand 
{	
    kPTZStop = 0,
    kPTZContinuousMove = 1,
    kPTZAutoFocus = 2,
    kPTZHome = 3,
    kPTZRectangle = 4,
    kPTZPresetGo = 5,
    kPTZPresetSet = 6,
    kPTZPresetClear = 7,
    kPTZPatrolStart = 8,
    kPTZPatrolStop = 9,
	kPTZContinuousMoveStop = 10,
};

enum Np_PanDirection   
{ 
    kNoPan = 0,   
    kPanLeft = 1,   
    kPanRight = 2 
};

enum Np_TiltDirection
{ 
    kNoTilt = 0,  
    kTiltUp = 1,    
    kTiltDown = 2  
};

enum Np_ZoomDirection 
{ 
    kNoZoom = 0,  
    kZoomIn = 1,    
    kZoomOut = 2   
};

enum Np_FocusDirection 
{ 
    kNoFocus = 0, 
    kFocusNear = 1, 
    kFocusFar = 2  
};

enum Np_PTZCap
{
    kPTZNone = 0x0000,
    kPTZEnable = 0x0001,
    kPTZBuiltin = 0x0002,
    kPTZPane = 0x0004,
    kPTZTilt = 0x0008,
    kPTZZoom = 0x0010,
    kPTZFocus = 0x0020,
    kPTZPreset = 0x0040,
    kPTZLilin = 0x0080,
    kPTZAutoPan = 0x0100,
    kPTZAreaZoom = 0x0200,
    kPTZSpeedDomeOSDMenu = 0x400
};

typedef struct ST_Np_SensorProfile
{
    Np_StreamProfile profile;
    std::wstring frameRate;	
    std::wstring bitrate;	
    std::wstring resolution;	
    std::wstring codec;	
    std::wstring quality;	    	
} Np_SensorProfile;

typedef std::vector<Np_SensorProfile> Np_SensorProfileList;

typedef struct ST_Np_SubDevice
{
	Np_ID ID;
	std::wstring name;
	std::wstring description;
} Np_SubDevice;

typedef struct ST_Np_Device
{
	Np_ID ID;
    std::wstring name;
    std::wstring description;    
    std::vector<Np_SubDevice> SensorDevices;
    std::vector<Np_SubDevice> PTZDevices;
    std::vector<Np_SubDevice> AudioDevices;
    std::vector<Np_SubDevice> DIDevices;
    std::vector<Np_SubDevice> DODevices;
} Np_Device;

typedef struct ST_Np_DeviceGroup
{
	std::wstring name; //server name
	std::wstring description;
	std::vector<Np_Device> Camera;
	std::vector<Np_Device> IOBox;
} Np_DeviceGroup;

typedef struct ST_Np_DeviceList
{
	std::vector<Np_DeviceGroup> PhysicalGroup;
	std::vector<Np_DeviceGroup> LogicalGroup;
} Np_DeviceList;

typedef struct ST_Np_LogConditions
{
	bool operationLog;
	bool configurationLog;
} Np_LogConditions;

typedef struct ST_Np_OperationLog
{
	Np_DateTime occurTime;
	Np_ID sourceID; 
	int eventID;	
	std::wstring description;
} Np_OperationLog;

typedef struct ST_Np_ConfigurationLog
{
	Np_DateTime occurTime;	
	Np_ID sourceID; 
	int eventID;	
	std::wstring description;
	std::wstring user;
	std::wstring behavier;
} Np_ConfigurationLog;

typedef struct ST_Np_Log
{
	std::vector<Np_OperationLog> opLog;
	std::vector<Np_ConfigurationLog> cnfgLog;
} Np_Log;

typedef struct ST_Np_ScheduleLogItem
{
    Np_ID ID;
    Np_DateTime startTime;
    Np_DateTime endTime;
    Np_ScheduleType type;
} Np_ScheduleLogItem;

typedef struct ST_Np_RecordLogItem
{
    Np_ID ID;
    Np_DateTime startTime;
    Np_DateTime endTime;
} Np_RecordLogItem;

typedef struct ST_Np_ScheduleLogList
{
    int size;
    Np_ScheduleLogItem* logList;
} Np_ScheduleLogList;

typedef struct ST_Np_RecordLogList
{
    int size;
    Np_RecordLogItem* logList;
} Np_RecordLogList;

typedef struct ST_Np_ScheduleLogListList
{
	int size;
	Np_ScheduleLogList* logListList;
} Np_ScheduleLogListList;

typedef struct ST_Np_RecordLogListList
{
	int size;
	Np_RecordLogList* logListList;
} Np_RecordLogListList;

typedef struct ST_Np_Event
{
    Np_DateTime occurTime;
    Np_ID sourceID; 
    int eventID;
    wchar_t* auxiliaryCode;
    wchar_t* description;
    wchar_t* sourceName;
} Np_Event;

typedef struct ST_Np_EventList
{
    int size;
    Np_Event* list;
} Np_EventList;

typedef struct ST_Np_RecordDateList
{
    int size;
    Np_DateTime* dateList;
} Np_RecordDateList;

typedef struct ST_Np_PushNotificationDeviceInfo
{
    wchar_t* deviceID;
    wchar_t* deviceName;
    wchar_t* deviceToken;
} Np_PushNotificationDeviceInfo;

typedef struct ST_Np_IDList
{
    int size; 
    Np_ID* IDList;
} Np_IDList;

typedef struct ST_Np_Period
{
    Np_DateTime startTime;
    Np_DateTime endTime;
} Np_Period;

typedef struct ST_Np_SequencedRecordOption
{
    size_t size;
    Np_Period* seqPeriod;
} Np_SequencedRecordOption;

typedef struct ST_Np_SequencedRecord
{
    Np_ID id;
    Np_Period seqPeriod;
    int startSeq;
    int endSeq;
} Np_SequencedRecord;

typedef struct ST_Np_SequencedRecordList
{
    size_t size;
    Np_SequencedRecord* items;
} Np_SequencedRecordList;

typedef struct ST_Np_MetadataSearchCriterion
{
    Np_DateTime* startTime;
    Np_DateTime* endTime;
    Np_IDList metadataDeviceID;
    wchar_t* keyWord;
    bool usingRE;
} Np_MetadataSearchCriterion;

typedef struct ST_Np_MetadatalogItem
{
    Np_IDList cameraIDList;
    Np_DateTime metadataTime;
    Np_ID metadataDeviceID;
    int	codepage;
    int textDataLen;
    char* textData;
} Np_MetadatalogItem;

typedef struct ST_Np_MetadatalogList
{
    int size;
    Np_MetadatalogItem* metadataList;
} Np_MetadatalogList;

typedef struct ST_Np_MetadataChannel
{
    Np_ID id;
    wchar_t* name;
} Np_MetadataChannel;

typedef struct ST_Np_MetadataChannelList
{
    size_t size;
    Np_MetadataChannel* items;
} Np_MetadataChannelList;

typedef struct ST_Np_MetadataSource
{
    Np_ID id;
    wchar_t* name;
    wchar_t* ip;
    long port;
    Np_MetadataSourceType type;
    Np_MetadataChannelList channels;
} Np_MetadataSource;

typedef struct ST_Np_MetadataSourceList
{
    size_t size;
    Np_MetadataSource* items;
} Np_MetadataSourceList;

//////////////////////////////////////////////////////////////////
/*
 * Control related 
 */
//////////////////////////////////////////////////////////////////

typedef struct ST_Np_PTZContinuousMove
{		
	Np_PanDirection pan;
	Np_TiltDirection tilt;
	Np_ZoomDirection zoom;
	Np_FocusDirection focus;			
	int panSpeed;   // speed from 1 ~ 100, -1 as default
	int tiltSpeed;  // speed from 1 ~ 100, -1 as default
	int zoomSpeed;  // speed from 1 ~ 100, -1 as default
	int focusSpeed; // speed from 1 ~ 100, -1 as default	
} Np_PTZContinuousMove;

typedef struct ST_Np_PTZRectangle
{
    int currentWidth;
    int currentHeight;
    int leftupX;
    int leftupY;
    int targetWidth;
    int targetHeight;
    int speed;      // speed from 1 ~ 100, -1 as default    
} Np_PTZRectangle;

typedef struct ST_Np_PTZPreset
{
    int presetNo;
    std::wstring presetName;        
} Np_PTZPreset;

typedef std::vector<Np_PTZPreset> Np_PTZPresetList;

typedef union U_Np_PTZParameter
{
	Np_PTZContinuousMove* move;
	Np_PTZRectangle* rectangle;
	Np_PTZPreset* preset;
} Np_PTZParameter; 

typedef struct ST_Np_PTZControlParam
{
	Np_PTZCommand command;
	Np_PTZParameter param;
} Np_PTZControlParam;

typedef struct ST_Np_ServerInfo_MainConsole
{
    wchar_t* serverType;
    wchar_t* serverVersion;
    wchar_t* modelType;
    wchar_t* oemName;
    int sessionNumber;
    int maximumChannel;
    int allowSmartGuard;
    int allowPOS;
    int allowCounting;
    int allowEMap;
    int dbType;
} Np_ServerInfo_MainConsole;

typedef union U_Np_ServerInfo
{
    Np_ServerInfo_MainConsole* serverInfoMC;
} Np_ServerInfo;

typedef struct ST_Np_ExportContent
{
    Np_ID id;
    Np_DateTime startTime;
    Np_DateTime endTime;
    bool excludeAudio;
    int width;
    int height;
} Np_ExportContent;

typedef struct ST_Np_ExportProfile
{
    int profile;
    wchar_t* description;
} Np_ExportProfile;

typedef struct ST_Np_ExportProfileList
{
    size_t size;
    Np_ExportProfile* items;
} Np_ExportProfileList;

typedef struct ST_Np_ExportFormat
{
    int format;
    wchar_t* description;
    Np_ExportProfileList supportedProfile;
} Np_ExportFormat;

typedef struct ST_Np_ExportFormatList
{
    size_t size;
    Np_ExportFormat* items;
} Np_ExportFormatList;

//////////////////////////////////////////////////////////////////
/*
* CSharp Related
*/
//////////////////////////////////////////////////////////////////

typedef struct ST_Np_SensorProfile_CS
{
    Np_StreamProfile profile;
    wchar_t* frameRate;	
    wchar_t* bitrate;	
    wchar_t* resolution;	
    wchar_t* codec;	
    wchar_t* quality;	    	
} Np_SensorProfile_CS;

typedef struct ST_Np_SensorProfile_CS1
{
    Np_StreamProfile profile;
	wchar_t* frameRate;
	wchar_t* bitrate;
	wchar_t* resolution;
	wchar_t* codec;
    wchar_t* quality;
	wchar_t* profile_name_alias;
	wchar_t* raw_source;
	wchar_t* resolution_height;
	wchar_t* resolution_width;
	wchar_t* source_stream;
} Np_SensorProfile_CS1;

typedef struct ST_Np_SensorProfile_CS_List
{
    size_t size;
    Np_SensorProfile_CS* items;
} Np_SensorProfile_CS_List;

typedef struct ST_Np_SensorProfile_CS1_List
{
	size_t size;
	Np_SensorProfile_CS1* items;
} Np_SensorProfile_CS1_List;

typedef struct ST_Np_SubDevice_CS
{
    Np_ID ID;
    wchar_t* name;
    wchar_t* description;
} Np_SubDevice_CS;

typedef struct ST_Np_SubDevice_CS_List
{
    size_t size;
    Np_SubDevice_CS* items;
} Np_SubDevice_CS_List;

typedef struct ST_Np_Device_CS
{
	Np_ID ID;
    wchar_t* name;
    wchar_t* description;    
    Np_SubDevice_CS_List SensorDevices;
    Np_SubDevice_CS_List PTZDevices;
    Np_SubDevice_CS_List AudioDevices;
    Np_SubDevice_CS_List DIDevices;
    Np_SubDevice_CS_List DODevices;
} Np_Device_CS;

typedef struct ST_Np_Device_CS_List
{
    size_t size;
    Np_Device_CS* items;
} Np_Device_CS_List;

typedef struct ST_Np_DeviceGroup_CS
{
	wchar_t* name; //server name
	wchar_t* description;
	Np_Device_CS_List Camera;
	Np_Device_CS_List IOBox;
} Np_DeviceGroup_CS;

typedef struct ST_Np_DeviceGroup_CS_List
{
    size_t size;
    Np_DeviceGroup_CS* items;
} Np_DeviceGroup_CS_List;

typedef struct ST_Np_DeviceList_CS
{
	Np_DeviceGroup_CS_List PhysicalGroup;
	Np_DeviceGroup_CS_List LogicalGroup;
} Np_DeviceList_CS;

typedef struct ST_Np_OperationLog_CS
{
	Np_DateTime occurTime;
	Np_ID sourceID; 
	int eventID;	
	wchar_t* description;
} Np_OperationLog_CS;

typedef struct ST_Np_OperationLog_CS_List
{
    size_t size;
    Np_OperationLog_CS* items;
} Np_OperationLog_CS_List;

typedef struct ST_Np_ConfigurationLog_CS
{
	Np_DateTime occurTime;	
	Np_ID sourceID; 
	int eventID;	
	wchar_t* description;
	wchar_t* user;
	wchar_t* behavier;
} Np_ConfigurationLog_CS;

typedef struct ST_Np_ConfigurationLog_CS_List
{
    size_t size;
    Np_ConfigurationLog_CS* items;
} Np_ConfigurationLog_CS_List;

typedef struct ST_Np_Log_CS
{
	Np_OperationLog_CS_List opLog;
	Np_ConfigurationLog_CS_List cnfgLog;
} Np_Log_CS;

typedef struct ST_Np_PTZPreset_CS
{
    int presetNo;
    wchar_t* presetName;
} Np_PTZPreset_CS;

typedef struct ST_Np_PTZPreset_CS_List
{
    size_t size;
    Np_PTZPreset_CS* items;
} Np_PTZPreset_CS_List;

typedef struct ST_Np_PTZPresetType_CS
{
    bool allowSetPresetByIndex;
    bool allowClearAllPreset;
    size_t maxPresetNumber;
} Np_PTZPresetType_CS;

typedef struct ST_Np_PatrolGroup_CS
{
    ST_Np_PatrolGroup_CS()
    {
        memset(m_preset_point, 0, MAX_PATROL_POINT * sizeof(int));
        m_preset_count = 0;
        m_period = 0;
        m_time = 0;
        m_next_point = 0;
        memset(m_name, 0, MAX_GROUP_NAME_LENGTH * sizeof(wchar_t));
    }

    int m_preset_point[MAX_PATROL_POINT];
    int m_preset_count;
    int m_period;
    int m_time;
    int m_next_point;
    wchar_t m_name[MAX_GROUP_NAME_LENGTH];
} Np_PatrolGroup_CS;

typedef struct ST_Np_PTZPatrol_CS
{
    bool isPatrolStartEnabled;
    bool isPatrolStopEnabled;
    bool m_is_patrol_enable;
    int m_active_group_index;
    int m_event_trigger_group_index;
    WORD m_max_patrol_group_number;
    Np_PatrolGroup_CS m_group[MAX_GROUP_AMOUNT];
} Np_PTZPatrol_CS;

typedef struct ST_Np_PTZInfo_CS
{
    Np_PTZPresetType_CS ptzPresetType;
    Np_PTZPreset_CS_List ptzPresetList;
    Np_PTZPatrol_CS ptzPatrol;
} Np_PTZInfo_CS;

typedef union U_Np_PTZParameter_CS
{
	Np_PTZContinuousMove* move;
	Np_PTZRectangle* rectangle;
    Np_PTZPreset_CS* preset;
} Np_PTZParameter_CS;

typedef struct ST_Np_PTZControlParam_CS
{
	Np_PTZCommand command;
	Np_PTZParameter_CS param;
} Np_PTZControlParam_CS;

//////////////////////////////////////////////////////////////////
/*
*  Talk Related 
*/
//////////////////////////////////////////////////////////////////

typedef struct ST_Np_TalkAudioFormat
{
    int channels;
    int bitsPerSample; 
    int sampleRate; 
    int sampleRequest;
} Np_TalkAudioFormat;

//////////////////////////////////////////////////////////////////
/*
* New structures for crystal server
*/
//////////////////////////////////////////////////////////////////
typedef struct ST_Np_ID_Ext
{
	unsigned long long centralID;
	unsigned long long localID;
} Np_ID_Ext;

typedef struct ST_Np_IDList_Ext
{
	int	size; 
	Np_ID_Ext* items;
} Np_IDList_Ext;

typedef struct ST_Np_SubDevice_Ext
{
	Np_ID_Ext ID;
	wchar_t* name;
	wchar_t* description;
} Np_SubDevice_Ext;

typedef struct ST_Np_SubDevice_Ext_List
{
	size_t size;
	Np_SubDevice_Ext* items;
} Np_SubDevice_Ext_List;

typedef struct ST_Np_Device_Ext
{
	Np_ID_Ext ID;
	wchar_t* name;
	wchar_t* description;    
	Np_SubDevice_Ext_List SensorDevices;
	Np_SubDevice_Ext_List PTZDevices;
	Np_SubDevice_Ext_List AudioDevices;
	Np_SubDevice_Ext_List DIDevices;
	Np_SubDevice_Ext_List DODevices;
} Np_Device_Ext;

typedef struct ST_Np_Device_Ext_List
{
	size_t size;
	Np_Device_Ext* items;
} Np_Device_Ext_List;

typedef struct ST_Np_DeviceGroup_Ext
{
	wchar_t* name; //server name
	wchar_t* description;
	Np_Device_Ext_List Camera;
	Np_Device_Ext_List IOBox;
} Np_DeviceGroup_Ext;

typedef struct ST_Np_DeviceGroup_Ext_List
{
	size_t size;
	Np_DeviceGroup_Ext* items;
} Np_DeviceGroup_Ext_List;

typedef struct ST_Np_DeviceList_Ext
{
	Np_DeviceGroup_Ext_List PhysicalGroup;
	Np_DeviceGroup_Ext_List LogicalGroup;
} Np_DeviceList_Ext;

typedef struct ST_Np_Server
{
	Np_ID_Ext ID;
    int uiOrder;
	wchar_t* name;
	wchar_t* description;
} Np_Server;

typedef struct ST_Np_ServerList
{
	int	size;
	Np_Server* items;
} Np_ServerList;

typedef struct ST_Np_Event_Ext
{
    Np_DateTime occurTime;
    Np_ID_Ext sourceID; 
    int eventID;
    wchar_t* auxiliaryCode;
    wchar_t* description;
    wchar_t* sourceName;
} Np_Event_Ext;

typedef struct ST_Np_EventList_Ext
{
    int size;
    Np_Event_Ext* list;
} Np_EventList_Ext;

typedef struct ST_Np_RecordLogItemExt
{
    Np_ID_Ext ID;
    Np_DateTime startTime;
    Np_DateTime endTime;
} Np_RecordLogItemExt;

typedef struct ST_Np_RecordLogListExt
{
    int size;
    Np_RecordLogItemExt* logList;
} Np_RecordLogListExt;

typedef struct ST_Np_RecordLogListListExt
{
	int size;
	Np_RecordLogListExt* logListList;
} Np_RecordLogListListExt;

typedef struct ST_Np_ExportContent_Ext
{
    Np_ID_Ext id;
    Np_DateTime startTime;
    Np_DateTime endTime;
    bool excludeAudio;
    int width;
    int height;
} Np_ExportContent_Ext;

//Metadata
typedef struct ST_Np_MetadataSearchCriterion_Ext
{
    Np_DateTime* startTime;
    Np_DateTime* endTime;
    Np_IDList_Ext metadataDeviceID;
    wchar_t* keyWord;
    bool usingRE;
} Np_MetadataSearchCriterion_Ext;

typedef struct ST_Np_MetadatalogItem_Ext
{
    Np_IDList_Ext cameraIDList;
    Np_DateTime metadataTime;
    Np_ID_Ext metadataDeviceID;
    int	codepage;
    int textDataLen;
    char* textData;
} Np_MetadatalogItem_Ext;

typedef struct ST_Np_MetadatalogList_Ext
{
    int size;
    Np_MetadatalogItem_Ext* metadataList;
} Np_MetadatalogList_Ext;

typedef struct ST_Np_MetadataChannel_Ext
{
    Np_ID_Ext id;
    wchar_t* name;
} Np_MetadataChannel_Ext;

typedef struct ST_Np_MetadataChannelList_Ext
{
    size_t size;
    Np_MetadataChannel_Ext* items;
} Np_MetadataChannelList_Ext;

typedef struct ST_Np_MetadataSource_Ext
{
    Np_ID_Ext id;
    wchar_t* name;
    wchar_t* ip;
    long port;
    Np_MetadataSourceType type;
    Np_MetadataChannelList_Ext channels;
} Np_MetadataSource_Ext;

typedef struct ST_Np_MetadataSourceList_Ext
{
    size_t size;
    Np_MetadataSource_Ext* items;
} Np_MetadataSourceList_Ext;

//////////////////////////////////////////////////////////////////
/*
 * Liveview/Playback/Event/Metadata callback related
 */
//////////////////////////////////////////////////////////////////

typedef void (*fnVideoHandle)(Np_DateTime time, char* buffer, int len, int width, int height, void* ctx);

typedef void (*fnAudioHandle)(Np_DateTime time, char* buffer, int len, int bitsPerSample, int samplesPerSec, 
                              int channels, void* ctx);
                           
typedef void (*fnRawVideoHandle)(Np_DateTime time, char* buffer, int len, bool isKeyFrame, Np_VideoCodec videoCodec,
                           	     void* ctx);

typedef void (*fnRawVideoWithFrameTypeHandle)(Np_DateTime time, char* buffer, int len, Np_FrameType frameType,
                                              Np_VideoCodec videoCodec, void* ctx);

typedef void (*fnGetRawFrameHandle)(Np_GetRawFrameStatus status, void* usrCtx);

typedef void (*fnErrorHandle)(Np_Error error, void* ctx);

typedef void (*fnExportHandle)(Np_ID id, Np_ExportError error, unsigned int percent, int iFormatChangedIndex,
                               void* usrCtx);

typedef void (*fnOSDHandle)(Np_ID id, char* buffer, int width, int height, Np_DateTime time, void* ctx);

typedef void (*fnMetadataHandle)(Np_ID id, char* textData, unsigned int codePage, bool isNew, bool isComplete,
                                 Np_Rectangle displayRectangle, int displayTimeout, bool isUseDefaultRect, 
                                 int textDataLen, void* ctx);

typedef void (*fnBackupHandle)(Np_BackupStatus status, const wchar_t* fileName, int updateFilePayloadSize, 
                               void* usrCtx);

typedef void (*fnEventHandle)(Np_Event, void* ctx);

typedef void (*fnEventHandleExt)(Np_Event_Ext, void* ctx);

typedef void (*fnRecordLogHandle)(Np_LogUpdateStatus status, Np_RecordLogListExt logs, void* ctx);

typedef void (*fnRecordDateHandle)(Np_RecordDateList dates, void* ctx);

typedef void (*fnQueryEventHandle)(Np_EventList_Ext events, void* ctx);

typedef void (*fnExportHandleExt)(Np_ID_Ext id, Np_ExportError error, unsigned int percent, int formatChangedIndex, 
                                  void* ctx);

typedef void (*fnOSDHandleExt)(Np_ID_Ext id, char* buffer, int width, int height, Np_DateTime time, void* ctx);

//////////////////////////////////////////////////////////////////
/*
 *  Create/Destroy Handler 
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t Create_Handle(void** handle, Np_ServerType type, std::wstring username, std::wstring passwd,
									 std::wstring ipaddress, WORD port);

SDK_EXPORT Np_Result_t Create_HandleWChar(void** handle, Np_ServerType type, const wchar_t* username, 
                                          const wchar_t* passwd, const wchar_t* ipaddress, WORD port);

SDK_EXPORT Np_Result_t Create_Handle_And_Event_Subscribe(void** handle, Np_ServerType type, const wchar_t* username, 
                                                         const wchar_t* passwd, const wchar_t* ipaddress, 
                                                         WORD port, fnEventHandle callback, void* ctx);

SDK_EXPORT Np_Result_t Create_Lite_Handle(void** handle, Np_ServerType type, const wchar_t* username,
                                          const wchar_t* password, const wchar_t* ipaddress, WORD port);

SDK_EXPORT Np_Result_t Create_Handle_With_DeviceInfo(void** handle, Np_ServerType type, const wchar_t* username, 
                                          const wchar_t* passwd, const wchar_t* ipaddress, WORD port, Np_DeviceCapability deviceCapability);

SDK_EXPORT Np_Result_t Destroy_Handle(void* handle);

//////////////////////////////////////////////////////////////////
/*
 *  Get server related info 
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t Info_GetDeviceIDList(void* handle, size_t& iDevNum, Np_ID** ppIDList);

SDK_EXPORT Np_Result_t Info_DestroyDeviceIDList(Np_ID* ppIDList);


SDK_EXPORT Np_Result_t Info_GetServerIdentification(void* handle, wchar_t** identification);

SDK_EXPORT Np_Result_t Info_ReleaseServerIdentification(void* handle, wchar_t** identification);

SDK_EXPORT Np_Result_t Info_GetServerInfo(void* handle, Np_ServerInfo& serverInfo);

SDK_EXPORT Np_Result_t Info_ReleaseServerInfo(void* handle, Np_ServerInfo& serverInfo);

SDK_EXPORT Np_Result_t Info_GetDeviceList(void* handle, Np_DeviceList& deviceList);

SDK_EXPORT Np_Result_t Info_GetDeviceList_CS(void* handle, Np_DeviceList_CS& deviceList);

SDK_EXPORT Np_Result_t Info_ReleaseDeviceList_CS(void* handle, Np_DeviceList_CS& deviceList);

SDK_EXPORT Np_Result_t Info_GetDeviceList_Ext(void* handle, Np_DeviceList_Ext& deviceList);

SDK_EXPORT Np_Result_t Info_GetDeviceListFromServer(void* handle, Np_ID_Ext id, Np_DeviceList_Ext& deviceList);

SDK_EXPORT Np_Result_t Info_ReleaseDeviceList_Ext(void* handle, Np_DeviceList_Ext& deviceList);

SDK_EXPORT Np_Result_t Info_GetRecordingServerList(void* handle, Np_ServerList& list);

SDK_EXPORT Np_Result_t Info_ReleaseRecordingServerList(void* handle, Np_ServerList& list);

SDK_EXPORT Np_Result_t Info_GetLog(void* handle, Np_LogConditions conditions, Np_Log& logs);

SDK_EXPORT Np_Result_t Info_GetLog_CS(void* handle, Np_LogConditions conditions, Np_Log_CS& logs);

SDK_EXPORT Np_Result_t Info_ReleaseLog_CS(void* handle, Np_Log_CS& logs);

SDK_EXPORT Np_Result_t Info_GetDeviceCapability(void* handle, Np_ID id, long long& capability);

SDK_EXPORT Np_Result_t Info_GetDeviceCapability_Ext(void* handle, Np_ID_Ext id, long long& capability);

SDK_EXPORT Np_Result_t Info_GetDeviceOrder(void* handle, Np_ID_Ext id, int& order);

SDK_EXPORT Np_Result_t Info_GetDIOStatus(void* handle, Np_ID id, Np_DIOStatus& status);

SDK_EXPORT Np_Result_t Info_GetDIOStatus_Ext(void* handle, Np_ID_Ext id, Np_DIOStatus& status);

SDK_EXPORT Np_Result_t Info_GetDOPrivilege(void* handle, Np_ID id, DWORD& dwDOPrivilege);

SDK_EXPORT Np_Result_t Info_GetDOPrivilegeExt(void* handle, Np_ID_Ext id, DWORD& dwDOPrivilege);

SDK_EXPORT Np_Result_t Info_GetDIAssociatedDevice(void* handle, Np_ID id, Np_ID& associatedDevice);

SDK_EXPORT Np_Result_t Info_GetPTZPreset(void* handle, Np_ID id, Np_PTZPresetList& ptzPresetList);

SDK_EXPORT Np_Result_t Info_GetPTZPreset_CS(void* handle, Np_ID id, Np_PTZPreset_CS_List& ptzPresetList);

SDK_EXPORT Np_Result_t Info_GetPTZPreset_CS_Ext(void* handle, Np_ID_Ext id, Np_PTZPreset_CS_List& ptzPresetList);

SDK_EXPORT Np_Result_t Info_ReleasePTZPreset_CS(void* handle, Np_PTZPreset_CS_List& ptzPresetList);

SDK_EXPORT Np_Result_t Info_ReleasePTZPreset_CS_Ext(void* handle, Np_PTZPreset_CS_List& ptzPresetList);

SDK_EXPORT Np_Result_t Info_GetPTZInfo_CS(void* handle, Np_ID id, Np_PTZInfo_CS& ptzInfo);

SDK_EXPORT Np_Result_t Info_ReleasePTZInfo_CS(void* handle, Np_PTZInfo_CS& ptzInfo);

SDK_EXPORT Np_Result_t Info_GetPTZCapability(void* handle, Np_ID id, long long& ptzCapability);

SDK_EXPORT Np_Result_t Info_GetPTZCapabilityExt(void* handle, Np_ID_Ext id, long long& ptzCapability);

SDK_EXPORT Np_Result_t Info_GetSensorProfileList(void* handle, Np_ID id, Np_SensorProfileList& sensorProfileList);

SDK_EXPORT Np_Result_t Info_GetSensorProfileList_CS(void* handle, Np_ID id, 
                                                    Np_SensorProfile_CS_List& sensorProfileList);

SDK_EXPORT Np_Result_t Info_GetSensorProfileList_CS1(void* handle, Np_ID id, 
                                                     Np_SensorProfile_CS1_List& sensorProfileList);

SDK_EXPORT Np_Result_t Info_GetSensorProfileList_Ext(void* handle, Np_ID_Ext id, 
                                                     Np_SensorProfile_CS_List& sensorProfileList);

SDK_EXPORT Np_Result_t Info_ReleaseSensorProfileList_CS(void* handle, Np_SensorProfile_CS_List& sensorProfileList);

SDK_EXPORT Np_Result_t Info_ReleaseSensorProfileList_CS1(void* handle, Np_SensorProfile_CS1_List& sensorProfileList);

SDK_EXPORT Np_Result_t Info_GetRecordDateList(void* handle, Np_RecordDateList& recordDateList);

SDK_EXPORT Np_Result_t Info_GetRangedRecordDateList(void* handle, Np_DateTime* startDate, Np_DateTime* endDate, 
                                                    Np_RecordDateList& recordDateList);

SDK_EXPORT Np_Result_t Info_GetRecordDateListExt(void* handle, Np_DateTime query_month);

SDK_EXPORT Np_Result_t Info_GetRecordDateList_Lazy(void* handle, Np_RecordDateList& recordDateList);

SDK_EXPORT Np_Result_t Info_GetRangedRecordDateList_Lazy(void* handle, Np_DateTime* startDate, Np_DateTime* endDate, 
                                                         Np_RecordDateList& recordDateList);

SDK_EXPORT Np_Result_t Info_ReleaseRecordDateList(void* handle, Np_RecordDateList& recordDateList);

SDK_EXPORT Np_Result_t Info_GetScheduleLogs(void* handle, Np_DateTime date, Np_ScheduleLogList& scheduleLogList);

SDK_EXPORT Np_Result_t Info_GetScheduleLogs_Lazy(void* handle, Np_DateTime date, Np_ScheduleLogList& scheduleLogList);

SDK_EXPORT Np_Result_t Info_GetScheduleLogsList(void* handle, Np_DateTime date, 
                                                Np_ScheduleLogListList& scheduleLogListList);

SDK_EXPORT Np_Result_t Info_GetScheduleLogsList_Lazy(void* handle, Np_DateTime date, 
                                                     Np_ScheduleLogListList& scheduleLogListList);

SDK_EXPORT Np_Result_t Info_ReleaseScheduleLogs(void* handle, Np_ScheduleLogList& scheduleLogList);

SDK_EXPORT Np_Result_t Info_ReleaseScheduleLogsList(void* handle, Np_ScheduleLogListList& scheduleLogListList);

SDK_EXPORT Np_Result_t Info_GetRecordLogs(void* handle, Np_DateTime date, Np_RecordLogList& scheduleLogList);

SDK_EXPORT Np_Result_t Info_GetRecordLogsExt(void* handle, Np_DateTime date, Np_RecordLogListExt& recordLogList);

SDK_EXPORT Np_Result_t Info_QueryRecordLogsExt(void* handle, Np_IDList_Ext idList, Np_DateTime startTime, Np_DateTime endTime, Np_RecordLogListExt& recordLogList);

SDK_EXPORT Np_Result_t Info_GetRecordLogsListExt(void* handle, Np_DateTime date, 
                                                 Np_RecordLogListListExt& recordLogListList);

SDK_EXPORT Np_Result_t Info_GetRecordLogs_Lazy(void* handle, Np_DateTime date, Np_RecordLogList& scheduleLogList);

SDK_EXPORT Np_Result_t Info_GetSequencedRecord(void* handle, Np_DateTime startTime, Np_DateTime endTime, 
                                               Np_IDList idList, Np_SequencedRecordList &options);

SDK_EXPORT Np_Result_t Info_ReleaseSequencedRecord(void* handle, Np_SequencedRecordList &options);

SDK_EXPORT Np_Result_t Info_ReleaseRecordLogs(void* handle, Np_RecordLogList& recordLogList);

SDK_EXPORT Np_Result_t Info_ReleaseRecordLogsExt(void* handle, Np_RecordLogListExt& recordLogList);

SDK_EXPORT Np_Result_t Info_ReleaseRecordLogsListExt(void* handle, Np_RecordLogListListExt& recordLogListList);

SDK_EXPORT Np_Result_t Info_QueryEvents(void* handle, Np_DateTime* startTime, Np_DateTime* endTime, int* deviceTypeID, 
                                        Np_ID* deviceID, int* eventID, Np_EventList& eventList);

SDK_EXPORT Np_Result_t Info_QueryEventsExt(void* handle, Np_DateTime* startTime, Np_DateTime* endTime,
                                           Np_SourceType* deviceTypeID, unsigned long long* serverID,
                                           unsigned long long* deviceID, int* eventID);

SDK_EXPORT Np_Result_t Info_ReleaseEvents(void* handle, Np_EventList& eventList);

SDK_EXPORT Np_Result_t Info_ReleaseEvents_Ext(void* handle, Np_EventList_Ext& eventList);

SDK_EXPORT Np_Result_t Info_RefreshRecordLogs(void* handle);

SDK_EXPORT Np_Result_t Info_GetMetadataLog(void* handle, Np_MetadataSearchCriterion criterion, Np_MetadatalogList& list, 
                                           bool& isLogExceedMaxLimit);

SDK_EXPORT Np_Result_t Info_GetMetadataLog_Ext(void* handle, Np_MetadataSearchCriterion_Ext criterion, Np_MetadatalogList_Ext& list, 
                                           bool& isLogExceedMaxLimit);

SDK_EXPORT Np_Result_t Info_ReleaseMetadataLog(void* handle, Np_MetadatalogList& metadataLogList);

SDK_EXPORT Np_Result_t Info_ReleaseMetadataLog_Ext(void* handle, Np_MetadatalogList_Ext& metadataLogList);

SDK_EXPORT Np_Result_t Info_CheckPushNotificationReg(void* handle, Np_PushNotificationDeviceInfo pnInfo, 
                                                     Np_PNStatus& status);

SDK_EXPORT Np_Result_t Info_RegisterPushNotification(void* handle, Np_PushNotificationDeviceInfo pnInfo);

SDK_EXPORT Np_Result_t Info_UnRegPushNotification(void* handle, Np_PushNotificationDeviceInfo pnInfo);

SDK_EXPORT Np_Result_t Info_GetMetadataSourceList(void* handle, Np_MetadataSourceList& list);

SDK_EXPORT Np_Result_t Info_GetMetadataSourceList_Ext(void* handle, Np_MetadataSourceList_Ext& list);

SDK_EXPORT Np_Result_t Info_ReleaseMetadataSourceList(void* handle, Np_MetadataSourceList& list);

SDK_EXPORT Np_Result_t Info_ReleaseMetadataSourceList_Ext(void* handle, Np_MetadataSourceList_Ext& list);

SDK_EXPORT Np_Result_t Info_GetBackupFileSize(void* handle, Np_DateTime startTime, Np_DateTime endTime, 
                                              Np_IDList backupIdList, Np_SequencedRecordList& seqRecordList,
                                              bool isIncluedExeFile, unsigned long long& size);

SDK_EXPORT Np_Result_t Info_GetMaxDurationDay(void* handle, int& durationDay);

SDK_EXPORT Np_Result_t Info_UpdateDeviceInfo(void* handle, Np_DeviceCapability deviceCapability);

//////////////////////////////////////////////////////////////////
/*
 *  PTZ related
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t Control_PTZ(void* handle, Np_ID id, Np_PTZControlParam* param);

SDK_EXPORT Np_Result_t Control_PTZ_PTZDeviceID(void* handle, Np_ID id, Np_PTZControlParam* param);

SDK_EXPORT Np_Result_t Control_PTZ_CS(void* handle, Np_ID id, Np_PTZControlParam_CS* param);

SDK_EXPORT Np_Result_t Control_PTZ_CS_Ext(void* handle, Np_ID_Ext id, Np_PTZControlParam_CS* param);

SDK_EXPORT Np_Result_t Control_PTZ_PTZDeviceID_CS(void* handle, Np_ID id, Np_PTZControlParam_CS* param);

SDK_EXPORT Np_Result_t Control_PTZ_PTZDeviceID_CS_Ext(void* handle, Np_ID_Ext id, Np_PTZControlParam_CS* param);

SDK_EXPORT Np_Result_t Control_DigitalOutput(void* handle, Np_ID id, bool turnOn);

SDK_EXPORT Np_Result_t Control_DigitalOutput_Ext(void* handle, Np_ID_Ext id, bool turnOn);

SDK_EXPORT Np_Result_t Control_SetPatrol(void* handle, Np_ID id, const Np_PTZPatrol_CS& info);

//////////////////////////////////////////////////////////////////
/*
 *  Talk related 
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t Talk_Enable(void* handle, Np_ID id, Np_TalkAudioFormat& fmt);

SDK_EXPORT Np_Result_t Talk_Disable(void* handle);

SDK_EXPORT Np_Result_t Talk_SendAudioPacket(void* handle, char* buf, int size);

SDK_EXPORT Np_Result_t Talk_GetEnabledID(void* handle, Np_ID& id);

//////////////////////////////////////////////////////////////////
/*
 *  
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t RecordingLog_Subscribe(void* handle, void** session, fnRecordLogHandle callback, void* ctx);

SDK_EXPORT Np_Result_t RecordingLog_Unsubscribe(void* session);

SDK_EXPORT Np_Result_t RecordingDate_Subscribe(void* handle, void** session, fnRecordDateHandle callback, 
                                               void* ctx);

SDK_EXPORT Np_Result_t RecordingDate_Unsubscribe(void* session);

SDK_EXPORT Np_Result_t QueryEvent_Subscribe(void* handle, void** session, fnQueryEventHandle callback, void* ctx);

SDK_EXPORT Np_Result_t QueryEvent_Unsubscribe(void* session);

SDK_EXPORT Np_Result_t Event_Subscribe(void* handle, void** session, fnEventHandle callback, void* ctx); 

SDK_EXPORT Np_Result_t Event_SubscribeExt(void* handle, void** session, fnEventHandleExt callback, void* ctx); 

SDK_EXPORT Np_Result_t Event_Unsubscribe(void* session);

SDK_EXPORT Np_Result_t Event_UnsubscribeExt(void* session);

SDK_EXPORT Np_Result_t Event_DuplicateFilter(void* handle, int timer);

//////////////////////////////////////////////////////////////////
/*
 *  Liveview related 
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t LiveView_CreatePlayer(void* handle, void** player);

SDK_EXPORT Np_Result_t LiveView_DestroyPlayer(void* player);

SDK_EXPORT Np_Result_t LiveView_AttachSession(void* player, void** session, Np_ID id, Np_StreamProfile profile, 
								              fnVideoHandle vcb, void* vctx, fnAudioHandle acb, void* actx, 
								              fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t LiveView_AttachSessionExt(void* player, void** session, Np_ID id, Np_StreamProfile profile,
                                                 Np_PixelFormat videoPixalFormat, fnVideoHandle vcb, void* vctx,
                                                 fnAudioHandle acb, void* actx, fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t LiveView_AttachSessionExtExt(void* player, void** session, Np_ID_Ext id, Np_StreamProfile profile,
                                                    Np_PixelFormat videoPixalFormat, fnVideoHandle vcb, void* vctx,
                                                    fnAudioHandle acb, void* actx, fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t LiveView_AttachRawVideoSession(void* player, void** session, Np_ID id,
                                                      Np_StreamProfile profile, fnRawVideoHandle vcb, void* vctx, 
                                                      fnAudioHandle acb, void* actx, fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t LiveView_DetachSession(void* player, void* session);

SDK_EXPORT Np_Result_t LiveView_DetachSessionExtExt(void* player, void* session);

SDK_EXPORT Np_Result_t LiveView_SetAudioOn(void* player, void* session);

SDK_EXPORT Np_Result_t LiveView_SetAudioOff(void* player);

SDK_EXPORT Np_Result_t LiveView_GetSessionAudioStatus(void* player, void* session, Np_AudioStatus& status);

SDK_EXPORT Np_Result_t LiveView_GetSessionBitrate(void* player, void* session, double& bitrate);

SDK_EXPORT Np_Result_t LiveView_GetSessionCurrentImage(void* player, void* session, Np_Frame& frame);

SDK_EXPORT Np_Result_t LiveView_ReleaseSessionCurrentImage(void* player, Np_Frame& frame);

//////////////////////////////////////////////////////////////////
/*
 *  Playback related 
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t PlayBack_CreatePlayer(void* handle, void** player);

SDK_EXPORT Np_Result_t PlayBack_DestroyPlayer(void* player);

SDK_EXPORT Np_Result_t PlayBack_AttachSession(void* player, void** session, Np_ID id, fnVideoHandle vcb, void* vctx, 
								              fnAudioHandle acb, void* actx, fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t PlayBack_AttachSessionExt(void* player, void** session, Np_ID id, Np_PixelFormat videoPixelFormat,
                                                 fnVideoHandle vcb, void* vctx, fnAudioHandle acb, void* actx, 
                                                 fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t PlayBack_AttachSession1Ext(void* player, void** session, Np_ID id, 
                                                  Np_PixelFormat videoPixelFormat, int recordIndex,
										          fnVideoHandle vcb, void* vctx, fnAudioHandle acb, void* actx,
										          fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t PlayBack_AttachSessionExtExt(void* player, void** session, Np_ID_Ext id,
                                                    Np_PixelFormat format, fnVideoHandle vcb, void* vctx, 
                                                    fnAudioHandle acb, void* actx, fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t PlayBack_AttachSession1ExtExt(void* player, void** session, Np_ID_Ext id,
										             Np_PixelFormat format, int recordIndex, 
                                                     fnVideoHandle vcb, void* vctx, fnAudioHandle acb, void* actx,
										             fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t PlayBack_AttachRawVideoSession(void* player, void** session, Np_ID id, 								    
                                                      fnRawVideoHandle vcb, void* vctx, fnAudioHandle acb, void* actx, 
                                                      fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t PlayBack_AttachRawVideoWithFrameTypeSession(void* player, void** session, Np_ID id,				    
                                                                   fnRawVideoWithFrameTypeHandle vcb, void* vctx, 
                                                                   fnAudioHandle acb, void* actx, 
                                                                   fnErrorHandle ecb, void* ectx);

SDK_EXPORT Np_Result_t PlayBack_DetachSession(void* player, void* session);

SDK_EXPORT Np_Result_t PlayBack_DetachSessionExtExt(void* player, void* session);

SDK_EXPORT Np_Result_t PlayBack_SetAudioOn(void* player, void* session);

SDK_EXPORT Np_Result_t PlayBack_SetAudioOff(void* player);

SDK_EXPORT Np_Result_t PlayBack_Seek(void* player, Np_DateTime time);

SDK_EXPORT Np_Result_t PlayBack_OpenRecord(void* player, Np_DateTime startTime, Np_DateTime endTime);

SDK_EXPORT Np_Result_t PlayBack_OpenSequencedRecord(void* player, Np_DateTime startTime, Np_DateTime endTime, 
                                                    Np_SequencedRecordList& seqRecordList);

SDK_EXPORT Np_Result_t PlayBack_Play(void* player);

SDK_EXPORT Np_Result_t PlayBack_ReversePlay(void* player);

SDK_EXPORT Np_Result_t PlayBack_Pause(void* player);

SDK_EXPORT Np_Result_t PlayBack_Stop(void* player);

SDK_EXPORT Np_Result_t PlayBack_StepForward(void* player);

SDK_EXPORT Np_Result_t PlayBack_StepBackward(void* player);

SDK_EXPORT Np_Result_t PlayBack_Next(void* player);

SDK_EXPORT Np_Result_t PlayBack_Previous(void* player);

SDK_EXPORT Np_Result_t PlayBack_NextFrame(void* player, void* focusSession);

SDK_EXPORT Np_Result_t PlayBack_PreviousFrame(void* player, void* focusSession);

SDK_EXPORT Np_Result_t PlayBack_SetSpeed(void* player, float speed); 

SDK_EXPORT Np_Result_t PlayBack_GetSpeed(void* player, float& speed);

SDK_EXPORT Np_Result_t PlayBack_GetTime(void* player, Np_DateTime& time);

SDK_EXPORT Np_Result_t PlayBack_GetPlayerState(void* player, Np_PlayerState& state);

SDK_EXPORT Np_Result_t PlayBack_ExportVideo(void* player, Np_ExportContent content, const wchar_t* filename, 
                                            int format, int profile, fnExportHandle ecb, void* ectx,
                                            fnOSDHandle ocb, void* octx);

SDK_EXPORT Np_Result_t PlayBack_StopExport(void* player);

SDK_EXPORT Np_Result_t PlayBack_ExportVideo_Ext(void* player, Np_ExportContent_Ext content, const wchar_t* filename, 
                                                int format, int profile, fnExportHandleExt ecb, void* ectx,
                                                fnOSDHandleExt ocb, void* octx);

SDK_EXPORT Np_Result_t PlayBack_StopExport_Ext(void* player);

SDK_EXPORT Np_Result_t PlayBack_GetExportFormatList(void* player, Np_ExportFormatList& fmtlist);

SDK_EXPORT Np_Result_t PlayBack_ReleaseExportFormatList(void* player, Np_ExportFormatList& fmtlist);

SDK_EXPORT Np_Result_t PlayBack_GetSessionCurrentImage(void* player, void* session, Np_Frame& frame);

SDK_EXPORT Np_Result_t PlayBack_ReleaseSessionCurrentImage(void* player, Np_Frame& frame);

SDK_EXPORT
Np_Result_t  PlayBack_GetRawFrameDirectly(void* player, 
                                          Np_ID_Ext id, 
                                          Np_DateTime start, 
                                          Np_DateTime end,
                                          fnGetRawFrameHandle ecb, void* ectx,
                                          fnRawVideoHandle rcb, void* rctx,
                                          fnAudioHandle acb, void* actx);

SDK_EXPORT
Np_Result_t  PlayBack_StopGetRawFrameDirectly(void* player);

//////////////////////////////////////////////////////////////////
/*
 *  Utility functions 
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t Utility_SaveSnapShotImage(const wchar_t* filename, const char* buffer, int len, int width, 
                                                 int height);

SDK_EXPORT Np_Result_t Utility_AddImageWaterMark(const wchar_t* filename, Np_DateTime& time, const wchar_t* cameraname);

SDK_EXPORT Np_Result_t Utility_AddVideoWaterMark(const wchar_t* filename, Np_DateTime& start_time, Np_DateTime& end_time, 
                                                 const wchar_t* cameraname);

SDK_EXPORT Np_Result_t Utility_ScaleImage(unsigned char* p_dst, int dst_width, int dst_height, int dst_stride, 
                                          Np_PixelFormat dst_format, unsigned char* p_src, int src_width, int src_height,
                                          int src_stride, Np_PixelFormat src_format);

//////////////////////////////////////////////////////////////////
/*
 *  Metadata related
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t LiveView_SubscribeMetadata(void* handle, fnMetadataHandle mcb, void* mctx);

SDK_EXPORT Np_Result_t LiveView_UnsubscribeMetadata(void* handle);

//////////////////////////////////////////////////////////////////
/*
 *  Backup related
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t Backup_Initial(void* handle, Np_DateTime startTime, Np_DateTime endTime, Np_IDList backupIdList, 
                                      Np_SequencedRecordList &seqRecordList, bool includeEventLog, 
                                      bool includeCounterLog, bool includeSystemLog, bool includeMatadataRecord, 
                                      bool includeExeFiles, fnBackupHandle bcb, void* bctx);

SDK_EXPORT Np_Result_t Backup_Uninit(void* handle);

SDK_EXPORT Np_Result_t Backup_GetBackupFileItemList(void* handle, Np_BackupItemList& backupItemList);

SDK_EXPORT Np_Result_t Backup_ReleaseBackupFileItemList(void* handle, Np_BackupItemList& backupItemList);

SDK_EXPORT Np_Result_t Backup_SetBackupDestinationDir(void* handle, const wchar_t* dir);

SDK_EXPORT Np_Result_t Backup_Start(void* handle);

SDK_EXPORT Np_Result_t Backup_Pause(void* handle);

SDK_EXPORT Np_Result_t Backup_Resume(void* handle);

SDK_EXPORT Np_Result_t Backup_Abort(void* handle);

//////////////////////////////////////////////////////////////////
/*
 *  Lite handle related
 */
//////////////////////////////////////////////////////////////////
SDK_EXPORT Np_Result_t LiteHandle_UpdateDeviceAudioInfo(void* handle, const Np_ID& id);

SDK_EXPORT Np_Result_t LiteHandle_UpdateIODeviceInfo(void* handle);

SDK_EXPORT Np_Result_t LiteHandle_UpdateProfileInfo(void* handle);

SDK_EXPORT Np_Result_t LiteHandle_UpdateDevicePTZInfo(void* handle, const Np_ID& id);

SDK_EXPORT Np_Result_t LiteHandle_UpdateDeviceInfo(void* handle, const Np_ID_Ext& serverID, Np_IDList_Ext deviceIDList);

SDK_EXPORT Np_Result_t LiteHandle_UpdateAssociatedDeviceInfo(void* handle);

SDK_EXPORT Np_Result_t LiteHandle_UpdateEventCouplesList(void* handle);

SDK_EXPORT Np_Result_t LiteHandle_UpdateEventActionItemList(void* handle);

SDK_EXPORT Np_Result_t LiteHandle_UpdateAllSupportedEvent(void* handle);

SDK_EXPORT Np_Result_t LiteHandle_UpdateCustomEventList(void* handle);

SDK_EXPORT Np_Result_t LiteHandle_UpdateFailoverLog(void* handle);

SDK_EXPORT Np_Result_t LiteHandle_UpdateDeviceStatus(void* handle, const Np_ID_Ext& serverID, Np_IDList_Ext deviceIDList);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif


