#ifndef _NP_CLIENTDEFINES_H_
#define _NP_CLIENTDEFINES_H_

//Version
#define Np_SDK_VERSION "1.0.0.0"

//Result
#define Np_Result_OK 0
#define Np_Result_CLI_FAULT	1
#define Np_Result_SVR_FAULT	2
#define Np_Result_USER_ERROR 3
#define Np_Result_FAILED 4
#define Np_Result_NO_METHOD	5
#define Np_Result_NO_DATA 6
#define Np_Result_SOCKET_ERROR 7
#define Np_Result_INVALID_ARGUMENT 8
#define Np_Result_FATAL_ERROR 9 // handle should be destroyed
#define Np_Result_PRESET_FULL 10
#define Np_Result_UPDATE_DEVICE_FAILED 11


//Error code (player)
#define Np_ERROR_CODE_BASE                                      1000
#define	Np_ERROR_CONNECT_SUCCESS								Np_ERROR_CODE_BASE + 1
#define	Np_ERROR_CONNECT_ERROR									Np_ERROR_CODE_BASE + 2
#define	Np_ERROR_DISCONNECT_SUCCESS								Np_ERROR_CODE_BASE + 3
#define Np_ERROR_SESSION_NODATA									Np_ERROR_CODE_BASE + 4
#define Np_ERROR_SESSION_LOST									Np_ERROR_CODE_BASE + 5
#define Np_ERROR_EOF         									Np_ERROR_CODE_BASE + 6
#define Np_ERROR_FATAL_ERROR         							Np_ERROR_CODE_BASE + 7
#define Np_ERROR_FINAL_FRAME                                    Np_ERROR_CODE_BASE + 8
#define Np_ERROR_UNSUPPORTED_FORMAT								Np_ERROR_CODE_BASE + 9

#define Np_ERROR_CODE_NEXTFRAME_BASE       						1500
#define Np_ERROR_CODE_NEXTFRAME_START       					Np_ERROR_CODE_NEXTFRAME_BASE + 1
#define Np_ERROR_CODE_NEXTFRAME_SUCCESS       					Np_ERROR_CODE_NEXTFRAME_BASE + 2
#define Np_ERROR_CODE_NEXTFRAME_FAIL       						Np_ERROR_CODE_NEXTFRAME_BASE + 3

#define Np_ERROR_CODE_PREVIOUSFRAME_BASE      					1510
#define Np_ERROR_CODE_PREVIOUSFRAMEE_START      				Np_ERROR_CODE_PREVIOUSFRAME_BASE + 1
#define Np_ERROR_CODE_PREVIOUSFRAME_SUCCESS      				Np_ERROR_CODE_PREVIOUSFRAME_BASE + 2
#define Np_ERROR_CODE_PREVIOUSFRAME_FAIL      					Np_ERROR_CODE_PREVIOUSFRAME_BASE + 3

//Event code 
#define Np_EVENT_CODE_BASE                                        3000
#define Np_EVENT_USER_DEFINED_CODE_BASE                           8000
//------for source entity type: general server
#define Np_EVENT_RESOURCE_DEPLETED                                Np_EVENT_CODE_BASE + 0
#define Np_EVENT_NETWORK_CONGESTION                               Np_EVENT_CODE_BASE + 1
#define Np_EVENT_SYSTEM_HEALTH_UNUSUAL                            Np_EVENT_CODE_BASE + 2
#define Np_EVENT_RESOURCE_DEPLETED_STOP                           Np_EVENT_CODE_BASE + 50
#define Np_EVENT_NETWORK_CONGESTION_STOP                          Np_EVENT_CODE_BASE + 51
#define Np_EVENT_SYSTEM_HEALTH_UNUSUAL_STOP                       Np_EVENT_CODE_BASE + 52
//------for source entity type: general server
#define Np_EVENT_SERVER_OFFLINE									  Np_EVENT_CODE_BASE + 82
//------for source entity type: archiver server
#define Np_EVENT_DISK_SPACE_EXHAUSTED                             Np_EVENT_CODE_BASE + 100
#define Np_EVENT_DISK_ABNORMAL                                    Np_EVENT_CODE_BASE + 101
#define Np_EVENT_SERVER_AUTO_BACKUP_START						  Np_EVENT_CODE_BASE + 106
#define Np_EVENT_SERVER_AUTO_BACKUP_STOP						  Np_EVENT_CODE_BASE + 107
#define Np_EVENT_SERVER_AUTO_BACKUP_FAIL						  Np_EVENT_CODE_BASE + 108 //description: "Failed channel:XXXXXXXX"
#define Np_EVENT_RECORD_SETTING_CHANGE_TO_NONE                    Np_EVENT_CODE_BASE + 109
#define Np_EVENT_RECORD_SETTING_CHANGE_TO_SCHEDULE                Np_EVENT_CODE_BASE + 110
#define Np_EVENT_RECORD_SETTING_CHANGE_TO_ALWAYS                  Np_EVENT_CODE_BASE + 111
#define Np_EVENT_SERVER_CONNECTION_LOST						      Np_EVENT_CODE_BASE + 112
#define Np_EVENT_SERVER_CONNECTION_RECONNECT				      Np_EVENT_CODE_BASE + 113
#define Np_EVENT_DISK_SPACE_EXHAUSTED_STOP                        Np_EVENT_CODE_BASE + 150
//------for source entity type: sensor (MainConsole only)
#define Np_EVENT_GENERAL_MOTION_1                                 Np_EVENT_CODE_BASE + 180
#define Np_EVENT_GENERAL_MOTION_2                                 Np_EVENT_CODE_BASE + 181
#define Np_EVENT_GENERAL_MOTION_3                                 Np_EVENT_CODE_BASE + 182
#define Np_EVENT_GENERAL_MOTION_4                                 Np_EVENT_CODE_BASE + 183
#define Np_EVENT_GENERAL_MOTION_5                                 Np_EVENT_CODE_BASE + 184
#define Np_EVENT_FOREIGN_OBJECT                                   Np_EVENT_CODE_BASE + 185
#define Np_EVENT_MISSING_OBJECT                                   Np_EVENT_CODE_BASE + 186
#define Np_EVENT_FOCUS_LOST                                       Np_EVENT_CODE_BASE + 187
#define Np_EVENT_CAMERA_OCCLUSION                                 Np_EVENT_CODE_BASE + 188
#define Np_EVENT_GENERAL_MOTION_DEVICE                            Np_EVENT_CODE_BASE + 189
#define Np_EVENT_COUNTING                                         Np_EVENT_CODE_BASE + 190
#define Np_EVENT_COUNTING_STOP                                    Np_EVENT_CODE_BASE + 191
#define Np_EVENT_SMART_GUARD_ON_SCREEN_DISPLAY_START              Np_EVENT_CODE_BASE + 192 //description: "EventId:xxxxxxxx"
#define Np_EVENT_SMART_GUARD_ON_SCREEN_DISPLAY_STOP               Np_EVENT_CODE_BASE + 193 //description: "EventId:xxxxxxxx"
#define Np_EVENT_SMART_GUARD_SOUND_ALERT_START                    Np_EVENT_CODE_BASE + 194 //description: "EventId:xxxxxxxx"
#define Np_EVENT_SMART_GUARD_SOUND_ALERT_STOP                     Np_EVENT_CODE_BASE + 195 //description: "EventId:xxxxxxxx"
#define Np_EVENT_RECORD_STATUS_UPDATE                             Np_EVENT_CODE_BASE + 196 //description: "Mode:None/Always/Event/Motion/Boost"
#define Np_EVENT_TALK_BE_RESERVED                                 Np_EVENT_CODE_BASE + 197 //description: "User:%s""
#define Np_EVENT_EMAP_POPUP_AND_UPDATE_EVENT_START                Np_EVENT_CODE_BASE + 198 //description: "EventId:xxxxxxxx"
#define Np_EVENT_EMAP_POPUP_AND_UPDATE_EVENT_STOP                 Np_EVENT_CODE_BASE + 199 //description: "EventId:xxxxxxxx"
//------for source entity type: sensor
#define Np_EVENT_SIGNAL_LOST                                      Np_EVENT_CODE_BASE + 200
#define Np_EVENT_SIGNAL_RESTORE                                   Np_EVENT_CODE_BASE + 201
#define Np_EVENT_MOTION_START                                     Np_EVENT_CODE_BASE + 202
#define Np_EVENT_MOTION_STOP                                      Np_EVENT_CODE_BASE + 203
#define Np_EVENT_RECORD_ON_MOTION_START                           Np_EVENT_CODE_BASE + 204
#define Np_EVENT_RECORD_ON_MOTION_STOP                            Np_EVENT_CODE_BASE + 205
#define Np_EVENT_RECORD_ON_EVENT_START                            Np_EVENT_CODE_BASE + 206 //description: "EventId:xxxxxxxx".
#define Np_EVENT_RECORD_ON_EVENT_STOP                             Np_EVENT_CODE_BASE + 207 //description: "EventId:xxxxxxxx".
#define Np_EVENT_RECORD_ON_MANUAL_START                           Np_EVENT_CODE_BASE + 208 //description: "User:xxxxxxxx".
#define Np_EVENT_RECORD_ON_MANUAL_STOP                            Np_EVENT_CODE_BASE + 209 //description: "User:xxxxxxxx".
#define Np_EVENT_RECORD_ON_SCHEDULE_START                         Np_EVENT_CODE_BASE + 210 //description: "ScheduleId:xxxxxxxx".
#define Np_EVENT_RECORD_ON_SCHEDULE_STOP                          Np_EVENT_CODE_BASE + 211 //description: "ScheduleId:xxxxxxxx".
#define Np_EVENT_RECORD_START                                     Np_EVENT_CODE_BASE + 212 //description: "Mode:Always/Schedule/Manual" (for Titan).
#define Np_EVENT_RECORD_STOP                                      Np_EVENT_CODE_BASE + 213 //description: "Mode:Always/Schedule/Manual" (for Titan).
#define Np_EVENT_BACKUP_AUTO_START                                Np_EVENT_CODE_BASE + 214
#define Np_EVENT_BACKUP_AUTO_STOP                                 Np_EVENT_CODE_BASE + 215
#define Np_EVENT_BACKUP_AUTO_FAIL                                 Np_EVENT_CODE_BASE + 216 //description: "Reason:Unable to access backup server/Auto-backup is not completed/Cancel auto-backup because the user change settings"
#define Np_EVENT_BACKUP_MANUAL_START                              Np_EVENT_CODE_BASE + 217
#define Np_EVENT_BACKUP_MANUAL_STOP                               Np_EVENT_CODE_BASE + 218
#define Np_EVENT_BACKUP_MANUAL_FAIL                               Np_EVENT_CODE_BASE + 219 //description: TBD
#define Np_EVENT_EXPORT_START                                     Np_EVENT_CODE_BASE + 220
#define Np_EVENT_EXPORT_STOP                                      Np_EVENT_CODE_BASE + 221
#define Np_EVENT_EXPORT_FAIL                                      Np_EVENT_CODE_BASE + 222 //description: TBD
#define Np_EVENT_MANUAL_RECORD_MODE_START						  Np_EVENT_CODE_BASE + 223 //(for Titan).	
#define Np_EVENT_MANUAL_RECORD_MODE_STOP						  Np_EVENT_CODE_BASE + 224 //(for Titan).	
#define Np_EVENT_RECURRENT_LAUNCH          						  Np_EVENT_CODE_BASE + 225 //(for Solo).	
//------for source entity type: POS (MainConsole only)
#define Np_EVENT_TRANSACTION_START                                Np_EVENT_CODE_BASE + 300
#define Np_EVENT_TRANSACTION_STOP                                 Np_EVENT_CODE_BASE + 301
#define Np_EVENT_CASH_DRAWER_OPENED                               Np_EVENT_CODE_BASE + 302
/*POSTPONE*/ //#define NUUO_EVENT_CASH_DRAWER_CLOSED              Np_EVENT_CODE_BASE + 303
#define Np_EVENT_USER_DEFINE_RULE_1                               Np_EVENT_CODE_BASE + 304
#define Np_EVENT_USER_DEFINE_RULE_2                               Np_EVENT_CODE_BASE + 305
#define Np_EVENT_USER_DEFINE_RULE_3                               Np_EVENT_CODE_BASE + 306
#define Np_EVENT_USER_DEFINE_RULE_4                               Np_EVENT_CODE_BASE + 307
#define Np_EVENT_USER_DEFINE_RULE_5                               Np_EVENT_CODE_BASE + 308
#define Np_EVENT_USER_DEFINE_RULE_6                               Np_EVENT_CODE_BASE + 309
#define Np_EVENT_USER_DEFINE_RULE_7                               Np_EVENT_CODE_BASE + 310
#define Np_EVENT_USER_DEFINE_RULE_8                               Np_EVENT_CODE_BASE + 311
#define Np_EVENT_USER_DEFINE_RULE_9                               Np_EVENT_CODE_BASE + 312
#define Np_EVENT_USER_DEFINE_RULE_10                              Np_EVENT_CODE_BASE + 313
#define Np_EVENT_USER_DEFINE_RULE_11                              Np_EVENT_CODE_BASE + 314
#define Np_EVENT_USER_DEFINE_RULE_12                              Np_EVENT_CODE_BASE + 315
#define Np_EVENT_USER_DEFINE_RULE_13                              Np_EVENT_CODE_BASE + 316
#define Np_EVENT_USER_DEFINE_RULE_14                              Np_EVENT_CODE_BASE + 317
#define Np_EVENT_USER_DEFINE_RULE_15                              Np_EVENT_CODE_BASE + 318
#define Np_EVENT_USER_DEFINE_RULE_16                              Np_EVENT_CODE_BASE + 319
#define Np_EVENT_USER_DEFINE_RULE_17                              Np_EVENT_CODE_BASE + 320
#define Np_EVENT_USER_DEFINE_RULE_18                              Np_EVENT_CODE_BASE + 321
#define Np_EVENT_USER_DEFINE_RULE_19                              Np_EVENT_CODE_BASE + 322
#define Np_EVENT_USER_DEFINE_RULE_20                              Np_EVENT_CODE_BASE + 323
//------for source entity type: digital input
#define Np_EVENT_INPUT_CLOSED                                     Np_EVENT_CODE_BASE + 400
#define Np_EVENT_INPUT_OPENED                                     Np_EVENT_CODE_BASE + 401
//------for source entity type: output relay
#define Np_EVENT_OUTPUT_ON                                        Np_EVENT_CODE_BASE + 500
#define Np_EVENT_OUTPUT_OFF                                       Np_EVENT_CODE_BASE + 501
//------for source entity type: unit
#define Np_EVENT_CONNECTION_LOST                                  Np_EVENT_CODE_BASE + 600
#define Np_EVENT_CONNECTION_RESTORE                               Np_EVENT_CODE_BASE + 601
//------for source entity type: ptz motor
#define Np_EVENT_PTZMOTOR_PATROL_START                            Np_EVENT_CODE_BASE + 800 //description: "Applied patrol index:xxxx".
#define Np_EVENT_PTZMOTOR_PATROL_STOP                             Np_EVENT_CODE_BASE + 801
#define Np_EVENT_PTZMOTOR_PATROL_UPDATED                          Np_EVENT_CODE_BASE + 803
#define Np_EVENT_PTZMOTOR_LOCK                                    Np_EVENT_CODE_BASE + 804
#define Np_EVENT_PTZMOTOR_UNLOCK                                  Np_EVENT_CODE_BASE + 805
//------for source entity type: video device/io device/metadata source.
#define Np_EVENT_DEVICE_LIST_UPDATE                               Np_EVENT_CODE_BASE + 1000
//------for source entity type: camera/metadata source node. (Crystal only)
#define Np_EVENT_DEVICE_CHANGE_TO_BE_UNRETENTIVE                  Np_EVENT_CODE_BASE + 1101
//------for source entity type: archiver server (Mini&Mini2 only)
#define Np_EVENT_DAILY_REPORT                                     Np_EVENT_CODE_BASE + 2000
#define Np_UNABLE_ACCESS_FTP                                      Np_EVENT_CODE_BASE + 2001
#define Np_UNFINISHED_BACKUP                                      Np_EVENT_CODE_BASE + 2002
//------for source entity type: different entity type
#define Np_EVENT_PHYSICAL_DEVICE_TREELIST_UPDATED                 Np_EVENT_CODE_BASE + 3000
#define Np_EVENT_LOGICAL_DEVICE_TREELIST_UPDATED                  Np_EVENT_CODE_BASE + 3001
#define Np_EVENT_DEVICE_TREELIST_UPDATED                          Np_EVENT_CODE_BASE + 3002
#define Np_EVENT_CAROUSEL_LIST_UPDATED                            Np_EVENT_CODE_BASE + 3003
#define Np_EVENT_PANORAMA_LIST_UPDATED                            Np_EVENT_CODE_BASE + 3004
#define Np_EVENT_AOI_LIST_UPDATED                                 Np_EVENT_CODE_BASE + 3005
#define Np_EVENT_EMAP_TREELIST_UPDATED                            Np_EVENT_CODE_BASE + 3006
#define Np_EVENT_EMAP_IMAGE_ID_LIST_UPDATED                       Np_EVENT_CODE_BASE + 3007
#define Np_EVENT_EMAP_IMAGE_ICON_ID_LIST_UPDATED                  Np_EVENT_CODE_BASE + 3008
#define Np_EVENT_LAYOUT_LIST_UPDATED                              Np_EVENT_CODE_BASE + 3009
#define Np_EVENT_SHARE_VIEW_TREE_UPDATED                          Np_EVENT_CODE_BASE + 3010
#define Np_EVENT_PRIVATE_VIEW_TREE_UPDATED                        Np_EVENT_CODE_BASE + 3011
#define Np_EVENT_PRIVILEGE_ACCESS_RIGHT_UPDATED                   Np_EVENT_CODE_BASE + 3012
#define Np_EVENT_PASSWORD_CHANGED					              Np_EVENT_CODE_BASE + 3013
#define Np_EVENT_ACCOUNT_DELETE                   				  Np_EVENT_CODE_BASE + 3014
#define Np_EVENT_SERVICE_NETWORK_SETTING_CHANGED     			  Np_EVENT_CODE_BASE + 3015

//------for eventNotify Parameter Type 2:
//------for general failover server
#define Np_EVENT_FAILOVER_SERVER_START                            Np_EVENT_CODE_BASE + 4000
#define Np_EVENT_FAILOVER_SERVER_STOP                             Np_EVENT_CODE_BASE + 4001

#define Np_EVENT_CONFIG_UPDATEBASE										10000
#define Np_EVENT_CONFIG_UPDATE_setViewTree_private						Np_EVENT_CONFIG_UPDATEBASE + 3
#define Np_EVENT_CONFIG_UPDATE_setViewTree_public						Np_EVENT_CONFIG_UPDATEBASE + 4
#define Np_EVENT_CONFIG_UPDATE_setViewProfileTree_private				Np_EVENT_CONFIG_UPDATEBASE + 5
#define Np_EVENT_CONFIG_UPDATE_setViewProfileTree_public				Np_EVENT_CONFIG_UPDATEBASE + 6
#define Np_EVENT_CONFIG_UPDATE_setServerList							Np_EVENT_CONFIG_UPDATEBASE + 33
#define Np_EVENT_CONFIG_UPDATE_removeServerList							Np_EVENT_CONFIG_UPDATEBASE + 34
#define Np_EVENT_CONFIG_UPDATE_addUnitDevices							Np_EVENT_CONFIG_UPDATEBASE + 35
#define Np_EVENT_CONFIG_UPDATE_modifyUnitDevices						Np_EVENT_CONFIG_UPDATEBASE + 36
#define Np_EVENT_CONFIG_UPDATE_removeUnitDevices						Np_EVENT_CONFIG_UPDATEBASE + 37
#define Np_EVENT_CONFIG_UPDATE_setMetadataNodeTreeList					Np_EVENT_CONFIG_UPDATEBASE + 38
#define Np_EVENT_CONFIG_UPDATE_modifyDeviceBasicInfo					Np_EVENT_CONFIG_UPDATEBASE + 39
#define Np_EVENT_CONFIG_UPDATE_setCameraAssociateList					Np_EVENT_CONFIG_UPDATEBASE + 40
#define Np_EVENT_CONFIG_UPDATE_installMetadataSourceDevicePlugin		Np_EVENT_CONFIG_UPDATEBASE + 48
#define Np_EVENT_CONFIG_UPDATE_uninstallMetadataSourceDevicePlugin		Np_EVENT_CONFIG_UPDATEBASE + 49
#define Np_EVENT_CONFIG_UPDATE_setMetadataSourceNodeAdditionalParameter	Np_EVENT_CONFIG_UPDATEBASE + 52
#define Np_EVENT_CONFIG_UPDATE_setPanoramaList							Np_EVENT_CONFIG_UPDATEBASE + 53
#define Np_EVENT_CONFIG_UPDATE_setEMapTreeList							Np_EVENT_CONFIG_UPDATEBASE + 55
#define Np_EVENT_CONFIG_UPDATE_onLineLicenseOperate						Np_EVENT_CONFIG_UPDATEBASE + 61
#define Np_EVENT_CONFIG_UPDATE_offLineActivate							Np_EVENT_CONFIG_UPDATEBASE + 62
#define Np_EVENT_CONFIG_UPDATE_offLineTransfer							Np_EVENT_CONFIG_UPDATEBASE + 63

//------Source Device Type ID
//------for Info_EventQuery
#define Np_SOURCE_DEVICE_EMPTY 0
#define Np_SOURCE_DEVICE_SENSOR 1
#define Np_SOURCE_DEVICE_DIGITAL_INPUT 2
#define Np_SOURCE_DEVICE_SERVER 3

#endif 

