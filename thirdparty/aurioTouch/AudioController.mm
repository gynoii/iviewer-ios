#import "AudioController.h"

// Framework includes
#import <AVFoundation/AVAudioSession.h>
#import "CAStreamBasicDescription.h"

struct CallbackData {
    BOOL*                   muteAudio;
    BOOL*                   audioChainIsBeingReconstructed;
    int                     channel;
    int                     bytesPerSample;
    
    CallbackData(): muteAudio(NULL), audioChainIsBeingReconstructed(NULL) {}
} cd;

static OSStatus	recordcallback(void                         *inRefCon,
                               AudioUnitRenderActionFlags 	*ioActionFlags,
                               const AudioTimeStamp 		*inTimeStamp,
                               UInt32 						inBusNumber,
                               UInt32 						inNumberFrames,
                               AudioBufferList              *ioData)
{
    OSStatus status = noErr;
    
    AudioBufferList bufferlist;
    bufferlist.mNumberBuffers = 1;
    AudioBuffer buffer;
    buffer.mNumberChannels = 1;
    buffer.mDataByteSize = inNumberFrames * cd.bytesPerSample * cd.channel;
    buffer.mData = new char[buffer.mDataByteSize];
    bufferlist.mBuffers[0] = buffer;
    
    AudioController *audioProcessor = (AudioController*)inRefCon;
    if (audioProcessor.isStop == NO) {
        status = AudioUnitRender(audioProcessor.rioUnit, ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, &bufferlist);
    
        // process the bufferlist in the audio processor
        [audioProcessor processBuffer:&bufferlist];
    }
    delete [] (char *)buffer.mData;
    
    return status;
}

static OSStatus playbackCallback(void *inRefCon,
                                 AudioUnitRenderActionFlags *ioActionFlags,
                                 const AudioTimeStamp *inTimeStamp,
                                 UInt32 inBusNumber,
                                 UInt32 inNumberFrames,
                                 AudioBufferList *ioData) {
    OSStatus err = noErr;
    /*if (*cd.audioChainIsBeingReconstructed == NO)
    {
        // we are calling AudioUnitRender on the input bus of AURemoteIO
        // this will store the audio data captured by the microphone in ioData
        err = AudioUnitRender(cd.rioUnit, ioActionFlags, inTimeStamp, 1, inNumberFrames, ioData);
        
        // mute audio if needed
        if (*cd.muteAudio)
        {
            for (UInt32 i=0; i<ioData->mNumberBuffers; ++i)
                memset(ioData->mBuffers[i].mData, 0, ioData->mBuffers[i].mDataByteSize);
        }
    }
    */
    return err;
}

@interface AudioController()
- (void)setupAudioSession;
- (void)setupIOUnit;
- (void)setupAudioChain;
@end

@implementation AudioController

@synthesize rioUnit = _rioUnit;
@synthesize muteAudio = _muteAudio;
@synthesize audioHandleDelegate;
@synthesize isStop;

- (id)init
{
    if (self = [super init]) {
        _muteAudio = YES;
        isStop = YES;
        
        m_channels = 1;
        m_bitsPerSample = 32;
        m_sampleRate = 44100;
        m_sampleRequest = 1; //default
        
        audioBufferCondition = [[NSCondition alloc] init];
    }
    return self;
}

-(void)processBuffer: (AudioBufferList*) audioBufferList
{
    if (isStop)
        return;
    
    [audioBufferCondition lock];

    if ((audioDataLength + audioBufferList->mBuffers[0].mDataByteSize) < audioByteBufferSize)
    {
        memcpy(audioByteBuffer + audioDataLength,
               audioBufferList->mBuffers[0].mData,
               audioBufferList->mBuffers[0].mDataByteSize);
        audioDataLength += audioBufferList->mBuffers[0].mDataByteSize;
    }
    [audioBufferCondition unlock];
}

- (void)checkAndSendAudioPacket{
    if (isStop)
        return;
    
    if (audioDataLength >= audioPacketByteSize)
    {
        char *buf = new char[audioPacketByteSize];
        memcpy(buf, audioByteBuffer, audioPacketByteSize);
        
        [audioBufferCondition lock];
        memcpy(audioByteBuffer, audioByteBuffer+audioPacketByteSize, audioPacketByteSize);
        audioDataLength -= audioPacketByteSize;
        [audioBufferCondition unlock];
        
        if (audioHandleDelegate != nil && [audioHandleDelegate respondsToSelector:@selector(audioControllerHandleCallback:RawData:DataSize:)]) {
            [audioHandleDelegate audioControllerHandleCallback:self RawData:buf DataSize:audioPacketByteSize];
        }
        
        delete [] buf;
    }
}

- (void)handleInterruption:(NSNotification *)notification
{
    //try {
        UInt8 theInterruptionType = [[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] intValue];
        NSLog(@"Session interrupted > --- %s ---\n", theInterruptionType == AVAudioSessionInterruptionTypeBegan ? "Begin Interruption" : "End Interruption");
        
        if (theInterruptionType == AVAudioSessionInterruptionTypeBegan) {
            [self stopIOUnit];
        }
        
        if (theInterruptionType == AVAudioSessionInterruptionTypeEnded) {
            // make sure to activate the session
            NSError *error = nil;
            [[AVAudioSession sharedInstance] setActive:YES error:&error];
            if (nil != error) NSLog(@"AVAudioSession set active failed with error: %@", error);
            
            [self startIOUnit];
        }
    /*} catch (CAXException e) {
        char buf[256];
        fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
    }*/
}


- (void)handleRouteChange:(NSNotification *)notification
{
    UInt8 reasonValue = [[notification.userInfo valueForKey:AVAudioSessionRouteChangeReasonKey] intValue];
    AVAudioSessionRouteDescription *routeDescription = [notification.userInfo valueForKey:AVAudioSessionRouteChangePreviousRouteKey];
    
    NSLog(@"Route change:");
    switch (reasonValue) {
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            NSLog(@"     NewDeviceAvailable");
            break;
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            NSLog(@"     OldDeviceUnavailable");
            break;
        case AVAudioSessionRouteChangeReasonCategoryChange:
            NSLog(@"     CategoryChange");
            NSLog(@" New Category: %@", [[AVAudioSession sharedInstance] category]);
            break;
        case AVAudioSessionRouteChangeReasonOverride:
            NSLog(@"     Override");
            break;
        case AVAudioSessionRouteChangeReasonWakeFromSleep:
            NSLog(@"     WakeFromSleep");
            break;
        case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
            NSLog(@"     NoSuitableRouteForCategory");
            break;
        default:
            NSLog(@"     ReasonUnknown");
    }
    
    NSLog(@"Previous route:\n");
    NSLog(@"%@", routeDescription);
}

- (void)handleMediaServerReset:(NSNotification *)notification
{
    NSLog(@"Media server has reset");
    _audioChainIsBeingReconstructed = YES;
    
    usleep(25000); //wait here for some time to ensure that we don't delete these objects while they are being accessed elsewhere
    
    // rebuild the audio chain
    [self setupAudioChain];
    [self startIOUnit];
    
    _audioChainIsBeingReconstructed = NO;
}

- (void)setupAudioSession
{
    // Configure the audio session
    AVAudioSession *sessionInstance = [AVAudioSession sharedInstance];
        
    // we are going to play and record so we pick that category
    NSError *error = nil;
    [sessionInstance setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    
    // set the buffer duration to 5 ms
    NSTimeInterval bufferDuration = .005;
    [sessionInstance setPreferredIOBufferDuration:bufferDuration error:&error];
    
    // set the session's sample rate
    [sessionInstance setPreferredSampleRate:m_sampleRate error:&error];
    
    // add interruption handler
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleInterruption:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:sessionInstance];
        
    // we don't do anything special in the route change notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleRouteChange:)
                                                 name:AVAudioSessionRouteChangeNotification
                                               object:sessionInstance];
    
    // if media services are reset, we need to rebuild our audio chain
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMediaServerReset:)
                                                 name:AVAudioSessionMediaServicesWereResetNotification
                                               object:sessionInstance];
        
    // activate the audio session
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    
    return;
}


- (void)setupIOUnit
{
    // Create a new instance of AURemoteIO
    AudioComponentDescription desc;
    desc.componentType = kAudioUnitType_Output;
    desc.componentSubType = kAudioUnitSubType_RemoteIO;
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;
        
    AudioComponent comp = AudioComponentFindNext(NULL, &desc);
    AudioComponentInstanceNew(comp, &_rioUnit);
        
    //  Enable input and output on AURemoteIO
    //  Input is enabled on the input scope of the input element
    //  Output is enabled on the output scope of the output element
    UInt32 flag = 1;
    AudioUnitSetProperty(_rioUnit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Input, kInputBus, &flag, sizeof(flag));
    AudioUnitSetProperty(_rioUnit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Output, kOutputBus, &flag, sizeof(flag));
        
    // Explicitly set the input and output client formats
    // sample rate = 44100, num channels = 1, format = 32 bit floating point
    CAStreamBasicDescription ioFormat;
    switch (m_bitsPerSample) {
        case 16:
            ioFormat = CAStreamBasicDescription(m_sampleRate, m_channels, CAStreamBasicDescription::kPCMFormatInt16, true);
            break;
        case 32:
            ioFormat = CAStreamBasicDescription(m_sampleRate, m_channels, CAStreamBasicDescription::kPCMFormatFloat32, true);
            break;
        default:
            ioFormat = CAStreamBasicDescription(m_sampleRate, m_channels, CAStreamBasicDescription::kPCMFormatFloat32, true);
            break;
    }
    
    AudioUnitSetProperty(_rioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, kInputBus, &ioFormat, sizeof(ioFormat));
    AudioUnitSetProperty(_rioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, kOutputBus, &ioFormat, sizeof(ioFormat));
    
    flag = 0;
    AudioUnitSetProperty(_rioUnit, kAudioUnitProperty_ShouldAllocateBuffer, kAudioUnitScope_Output, kInputBus, &flag, sizeof(flag));
    
    // We need references to certain data in the record callback
    // This simple struct is used to hold that information
    cd.muteAudio = &_muteAudio;
    cd.audioChainIsBeingReconstructed = &_audioChainIsBeingReconstructed;
    cd.channel = m_channels;
    cd.bytesPerSample = m_bitsPerSample / 8;
    
    // Set the record callback on AURemoteIO
    AURenderCallbackStruct recordingCallback;
    recordingCallback.inputProc = recordcallback;
    recordingCallback.inputProcRefCon = self;
    AudioUnitSetProperty(_rioUnit,
                         kAudioOutputUnitProperty_SetInputCallback,
                         kAudioUnitScope_Global,
                         kInputBus,
                         &recordingCallback,
                         sizeof(recordingCallback));
    
    // Initialize the AURemoteIO instance
    AudioUnitInitialize(_rioUnit);
    
    return;
}

- (void)setupAudioChain
{
    audioPacketByteSize = m_sampleRequest * (m_bitsPerSample / 8) * m_channels;
    audioByteBufferSize = audioPacketByteSize * 2; // double buffer
    if (audioByteBuffer != NULL)
        delete [] audioByteBuffer;
    audioByteBuffer = new char[audioByteBufferSize];
    audioDataLength = 0;
    
    [self setupAudioSession];
    [self setupIOUnit];
    
    if (sendAudioTimer != nil && [sendAudioTimer isValid]) {
        [sendAudioTimer invalidate];
        sendAudioTimer = nil;
    }
    sendAudioTimer = [NSTimer scheduledTimerWithTimeInterval:.01
                                                      target:self
                                                    selector:@selector(checkAndSendAudioPacket)
                                                    userInfo:nil
                                                     repeats:YES];
}

- (void)resetupAudioChain:(Np_TalkAudioFormat *)format
{
    _muteAudio = YES;
    isStop = YES;
    
    m_channels = format->channels;
    m_bitsPerSample = format->bitsPerSample;
    m_sampleRate = format->sampleRate;
    m_sampleRequest = format->sampleRequest;
    
    [self setupAudioChain];
}

- (OSStatus)startIOUnit
{
    isStop = NO;
    OSStatus err = AudioOutputUnitStart(_rioUnit);
    if (err) NSLog(@"couldn't start AURemoteIO: %d", (int)err);
    return err;
}

- (OSStatus)stopIOUnit
{
    isStop = YES;
    OSStatus err = AudioOutputUnitStop(_rioUnit);
    if (err) NSLog(@"couldn't stop AURemoteIO: %d", (int)err);
    
    NSError *error = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    if(error) NSLog(@"AudioSession reset category error at %s:%d", __FILE__, __LINE__);
    
    [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeDefault error:&error];
    if(error) NSLog(@"AudioSession reset mode error at %s:%d", __FILE__, __LINE__);
    
    return err;
}

- (double)sessionSampleRate
{
    return [[AVAudioSession sharedInstance] sampleRate];
}

- (BOOL)audioChainIsBeingReconstructed
{
    return _audioChainIsBeingReconstructed;
}

- (void)dealloc
{
    AudioComponentInstanceDispose(_rioUnit);
    [audioBufferCondition release];
    delete [] audioByteBuffer;
    [super dealloc];
}

@end
