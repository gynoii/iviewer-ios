#!/bin/sh
# Copyright (C) Pierre d'Herbemont, 2010
# Copyright (C) Felix Paul Kühne, 2012-2013

set -e

PLATFORM=iphoneos
SDK=7.1
SDK_MIN=6.1
VERBOSE=no
CONFIGURATION="Distribution"
XCRUN_OPTION=""

TARGET_INDEX=0
TARGET_ARRAY=("iVIewer" "i-Security" "D-ViewCam" "ReadyNAS" "EZwatch" "CamGraba" "NevioRemote" "iCamSecure")

usage()
{
cat << EOF
usage: $0 [-s] [-v] [-k sdk] [-d] [-r] [-i]

OPTIONS
   -k       Specify which sdk to use (see 'xcodebuild -showsdks', current: ${SDK})
   -v       Be more verbose
   -s       Build for simulator
   -d       Enable Debug
   -r       Release Build (Ad-hoc)
   -i       Select target index
             0) iVIewer
             1) i-Security
             2) D-ViewCam
             3) ReadyNAS
             4) EZwatch
             5) CamGraba
             6) NevioRemote
             7) iCamSecure
EOF
}

spushd()
{
     pushd "$1" 2>&1> /dev/null
}

spopd()
{
     popd 2>&1> /dev/null
}

info()
{
     local green="\033[1;32m"
     local normal="\033[0m"
     echo "[${green}info${normal}] $1"
}

buildxcodeproj()
{
    local target="$2"

    info "Building $1 ($target, ${CONFIGURATION})"

    local extra=""
    if [ "$PLATFORM" = "Simulator" ]; then
        extra="ARCHS=i386"
    fi

    xcodebuild -project "$1.xcodeproj" \
               -target "$target" \
               -sdk $PLATFORM$SDK \
               -configuration ${CONFIGURATION} ${extra} \
               IPHONEOS_DEPLOYMENT_TARGET=${SDK_MIN} > ${out}

    xcrun -sdk $PLATFORM PackageApplication \
          $(pwd)/build/${CONFIGURATION}-${PLATFORM}/${target}.app \
          -o $(pwd)/build/${target}_${CONFIGURATION}.ipa ${XCRUN_OPTION}
}

while getopts "hvsdrki:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         v)
             VERBOSE=yes
             XCRUN_OPTION="-v"
             ;;
         s)
             PLATFORM=iphonesimulator
             ;;
         d)
             CONFIGURATION="Debug"
             ;;
         r)
             CONFIGURATION="Release"
             ;;
         k)
             SDK=$OPTARG
             ;;
         i)
             TARGET_INDEX=$OPTARG
             ;;
         ?)
             usage
             exit 1
             ;;
     esac
done
shift $(($OPTIND - 1))

out="/dev/null"
if [ "$VERBOSE" = "yes" ]; then
   out="/dev/stdout"
fi

if [ "x$1" != "x" ]; then
    usage
    exit 1
fi

# Get root dir
spushd .
aspen_root_dir=`pwd`
spopd

#
# Build time
#

info "Building"

currentValue=${TARGET_ARRAY[$TARGET_INDEX]}
# Build the Aspen Project now
buildxcodeproj "iViewer" $currentValue


info "Build completed"
