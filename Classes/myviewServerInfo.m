//
//  myviewCameraInfo.m
//  TabView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright 2010 com.debug. All rights reserved.
//

#import "myviewServerInfo.h"


@implementation MyViewServerInfo

@synthesize serverID;
@synthesize userName;
@synthesize cameraList;

- (id)init {
	if ((self = [super init])) {
		[self setServerID:@""];
        [self setUserName:@""];
        [self setCameraList:nil];
	}
	return self;
}

- (id)initWithDictionary:(NSDictionary *) dictionary {
	if ((self = [self init])) {
		[self setValuesForKeysWithDictionary:dictionary];
	}
	return self;
}

- (NSArray *) getKeys {
    NSArray * keys = [NSArray arrayWithObjects:
                      @"serverID",
                      @"userName",
                      @"cameraList",
                      nil];
    return keys;
}

- (id)getPropertyDic {
    return [self dictionaryWithValuesForKeys:[self getKeys]];
}

- (NSString *)description {
	return [[self getPropertyDic] description];
}

- (void)dealloc {
    if ([NSNull null] != (NSNull *)serverID) {
        [serverID release];
        serverID = nil;
    }
    if ([NSNull null] != (NSNull *)userName) {
        [userName release];
        userName = nil;
    }
    if ([NSNull null] != (NSNull *)cameraList) {
        [cameraList release];
        cameraList = nil;
    }
    
    [super dealloc];
}

@end
