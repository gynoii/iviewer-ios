//
//  PresetTableViewController.m
//  LiveView
//
//  Created by johnlinvc on 10/04/02.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "PresetTableViewController.h"


@implementation PresetTableViewController

@synthesize presetNameArray;
@synthesize presetIdArray;
@synthesize cam;


- (void)viewDidLoad {
    [super viewDidLoad];
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [presetNameArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	UILabel * text = cell.textLabel;
    [text setText:[presetNameArray objectAtIndex:indexPath.row]];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //send Preset Signal
	int iPresetNO =[[self.presetIdArray objectAtIndex:indexPath.row] intValue];
	[cam sendPresetAtNO:iPresetNO];
	//quit this view
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
	[presetIdArray release];
	[presetNameArray release];
    [super dealloc];
}


@end

