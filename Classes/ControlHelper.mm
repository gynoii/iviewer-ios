//
//  ControlHelper.m
//  objcKernal
//
//  Created by johnlinvc on 10/01/29.
//  Copyright 2010. All rights reserved.
//
#define LoggingPacket NO

#if FUNC_P2P_SAT_SUPPORT
#define NAT_HOST "host"		//Local LAN Connection, A host candidate represents the actual local transport address in the host.
#define NAT_TYPE1 "srflx"	//P2P Type1
#define NAT_TYPE2 "prflx"	//P2P Type2 (P2P Client Side)
#define NAT_RELAY "relay"	//Relay Connection, which represents the address allocated in RELAY server.
#define P2P_RELAY_DIALOG_TIMEOUT     570
#define P2P_RELAY_CONNECTION_TIMEOUT 600
#define P2P_SEVER_MTU_SIZE 1440
#endif

#define FORMAT(format, ...) [NSString stringWithFormat:(format), ##__VA_ARGS__]

#import "ControlHelper.h"
#import "LiveViewAppDelegate.h"
#import	"serverInfo.h"
#include "Utility.h"
#import "UIDevice+IdentifierAddition.h"
#import "SvUDIDTools.h"
#import "Reachability.h" // jerrylu, 2012/08/31
#import "LoadingPage.h"

#import "QueryRecordViewController.h" // Brosso

@implementation ControlHelper

//check
// For server info
@synthesize serverInfo;
@synthesize eventInfo;
@synthesize myViewServerInfo;

// for GetPreset
@synthesize presetNameArray;
@synthesize presetIdArray;

@synthesize serverManagerDelegate;
@synthesize presetDelegate;
@synthesize loadingDelegate; // jerrylu, 2012/09/12

@synthesize cameraCount; // jerrylu, 2012/08/16
@synthesize bAllCamSupportSecondProfile;
@synthesize cameraHelperArray;
@synthesize isRunning;
@synthesize isLogouting;
@synthesize isServerDisconnected;
@synthesize isGetPresent; // jerrylu, 2012/06/19
@synthesize playbackEnable; // jerrylu, 2012/05/24

#if FUNC_P2P_SAT_SUPPORT
@synthesize quota_control; // jerrylu, 2012/09/14
@synthesize isRelayMode;
#endif

//SDK
@synthesize m_hNuSDK;
@synthesize m_hNuSDKPlayback; // jerrylu, 2012/05/08, Support Playback Function
@synthesize m_vecServerList;
@synthesize m_vecExtCheckedDevice;
@synthesize m_vecCheckedDevice;
@synthesize m_vecDIOMasterDevice;
@synthesize m_vecCamSupportSecondProfile;

@synthesize m_event;
@synthesize m_event_ext;

#pragma mark -
#pragma mark refactored

void Eventhandler(Np_Event e, void* ctx)
{
    ControlHelper* pThis = (ControlHelper*) ctx;
    
    if (e.eventID == Np_EVENT_SERVER_CONNECTION_LOST ||
        e.eventID == Np_EVENT_PHYSICAL_DEVICE_TREELIST_UPDATED ||
        e.eventID == Np_EVENT_LOGICAL_DEVICE_TREELIST_UPDATED ||
        e.eventID == Np_EVENT_DEVICE_TREELIST_UPDATED ||
        e.eventID == Np_EVENT_PRIVILEGE_ACCESS_RIGHT_UPDATED ||
        e.eventID == Np_EVENT_PASSWORD_CHANGED ||
        e.eventID == Np_EVENT_ACCOUNT_DELETE ||
        e.eventID == Np_EVENT_SERVICE_NETWORK_SETTING_CHANGED ||
        e.eventID == Np_EVENT_SERVER_OFFLINE
        )
    {
        pThis.m_event = e;
        if (pThis.isLogouting) {
            return;
        }
        else if (e.eventID == Np_EVENT_DEVICE_TREELIST_UPDATED && (pThis.serverInfo.serverType == eServer_NVRmini || pThis.serverInfo.serverType == eServer_NVRsolo)) {
            // jerrylu, 2012/08/24
#if DEBUG_LOG
            NSLog(@"Np_EVENT_DEVICE_TREELIST_UPDATED on eServer_NVRmini or eServer_NVRsolo.");
#endif
            [pThis reloadAllDevices];
            return;
        }
        else {
            if (pThis.serverManagerDelegate != nil && [pThis.serverManagerDelegate respondsToSelector:@selector(controlHelperEventHandleFromServer:event:)]) {
                [pThis.serverManagerDelegate controlHelperEventHandleFromServer:pThis event:e];            
            }
        }
    }
    else if (e.eventID == Np_EVENT_TALK_BE_RESERVED) {
        [pThis handelReservedTalk:e];
    }
}

void EventHandlerExt(Np_Event_Ext e, void* ctx)
{
    ControlHelper* pThis = (ControlHelper*) ctx;
    
    if (e.eventID == Np_EVENT_CONFIG_UPDATE_setServerList ||
        e.eventID == Np_EVENT_CONFIG_UPDATE_removeServerList ||
        e.eventID == Np_EVENT_CONFIG_UPDATE_addUnitDevices ||
        e.eventID == Np_EVENT_CONFIG_UPDATE_modifyUnitDevices ||
        e.eventID == Np_EVENT_CONFIG_UPDATE_removeUnitDevices ||
        e.eventID == Np_EVENT_CONFIG_UPDATE_modifyDeviceBasicInfo ||
        e.eventID == Np_EVENT_CONFIG_UPDATE_setCameraAssociateList ||
        e.eventID == Np_EVENT_SERVER_OFFLINE ||
        e.eventID == Np_EVENT_SERVER_CONNECTION_LOST
        )
    {
        pThis.m_event_ext = e;
        if (pThis.isLogouting) {
            return;
        }
        else {
            if (pThis.serverManagerDelegate != nil && [pThis.serverManagerDelegate respondsToSelector:@selector(controlHelperEventHandleFromServer:event_ext:)]) {
                [pThis.serverManagerDelegate controlHelperEventHandleFromServer:pThis event_ext:e];
            }
        }
    }
}

#if FUNC_P2P_SAT_SUPPORT
// Get callee UID from SAT server
// "" for List All SAT Device
//const char device_name[4] = "";

//Assigned SAT Device Name and Search for Device
//const char device_name[8] = "UDP_WEB"; 
//DeviceEntry remote_entry;
//short int internal_port = 0;

// Get callee UID from SAT server
bool GetDeviceUID(IP2PSATRequest *req, NSString *device_name, unsigned short *liveview_port, unsigned short *playback_port, unsigned short *control_port, unsigned short *web_port) {
#if DEBUG_LOG
	NSLog(@"Get device list");
#endif
	const std::vector<DeviceEntry *> *list;
    NSInteger ret = req->GetDeviceEntryList(&list, NULL, NULL, [device_name UTF8String]);
    if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
        NSLog(@"%s Get device entry list fail.", __PRETTY_FUNCTION__);
        return false;
    }
    
    DeviceEntry *remote_entry;
    
    if (device_name && strlen([device_name UTF8String])!=0) {
        cout << "Searching for device: " << [device_name UTF8String] << endl;
        for (int i=0; i<list->size(); i++) {
            remote_entry = (*list)[i];
            if ( (*remote_entry).uid.compare([device_name UTF8String])==0) {
                //internal_port = list->at(i)->port;
                cout << "------------------------------------------" << endl;
                cout << "Device Found !!!" << endl;
                cout << "  device_name:   " << (*remote_entry).device_name.c_str() << endl;
                cout << "  url_prefix:    " << (*remote_entry).url_prefix.c_str() << endl;
                cout << "  ip:            " << (*remote_entry).ip.c_str() << endl;
                cout << "  port:          " << (*remote_entry).port << endl;
                cout << "  url_path:      " << (*remote_entry).url_path << endl;
                cout << "  mac_address:   " << (*remote_entry).mac_address.c_str() << endl;
                cout << "  uid:           " << (*remote_entry).uid.c_str() << endl;
                cout << "  purpose:       " << (*remote_entry).purpose.c_str() << endl;
                cout << "------------------------------------------" << endl;
                
                // jerrylu, 2013/01/15, use "purpose" first. if the value is empty, try "device_name"
                if ((*remote_entry).purpose.compare("") != 0) {
                    if ((*remote_entry).purpose.compare("LIVEVIEW") == 0) {
                        *liveview_port = (*remote_entry).port;
                    }
                    else if ((*remote_entry).purpose.compare("PLAYBACK") == 0) {
                        *playback_port = (*remote_entry).port;
                    }
                    else if ((*remote_entry).purpose.compare("CONTROL") == 0) {
                        *control_port = (*remote_entry).port;
                    }
                    else if ((*remote_entry).purpose.compare("WEB") == 0) {
                        *web_port = (*remote_entry).port;
                    }
                }
                else {
                    if ((*remote_entry).device_name.compare("LIVEVIEW") == 0) {
                        *liveview_port = (*remote_entry).port;
                    }
                    else if ((*remote_entry).device_name.compare("PLAYBACK") == 0) {
                        *playback_port = (*remote_entry).port;
                    }
                    else if ((*remote_entry).device_name.compare("CONTROL") == 0) {
                        *control_port = (*remote_entry).port;
                    }
                    else if ((*remote_entry).device_name.compare("WEB") == 0) {
                        *web_port = (*remote_entry).port;
                    }
                }

                
                if (*liveview_port != 0 && *playback_port != 0 && *control_port != 0 && *web_port != 0) {
                    return true;
                }
			}
		}
		if (*liveview_port == 0 || *control_port == 0) {
            // not found
            cout << "Device Not Found !" << endl;
            return false;
        }
	}
	else {
        cout << "------------------------------------------" << endl;
		cout << "Available devices:" << endl;
		for (int i=0; i<list->size(); i++) {
			DeviceEntry *d = (*list)[i];
			cout << "  Device " << (i+1) << ". ";
			cout << d->device_name << ',';
			cout << d->url_prefix << ',';
			cout << d->ip << ',';
			cout << d->port << ',';
			cout << d->url_path << ',';
			cout << d->mac_address << ',';
			cout << d->uid << ',';
			cout << d->enc << endl;
		}
        cout << "------------------------------------------" << endl;
	}
    return true;
}
#endif

- (void)setlogoutstatus {
#if 1
    [m_Condition lock];
    
    self.isLogouting = YES;
    
    [m_Condition unlock];
#else
    NSCondition *ticketCondition = [[NSCondition alloc] init];
    [ticketCondition lock];
    
    self.isLogouting = YES;
    
    [ticketCondition unlock];
    [ticketCondition release];
    ticketCondition = nil;
#endif
}

- (id)init {
	if ((self = [super init])) {
		isRunning = NO;
        isServerDisconnected = NO;
        self.isLogouting = NO;
        m_event.eventID = Np_EVENT_CODE_BASE;
        m_event_ext.eventID = Np_ERROR_CODE_BASE;
        
        //create other thread to check connection, avoid to block main thread
        m_checkConnectionThread = [[NSThread alloc] initWithTarget:self selector:@selector(checkConnection) object:NULL];
        
        // jerrylu, 2012/05/11
        //create other thread to check playback connection, avoid to block main thread
        m_checkPlaybackConnectionThread = [[NSThread alloc] initWithTarget:self selector:@selector(checkPbConnection) object:NULL];
        
        m_Condition = [[NSCondition alloc] init]; // jerrylu, 2012/07/12
        
#if FUNC_P2P_SAT_SUPPORT
        // jerrylu, 2012/09/13
        quota_control = NO;
#endif
	}
    
    return self;
}

- (void)dealloc {
    [m_Condition release]; // jerrylu, 2012/07/12
    m_Condition = nil;
    [eventInfo release];
    eventInfo = nil;
    
    [super dealloc];
}

- (void)_login {
#if DEBUG_LOG
    NSLog(@"_login start");
#endif
    Np_Result_t ret = Np_Result_OK;
    
#if FUNC_P2P_SAT_SUPPORT
    // p2p
    if ([serverInfo connectionType] == eP2PConnection) {
        loginEznuuoFailedTime = 0;
        
        BOOL ret = NO;
        int max_retry = 4;
        while (ret == NO && loginEznuuoFailedTime < max_retry)
        {
            NSLog(@"time %d\n", loginEznuuoFailedTime);

            ret = [self loginEznuuo];
            if (ret == YES)
            {
                if ([self getIndexPage] == YES)
                {
                    serverMtuSize = [self querryMTUSize];
                    if (serverMtuSize == -1) //NVRSolo v1.0
                        serverMtuSize = 1500;
                    break;
                }
                else
                {
                    serverMtuSize = [self querryMTUSize];
                    if (serverMtuSize == -1) { //NVRSolo v1.0
                        serverMtuSize = 1500;
                        
                        if (loginEznuuoFailedTime == max_retry-1) {
                            ret = YES; // if it couldn't get index page and MTU, still try to login
                            break;
                        }
                        else {
                            ret = NO;
                        }
                    }
                    else if (serverMtuSize != P2P_SEVER_MTU_SIZE) {
                        [self setMTUSize:P2P_SEVER_MTU_SIZE];
                        ret = NO;
                        [NSThread sleepForTimeInterval:30];
                    }
                    else
                    {
                        // the MTU size is 1440, but still error
                        ret = NO;
                        break;
                    }
                }
            }
            
            loginEznuuoFailedTime++;
        }
        
        if (ret == NO) {
            if (p2pErrorCode != eP2pConnectIdError)
                p2pErrorCode = eP2pConnectFailed;
            [self performSelectorOnMainThread:@selector(_LoginFail) withObject:NULL waitUntilDone:YES];
            [NSThread exit];
            return;
        }
        
        if (loadingDelegate == nil) {
            [self performSelectorOnMainThread:@selector(_LoginFail) withObject:NULL waitUntilDone:YES];
            [NSThread exit];
            return;
        }
        
        if (![self loginMainConsole]) {
            if (p2pErrorCode != eP2pConnectAccountError)
                p2pErrorCode = eP2pConnectFailed;
            [self performSelectorOnMainThread:@selector(_LoginFail) withObject:NULL waitUntilDone:YES];
            return;
        }
    }
    else {
#endif
    if (![self loginByAllKindOfServer]) {
        [self performSelectorOnMainThread:@selector(_LoginFail) withObject:NULL waitUntilDone:YES];
        return;
    }
#if FUNC_P2P_SAT_SUPPORT
    }
#endif
    if ([[NSThread currentThread] isCancelled]) {
        [NSThread exit];
        return;
    }
    
    if (m_hNuSDK && !m_hLVPlayer)
    {
        ret |= LiveView_CreatePlayer(
                                    m_hNuSDK,
                                    &m_hLVPlayer
                                    );
    }
    if (m_hNuSDK && m_hLVPlayer)
        ret |= LiveView_SetAudioOff(m_hLVPlayer);
    
    if ([[NSThread currentThread] isCancelled]) {
        [NSThread exit];
        return;
    }
    
    if (ret != Np_Result_OK) {
        [self performSelectorOnMainThread:@selector(_LoginFail) withObject:NULL waitUntilDone:YES];
        [NSThread exit];
        return;
    }
    
    if (serverInfo.serverType == eServer_Crystal) {
        [self performSelectorOnMainThread:@selector(_LoginSucs) withObject:NULL waitUntilDone:YES];
#if DEBUG_LOG
        NSLog(@"_login end");
#endif
        return;
    }
    
    [self update_CameraDeviceAndSensorProfileProfileInfo];
    
    if ([[NSThread currentThread] isCancelled]) {
        [NSThread exit];
        return;
    }
    
    [self update_IODeviceInfo];
    
    if ([[NSThread currentThread] isCancelled]) {
        [NSThread exit];
        return;
    }
    
    [self performSelectorOnMainThread:@selector(_LoginSucs) withObject:NULL waitUntilDone:YES];
#if DEBUG_LOG
    NSLog(@"_login end");
#endif
}

- (void)update_CameraDeviceAndSensorProfileProfileInfo {
    cameraCount = 0;
    m_vecCheckedDevice.clear();
    bAllCamSupportSecondProfile = TRUE;
    m_vecCamSupportSecondProfile.clear();
    
    std::vector<Np_DeviceGroup>::iterator iterDeviceGroup = m_stDeviceList.PhysicalGroup.begin();
    for (;iterDeviceGroup!=m_stDeviceList.PhysicalGroup.end();iterDeviceGroup++)
    {
        if (serverInfo.serverType == eServer_Titan)
        {
            NSString *group_description = StringWToNSString(iterDeviceGroup->description);
            if ([group_description isEqualToString:@"Main Server"]) {
                [group_description release];
            }
            else {
                [group_description release];
                continue;
            }
        }
        
        std::vector<Np_SubDevice>::iterator iterSubDevice;
        std::vector<Np_Device>::iterator iterDevice;
        
        for (iterDevice = iterDeviceGroup->Camera.begin();iterDevice!=iterDeviceGroup->Camera.end();iterDevice++)
        {
            BOOL bFoundOriginal = false;
            BOOL bFoundLow = false;
            BOOL bFoundMinmum = false;
            Np_SensorProfileList profilelist;
            if (Np_Result_OK == Info_GetSensorProfileList(m_hNuSDK, iterDevice->SensorDevices[0].ID, profilelist))
            {
                for (int j=0; j<profilelist.size(); j++)
                {
                    if (profilelist[j].profile == kProfileLow) {
                        bFoundLow = TRUE;
                    }
                    else if (profilelist[j].profile == kProfileMinimum) {
                        bFoundMinmum = TRUE;
                    }
                    else {
                        if (serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo) {
                            if (![StringWToNSString(profilelist[j].codec) isEqualToString:@""])
                                bFoundOriginal = TRUE;
                        }
                        else
                            bFoundOriginal = TRUE;
                    }
                }
                
                if (!bFoundOriginal) // No codec means empty camera
                    continue;
                
                if (!bFoundLow && !bFoundMinmum) {
                    bAllCamSupportSecondProfile = FALSE;
                    m_vecCamSupportSecondProfile.push_back(FALSE);
                }
                else {
                    m_vecCamSupportSecondProfile.push_back(TRUE);
                }
            }
            else
            {
                bAllCamSupportSecondProfile = FALSE;
                m_vecCamSupportSecondProfile.push_back(FALSE);
            }
            
            m_vecCheckedDevice.push_back(*iterDevice);
            cameraCount++;
        }
    }
}

- (void)update_IODeviceInfo {
    IODeviceCount = 0;
    m_vecDIOMasterDevice.clear();
    m_vecDODevice.clear();
    m_vecDOStatus.clear();
    m_vecDIDevice.clear();
    m_vecDIStatus.clear();
    
    std::vector<Np_DeviceGroup>::iterator iterDeviceGroup = m_stDeviceList.PhysicalGroup.begin();
    for (;iterDeviceGroup!=m_stDeviceList.PhysicalGroup.end();iterDeviceGroup++)
    {
        if (serverInfo.serverType == eServer_Titan)
        {
            NSString *group_description = StringWToNSString(iterDeviceGroup->description);
            if ([group_description isEqualToString:@"Main Server"]) {
                [group_description release];
            }
            else {
                [group_description release];
                continue;
            }
        }
        
        std::vector<Np_SubDevice>::iterator iterSubDevice;
        std::vector<Np_Device>::iterator iterDevice;
        
        for (iterDevice = iterDeviceGroup->IOBox.begin();iterDevice!=iterDeviceGroup->IOBox.end();iterDevice++)
        {
            m_vecDIOMasterDevice.push_back(*iterDevice);
            
            for (iterSubDevice = iterDevice->DIDevices.begin(); iterSubDevice != iterDevice->DIDevices.end(); iterSubDevice++) {
                m_vecDIDevice.push_back(*iterSubDevice);
                m_vecDIStatus.push_back(FALSE); // jerrylu, 2012/07/15
            }
            for (iterSubDevice = iterDevice->DODevices.begin(); iterSubDevice != iterDevice->DODevices.end(); iterSubDevice++) {
                m_vecDODevice.push_back(*iterSubDevice);
                m_vecDOStatus.push_back(FALSE); // jerrylu, 2012/07/15
            }
            IODeviceCount++;
        }
        
        for (iterDevice = iterDeviceGroup->Camera.begin();iterDevice!=iterDeviceGroup->Camera.end();iterDevice++)
        {
            for (iterSubDevice = iterDevice->DIDevices.begin(); iterSubDevice != iterDevice->DIDevices.end(); iterSubDevice++) {
                m_vecDIDevice.push_back(*iterSubDevice);
                m_vecDIStatus.push_back(FALSE); // jerrylu, 2012/07/15
            }
            for (iterSubDevice = iterDevice->DODevices.begin(); iterSubDevice != iterDevice->DODevices.end(); iterSubDevice++) {
                m_vecDODevice.push_back(*iterSubDevice);
                m_vecDOStatus.push_back(FALSE); // jerrylu, 2012/07/15
            }
            
            if (!iterDevice->DIDevices.empty() || !iterDevice->DODevices.empty()) {
                IODeviceCount++;
            }
        }
    }
    
    if ((NSNull *)IOOutputPinNameArray != [NSNull null])
        [IOOutputPinNameArray release];
    
    IOOutputPinNameArray = [[NSMutableArray alloc] init];
    for (int i=0; i<m_vecDODevice.size(); i++) {
        NSString *dodevice_name = StringWToNSString(m_vecDODevice[i].name);
        [IOOutputPinNameArray addObject:dodevice_name];
        [dodevice_name release];
    }
}

- (void)update_AudioAndPtzDeviceInfoOnCam:(int) index {
    if (index >= [cameraHelperArray count]) {
#if DEBUG_LOG
        NSLog(@"[%s] Bad Index", __FUNCTION__);
#endif
		return;
	}
    
    if (m_hNuSDK == nil)
        return;
    
    if (serverInfo.serverType == eServer_MainConsole ||
        serverInfo.serverType == eServer_NVRmini ||
        serverInfo.serverType == eServer_NVRsolo)
    {
        LiteHandle_UpdateDeviceAudioInfo(m_hNuSDK, m_vecCheckedDevice[index].ID);
        LiteHandle_UpdateDevicePTZInfo(m_hNuSDK, m_vecCheckedDevice[index].ID);
        m_stDeviceList.PhysicalGroup.clear();
        m_stDeviceList.LogicalGroup.clear();
        Info_GetDeviceList(m_hNuSDK, m_stDeviceList);
        
        std::vector<Np_DeviceGroup>::iterator iterDeviceGroup = m_stDeviceList.PhysicalGroup.begin();
        for (;iterDeviceGroup!=m_stDeviceList.PhysicalGroup.end();iterDeviceGroup++)
        {
            if (serverInfo.serverType == eServer_Titan)
            {
                NSString *group_description = StringWToNSString(iterDeviceGroup->description);
                if ([group_description isEqualToString:@"Main Server"]) {
                    [group_description release];
                }
                else {
                    [group_description release];
                    continue;
                }
            }
            
            std::vector<Np_Device>::iterator iterDevice;
            for (iterDevice = iterDeviceGroup->Camera.begin();iterDevice!=iterDeviceGroup->Camera.end();iterDevice++)
            {
                if (iterDevice->ID.centralID == m_vecCheckedDevice[index].ID.centralID &&
                    iterDevice->ID.localID == m_vecCheckedDevice[index].ID.localID) {
                    m_vecCheckedDevice[index] = *iterDevice;
                    break;
                }
            }
        }
    }
}

- (void)create_FakeCameraList {
    cameraCount = 0;
    bAllCamSupportSecondProfile = TRUE;
    m_vecCamSupportSecondProfile.clear();
    
    if (serverInfo.serverType == eServer_Crystal) {
        m_vecExtCheckedDevice.clear();
        
        m_vecExtDODevice.clear();
        m_vecDOStatus.clear();
        m_vecExtDIDevice.clear();
        m_vecDIStatus.clear();
        
        for (MyViewCameraInfo *myViewCam in myViewServerInfo.cameraList) {
            Np_SubDevice_Ext fake_device;
            memset(&fake_device, 0, sizeof(Np_SubDevice_Ext));
            
            char *endstr;
            fake_device.ID.centralID = strtoull([myViewCam.camCentralID UTF8String], &endstr, 10);
            fake_device.ID.localID = strtoull([myViewCam.camLocalID UTF8String], &endstr, 10);
            std::wstring devicenamestring = NSStringToStringW(myViewCam.cameraName);
            fake_device.name = new wchar_t[devicenamestring.size() + 1];
            memset(fake_device.name, 0, sizeof(wchar_t)*(devicenamestring.size() + 1));
            memcpy(fake_device.name, devicenamestring.c_str(), sizeof(wchar_t)*devicenamestring.size());
            
            m_vecExtCheckedDevice.push_back(fake_device);
            m_vecCamSupportSecondProfile.push_back(TRUE);
            
            cameraCount++;
        }
    }
    else {
        m_vecCheckedDevice.clear();
        
        m_vecDODevice.clear();
        m_vecDOStatus.clear();
        m_vecDIDevice.clear();
        m_vecDIStatus.clear();
        
        for (MyViewCameraInfo *myViewCam in myViewServerInfo.cameraList) {
            Np_Device fake_device;
            fake_device.ID.centralID = [myViewCam.camCentralID intValue];
            fake_device.ID.localID = [myViewCam.camLocalID intValue];
            fake_device.name = NSStringToStringW(myViewCam.cameraName);

            Np_SubDevice fake_subdevice;
            fake_subdevice.ID.centralID = fake_device.ID.centralID;
            fake_subdevice.ID.localID = fake_device.ID.localID;
            fake_device.SensorDevices.push_back(fake_subdevice);
            
            m_vecCheckedDevice.push_back(fake_device);
            m_vecCamSupportSecondProfile.push_back(TRUE);
            
            cameraCount++;
        }
    }
}

- (void)_loginMyView:(NSMutableArray *)serverArray {
#if DEBUG_LOG
    NSLog(@"_login start");
#endif
    
    if (serverArray.count == 0) {
        [self performSelectorOnMainThread:@selector(_LoginFail) withObject:NULL waitUntilDone:YES];
        [NSThread exit];
        return;
    }
    
    BOOL ret = NO;
    for (int i=0; i < [serverArray count]; i++) {
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
        
        serverInfo = [serverArray objectAtIndex:i];

        if (serverInfo.serverType >= eServer_Max) {
            continue;
        }
        
        ret = NO;
        
#if FUNC_P2P_SAT_SUPPORT
        // p2p
        if ([serverInfo connectionType] == eP2PConnection) {
            if ([self loginEznuuo] == NO) {
                ret = NO;
                continue;
            }
            if (![self loginMainConsole]) {
                if ([[NSThread currentThread] isCancelled]) {
                    [NSThread exit];
                    return;
                }
                //Release SAT P2P Tunnel
                if(p_tunnel != NULL)
                {
                    p_tunnel->Stop();
                    delete p_tunnel;
                    p_tunnel = NULL;
                }
                ret = NO;
                continue;
            }
            else {
                ret = YES;
                break;
            }
        }
        else {
#endif
        if (![self loginByAllKindOfServer]) {
            ret = NO;
            continue;
        }
        
        // login success
        ret = YES;
        break;
#if FUNC_P2P_SAT_SUPPORT
        }
#endif
    }
    
    if ([[NSThread currentThread] isCancelled]) {
        [NSThread exit];
        return;
    }
    
    if (!ret) {
        [self performSelectorOnMainThread:@selector(_LoginFail) withObject:NULL waitUntilDone:YES];
        
        [NSThread exit];
        return;
    }
    
    if ([[NSThread currentThread] isCancelled]) {
        [NSThread exit];
        return;
    }
    if (serverInfo.serverType == eServer_Crystal) {
        for (int i = 0; i < m_recordingServerList.size; i++)
        {
            NSString *cid = [NSString stringWithFormat:@"%llu", m_recordingServerList.items[i].ID.centralID];
            NSString *lid = [NSString stringWithFormat:@"%llu", m_recordingServerList.items[i].ID.localID];
            
            for (MyViewCameraInfo *myViewCam in myViewServerInfo.cameraList) {
                if ([myViewCam.rsCentralID isEqualToString:cid] &&
                    [myViewCam.rsLocalID isEqualToString:lid]) {
                    Np_IDList_Ext idList;
                    memset(&idList, 0, sizeof(idList));
                    LiteHandle_UpdateDeviceInfo(m_hNuSDK, m_recordingServerList.items[i].ID, idList);
                    break;
                }
            }
        }
    }
    
    [self updateDeviceByMyView];
    
    if ([[NSThread currentThread] isCancelled]) {
        [NSThread exit];
        return;
    }
    
    [self performSelectorOnMainThread:@selector(_LoginSucs) withObject:NULL waitUntilDone:YES];
#if DEBUG_LOG
    NSLog(@"_login end");
#endif
}

- (void)_loginByEvent:(NSMutableArray *)serverArray {
#if DEBUG_LOG
    NSLog(@"_login by event start");
#endif
    BOOL ret = NO;
    
    for (int j=0; j < [serverArray count]; j++) {
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
        
        if (m_hNuSDKPlayback) {
            Destroy_Handle(m_hNuSDKPlayback);
            m_hNuSDKPlayback = nil;
        }
        if (m_hLVPlayer) {
            LiveView_DestroyPlayer(m_hLVPlayer);
            m_hLVPlayer = nil;
        }
        if (m_hNuSDK) {
            Destroy_Handle(m_hNuSDK);
            m_hNuSDK = nil;
        }
        
        serverInfo = [serverArray objectAtIndex:j];

#if FUNC_P2P_SAT_SUPPORT
        // p2p
        if ([serverInfo connectionType] == eP2PConnection) {
            if ([self loginEznuuo] == NO) {
                ret = NO;
                continue;
            }
        }
        else {
#endif
        
        Reachability *reachability = [Reachability reachabilityWithHostName:serverInfo.serverIP];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable) {
            NSLog(@"serverInfo.serverIP is NotReachable");
            ret = NO;
            continue;
        }
        else if (internetStatus == ReachableViaWWAN) {
            // if connection is wan 3G, don't connect the local ip range 192.168.x.x
            if ([serverInfo.serverIP hasPrefix:@"10."] ||
                [serverInfo.serverIP hasPrefix:@"172."] ||
                [serverInfo.serverIP hasPrefix:@"192.168"]) {
                ret = NO;
                continue;
            }
        }
#if FUNC_P2P_SAT_SUPPORT
        }
#endif

        if ([self loginMainConsole]) {
            
            if ([[NSThread currentThread] isCancelled]) {
                [NSThread exit];
                return;
            }
            
            if (m_hNuSDK && !m_hLVPlayer)
            {
                if(LiveView_CreatePlayer(m_hNuSDK, &m_hLVPlayer) == Np_Result_OK)
                {
                    if (m_hNuSDK && m_hLVPlayer)
                    {
                        LiveView_SetAudioOff(m_hLVPlayer);
                    }
                }
                else {
                    ret = NO;
                    continue;
                }
            }
            
            if (!m_hNuSDKPlayback) {
                Np_Result_t np_ret;
#if FUNC_P2P_SAT_SUPPORT // 2012/09/19
                if ([serverInfo connectionType] == eP2PConnection)
                    np_ret = Create_Handle(
                                         &m_hNuSDKPlayback,
                                         kMainConsolePlayback,
                                         NSStringToStringW([serverInfo userName_P2P]),
                                         NSStringToStringW([serverInfo userPassword_P2P]),
                                         NSStringToStringW([p2pServerInfo serverIP]),
                                         [[p2pServerInfo serverPlaybackPort] intValue]
                                         );
                else
#endif
                np_ret = Create_Handle(
                                    &m_hNuSDKPlayback,
                                    kMainConsolePlayback,
                                    NSStringToStringW([serverInfo userName]),
                                    NSStringToStringW([serverInfo userPassword]),
                                    NSStringToStringW([serverInfo serverIP]),
                                    [[serverInfo serverPlaybackPort] intValue]
                                       );
                
                if (np_ret != Np_Result_OK) {
                    ret = NO;
                    continue;
                }
            }
            
            [self update_CameraDeviceAndSensorProfileProfileInfo];
            [self update_IODeviceInfo];
            
            // Match cam id
            int cam_index = -1;
            for (int i = 0; i < cameraCount; i++) {
                if (m_vecCheckedDevice[i].ID.localID == [eventInfo.cameraID intValue])
                {
                    cam_index = i;
                    break;
                }
            }
            if (cam_index < 0) {
                ret = NO;
                continue;
            }
            
            // Login and Playback connection is ready
            ret = YES;
            break;
        }
    }
    
    if ([[NSThread currentThread] isCancelled]) {
        [NSThread exit];
        return;
    }
    
    if (!ret) {
        [self performSelectorOnMainThread:@selector(_LoginFail) withObject:NULL waitUntilDone:YES];
        
        [NSThread exit];
        return;
    }
    
    IOOutputPinNameArray = [[NSMutableArray alloc] init];
    for (int i=0; i<m_vecDODevice.size(); i++) {
        NSString *didevice_name = StringWToNSString(m_vecDODevice[i].name);
        [IOOutputPinNameArray addObject:didevice_name];
        [didevice_name release];
    }
    
    [self performSelectorOnMainThread:@selector(_LoginByEventSucs) withObject:NULL waitUntilDone:YES];
#if DEBUG_LOG
    NSLog(@"_login end");
#endif
}

- (BOOL)loginByAllKindOfServer {
    BOOL login_status = NO;

    // Use the previous type to login
    switch (serverInfo.serverType) {
        case eServer_MainConsole:
        case eServer_NVRmini:
        case eServer_NVRsolo:
            login_status = [self loginMainConsole];
            break;
        case eServer_Titan:
        case eServer_Crystal:
            login_status = [self loginCrystal];
            break;
        default:
            break;
    }
    
    if (!login_status) {
        if (serverInfo.serverType != eServer_MainConsole &&
            serverInfo.serverType != eServer_NVRmini &&
            serverInfo.serverType != eServer_NVRsolo) {
            login_status = [self loginMainConsole];
        }
        
        
        if (!login_status && serverInfo.serverType != eServer_Crystal) {
            login_status = [self loginCrystal];
        }
        
        if (!login_status && serverInfo.serverType != eServer_Titan) {
            login_status = [self loginTitan];
        }
        
        if (!login_status) {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)loginTitan
{
#if DEBUG_LOG
    NSLog(@"loginTitan start");
#endif
    Np_Result_t ret = Np_Result_OK;    
    
    if (!m_hNuSDK)    
    {
#if FUNC_P2P_SAT_SUPPORT // 2012/09/19
        if ([serverInfo connectionType] == eP2PConnection)
            ret |= Create_Handle(
                                 &m_hNuSDK,
                                 kMainConsoleLiveview,
                                 NSStringToStringW([serverInfo userName_P2P]),
                                 NSStringToStringW([serverInfo userPassword_P2P]),
                                 NSStringToStringW([p2pServerInfo serverIP]),
                                 [[p2pServerInfo serverPort] intValue]
                                 );
        else
#endif
        ret |= Create_Handle(
                            &m_hNuSDK,
                            kCorporate,
                            NSStringToStringW([serverInfo userName]),
                            NSStringToStringW([serverInfo userPassword]),
                            NSStringToStringW([serverInfo serverIP]),
                            [[serverInfo serverPort] intValue]
                            );
    }
    if (m_hNuSDK) {
        m_stDeviceList.PhysicalGroup.clear();
        m_stDeviceList.LogicalGroup.clear();
        ret |= Info_GetDeviceList(m_hNuSDK, m_stDeviceList);
        ret |= Event_Subscribe(m_hNuSDK, &m_hEventSession, Eventhandler, self);
    }

    if (ret != Np_Result_OK) {
        if (m_hNuSDK) {
            Destroy_Handle(m_hNuSDK);
            m_hNuSDK = NULL;
        }
#if 1 // jerrylu, 2012/05/08, Support Playback Function
        m_hNuSDKPlayback = NULL;
#endif
#if DEBUG_LOG
        NSLog(@"loginTitan failed");
#endif
        return FALSE;
    }
    
#if 1 // jerrylu, 2012/05/08, Support Playback Function
    m_hNuSDKPlayback = m_hNuSDK;
    m_pbDeviceList = m_stDeviceList;
    
    if (m_hNuSDKPlayback && !m_hPBPlayer) {
        ret |= PlayBack_CreatePlayer(m_hNuSDKPlayback, &m_hPBPlayer);
    }
    
    playbackEnable = TRUE;
#endif
    
#if DEBUG_LOG
    NSLog(@"loginTitan end");
#endif
    
    if ([serverInfo.serverID isEqualToString:@""]) {
        [serverInfo setServerID:[NSString stringWithFormat:@"Titan_%@_%@_%@", serverInfo.serverIP, serverInfo.serverPort, serverInfo.userName]];
    }
    serverInfo.serverType = eServer_Titan;
    [self saveServerInfo];
    
    return TRUE;
}

- (BOOL)loginCrystal
{
#if DEBUG_LOG
    NSLog(@"loginCrystal start");
#endif
    Np_Result_t ret = Np_Result_OK;
    
    if (!m_hNuSDK)
    {
        ret |= Create_Lite_Handle(&m_hNuSDK,
                                kCrystal,
                                NSStringToStringW([serverInfo userName]).c_str(),
                                NSStringToStringW([serverInfo userPassword]).c_str(),
                                NSStringToStringW([serverInfo serverIP]).c_str(),
                                [[serverInfo serverPort] intValue]
                                );
    }
    if (m_hNuSDK) {
        Info_GetRecordingServerList(m_hNuSDK,
                                    m_recordingServerList);
        m_vecServerList.clear();
        for (int i = 0; i < m_recordingServerList.size; i++) {
            Np_Server& item = m_recordingServerList.items[i];
            m_vecServerList.push_back(item);
        }
        
        ret |= Event_SubscribeExt(m_hNuSDK, &m_hEventSession, EventHandlerExt, self);
    }
    
    if (ret != Np_Result_OK) {
        if (m_hNuSDK) {
            Destroy_Handle(m_hNuSDK);
            m_hNuSDK = NULL;
            m_hNuSDKPlayback = NULL;
        }
#if DEBUG_LOG
        NSLog(@"loginCrystal failed");
#endif
        return FALSE;
    }
    
#if FUNC_PLAYBACK_SUPPORT
    m_hNuSDKPlayback = m_hNuSDK;
    if (m_hNuSDKPlayback && !m_hPBPlayer) {
        ret |= PlayBack_CreatePlayer(m_hNuSDKPlayback, &m_hPBPlayer);
    }
    playbackEnable = TRUE;
#endif
    
#if DEBUG_LOG
    NSLog(@"loginCrystal end");
#endif
    
    if ([serverInfo.serverID isEqualToString:@""]) {
        [serverInfo setServerID:[NSString stringWithFormat:@"Crystal_%@_%@_%@", serverInfo.serverIP, serverInfo.serverPort, serverInfo.userName]];
    }
    serverInfo.serverType = eServer_Crystal;
    [self saveServerInfo];
    
    return TRUE;
}

- (BOOL)loginMainConsole
{
#if DEBUG_LOG
    NSLog(@"loginMainConsole start");
#endif
    Np_Result_t ret = Np_Result_OK;
    
    if (!m_hNuSDK) {
#if FUNC_P2P_SAT_SUPPORT
        if ([serverInfo connectionType] == eP2PConnection) {
            ret |= Create_Lite_Handle(
                                 &m_hNuSDK,
                                 kMainConsoleLiveview,
                                 NSStringToStringW([serverInfo userName_P2P]).c_str(),
                                 NSStringToStringW([serverInfo userPassword_P2P]).c_str(),
                                 NSStringToStringW([p2pServerInfo serverIP]).c_str(),
                                 [[p2pServerInfo serverPort] intValue]
                                 );
            if (ret == Np_Result_USER_ERROR || ret == Np_Result_INVALID_ARGUMENT)
                p2pErrorCode = eP2pConnectAccountError;
        }
        else
#endif
        ret |= Create_Lite_Handle(
                            &m_hNuSDK,
                            kMainConsoleLiveview,
                            NSStringToStringW([serverInfo userName]).c_str(),
                            NSStringToStringW([serverInfo userPassword]).c_str(),
                            NSStringToStringW([serverInfo serverIP]).c_str(),
                            [[serverInfo serverPort] intValue]
                            );
    }
    
    if (m_hNuSDK) {
        ret |= LiteHandle_UpdateProfileInfo(m_hNuSDK);
        ret |= LiteHandle_UpdateIODeviceInfo(m_hNuSDK);
        m_stDeviceList.PhysicalGroup.clear();
        m_stDeviceList.LogicalGroup.clear();
        ret |= Info_GetDeviceList(m_hNuSDK, m_stDeviceList);
        ret |= Event_Subscribe(m_hNuSDK, &m_hEventSession, Eventhandler, self);
    }
    
    if (ret != Np_Result_OK) {
        if (m_hNuSDK) {
            Destroy_Handle(m_hNuSDK);
            m_hNuSDK = NULL;
        }
#if DEBUG_LOG
        NSLog(@"loginMainConsole failed");
#endif
        return FALSE;
    }
#if DEBUG_LOG
    NSLog(@"loginMainConsole end");
#endif
    
    NSString *servertypestr;
    Np_ServerInfo sinfo;
    Np_ServerInfo_MainConsole sinfomainconsole;
    sinfo.serverInfoMC = &sinfomainconsole;
    Info_GetServerInfo(m_hNuSDK, sinfo);
    if (sinfo.serverInfoMC != NULL) {
        servertypestr = StringWToNSString(sinfo.serverInfoMC->serverType);
        if ([servertypestr isEqualToString:@"NVRmini"]
#if CUSTOMER_FOR_DLINK // jerrylu, 2012/12/26, fix bug11032
            || [servertypestr isEqualToString:@"DLink_DNS-722-4"] || [servertypestr isEqualToString:@"DLink_DNS-726-4"]
#endif
            ) {
#if FUNC_P2P_SAT_SUPPORT
            if ([serverInfo connectionType] == eP2PConnection)
                serverInfo.serverType = eServer_NVRsolo;
            else
#endif
            serverInfo.serverType = eServer_NVRmini;
        }
        else {
            serverInfo.serverType = eServer_MainConsole;
        }
        [servertypestr release];
        
        wchar_t *serveridw;
        Info_GetServerIdentification(m_hNuSDK, &serveridw);
        NSString *serverid = StringWToNSString(serveridw);
#if DEBUG_LOG
        NSLog(@"Server ID: %@", serverid);
#endif
        [self.serverInfo setServerID:serverid];
        [serverid release];
        [self saveServerInfo];
        Info_ReleaseServerIdentification(m_hNuSDK, &serveridw);
    }
    else {
        serverInfo.serverType = eServer_MainConsole;
        
        if ([serverInfo.serverID isEqualToString:@""]) {
            if ([serverInfo connectionType] == eP2PConnection) {
                [serverInfo setServerID:[NSString stringWithFormat:@"MCF_%@_%@", serverInfo.serverID_P2P, serverInfo.userName_P2P]];
            }
            else {
                [serverInfo setServerID:[NSString stringWithFormat:@"MCF_%@_%@_%@", serverInfo.serverIP, serverInfo.serverPort, serverInfo.userName]];
            }
        }
    }
    Info_ReleaseServerInfo(m_hNuSDK, sinfo);
    
    return TRUE;
}

- (void)_LoginSucs {
#if DEBUG_LOG
    NSLog(@"_LoginSucs");
#endif
    [m_Condition lock];
    isRunning = YES;
    [m_Condition unlock];

#if FUNC_PUSH_NOTIFICATION_SUPPORT
    if (serverInfo.serverType == eServer_MainConsole ||
        serverInfo.serverType == eServer_NVRmini ||
        serverInfo.serverType == eServer_NVRsolo) {
        if (serverInfo.serverPushNotificationEnable == kPushNotificationEnable) {
            [self pnRegisterToServer];
        }
    }
    else {
        serverInfo.serverPushNotificationEnable = kPushNotificationDisable;
    }
#endif
#if FUNC_P2P_SAT_SUPPORT
    if (serverInfo.serverType == eServer_NVRsolo) {
        if (loadingDelegate != nil) {
            if ([loadingDelegate respondsToSelector:@selector(controlHelperSetLoadingProgressState:)]) {
                [loadingDelegate controlHelperSetLoadingProgressState:eProgressSuccess];
            }
        }
        
        p2pStartTime = [[NSDate alloc] init];
    }
#endif
    if (serverManagerDelegate != nil) {
        if ([serverManagerDelegate respondsToSelector:@selector(controlHelperConnectToServer:cameraCount:)]) {
            [serverManagerDelegate controlHelperConnectToServer:self cameraCount:cameraCount];
        }
    }
    else { // jerrylu, 2012/12/18, logout by self, fix bug10804
        [self stop];
    }
}

- (void)_LoginByEventSucs {
    [m_Condition lock];
    isRunning = YES;
    [m_Condition unlock];
    
    int cam_index = -1;
    
    // Match cam id
    for (int i = 0; i < cameraCount; i++) {
        if (m_vecCheckedDevice[i].ID.localID == [eventInfo.cameraID intValue])
        {
            cam_index = i;
            break;
        }
    }

    [self connectToAllCam];
        
    // Login successfully and enable the Event View toolbar
    if (serverManagerDelegate != nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperStartEventViewByIndex:index:)]) {
        [serverManagerDelegate controlHelperStartEventViewByIndex:self index:cam_index];
    }
}

- (void)_LoginFail {
#if DEBUG_LOG
    NSLog(@"_LoginFail");
#endif
    
    [m_Condition lock];
    isRunning = NO;
    [m_Condition unlock];
    
#if FUNC_P2P_SAT_SUPPORT
    //Release SAT P2P Tunnel
    if(p_tunnel != NULL)
    {
        p_tunnel->Stop();
        delete p_tunnel;
        p_tunnel = NULL;
    }

    if (loadingDelegate != nil) {
        if (p2pErrorCode == eP2pConnectSuccess) {
            if (serverManagerDelegate != nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperCantConnectToServer:)]) {
                [serverManagerDelegate controlHelperCantConnectToServer:self];
            }
        }
        else {
            if ([loadingDelegate respondsToSelector:@selector(controlHelperP2PConnectionFailed:)])
                [loadingDelegate controlHelperP2PConnectionFailed:p2pErrorCode];
        }
    }
    else {
        if (serverManagerDelegate != nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperCantConnectToServer:)]) {
            [serverManagerDelegate controlHelperCantConnectToServer:self];
        }
    }
#else
    if (serverManagerDelegate != nil)
        [serverManagerDelegate controlHelperCantConnectToServer:self];
#endif
}

#if FUNC_P2P_SAT_SUPPORT
- (NSString *)getEznuuoUID {
#if CUSTOMER_FOR_NUUO
    NSString *url = [[NSString alloc] initWithFormat:@"https://www.eznuuo.com/SIDRequest.php?SID=%s", [[serverInfo serverID_P2P] UTF8String]];
#else
    NSString *url = [[NSString alloc] initWithFormat:@"https://www.findnvr.com/SIDRequest.php?SID=%s", [[serverInfo serverID_P2P] UTF8String]];
#endif
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"GET"];
    //[urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //[urlRequest setHTTPBody:postData];
    [url release];
    
    NSURLResponse *response;
    NSError *error;
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    NSString *replyString;
    if (serverReply != nil) {
        replyString = [[[NSString alloc] initWithBytes:[serverReply bytes] length:[serverReply length] encoding: NSASCIIStringEncoding] autorelease];
    }
    else {
        replyString = [[[NSString alloc] initWithFormat:@"Unconnection"] autorelease];
    }
    NSLog(@"reply string is :%@",replyString);
    
    return replyString;
}

- (BOOL)loginEznuuo {
    p2pErrorCode = eP2pConnectSuccess;
    
    NSString *replyString = [self getEznuuoUID];
    if ([replyString isEqualToString:@"NOT FOUND"] || [replyString isEqualToString:@"SQL ERROR"]) {
        p2pErrorCode = eP2pConnectIdError;
        return NO;
    }
    else if ([replyString isEqualToString:@"Unconnection"]) {
        p2pErrorCode = eP2pConnectFailed;
        return NO;
    }
    
    if (p2pServerInfo == nil) {
        p2pServerInfo = [[ServerInfo alloc] init];
    }
    [p2pServerInfo setServerID_P2P:replyString];
    
    unsigned short local_lvport = 128;
    unsigned short local_pbport = 128;
    unsigned short local_ctrlport = 128;
    unsigned short local_webport = 128;
    unsigned short remote_lvport = 0;
    unsigned short remote_pbport = 0;
    unsigned short remote_ctrlport = 0;
    unsigned short remote_webport = 0;
    std::string local_addr = "127.0.0.1";
    
    int ret;
    char signal_server[1024] = "";
	unsigned short signal_server_port = 0;
    char signal_uid[1024] = "";

    // Default SAT Username/Password for DEMO
    // Please change to your own SAT Username/Password
    char sat_username[10] = "eznuuo";
    char sat_password[15] = "PX9b2TK85JMr";
    
    if (loadingDelegate != nil) {
        if ([loadingDelegate respondsToSelector:@selector(controlHelperSetLoadingProgressState:)]) {
            [loadingDelegate controlHelperSetLoadingProgressState:eProgressStart];
        }
    }

    //Set SAT P2P Debug Message Level (1 to 10)
    SAT_SDK_LIB_Debug::SetDebugLevel((DEBUG_LEVEL) 10);
    
    NSString* resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString* pathEnd =  [resourcePath stringByAppendingFormat:@"/license_N01IU"];
    const char *pathEndStr = [pathEnd UTF8String];
	IP2PLicense *license = P2PFactory::CreateLicense(pathEndStr);
    
    //SAT Server Request, check license file
    IP2PSATRequest *p2preq = P2PFactory::CreateSATRequest(sat_username, sat_password, license);
    
    if (p2preq == nil) {
        p2pErrorCode = eP2pConnectFailed;
        return NO;
    }
    
    if(!GetDeviceUID(p2preq, p2pServerInfo.serverID_P2P, &remote_lvport, &remote_pbport, &remote_ctrlport, &remote_webport)) {
        p2pErrorCode = eP2pConnectFailed;
        return NO;
    }
    
    //Set Caller UID
    //Please replace "000000000000" with "IPhone Wireless MAC Address"
    p2preq->GetUid(signal_uid,"000000000000");
    
    //Get Caller Signal Server Address/Port from License File
    license->GetSignalServer(signal_server,&signal_server_port);
    
    if (p_tunnel != NULL) {
        p_tunnel->Stop();
        delete p_tunnel;
        p_tunnel = NULL;
    }
    
    //Create SAT P2P Tunnel
    p_tunnel = P2PFactory::CreateTunnelCaller();
    
    //Set Caller Signal Server Address/Port
	p_tunnel->SetSignalServer(signal_server,signal_server_port);
    
    //Start SAT P2P Tunnel
	ret = p_tunnel->Start();
	if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
        if (p_tunnel != NULL) {
            p_tunnel->Stop();
            delete p_tunnel;
            p_tunnel = NULL;
        }
        local_lvport = 1;
        NSLog(@"local port = %u, return value = %i",local_lvport, ret);
        p2pErrorCode = eP2pConnectFailed;
        return NO;
	}
    
    if (loadingDelegate != nil) {
        if ([loadingDelegate respondsToSelector:@selector(controlHelperSetLoadingProgressState:)]) {
            [loadingDelegate controlHelperSetLoadingProgressState:eProgressAuthenticate];
        }
    }
    
    //Authenticate to Signal Server, with local Caller UID
	ret = p_tunnel->Authenticate(signal_uid);
	if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
        if (p_tunnel != NULL) {
            p_tunnel->Stop();
            delete p_tunnel;
            p_tunnel = NULL;
        }
        local_lvport = 2;
        NSLog(@"local port = %u, return value = %i",local_lvport, ret);
        p2pErrorCode = eP2pConnectFailed;
        return NO;
	}
    
    if (loadingDelegate != nil) {
        if ([loadingDelegate respondsToSelector:@selector(controlHelperSetLoadingProgressState:)]) {
            [loadingDelegate controlHelperSetLoadingProgressState:eProgressConnect];
        }
    }
    
    //Set Callee UID that Local Caller want to connecte to
    const char *CallerUIDStr = [[p2pServerInfo serverID_P2P] UTF8String];
    ret = p_tunnel->Connect(CallerUIDStr, 0x0007);
	if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS) {
        if (p_tunnel != NULL) {
            p_tunnel->Stop();
            delete p_tunnel;
            p_tunnel = NULL;
        }
        local_lvport = 5;
        NSLog(@"local port = %u, return value = %i",local_lvport, ret);
        p2pErrorCode = eP2pConnectFailed;
        return NO;
	}
    
    if (loadingDelegate != nil) {
        if ([loadingDelegate respondsToSelector:@selector(controlHelperSetLoadingProgressState:)]) {
            [loadingDelegate controlHelperSetLoadingProgressState:eProgressConnectTunnel];
        }
    }
    
    //Connect to Callee with UID CallerUIDStr, to HTTP Web Service (TCP/PORT=80)
    //UID and PORT can set manually
    //or Get from SAT Server by function "bool GetDeviceUID(IP2PSATRequest *req)"
    int times = 0; // jerrylu, 2013/02/01
    while (YES)
    {
    	ret = p_tunnel->ConnectTunnel(CallerUIDStr, IPPROTO_TCP, "127.0.0.1", remote_lvport, &local_lvport, &local_addr);
        //ret |= p_tunnel->ConnectTunnel(CallerUIDStr, IPPROTO_TCP, "127.0.0.1", remote_ctrlport, &local_ctrlport);
        ret |= p_tunnel->ConnectTunnel(CallerUIDStr, IPPROTO_TCP, "127.0.0.1", remote_webport, &local_webport, &local_addr);
        
        if (ret != SAT_SDK_LIB_RET_NULL_TRY_AGAIN_LATER) {
            break;
        }
        
        times++;
        
        // Timeout.
        if (times > 20) {
            break;
        }
        sleep(1);
    }
    
    if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS)
    {
        if (p_tunnel != NULL) {
            //SAT P2P Connection Failed !
            p_tunnel->Stop();
            delete p_tunnel;
            p_tunnel = NULL;
        }
		local_lvport = 6;
        NSLog(@"local port = %u, return value = %i",local_lvport, ret);
        p2pErrorCode = eP2pConnectFailed;
        return NO;
	}
    else {
        // jerrylu, 2012/09/19
        if (p2pServerInfo == nil) {
            p2pServerInfo = [[ServerInfo alloc] init];
        }
        //[p2pServerInfo setServerIP:@"127.0.0.1"];
        [p2pServerInfo setServerIP:[NSString stringWithFormat:@"%s", local_addr.c_str()]];
        NSString *lvport = [[NSString alloc] initWithFormat:@"%u",local_lvport];
        [p2pServerInfo setServerPort:lvport];
        [lvport release];

        controlport = local_ctrlport;
        webport = local_webport;
    }
    
    if (remote_pbport != 0 && remote_lvport != remote_pbport) {
        int times = 0;
        while (YES)
        {
            ret = p_tunnel->ConnectTunnel(CallerUIDStr, IPPROTO_TCP, "127.0.0.1", remote_pbport, &local_pbport, &local_addr);
            
            if (ret != SAT_SDK_LIB_RET_NULL_TRY_AGAIN_LATER) {
                break;
            }
            
            times++;
            
            // Timeout.
            if (times > 20) {
                break;
            }
            sleep(1);
        }
        
        if (ret != SAT_SDK_LIB_RET_NULL_SUCCESS)
        {
            // jerrylu, 2012/09/19
            [p2pServerInfo setServerPlaybackPort:@"0"];
        }
        else
        {
            // jerrylu, 2012/09/19
            NSString *pbport = [[NSString alloc] initWithFormat:@"%u",local_pbport];
            [p2pServerInfo setServerPlaybackPort:pbport];
            [pbport release];
        }
    }
    else if (remote_pbport == remote_lvport)
        local_pbport = local_lvport;
    
    if (loadingDelegate != nil) {
        if ([loadingDelegate respondsToSelector:@selector(controlHelperSetLoadingProgressState:)]) {
            [loadingDelegate controlHelperSetLoadingProgressState:eProgressConnectToServer];
        }
    }
    
    // Get NAT Type
    std::string laddr;
    std::string raddr;
    p_tunnel->GetNegotiationResult([p2pServerInfo.serverID_P2P UTF8String], &nattype, &laddr, &raddr);
    NSLog(@"nattype = %s, laddr = %s, laddr = %s",nattype.c_str(), laddr.c_str(), raddr.c_str());
    local_ip = [[NSString alloc] initWithFormat:@"%s", laddr.c_str()];
    
    if (strcmp(nattype.c_str(), NAT_RELAY) == 0)
        isRelayMode = YES;
    else
        isRelayMode = NO;
    
    return TRUE;
}
#endif

#if FUNC_P2P_SAT_SUPPORT
#pragma mark - 
#pragma mark P2P Quota handle methods
- (int)P2PUsedTime {
	int time = 0;
	if(strcmp(nattype.c_str(), NAT_RELAY) == 0)
		time = 5;
	else
		time = 0; //Default 0 sec.
    
	return time;
}

- (void)quotaControl {
    relayTimeout = 0;
    while(![[NSThread currentThread] isCancelled]) {
        [NSThread sleepForTimeInterval:0.1];
        
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
        
        relayTimeout++;
		int progressTime = relayTimeout*0.1;
        if (progressTime >= P2P_RELAY_DIALOG_TIMEOUT && progressTime <= P2P_RELAY_CONNECTION_TIMEOUT) {
#if CUSTOMER_FOR_NUUO
            NSString * msg = [NSString stringWithFormat:NSLocalizedString(@"The connection of ezNUUO will be stopped in %d seconds. If you want to continue this session, please click the Reconnect button", nil), P2P_RELAY_CONNECTION_TIMEOUT - progressTime];
#else
            NSString * msg = [NSString stringWithFormat:NSLocalizedString(@"The connection of findNVR will be stopped in %d seconds. If you want to continue this session, please click the Reconnect button", nil), P2P_RELAY_CONNECTION_TIMEOUT - progressTime];
#endif
            if (self.serverManagerDelegate != nil) {
                if ([self.serverManagerDelegate respondsToSelector:@selector(controlHelperShowP2PControlDialog:msg:)]) {
                    [self.serverManagerDelegate controlHelperShowP2PControlDialog:self msg:msg];
                }
            }
        }
        else if (progressTime > P2P_RELAY_CONNECTION_TIMEOUT) {
            if (self.serverManagerDelegate != nil) {
                if ([self.serverManagerDelegate respondsToSelector:@selector(controlHelperQuotaTimeout:)]) {
                    [self.serverManagerDelegate controlHelperQuotaTimeout:self];
                }
            }
            break;
        }
	}
    
    [NSThread exit];
    return;
}

- (void)quotaReset {
    relayTimeout = 0;
}

- (int)querryMTUSize {
    int mtu_size = -1;
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://%s:%d/cgi-bin/cgi_system?cmd=eznuuo&act=query", [[p2pServerInfo serverIP] UTF8String], webport];
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"GET"];
    [url release];
    
    NSURLResponse *response;
    NSError *error;
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    NSString *replyString;
    if (serverReply != nil && error == nil) {
        replyString = [[[NSString alloc] initWithBytes:[serverReply bytes] length:[serverReply length] encoding: NSASCIIStringEncoding] autorelease];
        NSArray * replyArray = [replyString componentsSeparatedByString:@"="];
        mtu_size = [[replyArray lastObject] intValue];
    }
    NSLog(@"mtu_size is :%d",mtu_size);
    
    return mtu_size;
}

- (void)setMTUSize:(int) mtu_size {
    NSString *url = [[NSString alloc] initWithFormat:@"http://%s:%d/cgi-bin/cgi_system?cmd=eznuuo&act=mtu&mtu_size=%d", [[p2pServerInfo serverIP] UTF8String], webport, mtu_size];
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"GET"];
    [url release];
    
    NSURLResponse *response;
    NSError *error;
    [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    return;
}

- (void)uploadRawDatatoServer {
    NSTimeInterval p2pUsedTime = -[p2pStartTime timeIntervalSinceNow];
    
    if (p2pUsedTime < 10)
        return;
    
    NSString *connecttype;
    if (isRelayMode)
        connecttype = [[NSString alloc] initWithFormat:@"Relay"];
    else
        connecttype = [[NSString alloc] initWithFormat:@"Stun"];
    
#if CUSTOMER_FOR_NUUO
    NSString *url = [[NSString alloc] initWithFormat:@"https://www.eznuuo.com/log_connect_info.php?where=ezNUUO&serverid=%@&con_time=%d&type=%@&device=iOS&mtu=%d&ipaddr=%@", [serverInfo serverID_P2P], (int)p2pUsedTime, connecttype, serverMtuSize, local_ip];
#else
    NSString *url = [[NSString alloc] initWithFormat:@"https://www.findnvr.com/log_connect_info.php?where=findNVR&serverid=%@&con_time=%d&type=%@&device=iOS&mtu=%d&ipaddr=%@", [serverInfo serverID_P2P], (int)p2pUsedTime, connecttype, serverMtuSize, local_ip];
#endif
    [connecttype release];
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"GET"];
    [url release];
    
    NSURLResponse *response;
    NSError *error;
    [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    return;
}

- (BOOL)getIndexPage {
    NSString *url = [[NSString alloc] initWithFormat:@"http://%s:%d/index.php", [[p2pServerInfo serverIP] UTF8String], webport];
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"GET"];
    [url release];
    
    NSURLResponse *response;
    NSError *error = nil;
    [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    if (error != nil) {
        NSLog(@"%@",[error description]);
        return NO;
    }
    return YES;
}

#endif //FUNC_P2P_SAT_SUPPORT

#pragma mark -
#pragma mark User Interface Call Functions
- (void)login {
    NSThread *logthread = [[NSThread alloc] initWithTarget:self selector:@selector(_login) object:nil];
    [logthread start];
    //pLoginThread = logthread;
    [logthread release];
}

- (void)loginByEvent:(NSMutableArray *)serverArray {
    NSThread *logthread = [[NSThread alloc] initWithTarget:self selector:@selector(_loginByEvent:) object:serverArray];
    [logthread start];
    //pLoginThread = logthread;
    [logthread release];
}

- (void)loginMyView:(NSMutableArray *)serverArray {
    NSThread *logthread = [[NSThread alloc] initWithTarget:self selector:@selector(_loginMyView:) object:serverArray];
    [logthread start];
    //pLoginThread = logthread;
    [logthread release];
}

- (BOOL)loginAndGetCameraList {
#if DEBUG_LOG
    NSLog(@"_login start");
#endif
    
#if FUNC_P2P_SAT_SUPPORT
    if ([serverInfo connectionType] == eP2PConnection) {
        loginEznuuoFailedTime = 0;
        
        BOOL ret = NO;
        int max_retry = 4;
        while (ret == NO && loginEznuuoFailedTime < max_retry)
        {
            NSLog(@"time %d\n", loginEznuuoFailedTime);
            
            ret = [self loginEznuuo];
            if (ret == YES)
            {
                if ([self getIndexPage] == YES)
                {
                    serverMtuSize = [self querryMTUSize];
                    if (serverMtuSize == -1) //NVRSolo v1.0
                        serverMtuSize = 1500;
                    break;
                }
                else
                {
                    serverMtuSize = [self querryMTUSize];
                    if (serverMtuSize == -1) { //NVRSolo v1.0
                        serverMtuSize = 1500;
                        
                        if (loginEznuuoFailedTime == max_retry-1) {
                            ret = YES; // if it couldn't get index page and MTU, still try to login
                            break;
                        }
                        else {
                            ret = NO;
                        }
                    }
                    else if (serverMtuSize != P2P_SEVER_MTU_SIZE) {
                        [self setMTUSize:P2P_SEVER_MTU_SIZE];
                        ret = NO;
                        [NSThread sleepForTimeInterval:30];
                    }
                    else
                    {
                        // the MTU size is 1440, but still error
                        ret = NO;
                        break;
                    }
                }
            }
            
            loginEznuuoFailedTime++;
        }
        
        if (ret == NO)
            return NO;
        
        if (![self loginMainConsole])
            return NO;
    }
    else {
#endif
    if (![self loginByAllKindOfServer]) {
        return NO;
    }
#if FUNC_P2P_SAT_SUPPORT
    }
#endif
    
    cameraCount = 0;
    m_vecCheckedDevice.clear();
    
    if (serverInfo.serverType != eServer_Crystal) {
        std::vector<Np_DeviceGroup>::iterator iterDeviceGroup = m_stDeviceList.PhysicalGroup.begin();
        for (;iterDeviceGroup!=m_stDeviceList.PhysicalGroup.end();iterDeviceGroup++)
        {
            if (serverInfo.serverType == eServer_Titan)
            {
                NSString *group_description = StringWToNSString(iterDeviceGroup->description);
                if ([group_description isEqualToString:@"Main Server"]) {
                    [group_description release];
                }
                else {
                    [group_description release];
                    continue;
                }
            }
            
            std::vector<Np_SubDevice>::iterator iterSubDevice;
            std::vector<Np_Device>::iterator iterDevice;
            
            for (iterDevice = iterDeviceGroup->Camera.begin();iterDevice!=iterDeviceGroup->Camera.end();iterDevice++)
            {
                BOOL bFoundOriginal = FALSE;
                Np_SensorProfileList profilelist;
                if (Np_Result_OK == Info_GetSensorProfileList(m_hNuSDK, iterDevice->SensorDevices[0].ID, profilelist))
                {
                    for (int j=0; j<profilelist.size(); j++)
                    {
                        if (![StringWToNSString(profilelist[j].codec) isEqualToString:@""])
                            bFoundOriginal = TRUE;
                    }
                    
                    if (!bFoundOriginal) // No codec means empty camera
                        continue;
                }
                else
                {
                    continue;
                }
                m_vecCheckedDevice.push_back(*iterDevice);
                cameraCount++;
            }
        }
    }
    
#if DEBUG_LOG
    NSLog(@"_login end");
#endif
    
    [m_Condition lock];
    isRunning = YES;
    [m_Condition unlock];
    
    return YES;
}

- (BOOL)loginAndGetRecordingServerList {
#if DEBUG_LOG
    NSLog(@"_login start");
#endif
    
    if (![self loginCrystal])
        return NO;
    
#if DEBUG_LOG
    NSLog(@"_login end");
#endif
    
    [m_Condition lock];
    isRunning = YES;
    [m_Condition unlock];
    
    return YES;
}

- (std::vector<Np_Device> *)getAllCameraList {
    std::vector<Np_Device> *cameraDeviceList = new std::vector<Np_Device>;;
    if (m_hNuSDK == nil)
        return cameraDeviceList;
    if (serverInfo.serverType == eServer_Crystal)
        return cameraDeviceList;
    
    Np_DeviceList stDeviceList;
    memset(&stDeviceList, 0, sizeof(Np_DeviceList));
    
    if (Info_GetDeviceList(m_hNuSDK, stDeviceList) == Np_Result_OK)
    {
        std::vector<Np_DeviceGroup>::iterator iterDeviceGroup = stDeviceList.PhysicalGroup.begin();
        for (;iterDeviceGroup!=stDeviceList.PhysicalGroup.end();iterDeviceGroup++)
        {
            if (serverInfo.serverType == eServer_Titan)
            {
                NSString *group_description = StringWToNSString(iterDeviceGroup->description);
                if ([group_description isEqualToString:@"Main Server"]) {
                    [group_description release];
                }
                else {
                    [group_description release];
                    continue;
                }
            }
            
            std::vector<Np_Device>::iterator iterDevice;
            for (iterDevice = iterDeviceGroup->Camera.begin();iterDevice!=iterDeviceGroup->Camera.end();iterDevice++)
            {
                BOOL bFoundOriginal = FALSE;
                
                Np_SubDevice subdevice = iterDevice->SensorDevices[0];
                Np_SensorProfileList profilelist;
                if (Np_Result_OK == Info_GetSensorProfileList(m_hNuSDK, subdevice.ID, profilelist))
                {
                    for (int j=0; j<profilelist.size(); j++)
                    {
                        if (profilelist[j].profile == kProfileOriginal || profilelist[j].profile == kProfileNormal)
                        {
                            if (serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo)
                            {
                                if (![StringWToNSString(profilelist[j].codec) isEqualToString:@""]) {
                                    bFoundOriginal = TRUE;
                                }
                            }
                            else {
                                bFoundOriginal = TRUE;
                            }
                        }
                    }
                }
                
                if (!bFoundOriginal)
                    continue;
                
                cameraDeviceList->push_back(*iterDevice);
            }
        }
    }

    return cameraDeviceList;
}

- (std::vector<Np_Device_Ext> *)getAllCameraListFromRecordingServer:(Np_ID_Ext) serverID {
    std::vector<Np_Device_Ext> *cameraDeviceList = new std::vector<Np_Device_Ext>;
    if (m_hNuSDK == nil)
        return cameraDeviceList;
    if (serverInfo.serverType != eServer_Crystal)
        return cameraDeviceList;
    
    Np_DeviceList_Ext extDeviceList;
    memset(&extDeviceList, 0, sizeof(Np_DeviceList_Ext));
    Np_IDList_Ext idList;
    memset(&idList, 0, sizeof(idList));
    Np_Result_t ret = LiteHandle_UpdateDeviceInfo(m_hNuSDK, serverID, idList);
    ret |= Info_GetDeviceListFromServer(m_hNuSDK, serverID, extDeviceList);
    if (ret == Np_Result_OK) {
        for (int i = 0; i < extDeviceList.PhysicalGroup.size; i++)
        {
            Np_DeviceGroup_Ext deviceGroup = extDeviceList.PhysicalGroup.items[i];
            
            int max_order = 0;
            NSMutableArray *order_array = [[NSMutableArray alloc] init];
            for (int j = 0; j < deviceGroup.Camera.size; j++) {
                Np_Device_Ext cameradevice = deviceGroup.Camera.items[j];
                int camera_order;
                Info_GetDeviceOrder(m_hNuSDK, cameradevice.ID, camera_order);
                [order_array addObject:[NSNumber numberWithInt:camera_order]];
                if (camera_order > max_order)
                    max_order = camera_order;
            }
            
            for (int order = 0; order <= max_order; order++) {
                for (int j = 0; j < [order_array count]; j++) {
                    int camera_order = [[order_array objectAtIndex:j] intValue];
                    if (camera_order == order) {
                        Np_Device_Ext cameradevice = deviceGroup.Camera.items[j];
                        if (cameradevice.SensorDevices.size <= 0)
                            continue;
                        else {
                            cameraDeviceList->push_back(cameradevice);
                        }
                    }
                }
            }
            
            [order_array release];
        }
    }
    
    return cameraDeviceList;
}

- (BOOL)updateCameraListFromRecordingServer:(Np_ID_Ext) serverID {
    if (m_hNuSDK == nil)
        return NO;
    
    cameraCount = 0;
    IODeviceCount = 0;
    if (m_vecExtCheckedDevice.size() > 0) {
        Info_ReleaseDeviceList_Ext(m_hNuSDK, m_extDeviceList);
        memset(&m_extDeviceList, 0, sizeof(Np_DeviceList_Ext));
    }
    m_vecExtCheckedDevice.clear();
    m_vecExtDIOMasterDevice.clear();
    m_vecExtDODevice.clear();
    m_vecDOStatus.clear();
    m_vecExtDIDevice.clear();
    m_vecDIStatus.clear();
    bAllCamSupportSecondProfile = TRUE;
    m_vecCamSupportSecondProfile.clear();
    
    Np_IDList_Ext idList;
    memset(&idList, 0, sizeof(idList));
    Np_Result_t ret = LiteHandle_UpdateDeviceInfo(m_hNuSDK, serverID, idList);
    ret |= Info_GetDeviceListFromServer(m_hNuSDK, serverID, m_extDeviceList);
    if (ret == Np_Result_OK) {
        for (int i = 0; i < m_extDeviceList.PhysicalGroup.size; i++)
        {
            Np_DeviceGroup_Ext deviceGroup = m_extDeviceList.PhysicalGroup.items[i];
            
            for (int j = 0; j < deviceGroup.IOBox.size; j++)
            {
                Np_Device_Ext ioboxdevice = deviceGroup.IOBox.items[j];
                m_vecExtDIOMasterDevice.push_back(ioboxdevice);
                
                for (int k = 0; k < ioboxdevice.DIDevices.size; k++) {
                    m_vecExtDIDevice.push_back(ioboxdevice.DIDevices.items[k]);
                    m_vecDIStatus.push_back(FALSE);
                }
                
                for (int k = 0; k < ioboxdevice.DODevices.size; k++) {
                    m_vecExtDODevice.push_back(ioboxdevice.DODevices.items[k]);
                    m_vecDOStatus.push_back(FALSE);
                }
                
                IODeviceCount++;
            }
            
            
            int max_order = 0;
            NSMutableArray *order_array = [[NSMutableArray alloc] init];
            for (int j = 0; j < deviceGroup.Camera.size; j++) {
                Np_Device_Ext cameradevice = deviceGroup.Camera.items[j];
                int camera_order;
                Info_GetDeviceOrder(m_hNuSDK, cameradevice.ID, camera_order);
                [order_array addObject:[NSNumber numberWithInt:camera_order]];
                if (camera_order > max_order)
                    max_order = camera_order;
            }
            
            for (int order = 0; order <= max_order; order++) {
                for (int j = 0; j < [order_array count]; j++) {
                    int camera_order = [[order_array objectAtIndex:j] intValue];
                    if (camera_order == order) {
                        Np_Device_Ext cameradevice = deviceGroup.Camera.items[j];
                        if (cameradevice.SensorDevices.size <= 0)
                            continue;
                        else {
                            for (int k = 0; k < cameradevice.SensorDevices.size; k++) {
                                Np_SubDevice_Ext subdevice = cameradevice.SensorDevices.items[k];
                                m_vecExtCheckedDevice.push_back(subdevice);
                                Np_SensorProfile_CS_List profilelist;
                                if (Np_Result_OK == Info_GetSensorProfileList_Ext(m_hNuSDK, subdevice.ID, profilelist)) {
                                    BOOL bFoundLow = false;
                                    BOOL bFoundMinmum = false;
                                    
                                    for (int j=0; j<profilelist.size; j++) {
                                        if (profilelist.items[j].profile == kProfileLow) {
                                            bFoundLow = TRUE;
                                        }
                                        else if (profilelist.items[j].profile == kProfileMinimum) {
                                            bFoundMinmum = TRUE;
                                        }
                                    }
                                    
                                    if (!bFoundLow && !bFoundMinmum) {
                                        bAllCamSupportSecondProfile = FALSE;
                                        m_vecCamSupportSecondProfile.push_back(FALSE);
                                    }
                                    else {
                                        m_vecCamSupportSecondProfile.push_back(TRUE);
                                    }
                                    
                                    Info_ReleaseSensorProfileList_CS(m_hNuSDK, profilelist);
                                }
                                else {
                                    bAllCamSupportSecondProfile = FALSE;
                                    m_vecCamSupportSecondProfile.push_back(FALSE);
                                }
                                cameraCount++;
                            }
                            
                            for (int k = 0; k < cameradevice.DIDevices.size; k++) {
                                m_vecExtDIDevice.push_back(cameradevice.DIDevices.items[k]);
                                m_vecDIStatus.push_back(FALSE);
                            }
                            for (int k = 0; k < cameradevice.DODevices.size; k++) {
                                m_vecExtDODevice.push_back(cameradevice.DODevices.items[k]);
                                m_vecDOStatus.push_back(FALSE);
                            }
                            if (cameradevice.DIDevices.size > 0 || cameradevice.DODevices.size > 0) {
                                IODeviceCount++;
                            }
                        }
                    }
                }
            }
            
            [order_array release];
        }
        
        NSLog(@"[%s] cameraCount = %d", __FUNCTION__, cameraCount);
        
        return YES;
    }
    else
        return NO;
}

- (BOOL)updateDeviceByMyView {
    if (m_hNuSDK == nil)
        return NO;
    
    // new handler should create liveview player
    if (m_hNuSDK && !m_hLVPlayer)
        LiveView_CreatePlayer(m_hNuSDK, &m_hLVPlayer);
    if (m_hNuSDK && m_hLVPlayer)
        LiveView_SetAudioOff(m_hLVPlayer);
    
    cameraCount = 0;
    bAllCamSupportSecondProfile = TRUE;
    m_vecCamSupportSecondProfile.clear();
    
    if (serverInfo.serverType == eServer_Crystal) {
        if (m_vecExtCheckedDevice.size() > 0) {
            Info_ReleaseDeviceList_Ext(m_hNuSDK, m_extDeviceList);
            memset(&m_extDeviceList, 0, sizeof(Np_DeviceList_Ext));
        }
        m_vecExtCheckedDevice.clear();
        
        m_vecExtDODevice.clear();
        m_vecDOStatus.clear();
        m_vecExtDIDevice.clear();
        m_vecDIStatus.clear();
        
        if (Info_GetDeviceList_Ext(m_hNuSDK, m_extDeviceList) == Np_Result_OK)
        {
            for (int i = 0; i < m_extDeviceList.PhysicalGroup.size; i++)
            {
                Np_DeviceGroup_Ext deviceGroup = m_extDeviceList.PhysicalGroup.items[i];
                for (int j = 0; j < deviceGroup.Camera.size; j++) {
                    Np_Device_Ext cameradevice = deviceGroup.Camera.items[j];
                    if (cameradevice.SensorDevices.size <= 0)
                        continue;
                    else {
                        for (int k = 0; k < cameradevice.SensorDevices.size; k++)
                        {
                            Np_SubDevice_Ext subdevice = cameradevice.SensorDevices.items[k];
                            NSString *cid = [NSString stringWithFormat:@"%llu", subdevice.ID.centralID];
                            NSString *lid = [NSString stringWithFormat:@"%llu", subdevice.ID.localID];
                            for (MyViewCameraInfo *myViewCam in myViewServerInfo.cameraList) {
                                if ([myViewCam.camCentralID isEqualToString:cid] &&
                                    [myViewCam.camLocalID isEqualToString:lid]) {
                                    m_vecExtCheckedDevice.push_back(subdevice);
                                    
                                    // Update camera name in MyView
                                    NSString * cname = WChartToNSString(cameradevice.name);
                                    NSString * subname = WChartToNSString(subdevice.name);
                                    NSString *camName = [NSString stringWithFormat:@"%@-%@", cname, subname];
                                    if (![myViewCam.cameraName isEqualToString:camName]) {
                                        myViewCam.cameraName = camName;
                                    }
                                    
                                    Np_SensorProfile_CS_List profilelist;
                                    if (Np_Result_OK == Info_GetSensorProfileList_Ext(m_hNuSDK, subdevice.ID, profilelist))
                                    {
                                        BOOL bFoundLow = false;
                                        BOOL bFoundMinmum = false;
                                        
                                        for (int j=0; j<profilelist.size; j++)
                                        {
                                            if (profilelist.items[j].profile == kProfileLow) {
                                                bFoundLow = TRUE;
                                            }
                                            else if (profilelist.items[j].profile == kProfileMinimum) {
                                                bFoundMinmum = TRUE;
                                            }
                                        }
                                        
                                        if (!bFoundLow && !bFoundMinmum) {
                                            bAllCamSupportSecondProfile = FALSE;
                                            m_vecCamSupportSecondProfile.push_back(FALSE);
                                        }
                                        else {
                                            m_vecCamSupportSecondProfile.push_back(TRUE);
                                        }
                                        
                                        Info_ReleaseSensorProfileList_CS(m_hNuSDK, profilelist);
                                    }
                                    else
                                    {
                                        bAllCamSupportSecondProfile = FALSE;
                                        m_vecCamSupportSecondProfile.push_back(FALSE);
                                    }
                                    
                                    cameraCount++;
                                }
                            }
                        }
                        for (int k = 0; k < cameradevice.DIDevices.size; k++) {
                            m_vecExtDIDevice.push_back(cameradevice.DIDevices.items[k]);
                            m_vecDIStatus.push_back(FALSE);
                        }
                        for (int k = 0; k < cameradevice.DODevices.size; k++) {
                            m_vecExtDODevice.push_back(cameradevice.DODevices.items[k]);
                            m_vecDOStatus.push_back(FALSE);
                        }
                    }
                }
            }
        }
    }
    else {
        //Except Crystal 2.0 server
        m_vecCheckedDevice.clear();
        m_stDeviceList.PhysicalGroup.clear();
        m_stDeviceList.LogicalGroup.clear();
        
        m_vecDODevice.clear();
        m_vecDOStatus.clear();
        m_vecDIDevice.clear();
        m_vecDIStatus.clear();
        
        if (Info_GetDeviceList(m_hNuSDK, m_stDeviceList) == Np_Result_OK)
        {
            std::vector<Np_DeviceGroup>::iterator iterDeviceGroup = m_stDeviceList.PhysicalGroup.begin();
            for (;iterDeviceGroup!=m_stDeviceList.PhysicalGroup.end();iterDeviceGroup++)
            {
                if (serverInfo.serverType == eServer_Titan)
                {
                    NSString *group_description = StringWToNSString(iterDeviceGroup->description);
                    if ([group_description isEqualToString:@"Main Server"]) {
                        [group_description release];
                    }
                    else {
                        [group_description release];
                        continue;
                    }
                }
                
                std::vector<Np_Device>::iterator iterDevice;
                std::vector<Np_SubDevice>::iterator iterSubDevice;
                
                for (iterDevice = iterDeviceGroup->Camera.begin();iterDevice!=iterDeviceGroup->Camera.end();iterDevice++)
                {
                    Np_SubDevice subdevice = iterDevice->SensorDevices[0];
                    NSString *cid = [NSString stringWithFormat:@"%d", subdevice.ID.centralID];
                    NSString *lid = [NSString stringWithFormat:@"%d", subdevice.ID.localID];
                    for (MyViewCameraInfo *myViewCam in myViewServerInfo.cameraList) {
                        if ([myViewCam.camCentralID isEqualToString:cid] &&
                            [myViewCam.camLocalID isEqualToString:lid]) {
                            Np_SensorProfileList profilelist;
                            if (Np_Result_OK == Info_GetSensorProfileList(m_hNuSDK, subdevice.ID, profilelist))
                            {
                                BOOL bFoundLow = false;
                                BOOL bFoundMinmum = false;
                                BOOL bFoundOriginal = false;
                                for (int j=0; j<profilelist.size(); j++)
                                {
                                    if (profilelist[j].profile == kProfileLow) {
                                        bFoundLow = TRUE;
                                    }
                                    else if (profilelist[j].profile == kProfileMinimum) {
                                        bFoundMinmum = TRUE;
                                    }
                                    else {
                                        if (serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo) {
                                            if (![StringWToNSString(profilelist[j].codec) isEqualToString:@""])
                                                bFoundOriginal = TRUE;
                                        }
                                        else
                                            bFoundOriginal = TRUE;
                                    }
                                }
                                
                                if (!bFoundOriginal)
                                    continue;
                                
                                if (!bFoundLow && !bFoundMinmum) {
                                    bAllCamSupportSecondProfile = FALSE;
                                    m_vecCamSupportSecondProfile.push_back(FALSE);
                                }
                                else {
                                    m_vecCamSupportSecondProfile.push_back(TRUE);
                                }
                            }
                            else
                            {
                                bAllCamSupportSecondProfile = FALSE;
                                m_vecCamSupportSecondProfile.push_back(FALSE);
                            }
                            
                            m_vecCheckedDevice.push_back(*iterDevice);
                            cameraCount++;
                            
                            // Update camera name in MyView
                            NSString *camName = StringWToNSString(iterDevice->name);
                            if (![myViewCam.cameraName isEqualToString:camName]) {
                                myViewCam.cameraName = camName;
                            }
                            [camName release];
                            
                            for (iterSubDevice = iterDevice->DIDevices.begin(); iterSubDevice != iterDevice->DIDevices.end(); iterSubDevice++) {
                                m_vecDIDevice.push_back(*iterSubDevice);
                                m_vecDIStatus.push_back(FALSE); // jerrylu, 2012/07/15
                            }
                            for (iterSubDevice = iterDevice->DODevices.begin(); iterSubDevice != iterDevice->DODevices.end(); iterSubDevice++) {
                                m_vecDODevice.push_back(*iterSubDevice);
                                m_vecDOStatus.push_back(FALSE); // jerrylu, 2012/07/15
                            }
                        }
                    }
                }
            }
        }
    }
    
    return YES;
}

- (NSString *)getCameraNameFromIDExt:(Np_ID_Ext) sensorID {
    NSString *camera_name = @"";
    
    if (isRunning) {
        if (m_extDeviceList.PhysicalGroup.size != 0) {
            for (int i = 0; i < m_extDeviceList.PhysicalGroup.size; i++) {
                Np_DeviceGroup_Ext deviceGroup = m_extDeviceList.PhysicalGroup.items[i];
                for (int j = 0; j < deviceGroup.Camera.size; j++) {
                    Np_Device_Ext cameradevice = deviceGroup.Camera.items[j];
                    for (int k = 0; k < cameradevice.SensorDevices.size; k++) {
                        Np_SubDevice_Ext subdevice = cameradevice.SensorDevices.items[k];
                        if (subdevice.ID.centralID == sensorID.centralID &&
                            subdevice.ID.localID == sensorID.localID) {
                            NSString * cname = WChartToNSString(cameradevice.name);
                            NSString * subname = WChartToNSString(subdevice.name);
                            camera_name = [NSString stringWithFormat:@"%@-%@", cname, subname];
                            [cname release];
                            [subname release];
                        }
                    }
                }
            }
        }
    }
    else {
        // fake camera list
        for (int i = 0; i < m_vecExtCheckedDevice.size(); i++) {
            Np_SubDevice_Ext subdevice = m_vecExtCheckedDevice.at(i);
            if (subdevice.ID.centralID == sensorID.centralID &&
                subdevice.ID.localID == sensorID.localID) {
                camera_name = WChartToNSString(subdevice.name);
            }
        }
    }

    return camera_name;
}

- (NSString *)getUserName {
    if (serverInfo.connectionType == eP2PConnection) {
        return serverInfo.userName_P2P;
    }
    else {
        return serverInfo.userName;
    }
}

- (void)logout {
#if DEBUG_LOG
    NSLog(@"ControlHElper logout");
#endif
    [self setlogoutstatus];
    
#if FUNC_P2P_SAT_SUPPORT
    if (p2pStartTime != nil) {
        [self uploadRawDatatoServer];
    }
#endif
    NSThread *logoutThread = [[NSThread alloc] initWithTarget:self selector:@selector(stop) object:nil];
    [logoutThread start];
    [logoutThread release];
}

- (Np_Device_Ext *)searchExtDeviceBySensorDeviceIndex:(int) index {
    for (int i = 0; i < m_extDeviceList.PhysicalGroup.size; i++)
    {
        Np_DeviceGroup_Ext deviceGroup = m_extDeviceList.PhysicalGroup.items[i];
        for (int j = 0; j < deviceGroup.Camera.size; j++) {
            Np_Device_Ext cameradevice = deviceGroup.Camera.items[j];
            for (int k = 0; k < cameradevice.SensorDevices.size; k++) {
                Np_SubDevice_Ext sensordevice = cameradevice.SensorDevices.items[k];
                if (sensordevice.ID.centralID == m_vecExtCheckedDevice[index].ID.centralID &&
                    sensordevice.ID.localID == m_vecExtCheckedDevice[index].ID.localID)
                {
                    return (Np_Device_Ext *)&deviceGroup.Camera.items[j];
                }
            }
        }
    }
    return nil;
}

#pragma mark -
#pragma mark CameraHelperDelegate protocol
- (void)cameraHelperDidRefreshImage:(int) index WithUIImage:(UIImage *) img {
    if (index < cameraCount && [NSNull null] != (NSNull *)img) {
        if (serverManagerDelegate!= nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperDidRefreshImages:camIndex:camImg:)]) {
            [serverManagerDelegate controlHelperDidRefreshImages:self camIndex:index camImg:img];
        }
    }
}

- (void)cameraHelperDidRefreshImage:(int) index WithRaw:(NSData *)data Length:(int)len Width:(int)width Hieght:(int)height {
    if (index < cameraCount && (NSNull *)data != [NSNull null]) {
        if (serverManagerDelegate!= nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperDidRefreshImagesBuf:camIndex:img:imgWidth:imgHeight:)]) {
            [serverManagerDelegate controlHelperDidRefreshImagesBuf:self camIndex:index img:data imgWidth:width imgHeight:height];
        }
    }
}

// jerrylu, 2012/05/23, OpenAL to handleg audio data
- (void)cameraHelperDidRefreshAudio:(int) index WithAudio:(AudioData) audiodata {
    CameraHelper * obj = [cameraHelperArray objectAtIndex:index];
    
    // jerrylu, 2012/06/13, disable audio by calling next/previous frame
    if (serverInfo.serverType == eServer_Crystal) {
        if ([obj isPlaybackConnectionCreated] && !playbackAudioEnable) {
            return;
        }
    }
    else {
        if ([obj isPlaybackConnectionCreated] && (!playbackAudioEnable || [self playbackGetState] != kStateRunning)) {
            return;
        }
    }
    
    if (serverManagerDelegate!= nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperDidRefreshAudio:camIndex:WithAudio:)]) {
        [serverManagerDelegate controlHelperDidRefreshAudio:self camIndex:index WithAudio:audiodata];
    }
}
// jerrylu, 2012/05/23
- (void)cameraHelperErrorHandle:(int) index error:(Np_Error) error {
    // jerrylu, 2012/06/18, if receive the event to logout, don't care error
    if (self.isLogouting == YES) {
        return;
    }
    
    CameraHelper * obj = [cameraHelperArray objectAtIndex:index];
    
    if (error == Np_ERROR_SESSION_LOST) {
#if DEBUG_LOG
        NSLog(@"Session Lost");
#endif
        if (serverManagerDelegate != nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperDidCameraSessionLost:camIndex:)]) {
            [serverManagerDelegate controlHelperDidCameraSessionLost:self camIndex:index];
        }
    }
    else if (error == Np_ERROR_FATAL_ERROR) {
#if DEBUG_LOG
        NSLog(@"Session Error");
#endif

        if ([obj isPlaybackConnectionCreated]) {
            playbackEnable = FALSE;
            [self disconnectAllPlaybackConnection]; // jerrylu, 2012/06/13
                
            if (serverManagerDelegate != nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperDidPlaybackDisconnection:)]) {
                [serverManagerDelegate controlHelperDidPlaybackDisconnection:self];
            }
        }
    }
    else if (error == Np_ERROR_UNSUPPORTED_FORMAT) {
        if (serverManagerDelegate != nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperDidUnsupportedFormat)]) {
            [serverManagerDelegate controlHelperDidUnsupportedFormat];
        }
    }
}

- (void)cameraHelperPlaybackFeedbackHandle:(int) index {
    // jerrylu, 2012/06/18, if receive the event to logout, don't care error
    if (self.isLogouting == YES) {
        return;
    }
    
    CameraHelper * obj = [cameraHelperArray objectAtIndex:index];
    
    if ([obj isPlaybackConnectionCreated]) {
        if (serverManagerDelegate != nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperDidPbNextPreviosFrameSuccess:)]) {
            [serverManagerDelegate controlHelperDidPbNextPreviosFrameSuccess:self];
        }
    }
}

#pragma mark -
#pragma mark Server General Methods

- (BOOL)isCamStreaming:(int) idx {
	// if user touch the camera and there is no obj, we will just return
	if (idx >= [cameraHelperArray count]) {
		return FALSE;
	}
	return TRUE;
}

- (void)stop
{
#if DEBUG_LOG
    NSLog(@"ControlHelper stop!");
#endif
    [m_Condition lock]; // jerrylu, 2012/07/12

    if(self.isRunning)
    {
#if DEBUG_LOG
        NSLog(@"ControlHelper ready to destroy handle!");
#endif
        self.isRunning = NO;
        
        // jerrylu, 2012/05/11
        if([m_checkConnectionThread isExecuting])
        {
            [m_checkConnectionThread cancel];
            while (![m_checkConnectionThread isFinished]) 
                [NSThread sleepForTimeInterval:0.5];
        }
        [m_checkConnectionThread release];
        
        if([m_checkPlaybackConnectionThread isExecuting])
        {
            [m_checkPlaybackConnectionThread cancel];
            while (![m_checkPlaybackConnectionThread isFinished]) 
                [NSThread sleepForTimeInterval:0.5];
        }
        [m_checkPlaybackConnectionThread release];
        
        [self stopStream];
        
        for (CameraHelper * camHelper in cameraHelperArray) {
            [camHelper liveViewStopConnection];
            [camHelper playbackStopConnection]; // jerrylu, 2012/05/09
        }
        
        if (m_hEventSession) {
            Event_UnsubscribeExt(m_hEventSession);
        }
        
        Info_ReleaseRecordLogsListExt(m_hNuSDKPlayback, m_recordLogListList);
        Info_ReleaseScheduleLogsList(m_hNuSDKPlayback, m_scheduleLogListList);
        
        Info_ReleaseDeviceList_Ext(m_hNuSDK, m_extDeviceList);
        Info_ReleaseRecordingServerList(m_hNuSDK, m_recordingServerList);
        
        // jerrylu, 2012/05/08, Support Playback Function
        if (m_hPBPlayer) {
            PlayBack_DestroyPlayer(m_hPBPlayer);
            m_hPBPlayer = NULL;
        }
        
        if (m_hNuSDKPlayback && (serverInfo.serverType == eServer_MainConsole || serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo)) {
            Destroy_Handle(m_hNuSDKPlayback);
            m_hNuSDKPlayback = NULL;
        }else {
            m_hNuSDKPlayback = NULL;
        }
        
        // release LiveView resource
        if (m_hLVPlayer) {
            LiveView_DestroyPlayer(m_hLVPlayer);
            m_hLVPlayer = NULL;
        }
        
        if (m_hNuSDK) {
            Destroy_Handle(m_hNuSDK);
            m_hNuSDK = NULL;
        }
        
#if FUNC_P2P_SAT_SUPPORT
        // jerrylu, 2012/09/13
        if([m_quotaControlThread isExecuting])
        {
            [m_quotaControlThread cancel];
            while (![m_quotaControlThread isFinished])
                [NSThread sleepForTimeInterval:0.5];
        }
        [m_quotaControlThread release];
        
        //Release SAT P2P Tunnel
        if(p_tunnel != NULL)
        {
            p_tunnel->Stop();
            delete p_tunnel;
            p_tunnel = NULL;
        }
        
        if (p2pStartTime != nil) {
            [p2pStartTime release];
            p2pStartTime = nil;
        }
        
        if (local_ip != nil)
            [local_ip release];
#endif
        presetDelegate = nil;
        loadingDelegate = nil;
    }
    else
        NSLog(@"CtrlHelper is not running!");
    
    if (cameraHelperArray != nil) {
        for (CameraHelper * camHelper in cameraHelperArray)
            [camHelper release];
        cameraHelperArray = nil;
	}
	[presetIdArray release];
    presetIdArray = nil;
	[presetNameArray release];
    presetNameArray = nil;
    
    serverManagerDelegate = nil;
    presetDelegate = nil;
    loadingDelegate = nil; // jerrylu, 2012/09/12
    
    [IOInputDevIDArray release];
    IOInputDevIDArray = nil;
	[IOOutputDevIDArray release];
    IOOutputDevIDArray = nil;
	[IOInputPinNameArray release];
    IOInputPinNameArray = nil;
	[IOOutputPinNameArray release];
    IOOutputPinNameArray = nil;
	[IOInputPinCountArray release];
    IOInputPinCountArray = nil;
	[IOOutputPinCountArray release];
    IOOutputPinCountArray = nil;
	[IODevNameArray release];
    IODevNameArray = nil;
	[IOPinDOAccessArray release];
    IOPinDOAccessArray = nil;
	[IOModuleIDArray release];
    IOModuleIDArray = nil;
#if FUNC_P2P_SAT_SUPPORT // jerrylu, 2012/09/19
    if (p2pServerInfo != nil) {
        [p2pServerInfo release];
        p2pServerInfo = nil;
    }
#endif
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
}

- (void)stopStream {
	[self disconnectAllCam];
    [self disconnectAllPlaybackConnection]; // jerrylu, 2012/05/10
}

// jerrylu, 2012/08/07
- (void)saveServerInfo {
    if (serverManagerDelegate != nil) {
        if ([serverManagerDelegate respondsToSelector:@selector(controlHelperSave:)]) {
            [serverManagerDelegate controlHelperSave:self];
        }
    }
}

#pragma mark -
#pragma mark Server LiveView Methods
- (void)connectToCam:(int) position eLayout:(ELayoutType) eType {
	EProfileType eProfileType;
    if (IS_IPAD) {
        switch (eType) {
            case eLayout_2X3:
            case eLayout_2X2:
            case eLayout_1X3:
            case eLayout_1X2:
            case eLayout_Custom:
            case eLayout_3X5:
            case eLayout_4X6:
            case eLayout_5X8:
                eProfileType = eMinimum;
                break;
            default:
                break;
        }
    }
    else {
        switch (eType) {
            case eLayout_2X3:
            case eLayout_Custom:
            case eLayout_2X2:
            case eLayout_1X3:
            case eLayout_1X2:
            case eLayout_3X5:
            case eLayout_4X6:
            case eLayout_5X8:
                eProfileType = eMinimum;
                break;
            default:
                break;
        }
    }
	
	if (cameraHelperArray == nil) {
		cameraHelperArray = [[NSMutableArray alloc] init];
		for (int i = 0; i < cameraCount; i++) {
			CameraHelper * camHelper = [[CameraHelper alloc] init];
			[camHelper setServerInfo:serverInfo];
			[camHelper setCtrlHelperDelegate:self];
			[camHelper setEProfileType:eNone];
			[camHelper setCameraIndex:i];
            [camHelper setLvPlayer:m_hLVPlayer];
            if (serverInfo.serverType != eServer_Crystal) {
                [camHelper setNpDevice:m_vecCheckedDevice[i]];
                // jerrylu, 2012/10/16, fix bug9456
                Np_SensorProfileList profilelist;
                if ( Np_Result_OK == Info_GetSensorProfileList(m_hNuSDK, m_vecCheckedDevice[i].SensorDevices[0].ID, profilelist))
                {
                    BOOL bFoundOriginal = FALSE;
                    
                    for (int j=0; j<profilelist.size(); j++)
                    {
                        if (profilelist[j].profile == kProfileOriginal) {
                            bFoundOriginal = TRUE;
                        }
                    }
                    
                    if (bFoundOriginal) {
                        [camHelper setEMaxProfile:eOriginal];
                    }
                    else {
                        [camHelper setEMaxProfile:eMedium];
                    }
                }
                else
                {
                    [camHelper setEMaxProfile:eMedium];
                }
                
                if (serverInfo.serverType == eServer_Titan) {
                    [camHelper setPbPlayer:m_hPBPlayer]; // jerrylu, 2012/05/08
                    [camHelper setPbNpDevice:m_vecCheckedDevice[i]];
                }
            }
            else {
                [camHelper setNpDeviceExt:m_vecExtCheckedDevice[i]];
                [camHelper setPbPlayer:m_hPBPlayer];
                [camHelper setPbNpDeviceExt:m_vecExtCheckedDevice[i]];
                
                Np_SensorProfile_CS_List profilelist;
                if (Np_Result_OK == Info_GetSensorProfileList_Ext(m_hNuSDK, m_vecExtCheckedDevice[i].ID, profilelist))
                {
                    BOOL bFoundOriginal = FALSE;
                    
                    for (int j=0; j<profilelist.size; j++)
                    {
                        if (profilelist.items[j].profile == kProfileOriginal) {
                            bFoundOriginal = TRUE;
                        }
                    }
                    
                    if (bFoundOriginal) {
                        [camHelper setEMaxProfile:eOriginal];
                    }
                    else {
                        [camHelper setEMaxProfile:eMedium];
                    }
                    
                    Info_ReleaseSensorProfileList_CS(m_hNuSDK, profilelist);
                }
                else
                {
                    [camHelper setEMaxProfile:eMedium];
                }
            }
            
			[cameraHelperArray addObject:camHelper];
			[camHelper release];
		}
        if(![m_checkConnectionThread isExecuting])
            [m_checkConnectionThread start];
        
        if(![m_checkPlaybackConnectionThread isExecuting])
            [m_checkPlaybackConnectionThread start]; // jerrylu, 2012/05/11

#if FUNC_P2P_SAT_SUPPORT
        // jerrylu, 2012/09/13
        if ([serverInfo connectionType] == eP2PConnection) {
            // create other thread to control quota
            if (m_quotaControlThread == nil)
                m_quotaControlThread = [[NSThread alloc] initWithTarget:self selector:@selector(quotaControl) object:NULL];
            if(![m_quotaControlThread isExecuting] && strcmp(nattype.c_str(), NAT_RELAY) == 0)
                [m_quotaControlThread start];
        }
#endif
	}
	
	CameraHelper * helper = [cameraHelperArray objectAtIndex:position];
	[helper initValue];
	[helper setEProfileType:eProfileType];
	//NSLog(@"connect to cam%d", position);
    if (m_vecCamSupportSecondProfile[position]) {
        [helper liveViewConnect];
    }
}

- (void)connectToAllCam {
    if (cameraHelperArray == nil) {
		cameraHelperArray = [[NSMutableArray alloc] init];
		for (int i = 0; i < cameraCount; i++) {
			//[cameraHelperArray addObject:[NSNull null]];
			CameraHelper * camHelper = [[CameraHelper alloc] init];
			[camHelper setServerInfo:serverInfo];
			[camHelper setCtrlHelperDelegate:self];
			[camHelper setEProfileType:eNone];
			[camHelper setCameraIndex:i];
            [camHelper setNpDevice:m_vecCheckedDevice[i]];
            [camHelper setLvPlayer:m_hLVPlayer];
            
            if (serverInfo.serverType == eServer_Titan) {
                [camHelper setPbPlayer:m_hPBPlayer]; // jerrylu, 2012/05/08
                [camHelper setPbNpDevice:m_vecCheckedDevice[i]];
            }
            else if (serverInfo.serverType == eServer_Crystal) {
                [camHelper setPbPlayer:m_hPBPlayer];
                [camHelper setPbNpDeviceExt:m_vecExtCheckedDevice[i]];
            }
			[cameraHelperArray addObject:camHelper];
			[camHelper release];
		}
        [self disconnectAllPlaybackConnection];
        [self disconnectAllCam];
        
        if(![m_checkConnectionThread isExecuting])
            [m_checkConnectionThread start];
        
        if(![m_checkPlaybackConnectionThread isExecuting])
            [m_checkPlaybackConnectionThread start]; // jerrylu, 2012/05/11
        
#if FUNC_P2P_SAT_SUPPORT
        // jerrylu, 2012/09/13
        if ([serverInfo connectionType] == eP2PConnection) {
            // create other thread to control quota
            if (m_quotaControlThread == nil)
                m_quotaControlThread = [[NSThread alloc] initWithTarget:self selector:@selector(quotaControl) object:NULL];
            if(![m_quotaControlThread isExecuting] && strcmp(nattype.c_str(), NAT_RELAY) == 0)
                [m_quotaControlThread start];
        }
#endif
	}
}
- (void)reloadAllDevices {
    if (serverInfo.serverType != eServer_NVRmini && serverInfo.serverType != eServer_NVRsolo) {
        return;
    }
    
    m_stDeviceList.PhysicalGroup.clear();
    m_stDeviceList.LogicalGroup.clear();
    Info_GetDeviceList(m_hNuSDK, m_stDeviceList);
    
    [self update_IODeviceInfo];
}

- (void)connectToCamWithHighResolution:(int) position {
	if (LoggingPacket) {
		NSLog(@"connect to cam with hi res at:%d",position);
	}
	CameraHelper * helper = [cameraHelperArray objectAtIndex:position];

	[helper initValue];
    
    if (m_vecCamSupportSecondProfile[position]) {
                    [helper setEProfileType:eLow];
        [helper liveViewConnect];
    }
    else {
        [helper setEProfileType:eOriginal];
        [helper liveViewConnect];
    }
}

- (void)setProfileTypeWithCam:(int) position profiletype:(EProfileType) profiletype {
	CameraHelper * helper = [cameraHelperArray objectAtIndex:position];
	[helper setEProfileType:profiletype];
}

- (EProfileType)getProfileTypeWithCam:(int) position {
	CameraHelper * helper = [cameraHelperArray objectAtIndex:position];
	return  helper.eProfileType;
}

// jerrylu, 2012/09/06, fix bug8410 
- (EProfileType)getConnectProfileTypeWithCam:(int) position {
	CameraHelper * helper = [cameraHelperArray objectAtIndex:position];
	return  [helper getConnectProfile];
}

- (void)stopconnectToCam {
    if([m_checkConnectionThread isExecuting])
    {
        [m_checkConnectionThread cancel];
        while (![m_checkConnectionThread isFinished])
            [NSThread sleepForTimeInterval:0.5];
    }
    [m_checkConnectionThread release];
    
    if([m_checkPlaybackConnectionThread isExecuting])
    {
        [m_checkPlaybackConnectionThread cancel];
        while (![m_checkPlaybackConnectionThread isFinished])
            [NSThread sleepForTimeInterval:0.5];
    }
    [m_checkPlaybackConnectionThread release];
    
    //create other thread to check connection, avoid to block main thread
    m_checkConnectionThread = [[NSThread alloc] initWithTarget:self selector:@selector(checkConnection) object:NULL];
    
    //create other thread to check playback connection, avoid to block main thread
    m_checkPlaybackConnectionThread = [[NSThread alloc] initWithTarget:self selector:@selector(checkPbConnection) object:NULL];
    
    [self stopStream];
    
    for (CameraHelper * camHelper in cameraHelperArray) {
        [camHelper liveViewStopConnection];
        [camHelper playbackStopConnection];
    }
    
    if (cameraHelperArray != nil) {
        for (CameraHelper * camHelper in cameraHelperArray)
            [camHelper release];
        cameraHelperArray = nil;
    }
}

- (void)disconnectAllCam {
	for (CameraHelper * camHelper in cameraHelperArray) {
		if ([camHelper respondsToSelector:@selector(liveViewDisconnect)]) {
			[camHelper liveViewDisconnect];
		}
	}
}

- (void)checkConnection {
    while (TRUE) {
        [NSThread sleepForTimeInterval:1];
        for (CameraHelper * camHelper in cameraHelperArray) {
            if ([camHelper respondsToSelector:@selector(checkLiveViewConnection)]) {
                [camHelper checkLiveViewConnection];
            }
            
            if ([[NSThread currentThread] isCancelled]) {
                [NSThread exit];
                return;
            }
        }
        
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
    }
}
// jerrylu, 2012/07/05
- (BOOL)getLiveViewConnection:(int) index {
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    if([camHelper isLiveViewConnectionCreated])
        return YES;
    else
        return NO;
}

#pragma mark -
#pragma mark Server PTZ Methods
- (BOOL)getPTZCapOnCam:(int) index {
    if (index >= [cameraHelperArray count]) {
#if DEBUG_LOG
        NSLog(@"[%s] Bad Index", __FUNCTION__);
#endif
		return NO;
	}
    
    if (serverInfo.serverType == eServer_Crystal) {
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        if (cameradevice->PTZDevices.size == 0) {
            return NO;
        }
    }
    else {
        if (m_vecCheckedDevice[index].PTZDevices.size() == 0) {
            return NO;
        }
    }
    return YES;
}

- (void)setPTZOnCamera:(int) index withDirection:(NSString*) dir {
    if (serverInfo.serverType == eServer_Crystal) {
        Np_PTZControlParam_CS ptzParam;
        Np_PTZContinuousMove ptzMoveDirection;
        memset(&ptzParam, 0, sizeof(ptzParam));
        memset(&ptzMoveDirection, 0, sizeof(ptzMoveDirection));
        
        ptzParam.command = kPTZContinuousMove;
        if ([dir isEqual:@"TiltUp"])
            ptzMoveDirection.tilt = kTiltUp;
        else if ( [dir isEqual:@"TiltDown"])
            ptzMoveDirection.tilt = kTiltDown;
        else if ( [dir isEqual:@"PanRight"])
            ptzMoveDirection.pan = kPanRight;
        else if ( [dir isEqual:@"PanLeft"])
            ptzMoveDirection.pan = kPanLeft;
        else if ( [dir isEqual:@"UpRight"])
        {
            ptzMoveDirection.pan = kPanRight;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"UpLeft"])
        {
            ptzMoveDirection.pan = kPanLeft;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"DownLeft"])
        {
            ptzMoveDirection.pan = kPanLeft;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"DownRight"])
        {
            ptzMoveDirection.pan = kPanRight;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"ZoomTele"])
            ptzMoveDirection.zoom = kZoomIn;
        else if ( [dir isEqual:@"ZoomWide"])
            ptzMoveDirection.zoom = kZoomOut;
        else if ( [dir isEqual:@"Home"])
            ptzParam.command = kPTZHome;
        else if ([dir isEqual:@"TiltUpStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"TiltDownStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"PanRightStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanRight;
        }
        else if ( [dir isEqual:@"PanLeftStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanLeft;
        }
        else if ( [dir isEqual:@"UpRightStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanRight;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"UpLeftStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanLeft;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"DownLeftStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanLeft;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"DownRightStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanRight;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"ZoomTeleStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.zoom = kZoomIn;
        }
        else if ( [dir isEqual:@"ZoomWideStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.zoom = kZoomOut;
        }
        else if ( [dir isEqual:@"HomeStop"])
        {
            ptzParam.command = kPTZStop;
            ptzParam.command = kPTZHome;
        }
        
        ptzParam.param.move = &ptzMoveDirection;
        
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        Np_ID_Ext stID = cameradevice->PTZDevices.items[0].ID;
        
        Control_PTZ_PTZDeviceID_CS_Ext(m_hNuSDK, stID, &ptzParam);
    }
    else {
        Np_PTZControlParam ptzParam;
        Np_PTZContinuousMove ptzMoveDirection;
        memset(&ptzParam, 0, sizeof(ptzParam));
        memset(&ptzMoveDirection, 0, sizeof(ptzMoveDirection));
        
        ptzParam.command = kPTZContinuousMove;
        if ([dir isEqual:@"TiltUp"])
            ptzMoveDirection.tilt = kTiltUp;
        else if ( [dir isEqual:@"TiltDown"])
            ptzMoveDirection.tilt = kTiltDown;
        else if ( [dir isEqual:@"PanRight"])
            ptzMoveDirection.pan = kPanRight;
        else if ( [dir isEqual:@"PanLeft"])
            ptzMoveDirection.pan = kPanLeft;
        else if ( [dir isEqual:@"UpRight"])
        {
            ptzMoveDirection.pan = kPanRight;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"UpLeft"])
        {
            ptzMoveDirection.pan = kPanLeft;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"DownLeft"])
        {
            ptzMoveDirection.pan = kPanLeft;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"DownRight"])
        {
            ptzMoveDirection.pan = kPanRight;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"ZoomTele"])
            ptzMoveDirection.zoom = kZoomIn;
        else if ( [dir isEqual:@"ZoomWide"])
            ptzMoveDirection.zoom = kZoomOut;
        else if ( [dir isEqual:@"Home"])
            ptzParam.command = kPTZHome;
        else if ([dir isEqual:@"TiltUpStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"TiltDownStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"PanRightStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanRight;
        }
        else if ( [dir isEqual:@"PanLeftStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanLeft;
        }
        else if ( [dir isEqual:@"UpRightStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanRight;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"UpLeftStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanLeft;
            ptzMoveDirection.tilt = kTiltUp;
        }
        else if ( [dir isEqual:@"DownLeftStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanLeft;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"DownRightStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.pan = kPanRight;
            ptzMoveDirection.tilt = kTiltDown;
        }
        else if ( [dir isEqual:@"ZoomTeleStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.zoom = kZoomIn;
        }
        else if ( [dir isEqual:@"ZoomWideStop"])
        {
            ptzParam.command = kPTZStop;
            ptzMoveDirection.zoom = kZoomOut;
        }
        else if ( [dir isEqual:@"HomeStop"])
        {
            ptzParam.command = kPTZStop;
            ptzParam.command = kPTZHome;
        }
        
        ptzParam.param.move = &ptzMoveDirection;    
        
        Np_Device cameradevice = m_vecCheckedDevice.at(index);
        Np_ID stID = cameradevice.PTZDevices[0].ID;
        
        // jerrylu, 2012/05/25, change to function for ptz device ID
        Control_PTZ_PTZDeviceID(m_hNuSDK, stID, &ptzParam);
    }
}

- (void)gotoPTZPreset:(int) index withIdx:(int) idx {
    if (serverInfo.serverType == eServer_Crystal) {
        if (idx < 0 || idx >= m_stExtPTZPresetList.size) {
            return;
        }
        Np_PTZControlParam_CS ptzParam;
        Np_PTZPreset_CS       ptzPreset;
        memset(&ptzParam, 0, sizeof(Np_PTZControlParam_CS));
        
        ptzPreset.presetNo = m_stExtPTZPresetList.items[idx].presetNo;
        ptzPreset.presetName = m_stExtPTZPresetList.items[idx].presetName;
        ptzParam.command = kPTZPresetGo;
        ptzParam.param.preset = &ptzPreset;
        
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        Np_ID_Ext stID = cameradevice->PTZDevices.items[0].ID;
        
        // jerrylu, 2012/05/25, change to function for ptz device ID
        Control_PTZ_PTZDeviceID_CS_Ext(m_hNuSDK, stID, &ptzParam);
    }
    else {
        if (idx < 0 || idx >= m_stPTZPresetList.size()) {
            return;
        }
        
        Np_PTZControlParam ptzParam;
        Np_PTZPreset       ptzPreset;
        memset(&ptzParam, 0, sizeof(Np_PTZControlParam));
        
        ptzPreset.presetNo = m_stPTZPresetList[idx].presetNo;
        ptzPreset.presetName = m_stPTZPresetList[idx].presetName;
        ptzParam.command = kPTZPresetGo;
        ptzParam.param.preset = &ptzPreset;
        
        Np_Device cameradevice = m_vecCheckedDevice.at(index);
        Np_ID stID = cameradevice.PTZDevices[0].ID;
        
        // jerrylu, 2012/05/25, change to function for ptz device ID
        Control_PTZ_PTZDeviceID(m_hNuSDK, stID, &ptzParam);
    }
}

- (void)getPresetToCam:(int) index {
    if (index >= [cameraHelperArray count]) {
#if DEBUG_LOG
        NSLog(@"[%s] Bad Index", __FUNCTION__);
#endif
		return;
	}
    
    if (serverInfo.serverType == eServer_Crystal) {
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        if (cameradevice->PTZDevices.size == 0) {
            return;
        }
        else
            m_idExtPreset = cameradevice->PTZDevices.items[0].ID;
    }
    else {
        m_idPreset = m_vecCheckedDevice[index].PTZDevices[0].ID;
    }
    
    [self setPresentStatus:NO]; // jerrylu, 2012/06/29
    
    NSThread * pGetPresetThread = [[NSThread alloc] initWithTarget:self selector:@selector(_getPreset) object:NULL];
    [pGetPresetThread start];
    [pGetPresetThread release];
}

- (void)_getPresetOK {
    if (presetDelegate != nil &&[presetDelegate respondsToSelector:@selector(ControlHelper:didGetPresetOnCamera:)]) {
		[presetDelegate ControlHelper:self didGetPresetOnCamera:0];
	}
}

- (void)_getPreset {
    if (serverInfo.serverType == eServer_Crystal) {
        Info_ReleasePTZPreset_CS_Ext(m_hNuSDK, m_stExtPTZPresetList);
        
        memset(&m_stExtPTZPresetList, 0 , sizeof(m_stExtPTZPresetList));
        
        Np_Result_t re = Info_GetPTZPreset_CS_Ext(m_hNuSDK, m_idExtPreset, m_stExtPTZPresetList);
        if (re == Np_Result_OK) {
            if (presetNameArray == nil) {
                presetNameArray = [[NSMutableArray alloc] init];
            }
            [presetNameArray removeAllObjects];
            for (int i=0; i < m_stExtPTZPresetList.size; i++) {
                NSString *presetName = WChartToNSString(m_stExtPTZPresetList.items[i].presetName);
                if (![presetName isEqualToString:@""]) {
                    [presetNameArray addObject:presetName];
                }
                else { // jerrylu, 2012/06/15
                    //[presetNameArray addObject:@"TEST"];
                }
                [presetName release];
            }
        }
    }
    else {
        Np_Result_t re = Info_GetPTZPreset(m_hNuSDK, m_idPreset, m_stPTZPresetList);
        
        if (re == Np_Result_OK) {
            if (presetNameArray == nil) {
                presetNameArray = [[NSMutableArray alloc] init];
            }
            [presetNameArray removeAllObjects];
            for (int i=0; i < m_stPTZPresetList.size(); i++) {
                if (m_stPTZPresetList[i].presetName.empty() == false) {
                    NSString *presetName = StringWToNSString(m_stPTZPresetList[i].presetName);
                    [presetNameArray addObject:presetName];
                    [presetName release];
                }
                else { // jerrylu, 2012/06/15
                    //[presetNameArray addObject:@"TEST"];
                }
            }
        }
    }
    
    [self setPresentStatus:YES]; // jerrylu, 2012/06/29
    
    [self performSelectorOnMainThread:@selector(_getPresetOK) withObject:NULL waitUntilDone:NO];
}

- (void)setPresentStatus:(BOOL) status {
    [m_Condition lock];
    
    self.isGetPresent = status;
    
    [m_Condition unlock];
}

#pragma mark -
#pragma mark Server DI/DO Methods
- (void)forceOutPut:(int) index state:(BOOL) state {
    [m_Condition lock]; // jerrylu, 2012/07/12
    if (isRunning == NO) {
        [m_Condition unlock];
        return;
    }
    
    if (serverInfo.serverType == eServer_Crystal) {
        Control_DigitalOutput_Ext(m_hNuSDK, m_vecExtDODevice.at(index).ID, state);
    }
    else {
        Control_DigitalOutput(m_hNuSDK, m_vecDODevice.at(index).ID, state);
        //assert(result == Np_Result_OK);
    }
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
}

- (BOOL)getDOPrivilege:(int) index {
    DWORD privilege = 0;
    
    if (serverInfo.serverType == eServer_Crystal) {
        Info_GetDOPrivilegeExt(m_hNuSDK, m_vecExtDODevice[index].ID, privilege);
        
        int mask = 1;
        int result = (privilege & mask);
        if (result)
            return YES;
        else
            return NO;
    }
    else {
        int do_count = -1;
        
        for (int i=0; i<m_vecDIOMasterDevice.size(); i++) {
            for (int j=0; j<m_vecDIOMasterDevice[i].DODevices.size(); j++) {
                do_count++;
                if (do_count == index) {
                    Info_GetDOPrivilege(m_hNuSDK, m_vecDIOMasterDevice[i].ID, privilege);
                    int mask = (1 << j);
                    
                    //NSLog(@"!!!!! %d %d %d", privilege, mask, privilege&mask );
                    
                    int result = (privilege & mask);
                    if (result)
                        return YES;
                    else
                        return NO;
                }
            }
        }
        
        for (int i=0; i<m_vecCheckedDevice.size(); i++) {
            for (int j=0; j<m_vecCheckedDevice[i].DODevices.size(); j++) {
                do_count++;
                if (do_count == index) {
                    Info_GetDOPrivilege(m_hNuSDK, m_vecCheckedDevice[i].ID, privilege);
                    int mask = (1 << j);
                    
                    //NSLog(@"!!!!! %d %d %d", privilege, mask, privilege&mask);
                    
                    int result = (privilege & mask);
                    if (result)
                        return YES;
                    else
                        return NO;
                }
            }
        }
    }
    
    return NO;
}

- (BOOL)queryDOState:(int) index {
    [m_Condition lock]; // jerrylu, 2012/07/12
    if (isLogouting == YES) {
        [m_Condition unlock];
        return NO;
    }
    
    Np_DIOStatus status = kDIO_OFF;
    if (serverInfo.serverType == eServer_Crystal) {
        Info_GetDIOStatus_Ext(m_hNuSDK, m_vecExtDODevice.at(index).ID, status);
    }
    else {
        Info_GetDIOStatus(m_hNuSDK, m_vecDODevice.at(index).ID, status);
    }
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
    
    return status;
}

- (BOOL)queryDIState:(int) index {
    [m_Condition lock]; // jerrylu, 2012/07/12
    if (isLogouting == YES) {
        [m_Condition unlock];
        return NO;
    }
    
    Np_DIOStatus status = kDIO_OFF;
    if (serverInfo.serverType == eServer_Crystal) {
        Info_GetDIOStatus_Ext(m_hNuSDK, m_vecExtDIDevice.at(index).ID, status);
    }
    else {
        Info_GetDIOStatus(m_hNuSDK, m_vecDIDevice.at(index).ID, status);
    }
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
    
    return status;
}

- (void)forceOutPutOnCam:(int) index doIndex:(int) do_index state:(BOOL) state {
    [m_Condition lock];
    if (isRunning == NO) {
        [m_Condition unlock];
        return;
    }
    
    if (serverInfo.serverType == eServer_Crystal) {
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        Control_DigitalOutput_Ext(m_hNuSDK, cameradevice->DODevices.items[do_index].ID, state);
    }
    else {
        Control_DigitalOutput(m_hNuSDK, m_vecCheckedDevice.at(index).DODevices[do_index].ID, state);
    }
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
}

- (BOOL)queryDOStateOnCam:(int) index doIndex:(int) do_index {
    [m_Condition lock]; // jerrylu, 2012/07/12
    if (isLogouting == YES) {
        [m_Condition unlock];
        return NO;
    }
    
    Np_DIOStatus status = kDIO_OFF;
    if (serverInfo.serverType == eServer_Crystal) {
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        Info_GetDIOStatus_Ext(m_hNuSDK, cameradevice->DODevices.items[do_index].ID, status);
    }
    else {
        Info_GetDIOStatus(m_hNuSDK, m_vecCheckedDevice.at(index).DODevices[do_index].ID, status);
    }
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
    
    return status;
}

- (BOOL)queryDIStateOnCam:(int) index diIndex:(int) di_index {
    [m_Condition lock]; // jerrylu, 2012/07/12
    if (isLogouting == YES) {
        [m_Condition unlock];
        return NO;
    }
    
    Np_DIOStatus status = kDIO_OFF;
    if (serverInfo.serverType == eServer_Crystal) {
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        Info_GetDIOStatus_Ext(m_hNuSDK, cameradevice->DIDevices.items[di_index].ID, status);
    }
    else {
        Info_GetDIOStatus(m_hNuSDK, m_vecCheckedDevice.at(index).DIDevices[di_index].ID, status);
    }
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
    
    return status;
}

- (BOOL)getDOPrivilegeOnCam:(int) index doIndex:(int) do_index {
    DWORD privilege = 0;
    
    if (serverInfo.serverType == eServer_Crystal) {
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        Info_GetDOPrivilegeExt(m_hNuSDK, cameradevice->DODevices.items[do_index].ID, privilege);
        
        int mask = 1;
        int result = (privilege & mask);
        if (result)
            return YES;
        else
            return NO;
    }
    else {
        Info_GetDOPrivilege(m_hNuSDK, m_vecCheckedDevice[index].ID, privilege);
        
        int mask = (1 << do_index);
        int result = (privilege & mask);
        if (result)
            return YES;
        else
            return NO;
    }
    
    return NO;
}

-(NSMutableArray *)getDODeviceNameList {
    NSMutableArray *doNameList = [[[NSMutableArray alloc] init] autorelease];
    
    if (serverInfo.serverType == eServer_Crystal) {
        for (int i = 0; i < m_vecExtDODevice.size(); i++)
        {
            [doNameList addObject:WChartToNSString(m_vecExtDODevice.at(i).name)];
        }
    }
    else {
        for (int i = 0; i < m_vecDODevice.size(); i++)
        {
            [doNameList addObject:StringWToNSString(m_vecDODevice.at(i).name)];
        }
    }
    
    return doNameList;
}

-(NSMutableArray *)getDODeviceNameOnCam:(int) index {
    NSMutableArray *doNameList = [[[NSMutableArray alloc] init] autorelease];
    
    if (serverInfo.serverType == eServer_Crystal) {
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        for (int i = 0; i < cameradevice->DODevices.size; i++)
        {
            [doNameList addObject:WChartToNSString(cameradevice->DODevices.items[i].name)];
        }
    }
    else {
        for (int i = 0; i < m_vecCheckedDevice.at(index).DODevices.size(); i++)
        {
            [doNameList addObject:StringWToNSString(m_vecCheckedDevice.at(index).DODevices.at(i).name)];
        }
    }
    
    return doNameList;
}

-(NSMutableArray *)getDIDeviceNameList {
    NSMutableArray *diNameList = [[[NSMutableArray alloc] init] autorelease];
    
    if (serverInfo.serverType == eServer_Crystal) {
        for (int i = 0; i < m_vecExtDIDevice.size(); i++)
        {
            [diNameList addObject:WChartToNSString(m_vecExtDIDevice.at(i).name)];
        }
    }
    else {
        for (int i = 0; i < m_vecDIDevice.size(); i++)
        {
            [diNameList addObject:StringWToNSString(m_vecDIDevice.at(i).name)];
        }
    }
    
    return diNameList;
}

-(NSMutableArray *)getDIDeviceNameOnCam:(int) index {
    NSMutableArray *diNameList = [[[NSMutableArray alloc] init] autorelease];
    
    if (serverInfo.serverType == eServer_Crystal) {
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        for (int i = 0; i < cameradevice->DIDevices.size; i++)
        {
            [diNameList addObject:WChartToNSString(cameradevice->DIDevices.items[i].name)];
        }
    }
    else {
        for (int i = 0; i < m_vecCheckedDevice.at(index).DIDevices.size(); i++)
        {
            [diNameList addObject:StringWToNSString(m_vecCheckedDevice.at(index).DIDevices.at(i).name)];
        }
    }
    
    return diNameList;
}

#pragma mark -
#pragma mark Server Audio Methods

- (BOOL)getLiveViewAudioCapOnCam:(int) index {
    if (index >= [cameraHelperArray count]) {
#if DEBUG_LOG
        NSLog(@"[%s] Bad Index", __FUNCTION__);
#endif
		return NO;
	}
    
#if CUSTOMER_FOR_DLINK // jerrylu, 2012/12/26, fix bug11032, the Dlink's servertype will make we could't get any audio device
    if (serverInfo.serverType == eServer_MainConsole && m_vecCheckedDevice[index].AudioDevices.size() == 0) {
        return NO;
    }
    return YES;
#else
    if (serverInfo.serverType == eServer_Crystal) {
        Np_Device_Ext *cameradevice = [self searchExtDeviceBySensorDeviceIndex:index];
        if (cameradevice->AudioDevices.size == 0) {
            return NO;
        }
    }
    else if ((serverInfo.serverType == eServer_MainConsole ||
              serverInfo.serverType == eServer_NVRmini ||
              serverInfo.serverType == eServer_NVRsolo)) {
        if (m_vecCheckedDevice[index].AudioDevices.size() == 0)
            return NO;
    }
    return YES;
#endif
}

- (BOOL)getLiveViewAudioStatusOnCam:(int) index {
    if (index >= [cameraHelperArray count]) {
#if DEBUG_LOG
        NSLog(@"[%s] Bad Index", __FUNCTION__);
#endif
		return NO;
	}

	CameraHelper * obj = [cameraHelperArray objectAtIndex:index];
    return [obj getLiveViewAudioStatus];
}

- (void)setLiveviewAudioStateOnCam:(int) index state:(BOOL) state {
    if (index >= [cameraHelperArray count]) {
#if DEBUG_LOG
        NSLog(@"[%s] Bad Index", __FUNCTION__);
#endif
		return;
	}
    CameraHelper * obj = [cameraHelperArray objectAtIndex:index];
    [obj setLiveviewAudioState:state];
}

- (BOOL)getPlaybackAudioStatus {
    return playbackAudioEnable;
}

- (void)setPlaybackAudioState:(BOOL) state {
    playbackAudioEnable = state;
}

- (void)audioControllerHandleCallback:(AudioController *) controller RawData:(char *)data DataSize:(int)size {
    if (self.isLogouting == YES)
        return;
    if (m_hNuSDK == nil)
        return;
    
    Np_Result_t ret = Np_Result_OK;
    ret = Talk_SendAudioPacket(m_hNuSDK, data, size);
    
    if (ret != Np_Result_OK) {
        if (serverManagerDelegate != nil &&
           [serverManagerDelegate respondsToSelector:@selector(controlHelperDidTalkSessionError:)]) {
            [serverManagerDelegate controlHelperDidTalkSessionError:self];
        }
    }
}

- (BOOL)getTalkCapOnCam:(int) index {
    if (index >= [cameraHelperArray count]) {
#if DEBUG_LOG
        NSLog(@"[%s] Bad Index", __FUNCTION__);
#endif
		return NO;
	}
    
    if ((serverInfo.serverType == eServer_MainConsole ||
         serverInfo.serverType == eServer_NVRmini ||
         serverInfo.serverType == eServer_NVRsolo)) {
        long long device_cap = 0;
        if (Np_Result_OK == Info_GetDeviceCapability(m_hNuSDK, m_vecCheckedDevice[index].ID, device_cap)) {
            if (device_cap & kDeviceTalk)
                return YES;
            else
                return NO;
        }
    }
    return NO;
}

- (BOOL)setTalkStateOnCam:(int) index state:(BOOL) state audioFormat:(Np_TalkAudioFormat *) audiofmt {
    if (index >= [cameraHelperArray count]) {
#if DEBUG_LOG
        NSLog(@"[%s] Bad Index", __FUNCTION__);
#endif
		return NO;
	}
    
    BOOL ret = NO;
    
    if (state) {
        Np_Device cameradevice = m_vecCheckedDevice.at(index);
        Np_ID stID = cameradevice.SensorDevices[0].ID;
        
        if (Talk_Enable(m_hNuSDK, stID, *audiofmt) == Np_Result_OK) {
            ret = YES;
        }
    }
    else {
        Talk_Disable(m_hNuSDK);
        ret = YES;
    }
    return ret;
}

- (void)handelReservedTalk:(Np_Event)event {
    NSString *description = StringWToNSString(event.description);
    // "User:username"
    NSArray *components = [description componentsSeparatedByString:@":"];
    
    if (serverManagerDelegate != nil &&
        [serverManagerDelegate respondsToSelector:@selector(controlHelperDidTalkReserved:srv:usr:)]) {
        if (serverInfo.connectionType == eP2PConnection)
            [serverManagerDelegate controlHelperDidTalkReserved:self srv:serverInfo.serverName_P2P usr:[components objectAtIndex:1]];
        else
            [serverManagerDelegate controlHelperDidTalkReserved:self srv:serverInfo.serverName usr:[components objectAtIndex:1]];
    }
    
    [description release];
}

#pragma mark -
#pragma mark Server Playback Methods
// jerrylu, 2012/07/05
- (BOOL)getPlaybackConnection:(int) index {
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    if([camHelper isPlaybackConnectionCreated])
        return YES;
    else
        return NO;
}

// jerrylu, 2012/06/06
- (Np_Result_t)resetPbConnection:(int) index {
    Np_Result_t ret = Np_Result_OK;
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    
    /* just for Mainconsole Protocol */
    if (serverInfo.serverType == eServer_MainConsole || serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo) {
        // release previous session, player and handler
        [self disconnectAllPlaybackConnection];
        // wait for disconnection of this playback session
        while ([camHelper isPlaybackConnectionCreated]) {
            sleep(0.5);
        }
        
        if (m_hPBPlayer) {
            ret = PlayBack_DestroyPlayer(m_hPBPlayer);
            m_hPBPlayer = NULL;
        }
        
        if (m_hNuSDKPlayback) {
            ret = Destroy_Handle(m_hNuSDKPlayback);
            m_hNuSDKPlayback = NULL;
        }
        
        if (ret != Np_Result_OK) {
            return ret;
        }
        
        if (!m_hNuSDKPlayback) {
#if FUNC_P2P_SAT_SUPPORT // 2012/09/19
            if ([serverInfo connectionType] == eP2PConnection)
                ret |= Create_Handle(
                                     &m_hNuSDKPlayback,
                                     kMainConsolePlayback,
                                     NSStringToStringW([serverInfo userName_P2P]),
                                     NSStringToStringW([serverInfo userPassword_P2P]),
                                     NSStringToStringW([p2pServerInfo serverIP]),
                                     [[p2pServerInfo serverPlaybackPort] intValue]
                                     );
            else
#endif
            ret = Create_Handle(
                                &m_hNuSDKPlayback,
                                kMainConsolePlayback,
                                NSStringToStringW([serverInfo userName]),
                                NSStringToStringW([serverInfo userPassword]),
                                NSStringToStringW([serverInfo serverIP]),
                                [[serverInfo serverPlaybackPort] intValue]
                                );
        }
        
        if (m_hNuSDKPlayback) {
            ret |= Info_GetDeviceList(m_hNuSDKPlayback, m_pbDeviceList);
        }
        
        if (ret != Np_Result_OK) {
            Destroy_Handle(m_hNuSDKPlayback);
            m_hNuSDKPlayback = NULL;
            //playbackEnable = FALSE;
            
            return ret;
        }
        //playbackEnable = TRUE;
        
        if (m_hNuSDKPlayback && !m_hPBPlayer) {
            ret |= PlayBack_CreatePlayer(m_hNuSDKPlayback, &m_hPBPlayer);
        }
        
        std::vector<Np_Device>::iterator iterPbDevice;
        std::vector<Np_DeviceGroup>::iterator iterPbDeviceGroup;
        iterPbDeviceGroup = m_pbDeviceList.PhysicalGroup.begin();
        iterPbDevice = iterPbDeviceGroup->Camera.begin();
        for (int i = 0; i < cameraCount; i++) {
            CameraHelper *camHelper = [cameraHelperArray objectAtIndex:i];
            [camHelper setPbPlayer:m_hPBPlayer];
            
            int pbdevice_count = iterPbDeviceGroup->Camera.size();
            for (int j = 0; j < pbdevice_count; j++) {
                Np_Device lv_device = [camHelper getNpDevice];
                
                if (serverInfo.serverType == eServer_NVRmini)
                {
                    if (iterPbDevice[j].ID.centralID == lv_device.ID.localID - 10000) {
                        [camHelper setPbNpDevice:iterPbDevice[j]];
                    }
                }
                else {
                    if (iterPbDevice[j].ID.localID == lv_device.ID.localID) {
                        [camHelper setPbNpDevice:iterPbDevice[j]];
                    }
                }
            }
        }
    }
    
    return ret;
}

// Brosso - helper function
- (void)setTimeLineDelegate:(id)delegate {
    if (nil != delegate) {
        timelineDelegate = delegate;
        [(QueryRecordViewController*) timelineDelegate canQueryNewDate:NO];
    }
}

- (BOOL)checkPlaybackTimeByNSDate:(int) index startdate:(NSDate *) startdate enddate:(NSDate *) enddate {
    Np_DateTime starttime, endtime;
    memset(&starttime, 0, sizeof(Np_DateTime));
    memset(&endtime, 0, sizeof(Np_DateTime));
    
    NSDate2NpDateTime(startdate, &starttime);
    NSDate2NpDateTime(enddate, &endtime);
    
    return [self checkPlaybackTime:index startdate:&starttime enddate:&endtime];
}


- (BOOL)checkPlaybackTime:(int) index startdate:(Np_DateTime *) startdate enddate:(Np_DateTime *) enddate {
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
//    BOOL result=TRUE, isStartFound=FALSE, isEndFound=FALSE;
//    Np_Result_t ret;
//    Np_RecordLogListExt recodlist;
//    Np_ScheduleLogList scheduleLogList; // jerrylu, 2013/02/01, fix bug11855
    
    if (m_hNuSDKPlayback == NULL)
        return FALSE;
    
    Np_DateTime date;
    memset(&date, 0, sizeof(Np_DateTime));
    date.year = startdate->year;
    date.month = startdate->month;
    date.day = startdate->day;
    date.hour = 0;
    date.minute = 0;
    date.second = 0;
    
    Np_DateTime tmp_enddate;
    memset(&tmp_enddate, 0, sizeof(Np_DateTime));
    tmp_enddate.year = enddate->year;
    tmp_enddate.month = enddate->month;
    tmp_enddate.day = enddate->day;
    tmp_enddate.hour = 23;
    tmp_enddate.minute = 59;
    tmp_enddate.second = 59;
    
    
    // Switched to Info_QueryRecordLogsExt API - Brosso
    Np_ID_Ext thisID;
    memset(&thisID, 0, sizeof(thisID));
    thisID.centralID = 0;
    thisID.localID = 0;
    
    // Copy from previous code
    if (serverInfo.serverType == eServer_MainConsole) {
        Np_Device pbdevice = [camHelper getPbNpDevice];
        thisID.localID = (unsigned long long)pbdevice.ID.localID;
    }
    else if (serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo) {
        Np_Device pbdevice = [camHelper getPbNpDevice];
        thisID.localID = (unsigned long long)pbdevice.ID.centralID;
    }
    else if (serverInfo.serverType == eServer_Titan) {
        Np_Device pbdevice = [camHelper getPbNpDevice];
        thisID.localID = (unsigned long long)pbdevice.SensorDevices[0].ID.localID;
    }
    else {
        //Crystal server
        Np_SubDevice_Ext pbdevice = [camHelper getPbNpDeviceExt];
        thisID.centralID = pbdevice.ID.centralID;
        thisID.localID = pbdevice.ID.localID;
    }
    
    Np_IDList_Ext *idList = new Np_IDList_Ext();
    
    Np_ID_Ext* items = new Np_ID_Ext[1]();
    items[0] = thisID;
    
    idList->size = 1;
    idList->items = items;
    
    
//    NSLog(@"\n==========================\nstart Info_QueryRecordLogsExt\n\n==========================");
    Np_RecordLogListExt *recordLogList = new Np_RecordLogListExt();
    
    Np_Result_t ret = Info_QueryRecordLogsExt(m_hNuSDKPlayback, *idList, date, tmp_enddate, *recordLogList);
    
    // Brosso - generate timeline view
    NSMutableArray *eventArray = [NSMutableArray array];
    NSDateComponents *compStart = [[NSDateComponents alloc] init];
    NSDateComponents *compEnd = [[NSDateComponents alloc] init];
    
    if (Np_Result_OK == ret && recordLogList->size) {
        NSLog(@"retrived %d records", recordLogList->size);
        Np_RecordLogItemExt *record = recordLogList->logList;
        for (int i = 0; i < recordLogList->size; i++) {
            [compStart setYear:record[i].startTime.year];
            [compStart setMonth:record[i].startTime.month];
            [compStart setDay:record[i].startTime.day];
            [compStart setHour:record[i].startTime.hour];
            [compStart setMinute:record[i].startTime.minute];
            [compStart setSecond:record[i].startTime.second];
            [compEnd setYear:record[i].endTime.year];
            [compEnd setMonth:record[i].endTime.month];
            [compEnd setDay:record[i].endTime.day];
            [compEnd setHour:record[i].endTime.hour];
            [compEnd setMinute:record[i].endTime.minute];
            [compEnd setSecond:record[i].endTime.second];
            
            NSDate *start = [[NSCalendar currentCalendar] dateFromComponents:compStart];
            NSDate *end = [[NSCalendar currentCalendar] dateFromComponents:compEnd];
            
            QueryRecordEvents *queryEvent = [QueryRecordEvents eventWithStartDate:start andEndDate:end];
            if (nil != queryEvent) {
                [eventArray addObject:queryEvent];
            }
        }
        
        if (nil != timelineDelegate) {
            NSString *dateKey = [NSString stringWithFormat:@"%04d-%02d-%02d",startdate->year, startdate->month, startdate->day];
        
            dispatch_async(dispatch_get_main_queue(), ^{
                [(QueryRecordViewController*) timelineDelegate fetchEventsOnDate:dateKey withEvents:eventArray];
                // Render events
                [(QueryRecordViewController*) timelineDelegate renderEvents];
                
                // Allow query
                [(QueryRecordViewController*) timelineDelegate canQueryNewDate:YES];
            });
        }
    }
    else {
        if (Np_Result_OK == ret) {
            NSLog(@"retrived %d records", recordLogList->size);
        }
        
        if (nil != timelineDelegate) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Render events
                [(QueryRecordViewController*) timelineDelegate renderEvents];
                
                // Allow query
                [(QueryRecordViewController*) timelineDelegate canQueryNewDate:YES];
            });
        }
    }
    
    [compStart release];
    [compEnd release];
    
    delete recordLogList;
    delete [] items;
    delete idList;
    
    return (Np_Result_OK == ret && recordLogList->size );
    
//    ret = Info_GetRecordLogsExt(m_hNuSDKPlayback, date, recodlist);
//    // jerrylu, 2013/02/01, fix bug11855
//    if (serverInfo.serverType == eServer_MainConsole || serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo)
//        ret |= Info_GetScheduleLogs(m_hNuSDKPlayback, date, scheduleLogList);
//    
//    if (ret == Np_Result_OK) {
//        // jerrylu, 2013/02/01, fix bug11879, search seconds
//        long start_second = NpDateTime2TimeInterval1970(startdate);
//        long end_second = NpDateTime2TimeInterval1970(enddate);
//        
//        unsigned long long centralID = 0;
//        unsigned long long localID = 0;
//        
//        if (serverInfo.serverType == eServer_MainConsole) {
//            Np_Device pbdevice = [camHelper getPbNpDevice];
//            localID = (unsigned long long)pbdevice.ID.localID;
//        }
//        else if (serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo) {
//            Np_Device pbdevice = [camHelper getPbNpDevice];
//            localID = (unsigned long long)pbdevice.ID.centralID;
//        }
//        else if (serverInfo.serverType == eServer_Titan) {
//            Np_Device pbdevice = [camHelper getPbNpDevice];
//            localID = (unsigned long long)pbdevice.SensorDevices[0].ID.localID;
//        }
//        else {
//            //Crystal server
//            Np_SubDevice_Ext pbdevice = [camHelper getPbNpDeviceExt];
//            centralID = pbdevice.ID.centralID;
//            localID = pbdevice.ID.localID;
//        }
//        
//        for(int i=0; i< recodlist.size; i++)
//        {
//            Np_RecordLogItemExt *record = &(recodlist.logList[i]);
//            
//            if ((localID == record->ID.localID &&
//                (serverInfo.serverType == eServer_MainConsole)) ||
//                (centralID == record->ID.centralID &&
//                 localID == record->ID.localID &&
//                 (serverInfo.serverType == eServer_Titan ||
//                 serverInfo.serverType == eServer_Crystal)) ||
//                (localID == record->ID.centralID &&
//                (serverInfo.serverType == eServer_NVRmini ||
//                 serverInfo.serverType == eServer_NVRsolo))
//                ) {
//                long record_start_sec = NpDateTime2TimeInterval1970(&record->startTime);
//                long record_end_sec = NpDateTime2TimeInterval1970(&record->endTime);
//                
//                if (!isStartFound) {
//                    if (start_second >= record_start_sec && start_second <= record_end_sec) {
//                        if (serverInfo.serverType == eServer_MainConsole ||
//                            serverInfo.serverType == eServer_NVRmini ||
//                            serverInfo.serverType == eServer_NVRsolo) {
//                            BOOL isOnSchedule = NO;
//                            for(int j=0; j < scheduleLogList.size; j++) {
//                                Np_ScheduleLogItem *scheduleLog = &(scheduleLogList.logList[j]);
//                                
//                                if ((localID == (unsigned long long)scheduleLog->ID.localID &&
//                                     serverInfo.serverType == eServer_MainConsole) ||
//                                    (localID == (unsigned long long)scheduleLog->ID.centralID &&
//                                     (serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo))
//                                    ) {
//                                    long scheduleLog_start_sec = NpDateTime2TimeInterval1970(&scheduleLog->startTime);
//                                    long scheduleLog_end_sec = NpDateTime2TimeInterval1970(&scheduleLog->endTime);
//                                    
//                                    if (start_second >= scheduleLog_start_sec && start_second <= scheduleLog_end_sec)
//                                    {
//                                        isOnSchedule = YES;
//                                        break;
//                                    }
//                                    else if (scheduleLog_start_sec >= start_second && scheduleLog_start_sec <= end_second) {
//                                        startdate->hour = scheduleLog->startTime.hour;
//                                        startdate->minute = scheduleLog->startTime.minute;
//                                        startdate->second = scheduleLog->startTime.second;
//                                        startdate->millisecond = scheduleLog->startTime.millisecond;
//                                        isOnSchedule = YES;
//                                        break;
//                                    }
//                                }
//                            }
//                            if (isOnSchedule) {
//                                isStartFound = TRUE;
//                            }
//                        }
//                        else {
//                            // Titan server NO scheduling Log
//                            isStartFound = TRUE;
//                        }
//                    }
//                    else if (record_start_sec >= start_second && record_start_sec <= end_second) {
//                        startdate->hour = record->startTime.hour;
//                        startdate->minute = record->startTime.minute;
//                        startdate->second = record->startTime.second;
//                        startdate->millisecond = record->startTime.millisecond;
//                        
//                        if (serverInfo.serverType == eServer_MainConsole ||
//                            serverInfo.serverType == eServer_NVRmini ||
//                            serverInfo.serverType == eServer_NVRsolo) {
//                            BOOL isOnSchedule = NO;
//                            for(int j=0; j < scheduleLogList.size; j++) {
//                                Np_ScheduleLogItem *scheduleLog = &(scheduleLogList.logList[j]);
//                                
//                                if ((localID == (unsigned long long)scheduleLog->ID.localID &&
//                                     serverInfo.serverType == eServer_MainConsole) ||
//                                    (localID == (unsigned long long)scheduleLog->ID.centralID &&
//                                     (serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo))
//                                    ) {
//                                    long scheduleLog_start_sec = NpDateTime2TimeInterval1970(&scheduleLog->startTime);
//                                    long scheduleLog_end_sec = NpDateTime2TimeInterval1970(&scheduleLog->endTime);
//                                    
//                                    if (start_second >= scheduleLog_start_sec && start_second <= scheduleLog_end_sec)
//                                    {
//                                        isOnSchedule = YES;
//                                        break;
//                                    }
//                                    else if (scheduleLog_start_sec >= start_second && scheduleLog_start_sec <= end_second) {
//                                        startdate->hour = scheduleLog->startTime.hour;
//                                        startdate->minute = scheduleLog->startTime.minute;
//                                        startdate->second = scheduleLog->startTime.second;
//                                        startdate->millisecond = scheduleLog->startTime.millisecond;
//                                        isOnSchedule = YES;
//                                        break;
//                                    }
//                                }
//                            }
//                            if (isOnSchedule) {
//                                isStartFound = TRUE;
//                            }
//                        }
//                        else {
//                            // Titan server NO scheduling Log
//                            isStartFound = TRUE;
//                        }
//                    }
//                }
//                
//                
//                if (!isEndFound) {
//                    if (end_second >= record_start_sec && end_second <= record_end_sec) {
//                        isEndFound = TRUE;
//                        tmp_enddate.hour = enddate->hour;
//                        tmp_enddate.minute = enddate->minute;
//                        tmp_enddate.second = enddate->second;
//                        tmp_enddate.millisecond = enddate->millisecond;
//                    }
//                    else if (start_second <= record_end_sec && record_end_sec <= end_second) {
//                        tmp_enddate.hour = record->endTime.hour;
//                        tmp_enddate.minute = record->endTime.minute;
//                        tmp_enddate.second = record->endTime.second;
//                        tmp_enddate.millisecond = record->endTime.millisecond;
//                    }
//                }
//                
//                if (isStartFound && isEndFound) {
//                    break;
//                }
//            }
//        }
//        
//        if (!isStartFound) {
//            if (serverInfo.serverType == eServer_Titan || serverInfo.serverType == eServer_Crystal) {
//                if (serverManagerDelegate != nil) {
//                    if ([serverManagerDelegate respondsToSelector:@selector(controlHelperAlertPlaybackEvent:camIndex:event:)]) {
//                        [serverManagerDelegate controlHelperAlertPlaybackEvent:self camIndex:index event:ePlayback_NoData];
//                    }
//                }
//            }
//            result = FALSE;
//        }
//        else {
//            if (!isEndFound) {
//                enddate->hour = tmp_enddate.hour;
//                enddate->minute = tmp_enddate.minute;
//                enddate->second = tmp_enddate.second;
//                enddate->millisecond = tmp_enddate.millisecond;
//            }
//        }
//        
//        Info_ReleaseRecordLogsExt(m_hNuSDKPlayback, recodlist);
//        // jerrylu, 2013/02/01, fix bug11855
//        if (serverInfo.serverType == eServer_MainConsole || serverInfo.serverType == eServer_NVRmini || serverInfo.serverType == eServer_NVRsolo)
//            Info_ReleaseScheduleLogs(m_hNuSDKPlayback, scheduleLogList);
//    }
//    else {
//        NSLog(@"[%s] Info_GetRecordLogs Failed", __FUNCTION__);
//        if (serverInfo.serverType == eServer_Titan || serverInfo.serverType == eServer_Crystal) { // jerrylu, 2012/11/29, fix bug7531
//            if (serverManagerDelegate != nil) {
//                if ([serverManagerDelegate respondsToSelector:@selector(controlHelperAlertPlaybackEvent:camIndex:event:)]) {
//                    [serverManagerDelegate controlHelperAlertPlaybackEvent:self camIndex:index event:ePlayback_ConnectError];
//                }
//            }
//        }
//        result = FALSE;
//    }
//    
//    return result;
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (BOOL)checkPlaybackTime:(int) index recordfile:(EDualRecordFile) rf_index startdate:(Np_DateTime *) startdate enddate:(Np_DateTime *) enddate {
    
    // Brosso - Info_QueryRecordLogsExt
    return [self checkPlaybackTime:index startdate:startdate enddate:enddate];
    
    
    
    // No use
    if (m_hNuSDKPlayback == NULL)
        return FALSE;
    
    if (   serverInfo.serverType != eServer_MainConsole
        && serverInfo.serverType != eServer_NVRmini
        && serverInfo.serverType != eServer_NVRsolo)
    {
        return FALSE;
    }
    
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    BOOL result=TRUE, isStartFound=FALSE, isEndFound=FALSE;
    Np_Result_t ret;
    
    Np_DateTime date;
    memset(&date, 0, sizeof(Np_DateTime));
    date.year = startdate->year;
    date.month = startdate->month;
    date.day = startdate->day;
    
    Np_DateTime tmp_enddate;
    memset(&tmp_enddate, 0, sizeof(Np_DateTime));
    tmp_enddate.year = enddate->year;
    tmp_enddate.month = enddate->month;
    tmp_enddate.day = enddate->day;
    
    Info_ReleaseRecordLogsListExt(m_hNuSDKPlayback, m_recordLogListList);
    ret = Info_GetRecordLogsListExt(m_hNuSDKPlayback, date, m_recordLogListList);
    Info_ReleaseScheduleLogsList(m_hNuSDKPlayback, m_scheduleLogListList);
    ret |= Info_GetScheduleLogsList(m_hNuSDKPlayback, date, m_scheduleLogListList);
    
    if (ret == Np_Result_OK && m_recordLogListList.size > 0 && m_scheduleLogListList.size > 0) {
        // jerrylu, 2013/02/01, fix bug11879, search seconds
        long start_second = NpDateTime2TimeInterval1970(startdate);
        long end_second = NpDateTime2TimeInterval1970(enddate);
        
        unsigned long long localID = 0;
        Np_Device pbdevice = [camHelper getPbNpDevice];
        localID = (unsigned long long)pbdevice.ID.localID;
        
        Np_RecordLogListExt *recordList = &(m_recordLogListList.logListList[rf_index]);
        Np_ScheduleLogList *scheduleLogList = &(m_scheduleLogListList.logListList[rf_index]);

        for(int i=0; i< recordList->size; i++)
        {
            Np_RecordLogItemExt *record = &(recordList->logList[i]);
            if (localID == record->ID.localID) {
                long record_start_sec = NpDateTime2TimeInterval1970(&record->startTime);
                long record_end_sec = NpDateTime2TimeInterval1970(&record->endTime);
                
                if (!isStartFound) {
                    if (start_second >= record_start_sec && start_second <= record_end_sec) {
                        BOOL isOnSchedule = NO;
                        
                        for(int j=0; j < scheduleLogList->size; j++) {
                            Np_ScheduleLogItem *scheduleLog = &(scheduleLogList->logList[j]);
                            
                            if (localID == (unsigned long long)scheduleLog->ID.localID) {
                                long scheduleLog_start_sec = NpDateTime2TimeInterval1970(&scheduleLog->startTime);
                                long scheduleLog_end_sec = NpDateTime2TimeInterval1970(&scheduleLog->endTime);
                                
                                if (start_second >= scheduleLog_start_sec && start_second <= scheduleLog_end_sec)
                                {
                                    isOnSchedule = YES;
                                    break;
                                }
                                else if (scheduleLog_start_sec >= start_second && scheduleLog_start_sec <= end_second) {
                                    startdate->hour = scheduleLog->startTime.hour;
                                    startdate->minute = scheduleLog->startTime.minute;
                                    startdate->second = scheduleLog->startTime.second;
                                    startdate->millisecond = scheduleLog->startTime.millisecond;
                                    isOnSchedule = YES;
                                    break;
                                }
                            }
                        }
                        if (isOnSchedule) {
                            isStartFound = TRUE;
                        }
                    }
                    else if (record_start_sec >= start_second && record_start_sec <= end_second) {
                        startdate->hour = record->startTime.hour;
                        startdate->minute = record->startTime.minute;
                        startdate->second = record->startTime.second;
                        startdate->millisecond = record->startTime.millisecond;
                        
                        BOOL isOnSchedule = NO;
                        for(int j=0; j < scheduleLogList->size; j++) {
                            Np_ScheduleLogItem *scheduleLog = &(scheduleLogList->logList[j]);
                            
                            if (localID == (unsigned long long)scheduleLog->ID.localID) {
                                long scheduleLog_start_sec = NpDateTime2TimeInterval1970(&scheduleLog->startTime);
                                long scheduleLog_end_sec = NpDateTime2TimeInterval1970(&scheduleLog->endTime);
                                
                                if (start_second >= scheduleLog_start_sec && start_second <= scheduleLog_end_sec)
                                {
                                    isOnSchedule = YES;
                                    break;
                                }
                                else if (scheduleLog_start_sec >= start_second && scheduleLog_start_sec <= end_second) {
                                    startdate->hour = scheduleLog->startTime.hour;
                                    startdate->minute = scheduleLog->startTime.minute;
                                    startdate->second = scheduleLog->startTime.second;
                                    startdate->millisecond = scheduleLog->startTime.millisecond;
                                    isOnSchedule = YES;
                                    break;
                                }
                            }
                        }
                        if (isOnSchedule) {
                            isStartFound = TRUE;
                        }
                    }
                }
                
                if (!isEndFound) {
                    if (end_second >= record_start_sec && end_second <= record_end_sec) {
                        isEndFound = TRUE;
                        tmp_enddate.hour = enddate->hour;
                        tmp_enddate.minute = enddate->minute;
                        tmp_enddate.second = enddate->second;
                        tmp_enddate.millisecond = enddate->millisecond;
                    }
                    else if (start_second <= record_end_sec && record_end_sec <= end_second) {
                        tmp_enddate.hour = record->endTime.hour;
                        tmp_enddate.minute = record->endTime.minute;
                        tmp_enddate.second = record->endTime.second;
                        tmp_enddate.millisecond = record->endTime.millisecond;
                    }
                }
                
                if (isStartFound && isEndFound) {
                    break;
                }
            }
        }
                
        if (!isStartFound) {
            result = FALSE;
        }
        else {
            if (!isEndFound) {
                enddate->hour = tmp_enddate.hour;
                enddate->minute = tmp_enddate.minute;
                enddate->second = tmp_enddate.second;
                enddate->millisecond = tmp_enddate.millisecond;
            }
        }
    }
    else {
        NSLog(@"[%s] Info_GetRecordLogsListExt Failed", __FUNCTION__);
        result = FALSE;
    }
    
    return result;
}

- (BOOL)checkPlaybackLogTime:(int) ch_index recordfile:(EDualRecordFile) rf_index datetime:(Np_DateTime *) datetime {
    if (m_hNuSDKPlayback == NULL)
        return NO;
    
    if (   serverInfo.serverType != eServer_MainConsole
        && serverInfo.serverType != eServer_NVRmini
        && serverInfo.serverType != eServer_NVRsolo)
    {
        return NO;
    }
    
    if (m_recordLogListList.size <= 0 || m_scheduleLogListList.size <= 0)
        return NO;
    
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:ch_index];
    unsigned long long localID = 0;
    Np_Device pbdevice = [camHelper getPbNpDevice];
    localID = (unsigned long long)pbdevice.ID.localID;
    
    long target_second = NpDateTime2TimeInterval1970(datetime);
    
    Np_RecordLogListExt *recordList = &(m_recordLogListList.logListList[rf_index]);
    for(int i=0; i< recordList->size; i++)
    {
        Np_RecordLogItemExt *record = &(recordList->logList[i]);
        if (localID == record->ID.localID) {
            long record_start_sec = NpDateTime2TimeInterval1970(&record->startTime);
            long record_end_sec = NpDateTime2TimeInterval1970(&record->endTime);
            
            if (target_second >= record_start_sec && target_second <= record_end_sec) {
                Np_ScheduleLogList *scheduleLogList = &(m_scheduleLogListList.logListList[rf_index]);
                for(int j=0; j < scheduleLogList->size; j++) {
                    Np_ScheduleLogItem *scheduleLog = &(scheduleLogList->logList[j]);
                    
                    if (localID == (unsigned long long)scheduleLog->ID.localID) {
                        long scheduleLog_start_sec = NpDateTime2TimeInterval1970(&scheduleLog->startTime);
                        long scheduleLog_end_sec = NpDateTime2TimeInterval1970(&scheduleLog->endTime);
                        
                        if (target_second >= scheduleLog_start_sec && target_second <= scheduleLog_end_sec)
                        {
                            return YES;
                        }
                    }
                }
            }
        }
    }
    return NO;
}

- (BOOL)checkPlaybackLogTime:(int) ch_index recordfile:(EDualRecordFile) rf_index starttime:(Np_DateTime *) starttime endtime:(Np_DateTime *)endtime {
    if (m_hNuSDKPlayback == NULL)
        return NO;
    
    if (   serverInfo.serverType != eServer_MainConsole
        && serverInfo.serverType != eServer_NVRmini
        && serverInfo.serverType != eServer_NVRsolo)
    {
        return NO;
    }
    
    if (m_recordLogListList.size <= 0 || m_scheduleLogListList.size <= 0)
        return NO;
    
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:ch_index];
    unsigned long long localID = 0;
    Np_Device pbdevice = [camHelper getPbNpDevice];
    localID = (unsigned long long)pbdevice.ID.localID;
    
    long start_second = NpDateTime2TimeInterval1970(starttime);
    long end_second = NpDateTime2TimeInterval1970(endtime);
    
    Np_RecordLogListExt *recordList = &(m_recordLogListList.logListList[rf_index]);
    Np_ScheduleLogList *scheduleLogList = &(m_scheduleLogListList.logListList[rf_index]);
    
    BOOL isStartFound = NO, isEndFound = NO;
    Np_DateTime tmp_enddate;
    memset(&tmp_enddate, 0, sizeof(Np_DateTime));
    tmp_enddate.year = endtime->year;
    tmp_enddate.month = endtime->month;
    tmp_enddate.day = endtime->day;
    
    for(int i=0; i< recordList->size; i++)
    {
        Np_RecordLogItemExt *record = &(recordList->logList[i]);
        if (localID == record->ID.localID) {
            long record_start_sec = NpDateTime2TimeInterval1970(&record->startTime);
            long record_end_sec = NpDateTime2TimeInterval1970(&record->endTime);
            
            if (!isStartFound) {
                if (start_second >= record_start_sec && start_second <= record_end_sec) {
                    BOOL isOnSchedule = NO;
                    
                    for(int j=0; j < scheduleLogList->size; j++) {
                        Np_ScheduleLogItem *scheduleLog = &(scheduleLogList->logList[j]);
                        
                        if (localID == (unsigned long long)scheduleLog->ID.localID) {
                            long scheduleLog_start_sec = NpDateTime2TimeInterval1970(&scheduleLog->startTime);
                            long scheduleLog_end_sec = NpDateTime2TimeInterval1970(&scheduleLog->endTime);
                            
                            if (start_second >= scheduleLog_start_sec && start_second <= scheduleLog_end_sec)
                            {
                                isOnSchedule = YES;
                                break;
                            }
                            else if (scheduleLog_start_sec >= start_second && scheduleLog_start_sec <= end_second) {
                                starttime->hour = scheduleLog->startTime.hour;
                                starttime->minute = scheduleLog->startTime.minute;
                                starttime->second = scheduleLog->startTime.second;
                                starttime->millisecond = scheduleLog->startTime.millisecond;
                                
                                isOnSchedule = YES;
                                break;
                            }
                        }
                    }
                    if (isOnSchedule) {
                        isStartFound = YES;
                    }
                }
                else if (record_start_sec >= start_second && record_start_sec <= end_second) {
                    starttime->hour = record->startTime.hour;
                    starttime->minute = record->startTime.minute;
                    starttime->second = record->startTime.second;
                    starttime->millisecond = record->startTime.millisecond;
                    
                    BOOL isOnSchedule = NO;
                    for(int j=0; j < scheduleLogList->size; j++) {
                        Np_ScheduleLogItem *scheduleLog = &(scheduleLogList->logList[j]);
                        
                        if (localID == (unsigned long long)scheduleLog->ID.localID) {
                            long scheduleLog_start_sec = NpDateTime2TimeInterval1970(&scheduleLog->startTime);
                            long scheduleLog_end_sec = NpDateTime2TimeInterval1970(&scheduleLog->endTime);
                            
                            if (start_second >= scheduleLog_start_sec && start_second <= scheduleLog_end_sec)
                            {
                                isOnSchedule = YES;
                                break;
                            }
                            else if (scheduleLog_start_sec >= start_second && scheduleLog_start_sec <= end_second) {
                                starttime->hour = scheduleLog->startTime.hour;
                                starttime->minute = scheduleLog->startTime.minute;
                                starttime->second = scheduleLog->startTime.second;
                                starttime->millisecond = scheduleLog->startTime.millisecond;
                                
                                isOnSchedule = YES;
                                break;
                            }
                        }
                    }
                    if (isOnSchedule) {
                        isStartFound = YES;
                    }
                }
            }
            
            if (!isEndFound) {
                if (end_second >= record_start_sec && end_second <= record_end_sec) {
                    isEndFound = YES;
                    tmp_enddate.hour = endtime->hour;
                    tmp_enddate.minute = endtime->minute;
                    tmp_enddate.second = endtime->second;
                    tmp_enddate.millisecond = endtime->millisecond;
                }
                else if (start_second <= record_end_sec && record_end_sec <= end_second) {
                    tmp_enddate.hour = record->endTime.hour;
                    tmp_enddate.minute = record->endTime.minute;
                    tmp_enddate.second = record->endTime.second;
                    tmp_enddate.millisecond = record->endTime.millisecond;
                }
            }
            
            if (isStartFound && isEndFound) {
                break;
            }
        }
    }
    
    if (isStartFound) {
        if (!isEndFound) {
            endtime->hour = tmp_enddate.hour;
            endtime->minute = tmp_enddate.minute;
            endtime->second = tmp_enddate.second;
            endtime->millisecond = tmp_enddate.millisecond;
        }
        
        return YES;
    }
    
    return NO;
}
#endif
- (void)disconnectAllPlaybackConnection {
	for (CameraHelper * camHelper in cameraHelperArray) {
		if ([camHelper respondsToSelector:@selector(playbackDisconnect)]) {
			[camHelper playbackDisconnect];
		}
	}
}

- (void)checkPbConnection {
    while (TRUE) {
        [NSThread sleepForTimeInterval:1];
        for (CameraHelper * camHelper in cameraHelperArray) {
            if ([camHelper respondsToSelector:@selector(checkPlaybackConnection)]) {
                [camHelper checkPlaybackConnection];
            }
            
            if ([[NSThread currentThread] isCancelled]) {
                [NSThread exit];
                return;
            }
        }
        
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
    }
}

- (BOOL)playbackStartFromDate:(int) index startdate:(NSDate *) startdate enddate:(NSDate *) enddate {
    // jerrylu, 2012/06/29
    [m_Condition lock]; // jerrylu, 2012/07/12
    if (self.isLogouting == YES) {
        [m_Condition unlock];
        return FALSE;
    }
    
    Np_DateTime starttime, endtime;
    memset(&starttime, 0, sizeof(Np_DateTime));
    memset(&endtime, 0, sizeof(Np_DateTime));

    if (index >= [cameraHelperArray count]) {
        [m_Condition unlock]; // jerrylu, 2012/07/12
        return FALSE;
    }
    
    // jerrylu, 2012/10/03, fix bug7764 retry the connection twice
    if ([self resetPbConnection:index] != Np_Result_OK) {
        [NSThread sleepForTimeInterval:2];
        if ([self resetPbConnection:index] != Np_Result_OK) {
            [m_Condition unlock]; // jerrylu, 2012/07/12
            return FALSE;
        }
    }
    
    NSDate2NpDateTime(startdate, &starttime);
    NSDate2NpDateTime(enddate, &endtime);
    if (starttime.minute > 0) starttime.minute -= 1;
    if (endtime.minute > 0) endtime.minute -= 1;
    starttime.millisecond = endtime.millisecond = 0;

    if(![self checkPlaybackTime:index startdate:&starttime enddate:&endtime]) {
        [m_Condition unlock]; // jerrylu, 2012/07/12
        return FALSE;
    }
    else { // jerrylu, 2013/02/01, fix bug11855, shift the start time
        if (serverManagerDelegate != nil) {
            if ([serverManagerDelegate respondsToSelector:@selector(controlHelperRefreshPlaybackTime:starttime:endtime:)]) {
                [serverManagerDelegate controlHelperRefreshPlaybackTime:self starttime:&starttime endtime:&endtime];
            }
        }
    }
    
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    [camHelper liveViewDisconnect]; //disconnect liveview session
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (   serverInfo.serverType == eServer_MainConsole
        || serverInfo.serverType == eServer_NVRmini
        || serverInfo.serverType == eServer_NVRsolo) {
        [camHelper setERecordFile:eRecordFile1];
    }
#endif
    [camHelper playbackConnect]; // just connect one playback session
    
    int timer = 0;
    while (![camHelper isPlaybackConnectionCreated]) {
        sleep(0.5);

        // Wait most 10(20*0.5s) seconds for session created
        //if (timer >= 20)
        //    break;
        timer++;
    }
    
    if ([self playbackOpenRecord:starttime enddate:endtime] == NO) {
        [m_Condition unlock]; // jerrylu, 2012/07/12
        return FALSE;
    }
        
    [m_Condition unlock]; // jerrylu, 2012/07/12
    
    return TRUE;
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (BOOL)playbackStartFromDate:(int) index recordfile:(EDualRecordFile) recordfile startdate:(NSDate *) startdate enddate:(NSDate *) enddate {
    [m_Condition lock];
    if (self.isLogouting == YES) {
        [m_Condition unlock];
        return FALSE;
    }
    
    Np_DateTime starttime, endtime;
    memset(&starttime, 0, sizeof(Np_DateTime));
    memset(&endtime, 0, sizeof(Np_DateTime));
    
    if (index >= [cameraHelperArray count]) {
        [m_Condition unlock];
        return FALSE;
    }
    
    if ([self resetPbConnection:index] != Np_Result_OK) {
        [NSThread sleepForTimeInterval:2];
        if ([self resetPbConnection:index] != Np_Result_OK) {
            [m_Condition unlock]; // jerrylu, 2012/07/12
            return FALSE;
        }
    }
  
    NSDate2NpDateTime(startdate, &starttime);
    NSDate2NpDateTime(enddate, &endtime);
    starttime.millisecond = endtime.millisecond = 0;
    
    if (   serverInfo.serverType == eServer_MainConsole
        || serverInfo.serverType == eServer_NVRmini
        || serverInfo.serverType == eServer_NVRsolo) {
        if(![self checkPlaybackTime:index recordfile:recordfile startdate:&starttime enddate:&endtime]) {
            [m_Condition unlock]; // jerrylu, 2012/07/12
            return FALSE;
        }
        else { // jerrylu, 2013/02/01, fix bug11855, shift the start time
            if (serverManagerDelegate != nil) {
                if ([serverManagerDelegate respondsToSelector:@selector(controlHelperRefreshPlaybackTime:recordfile:startTime:endTime:)]) {
                    [serverManagerDelegate controlHelperRefreshPlaybackTime:self recordfile:recordfile startTime:&starttime endTime:&endtime];
                }
            }
            
            Np_DateTime otherStarttime, otherEndtime;
            memset(&otherStarttime, 0, sizeof(Np_DateTime));
            memset(&otherEndtime, 0, sizeof(Np_DateTime));
            NSDate2NpDateTime(startdate, &otherStarttime);
            NSDate2NpDateTime(enddate, &otherEndtime);
            otherStarttime.millisecond = otherEndtime.millisecond = 0;
            
            EDualRecordFile otherRecordFile = (recordfile == eRecordFile1)? eRecordFile2 : eRecordFile1;
            if ([self checkPlaybackLogTime:index recordfile:otherRecordFile starttime:&otherStarttime endtime:&otherEndtime]) {
                if (serverManagerDelegate != nil) {
                    if ([serverManagerDelegate respondsToSelector:@selector(controlHelperSupportDualRecordPlayback:camIndex:)]) {
                        [serverManagerDelegate controlHelperSupportDualRecordPlayback:self camIndex:index];
                    }
                    
                    if ([serverManagerDelegate respondsToSelector:@selector(controlHelperRefreshPlaybackTime:recordfile:startTime:endTime:)]) {
                        [serverManagerDelegate controlHelperRefreshPlaybackTime:self recordfile:otherRecordFile startTime:&otherStarttime endTime:&otherEndtime];
                    }
                }
            }
        }
    }
    else {
        if(![self checkPlaybackTime:index startdate:&starttime enddate:&endtime]) {
            [m_Condition unlock]; // jerrylu, 2012/07/12
            return FALSE;
        }
        else { // jerrylu, 2013/02/01, fix bug11855, shift the start time
            if (serverManagerDelegate != nil) {
                if ([serverManagerDelegate respondsToSelector:@selector(controlHelperRefreshPlaybackTime:starttime:endtime:)]) {
                    [serverManagerDelegate controlHelperRefreshPlaybackTime:self starttime:&starttime endtime:&endtime];
                }
            }
        }
    }
    
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    [camHelper liveViewDisconnect]; //disconnect liveview session
    if (   serverInfo.serverType == eServer_MainConsole
        || serverInfo.serverType == eServer_NVRsolo
        || serverInfo.serverType == eServer_NVRmini) {
        [camHelper setERecordFile:recordfile];
    }
    [camHelper playbackConnect]; // just connect one playback session
    
    int timer = 0;
    while (![camHelper isPlaybackConnectionCreated]) {
        sleep(0.5);
        
        // Wait most 10(20*0.5s) seconds for session created
        //if (timer >= 20)
        //    break;
        timer++;
    }
    
    if ([self playbackOpenRecord:starttime enddate:endtime] == NO) {
        [m_Condition unlock]; // jerrylu, 2012/07/12
        return FALSE;
    }
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
    
    return TRUE;
}
#endif
- (BOOL)playbackStartFromDateByEvent:(int) index startdate:(NSDate *) startdate enddate:(NSDate *) enddate {
    // jerrylu, 2012/06/29
    [m_Condition lock]; // jerrylu, 2012/07/12
    if (self.isLogouting == YES) {
        [m_Condition unlock];
        return FALSE;
    }
    
    Np_DateTime starttime, endtime;
    memset(&starttime, 0, sizeof(Np_DateTime));
    memset(&endtime, 0, sizeof(Np_DateTime));
    
    if (index >= [cameraHelperArray count]) {
        [m_Condition unlock]; // jerrylu, 2012/07/12
        return FALSE;
    }
    
    if ([self resetPbConnection:index] != Np_Result_OK) {
        [m_Condition unlock]; // jerrylu, 2012/07/12
        return FALSE;
    }
    
    NSDate2NpDateTime(startdate, &starttime);
    NSDate2NpDateTime(enddate, &endtime);
    starttime.millisecond = endtime.millisecond = 0;
    
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (   serverInfo.serverType == eServer_MainConsole
        || serverInfo.serverType == eServer_NVRmini
        || serverInfo.serverType == eServer_NVRsolo) {
        [camHelper setERecordFile:eRecordFile2];
        [camHelper playbackConnect]; // just connect one playback session
        while (![camHelper isPlaybackConnectionCreated]) {
            sleep(0.5);
        }
        
        if ([self playbackOpenRecord:starttime enddate:endtime] == NO) {
            [m_Condition unlock]; // jerrylu, 2012/07/12
            return FALSE;
        }
        
        [camHelper playbackDisconnect];
        while ([camHelper isPlaybackConnectionCreated]) {
            sleep(0.5);
        }
        
        [camHelper setERecordFile:eRecordFile1];
        [camHelper playbackConnect];
        while (![camHelper isPlaybackConnectionCreated]) {
            sleep(0.5);
        }
        
        if ([self playbackOpenRecord:starttime enddate:endtime] == NO) {
            [m_Condition unlock]; // jerrylu, 2012/07/12
            return FALSE;
        }
        
        [camHelper playbackDisconnect];
        while ([camHelper isPlaybackConnectionCreated]) {
            sleep(0.5);
        }
        
    }
    else {
#endif
        [camHelper playbackConnect]; // just connect one playback session
        
        while (![camHelper isPlaybackConnectionCreated]) {
            sleep(0.5);
        }
        
        if ([self playbackOpenRecord:starttime enddate:endtime] == NO) {
            [m_Condition unlock]; // jerrylu, 2012/07/12
            return FALSE;
        }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    }
#endif
    sleep(2);
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (   serverInfo.serverType == eServer_MainConsole
        || serverInfo.serverType == eServer_NVRmini
        || serverInfo.serverType == eServer_NVRsolo) {
        // EventView: Default => 2nd Stream
        if([self checkPlaybackTime:index recordfile:eRecordFile2 startdate:&starttime enddate:&endtime
             ]) {
            if (serverManagerDelegate != nil) {
                if ([serverManagerDelegate respondsToSelector:@selector(controlHelperRefreshPlaybackTime:recordfile:startTime:endTime:)]) {
                    [serverManagerDelegate controlHelperRefreshPlaybackTime:self recordfile:eRecordFile2 startTime:&starttime endTime:&endtime];
                }
            }
            
            Np_DateTime otherStarttime, otherEndtime;
            memset(&otherStarttime, 0, sizeof(Np_DateTime));
            memset(&otherEndtime, 0, sizeof(Np_DateTime));
            NSDate2NpDateTime(startdate, &otherStarttime);
            NSDate2NpDateTime(enddate, &otherEndtime);
            otherStarttime.millisecond = otherEndtime.millisecond = 0;
            
            if ([self checkPlaybackLogTime:index recordfile:eRecordFile1 starttime:&otherStarttime endtime:&otherEndtime]) {
                if (serverManagerDelegate != nil) {
                    if ([serverManagerDelegate respondsToSelector:@selector(controlHelperSupportDualRecordPlayback:camIndex:)]) {
                        [serverManagerDelegate controlHelperSupportDualRecordPlayback:self camIndex:index];
                    }
                    
                    if ([serverManagerDelegate respondsToSelector:@selector(controlHelperRefreshPlaybackTime:recordfile:startTime:endTime:)]) {
                        [serverManagerDelegate controlHelperRefreshPlaybackTime:self recordfile:eRecordFile1 startTime:&otherStarttime endTime:&otherEndtime];
                    }
                }
            }
            
            [camHelper setERecordFile:eRecordFile2];
            [camHelper playbackConnect];
            while (![camHelper isPlaybackConnectionCreated]) {
                sleep(0.5);
            }
        }
        else {
            NSDate2NpDateTime(startdate, &starttime);
            NSDate2NpDateTime(enddate, &endtime);
            
            if ([self checkPlaybackLogTime:index recordfile:eRecordFile1 starttime:&starttime endtime:&endtime]) {
                if (serverManagerDelegate != nil) {
                    if ([serverManagerDelegate respondsToSelector:@selector(controlHelperChangePlaybackRecordFile:camIndex:recordFile:)]) {
                        [serverManagerDelegate controlHelperChangePlaybackRecordFile:self camIndex:index recordFile:eRecordFile1];
                    }
                    
                    if ([serverManagerDelegate respondsToSelector:@selector(controlHelperRefreshPlaybackTime:recordfile:startTime:endTime:)]) {
                        [serverManagerDelegate controlHelperRefreshPlaybackTime:self recordfile:eRecordFile1 startTime:&starttime endTime:&endtime];
                    }
                }
                
                [camHelper setERecordFile:eRecordFile1];
                [camHelper playbackConnect];
                while (![camHelper isPlaybackConnectionCreated]) {
                    sleep(0.5);
                }
            }
            else {
                [m_Condition unlock];
                return FALSE;
            }
        }
    }
    else {
#endif
        if(![self checkPlaybackTime:index startdate:&starttime enddate:&endtime]) {
            [m_Condition unlock]; // jerrylu, 2012/07/12
            return FALSE;
        }
        else { // jerrylu, 2013/02/01, fix bug11855, shift the start time
            if (serverManagerDelegate != nil) {
                if ([serverManagerDelegate respondsToSelector:@selector(controlHelperRefreshPlaybackTime:starttime:endtime:)]) {
                    [serverManagerDelegate controlHelperRefreshPlaybackTime:self starttime:&starttime endtime:&endtime];
                }
            }
        }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    }
#endif
    
    if ([self playbackOpenRecord:starttime enddate:endtime] == NO) {
        [m_Condition unlock]; // jerrylu, 2012/07/12
        return FALSE;
    }
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
    
    return TRUE;
}

- (BOOL)playbackOpenRecord:(Np_DateTime) starttime enddate:(Np_DateTime)endtime {
    if (m_hPBPlayer == nil)
        return NO;
    
    if (PlayBack_OpenRecord(m_hPBPlayer, starttime, endtime) == Np_Result_OK)
        return YES;
    
    return NO;
}

- (void)playbackStop:(int) index {
    [self playbackPause]; // jerrylu, 2012/06/18
    [self disconnectAllPlaybackConnection];
    
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    [camHelper liveViewConnect]; // Reconnect liveview session
    
    if (serverManagerDelegate != nil && [serverManagerDelegate respondsToSelector:@selector(controlHelperDidCameraSessionLost:camIndex:)]) {
        [serverManagerDelegate controlHelperDidCameraSessionLost:self camIndex:index];
    }
}

- (void)playbackStopByEvent:(int) index { // jerrylu, 2012/11/29
    [self playbackPause]; 
    [self disconnectAllPlaybackConnection];
}

- (void)playbackPlay {
    PlayBack_Pause(m_hPBPlayer);
    PlayBack_Play(m_hPBPlayer);
}
- (void)playbackReversePlay {
    PlayBack_Pause(m_hPBPlayer);
    PlayBack_ReversePlay(m_hPBPlayer);
}
- (void)playbackPause {
    PlayBack_Pause(m_hPBPlayer);
}

#if 1
- (void)playbackPreviousFrame:(int) index {
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    if ([camHelper isPlaybackConnectionCreated]) {
        PlayBack_StepBackward(m_hPBPlayer);
    }
}
- (void)playbackNextFrame:(int) index {
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    if ([camHelper isPlaybackConnectionCreated]) {
        PlayBack_StepForward(m_hPBPlayer);
    }
}
#else
- (void)playbackPreviousFrame {
    PlayBack_StepBackward(m_hPBPlayer);
}
- (void)playbackNextFrame {
    PlayBack_StepForward(m_hPBPlayer);
}
#endif

#if 1 // jerrylu, 2013/02/01, fix bug11855
- (void)playbackSeek:(Np_DateTime)seektime {
//    // Brosso test
//    Np_DateTime test;
//    test.year = 2016;
//    test.month = 12;
//    test.day = 25;
//    test.hour = 13;
//    test.minute = 2;
//    test.second = 0;
//    test.millisecond = 0;
//    PlayBack_Seek(m_hPBPlayer, test);
    PlayBack_Seek(m_hPBPlayer, seektime);
}
#else
- (void)playbackSeek:(NSDate *)seekdate {
    Np_DateTime seektime;
    memset(&seektime, 0, sizeof(Np_DateTime));
    
    NSDate2NpDateTime(seekdate, &seektime);
    seektime.second = seektime.millisecond = 0;
    
    PlayBack_Seek(m_hPBPlayer, seektime);
}
#endif

- (float)playbackGetSpeed {
    [m_Condition lock]; // jerrylu, 2012/07/12
    
    float speed = 1.0;
    
    // jerrylu, 2012/07/12
    if (isLogouting == YES) {
        [m_Condition unlock];
        return speed;
    }
    
    PlayBack_GetSpeed(m_hPBPlayer, speed);

    [m_Condition unlock]; // jerrylu, 2012/07/12
    
    return speed;
}
- (void)playbackSetSpeed:(float) speed {
    [m_Condition lock]; // jerrylu, 2012/07/12
    if (isLogouting == YES) {
        [m_Condition unlock];
        return;
    }
    
    PlayBack_SetSpeed(m_hPBPlayer, speed);
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
}

- (Np_DateTime)playbackGetTime {
    [m_Condition lock]; // jerrylu, 2012/07/12

    Np_DateTime time;
    memset(&time, 0, sizeof(Np_DateTime));

    // jerrylu, 2012/07/12
    if (isLogouting == YES) {
        [m_Condition unlock];
        return time;
    }
    
    PlayBack_GetTime(m_hPBPlayer, time);
    
    [m_Condition unlock];  // jerrylu, 2012/07/12
    
    return time;
}

- (Np_PlayerState)playbackGetState {
    [m_Condition lock]; // jerrylu, 2012/07/12
    if (isLogouting == YES) {
        [m_Condition unlock];
        return kStateStopped;
    }
    
    Np_PlayerState state = kStateStopped;
    if (m_hPBPlayer)
        PlayBack_GetPlayerState(m_hPBPlayer, state);
    
    [m_Condition unlock];  // jerrylu, 2012/07/12
    
    return state;
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (BOOL)playbackChangeRecordFile:(int) index recordfile:(EDualRecordFile) recordfile startdate:(Np_DateTime)starttime enddate:(Np_DateTime)endtime seektime:(Np_DateTime)seektime {
    [m_Condition lock];
    if (self.isLogouting == YES) {
        [m_Condition unlock];
        return FALSE;
    }
    
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    [camHelper playbackDisconnect];
    while ([camHelper isPlaybackConnectionCreated]) {
        [NSThread sleepForTimeInterval:0.5];
    }
    [camHelper setERecordFile:recordfile];
    [camHelper playbackConnect];
    while (![camHelper isPlaybackConnectionCreated]) {
        [NSThread sleepForTimeInterval:0.5];
    }
    
    [self playbackOpenRecord:starttime enddate:endtime];
    
    while (![camHelper isPbOpenRecordSuccess])
        [NSThread sleepForTimeInterval:0.5];
    
    [self playbackSeek:seektime];
    
    [m_Condition unlock]; // jerrylu, 2012/07/12
    
    return TRUE;
}
#endif

#if FUNC_PUSH_NOTIFICATION_SUPPORT
#pragma mark -
#pragma mark Push Notification methods
- (NSString *)getServerID {
    NSString *serverid = @"";
    if (serverInfo.serverType != eServer_Titan && serverInfo.serverType != eServer_Crystal) {
        Np_ServerInfo sinfo;
        Np_ServerInfo_MainConsole sinfomainconsole;
        sinfo.serverInfoMC = &sinfomainconsole;
        Info_GetServerInfo(m_hNuSDK, sinfo);
        
        if (sinfo.serverInfoMC != NULL) {
            wchar_t *serveridw;
            Info_GetServerIdentification(m_hNuSDK, &serveridw);
            serverid = StringWToNSString(serveridw);
            
            Info_ReleaseServerIdentification(m_hNuSDK, &serveridw);
        }
        Info_ReleaseServerInfo(m_hNuSDK, sinfo);
    }
    
    return serverid;
}

- (BOOL)pnRegisterToServer {
    Np_PushNotificationDeviceInfo pninfo;
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        std::wstring udidstring = NSStringToStringW([SvUDIDTools UDID]);
        pninfo.deviceID = new wchar_t[udidstring.size() + 1];
        memset(pninfo.deviceID, 0, sizeof(wchar_t)*(udidstring.size() + 1));
        memcpy(pninfo.deviceID, udidstring.c_str(), sizeof(wchar_t)*udidstring.size());
    }
    else {
        std::wstring udidstring = NSStringToStringW([[UIDevice currentDevice] uniqueDeviceIdentifier]);
        pninfo.deviceID = new wchar_t[udidstring.size() + 1];
        memset(pninfo.deviceID, 0, sizeof(wchar_t)*(udidstring.size() + 1));
        memcpy(pninfo.deviceID, udidstring.c_str(), sizeof(wchar_t)*udidstring.size());
    }
    //udidstring.clear();
    
    [appDelegate registerPushNotification];
    NSString *stoken = [[NSString alloc] initWithFormat:@"platform=iOS&iosdev=iphone&token=%@", [appDelegate strToken]];
    std::wstring tokenstring = NSStringToStringW(stoken);
    pninfo.deviceToken = new wchar_t[tokenstring.size() + 1];
    memset(pninfo.deviceToken, 0, sizeof(wchar_t)*(tokenstring.size() + 1));
    memcpy(pninfo.deviceToken, tokenstring.c_str(), sizeof(wchar_t)*tokenstring.size());
    [stoken release];
    //tokenstring.clear();
     
    std::wstring devicenamestring = NSStringToStringW([[UIDevice currentDevice] name]);
    pninfo.deviceName = new wchar_t[devicenamestring.size() + 1];
    memset(pninfo.deviceName, 0, sizeof(wchar_t)*(devicenamestring.size() + 1));
    memcpy(pninfo.deviceName, devicenamestring.c_str(), sizeof(wchar_t)*devicenamestring.size());
    //devicenamestring.clear();
    
    Np_Result_t ret;
    ret = Info_RegisterPushNotification(m_hNuSDK, pninfo);
    delete [] pninfo.deviceID;
    delete [] pninfo.deviceName;
    delete [] pninfo.deviceToken;
    
    if (ret == Np_Result_OK) {
        [self pnAddToEznuuoBlackList:serverInfo add:NO];
        [self saveServerInfo];
        return YES;
    }
    
    return NO;
}

- (BOOL)pnUnregisterToServer {
    Np_PushNotificationDeviceInfo pninfo;
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        std::wstring udidstring = NSStringToStringW([SvUDIDTools UDID]);
        pninfo.deviceID = new wchar_t[udidstring.size() + 1];
        memset(pninfo.deviceID, 0, sizeof(wchar_t)*(udidstring.size() + 1));
        memcpy(pninfo.deviceID, udidstring.c_str(), sizeof(wchar_t)*udidstring.size());
    }
    else {
        std::wstring udidstring = NSStringToStringW([[UIDevice currentDevice] uniqueDeviceIdentifier]);
        pninfo.deviceID = new wchar_t[udidstring.size() + 1];
        memset(pninfo.deviceID, 0, sizeof(wchar_t)*(udidstring.size() + 1));
        memcpy(pninfo.deviceID, udidstring.c_str(), sizeof(wchar_t)*udidstring.size());
    }
    //udidstring.clear();
    
    NSString *stoken = [[NSString alloc] initWithFormat:@"platform=iOS&token=%@", [appDelegate strToken]];
    std::wstring tokenstring = NSStringToStringW(stoken);
    pninfo.deviceToken = new wchar_t[tokenstring.size() + 1];
    memset(pninfo.deviceToken, 0, sizeof(wchar_t)*(tokenstring.size() + 1));
    memcpy(pninfo.deviceToken, tokenstring.c_str(), sizeof(wchar_t)*tokenstring.size());
    [stoken release];
    //tokenstring.clear();
    
    std::wstring devicenamestring = NSStringToStringW([[UIDevice currentDevice] name]);
    pninfo.deviceName = new wchar_t[devicenamestring.size() + 1];
    memset(pninfo.deviceName, 0, sizeof(wchar_t)*(devicenamestring.size() + 1));
    memcpy(pninfo.deviceName, devicenamestring.c_str(), sizeof(wchar_t)*devicenamestring.size());
    //devicenamestring.clear();
    
    Np_Result_t ret;
    ret = Info_UnRegPushNotification(m_hNuSDK, pninfo);
    delete [] pninfo.deviceID;
    delete [] pninfo.deviceName;
    delete [] pninfo.deviceToken;
    
    if (ret == Np_Result_OK) {
        [self saveServerInfo];
        return YES;
    }
    
    return NO;
}

- (BOOL)pnUnregisterToServerWithoutLogin:(ServerInfo *) sinfo {
    Np_Result_t ret = Np_Result_OK;
    
    [self setServerInfo:sinfo];
    
    if (sinfo.serverPushNotificationEnable == kPushNotificationDisable || [sinfo.serverID isEqualToString:@""]) {
        return YES;
    }
    
#if FUNC_P2P_SAT_SUPPORT
    // p2p
    if ([sinfo connectionType] == eP2PConnection) {
        if ([self loginEznuuo] == NO) {
            if ([self pnAddToEznuuoBlackList:sinfo add:YES]) {
                return YES;
            }
            else {
                return NO;
            }
        }
        
        if (!m_hNuSDK)
            ret = Create_Lite_Handle(
                                &m_hNuSDK,
                                kMainConsoleLiveview,
                                NSStringToStringW([sinfo userName_P2P]).c_str(),
                                NSStringToStringW([sinfo userPassword_P2P]).c_str(),
                                NSStringToStringW([p2pServerInfo serverIP]).c_str(),
                                [[p2pServerInfo serverPort] intValue]
                                );
        /*if (ret != Np_Result_OK)
            ret = Create_Handle(
                                &m_hNuSDK,
                                kCorporate,
                                NSStringToStringW([sinfo userName_P2P]),
                                NSStringToStringW([sinfo userPassword_P2P]),
                                NSStringToStringW([p2pServerInfo serverIP]),
                                [[p2pServerInfo serverPort] intValue]
                                );
         */
    }
    else {
#endif
    if (!m_hNuSDK)
        ret = Create_Lite_Handle(
                             &m_hNuSDK,
                             kMainConsoleLiveview,
                             NSStringToStringW([sinfo userName]).c_str(),
                             NSStringToStringW([sinfo userPassword]).c_str(),
                             NSStringToStringW([sinfo serverIP]).c_str(),
                             [[sinfo serverPort] intValue]
                             );
    /*
     if (ret != Np_Result_OK)
        ret = Create_Handle(
                             &m_hNuSDK,
                             kCorporate,
                             NSStringToStringW([sinfo userName]),
                             NSStringToStringW([sinfo userPassword]),
                             NSStringToStringW([sinfo serverIP]),
                             [[sinfo serverPort] intValue]
                             );
     */
#if FUNC_P2P_SAT_SUPPORT
    }
#endif
    if (ret != Np_Result_OK)
    {
        Destroy_Handle(m_hNuSDK);
        m_hNuSDK = NULL;
        
#if FUNC_P2P_SAT_SUPPORT
        if ([sinfo connectionType] == eP2PConnection) {
            //Release SAT P2P Tunnel
            if(p_tunnel != NULL)
            {
                p_tunnel->Stop();
                delete p_tunnel;
                p_tunnel = NULL;
            }
        }
#endif
        if ([self pnAddToEznuuoBlackList:sinfo add:YES]) {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    Np_PushNotificationDeviceInfo pninfo;
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        std::wstring udidstring = NSStringToStringW([SvUDIDTools UDID]);
        pninfo.deviceID = new wchar_t[udidstring.size() + 1];
        memset(pninfo.deviceID, 0, sizeof(wchar_t)*(udidstring.size() + 1));
        memcpy(pninfo.deviceID, udidstring.c_str(), sizeof(wchar_t)*udidstring.size());
    }
    else {
        std::wstring udidstring = NSStringToStringW([[UIDevice currentDevice] uniqueDeviceIdentifier]);
        pninfo.deviceID = new wchar_t[udidstring.size() + 1];
        memset(pninfo.deviceID, 0, sizeof(wchar_t)*(udidstring.size() + 1));
        memcpy(pninfo.deviceID, udidstring.c_str(), sizeof(wchar_t)*udidstring.size());
    }
    //udidstring.clear();
    
    NSString *stoken = [[NSString alloc] initWithFormat:@"platform=iOS&token=%@", [appDelegate strToken]];
    std::wstring tokenstring = NSStringToStringW(stoken);
    pninfo.deviceToken = new wchar_t[tokenstring.size() + 1];
    memset(pninfo.deviceToken, 0, sizeof(wchar_t)*(tokenstring.size() + 1));
    memcpy(pninfo.deviceToken, tokenstring.c_str(), sizeof(wchar_t)*tokenstring.size());
    [stoken release];
    //tokenstring.clear();
    
    std::wstring devicenamestring = NSStringToStringW([[UIDevice currentDevice] name]);
    pninfo.deviceName = new wchar_t[devicenamestring.size() + 1];
    memset(pninfo.deviceName, 0, sizeof(wchar_t)*(devicenamestring.size() + 1));
    memcpy(pninfo.deviceName, devicenamestring.c_str(), sizeof(wchar_t)*devicenamestring.size());
    //devicenamestring.clear();
    
    ret = Info_UnRegPushNotification(m_hNuSDK, pninfo);
    delete [] pninfo.deviceID;
    delete [] pninfo.deviceName;
    delete [] pninfo.deviceToken;
    
    if (m_hNuSDK) {
        Destroy_Handle(m_hNuSDK);
        m_hNuSDK = NULL;
    }
    
#if FUNC_P2P_SAT_SUPPORT
    if ([sinfo connectionType] == eP2PConnection) {
        //Release SAT P2P Tunnel
        if(p_tunnel != NULL)
        {
            p_tunnel->Stop();
            delete p_tunnel;
            p_tunnel = NULL;
        }
    }
#endif
    
    if (ret != Np_Result_OK && ret != Np_Result_SVR_FAULT) /* Np_Result_SVR_FAULT:server couldn't find the token */
    {
        if ([self pnAddToEznuuoBlackList:sinfo add:YES])
            return YES;
        else {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)pnAddToEznuuoBlackList:(ServerInfo *) sinfo add:(BOOL) isAdd {
    NSString *cmd;
    if (isAdd)
        cmd = [NSString stringWithFormat:@"add"];
    else
        cmd = [NSString stringWithFormat:@"remove"];
    
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    if ([appDelegate strToken] == nil || (NSNull *)[appDelegate strToken] == [NSNull null])
        return NO;
    
    NSString *devicename;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        devicename = [SvUDIDTools UDID];
    }
    else {
        devicename = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    }
    
    NSString *url;
    if (sinfo.connectionType == eP2PConnection) {
        url = [[NSString alloc] initWithFormat:@"https://www.eznuuo.com/update_push_blacklist.php?ver=1.0&update=%@&token=%@&serverid=%@&devicename=%@&username=%@&type=iOS", cmd, [appDelegate strToken], sinfo.serverID, devicename, sinfo.userName_P2P];
    }
    else {
        url = [[NSString alloc] initWithFormat:@"https://www.eznuuo.com/update_push_blacklist.php?ver=1.0&update=%@&token=%@&serverid=%@&devicename=%@&username=%@&type=iOS", cmd, [appDelegate strToken], sinfo.serverID, devicename, sinfo.userName];
    }
    NSString* encodedUrl = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:encodedUrl]];
    [urlRequest setHTTPMethod:@"GET"];
    //[urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //[urlRequest setHTTPBody:postData];
    [url release];
    
    NSURLResponse *response;
    NSError *error;
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    BOOL ret = NO;
    NSString *replyString;
    if (serverReply != nil) {
        replyString = [[[NSString alloc] initWithBytes:[serverReply bytes] length:[serverReply length] encoding: NSASCIIStringEncoding] autorelease];
        NSLog(@"reply string is :%@",replyString);
        
        if ([replyString isEqualToString:@"UPDATE SUCCESS"])
            ret = YES;
    }
    else {
        replyString = [[[NSString alloc] initWithFormat:@"Unconnection"] autorelease];
        NSLog(@"reply string is :%@",replyString);
    }
    
    return ret;
}
#endif // FUNC_PUSH_NOTIFICATION_SUPPORT

#pragma mark -
#pragma mark debug methods

#if SHOW_FRAMERATE_FLAG
- (double)getLiveViewFrameRate:(int) index {
    if (index >= [cameraHelperArray count])
        return 0;
    
    CameraHelper *camHelper = [cameraHelperArray objectAtIndex:index];
    return [camHelper getFrameRate];
}
#endif
@end
