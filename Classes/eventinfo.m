//
//  eventinfo.m
//  iViewer
//
//  Created by NUUO on 12/8/9.
//
//

#import "eventinfo.h"

@implementation Eventinfo

@synthesize eventType;
@synthesize serverID;
@synthesize cameraID;
@synthesize username;
@synthesize eventDate;
@synthesize eventMsg;
@synthesize isNotRead;

- (id)init {
	if ((self = [super init])) {
		[self setEventType:@""];
		[self setServerID:@""];
        [self setCameraID:@""]; // jerrylu, 2012/04/19
		[self setUsername:@""];
		[self setEventDate:nil];
		[self setEventMsg:@""];
        [self setIsNotRead:1];
	}
	return self;
}

- (id)initWithDictionary:(NSDictionary *) dictionary {
	if ((self = [self init])) {
		[self setValuesForKeysWithDictionary:dictionary];
	}
	return self;
}

- (NSArray *) getKeys {
    NSArray * keys = [NSArray arrayWithObjects:
                      @"eventType",
                      @"serverID",
                      @"cameraID",
                      @"username",
                      @"eventDate",
                      @"eventMsg",
                      @"isNotRead",
                      nil];
    return keys;
}

- (id)getPropertyDic {
    return [self dictionaryWithValuesForKeys:[self getKeys]];
}

- (NSString *)description {
	return [[self getPropertyDic] description];
}

- (void)dealloc {
    // Couldn't release directly
    if ((NSNull *)eventType != [NSNull null])
       [eventType release];
    eventType = nil;
    if ((NSNull *)serverID != [NSNull null])
        [serverID release];
    serverID = nil;
    if ((NSNull *)cameraID != [NSNull null])
        [cameraID release];
    cameraID = nil;
    if ((NSNull *)username != [NSNull null])
        [username release];
    username = nil;
    if ((NSNull *)eventDate != [NSNull null])
        [eventDate release];
    eventDate = nil;
    if ((NSNull *)eventMsg != [NSNull null])
        [eventMsg release];
    eventMsg = nil;
    [super dealloc];
}

@end
