//
//  MyCameraListController.h
//  iViewer
//
//  Created by NUUO on 2015/6/2.
//
//

#ifndef iViewer_MyCameraListController_h
#define iViewer_MyCameraListController_h


#endif

#import <UIKit/UIKit.h>
#import "ServerManager.h"

@interface MyCameraListController : UITableViewController<UIAlertViewDelegate> {
    id switchControlDelegate;
    ServerManager * serverMgr;
    
    int allCameraCount;
    NSMutableArray * allCameraNameArray;
    
    UIBarButtonItem *backButton;
}
@property (nonatomic, assign) id switchControlDelegate;
@property (nonatomic, assign) ServerManager * serverMgr;
@end

@protocol MyCameraListControllerDelegate
@optional
- (void)switchToCameraIndex:(int) ch_index;
@end