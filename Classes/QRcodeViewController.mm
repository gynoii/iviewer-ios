//
//  ViewController.m
//  QRCodeReader

#import "QRcodeViewController.h"
#import "LiveViewAppDelegate.h"
#import "serverInfo.h"

#define DeniedCaneraAlertTag 12

@interface QRcodeViewController ()
@property (nonatomic, retain) AVCaptureSession *captureSession;
@property (nonatomic, retain) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, retain) CAShapeLayer *maskLayer;
@property (nonatomic) BOOL isReading;

-(void)prepareReading;
-(void)startReading;
-(void)stopReading;

@end

@implementation QRcodeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Initially make the captureSession object nil.
    _captureSession = nil;
    
    // Set the initial value of the flag to NO.
    _isReading = NO;
    
    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                      target:self
                                      action:@selector(cancel)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    [[self navigationItem] setRightBarButtonItem:cancelButton];
    
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    [cancelButton release];
}

- (void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareReading];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [self changeLayout:[[UIApplication sharedApplication] statusBarOrientation]];
    }
    else {
        [self changeLayout:self.interfaceOrientation];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self startReading];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self stopReading];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        [self changeLayout:[[UIApplication sharedApplication] statusBarOrientation]];
    } completion:nil];
    
    return;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [self changeLayout:self.interfaceOrientation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    if (_captureSession != nil) {
        [_captureSession release];
        _captureSession = nil;
    }

    if (_videoPreviewLayer != nil) {
        [_videoPreviewLayer release];
        _videoPreviewLayer = nil;
    }
    
    [super dealloc];
}

- (void)changeLayout:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        [_videoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
    else if (interfaceOrientation == UIInterfaceOrientationPortrait)
        [_videoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortrait];
    else if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
        [_videoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
    else
        [_videoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    
    int square_size = 250.0;
    if (IS_IPAD)
        square_size = 400.0;
    
    float squareOrigin_x = (_viewPreview.frame.size.width - square_size) / 2;
    float squareOrigin_y = (_viewPreview.frame.size.height - square_size) / 2;
    _qrCodeSquare.frame = CGRectMake(squareOrigin_x, squareOrigin_y, square_size, square_size);
    
    // Add Mask
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, _videoPreviewLayer.bounds.size.width, _videoPreviewLayer.bounds.size.height) cornerRadius:0];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(squareOrigin_x, squareOrigin_y, square_size, square_size) cornerRadius:0];
    [path appendPath:circlePath];
    [path setUsesEvenOddFillRule:YES];
    
    [_maskLayer removeFromSuperlayer];
    _maskLayer = [CAShapeLayer layer];
    _maskLayer.path = path.CGPath;
    _maskLayer.fillRule = kCAFillRuleEvenOdd;
    _maskLayer.fillColor = [UIColor blackColor].CGColor;
    _maskLayer.opacity = 0.5;
    [_videoPreviewLayer addSublayer:_maskLayer];
    
    AVCaptureMetadataOutput *metadata_output = [_captureSession.outputs objectAtIndex:0];
    float origin_rate_x = squareOrigin_x / _viewPreview.frame.size.width;
    float origin_rate_y = squareOrigin_y / _viewPreview.frame.size.height;
    float width_interest = square_size / _viewPreview.frame.size.width;
    float height_interest = square_size / _viewPreview.frame.size.height;
    if (interfaceOrientation == UIInterfaceOrientationPortrait ||
        interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        metadata_output.rectOfInterest = CGRectMake(origin_rate_y, origin_rate_x, height_interest, width_interest);
    }
    else {
        metadata_output.rectOfInterest = CGRectMake(origin_rate_x, origin_rate_y, width_interest, height_interest);
    }
    
    return;
}

#pragma mark - Private method implementation

- (void)prepareReading {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        [self camOpen];
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        [self camDenied];
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        
        NSLog(@"Camera is restricted.");
    }
    else if(status == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted)
            {
                NSLog(@"Granted access to %@", AVMediaTypeVideo);
                [self camOpen];
            }
            else
            {
                NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                [self camDenied];
            }
        }];
    }
}

- (void)camOpen {
    NSError *error;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    [captureMetadataOutput release];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    // Start video capture.
    [_captureSession startRunning];
}

- (void)camDenied {
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        alertButton = NSLocalizedString(@"OK", nil);
    }
    else {
        alertButton = nil;
    }
    alertText = @"Please turn on camera privacy setting for QR code scanning.";
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:alertText preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:alertButton
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Cancel", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:cancel];
        if (canOpenSettings) {
            [alert addAction:ok];
        }
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Warning Message", nil)
                              message:alertText
                              delegate:self
                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                              otherButtonTitles:alertButton, nil];
        alert.tag = DeniedCaneraAlertTag;
        [alert show];
    }
}

- (void)stopReading {
    // Stop video capture.
    [_captureSession stopRunning];
    
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];
}

- (void)startReading {
    _isReading = !_isReading;
}

- (void)parseMetaData:(NSString *)metadata_string {
    NSMutableDictionary *parameter_dic = [[[NSMutableDictionary alloc] init] autorelease];
    
    BOOL isIncludeScheme = NO;
    NSArray* cfBundleURLTypes = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleURLTypes"];
    if ([cfBundleURLTypes isKindOfClass:[NSArray class]] && [cfBundleURLTypes lastObject]) {
        NSDictionary* cfBundleURLTypes0 = [cfBundleURLTypes objectAtIndex:0];
        if ([cfBundleURLTypes0 isKindOfClass:[NSDictionary class]]) {
            NSArray* cfBundleURLSchemes = [cfBundleURLTypes0 objectForKey:@"CFBundleURLSchemes"];
            if ([cfBundleURLSchemes isKindOfClass:[NSArray class]]) {
                for (NSString* scheme in cfBundleURLSchemes) {
                    if ([scheme isKindOfClass:[NSString class]]) {
                        if ([metadata_string rangeOfString:scheme].location == 0) {
                            isIncludeScheme = YES;
                            break;
                        }
                    }
                }
            }
        }
    }
    
    if (!isIncludeScheme) {
        [self popWarning:eError_InValidQRcode];
        return;
    }
    
    NSArray *comp = [metadata_string componentsSeparatedByString:@"?"];
    NSString *encodeData = [comp lastObject];
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:encodeData options:0];
    NSString *query = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    
    NSArray *queryElements = [query componentsSeparatedByString:@"&"];
    for (NSString *element in queryElements) {
        NSArray *keyVal = [element componentsSeparatedByString:@"="];
        if (keyVal.count > 0) {
            NSString *variableKey = [keyVal objectAtIndex:0];
            NSString *value = (keyVal.count == 2) ? [keyVal lastObject] : nil;
            if (value != nil)
                [parameter_dic setObject:value forKey:variableKey];
        }
    }
    
    if ([parameter_dic objectForKey:@"version"] == nil || [parameter_dic objectForKey:@"sid"] == nil) {
        [self popWarning:eError_InValidQRcode];
    }
    else {
        ServerInfo *sinfo = [[ServerInfo alloc] init];
        [sinfo setConnectionType:eP2PConnection];
        [sinfo setServerName_P2P:[parameter_dic objectForKey:@"sid"]];
        [sinfo setServerID_P2P:[parameter_dic objectForKey:@"sid"]];
        
        if ([parameter_dic objectForKey:@"username"] != nil) {
            [sinfo setUserName_P2P:[parameter_dic objectForKey:@"username"]];
            if ([parameter_dic objectForKey:@"pwd"] != nil) {
                [sinfo setUserPassword_P2P:[parameter_dic objectForKey:@"pwd"]];
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else {
            [self dismissViewControllerAnimated:NO completion:nil];
        }
        
        [_favoriteSite openServerView:sinfo];
        [sinfo release];
    }
}

- (void)popWarning:(EErrorType) eErrorType {
    NSString * strMsg;
    switch (eErrorType) {
        case eError_InValidQRcode:
            strMsg = [NSString stringWithFormat:NSLocalizedString(@"Wrong code type, please check you scan the solo QR code.", nil)];
            break;
        default:
            break;
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:strMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self cancel];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                        message:strMsg
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    if (_isReading == NO)
        return;
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            
            [self parseMetaData:[metadataObj stringValue]];
            
            _isReading = NO;
        }
    }
}

#pragma mark - UIAlertViewDelegate method
- (void)alertView:(UIAlertView *) alertView didDismissWithButtonIndex:(NSInteger) buttionIndex {
    if (alertView.tag == DeniedCaneraAlertTag) {
        if (buttionIndex == 1) {
            BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
    else {
        [self cancel];
    }
}

@end
