//
//  MyViewCameraController.m
//  iViewer
//
//  Created by NUUO on 13/10/4.
//
//

#import "MyViewCameraController.h"
#import "LiveViewAppDelegate.h"
#include "Utility.h"

@implementation MyViewCameraController

@synthesize myviewInfo = _myviewInfo;
@synthesize ctrlhelpr = _ctrlhelpr;
@synthesize recordingServerCID;
@synthesize recordingServerLID;
@synthesize isAdding;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        _isAdding = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isSelectAll = NO;
    myviewInfoServerIndex = -1;
    allSensorCount = 0;
    allCamCentralIdArray = [[NSMutableArray alloc] init];
    allCamLocalIdArray = [[NSMutableArray alloc] init];
    allCameraNameArray = [[NSMutableArray alloc] init];
    allSwitchArray = [[NSMutableArray alloc] init];
    
    
    if (_ctrlhelpr.serverInfo.serverType == eServer_Crystal) {
        Np_ID_Ext recordingServerId;
        recordingServerId.centralID = recordingServerCID;
        recordingServerId.localID = recordingServerLID;
        std::vector<Np_Device_Ext> *cameraDeviceList = [_ctrlhelpr getAllCameraListFromRecordingServer:recordingServerId];
        
        for (int i = 0; i < cameraDeviceList->size(); i++)
        {
            Np_Device_Ext cameradevice = cameraDeviceList->at(i);
            NSString * cname = WChartToNSString(cameradevice.name);
            
            for (int j = 0; j < cameradevice.SensorDevices.size; j++) {
                Np_SubDevice_Ext subdevice = cameradevice.SensorDevices.items[j];
                NSString * subname = WChartToNSString(subdevice.name);
                
                [allSwitchArray addObject:[NSNull null]];
                NSString *cid = [NSString stringWithFormat:@"%llu", subdevice.ID.centralID];
                NSString *lid = [NSString stringWithFormat:@"%llu", subdevice.ID.localID];
                [allCamCentralIdArray addObject:cid];
                [allCamLocalIdArray addObject:lid];
                
                NSString *camera_name = [NSString stringWithFormat:@"%@-%@", cname, subname];
                [allCameraNameArray addObject:camera_name];
                [subname release];
                
                allSensorCount++;
            
            }
            [cname release];
        }
        
        delete cameraDeviceList;
    }
    else {
        std::vector<Np_Device> * cameraDeviceList = [_ctrlhelpr getAllCameraList];
        
        for (int i = 0; i < cameraDeviceList->size(); i++)
        {
            Np_Device camdevice = cameraDeviceList->at(i);
            
            [allSwitchArray addObject:[NSNull null]];
            NSString *cid = [NSString stringWithFormat:@"%d", camdevice.SensorDevices[0].ID.centralID];
            NSString *lid = [NSString stringWithFormat:@"%d", camdevice.SensorDevices[0].ID.localID];
            [allCamCentralIdArray addObject:cid];
            [allCamLocalIdArray addObject:lid];
            NSString *camera_name = StringWToNSString(camdevice.name);
            [allCameraNameArray addObject:camera_name];
            [camera_name release];
            allSensorCount++;
        }
        
        delete cameraDeviceList;
    }
    [self performSelectorOnMainThread:@selector(initAllSwitch) withObject:nil waitUntilDone:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    [[self navigationItem] setLeftBarButtonItem:backButton];
    [backButton release];
    
    saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"OK", nil) style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [saveButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [saveButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    [[self navigationItem] setRightBarButtonItem:saveButton];
    [saveButton release];
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [allCamCentralIdArray release];
    [allCamLocalIdArray release];
    [allCameraNameArray release];
    [allSwitchArray release];
    _ctrlhelpr = nil;
    
    [super viewWillDisappear:animated];
}

- (void)initAllSwitch
{
    BOOL isFound = NO;
    MyViewServerInfo *sinfo = nil;
    for (MyViewServerInfo *mySinfo in _myviewInfo.serverList)
    {
        if ([mySinfo.serverID isEqualToString:_ctrlhelpr.serverInfo.serverID] &&
            [mySinfo.userName isEqualToString:[_ctrlhelpr getUserName]])
        {
            myviewInfoServerIndex = [_myviewInfo.serverList indexOfObject:mySinfo];
            isFound = YES;
            sinfo = mySinfo;
            break;
        }
    }
    
    for (int i = 0; i < allSensorCount; i++) {
        UISwitch * cellSwitch = [[[UISwitch alloc] init] autorelease];
        [cellSwitch addTarget:self action:@selector(switchchange:) forControlEvents:UIControlEventValueChanged];
        cellSwitch.tag = i;
        
        if (!isFound) {
            cellSwitch.on = NO;
        }
        else {
            BOOL isSelect = NO;
            for (MyViewCameraInfo *cinfo in sinfo.cameraList)
            {
                if ([cinfo.camCentralID isEqualToString:[allCamCentralIdArray objectAtIndex:i]] &&
                    [cinfo.camLocalID isEqualToString:[allCamLocalIdArray objectAtIndex:i]] &&
                    [cinfo.rsCentralID isEqualToString:[NSString stringWithFormat:@"%llu", recordingServerCID]] &&
                    [cinfo.rsLocalID isEqualToString:[NSString stringWithFormat:@"%llu", recordingServerLID]]) {
                    isSelect = YES;
                    break;
                }
            }
            cellSwitch.on = isSelect;
        }
        
        [allSwitchArray replaceObjectAtIndex:i withObject:cellSwitch];
    }
}

- (void)selectAll
{
    if(_isSelectAll)
    {
        // Remove All Camera
        _isSelectAll = NO;
        
        for (int i = 0; i < [allSwitchArray count]; i++) {
            UISwitch * camera_switch = [allSwitchArray objectAtIndex:i];
            camera_switch.on = NO;
        }
    }
    else
    {
        _isSelectAll = YES;
        
        for (int i = 0; i < [allSwitchArray count]; i++) {
            UISwitch * camera_switch = [allSwitchArray objectAtIndex:i];
            if (camera_switch.on)
                continue;
            
            if ([self checkCameraLimit] == NO)
            {
                NSString *msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"%d cameras are selected. The maximum camera number per MyView is %d channels.", nil), kMaxNumOfCamera, kMaxNumOfCamera];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"OK", nil)
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                    [alert show];
                    [alert release];
                }
                
                [msg release];
                break;
            }
            
            camera_switch.on = YES;
        }
    }
}

- (void)switchchange:(UISwitch*) sender
{
    UISwitch *switchCell = (UISwitch *)sender;
    BOOL selectcell = switchCell.on;
    
    if (selectcell)
    {
        if ([self checkCameraLimit] == NO)
        {
            switchCell.on = NO;
            
            NSString *msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"%d cameras are selected. The maximum camera number per MyView is %d channels.", nil), kMaxNumOfCamera, kMaxNumOfCamera];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"OK", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
            [msg release];
        }
    }
}

- (BOOL)checkCameraLimit
{
    int count = 0;
    for (MyViewServerInfo *sinfo in _myviewInfo.serverList) {
        count += [sinfo.cameraList count];
    }
    
    if (count == kMaxNumOfCamera)
        return NO;
    
    return YES;
}

- (int)getMaxCameraOrder
{
    int max_order = -1;
    for (MyViewServerInfo *sinfo in _myviewInfo.serverList) {
        for (MyViewCameraInfo *cinfo in sinfo.cameraList) {
            if (cinfo.cameraOrder > max_order)
                max_order = cinfo.cameraOrder;
        }
    }
    
    return max_order;
}

- (void)save
{
    BOOL isFound = NO;
    MyViewServerInfo *sinfo = nil;
    for (MyViewServerInfo *mySinfo in _myviewInfo.serverList)
    {
        if ([mySinfo.serverID isEqualToString:_ctrlhelpr.serverInfo.serverID] &&
            [mySinfo.userName isEqualToString:[_ctrlhelpr getUserName]])
        {
            isFound = YES;
            sinfo = mySinfo;
            break;
        }
    }
    if(!isFound) {
        sinfo = [[MyViewServerInfo alloc] init];
        [sinfo setServerID:_ctrlhelpr.serverInfo.serverID];
        [sinfo setUserName:[_ctrlhelpr getUserName]];
        sinfo.cameraList = [[NSMutableArray alloc] init];
        [_myviewInfo.serverList addObject:sinfo];
    }
    
    int max_order = [self getMaxCameraOrder] + 1;
    
    // remove disappearred and unselected camera
    for (int i = 0; i < [sinfo.cameraList count]; i++)
    {
        MyViewCameraInfo *cinfo = [sinfo.cameraList objectAtIndex:i];
        if ([cinfo.rsCentralID isEqualToString:[NSString stringWithFormat:@"%llu", recordingServerCID]] &&
            [cinfo.rsLocalID isEqualToString:[NSString stringWithFormat:@"%llu", recordingServerLID]]) {
            BOOL isRemoved = YES;
            for (int j = 0; j < allSensorCount; j++) {
                if ([cinfo.camCentralID isEqualToString:[allCamCentralIdArray objectAtIndex:j]] &&
                    [cinfo.camLocalID isEqualToString:[allCamLocalIdArray objectAtIndex:j]]) {
                    UISwitch * camera_switch = [allSwitchArray objectAtIndex:j];
                    if (camera_switch.on)
                        isRemoved = NO;
                    break;
                }
            }
            
            if (isRemoved) {
                [sinfo.cameraList removeObjectAtIndex:i];
                i--;
            }
        }
    }
    
    // add new camera
    for (int i = 0; i < allSensorCount; i++) {
        UISwitch * camera_switch = [allSwitchArray objectAtIndex:i];
        
        if (camera_switch.on) {
            isFound = NO;
            for (MyViewCameraInfo *cinfo in sinfo.cameraList)
            {
                if ([cinfo.camCentralID isEqualToString:[allCamCentralIdArray objectAtIndex:i]] &&
                    [cinfo.camLocalID isEqualToString:[allCamLocalIdArray objectAtIndex:i]] &&
                    [cinfo.rsCentralID isEqualToString:[NSString stringWithFormat:@"%llu", recordingServerCID]] &&
                    [cinfo.rsLocalID isEqualToString:[NSString stringWithFormat:@"%llu", recordingServerLID]]) {
                    isFound = YES;
                    break;
                }
            }
            
            if (!isFound) {
                MyViewCameraInfo *new_cinfo = [[MyViewCameraInfo alloc] init];
                [new_cinfo setCamCentralID:[allCamCentralIdArray objectAtIndex:i]];
                [new_cinfo setCamLocalID:[allCamLocalIdArray objectAtIndex:i]];
                [new_cinfo setRsCentralID:[NSString stringWithFormat:@"%llu", recordingServerCID]];
                [new_cinfo setRsLocalID:[NSString stringWithFormat:@"%llu", recordingServerLID]];
                [new_cinfo setCameraName:[allCameraNameArray objectAtIndex:i]];
                [new_cinfo setCameraOrder:max_order];
                max_order++;
                [sinfo.cameraList addObject:new_cinfo];
                [new_cinfo release];
            }
        }
    }
    
    // reset the order
    int count = 0;
    for (int i = 0; i < [_myviewInfo.serverList count]; i++) {
        MyViewServerInfo *mySinfo = [_myviewInfo.serverList objectAtIndex:i];
        if ([mySinfo.cameraList count] > 0)
            count += [mySinfo.cameraList count];
        else {
            [_myviewInfo.serverList removeObject:mySinfo];
            i--;
        }
    }
    
    for (int i = 0; i < count; ) {
        isFound = NO;
        for (MyViewServerInfo *mySinfo in _myviewInfo.serverList) {
            for (MyViewCameraInfo *myCinfo in mySinfo.cameraList) {
                if (myCinfo.cameraOrder == i) {
                    isFound = YES;
                    break;
                }
            }
            if (isFound)
                break;
        }
        
        if (!isFound) {
            for (MyViewServerInfo *mySinfo in _myviewInfo.serverList) {
                for (MyViewCameraInfo *myCinfo in mySinfo.cameraList) {
                    if (myCinfo.cameraOrder > i)
                        myCinfo.cameraOrder--;
                }
            }
        }
        else {
            i++;
        }
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Selected cameras are saved.", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *saveAlert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Selected cameras are saved.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [saveAlert show];
        [saveAlert release];
    }
}

- (void)back
{
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

# pragma mark - UIAlertViewDelegate Method

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 1;
    else
        return allSensorCount;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return nil;
    else
        return NSLocalizedString(@"Cameras", nil);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        
        if (indexPath.section == 0) {
            cell.textLabel.text = NSLocalizedString(@"All", nil);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UISwitch *selectAllSwitch = [[[UISwitch alloc] init] autorelease];
            [selectAllSwitch addTarget:self action:@selector(selectAll) forControlEvents:UIControlEventValueChanged];
            cell.accessoryView = selectAllSwitch;
        }
        else {
            NSInteger row = [indexPath row];
            cell.textLabel.text = [allCameraNameArray objectAtIndex:row];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryView = [allSwitchArray objectAtIndex:row];
        }
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
