//
//  serverInfo.m
//  TabView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright 2010 com.debug. All rights reserved.
//

#import "serverInfo.h"


@implementation ServerInfo

@synthesize serverIP;
@synthesize serverPort;
@synthesize userName;
@synthesize userPassword;
@synthesize serverName;
@synthesize serverType;
@synthesize serverPlaybackPort; // jerrylu, 2012/04/19
@synthesize connectionType; // jerrylu, 2012/07/16
@synthesize serverID;
@synthesize serverPushNotificationEnable; // jerrylu, 2012/07/25
@synthesize serverID_P2P; // jerrylu, 2012/09/10
@synthesize userName_P2P; // jerrylu, 2012/09/28
@synthesize userPassword_P2P;
@synthesize serverName_P2P;
@synthesize layoutType;
@synthesize myViewLayoutType;

- (id)init {
	if ((self = [super init])) {
		[self setServerIP:@""];
		[self setServerPort:@""];
        [self setServerPlaybackPort:@""]; // jerrylu, 2012/04/19
		[self setUserName:@""];
		[self setUserPassword:@""];
		[self setServerName:@""];
        [self setServerType:eServer_Default];
        [self setConnectionType:0]; // jerrylu, 2012/07/16
        [self setServerID:@""];
		[self setServerPushNotificationEnable:kDefaultPushNotification]; // jerrylu, 2012/07/25
        [self setServerID_P2P:@""]; // jerrylu, 2012/09/10
        [self setUserName_P2P:@""]; // jerrylu, 2012/09/28
        [self setUserPassword_P2P:@""];
        [self setServerName_P2P:@""];
        [self setLayoutType:eLayout_2X3];
	}
	return self;
}

- (id)initWithDictionary:(NSDictionary *) dictionary {
	if ((self = [self init])) {
		[self setValuesForKeysWithDictionary:dictionary];
	}
	return self;
}

- (NSArray *) getKeys {
  NSArray * keys = [NSArray arrayWithObjects:
					 @"serverIP",
					 @"serverPort",
                     @"userName",
					 @"userPassword",
					 @"serverName",
                     @"serverPlaybackPort", // jerrylu, 2012/04/19
                     @"connectionType", // jerrylu, 2012/07/16
                     @"serverID",
                     @"serverPushNotificationEnable", // jerrylu, 2012/07/25
                     @"serverID_P2P", // jerrylu, 2012/09/10
                     @"serverName_P2P", // jerrylu, 2012/09/28
                     @"userName_P2P",
                     @"userPassword_P2P",
					 @"serverType",
                     @"layoutType",
                     @"myViewLayoutType",
					 nil];
  return keys;
}

- (id)getPropertyDic {
  return [self dictionaryWithValuesForKeys:[self getKeys]];
}

- (NSString *)description {
	return [[self getPropertyDic] description];
}

- (void)dealloc {
    if ([NSNull null] != (NSNull *)serverIP) {
        [serverIP release];
        serverIP = nil;
    }
    if ([NSNull null] != (NSNull *)serverPort) {
        [serverPort release];
        serverPort = nil;
    }
    if ([NSNull null] != (NSNull *)userName) {
        [userName release];
        userName = nil;
    }
    if ([NSNull null] != (NSNull *)userPassword) {
        [userPassword release];
        userPassword = nil;
    }
    if ([NSNull null] != (NSNull *)serverName) {
        [serverName release];
        serverName = nil;
    }
    if ([NSNull null] != (NSNull *)serverPlaybackPort) {
        [serverPlaybackPort release];
        serverPlaybackPort = nil;
    }
    if ([NSNull null] != (NSNull *)serverID) {
        [serverID release];
        serverID = nil;
    }
    if ([NSNull null] != (NSNull *)serverID_P2P) {
        [serverID_P2P release];
        serverID_P2P = nil;
    }
    if ([NSNull null] != (NSNull *)userName_P2P) {
        [userName_P2P release];
        userName_P2P = nil;
    }
    if ([NSNull null] != (NSNull *)userPassword_P2P) {
        [userPassword_P2P release];
        userPassword_P2P = nil;
    }
    if ([NSNull null] != (NSNull *)serverName_P2P) {
        [serverName_P2P release];
        serverName_P2P = nil;
    }
    
    [super dealloc];
}

@end
