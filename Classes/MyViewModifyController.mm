//
//  MyViewModifyController.mm
//  iViewer
//
//  Created by NUUO on 13/8/19.
//
//

#import "MyViewModifyController.h"
#import "LiveViewAppDelegate.h"
#import "MyViewCameraController.h"
#import <QuartzCore/QuartzCore.h>
#include "Utility.h"

@implementation MyViewModifyController

@synthesize favoriteSite = _favoriteSite;

- (id)initWithStyle:(UITableViewStyle)style ServerMgr:(ServerManager *)srvMgr
{
    self = [super initWithStyle:style];
    if (self) {
        serverMgr = srvMgr;
        
        // backup the viewinfo
        newViewInfo = [[MyViewInfo alloc] init];
        NSMutableArray * serverInfoList = [[NSMutableArray alloc] init];
        for (MyViewServerInfo * sinfo in [serverMgr.myViewBackupInfo serverList]) {
            if ([sinfo.cameraList count] == 0)
                continue;
            
            NSMutableArray * cameraInfoList = [[NSMutableArray alloc] init];
            for (MyViewCameraInfo * cinfo in [sinfo cameraList]) {
                MyViewCameraInfo *new_cinfo = [[[MyViewCameraInfo alloc] init] autorelease];
                [new_cinfo setCamCentralID:cinfo.camCentralID];
                [new_cinfo setCamLocalID:cinfo.camLocalID];
                [new_cinfo setRsCentralID:cinfo.rsCentralID];
                [new_cinfo setRsLocalID:cinfo.rsLocalID];
                [new_cinfo setCameraName:cinfo.cameraName];
                [new_cinfo setCameraOrder:cinfo.cameraOrder];
                [cameraInfoList addObject:new_cinfo];
            }
            MyViewServerInfo *new_sinfo = [[[MyViewServerInfo alloc] init] autorelease];
            [new_sinfo setServerID:sinfo.serverID];
            [new_sinfo setUserName:sinfo.userName];
            [new_sinfo setCameraList:cameraInfoList];
            [serverInfoList addObject:new_sinfo];
            [cameraInfoList release];
        }
        [newViewInfo setServerList:serverInfoList];
        [serverInfoList release];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"OK", nil) style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [saveButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [saveButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    //[[self navigationItem] setRightBarButtonItem:saveButton];
    
    UIImage *calearAllImage;
    UIButton *caButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        calearAllImage = [UIImage imageNamed:@"Reset_48x48_nor.png"];
    else
        calearAllImage = [UIImage imageNamed:@"Reset_32x32_nor.png"];
    caButton.bounds = CGRectMake(0, 0, calearAllImage.size.width, calearAllImage.size.height);
    [caButton setImage:calearAllImage forState:UIControlStateNormal];
    [caButton addTarget:self action:@selector(checkToCleanVideInfo) forControlEvents:UIControlEventTouchUpInside];
    cleanAllButton = [[UIBarButtonItem alloc] initWithCustomView:caButton];
    
    NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects: saveButton, cleanAllButton, nil];
    [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
    
    [rightBarButtonArray release];
    [cleanAllButton release];
    
    [saveButton release];
    
    cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
	[[self navigationItem] setLeftBarButtonItem:cancelButton];
	[cancelButton release];
    
    serverInfoArray = [[NSMutableArray alloc] init];
    ctrlHlprArray = [[NSMutableArray alloc] init];
    isShowRecordingServerArray = [[NSMutableArray alloc] init];
    
    for (int i=0; i<[[_favoriteSite displayedObjects] count]; i++)
    {
        ServerInfo *sinfo = [[_favoriteSite displayedObjects] objectAtIndex:i];
        [serverInfoArray addObject:sinfo];
        [ctrlHlprArray addObject:[NSNull null]];
        [isShowRecordingServerArray addObject:[NSNumber numberWithBool:NO]];
    }
    
    // match the current controlhlpr(server handle)
    [serverMgr matchServerHandler:ctrlHlprArray refServerList:serverInfoArray];
}

- (void)dealloc
{
    [newViewInfo release];
    [serverInfoArray release];
    serverInfoArray = nil;
    [ctrlHlprArray release];
    ctrlHlprArray = nil;
    [isShowRecordingServerArray release];
    isShowRecordingServerArray = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (overlayView != nil) {
            overlayView.frame = self.tableView.bounds;
        }
        if (spinner != nil) {
            spinner.center = self.tableView.center;
        }
        [self.tableView reloadData];
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    if (overlayView != nil) {
        overlayView.frame = self.tableView.bounds;
    }
    if (spinner != nil) {
        spinner.center = self.tableView.center;
    }
    return;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES animated:NO];
}

- (void)startConnection {
    ServerInfo *sinfo = [serverInfoArray objectAtIndex:select_SrvIndex];
    ControlHelper *ctrlhlpr = [[ControlHelper alloc] init];
    [ctrlhlpr setServerInfo:sinfo];
    
    if ((sinfo.serverType == eServer_Default || sinfo.serverType == eServer_Crystal) &&
        [ctrlhlpr loginAndGetRecordingServerList]) {
        [ctrlHlprArray replaceObjectAtIndex:select_SrvIndex withObject:ctrlhlpr];
        [isShowRecordingServerArray replaceObjectAtIndex:select_SrvIndex withObject:[NSNumber numberWithBool:YES]];
        [self.tableView reloadData];
    }
    else if ([ctrlhlpr loginAndGetCameraList]) {
        [ctrlHlprArray replaceObjectAtIndex:select_SrvIndex withObject:ctrlhlpr];
        
        MyViewCameraController *cameraListView = [[MyViewCameraController alloc] initWithStyle:UITableViewStylePlain];
        cameraListView.ctrlhelpr = ctrlhlpr;
        cameraListView.myviewInfo = newViewInfo;
        cameraListView.isAdding = NO;
        if (sinfo.connectionType == eP2PConnection)
            [cameraListView setTitle:sinfo.serverName_P2P];
        else
            [cameraListView setTitle:sinfo.serverName];
        
        UINavigationController * newNavController = [[UINavigationController alloc]
                                                     initWithRootViewController:cameraListView];
        newNavController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
        [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
        
        [newNavController release];
        [cameraListView release];
    }
    else {
        // login failed
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Can't connect to server", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                            message:NSLocalizedString(@"Can't connect to server", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    [ctrlhlpr release];
    saveButton.enabled = YES;
    cancelButton.enabled = YES;
    [self releaseScreen];
}

- (void)stopConnection
{
    // stop the connection of some control helper which don't exit in final view info
    for (ControlHelper *ctrlhlpr in ctrlHlprArray) {
        if ((NSNull *)ctrlhlpr != [NSNull null]) {
            if (ctrlhlpr.isRunning)
                [ctrlhlpr logout];
            [ctrlhlpr release];
            [ctrlhlpr release]; // not sure that need to release twice
        }
    }
}

- (void)cancel {
    // remove the additional server handler
    [serverMgr removeServerHandlerInCurrentView:ctrlHlprArray];
    // logout the additional server handler
    [self stopConnection];
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)save
{
    if ([newViewInfo.serverList count] <= 0) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"The minimum camera number per MyView is 1 channel.", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"The minimum camera number per MyView is 1 channel.", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        return;
    }
    
    if ([self isChangeViewInfo]) {
        [serverMgr updateServerHandlerForNewView:newViewInfo refServerHandler:ctrlHlprArray];
    }
    else {
        [serverMgr removeServerHandlerInCurrentView:ctrlHlprArray];
    }
    [self stopConnection];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)checkToCleanVideInfo {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                                       message:NSLocalizedString(@"This button will reset all camera setting in each server to default, are you sure to continue?", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"YES", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self cleanVideInfo];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                         message:NSLocalizedString(@"This button will reset all camera setting in each server to default, are you sure to continue?", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
        [alert release];
    }
}

- (void)cleanVideInfo {
    for (int i = 0; i < [newViewInfo.serverList count]; i++) {
        MyViewServerInfo *current_sinfo = [newViewInfo.serverList objectAtIndex:i];
        [current_sinfo.cameraList removeAllObjects];
    }
    [newViewInfo.serverList removeAllObjects];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                                       message:NSLocalizedString(@"All setting are reset.", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                         message:NSLocalizedString(@"All setting are reset.", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                               otherButtonTitles:nil];
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
        [alert release];
    }
}

- (BOOL)isChangeViewInfo {
    if ([newViewInfo.serverList count] != [serverMgr.myViewBackupInfo.serverList count])
        return YES;
    
    for (int i = 0; i < [newViewInfo.serverList count]; i++) {
        MyViewServerInfo *current_sinfo = [newViewInfo.serverList objectAtIndex:i];
        MyViewServerInfo *backup_sinfo = [serverMgr.myViewBackupInfo.serverList objectAtIndex:i];
        
        if (![current_sinfo.serverID isEqualToString:backup_sinfo.serverID])
            return YES;
        if (![current_sinfo.userName isEqualToString:backup_sinfo.userName])
            return YES;
        if ([current_sinfo.cameraList count] != [backup_sinfo.cameraList count])
            return YES;
        
        for (int j = 0; j < [current_sinfo.cameraList count]; j++) {
            MyViewCameraInfo *current_cinfo = [current_sinfo.cameraList objectAtIndex:j];
            MyViewCameraInfo *backup_cinfo = [backup_sinfo.cameraList objectAtIndex:j];
            
            if (![current_cinfo.rsCentralID isEqualToString:backup_cinfo.rsCentralID] ||
                ![current_cinfo.rsLocalID isEqualToString:backup_cinfo.rsLocalID] ||
                ![current_cinfo.camCentralID isEqualToString:backup_cinfo.camCentralID] ||
                ![current_cinfo.camLocalID isEqualToString:backup_cinfo.camLocalID] ||
                ![current_cinfo.cameraName isEqualToString:backup_cinfo.cameraName] ||
                (current_cinfo.cameraOrder != backup_cinfo.cameraOrder))
                return YES;
        }
    }
    
    
    return NO;
}

- (void)holdScreen {
    // jerrylu, 2012/10/08, fix bug7760
    [self.tableView setScrollEnabled:NO];
    
    if (overlayView == nil) {
        overlayView = [[UIView alloc] init];
        overlayView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        overlayView.frame = self.tableView.bounds;
    }
    if (spinner == nil) {
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        spinner.center = self.tableView.center;
    }
    [overlayView addSubview:spinner];
    [spinner startAnimating];
    [self.tableView addSubview:overlayView];
}

- (void)releaseScreen {
    // jerrylu, 2012/10/08, fix bug7760
    [self.tableView setScrollEnabled:YES];
    
    [overlayView removeFromSuperview];
    [spinner removeFromSuperview];
    [spinner stopAnimating];
    
    if (overlayView != nil) {
        [overlayView release];
        overlayView = nil;
    }
    if (spinner != nil) {
        [spinner release];
        spinner = nil;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [ctrlHlprArray count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    section = section - 1;
    if (section < 0) {
        return 0;
    }
    
    if (![[isShowRecordingServerArray objectAtIndex:section] boolValue])
        return 0;
    
    ControlHelper *ctrlhlpr = [ctrlHlprArray objectAtIndex:section];
    return ctrlhlpr.m_vecServerList.size();
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        ControlHelper *ctrlhlpr = [ctrlHlprArray objectAtIndex:indexPath.section-1];
        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = WChartToNSString(ctrlhlpr.m_vecServerList.at(indexPath.row).name);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *) tableView heightForHeaderInSection:(NSInteger) section
{
    return 35.0;
}

- (UIView *)tableView:(UITableView*) tableView viewForHeaderInSection:(NSInteger) section
{
    float hwidth, hheight = 35.0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        hwidth = [[UIScreen mainScreen] bounds].size.width;
    }
    else {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            hwidth = [[UIScreen mainScreen] bounds].size.width;
        else
            hwidth = [[UIScreen mainScreen] bounds].size.height;
    }
    
    section = section - 1;
    if (section < 0) {
        UIView *customView = [[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, hwidth, hheight)] autorelease];
        UILabel *headerlabel = [[[UILabel alloc] initWithFrame:CGRectMake(5, 5.0, hwidth-5.0, 25.0)] autorelease];
        UIFont* font = [UIFont systemFontOfSize:16.0];
        [headerlabel setFont:font];
        headerlabel.textColor = [UIColor grayColor];
        [headerlabel setText:NSLocalizedString(@"Select Server", nil)];
        [customView addSubview:headerlabel];
        return customView;
    }
    
    UIView *customView = [[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, hwidth, hheight)] autorelease];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (section % 2 == 0) {
            [customView setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0]];
        }
        else {
            [customView setBackgroundColor:[UIColor colorWithRed:0.75 green:0.75 blue:0.75 alpha:1.0]];
        }
    }
    else {
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = customView.bounds;
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor lightGrayColor] CGColor], nil];
        [customView.layer insertSublayer:gradient atIndex:0];
    }
    
    UIButton *clearbutton = [[[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, hwidth, hheight)] autorelease];
    [clearbutton setBackgroundColor:[UIColor clearColor]];
    [clearbutton addTarget:self action:@selector(headerTapped:) forControlEvents:UIControlEventTouchUpInside];
    clearbutton.tag = section;
    
    UILabel *headerlabel = [[[UILabel alloc] initWithFrame:CGRectMake(40.0, 5.0, hwidth - 80.0, 25.0)] autorelease];
    UIFont* font = [UIFont systemFontOfSize:20.0];
	[headerlabel setFont:font];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        [headerlabel setTextColor:[UIColor blackColor]];
	else
        [headerlabel setTextColor:[UIColor blackColor]];
	[headerlabel setBackgroundColor:[UIColor clearColor]];
    if ([[serverInfoArray objectAtIndex:section] connectionType] == eIPConnection)
        [headerlabel setText:[[serverInfoArray objectAtIndex:section] serverName]];
    else
        [headerlabel setText:[[serverInfoArray objectAtIndex:section] serverName_P2P]];
    
    UIImage *image;
    if ([[isShowRecordingServerArray objectAtIndex:section] boolValue])
        image = [UIImage imageNamed:@"indicationDown.png"];
    else
        image = [UIImage imageNamed:@"indicationUp.png"];
    UIButton *imgbutton = [[[UIButton alloc] initWithFrame: CGRectMake(5.0, 5.0, image.size.width, image.size.height)] autorelease];
    [imgbutton setImage:image forState:UIControlStateNormal];
    imgbutton.tag = section;
    [imgbutton addTarget:self action:@selector(headerTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [customView addSubview:clearbutton];
	[customView addSubview:imgbutton];
    [customView addSubview:headerlabel];
    
    return customView;
}

- (void)headerTapped:(UIButton*) sender
{
    UIButton *btnSection = (UIButton *)sender;
    int server_index = btnSection.tag;
    
    if ([NSNull null] == [ctrlHlprArray objectAtIndex:server_index]) {
        [self holdScreen];
        saveButton.enabled = NO;
        cancelButton.enabled = NO;
        select_SrvIndex = server_index;
        NSThread *m_thread = [[NSThread alloc] initWithTarget:self selector:@selector(startConnection) object:NULL];
        [m_thread start];
    }
    else {
        ServerInfo *sinfo = [serverInfoArray objectAtIndex:server_index];
        if (sinfo.serverType == eServer_Crystal) {
            if (![[isShowRecordingServerArray objectAtIndex:server_index] boolValue])
                [isShowRecordingServerArray replaceObjectAtIndex:server_index withObject:[NSNumber numberWithBool:YES]];
            else
                [isShowRecordingServerArray replaceObjectAtIndex:server_index withObject:[NSNumber numberWithBool:NO]];
        
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:server_index+1] withRowAnimation:UITableViewRowAnimationFade];
        }
        else {
            ControlHelper *ctrlhlpr = [ctrlHlprArray objectAtIndex:server_index];
            MyViewCameraController *cameraListView = [[MyViewCameraController alloc] initWithStyle:UITableViewStylePlain];
            cameraListView.ctrlhelpr = ctrlhlpr;
            cameraListView.myviewInfo = newViewInfo;
            cameraListView.isAdding = NO;
            if (sinfo.connectionType == eP2PConnection)
                [cameraListView setTitle:sinfo.serverName_P2P];
            else
                [cameraListView setTitle:sinfo.serverName];
            
            UINavigationController * newNavController = [[UINavigationController alloc]
                                                         initWithRootViewController:cameraListView];
            newNavController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
            [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
            
            [newNavController release];
            [cameraListView release];
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyViewCameraController *cameraListView = [[MyViewCameraController alloc] initWithStyle:UITableViewStylePlain];
    ControlHelper *ctrlhlpr = [ctrlHlprArray objectAtIndex:indexPath.section-1];
    cameraListView.ctrlhelpr = ctrlhlpr;
    cameraListView.myviewInfo = newViewInfo;
    cameraListView.recordingServerCID = ctrlhlpr.m_vecServerList.at(indexPath.row).ID.centralID;
    cameraListView.recordingServerLID = ctrlhlpr.m_vecServerList.at(indexPath.row).ID.localID;
    cameraListView.isAdding = NO;
    [cameraListView setTitle:WChartToNSString(ctrlhlpr.m_vecServerList.at(indexPath.row).name)];
    
    UINavigationController * newNavController = [[UINavigationController alloc]
                                                 initWithRootViewController:cameraListView];
    newNavController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
    
    [newNavController release];
    [cameraListView release];
}

#pragma mark -
#pragma mark AlertView Methods
- (void)alertView:(UIAlertView *) alertView didDismissWithButtonIndex:(NSInteger) buttionIndex {
    if (buttionIndex == 1) {
        [self cleanVideInfo];
    }
    return;
}

@end
