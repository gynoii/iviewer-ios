//
//  LiveViewAppDelegate.h
//  LiveView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "EventAlertView.h"

#define MAX_EVENTCOUNT_SERVER 20
#define MAX_SHOWALERT_COUNT 60
#define NOTIFICATION_STOP_QUERY_THREAD @"stopquerythread"

#define SYSTEM_VERSION_EQUAL_TO(v)      ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_4_INCH (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] nativeScale] > 2.1)

#if CUSTOMER_FOR_SEAGATE
#define COLOR_NAVIGATIONBAR         [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.0]
#define COLOR_NAVIGATIONBARTITLE    [UIColor blackColor]
#define COLOR_TOOLBAR               COLOR_NAVIGATIONBAR
#define COLOR_NAVBUTTONTINT         [UIColor darkGrayColor]//[UIColor colorWithRed:0.1 green:0.5 blue:0.96 alpha:1.0]
#define COLOR_NAVBUTTONTINTPAD      [UIColor darkGrayColor]
#define COLOR_NAVBUTTONTINTIOS6     [UIColor lightGrayColor]
#define COLOR_NAVBUTTONTINTPADIOS6  [UIColor lightGrayColor]
#define COLOR_TOOLBUTTONTINT        COLOR_NAVBUTTONTINT
#define COLOR_TOOLBUTTONTINTIOS6    COLOR_NAVBUTTONTINTIOS6
#define COLOR_MYSERVERSECHEADER     [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:0.8]
#define COLOR_MYVIEWSECHEADER       [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:0.6]
#define COLOR_IOS6SITESECTION1      [UIColor whiteColor]
#define COLOR_IOS6SITESECTION2      [UIColor lightGrayColor]
#define COLOR_BACKGROUND            [UIColor lightGrayColor]
#define COLOR_CAMERANAMECOLOR       [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0]
#define COLOR_CAMERASPINNERCOLOR    [UIColor darkGrayColor]
#define STYLE_STATUSBAR             UIStatusBarStyleDefault
#else
#define COLOR_NAVIGATIONBAR         [UIColor blackColor]
#define COLOR_NAVIGATIONBARTITLE    [UIColor whiteColor]
#define COLOR_TOOLBAR               COLOR_NAVIGATIONBAR
#define COLOR_NAVBUTTONTINT         [UIColor lightTextColor]
#define COLOR_NAVBUTTONTINTPAD      [UIColor darkTextColor]
#define COLOR_NAVBUTTONTINTIOS6     [UIColor blackColor]
#define COLOR_NAVBUTTONTINTPADIOS6  [UIColor blackColor]
#define COLOR_TOOLBUTTONTINT        COLOR_NAVBUTTONTINT
#define COLOR_TOOLBUTTONTINTIOS6    COLOR_NAVBUTTONTINTIOS6
#define COLOR_MYSERVERSECHEADER     [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8]
#define COLOR_MYVIEWSECHEADER       [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]
#define COLOR_IOS6SITESECTION1      [UIColor grayColor]
#define COLOR_IOS6SITESECTION2      [UIColor blackColor]
#define COLOR_BACKGROUND            [UIColor blackColor]
#define COLOR_CAMERANAMECOLOR       [UIColor whiteColor]
#define COLOR_CAMERASPINNERCOLOR    [UIColor whiteColor]
#define STYLE_STATUSBAR             UIStatusBarStyleLightContent
#endif

#define PopoverSizeWidth  320
#define PopoverSizeHeight 480

@class UIApplication;
@class FavoriteSiteController;
@class ControlHelper;
@class LogoPage;
@class ServerInfo;
@class MyViewInfo;

@interface LiveViewAppDelegate : NSObject <UIApplicationDelegate, EventAlertViewDelegate> {
    UIWindow *window;
    UINavigationController *navigationController;
	FavoriteSiteController *_favoriteSiteController;
	NSString * strUUID;
	BOOL bDelayMsg;
#if FUNC_PUSH_NOTIFICATION_SUPPORT
    NSString *strToken;
    NSMutableArray *alertlist;
#endif
    NSMutableArray *eventlist;
    NSCondition *eventlistCondition;
    LogoPage *logoview;    
}

- (void)showServerDownMessage:(int) eventID;
- (void)showQuotaTimeoutMessage:(BOOL) outofquota;
- (void)popToRootView;
- (void)saveEventList;
- (BOOL)removeEventListByServerInfo:(ServerInfo *) sinfo;
- (void)dismissAllEvent;
- (void)registerPushNotification;
- (void)reconnectToServer:(ServerInfo *) sinfo;
- (void)reconnectToMyView:(MyViewInfo *) vinfo;
- (int)getUnreadEventCount;
- (NSMutableArray *)getEventListFromServer:(NSString *)sid User:(NSString *)uname;
- (void)setReadEvent:(Eventinfo *) einfo;

@property (nonatomic, retain) FavoriteSiteController * favoriteSiteController;
@property (nonatomic, retain) IBOutlet UIWindow * window;
@property (nonatomic, retain) IBOutlet UINavigationController * navigationController;
@property (nonatomic, assign) NSString * strUUID;
@property (nonatomic, assign) BOOL bDelayMsg;
#if FUNC_PUSH_NOTIFICATION_SUPPORT
@property (nonatomic, assign) NSString *strToken;
#endif
@end

