#import <UIKit/UIKit.h>
#import "SiteListController.h"
#import "EditableDetailCell.h"
#import "basicInfo.h"

@interface UserLoginController : UITableViewController <UITextFieldDelegate> {
    SiteListController * _pParent;
    EditableDetailCell * _accountCell;
    EditableDetailCell * _passwordCell;
    NSIndexPath *selectedIndex;
    BOOL _bLoginSuccess;
    Basicinfo* _basicInfo;
    BOOL _isSelectLogin;
}

@property (nonatomic, assign) SiteListController * parent;
@property (nonatomic, retain) EditableDetailCell * accountCell;
@property (nonatomic, retain) EditableDetailCell * passwordCell;
@property (assign) BOOL bLoginSuccess;
@property (nonatomic, retain) NSIndexPath * selectedIndex;
@property (nonatomic, assign) Basicinfo * basicInfo;

@end
