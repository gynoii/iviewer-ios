//
//  MyViewCameraController.h
//  iViewer
//
//  Created by NUUO on 13/10/4.
//
//

#import <UIKit/UIKit.h>
#import "myviewInfo.h"
#import "ControlHelper.h"

@interface MyViewCameraController : UITableViewController<UIAlertViewDelegate> {
    MyViewInfo * _myviewInfo;
    ControlHelper * _ctrlhelpr;
    
    int myviewInfoServerIndex;
    int allSensorCount;
    NSMutableArray * allCamCentralIdArray;
    NSMutableArray * allCamLocalIdArray;
    NSMutableArray * allCameraNameArray;
    NSMutableArray * allSwitchArray;
    
    BOOL _isAdding;
    BOOL _isSelectAll;
    UIBarButtonItem *saveButton;
    UIBarButtonItem *backButton;
    
    unsigned long long recordingServerCID;
    unsigned long long recordingServerLID;
}

@property (nonatomic, assign) MyViewInfo * myviewInfo;
@property (nonatomic, assign) ControlHelper * ctrlhelpr;
@property (assign) unsigned long long recordingServerCID;
@property (assign) unsigned long long recordingServerLID;
@property (assign) BOOL isAdding;
@end
