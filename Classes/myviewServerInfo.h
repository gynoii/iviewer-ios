//
//  myviewCameraInfo.h
//  TabView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright 2010 com.debug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyViewServerInfo : NSObject {
    NSString * serverID;
    NSString * userName;
    NSMutableArray * cameraList;
}
@property (nonatomic, retain) NSString * serverID;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSMutableArray * cameraList;

- (id)initWithDictionary:(NSDictionary *) dictionary;
- (NSArray *)getKeys;
- (id)getPropertyDic;

@end
