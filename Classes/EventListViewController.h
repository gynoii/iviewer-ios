//
//  EventListViewController.h
//  iViewer
//
//  Created by NUUO on 12/8/9.
//
//

#import <UIKit/UIKit.h>
#import "eventinfo.h"

@interface EventListViewController : UITableViewController {
    id eventviewDelegate;
    NSMutableArray *eventList;
}

@property (nonatomic, assign) id eventviewDelegate;

- (void)eventListUpdate;
@end

@protocol EventListContorlPlayDelegate
@optional
- (void)EventListContorlPlay:(Eventinfo *) einfo;
@end
