//
//  PlaybackViewController.h
//  iViewer
//
//  Created by Jerry Lu on 12/4/20.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "serverInfo.h"

#define kDefaultTimeInterval 30
#define kPlaybackTimeInterval1 kDefaultTimeInterval
#define kPlaybackTimeInterval2 60
#define kPlaybackTimeInterval3 120
#define kPlaybackTimeInterval4 300
#define kPlaybackTimeInterval5 600

typedef enum E_PLAYBACK_TIME_INTERVAL {
    ePlaybackTimeIntervalDefault = 0,
	ePlaybackTimeInterval1 = ePlaybackTimeIntervalDefault,
	ePlaybackTimeInterval2,
	ePlaybackTimeInterval3,
    ePlaybackTimeInterval4,
    ePlaybackTimeInterval5,
    ePlaybackTimeIntervalMax
} EPlaybackTimeInterval;

typedef enum E_PLAYBACK_OPTION_TYPE {
	ePlaybackOptionNone = -1,
	ePlaybackOptionPeriod,
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    ePlaybackOptionRecordFile,
#endif
	ePlaybackOptionPlay,
    ePlaybackOptionMax
} ePlaybackOptionType;

@interface PlaybackViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    id singleViewDelegate; 
    NSDate *pickupdate;
    UIDatePicker *pDatePicker;
    UIDatePicker *pTimePicker;
    UITableView *pActionTableView;
    EPlaybackTimeInterval kPlaybackTimeInterval;
    int kPlaybackRecordfile;
    EServerType serverType;
}

@property (nonatomic, assign) id singleViewDelegate;
@property (nonatomic, retain) NSDate *pickupdate;
@property (nonatomic, retain) IBOutlet UIDatePicker *pDatePicker;
@property (nonatomic, retain) IBOutlet UIDatePicker *pTimePicker;
@property (nonatomic, retain) IBOutlet UITableView *pActionTableView;

- (NSDate *)getPickDate;
- (int)getPickTimeInterval;
- (void)setPlaybackTimeInterval:(EPlaybackTimeInterval) timeinterval;
- (int)getSelectedRecordfile;
- (void)setSelectedRecordfile:(int) recordfile;
@end

@protocol PlaybackViewControllerDelegate
@optional
- (void)PbViewControllerOpenRecord;
@end
