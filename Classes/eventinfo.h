//
//  eventinfo.h
//  iViewer
//
//  Created by NUUO on 12/8/9.
//
//

#import <UIKit/UIKit.h>

@interface Eventinfo : NSObject {
    NSString *eventType;
    NSString *serverID;
    NSString *cameraID;
    NSString *username;
    NSDate *eventDate;
    NSString *eventMsg;
    NSInteger isNotRead;
}

@property (nonatomic, retain) NSString * eventType;
@property (nonatomic, retain) NSString * serverID;
@property (nonatomic, retain) NSString * cameraID;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSDate * eventDate;
@property (nonatomic, retain) NSString * eventMsg;
@property (nonatomic) NSInteger isNotRead;

- (id)initWithDictionary:(NSDictionary *) dictionary;
- (NSArray *)getKeys;
- (id)getPropertyDic;

@end
