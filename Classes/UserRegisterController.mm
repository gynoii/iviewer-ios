#import "UserRegisterController.h"
#import "LiveViewAppDelegate.h"

@implementation UserRegisterController

@synthesize parent = _parent;
@synthesize accountCell = _accountCell;
@synthesize passwordCell = _passwordCell;
@synthesize confirmCell = _confirmCell;
@synthesize nameCell = _nameCell;
@synthesize countryCell = _countryCell;
@synthesize companyCell = _companyCell;
@synthesize telephoneCell = _telephoneCell;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = NSLocalizedString(@"Add Account", nil);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];

    [[self navigationItem] setLeftBarButtonItem:backButton];
    [backButton release];
    
    EditableDetailCell *accountcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:0 detailcell:accountcell];
    [self setAccountCell:accountcell];

    EditableDetailCell *passwordcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:0 detailcell:passwordcell];
    [self setPasswordCell:passwordcell];
  
    EditableDetailCell *confirmcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:0 detailcell:confirmcell];
    [self setConfirmCell:confirmcell];
    
    EditableDetailCell *namecell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:0 detailcell:namecell];
    [self setNameCell:namecell];
    
    EditableDetailCell *countrycell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:0 detailcell:countrycell];
    [self setCountryCell:countrycell];
    
    EditableDetailCell *companycell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:0 detailcell:companycell];
    [self setCompanyCell:companycell];
    
    EditableDetailCell *telephonecell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:0 detailcell:telephonecell];
    [self setTelephoneCell:telephonecell];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        }
        else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
    }
    else {
        [self resizeViewWithOrientation:self.interfaceOrientation];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_accountCell release];
    [_passwordCell release];
    [_confirmCell release];
    [_nameCell release];
    [_countryCell release];
    [_companyCell release];
    [_telephoneCell release];

    [super dealloc];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (size.width > size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        } else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self resizeViewWithOrientation:toInterfaceOrientation];
}

-(void)resizeViewWithOrientation:(UIInterfaceOrientation) orientation {
    CGRect rect;
    UIFontDescriptor *currentDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleBody];
    CGFloat bodyTextSize = [currentDescriptor pointSize];
    
    if (IS_IPAD) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
                rect = CGRectMake(20.0, 10.0, 900.0, bodyTextSize+8.0);
            }
            else {
                rect = CGRectMake(20.0, 10.0, 640.0, bodyTextSize+8.0);
            }
        }
        else {
            if (UIInterfaceOrientationIsPortrait(orientation)) {
                rect = CGRectMake(20.0, 10.0, 640.0, 24.0);
            }
            else {
                rect = CGRectMake(20.0, 10.0, 900.0, 24.0);
            }
        }
    }
    else {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, bodyTextSize+8.0);
        }
        else {
            if (UIInterfaceOrientationIsPortrait(orientation)) {
                rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, 24.0);
            }
            else {
                rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.height - 60, 24.0);
            }
        }
    }
    _accountCell.textField.frame = rect;
    _passwordCell.textField.frame = rect;
    _confirmCell.textField.frame = rect;
    _nameCell.textField.frame = rect;
    _companyCell.textField.frame = rect;
    _countryCell.textField.frame = rect;
    _telephoneCell.textField.frame = rect;
    
    [self.view setNeedsDisplay];
}


- (BOOL) validateEmail: (NSString *) email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    
    return isValid;
}

- (void) showMessage: (NSString *) content {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:content preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                        message:content
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (BOOL)checkData {
    if ([[[_accountCell textField] text] length] == 0) {
        [self showMessage:@"Please input Email field."];
        
        return FALSE;
    }
    
    if ([self validateEmail:[[_accountCell textField] text]] == FALSE)
    {
        [self showMessage:@"Email format wrong."];
        
        return FALSE;
        
    }
    
    if ([[[_passwordCell textField] text] length] == 0) {
        [self showMessage:@"Please input Password field."];
        
        return FALSE;
    }
    
    if ([[[_confirmCell textField] text] length] == 0) {
        [self showMessage:@"Please input Confirm Password field."];
        
        return FALSE;
    }
    
    if ([[[_confirmCell textField] text] compare:[[_passwordCell textField] text]]) {
        [self showMessage:@"Password is not the same."];
        
        return FALSE;
    }
    
    return TRUE;
}

- (BOOL)startRegister {
    NSString *url = [[[NSString alloc] initWithFormat:@"http://54.251.107.11/UserAccount/nuuo/register.php"] autorelease];
    NSString* post = [NSString stringWithFormat:@"email=%s&password=%s&name=%s&country=%s&company=%s&phone=%s",
                                       [[[_accountCell textField] text] UTF8String], [[[_passwordCell textField] text] UTF8String],
                                       [[[_nameCell textField] text] UTF8String], [[[_companyCell textField] text] UTF8String],
                                       [[[_countryCell textField] text] UTF8String], [[[_telephoneCell textField] text] UTF8String]];
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSData* postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLen = [NSString stringWithFormat:@"%d", [postData length]];
    [urlRequest setValue:postLen forHTTPHeaderField:@"Content-Length"];
    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error;
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    BOOL bReuslt = FALSE;
    NSString *replyString;
    if (serverReply != nil) {
        replyString = [[[NSString alloc] initWithBytes:[serverReply bytes] length:[serverReply length] encoding: NSASCIIStringEncoding] autorelease];
        
        if ([replyString rangeOfString:@"already"].location != NSNotFound)
        {
            [self showMessage:@"This Account has been registeed, please try again."];
        }
        else
        {
            bReuslt = TRUE;
        }
    }
    else {
        //replyString = [[[NSString alloc] initWithFormat:@"Unconnection"] autorelease];
        [self showMessage:@"Can not access to the cloud\nPlease connect to Internet."];
    }
    NSLog(@"reply string is :%@",replyString);
    
    return bReuslt;
}

- (void)back {
    [self dismissViewControllerAnimated:YES completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
    //[[self navigationController] popViewControllerAnimated:YES];
}

- (void)newDetailCellWithTag:(NSInteger) tag detailcell:(EditableDetailCell *) detailcell {
    [[detailcell textField] setDelegate:self];
    [[detailcell textField] setTag:tag];
}

#pragma mark - UITextFieldDelegate Protocol

//  Sets the label of the keyboard's return key to 'Done' when the insertion
//  point moves to the table view's last field.
//
- (BOOL)textFieldShouldBeginEditing:(UITextField *) textField {
    UITableViewCell *cell = (UITableViewCell*) [[textField superview] superview];
    [[self tableView] scrollToRowAtIndexPath:[[self tableView] indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [textField setReturnKeyType:UIReturnKeyDone];
    
    return YES;
}
//  UITextField sends this message to its delegate after resigning
//  firstResponder status. Use this as a hook to save the text field's
//  value to the corresponding property of the model object.
//

- (void)textFieldDidEndEditing:(UITextField *) textField {
    
}

//  UITextField sends this message to its delegate when the return key
//  is pressed. Use this as a hook to navigate back to the list view
//  (by 'popping' the current view controller, or dismissing a modal nav
//  controller, as the case may be).
//
//  If the user is adding a new item rather than editing an existing one,
//  respond to the return key by moving the insertion point to the next cell's
//  textField, unless we're already at the last cell.
//
- (BOOL)textFieldShouldReturn:(UITextField *) textField {
	
    if ([textField returnKeyType] != UIReturnKeyDone) {
        //  If this is not the last field (in which case the keyboard's
        //  return key label will currently be 'Next' rather than 'Done'),
        //  just move the insertion point to the next field.
        //
        //  (See the implementation of -textFieldShouldBeginEditing: above.)
        //
        NSInteger nextTag = [textField tag] + 1;
        
        UIView *nextTextField = [[self tableView] viewWithTag:nextTag];
        
        [nextTextField becomeFirstResponder];
    }
	else {
		[textField resignFirstResponder];
    }
	
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 7;
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            EditableDetailCell * cell = [self accountCell];
            
            UITextField * textField = [cell textField];
            [textField setTag:0];
            [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
            [textField setPlaceholder:NSLocalizedString(@"Account/Email(Required)", nil)];
            return cell;
        }
        else if (indexPath.row == 1) {
            EditableDetailCell * cell = [self passwordCell];
            
            UITextField * textField = [cell textField];
            [textField setSecureTextEntry:YES];
            [textField setPlaceholder:NSLocalizedString(@"Password(Required)", nil)];
            return cell;
        }
        else if (indexPath.row == 2) {
            EditableDetailCell * cell = [self confirmCell];
            
            UITextField * textField = [cell textField];
            [textField setSecureTextEntry:YES];
            [textField setPlaceholder:NSLocalizedString(@"Confirm Password(Required)", nil)];
            return cell;
        }
        else if (indexPath.row == 3) {
            EditableDetailCell * cell = [self nameCell];
            
            UITextField * textField = [cell textField];
            [textField setPlaceholder:NSLocalizedString(@"Name", nil)];
            return cell;
        }
        else if (indexPath.row == 4) {
            EditableDetailCell * cell = [self countryCell];
            
            UITextField * textField = [cell textField];
            [textField setPlaceholder:NSLocalizedString(@"Country", nil)];
            return cell;
        }
        else if (indexPath.row == 5) {
            EditableDetailCell * cell = [self companyCell];
            
            UITextField * textField = [cell textField];
            [textField setPlaceholder:NSLocalizedString(@"Company", nil)];
            return cell;
        }
        else if (indexPath.row == 6) {
            EditableDetailCell * cell = [self telephoneCell];
            
            UITextField * textField = [cell textField];
            [textField setPlaceholder:NSLocalizedString(@"Telephone", nil)];
            return cell;
        }
    }
    else if (indexPath.section == 1) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.textLabel.text = NSLocalizedString(@"Confirm", nil);
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            [cell setBackgroundColor:[UIColor blueColor]];
        }
        return cell;
    }

    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
        if ([self checkData] == TRUE) {
            if ([self startRegister] == TRUE) {
                [self back];
            }
        }
    }
}
@end
