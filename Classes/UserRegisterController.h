#import <UIKit/UIKit.h>
#import "UserLoginController.h"
#import "EditableDetailCell.h"

@interface UserRegisterController : UITableViewController <UITextFieldDelegate> {
    UserLoginController * _parent;
    EditableDetailCell * _accountCell;
    EditableDetailCell * _passwordCell;
    EditableDetailCell * _confirmCell;
    EditableDetailCell * _nameCell;
    EditableDetailCell * _countryCell;
    EditableDetailCell * _companyCell;
    EditableDetailCell * _telephoneCell;
}

@property (nonatomic, assign) UserLoginController * parent;
@property (nonatomic, retain) EditableDetailCell * accountCell;
@property (nonatomic, retain) EditableDetailCell * passwordCell;
@property (nonatomic, retain) EditableDetailCell * confirmCell;
@property (nonatomic, retain) EditableDetailCell * nameCell;
@property (nonatomic, retain) EditableDetailCell * countryCell;
@property (nonatomic, retain) EditableDetailCell * companyCell;
@property (nonatomic, retain) EditableDetailCell * telephoneCell;

@end
