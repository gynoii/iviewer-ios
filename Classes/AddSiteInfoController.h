//
//  AddSiteInfoController.h
//  iViewer
//
//  Created by NUUO on 13/8/14.
//
//

#import <UIKit/UIKit.h>
#import "SiteListController.h"

typedef enum E_ADD_FUNC {
	eAdd_Server = 0,
	eAdd_View,
    eAdd_Max
} EAddFunc;

@interface AddSiteInfoController : UITableViewController <UINavigationControllerDelegate> {
    SiteListController * _favoriteSite;
}

@property (nonatomic, assign) SiteListController * favoriteSite;

@end
