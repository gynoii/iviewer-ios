//
//  SlideViewMenuController.mm
//  iViewer
//
//  Created by Jerry Lu on 12/8/1.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import "SlideViewMenuController.h"
#import "LiveViewAppDelegate.h"
#import "SlideView.h"
#import "SingleCamViewController.h"
#import "DOTableViewController.h"
#import "LiveViewEventListViewController.h"

@implementation SlideViewMenuController

@synthesize serverMgr;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        parent = nil;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style parentview:(id)parentview
{
    self = [super initWithStyle:style];
    if (self) {
        parent = parentview;
    }
    return self;
}

- (void)viewDidLoad
{
    if (IS_IPAD) {
        self.preferredContentSize = CGSizeMake(PopoverSizeWidth, PopoverSizeHeight);
    }
    if ((NSNull *)parent != [NSNull null]) {
        if ([parent isKindOfClass:[SingleCamViewController class]]) {
            SingleCamViewController *singlecam = (SingleCamViewController *)parent;
            NSMutableArray *diDeviceNameList = [serverMgr getDIDeviceNameByChIndex:[singlecam getCurrentChIndex]];
            NSMutableArray *doDeviceNameList = [serverMgr getDODeviceNameByChIndex:[singlecam getCurrentChIndex]];
            if ([diDeviceNameList count] == 0 && [doDeviceNameList count] == 0)
                isShowDIO = NO;
            else
                isShowDIO = YES;
        }
        else {
            NSMutableArray *diDeviceNameList = [serverMgr getDIDeviceNameList];
            NSMutableArray *doDeviceNameList = [serverMgr getDODeviceNameList];
            if ([diDeviceNameList count] == 0 && [doDeviceNameList count] == 0)
                isShowDIO = NO;
            else
                isShowDIO = YES;
        }
    }
    [super viewDidLoad];
}

- (void)dealloc
{
    [super dealloc];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    serverMgr = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (IS_IPAD) {
        UIBarButtonItem * CloseButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(dismissPopover)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [CloseButton setTintColor:COLOR_NAVBUTTONTINTPAD];
        }
        else {
            [CloseButton setTintColor:COLOR_NAVBUTTONTINTPADIOS6];
        }
        self.navigationItem.leftBarButtonItem = CloseButton;
        [CloseButton release];
    }
    else {
        //set back button title
#if 1
        UIImage *backImage;
        UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
        else
            backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
        bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
        [bButton setImage:backImage forState:UIControlStateNormal];
        [bButton addTarget:self action:@selector(popview) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(popview)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [backButton setTintColor:COLOR_NAVBUTTONTINT];
        }
        else {
            [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
        }
#endif
        self.navigationItem.leftBarButtonItem = backButton;
        [backButton release];
        
        [[self navigationController] setNavigationBarHidden:NO animated:NO];
        [[self navigationController] setToolbarHidden:NO animated:NO];
    }
#if FUNC_PUSH_NOTIFICATION_SUPPORT
    pnServerSwitch.on = [serverMgr getPushNotificationEnableByChIndex:0];
#endif
}

- (void)popview {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (overlayView != nil) {
            overlayView.frame = self.tableView.bounds;
        }
        if (spinner != nil) {
            spinner.center = self.tableView.center;
        }
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    if (overlayView != nil) {
        overlayView.frame = self.tableView.bounds;
    }
    if (spinner != nil) {
        spinner.center = self.tableView.center;
    }
    return;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (serverMgr.isMyViewMode) {
        if (isShowDIO)
            return 1;
        else
            return 0;
    }
    else {
        // jerrylu, 2012/08/20, if no privilege of DI/DO, just show Push.
        if ([serverMgr getServerTypeBySrvIndex:0] == eServer_Titan ||
            [serverMgr getServerTypeBySrvIndex:0] == eServer_Crystal) {
            if (isShowDIO)
                return 1;
            else
                return 0;
        }
        else {
            if (isShowDIO)
                return eSlideViewFunctionMax;
            else
                return eSlideViewFunctionMax - 1;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isShowDIO == NO)
        section += 1;
    
    switch (section) {
        case eSlideViewFunctionDIDO:
            return 1;
        case eSlideViewFunctionPushNotification:
            return 2;
        default:
            break;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger) section {
    if (isShowDIO == NO)
        section += 1;
    
    switch (section) {
        case eSlideViewFunctionDIDO:
            return NSLocalizedString(@"Digital Output/Input Devices", nil);
        case eSlideViewFunctionPushNotification:
            return NSLocalizedString(@"Push Notification", nil);
        default:
            break;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    int section = indexPath.section;
    if (isShowDIO == NO)
        section += 1;
    
    switch (section) {
#if FUNC_PUSH_NOTIFICATION_SUPPORT
        case eSlideViewFunctionPushNotification:
        {
            if (indexPath.row == 0) {
                if (cell == nil) {
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                    
                    UISwitch * cellSwitch = [[[UISwitch alloc] init] autorelease];
                    pnServerSwitch = cellSwitch;
                    cellSwitch.on = [serverMgr getPushNotificationEnableByChIndex:0]; // use the first control helper
                    [cellSwitch addTarget:self action:@selector(pnServerSwitchChanged) forControlEvents:UIControlEventValueChanged];
                    
                    cell.accessoryView = cellSwitch;
                    cell.textLabel.text = PUSH_NOTIFICATION_SERVER_NAME;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }            
            }
            else {
                if (cell == nil) {
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                    cell.textLabel.text = NSLocalizedString(@"Push Notification Event List", nil);
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
            }
            break;
        }
#endif
        case eSlideViewFunctionDIDO:
        {
            if (cell == nil) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                cell.textLabel.text = NSLocalizedString(@"Digital Output/Input", nil);
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;                
            }
            break;
        }
        default:
            break;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    if (isShowDIO == NO)
        section += 1;
    
    switch (section) {
        case eSlideViewFunctionDIDO:
        {
            DOTableViewController *doTable = [[DOTableViewController alloc] initWithStyle:UITableViewStylePlain];
            [doTable setTitle:NSLocalizedString(@"Digital Output", nil)];
            [doTable setParent:parent];
            [doTable setServerMgr:serverMgr];
            [self.navigationController pushViewController:doTable animated:YES];
            [doTable release];
            break;
        }
#if FUNC_PUSH_NOTIFICATION_SUPPORT
        case eSlideViewFunctionPushNotification:
        {
            if (indexPath.row == 1) {
                LiveViewEventListViewController *elistview = [[[LiveViewEventListViewController alloc] initWithStyle:UITableViewStylePlain serverID:[serverMgr getFilterPushNotificationServerID] userName:[serverMgr getFilterPushNotificationUserName]] autorelease];
                [elistview setLiveViewDelegate:parent];
                [elistview setServerMgr:serverMgr];
                if (IS_IPAD) {
                    if ([parent isKindOfClass:[UIViewController class]]) {
                        UIViewController * parentview = (UIViewController *)parent;
                        [parentview.navigationController pushViewController:elistview animated:YES];
                    }                   
                }
                else {
                    [self.navigationController pushViewController:elistview animated:YES];
                }
            }
            break;
        }
#endif
        default:
            break;
    }
}

#pragma mark - General methods

#if FUNC_PUSH_NOTIFICATION_SUPPORT
-(void) pnServerSwitchChanged {
    [self performSelectorOnMainThread:@selector(holdScreen) withObject:nil waitUntilDone:NO];
    
    NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(setServerPushNotification) object:nil];
    [thread start];
    [thread release];
}

-(void) setServerPushNotification {
    if ([serverMgr getPushNotificationEnableByChIndex:0] == YES) {
        [serverMgr setPushNotificationEnableByChIndex:0 enable:NO];
    }
    else {
        [serverMgr setPushNotificationEnableByChIndex:0 enable:YES];
    }
    
    [self performSelectorOnMainThread:@selector(pnServerUpdateSwitch) withObject:nil waitUntilDone:NO];
    [self performSelectorOnMainThread:@selector(releaseScreen) withObject:nil waitUntilDone:NO];
}

-(void) pnServerUpdateSwitch {
    pnServerSwitch.on = [serverMgr getPushNotificationEnableByChIndex:0];
}
#endif

- (void)holdScreen {
    // jerrylu, 2012/10/08, fix bug7760
    [self.tableView setScrollEnabled:NO];
    
    if (overlayView == nil) {
        overlayView = [[UIView alloc] init];
        overlayView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        overlayView.frame = self.tableView.bounds;
    }
    if (spinner == nil) {
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        spinner.center = self.tableView.center;
    }
    [overlayView addSubview:spinner];
    [spinner startAnimating];
    [self.tableView addSubview:overlayView];
}

- (void)releaseScreen {
    // jerrylu, 2012/10/08, fix bug7760
    [self.tableView setScrollEnabled:YES];
    
    [overlayView removeFromSuperview];
    [spinner removeFromSuperview];
    [spinner stopAnimating];
    
    if (overlayView != nil) {
        [overlayView release];
        overlayView = nil;
    }
    if (spinner != nil) {
        [spinner release];
        spinner = nil;
    }
}

- (void)dismissPopover {
    if (IS_IPAD) {
        if ([parent isKindOfClass:[SingleCamViewController class]])
            [parent dismissLiveViewMenuPopover];
        else
            [parent dismissPopover];
    }
    return;
}

@end
