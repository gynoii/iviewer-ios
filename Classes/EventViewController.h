//
//  EventViewController.h
//  iViewer
//
//  Created by Jerry Lu on 12/7/25.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PinchSlideImageView.h"
#import "MyGLView.h"
#import "ServerManager.h"
#import "SiteListController.h"
#import "EventListViewController.h"

#define kDefaultEvnetTimeInterval 10
#define kDefaultPlaybackSpeed 1.0
#define MAX_ZOOM_RATIO_NUM 12
#define ZOOM_RATIO 1.1
#define kDefaultPlaybackTimeout 60
#define HIDE_UI_INTERVAL 3

@class SiteListController;
@class PinchSlideImageView;
@interface EventViewController : UIViewController <PinchSlideImageViewDelegate, MyGLViewGestureDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverControllerDelegate, UIAlertViewDelegate, ServerManagerEventDelegate, EventListContorlPlayDelegate, UIActionSheetDelegate> {
#if RENDER_BY_OPENGL
    MyGLView * mImage;
#else
    IBOutlet PinchSlideImageView * mImage;
#endif
	IBOutlet UILabel * mLabel;
	IBOutlet UIActivityIndicatorView * mIndicator;
    IBOutlet UIToolbar *mPlaybackToolbar;
    
    SiteListController *parent;
    ServerManager *serverMgr;
    int CameraIndex;
    Eventinfo *eventInfo;

    EEVType eEvType;
    EEVType ePreviousEvType;
    EventPlayType ePlayType;
    EventPlayType ePrePlayType;
    Np_PlayerState playbackstate;
    
    NSThread *m_checkPlaybackStatusThread;
    
    NSDate *playbackStartTime;
    NSDate *playbackEndTime;
    NSDate *playbackReverseEndTime; // jerrylu, 2012/06/13
    Np_DateTime rec1_starttime;
    Np_DateTime rec1_endtime;
    Np_DateTime rec2_starttime;
    Np_DateTime rec2_endtime;
    Np_DateTime endrevtime;
    Np_DateTime currenttime; // jerrylu, 2012/07/12
    
    UIBarButtonItem * speedButton;
    float playbackSpeed;
    NSArray *playbackSpeedList;
    NSArray *playbackSpeedValueList;
    UIPickerView *speedPicker;
    UIActionSheet *speedSheet;
    UISlider *speedSlider;
    UIImageView *speedImageView;
    
    // for iPad version
    UIPopoverController *pPlaybackSpeedPopover;
    
    UIBarButtonItem *EventListButton;
    UIBarButtonItem *LiveViewButton;
    UIBarButtonItem *SiteListButton;
    
    BOOL isPlaybackError;
    NSCondition *m_errorCondition;
    
#if RENDER_BY_OPENGL
    float zoomLevel;
#else
    int zoomLevel;
#endif
    CGPoint offsetPos;
	CGSize imgSize;
    
    NSThread *m_checkTimeoutThread;
    UIAlertView *timeoutAlert;
    UIAlertController *timeoutAlertController;
    
    NSTimer *displayTimer;
    NSCondition *m_imgCondition;
    NSMutableArray *imgBuffer;
    BOOL isUpdateImg;
    
    BOOL isHoldButton;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    UIBarButtonItem * dualRecButton;
    EDualRecordFile playbackRecordFile;
    BOOL isDualRecording;
    UIActionSheet *dualRecordSheet;
    UIAlertController *dualRecordController;
#endif
#if FUNC_P2P_SAT_SUPPORT
    UIAlertView *relayAlert;
    UIAlertController *relayAlertController;
#endif
    
    NSTimer *hideUITimer;
}

@property (nonatomic, assign) SiteListController *parent;
@property (nonatomic, retain) ServerManager *serverMgr;
@property (nonatomic, retain) Eventinfo *eventInfo;
#if !RENDER_BY_OPENGL
@property (nonatomic, retain) PinchSlideImageView * image;
#endif
@property (nonatomic, assign) EEVType eEvType;

- (void)refreshSecondToolBar;
- (void)pbControlPause;
@end

