//
//  SlideView.m
//  LiveView
//
//  Created by johnlinvc on 10/02/26.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SlideView.h"

#import "SinglePageViewController_2X3.h"
#import "SinglePageViewController_3X5.h"
#import	"SinglePageViewController_4X6.h"
#import "SinglePageViewController_5X8.h"
#import "SinglePageViewController_2X3.h"
#import "SinglePageViewController_1X3.h"
#import	"SinglePageViewController_2X2.h"
#import "SinglePageViewController_1X2.h"

#import "SiteListController.h"
#import "DOTableViewController.h"
#import "DITableViewController.h"
#import "LiveViewAppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "SlideViewMenuController.h" // jerrylu, 2012/08/06
#import "SingleCamViewController.h" // jerrylu, 2012/09/21
#import "MyViewModifyController.h"

@interface SlideView (PrivateMethods)

- (void)loadScrollViewWithPage:(int) page;
- (void)scrollViewDidScroll:(UIScrollView *) sender;

@end

@implementation SlideView

@synthesize serverMgr;
@synthesize scrollView;
@synthesize iNumberOfPages;
@synthesize parent;
@synthesize DIDOPopover;
@synthesize pageControl;
@synthesize bNoChangePage;
@synthesize isLockSingleView;

- (id)initWithCameraNumber:(int) cam layout:(ELayoutType) layout {
    eLayoutType = layout;
    backFrom1x1 = false;
	if ((self = [super initWithNibName:@"SlideView" bundle:nil])) {

    }
    
    self.bNoChangePage = NO;
    return self;
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	// view controllers are created lazily
    // in the meantime, load the array with placeholders which will be replaced on demand
	[self calPageCnt];
    viewControllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < iNumberOfPages; i++) {
        [viewControllers addObject:[NSNull null]];
    }
    
    imageDataArray = [[NSMutableArray alloc] init];
    imageRawDataArray = [[NSMutableArray alloc] init];
    imageUpdateStatusArray = [[NSMutableArray alloc] init];
    imageConditionArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < [serverMgr getCameraTotalNum]; i++) {
        [imageDataArray addObject:[NSNull null]];
        [imageRawDataArray addObject:[NSNull null]];
        [imageUpdateStatusArray addObject:[NSNumber numberWithBool:NO]];
        NSCondition *imgcond = [[NSCondition alloc] init];
        [imageConditionArray addObject:imgcond];
        [imgcond release];
    }
	
    // a page is the width of the scroll view
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * iNumberOfPages, scrollView.frame.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = NO;
    scrollView.delegate = self;
	[[self navigationController] setDelegate:self];
    pageControl.numberOfPages = iNumberOfPages;
    pageControl.currentPage = 0;

    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(stopStreamAndBackToSiteList) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil)
																	style:UIBarButtonItemStylePlain 
																   target:self 
																   action:@selector(stopStreamAndBackToSiteList)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
    [backButton release];
    
    [self.view setBackgroundColor:COLOR_BACKGROUND];
    
    if (!serverMgr.isMyViewMode) {
        UIImage *cameraListImage;
        UIButton *cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        else
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        cButton.bounds = CGRectMake(0, 0, cameraListImage.size.width, cameraListImage.size.height);
        [cButton setImage:cameraListImage forState:UIControlStateNormal];
        [cButton addTarget:self action:@selector(liveviewCameraListPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * cameraListButton = [[UIBarButtonItem alloc] initWithCustomView:cButton];
        
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/20, change DI/DO button to Menu button
            UIBarButtonItem *menuButton;
            if (IS_IPAD)
                menuButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Menu", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showPopover)];
            else
                menuButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Menu", nil) style:UIBarButtonItemStylePlain target:self action:@selector(filpToDO)];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                [menuButton setTintColor:COLOR_NAVBUTTONTINT];
            }
            else {
                [menuButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
            }
        
            NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects:menuButton, cameraListButton, nil];
            [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
        
            [menuButton release];

#else
            //set DIO Page Button
            UIImage * image;
            NSMutableArray *diDeviceList = [serverMgr getDIDeviceNameList];
            NSMutableArray *doDeviceList = [serverMgr getDODeviceNameList];
            if ([diDeviceList count] != 0 || [doDeviceList count] != 0) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"Btn_DIO_iPad.png"];
                else
                    image = [UIImage imageNamed:@"Btn_DIO.png"];
            }
            else {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"Btn_DIO_Dis_iPad.png"];
                else
                    image = [UIImage imageNamed:@"Btn_DIO_Dis.png"];
            }
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [button setImage:image forState:UIControlStateNormal];
        
            if (IS_IPAD)
                [button addTarget:self action:@selector(showPopover) forControlEvents:UIControlEventTouchUpInside];
            else
                [button addTarget:self action:@selector(filpToDO) forControlEvents:UIControlEventTouchUpInside];
        
            UIBarButtonItem * Btn_DIO = [[UIBarButtonItem alloc] initWithCustomView:button];
        
            NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects:Btn_DIO, cameraListButton, nil];
            [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
            [Btn_DIO release];
        
            if ([diDeviceList count] != 0 || [doDeviceList count] != 0) {
                Btn_DIO.enabled = YES;
            }
            else {
                Btn_DIO.enabled = NO;
            }
#endif
        [cameraListButton release];
    }
    else { //MyViewMode
        UIBarButtonItem * addButton = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                       target:self
                                       action:@selector(editMyViewInfo)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [addButton setTintColor:COLOR_NAVBUTTONTINT];
        }
        else {
            [addButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
        }
        
        UIImage *saveImage;
        UIButton *sButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            saveImage = [UIImage imageNamed:@"Save_48x48_nor.png"];
        else
            saveImage = [UIImage imageNamed:@"Save_32x32_nor.png"];
        sButton.bounds = CGRectMake(0, 0, saveImage.size.width, saveImage.size.height);
        [sButton setImage:saveImage forState:UIControlStateNormal];
        [sButton addTarget:self action:@selector(popSaveLayoutAlert) forControlEvents:UIControlEventTouchUpInside];
        saveButton = [[UIBarButtonItem alloc] initWithCustomView:sButton];
        if (serverMgr.isChangeMyViewLayout)
            saveButton.enabled = YES;
        else
            saveButton.enabled = NO;
        
        NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects: saveButton, addButton, nil];
        [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
        
        [rightBarButtonArray release];
        [saveButton release];
        [addButton release];
        
        singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapOnScrollView:)];
        longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressOnScrollView:)];
        longPress.minimumPressDuration = 0.5;
        [scrollView addGestureRecognizer:longPress];
        changePageTimer = nil;
    }
    isLockSingleView = NO;
	
	[self addLayoutBtn];

#if FUNC_P2P_SAT_SUPPORT
    if ([serverMgr isConnectionWithP2PServer] && [serverMgr isP2PConnectionWithRelayMode]) {
#if CUSTOMER_FOR_NUUO
        NSString *relayinfo = [[NSString alloc] initWithFormat:NSLocalizedString(@"Due to firewall security or instable network status, you are using ezNUUO service via relay currently. In this relay mode, the bit rate will be automatically modified for the optimal video quality.", nil)];
#else
        NSString *relayinfo = [[NSString alloc] initWithFormat:NSLocalizedString(@"Due to firewall security or instable network status, you are using findNVR service via relay currently. In this relay mode, the bit rate will be automatically modified for the optimal video quality.",nil)];
#endif
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:relayinfo preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            popAlertController = alert;
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                            message:relayinfo
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        [relayinfo release];
    }
#endif
	[super viewDidLoad];
}

- (void)addLayoutBtn {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (IS_IPAD) {
        UIImage * image = [UIImage imageNamed:@"Btn_2X3_iPad.png"];
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_2X3) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_2X3 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)  // jerrylu, 2012/06/15
            image = [UIImage imageNamed:@"D06_5x3_h_48x48_nor.png"];
        else
            image = [UIImage imageNamed:@"Btn_3X5.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_3X5) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * Btn_3X5 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)  // jerrylu, 2012/06/15
            image = [UIImage imageNamed:@"D06_6x4_h_48x48_nor.png"];
        else
            image = [UIImage imageNamed:@"Btn_4X6.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_4X6) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * Btn_4X6 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)  // jerrylu, 2012/06/15
            image = [UIImage imageNamed:@"D06_8x5_h_48x48_nor.png"];
        else
            image = [UIImage imageNamed:@"Btn_5X8.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_5X8) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * Btn_5X8 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"D06_1x2_48x48_nor.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_1X2) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_1X2 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"D06_2x2_48x48_nor.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_2X2) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_2X2 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"D06_1x1_48x48_nor.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_1X1) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_1X1 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        
        UIBarButtonItem * flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
																			   target:nil
																			   action:nil];
        
        NSArray * buttonArray = [[NSArray alloc] initWithObjects:
                                 flexItem, Btn_1X1,
                                 flexItem, Btn_1X2,
                                 flexItem, Btn_2X2,
                                 flexItem, Btn_2X3,
                                 flexItem, Btn_3X5, 
                                 //flexItem, Btn_4X6,
                                 //flexItem, Btn_5X8,
                                 flexItem, nil];
        
        [self setToolbarItems:buttonArray];
        [buttonArray release];
        [Btn_1X1 release];
        [Btn_2X3 release];
        [Btn_3X5 release];
        [Btn_4X6 release];
        [Btn_5X8 release];
        [Btn_2X2 release];
        [Btn_1X2 release];
        [flexItem release];
    }
    else {
        UIImage * image = [UIImage imageNamed:@"Btn_1X1.png"];
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_1X1) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_1X1 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"Btn_1X2.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_1X2) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_1X2 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"Btn_1X3.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_1X3) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_1X3 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"Btn_2X2.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_2X2) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_2X2 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"Btn_2X3.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_2X3) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_2X3 = [[UIBarButtonItem alloc] initWithCustomView:button];

        
        UIBarButtonItem * flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
																			   target:nil
																			   action:nil];
        
        NSArray * buttonArray = [[NSArray alloc] initWithObjects:
                                 flexItem, Btn_1X1,
                                 flexItem, Btn_1X2,
                                 flexItem, Btn_1X3, 
                                 flexItem, Btn_2X2,
                                 flexItem, Btn_2X3,
                                 flexItem, nil];
        
        [self setToolbarItems:buttonArray];
        [buttonArray release];
        [Btn_1X1 release];
        [Btn_1X2 release];
        [Btn_1X3 release];
        [Btn_2X2 release];
        [Btn_2X3 release];
        [flexItem release];
    }
}

- (void)doChangeLayout_1X1 {
        if ([self.navigationController.visibleViewController isKindOfClass:[SlideView class]]) {
            int ch_index = 0;
            
            SinglePageViewController_Base *firstPage = [viewControllers objectAtIndex:ch_index];
            SingleCamViewController * mCam = [[SingleCamViewController alloc] initWithServerManager:serverMgr CurrentCam:ch_index];
            [mCam setParent:firstPage];
            [mCam setTitle:[serverMgr getDeviceNameByChIndex:ch_index]];
            mCam.changeLayoutDelegate = self;
            [serverMgr updateAudioAndPtzDeviceByChIndex:ch_index];
            [mCam setPtzCap:[serverMgr isSupportPTZ:ch_index]];
            [mCam setAudioCap:[serverMgr isSupportAudio:ch_index]];
            [mCam setTalkCap:[serverMgr isSupportTalk:ch_index]];
            [firstPage setSingleCam:mCam];
            [serverMgr setPlaybackDelegate:mCam];
            [self.navigationController pushViewController:mCam animated:YES];
            //clear to default picture
            [firstPage setDefaultPic];
            [serverMgr disconnectAllCam];
            [serverMgr connectToCam:ch_index layout:eLayout_1X1];
            [serverMgr connectToCamWithHighResolution:ch_index];
            [mCam release];
        }
        else {
            return;
        }
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO]; // jerrylu, 2012/05/31
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[self navigationController] setToolbarHidden:NO animated:NO];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    // jerrylu, 2012/06/15
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.toolbar setTranslucent:NO];

	[self changeUILayout];
    [self addLayoutBtn];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self changeUILayout];
    [serverMgr disableTalkFunc];
    //if back from 1x1 & layout type is 1x1, we should go back to site list
    //if change to 1x1 by event, we don't need to change here because changeUIlayout
    //will do it
    if (eLayoutType == eLayout_1X1 && !backFrom1x1 && !changeto1x1ByEvent) {
        // for ios 7, we need a delay for the controller to work correctly
        dispatch_after(0, dispatch_get_main_queue(), ^(void){
            [self changeLayout_1X1];
        });
    }
    changeto1x1ByEvent = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (IS_IPAD)
        [self dismissPopover];
    
    /*if (serverMgr.isMyViewMode && changePageTimer != nil) {
        [changePageTimer invalidate];
        changePageTimer = nil;
    }*/
#if FUNC_P2P_SAT_SUPPORT
    [self quotaAlertClose];
#endif
}

- (void)changeUILayout {
#if DEBUG_LOG
    NSLog(@"%s changeUILayout",__FUNCTION__);
#endif
    UIInterfaceOrientation to;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            to = UIInterfaceOrientationLandscapeLeft;
        }
        else {
            to = UIInterfaceOrientationPortrait;
        }
    }
    else {
        to = self.interfaceOrientation;
    }
	
	int width = scrollView.frame.size.width;
	int height = scrollView.frame.size.height;
	for (int idx = 0; idx < [viewControllers count]; idx++) {
		SinglePageViewController_Base * controller = [viewControllers objectAtIndex:idx];
		if ((NSNull *) controller != [NSNull null]) {
			if (to == UIInterfaceOrientationPortrait || 
			    to == UIInterfaceOrientationPortraitUpsideDown) {
				//NSLog(@"portrait %f %f", scrollView.frame.size.width, scrollView.frame.size.height);
				controller.view.frame = CGRectMake(width * idx, 0, width, height);
				scrollView.contentSize = CGSizeMake(width * iNumberOfPages, height);
			}
			else {
				//NSLog(@"landscope %f %f", scrollView.frame.size.width, scrollView.frame.size.height);
				controller.view.frame = CGRectMake(width * idx, 0, width, height);
				scrollView.contentSize = CGSizeMake(width * iNumberOfPages, height);
			}
            
            [controller didRotateFromInterfaceOrientation:to]; // change to didRotateFromInterfaceOrientation for ios6.0
		}
	}

    // jerrylu, 2012/08/16, Handle Event View to Single View
    if (isFromCameraChangebyEvent) {
        changeto1x1ByEvent = YES;
        iCurrentPage = iCameraOfChange / [self getPreviewNum];
        [self loadScrollViewWithPage:iCurrentPage - 1];
        [self loadScrollViewWithPage:iCurrentPage];
        if (iCurrentPage >= 0 && iCurrentPage < iNumberOfPages) {
            SinglePageViewController_Base * preController = [viewControllers objectAtIndex:iCurrentPage];
            [preController setDefaultPic];
        }
        [self loadScrollViewWithPage:iCurrentPage + 1];
        //important, set offset
        CGPoint offsetPoint = CGPointZero;
        offsetPoint.x = iCurrentPage * width;
        [scrollView setContentOffset:offsetPoint];
        
        pageControl.numberOfPages = iNumberOfPages;
        pageControl.currentPage = iCurrentPage;
        pageControl.enabled = TRUE;
        
        SinglePageViewController_Base * preController = [viewControllers objectAtIndex:iCurrentPage];
        [preController openSingleViewAtPosition:(iCameraOfChange % [self getPreviewNum])];
        
        isFromCameraChangebyEvent = NO;
    }
    else if (isFromCameraChangebySingleView) {
        //important, set offset
        CGPoint offsetPoint = CGPointZero;
        offsetPoint.x = iCurrentPage * width;
        [scrollView setContentOffset:offsetPoint];
        
        pageControl.numberOfPages = iNumberOfPages;
        pageControl.currentPage = iCurrentPage;
        pageControl.enabled = TRUE;
        
        isFromCameraChangebySingleView = NO;
    }
    else {
        //important, set offset
        CGPoint offsetPoint = CGPointZero;
        offsetPoint.x = iCurrentPage * width;
        [scrollView setContentOffset:offsetPoint];
        
        pageControl.numberOfPages = iNumberOfPages;
        pageControl.currentPage = iCurrentPage;
        pageControl.enabled = TRUE;
    }
}

- (void)changePagebyCameraIndexbyEvent:(int) camIndex {
    iCameraOfChange = camIndex;
    isFromCameraChangebyEvent = YES;
}
// jerrylu, 2012/09/18
- (void)changePagebyCameraIndexbySingleView:(int) camIndex SingleCam:(SingleCamViewController *) singleCam {
    iCameraOfChange = camIndex;
    isFromCameraChangebySingleView = YES;
    int page = iCameraOfChange / [self getPreviewNum];
    if (page != iCurrentPage) {
        iCurrentPage = page;
                
        [self loadScrollViewWithPage:iCurrentPage - 1];
        [self loadScrollViewWithPage:iCurrentPage];
        [self loadScrollViewWithPage:iCurrentPage + 1];
        
        if (iCurrentPage >= 0 && iCurrentPage < iNumberOfPages) {
            SinglePageViewController_Base * pageController;
            
            for (int i = 0; i < iNumberOfPages; i++) {
                pageController = [viewControllers objectAtIndex:i];
                if ((NSNull *)pageController != [NSNull null]) {
                    [pageController setDefaultPic];
                    pageController.singleCam = nil;
                }
            }
            
            pageController = [viewControllers objectAtIndex:iCurrentPage];
            [pageController setDefaultPic];
            pageController.singleCamPos = iCameraOfChange % [self getPreviewNum];
            pageController.singleCam = singleCam;
            singleCam.parent = pageController;
        }
    }
    else {
        if (iCurrentPage >= 0 && iCurrentPage < iNumberOfPages) {
            SinglePageViewController_Base * pageController = [viewControllers objectAtIndex:iCurrentPage];
            [pageController setDefaultPic];
            pageController.singleCamPos = iCameraOfChange % [self getPreviewNum];            
            //singleCam.parent = pageController;
        }
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    bNoChangePage = YES;
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        [self changeUILayout];
        [self addLayoutBtn];
        if (IS_IPAD)
            [DIDOPopover dismissPopoverAnimated:YES];
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    bNoChangePage = YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
	[self changeUILayout];
    [self addLayoutBtn]; // jerrylu, 2012/05/28
    
    if (IS_IPAD)
        [DIDOPopover dismissPopoverAnimated:YES];
}

- (void)stopStreamAndBackToSiteList {
    [viewControllers release];
    viewControllers = nil;
    
    if (serverMgr.isMyViewMode && serverMgr.isChangeMyViewLayout) {
        [serverMgr logoutAllServer];
        [self popSaveLayoutAlert];
    }
    else if (serverMgr.isMyViewMode) {
        [serverMgr logoutAllServer];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (!serverMgr.isMyViewMode && [serverMgr getServerTypeBySrvIndex:0] == eServer_Crystal)
    {
        [serverMgr stopconnectToCam];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [serverMgr logoutAllServer];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)popSaveLayoutAlert {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if (popAlertController != nil)
            [popAlertController dismissViewControllerAnimated:YES completion:nil];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Do you want to save this view?", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     if ([serverMgr isServerLogouting]) {
                                         [self.navigationController popViewControllerAnimated:YES];
                                     }
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        UIAlertAction* save = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Save", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [serverMgr saveMyViewLayout];
                                   if ([serverMgr isServerLogouting]) {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
        [alert addAction:cancel];
        [alert addAction:save];
        [self presentViewController:alert animated:YES completion:nil];
        
        popAlertController = alert;
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                        message:NSLocalizedString(@"Do you want to save this view?", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                              otherButtonTitles:NSLocalizedString(@"Save", nil),nil];
        [alert show];
        [alert release];
    }
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
    NSLog(@"Memory Warning");
}

- (void)dealloc {
    if (singleTap != nil) {
        [singleTap release];
        singleTap = nil;
    }
    if (longPress != nil) {
        [longPress release];
        longPress = nil;
    }
    
	[viewControllers release];
    viewControllers = nil;
    [imageDataArray release];
    imageDataArray = nil;
    [imageRawDataArray release];
    imageRawDataArray = nil;
    [imageUpdateStatusArray release];
    imageUpdateStatusArray = nil;
    [imageConditionArray release];
    imageConditionArray = nil;
    [scrollView release];
    scrollView = nil;
	[pageControl release];
    pageControl = nil;
    
    if (IS_IPAD) {
        [DIDOPopover release];
        DIDOPopover = nil;
    }

    [super dealloc];
}

- (UIImage *)getImageOfCamIndex:(int) index isUpdate:(BOOL*) isUpdate {
    NSCondition *imglock = [imageConditionArray objectAtIndex:index];
    [imglock lock];
    *isUpdate = [[imageUpdateStatusArray objectAtIndex:index] boolValue];
    
    UIImage *targetImg = [imageDataArray objectAtIndex:index];
    if ((NSNull *)targetImg == [NSNull null]) {
        [imglock unlock];
        return targetImg;
    }
    
    UIImage *img = [[imageDataArray objectAtIndex:index] copy];
    [imageUpdateStatusArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:NO]];
    
    [imglock unlock];
    
    return [img autorelease];
}

- (MyVideoFrameBuf *)getImageBufOfCamIndex:(int) index isUpdate:(BOOL*) isUpdate {
    NSCondition *imglock = [imageConditionArray objectAtIndex:index];
    [imglock lock];
    *isUpdate = [[imageUpdateStatusArray objectAtIndex:index] boolValue];
    
    MyVideoFrameBuf *targetImgBuf = [imageRawDataArray objectAtIndex:index];
    if ((NSNull *)targetImgBuf == [NSNull null]) {
        [imglock unlock];
        return targetImgBuf;
    }
    
    MyVideoFrameBuf *imgbuf = [[MyVideoFrameBuf alloc] init];
    imgbuf.width = targetImgBuf.width;
    imgbuf.height = targetImgBuf.height;
    int len = (targetImgBuf.width * targetImgBuf.height * 3) / 2;
    NSMutableData *targetmd = (NSMutableData *)targetImgBuf.data;
    NSMutableData *md = [NSMutableData dataWithLength:len];
    Byte *src = (Byte *)targetmd.mutableBytes;
    Byte *dst = (Byte *)md.mutableBytes;
    memcpy(dst, src, len);
    imgbuf.data = md;
    [imageUpdateStatusArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:NO]];
    
    [imglock unlock];
    
    return [imgbuf autorelease];
}

- (void)setImageUpdateOfCamIndex:(int) index flag:(BOOL) flag {
    NSCondition *imglock = [imageConditionArray objectAtIndex:index];
    [imglock lock];
    
    [imageUpdateStatusArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:flag]];
    
    [imglock unlock];
}

- (void)resetImageOfCamIndex:(int) index {
    NSCondition *imglock = [imageConditionArray objectAtIndex:index];
    [imglock lock];
    
    [imageDataArray replaceObjectAtIndex:index withObject:[NSNull null]];
    [imageRawDataArray replaceObjectAtIndex:index withObject:[NSNull null]];
    [imageUpdateStatusArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:NO]];
    
    [imglock unlock];
}

- (void)editMyViewInfo {
    MyViewModifyController *serverListView = [[MyViewModifyController alloc] initWithStyle:UITableViewStylePlain ServerMgr:serverMgr];
    serverListView.favoriteSite = parent;
    [serverListView setTitle:[self title]];
    
    UINavigationController * newNavController = [[UINavigationController alloc]
                                                 initWithRootViewController:serverListView];
    newNavController.navigationBar.barStyle = UIBarStyleBlack;
    newNavController.navigationBar.translucent = NO;
    [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
    [newNavController release];
    [serverListView release];
}

#pragma mark -
#pragma mark scrollView methods
- (void)loadScrollViewWithPage:(int) page {
    if (page < 0 ||
		page >= iNumberOfPages) 
		return;
	
	// replace the placeholder if necessary
    SinglePageViewController_Base * controller = [viewControllers objectAtIndex:page];
    if ((NSNull *) controller == [NSNull null]) {
        if (IS_IPAD) {
            switch (eLayoutType) {
                case eLayout_2X3:
                    controller = [[SinglePageViewController_2X3 alloc] initwithPage:page];
                    break;
                case eLayout_3X5:
                    controller = [[SinglePageViewController_3X5 alloc] initwithPage:page];
                    break;
                case eLayout_4X6:
                    controller = [[SinglePageViewController_4X6 alloc] initwithPage:page];
                    break;
                case eLayout_5X8:
                    controller = [[SinglePageViewController_5X8 alloc] initwithPage:page];
                    break;
                case eLayout_2X2:
                    controller = [[SinglePageViewController_2X2 alloc] initwithPage:page];
                    break;
                case eLayout_1X2:
                    controller = [[SinglePageViewController_1X2 alloc] initwithPage:page];
                    break;
                    //user can only see the single view page, so we just need to give a "valid" initialization here
                case eLayout_1X1:
                    controller = [[SinglePageViewController_1X2 alloc] initwithPage:page];
                    break;
                default:
                    break;
            }
        }
        else {
            switch (eLayoutType) {
                case eLayout_2X3:
                    controller = [[SinglePageViewController_2X3 alloc] initwithPage:page];
                    break;
                case eLayout_2X2:
                    controller = [[SinglePageViewController_2X2 alloc] initwithPage:page];
                    break;
                case eLayout_1X3:
                    controller = [[SinglePageViewController_1X3 alloc] initwithPage:page];
                    break;
                case eLayout_1X2:
                    controller = [[SinglePageViewController_1X2 alloc] initwithPage:page];
                    break;
                //user can only see the single view page, so we just need to give a "valid" initialization here
                case eLayout_1X1:
                    controller = [[SinglePageViewController_1X2 alloc] initwithPage:page];
                    break;
                default:
                    break;
            }
        }
        
		[controller setSlideView:self];
		[controller setServerMgr:serverMgr];
        [viewControllers replaceObjectAtIndex:page withObject:controller];
        [controller release];
		if (page == 0) {
            int iCameraCount = [serverMgr getCameraTotalNum];
			iCameraCount = (iCameraCount > [self getPreviewNum]) ? [self getPreviewNum] : iCameraCount;
            
			for (int i = 0; i < iCameraCount; i++) {
                [serverMgr connectToCam:i layout:eLayoutType];
			}
			needChangeCams = NO;
		}
    }
	
    // add the controller's view to the scroll view
    // for ios 7, 1x1 layout needs a delay to push controller, 
    // so we don't push the controller here
    if (controller.view.superview == nil && eLayoutType != eLayout_1X1) {
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [scrollView addSubview:controller.view];
    }
}

- (IBAction)changePage:(id) sender {
    int page = pageControl.currentPage;
	iCurrentPage = page;
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
	if (page >= 0 && page < iNumberOfPages) {
		SinglePageViewController_Base * preController = [viewControllers objectAtIndex:page];
		[preController setDefaultPic];
	}
	
    [self loadScrollViewWithPage:page + 1];
    
	//reconnect
	[self reconnectCurrentPage];
	// update the scroll view to the appropriate page
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *) sender {
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed) {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    
    if (scrollView.contentOffset.x < 0) {
        [scrollView setContentOffset:CGPointMake(0, 0)];
        scrollView.bounces = NO;
        needChangeCams = NO;
        return;
    }
    else if (scrollView.contentOffset.x > pageWidth*(iNumberOfPages-1)) {
        [scrollView setContentOffset:CGPointMake(pageWidth*(iNumberOfPages-1), 0)];
        scrollView.bounces = NO;
        needChangeCams = NO;
        return;
    }
    else {
        scrollView.bounces = YES;
        
        // If user just rotate, then we don't need to count scroll view change.
        if (bNoChangePage == YES) {
            bNoChangePage = NO;
        }
        else
        {
            int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            if (page != iCurrentPage) {
                needChangeCams = YES;
            }
            iCurrentPage = page;
        }
    }
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:iCurrentPage - 1];
    [self loadScrollViewWithPage:iCurrentPage];
	if (iCurrentPage >= 0 && iCurrentPage < iNumberOfPages) {
        SinglePageViewController_Base * preController = [viewControllers objectAtIndex:iCurrentPage];
		[preController setDefaultPic];
	}
    [self loadScrollViewWithPage:iCurrentPage + 1];
    
    // A possible optimization would be to unload the views+controllers which are no longer visible
}


// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *) scrollView {
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *) scrollView {
    pageControlUsed = NO;
	if (needChangeCams) {
		[self reconnectCurrentPage];
        needChangeCams = NO;
	}
	pageControl.currentPage = iCurrentPage;
    
    [self changeUILayout]; // jerrylu, 2012/10/22, fix the orientation for ios6.0
}

- (void)reconnectCurrentPage {
#if DEBUG_LOG
    NSLog(@"reconnectCurrentPage page:%d", iCurrentPage);
#endif
    dispatch_after(0, dispatch_get_main_queue(), ^(void){
        int iCount = [serverMgr getCameraTotalNum];
        SinglePageViewController_Base * thisPage = [viewControllers objectAtIndex:iCurrentPage];
        [thisPage startAnimatingIndicator];
        int initPosition = iCurrentPage * [self getPreviewNum];
        int length = (initPosition + [self getPreviewNum]) > iCount ? iCount - initPosition : [self getPreviewNum];
        //disconnect previous connection
        [serverMgr disconnectAllCam];
        
        //connect current connection
        for (int i = 0; i < length; i++) {
#if DEBUG_LOG
            NSLog(@"connectToCam:%d", initPosition + i);
#endif
            [self resetImageOfCamIndex:initPosition+i];
            [serverMgr connectToCam:initPosition+i layout:eLayoutType];
        }
    });
}

- (void)dismissPopover {
    if (IS_IPAD) {
        [DIDOPopover dismissPopoverAnimated:YES];
    }
    return;
}

- (void)showPopover {
    if (IS_IPAD) {
        if (DIDOPopover.popoverVisible == NO) {
#if FUNC_PUSH_NOTIFICATION_SUPPORT  // jerrylu, 2012/08/27, Pop SlideView Menu
            SlideMenuTable = [[[SlideViewMenuController alloc] initWithStyle:UITableViewStyleGrouped parentview:self] autorelease];
            SlideMenuTable.serverMgr = serverMgr;
            UINavigationController * navigation = [[UINavigationController alloc] initWithRootViewController:SlideMenuTable];
            DIDOPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
            DIDOPopover.delegate = self;
            [navigation release];
#else
            DOTable = [[[DOTableViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
            [DOTable setParent:self];
            [DOTable setServerMgr:serverMgr];
            [DOTable setTitle:NSLocalizedString(@"Digital Output", nil)];
            UINavigationController * navigation = [[UINavigationController alloc] initWithRootViewController:DOTable];
            DIDOPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
            DIDOPopover.delegate = self;
            [navigation release];
#endif // FUNC_PUSH_NOTIFICATION_SUPPORT
            
            [DIDOPopover presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];           
        }
        else {
            [DIDOPopover dismissPopoverAnimated:YES];
        }
    }
    return;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *) popoverController {
    if (IS_IPAD) {
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/27, Pop SlideView Menu
        [SlideMenuTable.navigationController popToRootViewControllerAnimated:NO];
#else
        [DOTable.navigationController popViewControllerAnimated:NO];
#endif
    }
    return;
}

- (void)filpToDO {
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/06
    SlideMenuTable = [[[SlideViewMenuController alloc] initWithStyle:UITableViewStyleGrouped parentview:self] autorelease];
    SlideMenuTable.serverMgr = serverMgr;
    [self.navigationController pushViewController:SlideMenuTable animated:YES];
#else
	DOTable = [[[DOTableViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
	[DOTable setTitle:NSLocalizedString(@"Digital Output", nil)];
	[DOTable setParent:self];
    [DOTable setServerMgr:serverMgr];
    [self.navigationController pushViewController:DOTable animated:YES];
#endif // FUNC_PUSH_NOTIFICATION_SUPPORT
}


- (void)singleTapOnScrollView:(UITapGestureRecognizer *)gesture {
    if (isLockSingleView == NO)
        return;
    
    [scrollView setScrollEnabled:YES];
    [scrollView removeGestureRecognizer:singleTap];
    isLockSingleView = NO;
    
    //CGPoint touchPoint=[gesture locationInView:scrollView];
    
    NSLog(@"[%s] get Single Tap on ScrollView", __FUNCTION__);
}

- (void)longPressOnScrollView:(UITapGestureRecognizer *)gesture {
    CGPoint touchPoint=[gesture locationInView:scrollView];
    int screen_w;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        screen_w = [[UIScreen mainScreen] bounds].size.width;
    }
    else {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            screen_w = [[UIScreen mainScreen] bounds].size.width;
        else
            screen_w = [[UIScreen mainScreen] bounds].size.height;
    }
    int pt_x = touchPoint.x;
    int l_x = pt_x % screen_w;
    
    if (singleMoveImage == nil) {
        singleMoveImage = [[UIImageView alloc] init];
        [singleMoveImage setAlpha:0.7];
    }
    
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        NSLog(@"[%s] begin Long Press on ScrollView", __FUNCTION__);
        
        isLockSingleView = YES;
        [scrollView setScrollEnabled:NO];
        
        SinglePageViewController_Base * controller = [viewControllers objectAtIndex:iCurrentPage];
        CGPoint relativeTouchPoint = CGPointMake(l_x, touchPoint.y);
        swap_begin_cindex = [controller getCameraIndexByTouchPoint:relativeTouchPoint];
        NSLog(@"[%s] swap_begin_cindex %d", __FUNCTION__, swap_begin_cindex);
        
        if (swap_begin_cindex < 0 ||
            swap_begin_cindex >= [serverMgr getCameraTotalNum])
            return;
        
        // get image first, and transform the multi-view scale
        [controller getImageViewByCameraIndex:singleMoveImage index:swap_begin_cindex];
        [singleMoveImage removeFromSuperview];
        [scrollView addSubview:singleMoveImage];
        [controller changeImageViewScale:0.9];
    }
    else if ([gesture state] == UIGestureRecognizerStateEnded) {
        NSLog(@"[%s] end Long Press on ScrollView", __FUNCTION__);
        [scrollView setScrollEnabled:YES];
        isLockSingleView = NO;
        
        if (changePageTimer != nil) {
            [changePageTimer invalidate];
            changePageTimer = nil;
        }
        
        for (SinglePageViewController_Base * viewr in viewControllers) {
            if ([NSNull null] != (NSNull *)viewr)
                [viewr restoreImageViewScale];
        }
        
        // transform the multi-view scale first
        SinglePageViewController_Base * controller = [viewControllers objectAtIndex:iCurrentPage];
        CGPoint relativeTouchPoint = CGPointMake(l_x, touchPoint.y);
        swap_end_cindex = [controller getCameraIndexByTouchPoint:relativeTouchPoint];
        NSLog(@"[%s] swap_end_cindex %d", __FUNCTION__, swap_end_cindex);
        [controller clearSelectImageView];
        
        [singleMoveImage removeFromSuperview];
        
        if (swap_begin_cindex < 0 ||
            swap_begin_cindex >= [serverMgr getCameraTotalNum] ||
            swap_end_cindex < 0 ||
            swap_end_cindex >= [serverMgr getCameraTotalNum])
            return;
        
        if (swap_end_cindex != swap_begin_cindex) {
            //[serverMgr disconnectAllCam];
            [serverMgr changeCameraOrderFrom:swap_begin_cindex to:swap_end_cindex];
            for (int i = 0; i < [viewControllers count]; i++) {
                SinglePageViewController_Base *thisPage = [viewControllers objectAtIndex:i];
                if ([NSNull null] != (NSNull *)thisPage)
                    [thisPage reloadThisPage];
            }
            //[self reconnectCurrentPage];
            [self resetCameraBetween:swap_begin_cindex to:swap_end_cindex];
        }
    }
    else if ([gesture state] == UIGestureRecognizerStateChanged) {
        if (l_x < 20) {
            if (changePageTimer == nil || ![changePageTimer isValid]) {
                if (iCurrentPage > 0) {
                    changePageTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                                       target:self
                                                                     selector:@selector(changePreviousPageByTimer)
                                                                     userInfo:nil
                                                                      repeats:NO];
                }
            }
        }
        else if ((screen_w - l_x) < 20) {
            if (changePageTimer == nil || ![changePageTimer isValid]) {
                if (iCurrentPage < iNumberOfPages - 1) {
                    changePageTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                                      target:self
                                                                    selector:@selector(changeNextPageByTimer)
                                                                    userInfo:nil
                                                                     repeats:NO];
                }
            }
        }
        else {
            if (changePageTimer != nil) {
                [changePageTimer invalidate];
                changePageTimer = nil;
            }
        }
        
        SinglePageViewController_Base * controller = [viewControllers objectAtIndex:iCurrentPage];
        CGPoint relativeTouchPoint = CGPointMake(l_x, touchPoint.y);
        int select_index = [controller getCameraIndexByTouchPoint:relativeTouchPoint];
        if (select_index >= 0 && select_index < [serverMgr getCameraTotalNum])
            [controller setSelectedImageView:select_index];
        else
            [controller clearSelectImageView];
    }
    else
        return;
    
    singleMoveImage.center = touchPoint;
    [scrollView bringSubviewToFront:singleMoveImage];
}

- (void)resetCameraBetween:(int)pre_index to:(int)post_index {
    [self resetImageOfCamIndex:pre_index];
    [self resetImageOfCamIndex:post_index];
    [serverMgr connectToCam:post_index layout:eLayoutType];
    saveButton.enabled = YES;
}

- (void)changePreviousPageByTimer {
    changePageTimer = nil;
    iCurrentPage = iCurrentPage - 1;
    [self changeUILayout];
    [self reconnectCurrentPage];
    SinglePageViewController_Base * controller = [viewControllers objectAtIndex:iCurrentPage];
    [controller changeImageViewScale:0.9];
    CGPoint touchPoint=[longPress locationInView:scrollView];
    singleMoveImage.center = touchPoint;
}

- (void)changeNextPageByTimer {
    changePageTimer = nil;
    iCurrentPage = iCurrentPage + 1;
    [self changeUILayout];
    [self reconnectCurrentPage];
    SinglePageViewController_Base * controller = [viewControllers objectAtIndex:iCurrentPage];
    [controller changeImageViewScale:0.9];
    CGPoint touchPoint=[longPress locationInView:scrollView];
    singleMoveImage.center = touchPoint;
}

#pragma mark -
#pragma mark ServerManagerDisplayDelegate protocol
- (void)serverManagerDidRefreshImages:(ServerManager *)servermgr camIndex:(int)index camImg:(UIImage *)img {
    NSCondition *imglock = [imageConditionArray objectAtIndex:index];
    [imglock lock];
    
    [imageDataArray replaceObjectAtIndex:index withObject:img];
    [imageUpdateStatusArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:YES]];
    
    [imglock unlock];
}

- (void)serverManagerDidRefreshImageBuf:(ServerManager *)servermgr camIndex:(int)index img:(NSData *)imgData width:(int)width height:(int)height {
    NSCondition *imglock = [imageConditionArray objectAtIndex:index];
    [imglock lock];
    
    MyVideoFrameBuf *myframebuf = [MyGLView CopyFullFrameToVideoFrameBuf:(uint8_t *)[imgData bytes] withWidth :width withHeight:height];
    [imageRawDataArray replaceObjectAtIndex:index withObject:myframebuf];
    
    [imageUpdateStatusArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:YES]];
    
    [imglock unlock];
}

- (void)serverManagerDidServerDown:(ServerManager *)servermgr serverIndex:(int)index eventID:(int)eventID {
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    UIViewController * modal = [self presentedViewController]; // modalViewController is deprecated in iOS 6.0.
	
	if ([modal isKindOfClass:[MFMailComposeViewController class]]) {
		[appDelegate setBDelayMsg:TRUE];
	}
	else {
		[appDelegate showServerDownMessage:eventID];
	}
}

- (void)serverManagerDidCameraSessionLost:(ServerManager *)servermgr camIndex:(int)index {
    [self resetImageOfCamIndex:index];
}

- (void)serverManagerDidPlaybackDisconnection:(ServerManager *)servermgr {
    SinglePageViewController_Base * thisPage = [viewControllers objectAtIndex:iCurrentPage];
    
	[thisPage playbackDisconnection];
}

- (void)serverManagerDidPbNextPreviosFrameSuccess:(ServerManager *)servermgr {
    SinglePageViewController_Base * thisPage = [viewControllers objectAtIndex:iCurrentPage];
    
	[thisPage playbackNextPreviousFrameSucess];
}

#if FUNC_P2P_SAT_SUPPORT
// jerrylu, 2012/09/14
- (void)serverManagerQuotaTimeout:(ServerManager *)servermgr serverIndex:(int)index isOutOfQuota:(BOOL)outofquota {
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
	[appDelegate showQuotaTimeoutMessage:outofquota];
}
#endif

- (void)serverManagerDidSaveLayout:(ServerManager *)servermgr {
    saveButton.enabled = NO;
}

- (void)serverManagerDidTalkReserved:(ServerManager *)servermgr srv:(NSString *)servername usr:(NSString *)username {
    SinglePageViewController_Base * thisPage = [viewControllers objectAtIndex:iCurrentPage];
    
	[thisPage getTalkReserved:servername user:username];
}

- (void)serverManagerDidTalkSessionError:(ServerManager *)servermgr {
    SinglePageViewController_Base * thisPage = [viewControllers objectAtIndex:iCurrentPage];
    
	[thisPage getTalkSessionError];
}

#if FUNC_P2P_SAT_SUPPORT
- (void)serverManagerShowP2PControlDialog:(ServerManager *)servermgr msg:(NSString *)dialogMsg {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if (popAlertController != nil)
            [popAlertController dismissViewControllerAnimated:YES completion:nil];
        
        if (relayAlertController == nil) {
            relayAlertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:dialogMsg preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* reconnectAct = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Reconnect", nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                                               [serverMgr resetP2PRelayQuota];
                                               relayAlertController = nil;
                                               [relayAlertController dismissViewControllerAnimated:YES completion:nil];
                                               
                                           }];
            [relayAlertController addAction:reconnectAct];
            [self presentViewController:relayAlertController animated:YES completion:nil];
        }
        else {
            [relayAlertController performSelectorOnMainThread:@selector(setMessage:) withObject:dialogMsg waitUntilDone:NO];
        }
    }
    else {
        if (relayAlert == nil)
        {
            relayAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:dialogMsg delegate:self cancelButtonTitle:NSLocalizedString(@"Reconnect", nil) otherButtonTitles:nil];
            if(![[NSThread currentThread] isCancelled])
                [relayAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
            [relayAlert release];
        }
        else
            [relayAlert performSelectorOnMainThread:@selector(setMessage:) withObject:dialogMsg waitUntilDone:NO];
    }
}

- (void)quotaAlertClose {
    if((NSNull *)relayAlert != [NSNull null])
        [relayAlert dismissWithClickedButtonIndex:-1 animated:NO];
    
    if((NSNull *)relayAlertController != [NSNull null])
        [relayAlertController dismissViewControllerAnimated:NO completion:nil];
}
#endif

- (void)serverManagerShowDialog:(ServerManager *)servermgr msg:(NSString *)dialogMsg {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if (popAlertController != nil)
            [popAlertController dismissViewControllerAnimated:YES completion:nil];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:dialogMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if (eLayoutType == eLayout_1X1)
                                     [self changeLayout_1X1];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        popAlertController = alert;
    }
}

- (void)serverManagerRecordingServerAvailable:(ServerManager *)servermgr msg:(NSString *)dialogMsg {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if (popAlertController != nil)
            [popAlertController dismissViewControllerAnimated:YES completion:nil];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:dialogMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [serverMgr reLoginMyView];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        popAlertController = alert;
    }
}

- (void)serverManagerRefreshAllLayout:(ServerManager *)servermgr {
    // enable save button
    saveButton.enabled = YES;
    
    // renew the image buffer list
    [imageDataArray release];
    imageDataArray = nil;
    [imageRawDataArray release];
    imageRawDataArray = nil;
    [imageUpdateStatusArray release];
    imageUpdateStatusArray = nil;
    [imageConditionArray release];
    imageConditionArray = nil;
    
    imageDataArray = [[NSMutableArray alloc] init];
    imageRawDataArray = [[NSMutableArray alloc] init];
    imageUpdateStatusArray = [[NSMutableArray alloc] init];
    imageConditionArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < [serverMgr getCameraTotalNum]; i++) {
        [imageDataArray addObject:[NSNull null]];
        [imageRawDataArray addObject:[NSNull null]];
        [imageUpdateStatusArray addObject:[NSNumber numberWithBool:NO]];
        NSCondition *imgcond = [[NSCondition alloc] init];
        [imageConditionArray addObject:imgcond];
        [imgcond release];
    }
    
    // renew the page list
    [self calPageCnt];
    
    for (int i=0;i<[viewControllers count];i++)
    {
        SinglePageViewController_Base* controller = [viewControllers objectAtIndex:i];
        [controller stop];
    }
    [viewControllers release];
    viewControllers = nil;
    
    viewControllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < iNumberOfPages; i++) {
        [viewControllers addObject:[NSNull null]];
    }
    
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * iNumberOfPages, scrollView.frame.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = NO;
    scrollView.delegate = self;
    [[self navigationController] setDelegate:self];
    
    //important, set offset to 0
    [scrollView setContentOffset:CGPointZero];
    
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
    pageControl.numberOfPages = iNumberOfPages;
    pageControl.currentPage = 0;
    
    [self changeUILayout]; // jerrylu, 2012/10/22, fix the orientation for ios6.0
}

#pragma mark -
#pragma mark LiveViewEventListContorlDelegate Methods

- (void)lvctrlFromEventListToLiveView {
    [self reconnectCurrentPage];
}

#pragma mark -
#pragma mark LiveViewEventViewContorlDelegate Methods

- (void)lvctrlFromEventViewToLiveView:(int) camIndex {
    [self changePagebyCameraIndexbyEvent:camIndex];
        
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.toolbar setTranslucent:NO];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    //pop LiveViewEventView page
    [self.navigationController popToViewController:self animated:NO];
}

#pragma mark -
#pragma mark navigationController delegate protocol
- (void)navigationController:(UINavigationController *) navigationController 
	   didShowViewController:(UIViewController *) viewController 
					animated:(BOOL)animated {
	if (viewController == self) {
		//NSLog(@"show slideView");
		//[[self navigationController] setToolbarHidden:YES animated:YES];
	}
	else if ([viewController isKindOfClass:[SiteListController class]] ) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	}
}

- (void)changeLayout {
	[serverMgr disconnectAllCamExceptFrom:0 To:([self getPreviewNum]-1)];
    [self calPageCnt];
    [viewControllers release];
    viewControllers = nil;
    
    viewControllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < iNumberOfPages; i++) {
        [viewControllers addObject:[NSNull null]];
    }
    
	scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * iNumberOfPages, scrollView.frame.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = NO;
    scrollView.delegate = self;
	[[self navigationController] setDelegate:self];
	
	//important, set offset to 0
	[scrollView setContentOffset:CGPointZero];
	
	[self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
	pageControl.numberOfPages = iNumberOfPages;
    pageControl.currentPage = 0;
    
    [self changeUILayout]; // jerrylu, 2012/10/22, fix the orientation for ios6.0
}

- (void)changeLayout_1X1 {
    eLayoutType = eLayout_1X1;
    [serverMgr setLayoutType:eLayout_1X1];
    [parent save];
    [self changeLayout];
    [self doChangeLayout_1X1];
}


- (void)changeLayout_2X2 {
	if (eLayoutType == eLayout_2X2) {
		return;
	}
    eLayoutType = eLayout_2X2;
    [serverMgr setLayoutType:eLayout_2X2];
    [parent save];
	[self changeLayout];
}

- (void)changeLayout_1X3; {
	if (eLayoutType == eLayout_1X3) {
		return;
	}
    eLayoutType = eLayout_1X3;
    [serverMgr setLayoutType:eLayout_1X3];
    [parent save];
	[self changeLayout];
}

- (void)changeLayout_1X2; {
	if (eLayoutType == eLayout_1X2) {
		return;
	}
	eLayoutType = eLayout_1X2;
    [serverMgr setLayoutType:eLayout_1X2];
    [parent save];
    [self changeLayout];
}

- (void)changeLayout_Custom; {
	if (eLayoutType == eLayout_Custom) {
		return;
	}
	eLayoutType = eLayout_Custom;
	[self changeLayout];
}

- (void)changeLayout_2X3 {
	if (eLayoutType == eLayout_2X3) {
		return;
	}
    eLayoutType = eLayout_2X3;
    [serverMgr setLayoutType:eLayout_2X3];
    [parent save];
	[self changeLayout];
}

- (void)changeLayout_3X5 {
	if (eLayoutType == eLayout_3X5) {
		return;
	}
    eLayoutType = eLayout_3X5;
    [serverMgr setLayoutType:eLayout_3X5];
    [parent save];
	[self changeLayout];
}

- (void)changeLayout_4X6 {
	if (eLayoutType == eLayout_4X6) {
		return;
	}
    eLayoutType = eLayout_4X6;
    [serverMgr setLayoutType:eLayout_4X6];
    [parent save];
	[self changeLayout];
}

- (void)changeLayout_5X8 {
	if (eLayoutType == eLayout_5X8) {
		return;
	}
    eLayoutType = eLayout_5X8;
    [serverMgr setLayoutType:eLayout_5X8];
    [parent save];
	[self changeLayout];
}

- (ELayoutType)getLayoutType {
    return eLayoutType;
}

- (void)calPageCnt {
	int iCamCnt = [serverMgr getCameraTotalNum];
	int iLayoutNum = [self getPreviewNum];
	int iPageCount = 1;
	while (iCamCnt > iLayoutNum) {
		iCamCnt -= iLayoutNum;
		++iPageCount;
	}
	iNumberOfPages = iPageCount;
	iCurrentPage = 0;
}

- (int)getPreviewNum {
	int iNum = 1;
	switch (eLayoutType) {
		case eLayout_2X3:
			iNum = 6;
			break;
		case eLayout_3X5:
			iNum = 15;
			break;
		case eLayout_4X6:
			iNum = 24;
			break;
		case eLayout_5X8:
			iNum = 40;
			break;
		case eLayout_2X2:
			iNum = 4;
			break;
		case eLayout_1X3:
			iNum = 3;
			break;
		case eLayout_1X2:
			iNum = 2;
			break;
		case eLayout_Custom:
			//iNum = 6;
			break;
		default:
			break;
	}
	return iNum;
}

#pragma mark -
#pragma mark AlertView Methods
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
	if ([alertView.message hasPrefix:NSLocalizedString(@"Do you want to save this view?", nil)]) {
        if (buttonIndex == 1) {
            [serverMgr saveMyViewLayout];
        }
        if ([serverMgr isServerLogouting]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
#if FUNC_P2P_SAT_SUPPORT
    else if ([alertView.message hasPrefix:@"The connection of"]) {
        [serverMgr resetP2PRelayQuota];
        relayAlert = nil;
    }
#endif
}

- (void)turnOffWarning {
    // jerrylu, 2012/11/23, 2012/11/29, fix bug9799
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Main_Console_Profile_Warning"];
}


#pragma mark -
#pragma mark MyCameraListControllerDelegate

- (void)liveviewCameraListPressed {
    pLvCameraListController = [[[MyCameraListController alloc] initWithStyle:UITableViewStyleGrouped] autorelease];
    pLvCameraListController.switchControlDelegate = self;
    pLvCameraListController.serverMgr = serverMgr;
    [self.navigationController pushViewController:pLvCameraListController animated:YES];
}

- (void)switchToCameraIndex:(int) ch_index {
    [serverMgr disconnectAllCam];
    
    //[self changePagebyCameraIndexbyEvent:ch_index];
    iCameraOfChange = ch_index;
    iCurrentPage = iCameraOfChange / [self getPreviewNum];
    [self loadScrollViewWithPage:iCurrentPage - 1];
    [self loadScrollViewWithPage:iCurrentPage];
    [self loadScrollViewWithPage:iCurrentPage + 1];
    
    [self reconnectCurrentPage];
    
    int width = scrollView.frame.size.width;
    //important, set offset
    CGPoint offsetPoint = CGPointZero;
    offsetPoint.x = iCurrentPage * width;
    [scrollView setContentOffset:offsetPoint];
}

#pragma mark -
#pragma mark MySingleCamViewControllerDelegate
- (void)changeToSpecificLayout:(NSString *)layout {
    if([layout isEqualToString:@"1X2"]){
        [self changeLayout_1X2];
    }else if([layout isEqualToString:@"1X3"]){
        [self changeLayout_1X3];
    }else if([layout isEqualToString:@"2X2"]){
        [self changeLayout_2X2];
    }else if([layout isEqualToString:@"2X3"]){
        [self changeLayout_2X3];
    }else if([layout isEqualToString:@"3X5"]){
        [self changeLayout_3X5];
    }
}

- (void)backFromBackBtn:(int) ch_index {
    if(eLayoutType == eLayout_1X1) {
        backFrom1x1 = true;
        [self stopStreamAndBackToSiteList];
    }else {
        backFrom1x1 = false;
        [self switchToCameraIndex:ch_index];
    }
}

- (void)singleViewEditBtnPressed {
    [self editMyViewInfo];
}

@end
