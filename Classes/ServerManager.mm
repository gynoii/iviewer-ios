//
//  ServerManager.mm
//
//  Created by jerrylu on 13/07/09.
//  Copyright 2010. All rights reserved.
//

#import "ServerManager.h"
#import "LiveViewAppDelegate.h"
#import "Utility.h"

@implementation ServerManager

@synthesize siteDelegate;
@synthesize displayDelegate;
@synthesize playbackDelegate;
@synthesize eventDelegate;
@synthesize isSingleCam;
@synthesize isMyViewMode;
@synthesize isChangeMyViewLayout;
@synthesize isUnsupportedFormat;
@synthesize myViewInfo = _myViewInfo;
@synthesize myViewBackupInfo = _myViewBackupInfo;

- (id)init {
	if ((self = [super init])) {

	}
    
    m_ControlHelperLoginNum = 0;
    m_ControlHelperLoginFailedNum = 0;
    m_CameraTotalNum = 0;
    m_ControlHelperArray = nil;
    m_ControlHelperLoginFailedArray = nil;
    _myViewInfo = nil;
    _myViewBackupInfo = nil;
    m_checkServerConnectionThread = [[NSThread alloc] initWithTarget:self selector:@selector(checkDisconnectedServer) object:NULL];
    m_AlertViewArray = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EnterBackground) name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];

    talkEnable = NO;
    m_AudioController = [[AudioController alloc] init];
    
    audioplayer = [[ALplayer alloc] init];
    [audioplayer initOpenAL];
    
    return self;
}

- (void)dealloc {
    [_myViewBackupInfo release];
    [super dealloc];
}

#pragma mark -
#pragma mark Server Control

- (void)setAllServer:(NSMutableArray *)serverlist {    
    m_ControlHelperArray = [[NSMutableArray alloc] init];
    m_CameraCountArray = [[NSMutableArray alloc] init];
    m_ControlHelperLoginFailedArray = [[NSMutableArray alloc] init];
    for (int i=0; i<serverlist.count; i++) {
        ControlHelper * helper = [[ControlHelper alloc] init];
        if (isMyViewMode) {
            [helper setMyViewServerInfo:[serverlist objectAtIndex:i]];
        }
        else {
            [helper setServerInfo:[serverlist objectAtIndex:i]];
        }
        [m_ControlHelperArray addObject:helper];
        [helper release];
        [m_CameraCountArray addObject:[NSNumber numberWithInt:0]];
    }
}

- (void)deleteAllServer {
    [m_ControlHelperArray release];
    m_ControlHelperArray = nil;
    [m_CameraCountArray release];
    m_CameraCountArray = nil;
    [m_ControlHelperLoginFailedArray release];
    m_ControlHelperLoginFailedArray = nil;
    
    if (m_DigitalOutputCountArray != nil)
        [m_DigitalOutputCountArray release];
    if (m_DigitalInputCountArray != nil)
        [m_DigitalInputCountArray release];
}

- (void)loginAllServer {
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:i];
        if ([ctrlhlpr isLogouting] == NO)
        {
            [ctrlhlpr setServerManagerDelegate:self];
            
            if(!isMyViewMode) { // My Server
                [ctrlhlpr login];
            }
            else { // My View
                MyViewServerInfo *mySinfo = [_myViewInfo.serverList objectAtIndex:i];
                NSMutableArray *serverlist = [[NSMutableArray alloc] init];
                if (siteDelegate != nil) {
                    if ([siteDelegate respondsToSelector:@selector(serverManagerGetServerlistByServerID:userName:serverlist:)]) {
                        [siteDelegate serverManagerGetServerlistByServerID:mySinfo.serverID userName:mySinfo.userName serverlist:serverlist];
                    }
                }
                [ctrlhlpr loginMyView:serverlist];
                [serverlist release];
            }
        }
    }
}

- (void)loginAllServerByEvent:(NSMutableArray *)serverList event:(Eventinfo *)eventInfo {
    m_ControlHelperArray = [[NSMutableArray alloc] init];
    m_CameraCountArray = [[NSMutableArray alloc] init];
    
    ControlHelper * helper = [[ControlHelper alloc] init];
    [helper setServerManagerDelegate:self];
    [helper setEventInfo:eventInfo];
    [m_ControlHelperArray addObject:helper];
    [m_CameraCountArray addObject:[NSNumber numberWithInt:0]];
    
    [helper loginByEvent:serverList]; // login by event with one control helper
    [helper release];
}

- (void)reLoginMyView {
    [self logoutAllServer];
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    [appDelegate reconnectToMyView:_myViewInfo];
}

- (void)logoutAllServer {
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_STOP_QUERY_THREAD object:[UIApplication sharedApplication]];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];

    [self dismissAllAlert];
    
    if((NSNull *)m_checkServerConnectionThread != [NSNull null] && [m_checkServerConnectionThread isExecuting])
    {
        [m_checkServerConnectionThread cancel];
        //while (![m_checkServerConnectionThread isFinished])
        //    [NSThread sleepForTimeInterval:0.5];
    }
    [m_checkServerConnectionThread release];
    
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:i];
        [ctrlhlpr setServerManagerDelegate:nil];
        [ctrlhlpr setLoadingDelegate:nil];
        if (ctrlhlpr.isRunning)
            [ctrlhlpr logout];
        else {
            // clean the fake list
            ctrlhlpr.m_vecExtCheckedDevice.clear();
            ctrlhlpr.m_vecCheckedDevice.clear();
            ctrlhlpr.m_vecCamSupportSecondProfile.clear();
        }
    }
    
    if ((NSNull *)m_AudioController != [NSNull null]) {
        if (talkEnable) {
            m_AudioController.audioHandleDelegate = nil;
            [m_AudioController stopIOUnit];
        }
        
        [m_AudioController release];
        m_AudioController = nil;
    }
    
    if ((NSNull *)audioplayer != [NSNull null]) {
        [audioplayer cleanUpOpenALID];
        [audioplayer cleanUpOpenAL];
        [audioplayer release];
        audioplayer = nil;
    }
}

- (void)abortLoginAllServer {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];
    
    siteDelegate = nil;
    displayDelegate = nil;
    
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:i];
        [ctrlhlpr setServerManagerDelegate:nil];
        [ctrlhlpr setLoadingDelegate:nil];
    }
}

- (void)loginSuccessByIndex:(int)index CameraCount:(int)count {
    m_ControlHelperLoginNum++;
    m_CameraTotalNum += count;
    [m_CameraCountArray replaceObjectAtIndex:index withObject:[NSNumber numberWithInt:count]];
    
    // Login All Server
    if ((m_ControlHelperLoginNum+m_ControlHelperLoginFailedNum) == m_ControlHelperArray.count) {
        [self loginFinished];
    }
}

- (void)loginFailedByIndex:(int)index {
    m_ControlHelperLoginFailedNum++;
    
    if (isMyViewMode) {
        MyViewServerInfo *mysinfo = [[m_ControlHelperArray objectAtIndex:index] myViewServerInfo];
        m_CameraTotalNum += [[mysinfo cameraList] count];
        [m_CameraCountArray replaceObjectAtIndex:index withObject:[NSNumber numberWithInt:[[mysinfo cameraList] count]]];
    }
    
    // Login All Server
    if ((m_ControlHelperLoginNum+m_ControlHelperLoginFailedNum) == m_ControlHelperArray.count) {
        [self loginFinished];
    }
}

- (void)loginFinished {
    if (m_ControlHelperLoginNum == 0) {
        if (siteDelegate != nil) {
            if ([siteDelegate respondsToSelector:@selector(serverManagerCantConnectToServer:)]) {
                [siteDelegate serverManagerCantConnectToServer:self];
            }
        }
    }
    else {
        if (isMyViewMode) {
            [self backupMyViewLayout]; // use backup info to connect

            for(int i = 0; i < [m_ControlHelperArray count]; i++) {
                ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:i];
                if (!ctrlhlpr.isRunning) {
                    [ctrlhlpr create_FakeCameraList];
                }
            }
            
            int max_order = 0;
            NSString *srvlist_str = [[[NSString alloc] initWithFormat:@""] autorelease];
            for (int i = 0; i < [_myViewBackupInfo.serverList count]; i++) {
                ControlHelper * ctrlHelper = [m_ControlHelperArray objectAtIndex:i];
                if (ctrlHelper.isRunning == NO) {
                    [self insertDisconnectedServer:ctrlHelper enableAlert:NO];
                    
                    if ([srvlist_str length] == 0) {
                        if (ctrlHelper.serverInfo.connectionType == eIPConnection)
                            srvlist_str = [srvlist_str stringByAppendingFormat:@"%@",ctrlHelper.serverInfo.serverName];
                        else
                            srvlist_str = [srvlist_str stringByAppendingFormat:@"%@",ctrlHelper.serverInfo.serverName_P2P];
                    }
                    else {
                        if (ctrlHelper.serverInfo.connectionType == eIPConnection)
                            srvlist_str = [srvlist_str stringByAppendingFormat:@" %@",ctrlHelper.serverInfo.serverName];
                        else
                            srvlist_str = [srvlist_str stringByAppendingFormat:@" %@",ctrlHelper.serverInfo.serverName_P2P];
                    }
                }
                else {
                    MyViewServerInfo *sinfo = [_myViewBackupInfo.serverList objectAtIndex:i];
                    for (int j = 0; j < [sinfo.cameraList count]; j++) {
                        BOOL isFound = NO;
                        MyViewCameraInfo *cinfo = [sinfo.cameraList objectAtIndex:j];
                        
                        if (ctrlHelper.serverInfo.serverType == eServer_Crystal) {
                            for (int k = 0; k < ctrlHelper.m_vecExtCheckedDevice.size(); k++) {
                                Np_SubDevice_Ext subdevice = ctrlHelper.m_vecExtCheckedDevice.at(k);
                                NSString *cid = [NSString stringWithFormat:@"%llu", subdevice.ID.centralID];
                                NSString *lid = [NSString stringWithFormat:@"%llu", subdevice.ID.localID];
                                if ([cinfo.camCentralID isEqualToString:cid] &&
                                    [cinfo.camLocalID isEqualToString:lid]) {
                                    if (cinfo.cameraOrder > max_order)
                                        max_order = cinfo.cameraOrder;
                                    isFound = YES;
                                    break;
                                }
                            }
                        }
                        else {
                            for (int k = 0; k < ctrlHelper.m_vecCheckedDevice.size(); k++) {
                                Np_SubDevice subdevice = ctrlHelper.m_vecCheckedDevice[k].SensorDevices[0];
                                NSString *cid = [NSString stringWithFormat:@"%d", subdevice.ID.centralID];
                                NSString *lid = [NSString stringWithFormat:@"%d", subdevice.ID.localID];
                                if ([cinfo.camCentralID isEqualToString:cid] &&
                                    [cinfo.camLocalID isEqualToString:lid]) {
                                    if (cinfo.cameraOrder > max_order)
                                        max_order = cinfo.cameraOrder;
                                    isFound = YES;
                                    break;
                                }
                            }
                        }
                        
                        if (!isFound) {
                            // The camera list is changed
                            isChangeMyViewLayout = YES;
                            [sinfo.cameraList removeObjectAtIndex:j];
                            j--;
                        }
                    }
                }
            }
            
            // reset the camera order
            for (int i = 0; i < m_CameraTotalNum; i++) {
                BOOL isfoundorder = NO;
                MyViewCameraInfo *target_cinfo = nil;
                int mini_order = max_order + 1;
                for (MyViewServerInfo *sinfo in _myViewBackupInfo.serverList) {
                    for (MyViewCameraInfo *cinfo in sinfo.cameraList) {
                        if (cinfo.cameraOrder == i) {
                            isfoundorder = YES;
                            break;
                        }
                        else if (cinfo.cameraOrder > i && cinfo.cameraOrder < mini_order) {
                            target_cinfo = cinfo;
                            mini_order = cinfo.cameraOrder;
                        }
                    }
                    if (isfoundorder)
                        break;
                }
                if (isfoundorder)
                    continue;
                else
                    target_cinfo.cameraOrder = i;
            }
            
            if (m_ControlHelperLoginFailedNum > 0) {
                NSString *msg = [[[NSString alloc] initWithFormat:NSLocalizedString(@"Server %@ disconnected.", nil), srvlist_str] autorelease];
                [self performSelectorOnMainThread:@selector(showManagerAlert:) withObject:msg waitUntilDone:NO];
                if (![m_checkServerConnectionThread isExecuting])
                    [m_checkServerConnectionThread start];
            }
            
            if (isChangeMyViewLayout && m_CameraTotalNum > 0) {
                [self saveMyViewLayout];
                NSString *msg = [[[NSString alloc] initWithFormat:NSLocalizedString(@"Some cameras are disabled/deleted.", nil)] autorelease];
                [self performSelectorOnMainThread:@selector(showManagerAlert:) withObject:msg waitUntilDone:NO];
            }
        }
        
        if (siteDelegate != nil) {
            if ([siteDelegate respondsToSelector:@selector(serverManagerSaveInfo:)])
                [siteDelegate serverManagerSaveInfo:self];
            
            if (isMyViewMode) {
                if ([siteDelegate respondsToSelector:@selector(serverManagerDidGetCameraCount:CameraCount:)])
                    [siteDelegate serverManagerDidGetCameraCount:self CameraCount:m_CameraTotalNum];
            }else {
                if([[[m_ControlHelperArray objectAtIndex:0] serverInfo] serverType] == eServer_Crystal) {
                    if ([siteDelegate respondsToSelector:@selector(serverManagerloginCrystalServer:)])
                        [siteDelegate serverManagerloginCrystalServer:self];
                }
                else {
                    if ([siteDelegate respondsToSelector:@selector(serverManagerDidGetCameraCount:CameraCount:)])
                        [siteDelegate serverManagerDidGetCameraCount:self CameraCount:m_CameraTotalNum];
                }
            }
        }
    }
}

- (void)backupMyViewLayout {
    NSMutableArray * serverInfoList = [[NSMutableArray alloc] init];
    for (MyViewServerInfo * sinfo in [_myViewInfo serverList]) {
        NSMutableArray * cameraInfoList = [[NSMutableArray alloc] init];
        for (MyViewCameraInfo * cinfo in [sinfo cameraList]) {
            MyViewCameraInfo *new_cinfo = [[[MyViewCameraInfo alloc] init] autorelease];
            [new_cinfo setCamCentralID:cinfo.camCentralID];
            [new_cinfo setCamLocalID:cinfo.camLocalID];
            [new_cinfo setRsCentralID:cinfo.rsCentralID];
            [new_cinfo setRsLocalID:cinfo.rsLocalID];
            [new_cinfo setCameraName:cinfo.cameraName];
            [new_cinfo setCameraOrder:cinfo.cameraOrder];
            [cameraInfoList addObject:new_cinfo];
        }
        MyViewServerInfo *new_sinfo = [[[MyViewServerInfo alloc] init] autorelease];
        [new_sinfo setServerID:sinfo.serverID];
        [new_sinfo setUserName:sinfo.userName];
        [new_sinfo setCameraList:cameraInfoList];
        [serverInfoList addObject:new_sinfo];
        [cameraInfoList release];
    }
    _myViewBackupInfo = [[MyViewInfo alloc] init];
    [_myViewBackupInfo setViewName:_myViewInfo.viewName];
    [_myViewBackupInfo setServerList:serverInfoList];
    [_myViewBackupInfo setMyViewLayoutType:_myViewInfo.myViewLayoutType];
    [serverInfoList release];
}

- (void)insertDisconnectedServer:(ControlHelper *)ctrhlpr enableAlert:(BOOL)enable {
    ControlHelper *new_ctrlhlpr = [[[ControlHelper alloc] init] autorelease];
    [new_ctrlhlpr setServerInfo:ctrhlpr.serverInfo];
    [m_ControlHelperLoginFailedArray addObject:new_ctrlhlpr];

    [ctrhlpr setIsServerDisconnected:YES];

    if (enable) {
        NSString *msg;
        if (ctrhlpr.serverInfo.connectionType == eIPConnection)
            msg = [[[NSString alloc] initWithFormat:NSLocalizedString(@"Server %@ disconnected.", nil), ctrhlpr.serverInfo.serverName] autorelease];
        else
            msg = [[[NSString alloc] initWithFormat:NSLocalizedString(@"Server %@ disconnected.", nil), ctrhlpr.serverInfo.serverName_P2P] autorelease];
        [self performSelectorOnMainThread:@selector(showManagerAlert:) withObject:msg waitUntilDone:NO];
    }
    
    if (![m_checkServerConnectionThread isExecuting])
        [m_checkServerConnectionThread start];
    
}

- (void)checkDisconnectedServer {
    NSMutableArray *serverNameList = [[[NSMutableArray alloc] init] autorelease];

    while (TRUE) {
        for (int i = 0; i < 20; i++) {
            [NSThread sleepForTimeInterval:2];
        
            if ([[NSThread currentThread] isCancelled]) {
                [NSThread exit];
                return;
            }
        }
        
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
        
        [serverNameList removeAllObjects];
        
        for (int i = 0; i < [m_ControlHelperLoginFailedArray count]; i++) {
            ControlHelper * ctrlHelper = [m_ControlHelperLoginFailedArray objectAtIndex:i];
            if (!ctrlHelper.isRunning) {
                if([ctrlHelper loginAndGetCameraList]) {
                    if (ctrlHelper.serverInfo.connectionType == eIPConnection)
                        [serverNameList addObject:ctrlHelper.serverInfo.serverName];
                    else
                        [serverNameList addObject:ctrlHelper.serverInfo.serverName_P2P];
                    
                    [ctrlHelper logout];
                    [m_ControlHelperLoginFailedArray removeObject:ctrlHelper];
                    i--;
                }
            }
            
            if ([[NSThread currentThread] isCancelled]) {
                [NSThread exit];
                return;
            }
        }
        
        if (serverNameList.count > 0) {
            NSString *srvlist_str = [[[NSString alloc] initWithFormat:@""] autorelease];
            for (NSString *srv_name in serverNameList)
                srvlist_str = [srvlist_str stringByAppendingFormat:@"%@ ",srv_name];
            NSString *msg = [[[NSString alloc] initWithFormat:NSLocalizedString(@"Server %@ is available now. Do you want to update now?", nil), srvlist_str] autorelease];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                if (displayDelegate != nil) {
                    if ([displayDelegate respondsToSelector:@selector(serverManagerRecordingServerAvailable:msg:)]) {
                        [displayDelegate serverManagerRecordingServerAvailable:self msg:msg];
                    }
                }
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
                [m_AlertViewArray addObject:alert];
                [alert release];
            }
        }
        
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
    }
}

- (BOOL)isServerLogouting {
    for (int i = 0; i <m_ControlHelperArray.count; i++) {
        if ([[m_ControlHelperArray objectAtIndex:i] isLogouting])
            return YES;
    }
    return NO;
}

- (BOOL)isServerLogoutingByIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return YES;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] isLogouting];
}

- (void)goLiveViewFromEventView:(id)displaydelegate {
    eventDelegate = nil;
    displayDelegate = displaydelegate;
}

- (void)EnterBackground {
    UIApplication *app = [UIApplication sharedApplication];
    UIBackgroundTaskIdentifier _bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"background task %d ExpirationHandler fired remaining time %d.",_bgTask, (int)app.backgroundTimeRemaining);
    }];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if([m_checkServerConnectionThread isExecuting])
        {
            [m_checkServerConnectionThread cancel];
            //while (![m_checkServerConnectionThread isFinished])
            //[NSThread sleepForTimeInterval:0.5];
        }
        [m_checkServerConnectionThread release];
        
        for (int i=0; i<m_ControlHelperArray.count; i++) {
            ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:i];
            [ctrlhlpr setServerManagerDelegate:nil];
            [ctrlhlpr setLoadingDelegate:nil];
            if (ctrlhlpr.isRunning)
                [ctrlhlpr logout];
            else {
                // clean the fake list
                ctrlhlpr.m_vecExtCheckedDevice.clear();
                ctrlhlpr.m_vecCheckedDevice.clear();
                ctrlhlpr.m_vecCamSupportSecondProfile.clear();
            }
        }
        [[UIApplication sharedApplication] endBackgroundTask:_bgTask];
    });

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];
}

- (void)showManagerAlert:(NSString *) msg {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if (displayDelegate != nil) {
            if ([displayDelegate respondsToSelector:@selector(serverManagerShowDialog:msg:)]) {
                [displayDelegate serverManagerShowDialog:self msg:msg];
            }
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
        [m_AlertViewArray addObject:alert];
        [alert release];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
	if ([alertView.message hasSuffix:[NSString stringWithFormat:NSLocalizedString(@"is available now. Do you want to update now?", nil)]]) {
        [self reLoginMyView];
    }
}

- (void)dismissAllAlert {
    if ((NSNull *)m_AlertViewArray == [NSNull null])
        return;
    
    for (UIAlertView *alert in m_AlertViewArray) {
        alert.delegate = nil;
        [alert dismissWithClickedButtonIndex:0 animated:NO];
    }
    
    [m_AlertViewArray removeAllObjects]; // jerrylu, 2013/02/01, fix bug11810
    [m_AlertViewArray release];
    m_AlertViewArray = nil;
}

#pragma mark -

- (int)getCameraTotalNum {
    return m_CameraTotalNum;
}

- (NSString*)getServerManagerTitle {
    if (!isMyViewMode) {
        ControlHelper *ctrhlpr = [m_ControlHelperArray objectAtIndex:0];
        if ([[ctrhlpr serverInfo] connectionType] == eIPConnection)
            return [[ctrhlpr serverInfo] serverName];
        else
            return [[ctrhlpr serverInfo] serverName_P2P];
    }
    else {
        return _myViewBackupInfo.viewName;
    }
}

- (ELayoutType)getLayoutType {
    ControlHelper *ctrhlpr = [m_ControlHelperArray objectAtIndex:0];
    if (isMyViewMode) {
        //return ctrhlpr.myViewServerInfo.myViewLayoutType;
        return _myViewInfo.myViewLayoutType;
    }
    else {
        return ctrhlpr.serverInfo.layoutType;
    }
}

- (EServerType)getServerTypeByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    return [[[m_ControlHelperArray objectAtIndex:srv_index] serverInfo] serverType];
}

- (EServerType)getServerTypeBySrvIndex:(int)index {
    return [[[m_ControlHelperArray objectAtIndex:index] serverInfo] serverType];
}

- (BOOL)isConnectionWithP2PServer {
    BOOL ret = YES;
    
    if (isMyViewMode) {
        ret = NO;
    }
    else {
        for (int i=0; i<m_ControlHelperArray.count; i++) {
            if ([[[m_ControlHelperArray objectAtIndex:i] serverInfo] connectionType] == eIPConnection)
                ret = NO;
        }
    }
    return ret;
}

#if FUNC_P2P_SAT_SUPPORT
- (BOOL)isP2PConnectionWithRelayMode {
    BOOL ret = NO;
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        if ([[[m_ControlHelperArray objectAtIndex:i] serverInfo] connectionType] == eP2PConnection &&
            [[m_ControlHelperArray objectAtIndex:i] isRelayMode])
            ret = YES;
    }
    return ret;
}

- (void)resetP2PRelayQuota {
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:i];
        if ([[ctrlhlpr serverInfo] connectionType] == eP2PConnection && [ctrlhlpr isRelayMode]) {
            [ctrlhlpr quotaReset];
        }
    }
}
#endif

- (BOOL)isAllSupportSecondStream {
    BOOL ret = YES;
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        if ([[m_ControlHelperArray objectAtIndex:i] bAllCamSupportSecondProfile] == NO)
            ret = NO;
    }
    return ret;
}

- (BOOL)isSupportSecondStreamByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] m_vecCamSupportSecondProfile][ca_index];
}

- (void)updateAudioAndPtzDeviceByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    if (ctrlhlpr.isRunning && !ctrlhlpr.isServerDisconnected) {
        [ctrlhlpr update_AudioAndPtzDeviceInfoOnCam:ca_index];
    }
}

#pragma mark -
#pragma mark Connection Control
- (void)connectToCam:(int)index layout:(ELayoutType)layout {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    if (ctrlhlpr.isRunning && !ctrlhlpr.isServerDisconnected) {
        [ctrlhlpr connectToCam:ca_index eLayout:layout];
    }
}

- (void)setLayoutType:(ELayoutType)layout {
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:0];
    if (isMyViewMode) {
        //ctrlhlpr.myViewServerInfo.myViewLayoutType = layout;
        _myViewInfo.myViewLayoutType = layout;
    } else {
        ctrlhlpr.serverInfo.layoutType = layout;
    }
}

- (void)connectToCamWithHighResolution:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    if (ctrlhlpr.isRunning && !ctrlhlpr.isServerDisconnected) {
        [ctrlhlpr connectToCamWithHighResolution:ca_index];
    }
}

- (void)stopconnectToCam {
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        [[m_ControlHelperArray objectAtIndex:i] stopconnectToCam];
    }
}

- (void)disconnectAllCam {
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        [[m_ControlHelperArray objectAtIndex:i] disconnectAllCam];
    }
}

- (void)disconnectAllCamExceptFrom:(int)start_cam To:(int)end_cam {
    if (isMyViewMode) {
        for (ControlHelper * ctrlHelper in m_ControlHelperArray) {
            for (int i = 0; i < [ctrlHelper.cameraHelperArray count]; i++) {
                int index = [self getIndexOfChannelByServer:ctrlHelper index:i];
                if (index < start_cam || index > end_cam) {
                    CameraHelper * camHelper = [ctrlHelper.cameraHelperArray objectAtIndex:i];
                    if ([camHelper respondsToSelector:@selector(liveViewDisconnect)]) {
                        [camHelper liveViewDisconnect];
                    }
                }
            }
        }
    }
    else {
        ControlHelper * ctrlHelper = [m_ControlHelperArray objectAtIndex:0];
        for (int i = 0; i < [ctrlHelper.cameraHelperArray count]; i++) {
            if (i < start_cam || i > end_cam) {
                CameraHelper * camHelper = [ctrlHelper.cameraHelperArray objectAtIndex:i];
                if ([camHelper respondsToSelector:@selector(liveViewDisconnect)]) {
                    [camHelper liveViewDisconnect];
                }
            }
        }
    }
}

- (void)disconnectAllPlaybackConnection {
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        [[m_ControlHelperArray objectAtIndex:i] disconnectAllPlaybackConnection];
    }
}

- (BOOL)isCamStreaming:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    ControlHelper * ctrlHelper = [m_ControlHelperArray objectAtIndex:srv_index];
    if (!ctrlHelper.isRunning)
        return YES;
    else
        return [[m_ControlHelperArray objectAtIndex:srv_index] isCamStreaming:ca_index];
}
/*
- (Np_Device)getDeviceByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    return [[m_ControlHelperArray objectAtIndex:srv_index] m_vecCheckedDevice][ca_index];
}
*/
- (NSString *)getDeviceNameByChIndex:(int)index {
    NSString *device_name = @"";
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return device_name;
    
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    if (ctrlhlpr.serverInfo.serverType != eServer_Crystal) {
        device_name = StringWToNSString(ctrlhlpr.m_vecCheckedDevice.at(ca_index).name);
    }
    else {
        Np_SubDevice_Ext subdevice = ctrlhlpr.m_vecExtCheckedDevice.at(ca_index);
        device_name = [[NSString alloc] initWithFormat:@"%@", [ctrlhlpr getCameraNameFromIDExt:subdevice.ID]];
    }
    return [device_name autorelease];
}

- (int)getDeviceCidByChIndex:(int)index {
    int cid = -1;
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    if (ctrlhlpr.serverInfo.serverType != eServer_Crystal)
        cid = ctrlhlpr.m_vecCheckedDevice.at(ca_index).ID.centralID;
    return cid;
}

- (int)getDeviceLidByChIndex:(int)index {
    int lid = -1;
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    if (ctrlhlpr.serverInfo.serverType != eServer_Crystal)
        lid = ctrlhlpr.m_vecCheckedDevice.at(ca_index).ID.localID;
    return lid;
}

- (unsigned long long)getExtDeviceCidByChIndex:(int)index {
    unsigned long long cid = 0;
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    if (ctrlhlpr.serverInfo.serverType == eServer_Crystal)
        cid = ctrlhlpr.m_vecExtCheckedDevice.at(ca_index).ID.centralID;
    return cid;
}

- (unsigned long long)getExtDeviceLidByChIndex:(int)index {
    unsigned long long lid = 0;
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    if (ctrlhlpr.serverInfo.serverType == eServer_Crystal)
        lid = ctrlhlpr.m_vecExtCheckedDevice.at(ca_index).ID.localID;
    return lid;
}

- (void)reloadAllDevices {
    for (int i=0; i<m_ControlHelperArray.count; i++) {
        [[m_ControlHelperArray objectAtIndex:i] reloadAllDevices];
    }
}

- (BOOL)getLiveViewConnection:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getLiveViewConnection:ca_index];
}

- (BOOL)getPlaybackConnection:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getPlaybackConnection:ca_index];
}

- (void)changeCameraOrderFrom:(int)pre_index to:(int)post_index {
    if (!isMyViewMode)
        return;
    if (pre_index == post_index)
        return;
    
    isChangeMyViewLayout = YES;
#if 1
    MyViewCameraInfo *pre_cinfo, *post_cinfo;
    for (MyViewServerInfo *sinfo in _myViewBackupInfo.serverList) {
        BOOL isfound = NO;
        for (MyViewCameraInfo *cinfo in sinfo.cameraList)
            if(cinfo.cameraOrder == pre_index) {
                pre_cinfo = cinfo;
                isfound = YES;
                break;
            }
        if (isfound)
            break;
    }
    for (MyViewServerInfo *sinfo in _myViewBackupInfo.serverList) {
        BOOL isfound = NO;
        for (MyViewCameraInfo *cinfo in sinfo.cameraList)
            if(cinfo.cameraOrder == post_index) {
                post_cinfo = cinfo;
                isfound = YES;
                break;
            }
        if (isfound)
            break;
    }
    pre_cinfo.cameraOrder = post_index;
    post_cinfo.cameraOrder = pre_index;
#else
    MyViewCameraInfo *target_cinfo;
    for (MyViewServerInfo *sinfo in _myViewBackupInfo.serverList) {
        BOOL isfound = NO;
        for (MyViewCameraInfo *cinfo in sinfo.cameraList)
            if(cinfo.cameraOrder == pre_index) {
                target_cinfo = cinfo;
                isfound = YES;
                break;
            }
        if (isfound)
            break;
    }
    
    if (pre_index < post_index) {
        for (int i = pre_index+1; i <= post_index; i++) {
            BOOL isfound = NO;
            for (MyViewServerInfo *sinfo in _myViewBackupInfo.serverList) {
                for (MyViewCameraInfo *cinfo in sinfo.cameraList) {
                    if (cinfo.cameraOrder == i) {
                        cinfo.cameraOrder--;
                        isfound = YES;
                        break;
                    }
                }
                
                if (isfound)
                    break;
            }
        }
    }
    else if (pre_index > post_index) {
        for (int i = pre_index-1; i >= post_index; i--) {
            BOOL isfound = NO;
            for (MyViewServerInfo *sinfo in _myViewBackupInfo.serverList) {
                for (MyViewCameraInfo *cinfo in sinfo.cameraList) {
                    if (cinfo.cameraOrder == i) {
                        cinfo.cameraOrder++;
                        isfound = YES;
                        break;
                    }
                }
                
                if (isfound)
                    break;
            }
        }
        
    }
    target_cinfo.cameraOrder = post_index;
#endif
}

- (void)saveMyViewLayout {
    isChangeMyViewLayout = NO;
    
    NSMutableArray * serverInfoList = [[NSMutableArray alloc] init];
    for (MyViewServerInfo * sinfo in [_myViewBackupInfo serverList]) {
        if ([sinfo.cameraList count] == 0)
            continue;
        
        NSMutableArray * cameraInfoList = [[NSMutableArray alloc] init];
        for (MyViewCameraInfo * cinfo in [sinfo cameraList]) {
            MyViewCameraInfo *new_cinfo = [[[MyViewCameraInfo alloc] init] autorelease];
            [new_cinfo setCamCentralID:cinfo.camCentralID];
            [new_cinfo setCamLocalID:cinfo.camLocalID];
            [new_cinfo setRsCentralID:cinfo.rsCentralID];
            [new_cinfo setRsLocalID:cinfo.rsLocalID];
            [new_cinfo setCameraName:cinfo.cameraName];
            [new_cinfo setCameraOrder:cinfo.cameraOrder];
            [cameraInfoList addObject:new_cinfo];
        }
        MyViewServerInfo *new_sinfo = [[[MyViewServerInfo alloc] init] autorelease];
        [new_sinfo setServerID:sinfo.serverID];
        [new_sinfo setUserName:sinfo.userName];
        [new_sinfo setCameraList:cameraInfoList];
        [serverInfoList addObject:new_sinfo];
        [cameraInfoList release];
    }
    [_myViewInfo setServerList:serverInfoList];
    _myViewInfo.myViewLayoutType = _myViewBackupInfo.myViewLayoutType;
    [serverInfoList release];
    
    if (siteDelegate != nil) {
        if ([siteDelegate respondsToSelector:@selector(serverManagerSaveInfo:)]) {
            [siteDelegate serverManagerSaveInfo:self];
        }
    }
    
    if (displayDelegate != nil) {
        if ([displayDelegate respondsToSelector:@selector(serverManagerDidSaveLayout:)]) {
            [displayDelegate serverManagerDidSaveLayout:self];
        }
    }
}

#pragma mark -
#pragma mark DI/O Function Control

- (NSMutableArray *)getDODeviceNameList {
    if ((NSNull *)m_DigitalOutputCountArray != [NSNull null])
    {
        [m_DigitalOutputCountArray removeAllObjects];
        [m_DigitalOutputCountArray release];
    }
    m_DigitalOutputCountArray = [[NSMutableArray alloc] init];
    
    if ((NSNull *)m_DigitalOutputDeviceNameArray != [NSNull null])
    {
        [m_DigitalOutputDeviceNameArray removeAllObjects];
        [m_DigitalOutputDeviceNameArray release];
    }
    m_DigitalOutputDeviceNameArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < m_ControlHelperArray.count; i++) {
        NSMutableArray *doDeviceNameArray = [[m_ControlHelperArray objectAtIndex:i] getDODeviceNameList];
        
        [m_DigitalOutputCountArray addObject:[NSNumber numberWithInteger:[doDeviceNameArray count]]];
        for (int j = 0; j < [doDeviceNameArray count]; j++) {
            [m_DigitalOutputDeviceNameArray addObject:[doDeviceNameArray objectAtIndex:j]];
        }
    }
    return m_DigitalOutputDeviceNameArray;
}

- (NSMutableArray *)getDODeviceNameByChIndex:(int)index {
    if ((NSNull *)m_DigitalOutputCountArray != [NSNull null])
    {
        [m_DigitalOutputCountArray removeAllObjects];
        [m_DigitalOutputCountArray release];
    }
    m_DigitalOutputCountArray = [[NSMutableArray alloc] init];
    
    if ((NSNull *)m_DigitalOutputDeviceNameArray != [NSNull null])
    {
        [m_DigitalOutputDeviceNameArray removeAllObjects];
        [m_DigitalOutputDeviceNameArray release];
    }
    m_DigitalOutputDeviceNameArray = [[NSMutableArray alloc] init];
    
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index >= 0 && ca_index >= 0)
    {
        for (int i = 0; i < [m_CameraCountArray count]; i++) {
            if (srv_index == i) {
                ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
                NSMutableArray *doDeviceNameArray = [ctrlhlpr getDODeviceNameOnCam:ca_index];
                
                [m_DigitalOutputCountArray addObject:[NSNumber numberWithInteger:[doDeviceNameArray count]]];
                for (int j = 0; j < [doDeviceNameArray count]; j++) {
                    [m_DigitalOutputDeviceNameArray addObject:[doDeviceNameArray objectAtIndex:j]];
                }
            }
            else {
                [m_DigitalOutputCountArray addObject:[NSNumber numberWithInteger:0]];
            }
        }
        
    }
    return m_DigitalOutputDeviceNameArray;
}

- (NSMutableArray *)getDIDeviceNameList {
    if ((NSNull *)m_DigitalInputCountArray != [NSNull null])
    {
        [m_DigitalInputCountArray removeAllObjects];
        [m_DigitalInputCountArray release];
    }
    m_DigitalInputCountArray = [[NSMutableArray alloc] init];
    
    if ((NSNull *)m_DigitalInputDeviceNameArray != [NSNull null])
    {
        [m_DigitalInputDeviceNameArray removeAllObjects];
        [m_DigitalInputDeviceNameArray release];
    }
    m_DigitalInputDeviceNameArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < m_ControlHelperArray.count; i++) {
        NSMutableArray *diDeviceNameArray = [[m_ControlHelperArray objectAtIndex:i] getDIDeviceNameList];
        
        [m_DigitalInputCountArray addObject:[NSNumber numberWithInteger:[diDeviceNameArray count]]];
        for (int j = 0; j < [diDeviceNameArray count]; j++) {
            [m_DigitalInputDeviceNameArray addObject:[diDeviceNameArray objectAtIndex:j]];
        }
    }
    return m_DigitalInputDeviceNameArray;
}

- (NSMutableArray *)getDIDeviceNameByChIndex:(int)index {
    if ((NSNull *)m_DigitalInputCountArray != [NSNull null])
    {
        [m_DigitalInputCountArray removeAllObjects];
        [m_DigitalInputCountArray release];
    }
    m_DigitalInputCountArray = [[NSMutableArray alloc] init];
    
    if ((NSNull *)m_DigitalInputDeviceNameArray != [NSNull null])
    {
        [m_DigitalInputDeviceNameArray removeAllObjects];
        [m_DigitalInputDeviceNameArray release];
    }
    m_DigitalInputDeviceNameArray = [[NSMutableArray alloc] init];
    
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index >= 0 && ca_index >= 0)
    {
        for (int i = 0; i < [m_CameraCountArray count]; i++) {
            if (srv_index == i) {
                ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
                NSMutableArray *diDeviceNameArray = [ctrlhlpr getDIDeviceNameOnCam:ca_index];
                
                [m_DigitalInputCountArray addObject:[NSNumber numberWithInteger:[diDeviceNameArray count]]];
                for (int j = 0; j < [diDeviceNameArray count]; j++) {
                    [m_DigitalInputDeviceNameArray addObject:[diDeviceNameArray objectAtIndex:j]];
                }
            }
            else {
                [m_DigitalInputCountArray addObject:[NSNumber numberWithInteger:0]];
            }
        }
    }
    return m_DigitalInputDeviceNameArray;
}

- (BOOL)getDOState:(int)index {
    int srv_index = [self getIndexOfServerByDoIndex:index];
    if (srv_index < 0)
        return NO;

    int do_index = [self getIndexOfDoDeviceByIndex:index];
    if (do_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] queryDOState:do_index];
}

- (BOOL)getDIState:(int)index {
    int srv_index = [self getIndexOfServerByDiIndex:index];
    if (srv_index < 0)
        return NO;
    
    int di_index = [self getIndexOfDiDeviceByIndex:index];
    if (di_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] queryDIState:index];
}

- (void)setDOState:(int)index state:(BOOL)state {
    int srv_index = [self getIndexOfServerByDoIndex:index];
    if (srv_index < 0)
        return;
    
    int do_index = [self getIndexOfDoDeviceByIndex:index];
    if (do_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] forceOutPut:do_index state:state];
}

- (BOOL)getDOPrivilege:(int)index {
    int srv_index = [self getIndexOfServerByDoIndex:index];
    if (srv_index < 0)
        return NO;
    
    int do_index = [self getIndexOfDoDeviceByIndex:index];
    if (do_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getDOPrivilege:do_index];
}

- (BOOL)getDOState:(int)do_index chIndex:(int)ch_index {
    int srv_index = [self getIndexOfServerByChannel:ch_index];
    int ca_index = [self getIndexOfCameraByChannel:ch_index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] queryDOStateOnCam:ca_index doIndex:do_index];
}

- (BOOL)getDIState:(int)di_index chIndex:(int)ch_index {
    int srv_index = [self getIndexOfServerByChannel:ch_index];
    int ca_index = [self getIndexOfCameraByChannel:ch_index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] queryDIStateOnCam:ca_index diIndex:di_index];
}

- (void)setDOState:(int)do_index chIndex:(int)ch_index state:(BOOL)state {
    int srv_index = [self getIndexOfServerByChannel:ch_index];
    int ca_index = [self getIndexOfCameraByChannel:ch_index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] forceOutPutOnCam:ca_index doIndex:do_index state:state];
}

- (BOOL)getDOPrivilege:(int)do_index chIndex:(int)ch_index {
    int srv_index = [self getIndexOfServerByChannel:ch_index];
    int ca_index = [self getIndexOfCameraByChannel:ch_index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getDOPrivilegeOnCam:ca_index doIndex:do_index];
}

#pragma mark -
#pragma mark PTZ Function Control

- (BOOL)isSupportPTZ:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getPTZCapOnCam:ca_index];
}

- (void)getPresetByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] getPresetToCam:ca_index];
}

- (BOOL)isGetPresetByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] isGetPresent];
}

- (NSMutableArray *)getPresetNameArrayByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return nil;
    
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    if (ctrlhlpr.isGetPresent == NO)
        return nil;
    return [ctrlhlpr presetNameArray];
}

- (void)setPTZByChIndex:(int)index withDirection:(NSString *)dir {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    [ctrlhlpr setPTZOnCamera:ca_index withDirection:dir];
}

- (void)gotoPTZPresetByChIndex:(int)index withIdx:(int)idx {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    [ctrlhlpr gotoPTZPreset:ca_index withIdx:idx];
}

#pragma mark -
#pragma mark Audio Function Control

- (BOOL)isSupportAudio:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getLiveViewAudioCapOnCam:ca_index];
}

- (BOOL)getLiveViewAudioStateByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getLiveViewAudioStatusOnCam:ca_index];
}

- (void)setLiveviewAudioStateByChIndex:(int)index state:(BOOL) state {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] setLiveviewAudioStateOnCam:ca_index state:state];
    
    [self stopOpenAL];
}

- (BOOL)getPlaybackAudioStateByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getPlaybackAudioStatus];
}

- (void)setPlaybackAudioStateByChIndex:(int)index state:(BOOL) state {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] setPlaybackAudioState:state];
    
    [self stopOpenAL];
}

- (void)stopOpenAL {
    // to fix bug20528
    // must clean buffer first, then stop the audio play
    [audioplayer cleanQueueBuffer];
    [audioplayer stopSound];
}

- (BOOL)isSupportTalk:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getTalkCapOnCam:ca_index];
}

- (BOOL)getTalkStateByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return talkEnable;
}

- (void)setTalkStateByChIndex:(int)index state:(BOOL) state {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    if (talkEnable == state)
        return;
    
    talkEnable = state;
    Np_TalkAudioFormat audiofmt;
    BOOL ret = [[m_ControlHelperArray objectAtIndex:srv_index] setTalkStateOnCam:ca_index state:state audioFormat:&audiofmt];
    if (ret) {
        if (state) {
            [m_AudioController resetupAudioChain:&audiofmt];
            m_AudioController.audioHandleDelegate = [m_ControlHelperArray objectAtIndex:srv_index];
            [m_AudioController startIOUnit];
        }
        else {
            m_AudioController.audioHandleDelegate = nil;
            [m_AudioController stopIOUnit];
        }
    }
    else {
        talkEnable = !talkEnable;
    }
}

- (void)disableTalkFunc {
    talkEnable = NO;
    
    m_AudioController.audioHandleDelegate = nil;
    [m_AudioController stopIOUnit];
    
    Np_TalkAudioFormat audiofmt;
    for (int i=0; i< m_ControlHelperArray.count; i++) {
        ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:i];
        [ctrlhlpr setTalkStateOnCam:0 state:NO audioFormat:&audiofmt];
    }
}

#pragma mark -
#pragma mark Mega-Pixel Function Control
- (BOOL)isSupportSecondProfile:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:srv_index];
    return [ctrlhlpr m_vecCamSupportSecondProfile][ca_index];
}

- (EProfileType)getConnectProfileTypeWithChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    return [[m_ControlHelperArray objectAtIndex:srv_index] getConnectProfileTypeWithCam:ca_index];
}

- (void)setConnectProfileTypeWithChIndex:(int)index profiletype:(EProfileType)profiletype {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] setProfileTypeWithCam:ca_index profiletype:profiletype];
}

#if FUNC_PUSH_NOTIFICATION_SUPPORT
#pragma mark -
#pragma mark Push Notification Function Control

- (NSString *)getFilterPushNotificationServerID {
    ControlHelper *ctrhlpr = [m_ControlHelperArray objectAtIndex:0];
    return [ctrhlpr getServerID];
}

- (NSString *)getFilterPushNotificationUserName {
    ControlHelper *ctrhlpr = [m_ControlHelperArray objectAtIndex:0];
    return [ctrhlpr getUserName];
}

- (BOOL)getPushNotificationEnableByChIndex:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    
    if ([[[m_ControlHelperArray objectAtIndex:srv_index] serverInfo] serverPushNotificationEnable] == kPushNotificationEnable)
        return YES;
    else
        return NO;
}

- (void)setPushNotificationEnableByChIndex:(int)index enable:(BOOL)enable {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return;
    
    if (enable) {
        if ([[m_ControlHelperArray objectAtIndex:srv_index] pnRegisterToServer]) {
            [[[m_ControlHelperArray objectAtIndex:srv_index] serverInfo] setServerPushNotificationEnable:kPushNotificationEnable];
        }
    }
    else {
        if ([[m_ControlHelperArray objectAtIndex:srv_index] pnUnregisterToServer]) {
            [[[m_ControlHelperArray objectAtIndex:srv_index] serverInfo] setServerPushNotificationEnable:kPushNotificationDisable];
        }
    }
}
#endif

#pragma mark -
#pragma mark Playback Function Control

- (BOOL)isSupportPlayback:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] playbackEnable];
}

- (BOOL)isPlaybackStreaming:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return NO;
    
    if ([[m_ControlHelperArray objectAtIndex:srv_index] m_hNuSDKPlayback] == NULL)
        return NO;
    else
        return YES;
}

// Brosso - Timeline
- (void)setTimeLineDelegate:(id)delegate {
    if (nil != delegate) {
        timelineDelegate = delegate;
    }
}

- (void)queryRecordByIndex:(int)index andDate:(NSDate*)date {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] checkPlaybackTimeByNSDate:ca_index startdate:date enddate:date];
}

- (BOOL)playbackStartFromDate:(int)index startdate:(NSDate *)startdate enddate:(NSDate *)enddate {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;

    [self stopOpenAL];
    
    // Brosso - Timeline
    [[m_ControlHelperArray objectAtIndex:srv_index] setTimeLineDelegate:timelineDelegate];
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] playbackStartFromDate:ca_index startdate:startdate enddate:enddate];
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (BOOL)playbackStartFromDate:(int)index recordfile:(EDualRecordFile)recordfile startdate:(NSDate *)startdate enddate:(NSDate *)enddate {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    [self stopOpenAL];
    
    // Brosso - Timeline
    [[m_ControlHelperArray objectAtIndex:srv_index] setTimeLineDelegate:timelineDelegate];
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] playbackStartFromDate:ca_index recordfile:recordfile startdate:startdate enddate:enddate];
}
#endif
- (BOOL)playbackStartFromDateByEvent:(int)index startdate:(NSDate *)startdate enddate:(NSDate *)enddate {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;

    [self stopOpenAL];
    return [[m_ControlHelperArray objectAtIndex:srv_index] playbackStartFromDateByEvent:ca_index startdate:startdate enddate:enddate];
}

- (BOOL)playbackOpenRecord:(int)index startdate:(Np_DateTime)starttime enddate:(Np_DateTime)endtime {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return NO;

    return [[m_ControlHelperArray objectAtIndex:srv_index] playbackOpenRecord:starttime enddate:endtime];
}

- (void)playbackStop:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;

    [[m_ControlHelperArray objectAtIndex:srv_index] playbackStop:ca_index];
}

- (void)playbackStopByEvent:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;

    [[m_ControlHelperArray objectAtIndex:srv_index] playbackStopByEvent:ca_index];
}

- (void)playbackPlay:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return;

    [[m_ControlHelperArray objectAtIndex:srv_index] playbackPlay];
}

- (void)playbackReversePlay:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] playbackReversePlay];
}

- (void)playbackPause:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] playbackPause];

    [self stopOpenAL];
}

- (void)playbackPreviousFrame:(int) index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] playbackPreviousFrame:ca_index];
}

- (void)playbackNextFrame:(int) index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] playbackNextFrame:ca_index];
}

- (void)playbackSeek:(int)index seektime:(Np_DateTime)seektime {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] playbackSeek:seektime];
}

- (float)playbackGetSpeed:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    return [[m_ControlHelperArray objectAtIndex:srv_index] playbackGetSpeed];
}

- (void)playbackSetSpeed:(int)index speed:(float)speed {
    int srv_index = [self getIndexOfServerByChannel:index];
    if (srv_index < 0)
        return;
    
    [[m_ControlHelperArray objectAtIndex:srv_index] playbackSetSpeed:speed];
}

- (Np_DateTime)playbackGetTime:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    return [[m_ControlHelperArray objectAtIndex:srv_index] playbackGetTime];
}

- (Np_PlayerState)playbackGetState:(int)index {
    int srv_index = [self getIndexOfServerByChannel:index];
    return [[m_ControlHelperArray objectAtIndex:srv_index] playbackGetState];    
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (BOOL)playbackChangeRecordFile:(int) index recordfile:(EDualRecordFile) recordfile startdate:(Np_DateTime)starttime enddate:(Np_DateTime)endtime seektime:(Np_DateTime)seektime {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    [self stopOpenAL];
    return [[m_ControlHelperArray objectAtIndex:srv_index] playbackChangeRecordFile:ca_index recordfile:recordfile startdate:starttime enddate:endtime seektime:seektime];
}

- (BOOL)checkPlaybackLogTime:(int) index recordfile:(EDualRecordFile) rf_index datetime:(Np_DateTime *) datetime {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] checkPlaybackLogTime:ca_index recordfile:rf_index datetime:datetime];
}
#endif

#pragma mark -
#pragma mark Crystal Server Control
- (std::vector<Np_Server>)getRecordingServerByMSIndex:(int)index {
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:index];
    if (ctrlhlpr.serverInfo.serverType == eServer_Crystal)
        return ctrlhlpr.m_vecServerList;
    
    std::vector<Np_Server> empty_vector;
    empty_vector.clear();
    return empty_vector;
}

- (BOOL)getCameraListFromByMSIndex:(int)index rsID:(Np_ID_Ext) serverID {
    ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:index];
    if (ctrlhlpr.serverInfo.serverType != eServer_Crystal)
        return NO;
    
    BOOL ret = [ctrlhlpr updateCameraListFromRecordingServer:serverID];
    m_CameraTotalNum = ctrlhlpr.cameraCount;
    [m_CameraCountArray replaceObjectAtIndex:index withObject:[NSNumber numberWithInt:ctrlhlpr.cameraCount]];
    
    if (ctrlhlpr.cameraCount == 0)
        ret = NO;
    
    return ret;
}

#pragma mark -
#pragma mark MyView Edit Function
- (void)matchServerHandler:(NSMutableArray *) targetHlprArray refServerList:(NSMutableArray *) serverInfoList {
    for (int i = 0; i < [m_ControlHelperArray count]; i++) {
        ServerInfo *sinfo = [[m_ControlHelperArray objectAtIndex:i] serverInfo];
        for (int j = 0; j < [serverInfoList count]; j++) {
            ServerInfo *list_sinfo = [serverInfoList objectAtIndex:j];
            if (sinfo.connectionType == list_sinfo.connectionType) {
                switch (sinfo.connectionType) {
                    case eIPConnection:
                        if ([sinfo.serverIP isEqualToString:list_sinfo.serverIP] &&
                            [sinfo.serverPort isEqualToString:list_sinfo.serverPort] &&
                            [sinfo.serverPlaybackPort isEqualToString:list_sinfo.serverPlaybackPort] &&
                            [sinfo.userName isEqualToString:list_sinfo.userName]) {
                            [targetHlprArray replaceObjectAtIndex:j withObject:[m_ControlHelperArray objectAtIndex:i]];
                        }
                        break;
                    case eP2PConnection:
                        if ([sinfo.serverID_P2P isEqualToString:list_sinfo.serverID_P2P] &&
                            [sinfo.userName_P2P isEqualToString:list_sinfo.userName_P2P]) {
                            [targetHlprArray replaceObjectAtIndex:j withObject:[m_ControlHelperArray objectAtIndex:i]];
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

- (void)updateServerHandlerForNewView:(MyViewInfo *) newViewInfo refServerHandler:(NSMutableArray *) targetHlprArray {
    // refresh the backup view info
    [_myViewBackupInfo release];
    _myViewBackupInfo = [newViewInfo retain];
    
    isChangeMyViewLayout = YES;
    
    [self stopconnectToCam];
    
    // renew all control helper
    [m_ControlHelperArray removeAllObjects];
    m_ControlHelperLoginNum = 0;
    m_CameraTotalNum = 0;
    [m_CameraCountArray removeAllObjects];
    
    for (int i = 0; i < [_myViewBackupInfo.serverList count]; i++) {
        MyViewServerInfo *targetsinfo = [_myViewBackupInfo.serverList objectAtIndex:i];
        
        for (int j = 0; j < [targetHlprArray count]; j++) {
            ControlHelper *ctrlhlpr = [targetHlprArray objectAtIndex:j];
            if ((NSNull *)ctrlhlpr == [NSNull null])
                continue;
            
            ServerInfo *sinfo = [ctrlhlpr serverInfo];
            
            if ([targetsinfo.serverID isEqualToString:sinfo.serverID]) {
                if ((sinfo.connectionType == eIPConnection && [targetsinfo.userName isEqualToString:sinfo.userName]) ||
                    (sinfo.connectionType == eP2PConnection && [targetsinfo.userName isEqualToString:sinfo.userName_P2P])) {
                    [ctrlhlpr setMyViewServerInfo:targetsinfo];
                    [ctrlhlpr updateDeviceByMyView];
                    [m_ControlHelperArray addObject:ctrlhlpr];
                    [targetHlprArray replaceObjectAtIndex:j withObject:[NSNull null]];
                    [ctrlhlpr setServerManagerDelegate:self];
                    
                    m_ControlHelperLoginNum++;
                    m_CameraTotalNum += ctrlhlpr.cameraCount;
                    [m_CameraCountArray addObject:[NSNumber numberWithInt:ctrlhlpr.cameraCount]];                    
                }
            }
        }
    }
    
    if (displayDelegate != nil &&
        [displayDelegate respondsToSelector:@selector(serverManagerRefreshAllLayout:)])
        [displayDelegate serverManagerRefreshAllLayout:self];
}

- (void)removeServerHandlerInCurrentView:(NSMutableArray *) targetHlprArray {
    for (int i = 0; i < [targetHlprArray count]; i++) {
        ControlHelper *ctrlhlpr = [targetHlprArray objectAtIndex:i];
        if ((NSNull *)ctrlhlpr == [NSNull null])
            continue;
        
        for (int j = 0; j < [m_ControlHelperArray count]; j++) {
            ControlHelper *current_ctrlhlpr = [m_ControlHelperArray objectAtIndex:j];
            
            BOOL isMatch = NO;
            
            if (ctrlhlpr == current_ctrlhlpr) {
                [targetHlprArray replaceObjectAtIndex:i withObject:[NSNull null]];
                isMatch = YES;
            }
            
            if (isMatch)
                break;
        }
    }
}

#pragma mark -
#pragma mark ControlHelperLoginDelegate Protocol

- (void)controlHelperConnectToServer:(ControlHelper *)helper cameraCount:(int) count {
    int index = [m_ControlHelperArray indexOfObject:helper];
    
    [self loginSuccessByIndex:index CameraCount:count];
}

- (void)controlHelperConnectToCrystalServer:(ControlHelper *)helper {
    if (siteDelegate != nil) {
        if ([siteDelegate respondsToSelector:@selector(serverManagerSaveInfo:)])
            [siteDelegate serverManagerSaveInfo:self];
        
        if ([siteDelegate respondsToSelector:@selector(serverManagerDidGetCameraCount:CameraCount:)])
            [siteDelegate serverManagerDidGetCameraCount:self CameraCount:m_CameraTotalNum];
    }
}

- (void)controlHelperCantConnectToServer:(ControlHelper *)helper {
    int index = [m_ControlHelperArray indexOfObject:helper];
    
    if (eventDelegate == nil) {
        [self loginFailedByIndex:index];
    }
    else {
        [eventDelegate serverManagerEventLoginFailed:self];
    }
}

- (void)controlHelperEventHandleFromServer:(ControlHelper *)helper event:(Np_Event)event {
    if (event.eventID == Np_EVENT_SERVER_CONNECTION_LOST ||
        event.eventID == Np_EVENT_PHYSICAL_DEVICE_TREELIST_UPDATED ||
        event.eventID == Np_EVENT_LOGICAL_DEVICE_TREELIST_UPDATED ||
        event.eventID == Np_EVENT_DEVICE_TREELIST_UPDATED ||
        event.eventID == Np_EVENT_PRIVILEGE_ACCESS_RIGHT_UPDATED ||
        event.eventID == Np_EVENT_PASSWORD_CHANGED ||
        event.eventID == Np_EVENT_ACCOUNT_DELETE ||
        event.eventID == Np_EVENT_SERVICE_NETWORK_SETTING_CHANGED ||
        event.eventID == Np_EVENT_SERVER_OFFLINE
        )
    {
        if (isMyViewMode &&
            (event.eventID == Np_EVENT_SERVER_CONNECTION_LOST ||
             event.eventID == Np_EVENT_SERVER_OFFLINE)) {
            [self insertDisconnectedServer:helper enableAlert:YES];
        }
        else {
            // Control LiveView
            if (displayDelegate != nil) {
                [self logoutAllServer];
            
                int srv_index = [m_ControlHelperArray indexOfObject:helper];
                if ([displayDelegate respondsToSelector:@selector(serverManagerDidServerDown:serverIndex:eventID:)]) {
                    [displayDelegate serverManagerDidServerDown:self serverIndex:srv_index eventID:event.eventID];
                }
            }
        
            // Control EventView
            if (eventDelegate != nil) {
                if ([eventDelegate respondsToSelector:@selector(serverManagerDidServerDown:eventID:)]) {
                    [eventDelegate serverManagerDidServerDown:self eventID:event.eventID];
                }
            }
        }
    }
}

- (void)controlHelperEventHandleFromServer:(ControlHelper *)helper event_ext:(Np_Event_Ext)event {
    if (event.eventID == Np_EVENT_CONFIG_UPDATE_setServerList ||
        event.eventID == Np_EVENT_CONFIG_UPDATE_removeServerList ||
        event.eventID == Np_EVENT_CONFIG_UPDATE_addUnitDevices ||
        event.eventID == Np_EVENT_CONFIG_UPDATE_modifyUnitDevices ||
        event.eventID == Np_EVENT_CONFIG_UPDATE_removeUnitDevices ||
        event.eventID == Np_EVENT_CONFIG_UPDATE_modifyDeviceBasicInfo ||
        event.eventID == Np_EVENT_CONFIG_UPDATE_setCameraAssociateList ||
        event.eventID == Np_EVENT_SERVER_OFFLINE ||
        event.eventID == Np_EVENT_SERVER_CONNECTION_LOST
        )
    {
        if (event.sourceID.centralID != 0 &&
            (event.eventID == Np_EVENT_SERVER_OFFLINE ||
             event.eventID == Np_EVENT_SERVER_CONNECTION_LOST)
            ) {
            // Recording Server Disconnected
            for (int i = 0; i < helper.m_vecServerList.size(); i++) {
                if (event.sourceID.centralID == helper.m_vecServerList.at(i).ID.centralID) {
                    NSString *msg = [[[NSString alloc] initWithFormat:NSLocalizedString(@"Recording server %@ is disconnected", nil), WChartToNSString(helper.m_vecServerList.at(i).name)] autorelease];
                    [self performSelectorOnMainThread:@selector(showManagerAlert:) withObject:msg waitUntilDone:NO];
                    break;
                }
            }
        }
        else if (isMyViewMode &&
                 (event.eventID == Np_EVENT_SERVER_CONNECTION_LOST ||
                  event.eventID == Np_EVENT_SERVER_OFFLINE)) {
            [self insertDisconnectedServer:helper enableAlert:YES];
        }
        else {
            // Control LiveView
            if (displayDelegate != nil) {
                [self logoutAllServer];
            
                int srv_index = [m_ControlHelperArray indexOfObject:helper];
                if ([displayDelegate respondsToSelector:@selector(serverManagerDidServerDown:serverIndex:eventID:)]) {
                    [displayDelegate serverManagerDidServerDown:self serverIndex:srv_index eventID:event.eventID];
                }
            }
            
            // Control EventView
            if (eventDelegate != nil) {
                if ([eventDelegate respondsToSelector:@selector(serverManagerDidServerDown:eventID:)]) {
                    [eventDelegate serverManagerDidServerDown:self eventID:event.eventID];
                }
            }
        }
    }
}

- (void)controlHelperDidRefreshImages:(ControlHelper *)helper camIndex:(int)index camImg:(UIImage *)img{
    int ch_index = [self getIndexOfChannelByServer:helper index:index];
    if (ch_index < 0)
        return;
    
    if (displayDelegate != nil) {
        if ([displayDelegate respondsToSelector:@selector(serverManagerDidRefreshImages:camIndex:camImg:)]) {
            [displayDelegate serverManagerDidRefreshImages:self camIndex:ch_index camImg:img];
        }
    }
    
    if (eventDelegate != nil) {
        if ([eventDelegate respondsToSelector:@selector(serverManagerEventDidRefreshImages:camIndex:camImg:)]) {
            [eventDelegate serverManagerEventDidRefreshImages:self camIndex:index camImg:img];
        }
    }
}

- (void)controlHelperDidRefreshImagesBuf:(ControlHelper *)helper camIndex:(int)index img:(NSData *)img imgWidth:(int)width imgHeight:(int)height {
    int ch_index = [self getIndexOfChannelByServer:helper index:index];
    if (ch_index < 0)
        return;
    
    if (displayDelegate != nil) {
        if ([displayDelegate respondsToSelector:@selector(serverManagerDidRefreshImageBuf:camIndex:img:width:height:)]) {
            [displayDelegate serverManagerDidRefreshImageBuf:self camIndex:ch_index img:img width:width height:height];
        }
    }
    
    if (eventDelegate != nil) {
        if ([eventDelegate respondsToSelector:@selector(serverManagerEventDidRefreshImageBuf:camIndex:img:width:height:)]) {
            [eventDelegate serverManagerEventDidRefreshImageBuf:self camIndex:ch_index img:img width:width height:height];
        }
    }
}

- (void)controlHelperDidCameraSessionLost:(ControlHelper *) helper camIndex:(int)index {
    int ch_index = [self getIndexOfChannelByServer:helper index:index];
    if (ch_index < 0)
        return;
    
    if (displayDelegate != nil) {
        if ([displayDelegate respondsToSelector:@selector(serverManagerDidCameraSessionLost:camIndex:)]) {
            [displayDelegate serverManagerDidCameraSessionLost:self camIndex:ch_index];
        }
    }
}

- (void)controlHelperDidPlaybackDisconnection:(ControlHelper *) helper {
    if (displayDelegate != nil) {
        if ([displayDelegate respondsToSelector:@selector(serverManagerDidPlaybackDisconnection:)]) {
            [displayDelegate serverManagerDidPlaybackDisconnection:self];
        }
    }
    
    if (eventDelegate != nil) {
        if ([eventDelegate respondsToSelector:@selector(serverManagerDidPlaybackDisconnection:)]) {
            [eventDelegate serverManagerDidPlaybackDisconnection:self];
        }
    }
}

- (void)controlHelperDidUnsupportedFormat {
    isUnsupportedFormat = true;
    if (playbackDelegate != nil) {
        if ([playbackDelegate respondsToSelector:@selector(serverManagerUnsupportedFormat)]) {
            [playbackDelegate serverManagerUnsupportedFormat];
        }
    }
}


- (void)controlHelperQuotaTimeout:(ControlHelper *) helper {
    [self logoutAllServer];
    
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    [appDelegate popToRootView];
}

- (void)controlHelperDidPbNextPreviosFrameSuccess:(ControlHelper *) helper {
    if (displayDelegate != nil) {
        if ([displayDelegate respondsToSelector:@selector(serverManagerDidPbNextPreviosFrameSuccess:)]) {
            [displayDelegate serverManagerDidPbNextPreviosFrameSuccess:self];
        }
    }
    
    if (eventDelegate != nil) {
        if ([eventDelegate respondsToSelector:@selector(serverManagerDidPbNextPreviosFrameSuccess:)]) {
            [eventDelegate serverManagerDidPbNextPreviosFrameSuccess:self];
        }
    }
}

- (void)controlHelperAlertPlaybackEvent:(ControlHelper *) helper camIndex:(int) index event:(EPlaybackEvent)event {
    if (playbackDelegate != nil) {
        if ([playbackDelegate respondsToSelector:@selector(serverManagerAlertPlaybackEvent:event:)]) {
            [playbackDelegate serverManagerAlertPlaybackEvent:self event:event];
        }
    }
}

- (void)controlHelperRefreshPlaybackTime:(ControlHelper *) helper starttime:(Np_DateTime *)newStartTime endtime:(Np_DateTime *)newEndTime {
    if (playbackDelegate != nil) {
        if ([playbackDelegate respondsToSelector:@selector(serverManagerRefreshPlaybackTime:startTime:endTime:)]) {
            [playbackDelegate serverManagerRefreshPlaybackTime:self startTime:newStartTime endTime:newEndTime];
        }
    }
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)controlHelperRefreshPlaybackTime:(ControlHelper *) helper recordfile:(EDualRecordFile)rec startTime:(Np_DateTime *)newStartTime endTime:(Np_DateTime *)newEndTime {
    if (playbackDelegate != nil) {
        if ([playbackDelegate respondsToSelector:@selector(serverManagerRefreshPlaybackTime:recordfile:startTime:endTime:)]) {
            [playbackDelegate serverManagerRefreshPlaybackTime:self recordfile:rec startTime:newStartTime endTime:newEndTime];
        }
    }
}

- (void)controlHelperSupportDualRecordPlayback:(ControlHelper *) helper camIndex:(int)index {
    if (playbackDelegate != nil) {
        if ([playbackDelegate respondsToSelector:@selector(serverManagerSupportDualRecordPlayback:)]) {
            [playbackDelegate serverManagerSupportDualRecordPlayback:self];
        }
    }
    
    if (eventDelegate != nil) {
        if ([eventDelegate respondsToSelector:@selector(serverManagerEventSupportDualRecordPlayback:)]) {
            [eventDelegate serverManagerEventSupportDualRecordPlayback:self];
        }
    }
}

- (void)controlHelperChangePlaybackRecordFile:(ControlHelper *) helper camIndex:(int) index recordFile:(EDualRecordFile) recordfile {
    if (eventDelegate != nil) {
        if ([eventDelegate respondsToSelector:@selector(serverManagerEventChangePlaybackRecordFile:recordFile:)]) {
            [eventDelegate serverManagerEventChangePlaybackRecordFile:self recordFile:recordfile];
        }
    }
}
#endif
- (void) controlHelperSave:(ControlHelper *) helper {
    if (siteDelegate != nil) {
        if ([siteDelegate respondsToSelector:@selector(serverManagerSaveInfo:)]) {
            [siteDelegate serverManagerSaveInfo:self];
        }
    }
}

- (void)controlHelperDidRefreshAudio:(ControlHelper *) helper camIndex:(int) index WithAudio:(AudioData) audiodata {
    if ((NSNull *)audioplayer != [NSNull null]) {
        [audioplayer updateAudioDataToQueue:audiodata];
    }
}

- (void)controlHelperStartEventViewByIndex:(ControlHelper *) helper index:(int) index {
    // Update the number of cameras
    m_ControlHelperLoginNum++;
    m_CameraTotalNum += helper.m_vecCheckedDevice.size();
    [m_CameraCountArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:helper.m_vecCheckedDevice.size()]];
    
    if (eventDelegate != nil) {
        if ([eventDelegate respondsToSelector:@selector(serverManagerStartEventViewByIndex:camIndex:)]) {
            [eventDelegate serverManagerStartEventViewByIndex:self camIndex:index];
        }
    }
}

- (void)controlHelperDidTalkReserved:(ControlHelper *) helper srv:(NSString *)servername usr:(NSString *)username {
    if (talkEnable) {
        [self disableTalkFunc];
    }
    
    if (displayDelegate != nil) {
        if ([displayDelegate respondsToSelector:@selector(serverManagerDidTalkReserved:srv:usr:)]) {
            [displayDelegate serverManagerDidTalkReserved:self srv:servername usr:username];
        }
    }
}

- (void)controlHelperDidTalkSessionError:(ControlHelper *) helper {
    if (talkEnable) {
        [self disableTalkFunc];
    }
    
    if (displayDelegate != nil) {
        if ([displayDelegate respondsToSelector:@selector(serverManagerDidTalkSessionError:)]) {
            [displayDelegate serverManagerDidTalkSessionError:self];
        }
    }
}

- (void)controlHelperShowP2PControlDialog:(ControlHelper *) helper msg:(NSString *)dialogMsg {
    if (displayDelegate != nil) {
        if ([displayDelegate respondsToSelector:@selector(serverManagerShowP2PControlDialog:msg:)]) {
            [displayDelegate serverManagerShowP2PControlDialog:self msg:dialogMsg];
        }
    }
    
    if (eventDelegate != nil) {
        if ([eventDelegate respondsToSelector:@selector(serverManagerEventShowP2PControlDialog:msg:)]) {
            [eventDelegate serverManagerEventShowP2PControlDialog:self msg:dialogMsg];
        }
    }
}

#pragma mark -
#pragma mark delegate methods

- (void)setPresetDelegateByChIndex:(int)index delegate:(id)theDelegate {
    int srv_index = [self getIndexOfServerByChannel:index];
    [[m_ControlHelperArray objectAtIndex:srv_index] setPresetDelegate:theDelegate];
}

- (void)setLoadingDelegate:(id)theDelegate {
    // Avoid controlhelper to control UI in MyView mode
    if (isMyViewMode)
        return;
    
    for (int i = 0; i < m_ControlHelperArray.count; i++)
        [[m_ControlHelperArray objectAtIndex:i] setLoadingDelegate:theDelegate];
}

#pragma mark -
#pragma mark Calculate Index of server or camera
- (int)getIndexOfChannelByServer:(ControlHelper *)helper index:(int)index {
    if (isMyViewMode) {
        for (MyViewServerInfo *sinfo in _myViewBackupInfo.serverList) {
            if ([sinfo.serverID isEqualToString:helper.serverInfo.serverID] &&
                [sinfo.userName isEqualToString:[helper getUserName]])
            {
                if (helper.serverInfo.serverType == eServer_Crystal) {
                    Np_SubDevice_Ext device = helper.m_vecExtCheckedDevice.at(index);
                    for (MyViewCameraInfo *cinfo in sinfo.cameraList) {
                        if ([cinfo.camCentralID isEqualToString:[NSString stringWithFormat:@"%llu",device.ID.centralID]] &&
                            [cinfo.camLocalID isEqualToString:[NSString stringWithFormat:@"%llu",device.ID.localID]])
                            return cinfo.cameraOrder;
                    }
                }
                else {
                    Np_Device device = helper.m_vecCheckedDevice.at(index);
                    for (MyViewCameraInfo *cinfo in sinfo.cameraList) {
                        if ([cinfo.camCentralID isEqualToString:[NSString stringWithFormat:@"%d",device.SensorDevices[0].ID.centralID]] &&
                            [cinfo.camLocalID isEqualToString:[NSString stringWithFormat:@"%d",device.SensorDevices[0].ID.localID]])
                            return cinfo.cameraOrder;
                    }
                }
            }
        }
    }
    else {
        int ch_index = 0;
        int server_index = [m_ControlHelperArray indexOfObject:helper];
        for (int i = 0; i < server_index; i++)
            ch_index += [[m_CameraCountArray objectAtIndex:i] intValue];
        ch_index += index;
        return ch_index;
    }
    return -1;
}

- (int)getIndexOfServerByChannel:(int)index {
    if (isMyViewMode) {
        for (MyViewServerInfo *sinfo in _myViewBackupInfo.serverList) {
            for (MyViewCameraInfo *cinfo in sinfo.cameraList) {
                if (cinfo.cameraOrder == index) {
                    for (int i=0; i< m_ControlHelperArray.count;i++) {
                        ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:i];
                        if ([sinfo.serverID isEqualToString:ctrlhlpr.serverInfo.serverID] &&
                            [sinfo.userName isEqualToString:[ctrlhlpr getUserName]])
                        {
                            return i;
                        }
                    }
                }
            }
        }
    }
    else {
        int ch_num = 0;
        for (int i = 0; i<[m_CameraCountArray count]; i++) {
            ch_num += [[m_CameraCountArray objectAtIndex:i] intValue];
            if (ch_num > index)
                return i;
        }
    }
    return -1;
}

- (int)getIndexOfCameraByChannel:(int)index {
    if (isMyViewMode) {
        for (MyViewServerInfo *sinfo in _myViewBackupInfo.serverList) {
            for (MyViewCameraInfo *cinfo in sinfo.cameraList) {
                if (cinfo.cameraOrder == index) {
                    for (int i=0; i< m_ControlHelperArray.count;i++) {
                        ControlHelper *ctrlhlpr = [m_ControlHelperArray objectAtIndex:i];
                        if (ctrlhlpr.serverInfo.serverType == eServer_Crystal) {
                            if ([sinfo.serverID isEqualToString:ctrlhlpr.serverInfo.serverID] &&
                                [sinfo.userName isEqualToString:[ctrlhlpr getUserName]])
                            {
                                for (int j=0; j<ctrlhlpr.m_vecExtCheckedDevice.size(); j++) {
                                    Np_SubDevice_Ext device = ctrlhlpr.m_vecExtCheckedDevice.at(j);
                                    if ([cinfo.camCentralID isEqualToString:[NSString stringWithFormat:@"%llu",device.ID.centralID]] &&
                                        [cinfo.camLocalID isEqualToString:[NSString stringWithFormat:@"%llu",device.ID.localID]])
                                        return j;
                                }
                            }
                        }
                        else {
                            if ([sinfo.serverID isEqualToString:ctrlhlpr.serverInfo.serverID] &&
                                [sinfo.userName isEqualToString:[ctrlhlpr getUserName]])
                            {
                                for (int j=0; j<ctrlhlpr.m_vecCheckedDevice.size(); j++) {
                                    Np_Device device = ctrlhlpr.m_vecCheckedDevice.at(j);
                                    if ([cinfo.camCentralID isEqualToString:[NSString stringWithFormat:@"%d",device.SensorDevices[0].ID.centralID]] &&
                                        [cinfo.camLocalID isEqualToString:[NSString stringWithFormat:@"%d",device.SensorDevices[0].ID.localID]])
                                        return j;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else {
        int ch_index = index;
        for (int i = 0; i<[m_CameraCountArray count]; i++) {
            int ch_num = [[m_CameraCountArray objectAtIndex:i] intValue];
            if (ch_num > ch_index)
                return ch_index;
            else
                ch_index -= ch_num;
        }
    }
    return -1;
}

- (int)getIndexOfServerByDoIndex:(int)index {
    int do_num = 0;
    for (int i = 0; i<[m_DigitalOutputCountArray count]; i++) {
        do_num += [[m_DigitalOutputCountArray objectAtIndex:i] intValue];
        if (do_num > index)
            return i;
    }
    return -1;
}

- (int)getIndexOfDoDeviceByIndex:(int)index {
    int do_index = index;
    for (int i = 0; i<[m_DigitalOutputCountArray count]; i++) {
        int do_num = [[m_DigitalOutputCountArray objectAtIndex:i] intValue];
        if (do_num > do_index)
            return do_index;
        else
            do_index -= do_num;
    }
    return -1;
}

- (int)getIndexOfServerByDiIndex:(int)index {
    int di_num = 0;
    for (int i = 0; i<[m_DigitalInputCountArray count]; i++) {
        di_num += [[m_DigitalInputCountArray objectAtIndex:i] intValue];
        if (di_num > index)
            return i;
    }
    return -1;
}

- (int)getIndexOfDiDeviceByIndex:(int)index {
    int di_index = index;
    for (int i = 0; i<[m_DigitalInputCountArray count]; i++) {
        int di_num = [[m_DigitalInputCountArray objectAtIndex:i] intValue];
        if (di_num > di_index)
            return di_index;
        else
            di_index -= di_num;
    }
    return -1;
}

#if SHOW_FRAMERATE_FLAG
- (double)getLiveViewFrameRate:(int) index {
    int srv_index = [self getIndexOfServerByChannel:index];
    int ca_index = [self getIndexOfCameraByChannel:index];
    if (srv_index < 0 || ca_index < 0)
        return NO;
    
    return [[m_ControlHelperArray objectAtIndex:srv_index] getLiveViewFrameRate:ca_index];
}
#endif

@end
