//
//  DOTableViewController.h
//  LiveView
//
//  Created by johnlinvc on 10/05/06.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "SlideView.h"

@interface DITableViewController : UITableViewController {
	NSMutableArray * statArray;
	NSMutableArray * lableArray;
	id parent;
    ServerManager *serverMgr;
    NSTimer * readDITimer;
    NSThread* diThread;
    
    //SDK
    NSMutableArray * m_vecDIDeviceName;
    std::vector<BOOL> m_vecDIStatus;
    
    SingleCamViewController *singlecam;
}

@property (nonatomic, retain) ServerManager *serverMgr;
@property (nonatomic, assign) id parent;
@property (readonly) NSTimer * readDITimer;

- (void)dismissPopover;
- (void)filpToDO;
- (id)initWithDIDevice:(NSMutableArray *) pDeviceName;
- (void)updateUI;
- (void)readDI;
@end
