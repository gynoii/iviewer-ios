//
//  PlaybackMenuViewController.h
//  iViewer
//
//  Created by Toby Huang on 12/6/4.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"

typedef enum E_PLAYBACK_FUNCTION_TYPE {
	ePlaybackFunctionNone = -1,
	ePlaybackFunctionAudio,
	ePlaybackFunctionSnapshot,
    ePlaybackFunctionMax
} ePlaybackFunctionType; 

@interface PlaybackMenuViewController : UITableViewController {
    int chIndex;
    id singleViewDelegate;
    ServerManager *serverMgr;
    UISwitch *audioswitch;
}

@property (nonatomic, assign) int chIndex;
@property (nonatomic, assign) id singleViewDelegate;
@property (nonatomic, assign) ServerManager *serverMgr;
@property (nonatomic, retain) UISwitch *audioswitch;

-(void) audioSwitchChanged;

@end

@protocol PlaybackMenuViewControllerDelegate
@optional
- (void)PbMenuViewControllerAudioChange;
- (void)PbMenuViewControllerSnapshot;
@end

