//
//  PinchSlideImageView.m
//  pinchSlide
//
//  Created by johnlinvc on 10/03/03.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PinchSlideImageView.h"
#define MIN_ZOOM 24
#define MIN_ANGLE 10
#define MIN_SLIDE 20

enum {
	readyMode,
	slideMode,
	pinchMode,
	dontCareMode
};

@implementation PinchSlideImageView
@synthesize delegate = mDelegate;

- (id)init{
	if ((self=[super init])) {
		mode = readyMode;
		isUsed = NO;
		[self setUserInteractionEnabled:YES];
		[self setMultipleTouchEnabled:YES];
	}
	return self;
}

- (id)initWithImage:(UIImage *) image {
	if ((self = [super initWithImage:image])) {
		mode = readyMode;
		isUsed = NO;
		[self setUserInteractionEnabled:YES];
		[self setMultipleTouchEnabled:YES];
	}
	return self;
}

- (CGFloat)distanceBetweenTwoPoints:(CGPoint) fromPoint toPoint:(CGPoint) toPoint {
	
	float x = toPoint.x - fromPoint.x;
    float y = toPoint.y - fromPoint.y;
    
    return sqrt(x * x + y * y);
}

- (void)touchesBegan:(NSSet *) touches withEvent:(UIEvent *) event {
#if DEBUG_LOG
	NSLog(@"began touches :\n%@\n event :\n%@",[touches description],[event description]);
#endif
	int touchNumber = [[event touchesForView:self] count];
	switch (touchNumber) {
		case 1:
            //move change mode to slideMode to touchesMoved
            //enter slide mode
			if (mode == readyMode) {
			//	mode = slideMode;
			}
            
			//save initial position
			UITouch * touch = [touches anyObject];
			initPosition = [touch locationInView:self];
            if ([mDelegate respondsToSelector:@selector(onPSImageViewDidBeginTouch:)]) {
                [mDelegate onPSImageViewDidBeginTouch:self];
            }
			break;
		case 2:
			//enter pinch Mode
			if (mode == slideMode || mode==readyMode) {
				mode = pinchMode;
			}
			//save initial distance
			UITouch * touch1 = [[[event touchesForView:self] allObjects] objectAtIndex:0];
			UITouch * touch2 = [[[event touchesForView:self] allObjects] objectAtIndex:1];
            CGPoint point1 = [touch1 locationInView:self];
            CGPoint point2 = [touch2 locationInView:self];
            initPosition = CGPointMake((point1.x + point2.x) / 2,
                                         (point1.y + point2.y) / 2);
            
			initDistance = [self distanceBetweenTwoPoints:[touch1 locationInView:self]
												  toPoint:[touch2 locationInView:self]];
            if ([mDelegate respondsToSelector:@selector(onPSImageViewDidBeginTouch:)]) {
                [mDelegate onPSImageViewDidBeginTouch:self];
            }
			break;
		default:
			//enter no response mode
			mode = dontCareMode;
			break;
	}
}
- (void)touchesMoved:(NSSet *) touches withEvent:(UIEvent *) event {
	if (mode == readyMode) {
        mode = slideMode;
    }
	switch (mode) {
		case readyMode:
			//mode = readyMode;
            //enter slide mode
			break;
		case slideMode:
			if ([[event touchesForView:self] count] == 1) {
				UITouch * touch = [[event touchesForView:self] anyObject];
				CGPoint currPosition = [touch locationInView:self];
				float deltaX = currPosition.x - initPosition.x;
				float deltaY = currPosition.y - initPosition.y;
				
				BOOL bDirX = (fabsf(deltaX) >= fabsf(deltaY)) && !isUsed;
				BOOL bDirY = (fabsf(deltaY) >= fabsf(deltaX)) && !isUsed;
				
				double angle = fabsf((atan((deltaY / deltaX)) * 180) / M_PI);
				
				float delta = fabsf(deltaX) + fabsf(deltaY);
                
				if (angle > MIN_ANGLE &&
					angle < 90 - MIN_ANGLE &&
					delta > MIN_SLIDE &&
					!isUsed) {
					
					if ([mDelegate respondsToSelector:@selector(onPSImageViewDidEndTouch:)]) {
						[mDelegate onPSImageViewDidEndTouch:self];
					}
					
					if (deltaX > 0 && deltaY > 0) {
						if ([mDelegate respondsToSelector:@selector(onPSImageView:SlideOnDirection:offset:)]) {
							[mDelegate onPSImageView:self SlideOnDirection:eSlideUpLeft offset:CGPointMake(deltaX, deltaY)];
						}
#if DEBUG_LOG
						NSLog(@"eSlideUpLeft x %f y %f", deltaX, deltaY);
#endif
					}
					else if (deltaX > 0 && deltaY < 0) {
						if ([mDelegate respondsToSelector:@selector(onPSImageView:SlideOnDirection:offset:)]) {
							[mDelegate onPSImageView:self SlideOnDirection:eSlideDownLeft offset:CGPointMake(deltaX, deltaY)];
						}
#if DEBUG_LOG
						NSLog(@"eSlideDownLeft x %f y %f", deltaX, deltaY);
#endif
					}
					else if (deltaX < 0 && deltaY > 0) {
						if ([mDelegate respondsToSelector:@selector(onPSImageView:SlideOnDirection:offset:)]) {
							[mDelegate onPSImageView:self SlideOnDirection:eSlideUpRight offset:CGPointMake(deltaX, deltaY)];
						}
#if DEBUG_LOG
						NSLog(@"eSlideUpRight x %f y %f", deltaX, deltaY);
#endif
					}
					else if (deltaX < 0 && deltaY < 0) {
						if ([mDelegate respondsToSelector:@selector(onPSImageView:SlideOnDirection:offset:)]) {
							[mDelegate onPSImageView:self SlideOnDirection:eSlideDownRight offset:CGPointMake(deltaX, deltaY)];
						}
#if DEBUG_LOG
						NSLog(@"eSlideDownRight x %f y %f", deltaX, deltaY);
#endif
					}
					//isUsed = YES;
					initPosition = currPosition;
				}
				else if (bDirX && 
						 delta > MIN_SLIDE) {
					
					if ([mDelegate respondsToSelector:@selector(onPSImageViewDidEndTouch:)]) {
						[mDelegate onPSImageViewDidEndTouch:self];
					}
					
					if (deltaX > 0) {
						if ([mDelegate respondsToSelector:@selector(onPSImageView:SlideOnDirection:offset:)]) {
							[mDelegate onPSImageView:self SlideOnDirection:eSlidePanLeft offset:CGPointMake(deltaX, 0.0)];
						}
#if DEBUG_LOG
						NSLog(@"eSlidePanLeft x %f y %f", deltaX, deltaY);
#endif
                    }
					else {
						if ([mDelegate respondsToSelector:@selector(onPSImageView:SlideOnDirection:offset:)]) {
							[mDelegate onPSImageView:self SlideOnDirection:eSlidePanRight offset:CGPointMake(deltaX, 0.0)];
						}
#if DEBUG_LOG
						NSLog(@"eSlidePanRight x %f y %f", deltaX, deltaY);
#endif
					}
					//isUsed = YES;
					initPosition = currPosition;
				}
				else if (bDirY &&
						 delta > MIN_SLIDE) {
					if ([mDelegate respondsToSelector:@selector(onPSImageViewDidEndTouch:)]) {
						[mDelegate onPSImageViewDidEndTouch:self];
					}
					
					if (deltaY > 0) {
						if ([mDelegate respondsToSelector:@selector(onPSImageView:SlideOnDirection:offset:)]) {
							[mDelegate onPSImageView:self SlideOnDirection:eSlideTiltUp offset:CGPointMake(0.0, deltaY)];
						}
#if DEBUG_LOG
						NSLog(@"eSlideTiltUp x %f y %f", deltaX, deltaY);
#endif
					}
					else {
						if ([mDelegate respondsToSelector:@selector(onPSImageView:SlideOnDirection:offset:)]) {
							[mDelegate onPSImageView:self SlideOnDirection:eSlideTiltDown offset:CGPointMake(0.0, deltaY)];
						}
#if DEBUG_LOG
						NSLog(@"eSlideTiltDown  x %f y %f", deltaX, deltaY);
#endif
					}
					//isUsed = YES;
					initPosition = currPosition;
				}
			}
			break;
		case pinchMode:
			if ([[event touchesForView:self] count] == 2) {
				//calc,send zoom
				UITouch * touch1 = [[[event touchesForView:self] allObjects] objectAtIndex:0];
				UITouch * touch2 = [[[event touchesForView:self] allObjects] objectAtIndex:1];
				CGFloat currDistance = [self distanceBetweenTwoPoints:[touch1 locationInView:self] 
															  toPoint:[touch2 locationInView:self]];
				float delta = fabsf(currDistance - initDistance);
				if (delta > MIN_ZOOM && !isUsed) {
					if (currDistance > initDistance) {
#if DEBUG_LOG
                        NSLog(@"zoom out");
#endif
                        if ([mDelegate respondsToSelector:@selector(onPSImageView:Zooming:Center:)]) {
							[mDelegate onPSImageView:self Zooming:NO Center:initPosition];
							initDistance = currDistance;
						}
                        //isUsed = YES;
					}
					else {
#if DEBUG_LOG
						NSLog(@"zoom in");
#endif
                        if ([mDelegate respondsToSelector:@selector(onPSImageView:Zooming:Center:)]) {
							[mDelegate onPSImageView:self Zooming:YES Center:initPosition];
							initDistance = currDistance;
						}
                        //isUsed = YES;
                    }
				}
				else if (delta < 8) {
#if DEBUG_LOG
					NSLog(@"pinched with delta:%f",delta);
#endif
                    if ([mDelegate respondsToSelector:@selector(onPSImageViewDidEndTouch:)]) {
						[mDelegate onPSImageViewDidEndTouch:self];
					}
				}
			}
			break;
		case dontCareMode:
			if ([[event touchesForView:self] count] == 1) {
				//mode = readyMode;
			}
			break;
		default:
			break;
	}
	
}

- (void)touchesEnded:(NSSet *) touches withEvent:(UIEvent *) event {
	/*
	 remain 1 finger:
	 slide:to ready,calc dir
	 pinch:to ready
	 dontc:to ready
	 
	 remain 2 finger:
	 slide:impossoble
	 pinch:to pinch,calc distance,send zoom
	 */
	switch (mode) {
		case readyMode:
            // jerrylu, 2012/11/21, fix bug9800, add double tap
            if ([[event touchesForView:self] count] == 1) {
                UITouch * touchCnt = [touches anyObject];
                if ([touchCnt tapCount] > 1) {
                    if ([mDelegate respondsToSelector:@selector(onPSImageViewDoubleTap:)]) {
                        [mDelegate onPSImageViewDoubleTap:self];
                    }
#if DEBUG_LOG
                    NSLog(@"double click");
#endif
                }
                else if ([mDelegate respondsToSelector:@selector(onPSImageViewSingleTap:)]) {
                    [mDelegate onPSImageViewSingleTap:self];
                }
            }
            
			mode = readyMode;
			isUsed = NO;
            
            if ([mDelegate respondsToSelector:@selector(onPSImageViewDidEndTouch:)]) {
                [mDelegate onPSImageViewDidEndTouch:self];
            }
			break;
		case slideMode:
			if ([[event touchesForView:self] count] == 1) {
				UITouch * touchCnt = [touches anyObject];
				if ([touchCnt tapCount] > 1) {
					if ([mDelegate respondsToSelector:@selector(onPSImageViewDoubleTap:)]) {
						[mDelegate onPSImageViewDoubleTap:self];
					}
#if DEBUG_LOG
					NSLog(@"double click");
#endif
					return;
				}
                
				mode = readyMode;
				isUsed = NO;
				if ([mDelegate respondsToSelector:@selector(onPSImageViewDidEndTouch:)]) {
					[mDelegate onPSImageViewDidEndTouch:self];
				}
#if DEBUG_LOG
				NSLog(@"Slided Finished single");
#endif
			}
			break;
		case pinchMode:
			if ([[event touchesForView:self] count] == 2) {
				isUsed = NO;
				if ([mDelegate respondsToSelector:@selector(onPSImageViewDidEndTouch:)]) {
					[mDelegate onPSImageViewDidEndTouch:self];
				}
#if DEBUG_LOG
				NSLog(@"Pinch Finished");
#endif
			}
			mode = readyMode;
			break;
		case dontCareMode:
			if ([[event touchesForView:self] count] == 1) {
				mode = readyMode;
				isUsed = NO;
			}
			break;

		default:
			break;
	}
	
}
- (void)touchesCancelled:(NSSet *) touches withEvent:(UIEvent *) event {
	mode = readyMode;
	isUsed = NO;
}

@end
