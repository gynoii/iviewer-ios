#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <GLKit/GLKit.h>
#import "MyGLView.h"
#import "Utility.h"
#import "LiveViewAppDelegate.h"

//////////////////////////////////////////////////////////

#pragma mark - shaders

#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)
#define SHADER_STRING(text) @ STRINGIZE2(text)

#define MIN_ANGLE 10
#define MIN_SLIDE 20

NSString *const VSaaS_vertexShaderString = SHADER_STRING
(
 attribute vec4 position;
 attribute vec2 texcoord;
 attribute vec4 position_shift;
 uniform mat4 modelViewProjectionMatrix;
 varying vec2 v_texcoord;
 
 void main()
 {
     gl_Position = modelViewProjectionMatrix * position;
     
     // Change the gl_Position can change the texture location
     // gl_Position.x += 0.1;
//     gl_Position += position_shift;
//     gl_Position.x += position_shift.x;
//     gl_Position.y += position_shift.y;
     
     v_texcoord = texcoord.xy;
 }
 );

NSString *const VSaaS_yuvFragmentShaderString = SHADER_STRING
(
 varying highp vec2 v_texcoord;
 uniform sampler2D s_texture_y;
 uniform sampler2D s_texture_u;
 uniform sampler2D s_texture_v;
 
 void main()
 {
     highp float y = texture2D(s_texture_y, v_texcoord).r;
     highp float u = texture2D(s_texture_u, v_texcoord).r - 0.5;
     highp float v = texture2D(s_texture_v, v_texcoord).r - 0.5;
     
     highp float r = y +             1.402 * v;
     highp float g = y - 0.344 * u - 0.714 * v;
     highp float b = y + 1.772 * u;
     
     gl_FragColor = vec4(r,g,b,1.0);
 }
 );

static BOOL validateProgram(GLuint prog)
{
	GLint status;
	
    glValidateProgram(prog);
    
#ifdef DEBUG
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        //NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == GL_FALSE) {
		//NSLog(@"Failed to validate program %d", prog);
        return NO;
    }
	
	return YES;
}

static GLuint compileShader(GLenum type, NSString *shaderString)
{
	GLint status;
	const GLchar *sources = (GLchar *)shaderString.UTF8String;
	
    GLuint shader = glCreateShader(type);
    if (shader == 0 || shader == GL_INVALID_ENUM) {
        //NSLog(@"Failed to create shader %d", type);
        return 0;
    }
    
    glShaderSource(shader, 1, &sources, NULL);
    glCompileShader(shader);
	
#ifdef DEBUG
	GLint logLength;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(shader, logLength, &logLength, log);
        //NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        glDeleteShader(shader);
		//NSLog(@"Failed to compile shader:\n");
        return 0;
    }
    
	return shader;
}


static void mat4f_LoadOrtho(float left, float right, float bottom, float top, float near, float far, float* mout)
{
	float r_l = right - left;
	float t_b = top - bottom;
	float f_n = far - near;
	float tx = - (right + left) / (right - left);
	float ty = - (top + bottom) / (top - bottom);
	float tz = - (far + near) / (far - near);
    
	mout[0] = 2.0f / r_l;
	mout[1] = 0.0f;
	mout[2] = 0.0f;
	mout[3] = 0.0f;
	
	mout[4] = 0.0f;
	mout[5] = 2.0f / t_b;
	mout[6] = 0.0f;
	mout[7] = 0.0f;
	
	mout[8] = 0.0f;
	mout[9] = 0.0f;
	mout[10] = -2.0f / f_n;
	mout[11] = 0.0f;
	
	mout[12] = tx;
	mout[13] = ty;
	mout[14] = tz;
	mout[15] = 1.0f;
}

//////////////////////////////////////////////////////////

#pragma mark - frame renderers

@protocol MyGLRenderer
- (BOOL) isValid;
- (NSString *) fragmentShader;
- (void) resolveUniforms: (GLuint) program;
- (void) setFrame: (MyVideoFrame *) frame;
- (void) setFrame: (MyVideoFrame *) pFrame width:(int)w height:(int)h;
- (BOOL) prepareRender;
@end

@interface MyGLRenderer_YUV : NSObject<MyGLRenderer> {
    GLint _uniformSamplers[3];
    GLuint _textures[3];
}
@end

@implementation MyGLRenderer_YUV

- (BOOL) isValid
{
    return (_textures[0] != 0);
}

- (NSString *) fragmentShader
{
    return VSaaS_yuvFragmentShaderString;
}

- (void) resolveUniforms: (GLuint) program
{
    _uniformSamplers[0] = glGetUniformLocation(program, "s_texture_y");
    _uniformSamplers[1] = glGetUniformLocation(program, "s_texture_u");
    _uniformSamplers[2] = glGetUniformLocation(program, "s_texture_v");
}


- (void) setFrame: (MyVideoFrame *) yuvFrame width:(int)w height:(int)h
{
    //NSLog(@"---yuvFrame %d %d %d", yuvFrame.luma.length, yuvFrame.chromaB.length, yuvFrame.chromaR.length);
    
    const NSUInteger frameWidth = w;
    const NSUInteger frameHeight = h;
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    if (0 == _textures[0])
        glGenTextures(3, _textures);

    const UInt8 *pixels[3] = { (UInt8 *)yuvFrame.luma.bytes, (UInt8 *)yuvFrame.chromaB.bytes, (UInt8 *)yuvFrame.chromaR.bytes };
    const NSUInteger widths[3]  = { frameWidth, frameWidth / 2, frameWidth / 2 };
    const NSUInteger heights[3] = { frameHeight, frameHeight / 2, frameHeight / 2 };
    
    for (int i = 0; i < 3; ++i)
    {
        
        glBindTexture(GL_TEXTURE_2D, _textures[i]);
        
        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_LUMINANCE,
                     widths[i],
                     heights[i],
                     0,
                     GL_LUMINANCE,
                     GL_UNSIGNED_BYTE,
                     pixels[i]);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
}

- (void) setFrame: (MyVideoFrame *) yuvFrame
{
    //NSLog(@"---yuvFrame %d %d %d", yuvFrame.luma.length, yuvFrame.chromaB.length, yuvFrame.chromaR.length);

    const NSUInteger frameWidth = yuvFrame.width;
    const NSUInteger frameHeight = yuvFrame.height;
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    if (0 == _textures[0])
        glGenTextures(3, _textures);
    
    const UInt8 *pixels[3] = { (UInt8 *)yuvFrame.luma.bytes, (UInt8 *)yuvFrame.chromaB.bytes, (UInt8 *)yuvFrame.chromaR.bytes };
    const NSUInteger widths[3]  = { frameWidth, frameWidth / 2, frameWidth / 2 };
    const NSUInteger heights[3] = { frameHeight, frameHeight / 2, frameHeight / 2 };
    
    for (int i = 0; i < 3; ++i) {
        
        glBindTexture(GL_TEXTURE_2D, _textures[i]);
        
        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_LUMINANCE,
                     widths[i],
                     heights[i],
                     0,
                     GL_LUMINANCE,
                     GL_UNSIGNED_BYTE,
                     pixels[i]);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
}

- (BOOL) prepareRender
{
    if (_textures[0] == 0)
        return NO;
    
    for (int i = 0; i < 3; ++i) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, _textures[i]);
        glUniform1i(_uniformSamplers[i], i);
    }
    
    return YES;
}


- (void) dealloc
{
    //NSLog(@">>>>>>>>>>>>>   %@ dealloc", self);
    
    if (_textures[0])
        glDeleteTextures(3, _textures);
    
    [super dealloc];
}

@end

//////////////////////////////////////////////////////////

#pragma mark - MyVideoFrameBuf
@implementation MyVideoFrameBuf
@synthesize width, height;
-(void)dealloc {
    [_data release];
    [super dealloc];
}
@end

#pragma mark - MyVideoFrame
@implementation MyVideoFrame
@synthesize width, height;
@end

//////////////////////////////////////////////////////////

#pragma mark - MyGLView

enum {
	ATTRIBUTE_VERTEX,
   	ATTRIBUTE_TEXCOORD,
    ATTRIBUTE_POS_SHIFT,
};

@implementation MyGLView {
    EAGLContext     *_context;
    GLuint          _framebuffer;
    GLuint          _renderbuffer;
    GLint           _backingWidth;
    GLint           _backingHeight;
    GLuint          _program;
    GLint           _uniformMatrix;
    GLfloat         _vertices[8];

    id<MyGLRenderer> _renderer;
    
    UIImageView *disconnectImageView;
    
    int iWidth;
    int iHeight;
    
    GLuint pTmpTextures[3];
    GLint  pTmpUniformSamplers[3];
    
    // For Scale and Roate for single screen
    GLfloat ScaleFactor;
    GLfloat FinalFixedScaleFator;
    GLfloat SwipeFactor_X;
    GLfloat SwipeFactor_Y;
    
    GLfloat LeftBoundaryAfterScale;
    GLfloat RightBoundaryAfterScale;
    GLfloat UpBoundaryAfterScale;
    GLfloat DownBoundaryAfterScale;
    
    GLfloat SwipeOffset;
}

@synthesize gDelegate;

+ (Class) layerClass
{
	return [CAEAGLLayer class];
}

// frameWidth and frameHeight is used to caculate the scale factor
- (id) initWithFrame:(CGRect)frame frameWidth:(float) w frameHeight:(float) h
{
    self = [super initWithFrame:frame];
    if (self) {
        iWidth = w;
        iHeight = h;
        
        _renderer = [[MyGLRenderer_YUV alloc] init];
        //NSLog(@"MyGLRenderer_YUV alloc ok");
        
        self.drawableMultisample = GLKViewDrawableMultisample4X;
        
        CAEAGLLayer *eaglLayer = (CAEAGLLayer*) self.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking,
                                        kEAGLColorFormatRGBA8,           kEAGLDrawablePropertyColorFormat,
                                        nil];
        
        _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        
        if (!_context ||
            ![EAGLContext setCurrentContext:_context])
        {
            //NSLog(@"failed to setup EAGLContext");
            self = nil;
            return nil;
        }
        
        // If we set scale, the fps will down
        //[self.layer setContentsScale:2.0];
        
        glGenFramebuffers(1, &_framebuffer);
        glGenRenderbuffers(1, &_renderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, _renderbuffer);
        [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _renderbuffer);
        
        
        // Disable unused fucntion to speed up
        glDisable(GL_DITHER);
        glDisable(GL_BLEND);
        glDisable(GL_STENCIL_TEST);
        //glDisable(GL_TEXTURE_2D);
        glDisable(GL_DEPTH_TEST);
        
        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            self = nil;
            return nil;
        }
        
        GLenum glError = glGetError();
        if (GL_NO_ERROR != glError) {
            self = nil;
            return nil;
        }
        
        if (![self loadShaders]) {
            self = nil;
            return nil;
        }
        
        _vertices[0] = -1.0f;  // x0
        _vertices[1] = -1.0f;  // y0
        _vertices[2] =  1.0f;  // ..
        _vertices[3] = -1.0f;
        _vertices[4] = -1.0f;
        _vertices[5] =  1.0f;
        _vertices[6] =  1.0f;  // x3
        _vertices[7] =  1.0f;  // y3
    }
    
    // support gesture recognization
    ScaleFactor = 1.0;
    FinalFixedScaleFator = 1.0;
    SwipeFactor_X = 0.0;
    SwipeFactor_Y = 0.0;
    SwipeOffset = 10.0;
    
    [self setUserInteractionEnabled:YES];
    
    gSingleTapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapHandler:)];
    gSingleTapper.numberOfTapsRequired = 1;
    gDoubleTapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapHandler:)];
    gDoubleTapper.numberOfTapsRequired = 2;
    gLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandler:)];
    gPincher = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchHandler:)];
    gPanSwiper = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHandler:)];
    isLockZoom = NO;
    
    UIImage * imgDefault = [UIImage imageNamed:@"novideo320x240_noword.png"];
    disconnectImageView = [[UIImageView alloc] initWithImage:imgDefault];
    [disconnectImageView setFrame:self.bounds];
    
    [self setBackgroundColor:COLOR_BACKGROUND];
    
    return self;
}

- (void)dealloc
{
    MyGLRenderer_YUV *render = (MyGLRenderer_YUV *)_renderer;
    [render release];
    _renderer = nil;
    
    if (_framebuffer) {
        glDeleteFramebuffers(1, &_framebuffer);
        _framebuffer = 0;
    }
    
    if (_renderbuffer) {
        glDeleteRenderbuffers(1, &_renderbuffer);
        _renderbuffer = 0;
    }
    
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
    
	if ([EAGLContext currentContext] == _context) {
		[EAGLContext setCurrentContext:nil];
	}
    _context = nil;
    
    if (0 == pTmpTextures[0])
        glDeleteTextures(3, pTmpTextures);
    
    [gSingleTapper release];
    [gDoubleTapper release];
    [gLongPress release];
    [gPincher release];
    [gPanSwiper release];
    
    if (disconnectImageView.superview) {
        [disconnectImageView removeFromSuperview];
    }
    
    if ((NSNull *)disconnectImageView != [NSNull null]) {
        [disconnectImageView release];
    }
    
    [super dealloc];
}

- (void)layoutSubviews
{
    glBindRenderbuffer(GL_RENDERBUFFER, _renderbuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_backingWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_backingHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _renderbuffer);
    
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"failed to make complete framebuffer object %x", status);
	} else {
        //NSLog(@"OK setup GL framebuffer %d:%d", _backingWidth, _backingHeight);
    }
    
    [self updateVertices];
    if (_renderer.isValid)
        [self render: nil];
}

- (void)setContentMode:(UIViewContentMode)contentMode
{
    [super setContentMode:contentMode];
    [self updateVertices];
    if (_renderer.isValid)
        [self render:nil];
}

- (BOOL)loadShaders
{
    BOOL result = NO;
    GLuint vertShader = 0, fragShader = 0;
    
	_program = glCreateProgram();
	
    vertShader = compileShader(GL_VERTEX_SHADER, VSaaS_vertexShaderString);
	if (!vertShader)
        goto exit;
    
	fragShader = compileShader(GL_FRAGMENT_SHADER, _renderer.fragmentShader);
    if (!fragShader)
        goto exit;
    
	glAttachShader(_program, vertShader);
	glAttachShader(_program, fragShader);
	glBindAttribLocation(_program, ATTRIBUTE_VERTEX, "position");
    glBindAttribLocation(_program, ATTRIBUTE_TEXCOORD, "texcoord");
    glBindAttribLocation(_program, ATTRIBUTE_POS_SHIFT, "position_shift");
    
	
	glLinkProgram(_program);
    
    GLint status;
    glGetProgramiv(_program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
		//NSLog(@"Failed to link program %d", _program);
        goto exit;
    }
    
    result = validateProgram(_program);
    
    // 20131106 test
    glUseProgram(_program);
    pTmpUniformSamplers[0] = glGetUniformLocation(_program, "s_texture_y");
    pTmpUniformSamplers[1] = glGetUniformLocation(_program, "s_texture_u");
    pTmpUniformSamplers[2] = glGetUniformLocation(_program, "s_texture_v");
    
    _uniformMatrix = glGetUniformLocation(_program, "modelViewProjectionMatrix");
    //GLKMatrix4Multiply(_uniformMatrix, GLKMatrix4MakeScale(0.5, 0.5, 1));
    //[_renderer resolveUniforms:_program];
	
exit:
    if (vertShader)
        glDeleteShader(vertShader);
    if (fragShader)
        glDeleteShader(fragShader);
    
    if (result) {
        //NSLog(@"OK setup GL programm");
    } else {
        glDeleteProgram(_program);
        _program = 0;
    }
    
    return result;
}

- (void)updateVertices
{
    const BOOL fit      = (self.contentMode == UIViewContentModeScaleAspectFit);
    const float width   = iWidth;
    const float height  = iHeight;
    
    const float dH      = (float)_backingHeight / height;
    const float dW      = (float)_backingWidth	  / width;
    
    float w, h;
    if (fit) {
        const float dd = fit ? MIN(dH, dW) : MAX(dH, dW);
        h = (height * dd / (float)_backingHeight);
        w = (width  * dd / (float)_backingWidth);
    }
    else {
        h = (height * dH / (float)_backingHeight);
        w = (width  * dW / (float)_backingWidth);
    }
#if DEBUG_LOG
    NSLog(@"updateVertices fit=%d, (%f,%f) (%d,%d)", fit, width, height, _backingWidth, _backingHeight);
    NSLog(@"updateVertices w,h=(%f,%f) dw,dh=(%f,%f) dd=%f",w, h, dW, dH, dd);
#endif
    _vertices[0] = - w;
    _vertices[1] = - h;
    _vertices[2] =   w;
    _vertices[3] = - h;
    _vertices[4] = - w;
    _vertices[5] =   h;
    _vertices[6] =   w;
    _vertices[7] =   h;
}

- (void)render: (MyVideoFrame *) frame
{
    static const GLfloat texCoords[] = {
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
    };
	
    [EAGLContext setCurrentContext:_context];
    
    glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
    glViewport(-1, -1, _backingWidth, _backingHeight);
    //glViewport(0, 0, _backingHeight, _backingWidth);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(_program);
    
    if (frame) {
        [_renderer setFrame:frame width:iWidth height:iHeight];
        //[_renderer setFrame:frame width:_backingWidth height:_backingHeight at:0];
    }
    
    if ([_renderer prepareRender]) {
        GLfloat modelviewProj[16];
        mat4f_LoadOrtho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f, modelviewProj);
        glUniformMatrix4fv(_uniformMatrix, 1, GL_FALSE, modelviewProj);
        
        glVertexAttribPointer(ATTRIBUTE_VERTEX, 2, GL_FLOAT, 0, 0, _vertices);
        glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
        glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, 0, 0, texCoords);
        glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);
        
#if 0
        if (!validateProgram(_program))
        {
            //NSLog(@"Failed to validate program");
            return;
        }
#endif
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, _renderbuffer);
    [_context presentRenderbuffer:GL_RENDERBUFFER];
    /*glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_backingWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_backingHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _renderbuffer);*/
}


- (void)clearFrameBuffer
{
    //[EAGLContext setCurrentContext:_context];
    
    glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
    //glViewport(0, 0, _backingWidth, _backingHeight);

    UIColor *bgcolor = COLOR_BACKGROUND;
    CGFloat red, green, blue, alpha;
    [bgcolor getRed:&red green:&green blue:&blue alpha:&alpha];
    glClearColor(red, green, blue, alpha);
    //glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    glClear(GL_COLOR_BUFFER_BIT); // GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT
}

- (void)setDefaultPic
{
    if (disconnectImageView.superview != nil) {
        return;
    }
    if (self.contentMode == UIViewContentModeScaleAspectFit)
    {
        disconnectImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    [self addSubview:disconnectImageView];
}

- (void)resetDefaultPicFrame
{
    [disconnectImageView setFrame:self.bounds];
}

- (void)setMyVideoFrame:(MyVideoFrame *) frame
{
    static const GLfloat texCoords[] = {
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
    };
	
    [EAGLContext setCurrentContext:_context];
    
    glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
    glViewport(0, 0, _backingWidth, _backingHeight);
    
//    NSLog(@" w,h = (%d,%d) (%d,%.d)",_backingWidth, _backingHeight, iWidth, iHeight);
//    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//    glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(_program);
    
    if (frame) {
        [_renderer setFrame:frame width:iWidth height:iHeight];
        //[_renderer setFrame:frame width:_backingWidth height:_backingHeight at:0];
    }
    
    if ([_renderer prepareRender]) {
        
        GLfloat modelviewProj[16];
        mat4f_LoadOrtho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f, modelviewProj);
        glUniformMatrix4fv(_uniformMatrix, 1, GL_FALSE, modelviewProj);
        
        glVertexAttribPointer(ATTRIBUTE_VERTEX, 2, GL_FLOAT, 0, 0, _vertices);
        glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
        glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, 0, 0, texCoords);
        glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
}

- (void)setFrameBuf:(uint8_t *) frame width:(int) vWidth height:(int) vHeight
{
    if(vWidth != iWidth || vHeight != iHeight) {
        iWidth = vWidth;
        iHeight = vHeight;
        [self updateVertices];
        if (_renderer.isValid)
            [self render:nil];
    }
    
    static GLfloat texCoords[] = {
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
    };
	
    if (!frame)
        return;
    
    [EAGLContext setCurrentContext:_context];
    
    //glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
    glViewport(0, 0, _backingWidth, _backingHeight);
    
    // Try to minimize below time....
    //NSTimeInterval vTmpTime= [NSDate timeIntervalSinceReferenceDate];
    {
        const NSUInteger frameWidth = vWidth;
        const NSUInteger frameHeight = vHeight;
        
        NSUInteger widths[3]  = { frameWidth, frameWidth / 2, frameWidth / 2 };
        NSUInteger heights[3] = { frameHeight, frameHeight / 2, frameHeight / 2 };
        
        // If the source data is not 4 byte alignment, we should set GL_UNPACK_ALIGNMENT
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        //glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        //glPixelStorei(GL_UNPACK_ALIGNMENT, 2); //better
        
        // reference http://www.zwqxin.com/archives/opengl/opengl-api-memorandum-2.html
        
        if (0 == pTmpTextures[0])
            glGenTextures(3, pTmpTextures);
        
        // In iPAD2, below need 4~5 ms
        {
            int i = 0;
            UInt8 *pSrc = frame;
            
            glBindTexture(GL_TEXTURE_2D, pTmpTextures[i]);
            
            glTexImage2D(GL_TEXTURE_2D,
                         0,
                         GL_LUMINANCE,
                         widths[i],
                         heights[i],
                         0,
                         GL_LUMINANCE,
                         GL_UNSIGNED_BYTE,
                         pSrc);
            
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            i = 1;
            glBindTexture(GL_TEXTURE_2D, pTmpTextures[i]);
            
            glTexImage2D(GL_TEXTURE_2D,
                         0,
                         GL_LUMINANCE,
                         widths[i],
                         heights[i],
                         0,
                         GL_LUMINANCE,
                         GL_UNSIGNED_BYTE,
                         pSrc+frameWidth*frameHeight);
            
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            i = 2;
            glBindTexture(GL_TEXTURE_2D, pTmpTextures[i]);
            
            glTexImage2D(GL_TEXTURE_2D,
                         0,
                         GL_LUMINANCE,
                         widths[i],
                         heights[i],
                         0,
                         GL_LUMINANCE,
                         GL_UNSIGNED_BYTE,
                         pSrc+frameWidth*frameHeight * 5 / 4);
            
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }
    }
    
    for (int i = 0; i < 3; ++i) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, pTmpTextures[i]);
        glUniform1i(pTmpUniformSamplers[i], i);
    }
    
    {
        //對於放大縮小以及移動，要試著統一只改變 modelviewProj
        GLKMatrix4 modelviewProjectionMatrix =
        {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f,
        };
        
        // 此時矩陣運算要直接使用 GLKMatrix4Scale(), GLKMatrix4Translate()
        modelviewProjectionMatrix = GLKMatrix4Scale(modelviewProjectionMatrix, ScaleFactor, ScaleFactor,1.0f);
        modelviewProjectionMatrix = GLKMatrix4Translate(
                                                    modelviewProjectionMatrix,
                                                    SwipeFactor_X,
                                                    SwipeFactor_Y,
                                                    0.0f);
        
        glUniformMatrix4fv(_uniformMatrix, 1, GL_FALSE, modelviewProjectionMatrix.m);//(CGFloat *)(&modelviewProjectionMatrix));
        
        glVertexAttribPointer(ATTRIBUTE_VERTEX, 2, GL_FLOAT, 0, 0, _vertices);
        glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
        
        glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, 0, 0, texCoords);
        glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
}

-(void)RenderToHardware:(NSTimer *)timer {
    if (disconnectImageView.superview != nil)
        [disconnectImageView removeFromSuperview];
    
    glBindRenderbuffer(GL_RENDERBUFFER, _renderbuffer);
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

- (UIImage *)getImageOnView {
    UIImage *img = NULL;
    
    if (disconnectImageView.superview)
    {
        img = disconnectImageView.image;
    }
    else {
        NSInteger myDataLength = iWidth * iHeight * 4;
        
        // allocate array and read pixels into it.
        GLubyte *buffer = (GLubyte *) malloc(myDataLength);
        glReadPixels(0, 0, iWidth, iHeight, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
        
        
        // gl renders "upside down" so swap top to bottom into new array.
        // there's gotta be a better way, but this works.
        GLubyte *buffer2 = (GLubyte *) malloc(myDataLength);
        for(int y = 0; y < iHeight; y++)
        {
            for(int x = 0; x < iWidth * 4; x++)
            {
                buffer2[((iHeight - 1) - y) * iWidth * 4 + x] = buffer[y * 4 * iWidth + x];
            }
        }
        
        
        // make data provider with data.
        CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer2, myDataLength, NULL);
        
        // prep the ingredients
        int bitsPerComponent = 8;
        int bitsPerPixel = 32;
        int bytesPerRow = 4 * iWidth;
        CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
        CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
        CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
        
        // make the cgimage
        CGImageRef imageRef = CGImageCreate(iWidth, iHeight, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
        
        // then make the uiimage from that
        img = [UIImage imageWithCGImage:imageRef];
    }
    return img;
}

static NSData * copyFrameData(UInt8 *src, int linesize, int width, int height)
{
    width = MIN(linesize, width);
    NSMutableData *md = [NSMutableData dataWithLength: width * height];
    Byte *dst = (Byte *)md.mutableBytes;
    
    for (NSUInteger i = 0; i < height; ++i) {
        memcpy(dst, src, width);
        dst += width;
        src += linesize;
    }

    return md;
}

+ (MyVideoFrame *) CopyFullFrameToVideoFrame: (uint8_t *) pFrameIn withWidth :(int) vWidth withHeight:(int) vHeight
{
    MyVideoFrame *yuvFrame = [[MyVideoFrame alloc] init];
    
//    NSTimeInterval vTmpTime= [NSDate timeIntervalSinceReferenceDate];
    //yuvFrame.luma.bytes = pFrameIn->data[0];
    yuvFrame.luma = copyFrameData(pFrameIn,
                                  vWidth,
                                  vWidth,
                                  vHeight);
    
    yuvFrame.chromaB = copyFrameData(pFrameIn + vWidth*vHeight,
                                     vWidth / 2,
                                     vWidth / 2,
                                     vHeight / 2);
    
    yuvFrame.chromaR = copyFrameData(pFrameIn + (vWidth*vHeight * 5 / 4),
                                     vWidth / 2,
                                     vWidth / 2,
                                     vHeight / 2);
    
//    vTmpTime = [NSDate timeIntervalSinceReferenceDate]-vTmpTime;
//    NSLog(@"vTmpTime=%f",vTmpTime); // For ipad2, above copy time cost 5ms
    
    yuvFrame.width = vWidth;
    yuvFrame.height = vHeight;
    
    
    return [yuvFrame autorelease];
}

static NSData * copyFrameBufData(UInt8 *src, int width, int height)
{
    int len = (width * height * 3) / 2;
    
    NSMutableData *md = [NSMutableData dataWithLength:len];
    Byte *dst = (Byte *)md.mutableBytes;
    memcpy(dst, src, len);
    
    return md;
}

+ (MyVideoFrameBuf *)CopyFullFrameToVideoFrameBuf: (uint8_t *) pFrameIn withWidth:(int) vWidth withHeight:(int) vHeight
{
    MyVideoFrameBuf *frameBuf = [[MyVideoFrameBuf alloc] init];
    
    frameBuf.data = copyFrameBufData(pFrameIn, vWidth, vHeight);
    frameBuf.width = vWidth;
    frameBuf.height = vHeight;
    
    return [frameBuf autorelease];
}

#pragma mark - Enable/Disable Gesture

- (void)enableSingleTap
{
    [self addGestureRecognizer:gSingleTapper];
}

- (void)disableSingleTap
{
    [self removeGestureRecognizer:gSingleTapper];
}

- (void)enableDoubleTap
{
    [self addGestureRecognizer:gDoubleTapper];
}

- (void)disableDoubleTap
{
    [self removeGestureRecognizer:gDoubleTapper];
}

- (void)enableLongPress
{
    [self addGestureRecognizer:gLongPress];
}

- (void)disableLongPress
{
    [self removeGestureRecognizer:gLongPress];
}

- (void)enablePinch
{
    [self addGestureRecognizer:gPincher];
}

- (void)disablePinch
{
    [self removeGestureRecognizer:gPincher];
}

- (void)enablePanSwipe
{
    [self addGestureRecognizer:gPanSwiper];
}

- (void)disablePanSwipe
{
    [self removeGestureRecognizer:gPanSwiper];
}

- (void)lockZoom {
    isLockZoom = YES;
}

- (void)unlockZoom {
    isLockZoom = NO;
}

- (void)resetScaleFactors {
    FinalFixedScaleFator = 1.0f;
    ScaleFactor = 1.0f;
    
    SwipeFactor_X = 0.0f;
    SwipeFactor_Y = 0.0f;
    
    LeftBoundaryAfterScale = 0.0f;
    RightBoundaryAfterScale = 0.0f;
    UpBoundaryAfterScale = 0.0f;
    DownBoundaryAfterScale = 0.0f;
}

#pragma mark - Gesture Handler

- (void)singleTapHandler:(UITapGestureRecognizer *)sender
{
#if DEBUG_LOG
    NSLog(@"SINGLE TAP!");
#endif
    if ([gDelegate respondsToSelector:@selector(myGLViewDelegateSingleTap:)])
        [gDelegate myGLViewDelegateSingleTap:self];
}

- (void)doubleTapHandler:(UITapGestureRecognizer *)sender
{
#if DEBUG_LOG
    NSLog(@"Double TAP!");
#endif
    if (!isLockZoom)
    {
        [self resetScaleFactors];
        [self removeGestureRecognizer:gPanSwiper];
    }
    
    if ([gDelegate respondsToSelector:@selector(myGLViewDelegateDoubleTap:)])
        [gDelegate myGLViewDelegateDoubleTap:self];
}

- (void) pinchHandler:(UIPinchGestureRecognizer *)sender
{
    // if sender.scale > 1.0, zoom in
    // if sender.scale < 1.0, zoom out
    float FinalScaleFator = 1.0f;
    FinalScaleFator = FinalFixedScaleFator * sender.scale;
    
    if (!isLockZoom)
    {
        if((FinalScaleFator>1) && (FinalScaleFator<6))
        {
            ScaleFactor = FinalScaleFator;
            
            if([(UIPinchGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded)
            {
                LeftBoundaryAfterScale = (1.0/ScaleFactor);
                RightBoundaryAfterScale = 1-(1.0/ScaleFactor);
                UpBoundaryAfterScale = LeftBoundaryAfterScale;
                DownBoundaryAfterScale = RightBoundaryAfterScale;
                
                FinalFixedScaleFator = ScaleFactor;
                //NSLog(@"FixScale=%0.2f, x,y=(%0.2f,%0.2f), boundary(x,y)=(%0.2f,%0.2f)",FinalFixedScaleFator, SwipeFactor_X, SwipeFactor_Y, LeftBoundaryAfterScale,UpBoundaryAfterScale);
                
                // The boundary may change when scale, we should correct the value
                if(SwipeFactor_X > RightBoundaryAfterScale)
                    SwipeFactor_X = RightBoundaryAfterScale;
                else if(SwipeFactor_X < -1*LeftBoundaryAfterScale)
                    SwipeFactor_X = -1*LeftBoundaryAfterScale;
                
                if(SwipeFactor_Y > UpBoundaryAfterScale)
                    SwipeFactor_Y = UpBoundaryAfterScale;
                else if(SwipeFactor_Y < -1*DownBoundaryAfterScale)
                    SwipeFactor_Y = -1*DownBoundaryAfterScale;
            }
        }
        
        if (FinalScaleFator <= 1.0)
        {
            [self resetScaleFactors];
        }

        //NSLog(@"PICH %0.2f! %0.2f, Left=%0.2f, Right=%0.2f", sender.scale, ScaleFactor,LeftBoundaryAfterScale,RightBoundaryAfterScale);
    }
    
    if (sender.state == UIGestureRecognizerStateChanged)
    {
        if ([gDelegate respondsToSelector:@selector(myGLViewDelegatePinchEnd:)])
            [gDelegate myGLViewDelegatePinchEnd:self];
        
        if (sender.scale > 1.0)
        {
            if ([gDelegate respondsToSelector:@selector(myGLViewDelegatePinch:zooming:zoomScale:)])
                [gDelegate myGLViewDelegatePinch:self zooming:YES zoomScale:FinalScaleFator];
        }
        else
        {
            if ([gDelegate respondsToSelector:@selector(myGLViewDelegatePinch:zooming:zoomScale:)])
                [gDelegate myGLViewDelegatePinch:self zooming:NO zoomScale:FinalScaleFator];
        }
    }
    else if (sender.state == UIGestureRecognizerStateEnded)
    {
        if ([gDelegate respondsToSelector:@selector(myGLViewDelegatePinchEnd:)])
            [gDelegate myGLViewDelegatePinchEnd:self];
    }
}


-(void) panHandler:(UIPanGestureRecognizer *)sender
{
    static CGFloat startX, startY;

    if (!isLockZoom)
    {
        if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged)
        {
            CGPoint tp = [(UIPanGestureRecognizer*)sender locationInView:self.superview];
            CGFloat deltaX = tp.x - startX;
            CGFloat deltaY = - tp.y + startY;
            startX = tp.x; startY = tp.y;
            
            if (fabsf(deltaX) > fabsf(deltaY))
            {
                if (deltaX > 0)
                {
                    if ((int)SwipeFactor_X <= (int)RightBoundaryAfterScale)
                        SwipeFactor_X += 0.01;
                }
                else
                {
                    if ((int)SwipeFactor_X >= (int)(-1*LeftBoundaryAfterScale))
                        SwipeFactor_X -= 0.01;
                }
            }
            else
            {
                if (deltaY > 0)
                {
                    if ((int)SwipeFactor_Y <= (int)UpBoundaryAfterScale)
                        SwipeFactor_Y += 0.01;
                }
                else
                {
                    if ((int)SwipeFactor_Y >= (int)(-1*DownBoundaryAfterScale))
                        SwipeFactor_Y -= 0.01;
                }
            }
            
            //NSLog(@"start(x,y)=%f,%f, del(x,y)=%f,%f",startX,startY,deltaX,deltaY);
            
            if ([gDelegate respondsToSelector:@selector(myGLViewDelegatePanSwipeEnd:)])
                [gDelegate myGLViewDelegatePanSwipeEnd:self];
        }
        
        if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan)
        {
            CGPoint tp = [(UIPanGestureRecognizer*)sender locationInView:self.superview];
            startX = tp.x;
            startY = tp.y;
        }
    }
    else
    {
        if (   [sender state] != UIGestureRecognizerStateBegan
            && [sender state] != UIGestureRecognizerStateChanged)
        {
            if ([gDelegate respondsToSelector:@selector(myGLViewDelegatePanSwipeEnd:)])
                [gDelegate myGLViewDelegatePanSwipeEnd:self];
            return;
        }

        if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged)
        {
            ePanDirection dir = ePanNone;
            
            CGPoint tp = [(UIPanGestureRecognizer*)sender locationInView:self.superview];
            CGFloat deltaX = tp.x - startX;
            CGFloat deltaY = - tp.y + startY;
            
            BOOL bDirX = (fabsf(deltaX) >= fabsf(deltaY));
            BOOL bDirY = (fabsf(deltaY) >= fabsf(deltaX));
            
            double angle = fabsf((atan((deltaY / deltaX)) * 180) / M_PI);
            
            float delta = fabsf(deltaX) + fabsf(deltaY);
            
            if (   angle > MIN_ANGLE
                && angle < 90 - MIN_ANGLE
                && delta > MIN_SLIDE)
            {
                if (deltaX > 0 && deltaY > 0)
                {
                    dir = ePanDownLeft;
#if DEBUG_LOG
                    NSLog(@"ePanDownLeft x %f y %f", deltaX, deltaY);
#endif
                }
                else if (deltaX > 0 && deltaY < 0)
                {
                    dir = ePanUpLeft;
#if DEBUG_LOG
                    NSLog(@"ePanUpLeft x %f y %f", deltaX, deltaY);
#endif
                }
                else if (deltaX < 0 && deltaY > 0)
                {
                    dir = ePanDownRight;
#if DEBUG_LOG
                    NSLog(@"ePanDownRight x %f y %f", deltaX, deltaY);
#endif
                }
                else if (deltaX < 0 && deltaY < 0)
                {
                    dir = ePanUpRight;
#if DEBUG_LOG
                    NSLog(@"ePanUpRight x %f y %f", deltaX, deltaY);
#endif
                }
            }
            else if (   bDirX
                     && delta > MIN_SLIDE)
            {
                if (deltaX > 0)
                {
                    dir = ePanLeft;
#if DEBUG_LOG
                    NSLog(@"ePanLeft x %f y %f", deltaX, deltaY);
#endif
                }
                else
                {
                    dir = ePanRight;
#if DEBUG_LOG
                    NSLog(@"ePanRight x %f y %f", deltaX, deltaY);
#endif
                }
            }
            else if (   bDirY
                     && delta > MIN_SLIDE)
            {
                if (deltaY < 0)
                {
                    dir = ePanUp;
#if DEBUG_LOG
                    NSLog(@"ePanUp x %f y %f", deltaX, deltaY);
#endif
                }
                else
                {
                    dir = ePanDown;
#if DEBUG_LOG
                    NSLog(@"ePanDown  x %f y %f", deltaX, deltaY);
#endif
                }
            }
            
            if (dir != ePanNone)
            {
                startX = tp.x;
                startY = tp.y;

                if ([gDelegate respondsToSelector:@selector(myGLViewDelegatePanSwipe:onDirection:)])
                    [gDelegate myGLViewDelegatePanSwipe:self onDirection:dir];
            }
        }
        
        if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan)
        {
            CGPoint tp = [(UIPanGestureRecognizer*)sender locationInView:self.superview];
            startX = tp.x;
            startY = tp.y;
        }
    }
}

- (void) rotateHandler:(id)sender
{
    NSLog(@"ROTATE!");
}

- (void) longPressHandler:(UILongPressGestureRecognizer *)sender
{
#if DEBUG_LOG
    NSLog(@"LONG PRESS!");
#endif
    if ([gDelegate respondsToSelector:@selector(myGLViewDelegateLongPress:)])
        [gDelegate myGLViewDelegateLongPress:self];
}

- (void) swipeHandler:(UISwipeGestureRecognizer *)sender
{
    if(sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        NSLog(@"SWIPE Right");
    }
    if(sender.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        NSLog(@"SWIPE Left");
    }
    if(sender.direction == UISwipeGestureRecognizerDirectionUp)
    {
        NSLog(@"SWIPE Up");
    }
    if(sender.direction == UISwipeGestureRecognizerDirectionDown)
    {
        NSLog(@"SWIPE Down");
    }
}


@end

