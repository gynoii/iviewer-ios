//
//  MyViewModifyController.h
//  iViewer
//
//  Created by NUUO on 13/8/19.
//
//

#import <UIKit/UIKit.h>
#import "SiteInfoController.h"
#import "myviewInfo.h"

@interface MyViewModifyController : UITableViewController<UIAlertViewDelegate> {
    SiteListController * _favoriteSite;
    MyViewInfo * newViewInfo;
    UIBarButtonItem * saveButton;
    UIBarButtonItem * cleanAllButton;
    UIBarButtonItem * cancelButton;
    
    NSMutableArray * serverInfoArray;
    ServerManager * serverMgr;
    NSMutableArray * ctrlHlprArray;
    NSMutableArray * isShowRecordingServerArray;
    
    UIActivityIndicatorView *spinner;
    UIView *overlayView;
    int select_SrvIndex;
}

@property (nonatomic, assign) SiteListController * favoriteSite;

- (id)initWithStyle:(UITableViewStyle)style ServerMgr:(ServerManager *)srvMgr;

@end
