//
//  LiveViewAppDelegate.m
//  LiveView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import "LiveViewAppDelegate.h"
#import "SiteListController.h"
#import "ControlHelper.h"
#import "LogoPage.h"
#import "UIDevice+IdentifierAddition.h"
#import "EventAlertView.h"
#import "eventinfo.h"
#include "Utility.h"
#import "LiveViewEventViewController.h"
#import "LiveViewEventListViewController.h"
#import "SingleCamViewController.h"

@implementation LiveViewAppDelegate

@synthesize window;
@synthesize navigationController;
@synthesize strUUID;
@synthesize favoriteSiteController =_favoriteSiteController;
@synthesize bDelayMsg;
#if FUNC_PUSH_NOTIFICATION_SUPPORT
@synthesize strToken;
#endif

#pragma mark -
#pragma mark Application lifecycle

- (void)applicationDidFinishLaunching:(UIApplication *) application {
    signal(SIGPIPE, SIG_IGN); // jerrylu, 2012/09/12, fix bug8661
    
    [[NSUserDefaults standardUserDefaults] setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:@"App_Version"];
    
    // Override point for customization after app launch
	//application.networkActivityIndicatorVisible = YES;
	[application setStatusBarStyle:STYLE_STATUSBAR];
    
#if FUNC_LOGO_PAGE // jerrylu, 2012/12/06
    logoview = [[[LogoPage alloc] initWithNibName:@"LogoPage" bundle:nil] autorelease];
    // jerrylu, 2012/11/26, support ios 6.0, and it avoid to use addSubview function
    window.rootViewController = logoview;
    //[window addSubview:[logoview view]];
    
    [NSTimer scheduledTimerWithTimeInterval:2
                                     target:self
                                   selector:@selector(showServerListView)
                                   userInfo:nil
                                    repeats:NO];
#else
	self.window.rootViewController = navigationController;
    //[window addSubview:[navigationController view]];
#endif
    UIViewController *pFirstView = [[navigationController viewControllers] objectAtIndex:0];
    [self setFavoriteSiteController:(FavoriteSiteController *)pFirstView];
    [window makeKeyAndVisible];
    
    [navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    // avoid to enter sleep mode
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

#if FUNC_PUSH_NOTIFICATION_SUPPORT
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
#if DEBUG_LOG
	NSLog(@"didFinishLaunchingWithOptions");
#endif
    
    signal(SIGPIPE, SIG_IGN); // jerrylu, 2012/09/12, fix bug8661
    
    [[NSUserDefaults standardUserDefaults] setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:@"App_Version"];
    
    // Override point for customization after app launch
	//application.networkActivityIndicatorVisible = YES;
	[application setStatusBarStyle:STYLE_STATUSBAR];
    
    UIViewController *pFirstView = [[navigationController viewControllers] objectAtIndex:0];
    [self setFavoriteSiteController:(FavoriteSiteController *)pFirstView];
    [window makeKeyAndVisible];
    
    [self loadEventList];
    
    // avoid to enter sleep mode
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    // jerrylu, 2013/02/18, add sound
    // Let the device know we want to receive push notifications
    [self registerPushNotification];
    
    NSURL *urlToParse = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    if (urlToParse) {
        self.window.rootViewController = navigationController;
        [self handleReceiveURL:urlToParse];
    }
    else {
#if FUNC_LOGO_PAGE // jerrylu, 2012/12/06
        logoview = [[[LogoPage alloc] initWithNibName:@"LogoPage" bundle:nil] autorelease];
        // jerrylu, 2012/11/26, support ios 6.0, and it avoid to use addSubview function
        window.rootViewController = logoview;
        
        [NSTimer scheduledTimerWithTimeInterval:2
                                         target:self
                                       selector:@selector(showServerListView)
                                       userInfo:nil
                                        repeats:NO];
#else
        self.window.rootViewController = navigationController;
#endif
        UILocalNotification *remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (remoteNotification) {
            NSDictionary *userInfo = [launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
            [self handleReceiveRemoteNotification:userInfo appState:[[UIApplication sharedApplication] applicationState]];
        }
    }
    
    [navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    /*
    NSLog(@"Calling Application Bundle ID: %@", sourceApplication);
    NSLog(@"URL scheme:%@", [url scheme]);
    NSLog(@"URL query: %@", [url query]);
    */
    [self handleReceiveURL:url];
    
    return YES;
}

- (void)registerPushNotification {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeNone|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationType)(UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeNone|UIRemoteNotificationTypeSound)];
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
#if DEBUG_LOG
	NSLog(@"My token is: %@", deviceToken);
#endif
    if ((NSNull *)strToken != [NSNull null])
        [strToken release];
    strToken = [[[[[deviceToken description]
                   stringByReplacingOccurrencesOfString: @"<" withString: @""] 
                  stringByReplacingOccurrencesOfString: @">" withString: @""]
                 stringByReplacingOccurrencesOfString: @" " withString: @""] retain];
    NSLog(@"Token:%@", strToken);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
#if DEBUG_LOG
    NSLog(@"didReceiveRemoteNotification");
#endif
    [self handleReceiveRemoteNotification:userInfo appState:[[UIApplication sharedApplication] applicationState]];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        [self handleReceiveRemoteNotification:userInfo appState:[[UIApplication sharedApplication] applicationState]];
    }
    else if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateInactive) {
        [self handleReceiveRemoteNotification:userInfo appState:[[UIApplication sharedApplication] applicationState]];
    }
    else {
        //NSLog(@"UIBackgroundFetchResultNewData");
        [self handleReceiveRemoteNotificationOnBackground:userInfo];
    }
    completionHandler(UIBackgroundFetchResultNewData);
}
#endif // FUNC_PUSH_NOTIFICATION_SUPPORT

- (void)applicationWillResignActive:(UIApplication *)application{
#if DEBUG_LOG
    NSLog(@"applicationWillResignActive");
#endif
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
#if DEBUG_LOG
    NSLog(@"applicationDidEnterBackground");
#endif
}

-(void) applicationWillEnterForeground:(UIApplication *)application{
#if DEBUG_LOG
    NSLog(@"applicationWillEnterForeground");
#endif
    [self popToRootView];
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
#if DEBUG_LOG
    NSLog(@"applicationDidBecomeActive");
#endif
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application{
#if DEBUG_LOG
    NSLog(@"applicationDidReceiveMemoryWarning");
#endif    
}

- (void)applicationWillTerminate:(UIApplication *) application {
	// Save data if appropriate
	[_favoriteSiteController save];
}
- (void)popToRootView {
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [navigationController setNavigationBarHidden:NO animated:NO];
    [navigationController setToolbarHidden:NO animated:NO];
    
    // jerrylu, 2012/08/28, fix bug8451
    [navigationController dismissViewControllerAnimated:NO completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
    [navigationController popToRootViewControllerAnimated:NO];
    UIViewController *pFirstView = [[navigationController viewControllers] objectAtIndex:0];
    [self setFavoriteSiteController:(FavoriteSiteController *)pFirstView];
}

- (void)showServerDownMessage:(int) eventID {
    while (1) {
        UIViewController *lastview = [[navigationController viewControllers] lastObject];
        if (lastview.view.window && [lastview isViewLoaded]) {
            [self performSelectorOnMainThread:@selector(popToRootView) withObject:nil waitUntilDone:YES];
            break;
        }
        sleep(0.5);
    }
    
	NSString *eventstr = nil;
    switch (eventID) {
        case Np_EVENT_CONNECTION_LOST:
        case Np_EVENT_SERVER_CONNECTION_LOST:
        case Np_EVENT_SERVER_OFFLINE:
            eventstr = [[NSString alloc] initWithFormat:NSLocalizedString(@"Server Disconnected", nil)];
            break;
        case Np_EVENT_PHYSICAL_DEVICE_TREELIST_UPDATED:
        case Np_EVENT_LOGICAL_DEVICE_TREELIST_UPDATED:
        case Np_EVENT_DEVICE_TREELIST_UPDATED:
        case Np_EVENT_CONFIG_UPDATE_setServerList:
        case Np_EVENT_CONFIG_UPDATE_removeServerList:
        case Np_EVENT_CONFIG_UPDATE_addUnitDevices:
        case Np_EVENT_CONFIG_UPDATE_modifyUnitDevices:
        case Np_EVENT_CONFIG_UPDATE_removeUnitDevices:
        case Np_EVENT_CONFIG_UPDATE_modifyDeviceBasicInfo:
        case Np_EVENT_CONFIG_UPDATE_setCameraAssociateList:
            eventstr = [[NSString alloc] initWithFormat:NSLocalizedString(@"Server Setting Updated", nil)];
            break;
        case Np_EVENT_PRIVILEGE_ACCESS_RIGHT_UPDATED:
        case Np_EVENT_PASSWORD_CHANGED:
        case Np_EVENT_ACCOUNT_DELETE:
            eventstr = [[NSString alloc] initWithFormat:NSLocalizedString(@"Account Modified", nil)];
            break;
        case Np_EVENT_SERVICE_NETWORK_SETTING_CHANGED:
            eventstr = [[NSString alloc] initWithFormat:NSLocalizedString(@"Network Setting Changed", nil)];
            break;
        default:
            break;
    }
    
    // jerrylu, 2012/06/14
    if (eventID != Np_EVENT_CODE_BASE) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:eventstr preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [navigationController presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView *eventAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:eventstr delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [eventAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
            [eventAlert release];
        }
    }
    [eventstr release];
}

// jerrylu, 2012/09/14
- (void)showQuotaTimeoutMessage:(BOOL) outofquota {
    [self popToRootView];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Out of Quota", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [navigationController presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *eventAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Out of Quota", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [eventAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO]; // jerrylu, 2012/12/27
        [eventAlert release];
    }
}

// jerrylu, 2012/07/18
- (void)showServerListView {
    // jerrylu, 2012/10/22, support ios 6.0, and it avoid to use addSubview function
    self.window.rootViewController = navigationController;
}

- (void)handleReceiveURL:(NSURL *)url {
    NSLog(@"handleReceiveURL");
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[url query] options:0];
    NSString *query = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *parameter_dic = [[[NSMutableDictionary alloc] init] autorelease];
    NSArray *queryElements = [query componentsSeparatedByString:@"&"];
    for (NSString *element in queryElements) {
        NSArray *keyVal = [element componentsSeparatedByString:@"="];
        if (keyVal.count > 0) {
            NSString *variableKey = [keyVal objectAtIndex:0];
            NSString *value = (keyVal.count == 2) ? [keyVal lastObject] : nil;
            if (value != nil)
                [parameter_dic setObject:value forKey:variableKey];
        }
    }
    
    if ([parameter_dic objectForKey:@"version"] == nil || [parameter_dic objectForKey:@"sid"] == nil) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Wrong code type, please check you scan the solo QR code.", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [navigationController presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                            message:NSLocalizedString(@"Wrong code type, please check you scan the solo QR code.", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    else {
        ServerInfo *sinfo = [[ServerInfo alloc] init];
        [sinfo setConnectionType:eP2PConnection];
        [sinfo setServerName_P2P:[parameter_dic objectForKey:@"sid"]];
        [sinfo setServerID_P2P:[parameter_dic objectForKey:@"sid"]];
        if ([parameter_dic objectForKey:@"username"] != nil) {
            [sinfo setUserName_P2P:[parameter_dic objectForKey:@"username"]];
            if ([parameter_dic objectForKey:@"pwd"] != nil) {
                [sinfo setUserPassword_P2P:[parameter_dic objectForKey:@"pwd"]];
            }
        }
        
        SiteListController *rootviewcontroller = (SiteListController *)self.favoriteSiteController;
        [rootviewcontroller openServerView:sinfo];
        [sinfo release];
    }
}

#if FUNC_PUSH_NOTIFICATION_SUPPORT
- (void)handleReceiveRemoteNotification:(NSDictionary *)userInfo appState:(UIApplicationState)state {
    NSString *lockey = nil;
    NSArray *args = nil;
    for (id key in userInfo) {
        NSString *keystr = [[NSString alloc] initWithFormat:@"%@", key];
        
        if ([keystr isEqualToString:@"aps"]) {
            NSDictionary *aps = [NSDictionary dictionaryWithDictionary:(NSDictionary *) [userInfo objectForKey:key] ];
            for (id key1 in aps){
                id alert = [aps objectForKey:key1];
                if ([alert isKindOfClass:[NSDictionary class]]) {
                    lockey = [alert objectForKey:@"loc-key"];
#if DEBUG_LOG
                    NSLog(@"loc-key: %@", lockey);
#endif
                    args = (NSArray *) [alert objectForKey:@"loc-args"] ;
#if DEBUG_LOG
                    for (id key2 in args){
                        NSLog(@"loc-args: %@, value: ", key2);
                    }
#endif
                }
                else if ([alert isKindOfClass:[NSArray class]]) {
#if DEBUG_LOG
                    for (id key2 in key1){
                        NSLog(@"key2: %@, value: %@", key2, [key1 objectForKey:key2]);
                    }
#endif
                }
                else if([key1 isKindOfClass:[NSString class]]) {
#if DEBUG_LOG
                    NSLog(@"key1: %@, value: %@", key1, lockey);
#endif
                }
            }
        }
        
        [keystr release];
    }
    
    // jerrylu, 2012/08/28, fix bug8391
    if (args == nil) {
        if (state == UIApplicationStateActive) {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:lockey preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* close = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Close", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alert addAction:close];
                [navigationController presentViewController:alert animated:YES completion:nil];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:lockey delegate:nil cancelButtonTitle:NSLocalizedString(@"Close", nil) otherButtonTitles:nil, nil];
                
                if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
                    NSArray *subviewArray = alert.subviews;
                    for(int x = 1; x < [subviewArray count]; x++){
                        if([[[subviewArray objectAtIndex:x] class] isSubclassOfClass:[UILabel class]]) {
                            UILabel *label = [subviewArray objectAtIndex:x];
                            label.textAlignment = NSTextAlignmentLeft;
                        }
                    }
                }
                
                [alert show];
                [alert release];
            }
        }
        [self pausePlaybackView];
        return;
    }
    
    NSString *content = NSLocalizedString(lockey, nil);
    if (content == lockey) {
        NSString *msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"Unkown event, please update your mobile client application.", nil)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"OK", nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
            [alert addAction:ok];
            [navigationController presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        [msg release];
        [self pausePlaybackView];
        return;
    }

    Eventinfo *einfo = [[Eventinfo alloc] init];
    einfo.eventType = lockey;
    einfo.username = [userInfo objectForKey:@"name"];
    einfo.serverID = [userInfo objectForKey:@"server_id"];
    einfo.cameraID = [userInfo objectForKey:@"camera_id"];
    char *argList = (char *)malloc(sizeof(NSString *) * [args count]);
    [args getObjects:(id *)argList];
    NSString *message = [[NSString alloc] initWithFormat:content arguments:argList];
    free(argList);
    einfo.eventMsg = message;

    // get Date by Event
    NSString *dateStr = nil;
    if ([lockey isEqualToString:@"CameraEvent3"] || [lockey isEqualToString:@"IOEvent1"])
    {
        dateStr = [[NSString alloc] initWithFormat:@"%@ %@", [args objectAtIndex:5], [args objectAtIndex:6]];
    }
    else if ([lockey hasPrefix:@"CameraEvent"] || [lockey hasPrefix:@"IvsEvent"])
    {
        dateStr = [[NSString alloc] initWithFormat:@"%@ %@", [args objectAtIndex:3], [args objectAtIndex:4]];
    }
    else if ([lockey hasPrefix:@"PosEvent"])
    {
        dateStr = [[NSString alloc] initWithFormat:@"%@ %@", [args objectAtIndex:2], [args objectAtIndex:3]];
    }
    if (dateStr != nil) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"HH:mm:ss yyyy/MM/dd"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        einfo.eventDate = date;
        [dateFormat release];
        [dateStr release];
    }
    
    if ([self isNeedWriteEventList:einfo]) {
        //[einfo setEventType:lockey];
        if ([self addEventList:einfo]) {
            [self saveEventList];
            
            UIViewController *lastview = (UIViewController *)[[navigationController viewControllers] lastObject];
            if ([lastview isKindOfClass:[EventViewController class]]) {
                EventViewController *eventview = (EventViewController *)lastview;
                [eventview performSelectorOnMainThread:@selector(refreshSecondToolBar) withObject:nil waitUntilDone:NO];
            }
            else if ([lastview isKindOfClass:[EventListViewController class]]) {
                EventListViewController *eventlistview = (EventListViewController *)lastview;
                [eventlistview eventListUpdate];
            }
            else if ([lastview isKindOfClass:[LiveViewEventListViewController class]]) {
                LiveViewEventListViewController *eventlistview = (LiveViewEventListViewController *)lastview;
                if ([einfo.serverID isEqualToString:eventlistview.filterServerID]) {
                    NSArray *userArray = [einfo.username componentsSeparatedByString:@","];
                    BOOL isEqualUser = NO;
                    for (int i=0; i < [userArray count]; i++) {
                        NSString *username = [userArray objectAtIndex:i];
                        if ([username isEqualToString:eventlistview.filterUserName]) {
                            [eventlistview eventListUpdate];
                            isEqualUser = YES;
                            break;
                        }
                    }
                    if (!isEqualUser)
                        [self showEventAlertView:einfo];
                }
                else
                    [self showEventAlertView:einfo];
            }
            else if ([lastview isKindOfClass:[LiveViewEventViewController class]]) {
                LiveViewEventViewController *eventview = (LiveViewEventViewController *)lastview;
                if (![einfo.serverID isEqualToString:[eventview.serverMgr getFilterPushNotificationServerID]])
                    [self showEventAlertView:einfo];
                else {
                    NSArray *eventuserArray = [einfo.username componentsSeparatedByString:@","];
                    BOOL isEqualUser = NO;
                    for (int i=0; i < [eventuserArray count]; i++) {
                        NSString *eusername = [eventuserArray objectAtIndex:i];
                        if ([eusername isEqualToString:[eventview.serverMgr getFilterPushNotificationUserName]]) {
                            isEqualUser = YES;
                            break;
                        }
                    }
                    if (!isEqualUser)
                        [self showEventAlertView:einfo];
                }
            }
            else {
                if (state == UIApplicationStateInactive || state == UIApplicationStateBackground) {
                    einfo.isNotRead = 0; // jerrylu, 2012/11/15
                    [self showEventView:einfo];
                }
                else {
                    [self showEventAlertView:einfo];
                }
            }
        }
        else {
            if (state == UIApplicationStateInactive || state == UIApplicationStateBackground) {
                [self showEventView:einfo];
            }
            NSLog(@"Duplicated Event");
        }
    }
    else {
        if (state == UIApplicationStateActive) {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* close = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Close", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alert addAction:close];
                [navigationController presentViewController:alert animated:YES completion:nil];
                
                UIUserNotificationSettings *remoteNoteSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
                if(remoteNoteSettings.types & UIUserNotificationTypeSound)
                    AudioServicesPlaySystemSound(1002);
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"Close", nil) otherButtonTitles:nil, nil];
                
                NSArray *subviewArray = alert.subviews;
                for(int x = 1; x < [subviewArray count]; x++){
                    if([[[subviewArray objectAtIndex:x] class] isSubclassOfClass:[UILabel class]]) {
                        UILabel *label = [subviewArray objectAtIndex:x];
                        label.textAlignment = NSTextAlignmentLeft;
                    }
                }
                
                [alert show];
                [alert release];
                
                UIRemoteNotificationType remoteNoteTypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
                if(remoteNoteTypes & UIRemoteNotificationTypeSound)
                    AudioServicesPlaySystemSound(1002);
            }
            
            [self pausePlaybackView];
        }
    }
    
    [message release];
    [einfo release];
}

- (void)handleReceiveRemoteNotificationOnBackground:(NSDictionary *)userInfo {
    NSString *lockey = nil;
    NSArray *args = nil;
    for (id key in userInfo) {
        NSString *keystr = [[NSString alloc] initWithFormat:@"%@", key];
        
        if ([keystr isEqualToString:@"aps"]) {
            NSDictionary *aps = [NSDictionary dictionaryWithDictionary:(NSDictionary *) [userInfo objectForKey:key] ];
            for (id key1 in aps){
                id alert = [aps objectForKey:key1];
                if ([alert isKindOfClass:[NSDictionary class]]) {
                    lockey = [alert objectForKey:@"loc-key"];
#if DEBUG_LOG
                    NSLog(@"loc-key: %@", lockey);
#endif
                    args = (NSArray *) [alert objectForKey:@"loc-args"] ;
#if DEBUG_LOG
                    for (id key2 in args){
                        NSLog(@"loc-args: %@, value: ", key2);
                    }
#endif
                }
                else if ([alert isKindOfClass:[NSArray class]]) {
#if DEBUG_LOG
                    for (id key2 in key1){
                        NSLog(@"key2: %@, value: %@", key2, [key1 objectForKey:key2]);
                    }
#endif
                }
                else if([key1 isKindOfClass:[NSString class]]) {
#if DEBUG_LOG
                    NSLog(@"key1: %@, value: %@", key1, lockey);
#endif
                }
            }
        }
        
        [keystr release];
    }
    
    if (args == nil) {
        return;
    }
    
    NSString *content = NSLocalizedString(lockey, nil);
    if (content == lockey) {
        return;
    }
    
    Eventinfo *einfo = [[Eventinfo alloc] init];
    einfo.eventType = lockey;
    einfo.username = [userInfo objectForKey:@"name"];
    einfo.serverID = [userInfo objectForKey:@"server_id"];
    einfo.cameraID = [userInfo objectForKey:@"camera_id"];
    char *argList = (char *)malloc(sizeof(NSString *) * [args count]);
    [args getObjects:(id *)argList];
    NSString *message = [[NSString alloc] initWithFormat:content arguments:argList];
    free(argList);
    einfo.eventMsg = message;
    
    // get Date by Event
    NSString *dateStr = nil;
    if ([lockey isEqualToString:@"CameraEvent3"] || [lockey isEqualToString:@"IOEvent1"])
    {
        dateStr = [[NSString alloc] initWithFormat:@"%@ %@", [args objectAtIndex:5], [args objectAtIndex:6]];
    }
    else if ([lockey hasPrefix:@"CameraEvent"] || [lockey hasPrefix:@"IvsEvent"])
    {
        dateStr = [[NSString alloc] initWithFormat:@"%@ %@", [args objectAtIndex:3], [args objectAtIndex:4]];
    }
    else if ([lockey hasPrefix:@"PosEvent"])
    {
        dateStr = [[NSString alloc] initWithFormat:@"%@ %@", [args objectAtIndex:2], [args objectAtIndex:3]];
    }
    if (dateStr != nil) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"HH:mm:ss yyyy/MM/dd"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        einfo.eventDate = date;
        [dateFormat release];
        [dateStr release];
    }
    
    if ([self isNeedWriteEventList:einfo]) {
        //[einfo setEventType:lockey];
        if ([self addEventList:einfo]) {
            [self saveEventList];
        }
        else {
            NSLog(@"Duplicated Event");
        }
    }
    
    [message release];
    [einfo release];
}

- (void)showEventAlertView:(Eventinfo *) einfo {
    EventAlertView *alert = [[EventAlertView alloc] init];
    alert.handledelegate = self;
    alert.eInfo = einfo;
        
    // jerrylu, 2012/11/27
    if (alertlist == nil) {
        alertlist = [[NSMutableArray alloc] init];
    }
    else if (alertlist.count >= MAX_SHOWALERT_COUNT) {
        while (alertlist.count >= MAX_SHOWALERT_COUNT) {
            EventAlertView *event = [alertlist objectAtIndex:0];
            [event dismissAlert];
        }
    }
    [alertlist addObject:alert];
#if DEBUG_LOG
    NSLog(@"alertlist size %d", [alertlist count]);
#endif
    [alert showAlert];
    [alert release];
    
    // jerrylu, 2013/02/18, add sound
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIUserNotificationSettings *remoteNoteSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        if(remoteNoteSettings.types & UIUserNotificationTypeSound)
            AudioServicesPlaySystemSound(1002);
    }
    else {
        UIRemoteNotificationType remoteNoteTypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if(remoteNoteTypes & UIRemoteNotificationTypeSound)
            AudioServicesPlaySystemSound(1002);
    }
    
//    [self pausePlaybackView]; // Annoying - Brosso
}

- (void)pausePlaybackView {
    UIViewController *lastview = (UIViewController *)[[navigationController viewControllers] lastObject];
    if ([lastview isKindOfClass:[SingleCamViewController class]]) {
        SingleCamViewController *singlecamview = (SingleCamViewController *)lastview;
        if (singlecamview.eViewModeType == eViewMode_Playback)
            [singlecamview performSelectorOnMainThread:@selector(pbControlPause) withObject:nil waitUntilDone:NO];
    }
    else if ([lastview isKindOfClass:[EventViewController class]]) {
        EventViewController *eventview = (EventViewController *)lastview;
        if (eventview.eEvType == eEV_Playback)
            [eventview performSelectorOnMainThread:@selector(pbControlPause) withObject:nil waitUntilDone:NO];
    }
    else if ([lastview isKindOfClass:[LiveViewEventViewController class]]) {
        LiveViewEventViewController *eventview = (LiveViewEventViewController *)lastview;
        if (eventview.eEvType == eEV_Playback)
            [eventview performSelectorOnMainThread:@selector(pbControlPause) withObject:nil waitUntilDone:NO];
    }
}

- (void)showEventView:(Eventinfo *) einfo {
    // jerrylu, 2012/10/22, support ios 6.0, and it avoid to use addSubview function
    self.window.rootViewController = navigationController;
    
    SiteListController *rootviewcontroller = (SiteListController *)self.favoriteSiteController;
    [rootviewcontroller openEventView:einfo];
}
#endif //FUNC_PUSH_NOTIFICATION_SUPPORT

- (void)reconnectToServer:(ServerInfo *) sinfo {
    return;
}

- (void)reconnectToMyView:(MyViewInfo *) vinfo {
    [self popToRootView];
    SiteListController *rootviewcontroller = (SiteListController *)self.favoriteSiteController;
    [rootviewcontroller openMyView:vinfo];
}

#pragma mark -
#pragma mark Memory management

- (void)dealloc {
#if FUNC_PUSH_NOTIFICATION_SUPPORT
    [strToken release];
    [eventlist release];
    eventlist = nil;
    [eventlistCondition release];
    eventlistCondition = nil;
    [alertlist release];
    alertlist = nil;
#endif
    [logoview release];
	[navigationController release];
	[window release];
	[super dealloc];
}

#if FUNC_PUSH_NOTIFICATION_SUPPORT
#pragma mark -
#pragma mark EventList Access Method
- (BOOL)isNeedWriteEventList:(Eventinfo *)einfo {
    if ([einfo.eventType hasPrefix:@"CameraEvent"] ||
        [einfo.eventType hasPrefix:@"PosEvent"] ||
        [einfo.eventType hasPrefix:@"IvsEvent"] ||
        [einfo.eventType hasPrefix:@"IOEvent1"] ||
        [einfo.eventType hasPrefix:@"EdgeEvent"]) {
        if (einfo.cameraID != nil &&
            ![einfo.cameraID isEqualToString:@"-1"])
            return YES;
    }
    return NO;
}

- (void)saveEventList {
    [eventlistCondition lock];
    
    NSString * path = [[self class] pathForDocumentWithName:@"EventList.plist"];
	NSMutableArray * infoList = [[NSMutableArray alloc] init];
	for (Eventinfo * info in eventlist) {
        [infoList addObject:[info getPropertyDic]];
	}
    
	NSString * errStr;
	NSData * pList = [NSPropertyListSerialization dataFromPropertyList:infoList format:NSPropertyListXMLFormat_v1_0 errorDescription:&errStr];
	[infoList release];
    
    if (![pList writeToFile:path atomically:YES])
        NSLog(@"Failed to write the Event List");
    
    [eventlistCondition unlock];
}

- (void)loadEventList {
    if (eventlistCondition == nil) {
        eventlistCondition = [[NSCondition alloc] init];
    }
    
    [eventlistCondition lock];
    
    if (eventlist == nil) {
        NSString * path = [[self class] pathForDocumentWithName:@"EventList.plist"];
        NSData * savedData = [NSData dataWithContentsOfFile:path];
        NSArray * infoDicts;
        NSString * errStr;
        NSPropertyListFormat format;
        infoDicts = [NSPropertyListSerialization propertyListFromData:savedData
													 mutabilityOption:NSPropertyListImmutable
                                                               format:&format
													 errorDescription:&errStr];
	    
        if (infoDicts == nil) {
            NSLog(@"Unable to read plist file at path: %@", path);
            path = [[NSBundle mainBundle] pathForResource:@"EventList"
                                                   ofType:@"plist"];
            infoDicts = [NSMutableArray arrayWithContentsOfFile:path];
        }
        
        eventlist = [[NSMutableArray alloc] initWithCapacity:[infoDicts count]];
        
        for (NSDictionary * currDict in infoDicts) {
            Eventinfo *info = [[[Eventinfo alloc] initWithDictionary:currDict] autorelease];
            [eventlist addObject:info];
        }
    }
    
    [eventlistCondition unlock];
}

- (BOOL)addEventList:(Eventinfo *) einfo {
    [eventlistCondition lock];
    
    NSMutableArray *new_eventlist = [[NSMutableArray alloc] init];
    int count = 0;
    
    for (Eventinfo *info in eventlist) {
        if ([einfo.serverID isEqualToString:info.serverID]) {
            // check duplicated event
            if ([einfo.cameraID isEqualToString:info.cameraID] &&
                [einfo.eventType isEqualToString:info.eventType] &&
                [einfo.username isEqualToString:info.username] &&
                [einfo.eventDate isEqualToDate:info.eventDate]) {
                [new_eventlist release];
                new_eventlist = nil;
                [eventlistCondition unlock];
                return NO;
            }
                        
            if (count < MAX_EVENTCOUNT_SERVER - 1) {
                [new_eventlist addObject:info];
                count++;
            }
        }
        else {
            [new_eventlist addObject:info];
        }
    }
    [new_eventlist insertObject:einfo atIndex:0];    

    [eventlist removeAllObjects];
    for (Eventinfo *info in new_eventlist) {
        [eventlist addObject:info];
    }
    [new_eventlist release];
    [eventlistCondition unlock];
    
    return YES;
}

- (BOOL)removeEventListByServerInfo:(ServerInfo *) sinfo {
    if (sinfo.serverType != eServer_MainConsole &&
        sinfo.serverType != eServer_NVRmini &&
        sinfo.serverType != eServer_NVRsolo) {
        return YES;
    }
    else if ([sinfo.serverID isEqualToString:@""])
        return YES;
    
    [eventlistCondition lock];
    
    NSMutableArray *new_eventlist = [[NSMutableArray alloc] init];
    for (Eventinfo *info in eventlist) {
        if ([info.serverID isEqualToString:sinfo.serverID]) {
            if (sinfo.connectionType == eIPConnection &&
                [info.username rangeOfString:sinfo.userName].location == NSNotFound) {
                [new_eventlist addObject:info];
            }
            else if (sinfo.connectionType == eP2PConnection &&
                     [info.username rangeOfString:sinfo.userName_P2P].location == NSNotFound) {
                [new_eventlist addObject:info];
            }
            else {
                NSArray *userArray = [info.username componentsSeparatedByString:@","];
                if ([userArray count] > 1) {
                    NSString *new_username = @"";
                    for (int i=0; i < [userArray count]; i++) {
                        NSString *username = [userArray objectAtIndex:i];
                        if (sinfo.connectionType == eIPConnection && [username isEqualToString:sinfo.userName]) {
                            continue;
                        }
                        else if (sinfo.connectionType == eP2PConnection && [username isEqualToString:sinfo.userName_P2P]) {
                            continue;
                        }
                        
                        if ([new_username isEqualToString:@""])
                            new_username = [NSString stringWithFormat:@"%@", username];
                        else
                            new_username = [NSString stringWithFormat:@"%@,%@", new_username, username];
                    }
                    info.username = new_username;
                    
                    NSString *new_emsg = [NSString stringWithFormat:@"%@,", new_username];
                    NSArray *emsg = [info.eventMsg componentsSeparatedByString:@"\n"];
                    for (int i=1; i < [emsg count]; i++) {
                        new_emsg = [NSString stringWithFormat:@"%@\n%@", new_emsg, [emsg objectAtIndex:i]];
                    }
                    info.eventMsg = new_emsg;
                    
                    [new_eventlist addObject:info];
                }
            }
        }
        else {
            [new_eventlist addObject:info];
        }
    }
    
    [eventlist removeAllObjects];
    for (Eventinfo *info in new_eventlist) {
        [eventlist addObject:info];
    }
    [new_eventlist release];
    
    [eventlistCondition unlock];
    
    return YES;
}

- (BOOL)checkDateToday:(NSDate *)date {
    NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    if([today day] == [otherDay day] &&
       [today month] == [otherDay month] &&
       [today year] == [otherDay year]) {
        return YES;
    }
    return NO;
}

NSInteger sortEventList(Eventinfo *einfo1, Eventinfo *einfo2, void *context)
{
    if ([einfo1.eventDate isEqualToDate:einfo2.eventDate]) {
        return NSOrderedSame;
    }
    
    if ([einfo1.eventDate compare:einfo2.eventDate] == NSOrderedAscending)   {
        return NSOrderedDescending;
    }
    else {
        return NSOrderedAscending;
    }
}

+ (NSString *)pathForDocumentWithName:(NSString *) documentName {
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                          NSUserDomainMask,
                                                          YES);
    NSString * path = [paths objectAtIndex:0];
    
    return [path stringByAppendingPathComponent:documentName];
}

- (void)dismissAllEvent {
    [self performSelectorOnMainThread:@selector(dismissAllEventThread) withObject:nil waitUntilDone:NO];
}

- (void)dismissAllEventThread {
    if (alertlist == nil)
        return;
    
    for (EventAlertView *event in alertlist) {
        event.handledelegate = nil;
        [event dismissAlert];
    }
    
    [alertlist removeAllObjects]; // jerrylu, 2013/02/01, fix bug11810
    [alertlist release];
    alertlist = nil;
}

- (int)getUnreadEventCount {
    int count = 0;
    
    [eventlistCondition lock];
    
    for (int i = 0; i < [eventlist count]; i++) {
        Eventinfo *einfo = [eventlist objectAtIndex:i];
        if (einfo.isNotRead) {
            count++;
        }
    }
    
    [eventlistCondition unlock];
    
    return count;
}

- (NSMutableArray *)getEventListFromServer:(NSString *)sid User:(NSString *)uname {
    NSMutableArray *filtereventlist = [[NSMutableArray alloc] init];
    
    [eventlistCondition lock];
    
    if ([sid isEqualToString:@""] && [uname isEqualToString:@""]) {
        for (Eventinfo *eventinfo in eventlist) {
            Eventinfo *new_einfo = [[[Eventinfo alloc] init] autorelease];
            [new_einfo setEventType:eventinfo.eventType];
            [new_einfo setUsername:eventinfo.username];
            [new_einfo setServerID:eventinfo.serverID];
            [new_einfo setCameraID:eventinfo.cameraID];
            [new_einfo setEventMsg:eventinfo.eventMsg];
            [new_einfo setEventDate:eventinfo.eventDate];
            new_einfo.isNotRead = eventinfo.isNotRead;
            
            [filtereventlist addObject:new_einfo];
        }
    }
    else {
        for (Eventinfo *eventinfo in eventlist) {
            if ([eventinfo.serverID isEqualToString:sid]) {
                NSArray *userArray = [eventinfo.username componentsSeparatedByString:@","];
                for (int i=0; i < [userArray count]; i++) {
                    NSString *username = [userArray objectAtIndex:i];
                    if ([username isEqualToString:uname]) {
                        Eventinfo *new_einfo = [[[Eventinfo alloc] init] autorelease];
                        [new_einfo setEventType:eventinfo.eventType];
                        [new_einfo setUsername:eventinfo.username];
                        [new_einfo setServerID:eventinfo.serverID];
                        [new_einfo setCameraID:eventinfo.cameraID];
                        [new_einfo setEventMsg:eventinfo.eventMsg];
                        [new_einfo setEventDate:eventinfo.eventDate];
                        new_einfo.isNotRead = eventinfo.isNotRead;
                        
                        [filtereventlist addObject:new_einfo];
                        break;
                    }
                }
            }
        }
    }
    
    [eventlistCondition unlock];
    
    return [filtereventlist autorelease];
}

- (void)setReadEvent:(Eventinfo *) einfo {
    [eventlistCondition lock];
    
    for (Eventinfo *eventinfo in eventlist) {
        if ([eventinfo.serverID isEqualToString:einfo.serverID] &&
            [eventinfo.cameraID isEqualToString:einfo.cameraID] &&
            [eventinfo.eventType isEqualToString:einfo.eventType] &&
            [eventinfo.username isEqualToString:einfo.username] &&
            [eventinfo.eventDate isEqualToDate:einfo.eventDate]) {
            eventinfo.isNotRead = 0;
            break;
        }
    }
    
    [eventlistCondition unlock];
}

#pragma mark - EventAlertViewDelegate EventiOS7AlertViewDelegate Method
- (void)eventAlertViewShowEventView:(EventAlertView *) alert {
    [self showEventView:alert.eInfo];
    [alertlist removeObject:alert];
    [self dismissAllEventThread];
    return;
}

- (void)eventAlertViewRemoveAlertFromList:(EventAlertView *) alert {
    [alertlist removeObject:alert];
}
#endif //FUNC_PUSH_NOTIFICATION_SUPPORT

@end

