//
//  PlaybackMenuViewController.m
//  iViewer
//
//  Created by Toby Huang on 12/6/4.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import "PlaybackMenuViewController.h"
#import "LiveViewAppDelegate.h"
#import "SingleCamViewController.h"

@implementation PlaybackMenuViewController

@synthesize chIndex;
@synthesize singleViewDelegate;
@synthesize serverMgr;
@synthesize audioswitch;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IPAD) {
        self.preferredContentSize = CGSizeMake(PopoverSizeWidth, 200);
        
        UIBarButtonItem * CloseButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(dismissPopover)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [CloseButton setTintColor:COLOR_NAVBUTTONTINTPAD];
        }
        else {
            [CloseButton setTintColor:COLOR_NAVBUTTONTINTPADIOS6];
        }
        self.navigationItem.leftBarButtonItem = CloseButton;
        [CloseButton release];
    }
    else {
#if 1
        UIImage *backImage;
        UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
        else
            backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
        bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
        [bButton setImage:backImage forState:UIControlStateNormal];
        [bButton addTarget:self action:@selector(popview) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(popview)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [backButton setTintColor:COLOR_NAVBUTTONTINT];
        }
        else {
            [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
        }
#endif
        self.navigationItem.leftBarButtonItem = backButton;
        [backButton release];
    }
}

- (void)popview {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dismissPopover {
    if (IS_IPAD) {
        SingleCamViewController *camview = (SingleCamViewController *)singleViewDelegate;
        [camview dismissPlaybackMenuPopover];
    }
    return;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    audioswitch.on = [serverMgr getPlaybackAudioStateByChIndex:chIndex];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    singleViewDelegate = nil;
    [super dealloc];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ePlaybackFunctionMax;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSString *)tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger) section {
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        NSUInteger section = [indexPath section];
        switch (section) {
            case ePlaybackFunctionAudio:
            {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                
                UISwitch *switchobj = [[[UISwitch alloc] initWithFrame:CGRectZero] autorelease];
                
                [switchobj addTarget:self action:@selector(audioSwitchChanged) forControlEvents:UIControlEventValueChanged];
                audioswitch = switchobj;
                cell.accessoryView = switchobj;
                cell.textLabel.text = NSLocalizedString(@"Audio", nil);
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                break;
            }
            case ePlaybackFunctionSnapshot:
            {
                cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.textLabel.text = NSLocalizedString(@"Snapshot", nil);
                cell.textLabel.textAlignment = NSTextAlignmentCenter;
                break;
            }
            default:
                break;
        }
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = [indexPath section];
    
    switch (section) {
        case ePlaybackFunctionSnapshot:
        {
            [self.navigationController popViewControllerAnimated:YES];
            if ([singleViewDelegate respondsToSelector:@selector(PbMenuViewControllerSnapshot)]) {
                [singleViewDelegate PbMenuViewControllerSnapshot];
            }
            break;
        }
        case ePlaybackFunctionAudio:
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - General methods
-(void) audioSwitchChanged {
    if ([singleViewDelegate respondsToSelector:@selector(PbMenuViewControllerAudioChange)]) {
        [singleViewDelegate PbMenuViewControllerAudioChange];
    }
}

@end
