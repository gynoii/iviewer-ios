//
//  QuickLinkController.m
//  TabView
//
//  Created by johnlinvc on 10/02/04.
//  Copyright 2010 com.debug. All rights reserved.
//

#import "SiteInfoController.h"
#import "LiveViewAppDelegate.h"
#import "SinglePageViewController_Base.h"
#import "SlideView.h"
#import "ControlHelper.h"
#import "Utility.h"

@implementation SiteInfoController

@synthesize favoriteSite = _favoriteSite;
@synthesize serverInfo = _serverInfo;
@synthesize IPCell = _IPCell;
@synthesize portCell = _portCell;
#if FUNC_PLAYBACK_SUPPORT
@synthesize playbackportCell = _playbackportCell; // jerrylu, 2012/04/19
#endif
@synthesize accountCell = _accountCell;
@synthesize passwordCell = _passwordCell;
@synthesize nameCell = _nameCell;
@synthesize idCell = _idCell;
@synthesize p2pNameCell = _p2pNameCell; // jerrylu, 2012/09/28
@synthesize p2pAccountCell = _p2pAccountCell;
@synthesize p2pPasswordCell = _p2pPasswordCell;
@synthesize isAdding = _isAdding;
@synthesize backupInfo;

#pragma mark -
#pragma mark General Methods
- (void)dealloc {
    [_ConnectionTypeControl release];
	[_serverInfo release];
	[_IPCell release];
	[_portCell release];
#if FUNC_PLAYBACK_SUPPORT
    [_playbackportCell release]; // jerrylu, 2012/04/19
#endif
	[_accountCell release];
	[_passwordCell release];
	[_nameCell release];
    [_idCell release];
    [_p2pNameCell release]; // jerrylu, 2012/09/28
    [_p2pAccountCell release];
    [_p2pPasswordCell release];
	[_cellArray release];
	[backupInfo release];
    [super dealloc];
}

- (id)initWithStyle:(UITableViewStyle) style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if ((self = [super initWithStyle:UITableViewStyleGrouped])) {
        self.title = NSLocalizedString(@"Add Server", nil);
		_isAdding = NO;
		bSaving = TRUE;
		backupInfo = [[ServerInfo alloc] init];
    }
    return self;
}

- (void)newDetailCellWithTag:(NSInteger) tag detailcell:(EditableDetailCell *) detailcell {
    [[detailcell textField] setDelegate:self];
    [[detailcell textField] setTag:tag];
}

- (void)viewDidLoad {
    UIBarButtonItem * saveButton = [[UIBarButtonItem alloc]
									initWithBarButtonSystemItem:UIBarButtonSystemItemSave
									target:self
									action:@selector(save)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [saveButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [saveButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
	[[self navigationItem] setRightBarButtonItem:saveButton];
	[saveButton release];
    
    // chech if it is the root viewcontroller
    if ([[[self navigationController] viewControllers] indexOfObject:self] != 0) {
#if 1
        UIImage *backImage;
        UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
        else
            backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
        bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
        [bButton setImage:backImage forState:UIControlStateNormal];
        [bButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(back)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [backButton setTintColor:COLOR_NAVBUTTONTINT];
        }
        else {
            [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
        }
#endif
        [[self navigationItem] setLeftBarButtonItem:backButton];
        [backButton release];
    }
    else {
        UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                          target:self
                                          action:@selector(cancel)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [cancelButton setTintColor:COLOR_NAVBUTTONTINT];
        }
        else {
            [cancelButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
        }
        [[self navigationItem] setLeftBarButtonItem:cancelButton];
        [cancelButton release];
    }
    
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    EditableDetailCell *ipcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:eIP detailcell:ipcell];
    [self setIPCell:ipcell];
    EditableDetailCell *portcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:ePort detailcell:portcell];
    [self setPortCell:portcell];
#if FUNC_PLAYBACK_SUPPORT
    EditableDetailCell *pbportcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:ePlaybackPort detailcell:pbportcell];
    [self setPlaybackportCell:pbportcell];
#endif
    EditableDetailCell *acountcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:eAccount detailcell:acountcell];
    [self setAccountCell:acountcell];
    EditableDetailCell *pwdcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:ePassword detailcell:pwdcell];
    [self setPasswordCell:pwdcell];
    EditableDetailCell *namecell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:eName detailcell:namecell];
    [self setNameCell:namecell];
    EditableDetailCell *idcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:eP2pId detailcell:idcell];
    [self setIdCell:idcell];
    EditableDetailCell *p2pNamecell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    // jerrylu, 2012/09/28
    [self newDetailCellWithTag:eP2pName detailcell:p2pNamecell];
    [self setP2pNameCell:p2pNamecell];
    EditableDetailCell *p2pAccountcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:eP2pAccount detailcell:p2pAccountcell];
    [self setP2pAccountCell:p2pAccountcell];
    EditableDetailCell *p2pPwdcell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:eP2pPassword detailcell:p2pPwdcell];
    [self setP2pPasswordCell:p2pPwdcell];
    
	[backupInfo setServerIP:[_serverInfo serverIP]];
	[backupInfo setServerPort:[_serverInfo serverPort]];
    [backupInfo setServerPlaybackPort:[_serverInfo serverPlaybackPort]]; // jerrylu, 2012/04/19
	[backupInfo setUserName:[_serverInfo userName]];
	[backupInfo setUserPassword:[_serverInfo userPassword]];
	[backupInfo setServerName:[_serverInfo serverName]];
    [backupInfo setServerID_P2P:[_serverInfo serverID_P2P]]; // jerrylu, 2012/09/10
    [backupInfo setConnectionType:[_serverInfo connectionType]]; // jerrylu, 2012/07/16
    [backupInfo setServerName_P2P:[_serverInfo serverName_P2P]]; // jerrylu, 2012/09/28
    [backupInfo setUserName_P2P:[_serverInfo userName_P2P]];
    [backupInfo setUserPassword_P2P:[_serverInfo userPassword_P2P]];
    [backupInfo setServerID:[_serverInfo serverID]];
    [backupInfo setServerType:[_serverInfo serverType]];
    
    // jerrylu, 2012/07/25
#if CUSTOMER_FOR_NUUO
    _ConnectionTypeControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"IP Address", nil), NSLocalizedString(@"ezNUUO", nil), nil]];
#else
    _ConnectionTypeControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"IP Address", nil), NSLocalizedString(@"findNVR", nil), nil]];
#endif
    _ConnectionTypeControl.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [_ConnectionTypeControl addTarget:self action:@selector(toggleControls) forControlEvents:UIControlEventValueChanged];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        }
        else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
    }
    else {
        [self resizeViewWithOrientation:self.interfaceOrientation];
    }
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL) animated {
    [super viewWillAppear:animated];

    _ConnectionTypeControl.selectedSegmentIndex = [_serverInfo connectionType];
    
    [[_nameCell textField] setText:[_serverInfo serverName]];
    [[_IPCell textField] setText:[_serverInfo serverIP]];
    [[_portCell textField] setText:[_serverInfo serverPort]];
    [[_playbackportCell textField] setText:[_serverInfo serverPlaybackPort]];
    [[_accountCell textField] setText:[_serverInfo userName]];
    [[_passwordCell textField] setText:[_serverInfo userPassword]];
    [[_p2pNameCell textField] setText:[_serverInfo serverName_P2P]];
    [[_idCell textField] setText:[_serverInfo serverID_P2P]];
    [[_p2pAccountCell textField] setText:[_serverInfo userName_P2P]];
    [[_p2pPasswordCell textField] setText:[_serverInfo userPassword_P2P]];
    
    NSUInteger indexes[] = {0, 0};

    NSIndexPath * indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];

#if FUNC_P2P_SAT_SUPPORT
    UITableViewCell *cell = (UITableViewCell *)[[self tableView] cellForRowAtIndexPath:indexPath];
    [cell becomeFirstResponder];
#else
    EditableDetailCell * cell = (EditableDetailCell *)[[self tableView] cellForRowAtIndexPath:indexPath];
    [[cell textField] becomeFirstResponder];
#endif
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        }
        else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
    }
    else {
        [self resizeViewWithOrientation:self.interfaceOrientation];
    }
}

- (void)viewWillDisappear:(BOOL) animated {
    [super viewWillDisappear:animated];
	   
    int numberSection;
#if FUNC_P2P_SAT_SUPPORT
    if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection)
#endif
        numberSection = eMaxSectionType;
#if FUNC_P2P_SAT_SUPPORT
    else
        numberSection = eMaxSectionP2pType;
#endif
    
    for (NSUInteger section = 0; section < numberSection; section++) {
#if FUNC_P2P_SAT_SUPPORT
        if (section == eTypeControlSection || section == eP2pTypeControlSection) {
            continue;
        }
#endif
        for (NSUInteger row = 0; row < [[self tableView] numberOfRowsInSection:section]; row++) {
            NSUInteger indexes[] = { section, row };
            NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];
            EditableDetailCell * cell = (EditableDetailCell *)[[self tableView] cellForRowAtIndexPath:indexPath];
            if ([[cell textField] isFirstResponder]) {
                [[cell textField] resignFirstResponder];
            }
        }
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (size.width > size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        } else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self resizeViewWithOrientation:toInterfaceOrientation];
}

-(void)resizeViewWithOrientation:(UIInterfaceOrientation) orientation {
    for (EditableDetailCell * cell in _cellArray) {
        CGRect rect;
        UIFontDescriptor *currentDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleBody];
        CGFloat bodyTextSize = [currentDescriptor pointSize];
        
        if (IS_IPAD) {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
                    rect = CGRectMake(20.0, 10.0, 900.0, bodyTextSize+8.0);
                }
                else {
                    rect = CGRectMake(20.0, 10.0, 640.0, bodyTextSize+8.0);
                }
            }
            else {
                if (UIInterfaceOrientationIsPortrait(orientation)) {
                    rect = CGRectMake(20.0, 10.0, 640.0, 24.0);
                }
                else {
                    rect = CGRectMake(20.0, 10.0, 900.0, 24.0);
                }
            }
        }
        else {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, bodyTextSize+8.0);
            }
            else {
                if (UIInterfaceOrientationIsPortrait(orientation)) {
                    rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, 24.0);
                }
                else {
                    rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.height - 60, 24.0);
                }
            }
        }
        
        cell.textField.frame = rect;
    }
    [self.view setNeedsDisplay];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	// Release any cached data, images, etc that aren't in use.
}

/*
-(void) keyboardWillShow:(NSNotification *)note
{
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = _TableView.frame;
    
    // Start animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view 
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height -= keyboardBounds.size.height;
    else 
        frame.size.height -= keyboardBounds.size.width;
    
    if (frame.size.height < 0) {
        return;
    }
    
    // Apply new size of table view
    _TableView.frame = frame;
    
    if (activeTextField) {
        UITableViewCell *cell = (UITableViewCell*) [[activeTextField superview] superview];
        [_TableView scrollToRowAtIndexPath:[_TableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = _TableView.frame;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view 
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height += keyboardBounds.size.height;
    else 
        frame.size.height += keyboardBounds.size.width;
    
    // Apply new size of table view
    _TableView.frame = frame;
    
    [UIView commitAnimations];
}
*/
#pragma mark -
#pragma mark Action Methods

- (void)save {
	if ([self checkInfo] != TRUE) {
		return;
	}
	
	[self loadCellInfos];

	if (_isAdding) {
		[[self favoriteSite] addObject:[self serverInfo]];
        [[self favoriteSite] setIsShowMyServer:YES];
        [[self favoriteSite] setIsShowMyView:NO];
	}
    else {
        if (isChangeSettings) {
            [_favoriteSite removeMyServerInMyView:backupInfo];
            _serverInfo.serverPushNotificationEnable = kDefaultPushNotification;
            _serverInfo.serverType = eServer_Default;
        }
        
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/11/12, fix bug10111
        if (backupInfo.connectionType != self.serverInfo.connectionType || isChangeSettings) {
            // Unregister Push Notification
            NSThread *unregister_thread = [[NSThread alloc] initWithTarget:_favoriteSite selector:@selector(unRegPushServer:) object:backupInfo];
            [unregister_thread start];
            [unregister_thread release];
            
            LiveViewAppDelegate *appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
            [appDelegate removeEventListByServerInfo:backupInfo];
            [appDelegate saveEventList];
            
            _serverInfo.serverID = @"";
        }
#endif
    }
    
    if (bSaving == TRUE) {
		[_favoriteSite save];
	}
    
    [self dismissViewControllerAnimated:YES completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
}

- (void)back {
	bSaving = FALSE;
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)cancel {
	bSaving = FALSE;
    [self dismissViewControllerAnimated:YES completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
}

- (BOOL)checkInfo {
    for (EditableDetailCell * cell in _cellArray) {
        NSInteger tag = [[cell textField] tag];
#if FUNC_P2P_SAT_SUPPORT
        if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
#endif // FUNC_P2P_SAT_SUPPORT
#if FUNC_PLAYBACK_SUPPORT
            if (tag == ePort || tag == ePlaybackPort) // jerrylu, 2012/05/08
#else
            if (tag == ePort)
#endif
            {
                if ([[[cell textField] text] length] == 0) {
#if FUNC_PLAYBACK_SUPPORT
                    if (tag == ePlaybackPort) // jerrylu, 2012/12/11
                        [self popWarning:eError_BlankPlaybackPort];
                    else
#endif
                    [self popWarning:eError_Blank];
                    return FALSE;
                }
                NSString * str = [[cell textField] text];
                NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^[0-9]+$"];
                if ([predicate evaluateWithObject:str] != YES) {
                    [self popWarning:eError_Port];
                    return FALSE;
                }
                
                int value = [str intValue];
                if (value < 1 || value >65535) {
                    [self popWarning:eError_InValidPort];
                    return FALSE;
                }
                else { // jerrylu, 2013.01.28 fix bug11785
                    NSString *portstr = [[NSString alloc] initWithFormat:@"%d", value];
                    [[cell textField] setText:portstr];
                    [portstr release];
                }
            }
            if (tag == eIP) {
                if ([[[cell textField] text] length] == 0) {
                    [self popWarning:eError_Blank];
                    return FALSE;
                }
                if (isValidIpAddress((char *)[[[cell textField] text] UTF8String])) {
                    NSString *new_ip = [NSString stringWithFormat:@"%s", convertIpAddress((char *)[[[cell textField] text] UTF8String])];
                    [[cell textField] setText:new_ip];
                }
            }
            if (tag == eAccount) {
                if ([[[cell textField] text] length] == 0) {
                    [self popWarning:eError_Blank];
                    return FALSE;
                }
                
                NSRange whiteSpaceRange = [[[cell textField] text] rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
                if (whiteSpaceRange.location != NSNotFound) {
                    [self popWarning:eError_InValidInputStr];
                    return FALSE;
                }
            }
            if (tag == ePassword) {
                NSRange whiteSpaceRange = [[[cell textField] text] rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
                if (whiteSpaceRange.location != NSNotFound) {
                    [self popWarning:eError_InValidInputStr];
                    return FALSE;
                }
            }
#if FUNC_P2P_SAT_SUPPORT
        }
        else {
            if (tag == eP2pId) {
                if ([[[cell textField] text] length] == 0) {
                    [self popWarning:eError_Blank];
                    return FALSE;
                }
            }
            if (tag == eP2pAccount) {
                if ([[[cell textField] text] length] == 0) {
                    [self popWarning:eError_Blank];
                    return FALSE;
                }
            
                NSRange whiteSpaceRange = [[[cell textField] text] rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
                if (whiteSpaceRange.location != NSNotFound) {
                    [self popWarning:eError_InValidInputStr];
                    return FALSE;
                }
            }
            if (tag == eP2pPassword) {
                NSRange whiteSpaceRange = [[[cell textField] text] rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
                if (whiteSpaceRange.location != NSNotFound) {
                    [self popWarning:eError_InValidInputStr];
                    return FALSE;
                }
            }
        }
#endif
    }
    // Check duplicate account
    for (ServerInfo *sinfo in _favoriteSite.displayedObjects) {
        if (!_isAdding && sinfo == _serverInfo) {
            continue;
        }
        
        if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection &&
            sinfo.connectionType == eIPConnection &&
            [sinfo.serverIP isEqualToString:[_IPCell.textField text]] &&
            [sinfo.serverPort isEqualToString:[_portCell.textField text]] &&
            [sinfo.userName isEqualToString:[_accountCell.textField text]]) {
            [self popWarning:eError_DuplicateIpAccount];
            return false;
        }
        else if ([_ConnectionTypeControl selectedSegmentIndex] == eP2PConnection &&
                 sinfo.connectionType == eP2PConnection &&
                 [sinfo.serverID_P2P isEqualToString:[_idCell.textField text]] &&
                 [sinfo.userName_P2P isEqualToString:[_p2pAccountCell.textField text]]) {
            [self popWarning:eError_DuplicateP2pAccount];
            return false;
        }
    }
	return TRUE;
}

- (void)popWarning:(EErrorType) eErrorType {
	NSString * strMsg;
	switch (eErrorType) {
		case eError_Port:
			strMsg = [NSString stringWithFormat:NSLocalizedString(@"Server Port only supports numeric", nil)];
			break;
		case eError_Username:
			strMsg = [NSString stringWithFormat:NSLocalizedString(@"Username only supports English", nil)];
			break;
		case eError_Blank:
#if FUNC_P2P_SAT_SUPPORT
            if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection)
#endif
                strMsg = [NSString stringWithFormat:NSLocalizedString(@"Server Address, Port and Username cannot be empty", nil)];
#if FUNC_P2P_SAT_SUPPORT
			else
                strMsg = [NSString stringWithFormat:NSLocalizedString(@"Server ID and Username cannot be empty", nil)];
#endif
            break;
        case eError_InValidPort:
            strMsg = [NSString stringWithFormat:NSLocalizedString(@"Port number must be 1~65535.", nil)];
			break;
        case eError_BlankPlaybackPort:
            strMsg = [NSString stringWithFormat:NSLocalizedString(@"The playback port of Titan NVR and Crystal Titan should be the same as the live streaming port.", nil)];
			break;
        case eError_DuplicateIpAccount:
            strMsg = [NSString stringWithFormat:NSLocalizedString(@"The combination of this IP address, port number and user account has been used.", nil)];
            break;
        case eError_DuplicateP2pAccount:
#if CUSTOMER_FOR_NUUO
            strMsg = [NSString stringWithFormat:NSLocalizedString(@"The combination of ezNUUO ID and user account has been used.", nil)];
#else
            strMsg = [NSString stringWithFormat:NSLocalizedString(@"The combination of findNVR ID and user account has been used.", nil)];
#endif
            break;
        case eError_InValidInputStr:
			strMsg = [NSString stringWithFormat:NSLocalizedString(@"Username and password should not contain any special characters, symbols or spaces.", nil)];
			break;
		default:
			break;
	}
	
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:strMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                        message:strMsg
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)loadCellInfos {
	for (EditableDetailCell * cell in _cellArray) {
        UITextField * field = [cell textField];
		NSString * text = [field text];
#if FUNC_P2P_SAT_SUPPORT
        if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
#endif
            switch ([field tag]) {
                case eIP:
                    [_serverInfo setServerIP:text];
                    if (![text isEqualToString:[backupInfo serverIP]]) {
                        isChangeSettings = YES;
                    }
                    break;
                case ePort:
                    [_serverInfo setServerPort:text];
                    if (![text isEqualToString:[backupInfo serverPort]]) {
                        isChangeSettings = YES;
                    }
                    break;
#if FUNC_PLAYBACK_SUPPORT
                case ePlaybackPort: // jerrylu, 2012/04/19
                    [_serverInfo setServerPlaybackPort:text];
                    break;
#endif
                case eAccount:
                    [_serverInfo setUserName:text];
                    if (![text isEqualToString:[backupInfo userName]]) {
                        isChangeSettings = YES;
                    }
                    break;
                case ePassword:
                    [_serverInfo setUserPassword:text];
                    break;
                case eName:
                    if ([text length] > 0)
                        [_serverInfo setServerName:text];
                    else {
                        [_serverInfo setServerName:[[_IPCell textField] text]];
                        [[_nameCell textField] setText:[[_IPCell textField] text]];
                    }
                    break;
                default:
                    break;
            }
#if FUNC_P2P_SAT_SUPPORT
        }
        else {
            switch ([field tag]) {
                case eP2pName:
                    if ([text length] > 0)
                        [_serverInfo setServerName_P2P:text];
                    else {
                        [_serverInfo setServerName_P2P:[[_idCell textField] text]];
                        [[_p2pNameCell textField] setText:[[_idCell textField] text]];
                    }
                    break;
                case eP2pId:
                    [_serverInfo setServerID_P2P:text]; // jerrylu, 2012/09/10
                    if (![text isEqualToString:[backupInfo serverID_P2P]]) {
                        isChangeSettings = YES;
                    }
                    break;
                case eP2pAccount:
                    [_serverInfo setUserName_P2P:text];
                    if (![text isEqualToString:[backupInfo userName_P2P]]) {
                        isChangeSettings = YES;
                    }
                    break;
                case eP2pPassword:
                    [_serverInfo setUserPassword_P2P:text];
                    break;
                default:
                    break;
            }
        }
#endif
	}

    // jerrylu, 2012/07/16
    NSInteger index = _ConnectionTypeControl.selectedSegmentIndex;
    [_serverInfo setConnectionType:index];

#if FUNC_P2P_SAT_SUPPORT // jerrylu, 2012/10/24, just save one of connection type
    if ([_serverInfo connectionType] == eIPConnection) {
        [_serverInfo setServerName_P2P:@""];
        [_serverInfo setServerID_P2P:@""];
        [_serverInfo setUserName_P2P:@""];
        [_serverInfo setUserPassword_P2P:@""];
    }
    else {
        [_serverInfo setServerName:@""];
        [_serverInfo setServerIP:@""];
        [_serverInfo setServerPort:@""];
        [_serverInfo setServerPlaybackPort:@""];
        [_serverInfo setUserName:@""];
        [_serverInfo setUserPassword:@""];
    }
#endif
}

#pragma mark -
#pragma mark Action Methods
- (void)toggleControls {
    isChangeConnectionType = YES; // jerrylu, 2012/09/19

#if FUNC_P2P_SAT_SUPPORT
    if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
        if ([_p2pNameCell.textField text] != nil)
            [_serverInfo setServerName_P2P:[_p2pNameCell.textField text]];
        if ([_idCell.textField text] != nil)
            [_serverInfo setServerID_P2P:[_idCell.textField text]];
        if ([_p2pAccountCell.textField text] != nil)
            [_serverInfo setUserName_P2P:[_p2pAccountCell.textField text]];
        if ([_p2pPasswordCell.textField text] != nil)
            [_serverInfo setUserPassword_P2P:[_p2pPasswordCell.textField text]];
    }
    else {
        if ([_nameCell.textField text] != nil)
            [_serverInfo setServerName:[_nameCell.textField text]];
        if ([_IPCell.textField text] != nil)
            [_serverInfo setServerIP:[_IPCell.textField text]];
        if ([_portCell.textField text] != nil)
            [_serverInfo setServerPort:[_portCell.textField text]];
        if ([_playbackportCell.textField text] != nil)
            [_serverInfo setServerPlaybackPort:[_playbackportCell.textField text]];
        if ([_accountCell.textField text] != nil)
            [_serverInfo setUserName:[_accountCell.textField text]];
        if ([_passwordCell.textField text] != nil)
            [_serverInfo setUserPassword:[_passwordCell.textField text]];
    }
#endif
    
    [_cellArray release];
    _cellArray = nil;
    
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark UITextFieldDelegate Protocol

//  Sets the label of the keyboard's return key to 'Done' when the insertion
//  point moves to the table view's last field.
//
- (BOOL)textFieldShouldBeginEditing:(UITextField *) textField {
    UITableViewCell *cell = (UITableViewCell*) [[textField superview] superview];
    [[self tableView] scrollToRowAtIndexPath:[[self tableView] indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    if (isChangeConnectionType) { // jerrylu, 2012/09/19
        isChangeConnectionType = NO;
    }
    
#if FUNC_P2P_SAT_SUPPORT
    if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
#endif
        if ([textField tag] == ePassword) {
            [textField setReturnKeyType:UIReturnKeyDone];
        }
#if FUNC_P2P_SAT_SUPPORT
    }
    else {
        if ([textField tag] == eP2pPassword) {
            [textField setReturnKeyType:UIReturnKeyDone];
        }
    }
#endif
    activeTextField = textField;
    
    return YES;
}
//  UITextField sends this message to its delegate after resigning
//  firstResponder status. Use this as a hook to save the text field's
//  value to the corresponding property of the model object.
//

- (void)textFieldDidEndEditing:(UITextField *) textField {
    NSString * text = [textField text];
    
    if (isChangeConnectionType) { // jerrylu, 2012/09/19
        isChangeConnectionType = NO; 
    }
    else {
#if FUNC_P2P_SAT_SUPPORT
        if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
#endif
            switch ([textField tag]) {
                case eIP:
                    [_serverInfo setServerIP:text];
                    break;
                case ePort:
                    [_serverInfo setServerPort:text];
                    break;
#if FUNC_PLAYBACK_SUPPORT
                case ePlaybackPort: // jerrylu, 2012/04/19
                    [_serverInfo setServerPlaybackPort:text];
                    break;
#endif
                case eAccount:
                    [_serverInfo setUserName:text];
                    break;
                case ePassword:
                    [_serverInfo setUserPassword:text];
                    break;
                case eName:
                    [_serverInfo setServerName:text];
                    break;
                default:
                    break;
            }
#if FUNC_P2P_SAT_SUPPORT
        }
        else {
            switch ([textField tag]) {
                case eP2pName:
                    [_serverInfo setServerName_P2P:text];
                    break;
                case eP2pId:
                    [_serverInfo setServerID_P2P:text]; // jerrylu, 2012/09/10
                    break;
                case eP2pAccount:
                    [_serverInfo setUserName_P2P:text];
                    break;
                case eP2pPassword:
                    [_serverInfo setUserPassword_P2P:text];
                    break;
                default:
                    break;
            }
        }
#endif
	}
	if (bSaving == TRUE) {
		return;
	}
    
	[_serverInfo setServerIP:[backupInfo serverIP]];
	[_serverInfo setServerPort:[backupInfo serverPort]];
    [_serverInfo setServerPlaybackPort:[backupInfo serverPlaybackPort]];
	[_serverInfo setUserName:[backupInfo userName]];
	[_serverInfo setUserPassword:[backupInfo userPassword]];
	[_serverInfo setServerName:[backupInfo serverName]];
    [_serverInfo setServerID_P2P:[backupInfo serverID_P2P]];
    [_serverInfo setConnectionType:[backupInfo connectionType]];
    [_serverInfo setUserName_P2P:[backupInfo userName_P2P]];
	[_serverInfo setUserPassword_P2P:[backupInfo userPassword_P2P]];
	[_serverInfo setServerName_P2P:[backupInfo serverName_P2P]];
    [_serverInfo setServerType:[backupInfo serverType]];
    
    activeTextField = nil;
}

//  UITextField sends this message to its delegate when the return key
//  is pressed. Use this as a hook to navigate back to the list view 
//  (by 'popping' the current view controller, or dismissing a modal nav
//  controller, as the case may be).
//
//  If the user is adding a new item rather than editing an existing one,
//  respond to the return key by moving the insertion point to the next cell's
//  textField, unless we're already at the last cell.
//
- (BOOL)textFieldShouldReturn:(UITextField *) textField {
	
    if ([textField returnKeyType] != UIReturnKeyDone) {
        //  If this is not the last field (in which case the keyboard's
        //  return key label will currently be 'Next' rather than 'Done'), 
        //  just move the insertion point to the next field.
        //
        //  (See the implementation of -textFieldShouldBeginEditing: above.)
        //
        NSInteger nextTag = [textField tag] + 1;
        
        UIView *nextTextField = [[self tableView] viewWithTag:nextTag];
        
        [nextTextField becomeFirstResponder];
    }
	else {
		[textField resignFirstResponder];
    }
	
    return YES;
}

#pragma mark -
#pragma mark UITableViewDataSource Protocol

- (NSInteger)numberOfSectionsInTableView:(UITableView *) tableView {
#if FUNC_P2P_SAT_SUPPORT
    if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
        return eMaxSectionType;
    }
    else {
        return eMaxSectionP2pType;
    }
#else
    return eMaxSectionType;
#endif
}

- (NSInteger)tableView:(UITableView *) tableView
 numberOfRowsInSection:(NSInteger) section {
	return 1;
}

- (NSString *)tableView:(UITableView *) tableView
titleForHeaderInSection:(NSInteger) section {
#if FUNC_P2P_SAT_SUPPORT
    if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
#endif
        switch (section) {
            case eIPSection:  
                return NSLocalizedString(@"Server Address", nil);
            case ePortSection:
                return NSLocalizedString(@"Live Streaming Port", nil);
#if FUNC_PLAYBACK_SUPPORT
            case ePlaybackPortSection: // jerrylu, 2012/04/19
                return NSLocalizedString(@"Playback Port", nil);
#endif
            case eAccountSection:   
                return NSLocalizedString(@"Username", nil);
            case ePasswordSection:  
                return NSLocalizedString(@"Password", nil);
            case eNameSection: 
                return NSLocalizedString(@"Server Name", nil);
            default:	
                return nil;
        }
#if FUNC_P2P_SAT_SUPPORT
    }
    else {
        switch (section) {
            case eP2pNameSection: 
                return NSLocalizedString(@"Server Name", nil);
            case eP2pIdSection:  
                return NSLocalizedString(@"Server ID", nil);
            case eP2pAccountSection:   
                return NSLocalizedString(@"Username", nil);
            case eP2pPasswordSection:  
                return NSLocalizedString(@"Password", nil);
            default:	
                return nil;
        }
    }
#endif
}

- (UITableViewCell *)tableView:(UITableView *) tableView
         cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    NSUInteger section = [indexPath section];
    
#if FUNC_P2P_SAT_SUPPORT
    if (([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection && section == eTypeControlSection) || ([_ConnectionTypeControl selectedSegmentIndex] == eP2PConnection && section == eP2pTypeControlSection)) {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        _ConnectionTypeControl.frame = cell.contentView.bounds;
        [cell.contentView addSubview:_ConnectionTypeControl];
        
        return cell;
    }
    else {
#endif //FUNC_P2P_SAT_SUPPORT
        
        //  Pick the editable cell and the values for its textField
        EditableDetailCell * cell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
        NSInteger tag = INT_MIN;
        NSString * text = nil;
        NSString * placeholder = nil;

#if FUNC_P2P_SAT_SUPPORT
        if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
#endif
            switch (section) {
                case eNameSection:
                {
                    cell = [self nameCell];
                    text = [_serverInfo serverName];
                    tag = eName;
                    placeholder = NSLocalizedString(@"Optional Server Name", nil);
                    break;
                }
                case eIPSection:
                {
                    cell = [self IPCell];
                    text = [_serverInfo serverIP];
                    tag = eIP;
                    placeholder = NSLocalizedString(@"Server IP", nil);
                    break;
                }
                case ePortSection:
                {
                    cell = [self portCell];
                    text = [_serverInfo serverPort];
                    tag = ePort;
                    placeholder = NSLocalizedString(@"Server Port", nil);
                    break;            
                }
#if FUNC_PLAYBACK_SUPPORT
                case ePlaybackPortSection: // jerrylu, 2012/04/19
                {
                    cell = [self playbackportCell];
                    text = [_serverInfo serverPlaybackPort];
                    tag = ePlaybackPort;
                    placeholder = NSLocalizedString(@"Playback Port", nil);
                    break;
                }
#endif
                case eAccountSection:
                {
                    cell = [self accountCell];
                    text = [_serverInfo userName];
                    tag = eAccount;
                    placeholder = NSLocalizedString(@"Username", nil);
                    break;
                }
                case ePasswordSection: 
                {
                    cell = [self passwordCell];
                    text = [_serverInfo userPassword];
                    tag = ePassword;
                    placeholder = NSLocalizedString(@"User Password", nil);
                    break;
                }
                default:
                    break;
            }
#if FUNC_P2P_SAT_SUPPORT
        }
        else { // P2P
            switch (section) {
                case eP2pNameSection:
                {
                    cell = [self p2pNameCell];
                    text = [_serverInfo serverName_P2P];
                    tag = eP2pName;
                    placeholder = NSLocalizedString(@"Optional Server Name", nil);
                    break;
                }
                case eP2pIdSection:
                {
                    cell = [self idCell];
                    text = [_serverInfo serverID_P2P]; // jerrylu, 2012/09/10
                    tag = eP2pId;
                    placeholder = NSLocalizedString(@"Server ID", nil);
                    break;
                }
                case eP2pAccountSection:
                {
                    cell = [self p2pAccountCell];
                    text = [_serverInfo userName_P2P];
                    tag = eP2pAccount;
                    placeholder = NSLocalizedString(@"Username", nil);
                    break;
                }
                case eP2pPasswordSection: 
                {
                    cell = [self p2pPasswordCell];
                    text = [_serverInfo userPassword_P2P];
                    tag = eP2pPassword;
                    placeholder = NSLocalizedString(@"User Password", nil);
                    break;
                }
                default:
                    break;
            }
        }
#endif
        
        UITextField * textField = [cell textField];
        [textField setTag:tag];
        [textField setText:text];
        [textField setPlaceholder:placeholder];
#if FUNC_P2P_SAT_SUPPORT
        if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
#endif
            if (section == ePasswordSection) {
                textField.secureTextEntry = YES;
            }
#if FUNC_PLAYBACK_SUPPORT
            if (section == ePortSection || section == ePlaybackPortSection) {
#else
            if (section == ePortSection) {
#endif
                // 'publicationYear' is an NSNumber, so show a numeric keyboard.
                [textField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
            }
#if FUNC_P2P_SAT_SUPPORT
        }
        else {
            if (section == eP2pPasswordSection) {
                textField.secureTextEntry = YES;
            }
        }
#endif
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
           
        CGRect rect;
        UIFontDescriptor *currentDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleBody];
        CGFloat bodyTextSize = [currentDescriptor pointSize];
        
        if (IS_IPAD) {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
                    rect = CGRectMake(20.0, 10.0, 900.0, bodyTextSize+8.0);
                }
                else {
                    rect = CGRectMake(20.0, 10.0, 640.0, bodyTextSize+8.0);
                }
            }
            else {
                if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
                    rect = CGRectMake(20.0, 10.0, 640.0, 24.0);
                }
                else {
                    rect = CGRectMake(20.0, 10.0, 900.0, 24.0);
                }
            }
        }
        else {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, bodyTextSize+8.0);
            }
            else {
                if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
                    rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, 24.0);
                }
                else {
                    rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.height - 60, 24.0);
                }
            }
        }
        
        cell.textField.frame = rect;
        
        EditableDetailCell * theNull = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
        theNull.textField.tag = -1;
        
        if (_cellArray == nil) {
#if FUNC_P2P_SAT_SUPPORT
            if ([_ConnectionTypeControl selectedSegmentIndex] == eIPConnection) {
#endif
                _cellArray = [[NSMutableArray alloc] initWithCapacity:eMaxSectionType];
                
                for (int i = 0; i < eMaxSectionType; i++) {
                    [_cellArray addObject:theNull];
                }
#if FUNC_P2P_SAT_SUPPORT
            }else {
                _cellArray = [[NSMutableArray alloc] initWithCapacity:eMaxSectionP2pType];
                
                for (int i = 0; i < eMaxSectionP2pType; i++) {
                    [_cellArray addObject:theNull];
                }
            }
#endif
        }
        [_cellArray replaceObjectAtIndex:tag withObject:cell];
        return cell;
#if FUNC_P2P_SAT_SUPPORT
    }
#endif //FUNC_P2P_SAT_SUPPORT
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *) indexPath {
    // Return NO if you do not want the specified item to be editable.
	return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath {
    // Navigation logic may go here. Create and push another view controller.
}

@end
