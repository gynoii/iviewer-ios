//
//  singlecam.m
//  TabView
//
//  Created by johnlinvc on 10/02/04.
//  Copyright 2010 com.debug. All rights reserved.
//

#import "SingleCamViewController.h"
#import "SinglePageViewController_Base.h"
#import "SinglePageViewController_1x1.h"
#import "ControlHelper.h"
#import "PresetTableViewController.h"
#import <UIKit/UIImage.h>
#import "LiveViewAppDelegate.h"
#import "Utility.h"

@interface SingleCamViewController (PrivateMethods)

- (void)loadScrollViewWithCameraIndex:(int) index;
- (void)scrollViewDidScroll:(UIScrollView *) sender;

@end

@implementation SingleCamViewController

@synthesize changeLayoutDelegate;

@synthesize scrollView;
@synthesize pagecontrol;
@synthesize parent;
@synthesize serverMgr;

@synthesize ptzCap;
@synthesize audioCap;
@synthesize talkCap;

@synthesize arrowUp;
@synthesize arrowDown;
@synthesize arrowRight;
@synthesize arrowLeft;

@synthesize eViewModeType;

#define MAX_ZOOM_RATIO_NUM 12
#define ZOOM_RATIO 1.1
#define HIDE_UI_INTERVAL (3)
#define WAIT_FEEDBACK_INTERVAL 10

- (id)initWithServerManager:(ServerManager *)smgr CurrentCam:(int) chIndex; {
    serverMgr = smgr;
    iNumberOfCamera = [serverMgr getCameraTotalNum];
    iCurrentCamera = chIndex;
    if (IS_IPAD) {
        if ((self = [super initWithNibName:@"SingleCamViewController_iPad" bundle:nil])) {
            bNoChangePage = YES; // jerrylu, 2013/01/04
        }
    }
    else {
        if ((self = [super initWithNibName:@"SingleCamViewController" bundle:nil])) {
            bNoChangePage = YES; // jerrylu, 2013/01/04
        }
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    return self;
}

- (void)initScrollView {
    int width = scrollView.frame.size.width;
    int height = scrollView.frame.size.height;
    
    // init the size of scrollview
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(width * iNumberOfCamera, height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = NO;
    scrollView.delegate = self;
    
    // init the array of page controllers
    viewControllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < iNumberOfCamera; i++) {
        [viewControllers addObject:[NSNull null]];
    }
    
    // load 3 pages
    [self loadScrollViewWithCameraIndex:iCurrentCamera - 1];
    [self loadScrollViewWithCameraIndex:iCurrentCamera];
    [self loadScrollViewWithCameraIndex:iCurrentCamera + 1];
    
    //important, set offset
    CGPoint offsetPoint = CGPointZero;
    offsetPoint.x = iCurrentCamera * width;
    [scrollView setContentOffset:offsetPoint];
    
    if (pagecontrol == nil) {
        pagecontrol = [[UIPageControl alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - PAGECONTROL_WIDTH/2, [[UIScreen mainScreen] bounds].size.height - 120, PAGECONTROL_WIDTH, PAGECONTROL_HEIGHT)];
    }
    pagecontrol.userInteractionEnabled = NO;
    if (iNumberOfCamera > 2) {
        pagecontrol.numberOfPages = 3;
        if (iCurrentCamera == 0)
            pagecontrol.currentPage = 0;
        else if (iCurrentCamera == iNumberOfCamera-1)
            pagecontrol.currentPage = 2;
        else
            pagecontrol.currentPage = 1;
    }
    else if (iNumberOfCamera == 2) {
        pagecontrol.numberOfPages = 2;
        if (iCurrentCamera == 0)
            pagecontrol.currentPage = 0;
        else
            pagecontrol.currentPage = 1;
    }
    else if (iNumberOfCamera == 1) {
        pagecontrol.numberOfPages = 1;
        pagecontrol.currentPage = 0;
    }
    
    [self.view addSubview:pagecontrol];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	readyToPTZ = NO;
	eActionType = eAct_None;
	eViewModeType = eViewMode_None;
    
    ePreViewModeType = eViewMode_None;
    connectstatus = FALSE;
    
    ePlayType = ePLAY_PAUSE;
    if ([serverMgr isSupportSecondProfile:iCurrentCamera]) {
                    eProfile = eLow;
    }
    else
        eProfile = eOriginal;
    
#if RENDER_BY_OPENGL
    zoomLevel = 1.0;
#else
    zoomLevel = 0;
#endif
	offsetPos = CGPointZero;
	imgSize = CGSizeZero;
	presetSheet = nil;
    presetController = nil;
    snapshotSheet = nil;
    snapshotController = nil;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    dualRecordSheet = nil;
    dualRecordController = nil;
#endif
    isHoldButton = NO;
    
    liveviewaudioenable = NO;
    playbackaudioenable = NO;
    talkenable = NO;
    
    [self initScrollView];
    imageview = (SinglePageViewController_1X1 *)[viewControllers objectAtIndex:iCurrentCamera];
    
    // jerrylu, 2012/07/12
    m_errorCondition = [[NSCondition alloc] init];
    
    pPlaybackController = [[PlaybackViewController alloc] initWithNibName:@"PlaybackViewController" bundle:nil];
    pPlaybackController.singleViewDelegate = self;
    
    // Brosso
    pTimeLineViewController = [[QueryRecordViewController alloc] initWithNibName:@"QueryRexordViewController" bundle:nil];
    [pTimeLineViewController setSingleViewDeledate:self];
    
    
    if (IS_IPAD) { // jerrylu, 2012/06/08
        UINavigationController * navigation = [[UINavigationController alloc] initWithRootViewController:pPlaybackController];
        pPlaybackPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
        pPlaybackPopover.delegate = self;
        [navigation release];
    }
    
    pPbMenuController = [[PlaybackMenuViewController alloc] initWithStyle:UITableViewStyleGrouped]; // jerrylu, 2012/06/04
    pPbMenuController.singleViewDelegate = self;
    pPbMenuController.serverMgr = serverMgr;
    pPbMenuController.chIndex = iCurrentCamera;
    
    if (IS_IPAD) { // jerrylu, 2012/06/08
        UINavigationController * navigation = [[UINavigationController alloc] initWithRootViewController:pPbMenuController];
        pPlaybackMenuPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
        pPlaybackMenuPopover.delegate = self;
        [navigation release];
    }
    
    playbackSpeedList = [[NSArray alloc] initWithObjects:NSLocalizedString(@"playback speed 1/4X", nil), NSLocalizedString(@"playback speed 1/2X", nil), NSLocalizedString(@"playback speed 1X", nil), NSLocalizedString(@"playback speed 2X", nil), NSLocalizedString(@"playback speed 4X", nil), nil];
    playbackSpeedValueList = [[NSArray alloc] initWithObjects:@"0.25", @"0.5", @"1.0", @"2.0", @"4.0", nil];
    speedSheet = nil;
    speedSlider = nil;
    speedImageView = nil;
    playbackstate = kStateStopped;
    
	//add flash subview
    if (IS_IPAD)
        flashView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
    else
        flashView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
	flashView.backgroundColor = [UIColor whiteColor];
	flashView.alpha = 0;
	
    float toolbar_h = self.navigationController.toolbar.frame.size.height;
    float offset_h;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if (IS_IPAD && SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"8.0")) {
        if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
            offset_h = 7.8;
        } else {
            offset_h = 2;
        }
    }else {
        offset_h = 2;
    }
    mLayoutToolbar = [[UIToolbar alloc]initWithFrame: CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height - toolbar_h * offset_h, self.navigationController.toolbar.frame.size.width, toolbar_h)];
    [mLayoutToolbar setBarStyle:UIBarStyleBlack];
    
    [self.view setBackgroundColor:COLOR_BACKGROUND];
    
    //set action to enter modalview
    [self changeTONonePTZ];
    [imageview.mLabel setTextAlignment:NSTextAlignmentCenter];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopQueryThread)
                                                 name:NOTIFICATION_STOP_QUERY_THREAD
                                               object:[UIApplication sharedApplication]];
    
    [self addLayoutToolbar];
    [super viewDidLoad];
}

- (void)stopQueryThread {
    serverMgr = nil;
    
    if (m_checkPlaybackStatusThread != nil && [m_checkPlaybackStatusThread isExecuting]) {
        [m_checkPlaybackStatusThread cancel];
        while (![m_checkPlaybackStatusThread isFinished])
        {
            [NSThread sleepForTimeInterval:0.5];
        }
    }
    
    if (m_checkLiveViewStatusThread != nil && [m_checkLiveViewStatusThread isExecuting]) {
        [m_checkLiveViewStatusThread cancel];
        while (![m_checkLiveViewStatusThread isFinished])
        {
            [NSThread sleepForTimeInterval:0.5];
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
	/*if (parent != nil ) {
		[parent setSingleCam:nil];
	}
    */
    // jerrylu, 2012/07/12
    [m_errorCondition release];
    m_errorCondition = nil;
    
    [pPlaybackController release];
    pPlaybackController = nil; // jerrylu, 2012/04/20
    [pPbMenuController release];
    pPbMenuController = nil; // jerrylu, 2012/06/04
    
    if (playbackStartTime != nil) {
        [playbackStartTime release];
        playbackStartTime = nil;
    }
    if (playbackEndTime != nil) {
        [playbackEndTime release];
        playbackEndTime = nil;
    }
    if (playbackReverseEndTime != nil) {
        [playbackReverseEndTime release];
        playbackReverseEndTime = nil;
    }
    
    [playbackSpeedList release];
    playbackSpeedList = nil;
    [playbackSpeedValueList release];
    playbackSpeedValueList = nil;
    
    if (speedSlider != nil) {
        [speedSlider release];
        speedSlider = nil;
        [speedImageView release];
        speedImageView = nil;
    }
    
    scrollView = nil;
    [pagecontrol release];
    pagecontrol = nil;
    speedPicker = nil;
    [arrowDown release];
    arrowDown = nil;
    [arrowLeft release];
    arrowLeft = nil;
    [arrowRight release];
    arrowRight = nil;
    [arrowUp release];
    arrowUp = nil;
    
    [flashView release];
    flashView = nil;
    
    if (IS_IPAD) { // jerrylu, 2012/06/08
        [pPlaybackPopover release];
        pPlaybackPopover = nil;
        [pPlaybackSpeedPopover release];
        pPlaybackSpeedPopover = nil;
        [pPlaybackMenuPopover release];
        pPlaybackMenuPopover = nil;
        [pLiveViewMenuPopover release];
        pLiveViewMenuPopover = nil; // jerrylu, 2012/8/27
    }
    
	[viewControllers release];
    [scrollView release];
    
    if (audioWaitTimer != nil && [audioWaitTimer isValid]) {
        [audioWaitTimer invalidate];
        audioWaitTimer = nil;
    }
    if (talkWaitTimer != nil && [talkWaitTimer isValid]) {
        [talkWaitTimer invalidate];
        talkWaitTimer = nil;
    }
    if (profileWaitTimer != nil && [profileWaitTimer isValid]) {
        [profileWaitTimer invalidate];
        profileWaitTimer = nil;
    }
    if (playbackWaitTimer != nil && [playbackWaitTimer isValid]) {
        [playbackWaitTimer invalidate];
        playbackWaitTimer = nil;
    }
    
    [mLayoutToolbar release];
    
    // Brosso
    [self hideTimeLine];
    [pTimeLineViewController release];
    
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self hideUIWithValue:NO];
    [self resetHideTimer]; // jerrylu, 2012/10/18
    
    //adjust navigationBar, toolbar and status bar translucent
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    [self.navigationController.navigationBar setTranslucent:YES];
    //[self.navigationController.navigationBar setAlpha:0.300];
    
    [[[self navigationController] toolbar] setTranslucent:YES];
    
    //apply to full screen layout
    //not use this will make gap 
    [self setWantsFullScreenLayout:YES];
    
    m_checkPlaybackStatusThread = [[NSThread alloc] initWithTarget:self selector:@selector(checkPlaybackStatus) object:NULL];
    [m_checkPlaybackStatusThread start];
    
    m_checkLiveViewStatusThread = [[NSThread alloc] initWithTarget:self selector:@selector(checkLiveViewStatus) object:NULL];
    [m_checkLiveViewStatusThread start];
    
    [self changeUILayout];
    [self refreshToolBar]; // jerrylu, 2013/01/24, fix bug11747
    
#if RENDER_BY_OPENGL
    [imageview.image clearFrameBuffer];
    [imageview.image RenderToHardware:nil];
    [imageview.image setDefaultPic];
    [parent.slideView setImageUpdateOfCamIndex:iCurrentCamera flag:YES];
#endif
    
#if SHOW_FRAMERATE_FLAG
    if (showFRTimer == nil) {
        showFRTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                       target:self
                                                     selector:@selector(showframerate)
                                                     userInfo:nil
                                                      repeats:YES];
    }
#endif
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self changeUILayout];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (IS_IPAD) {
        [self dismissAllPopover];
    }
    else {
        if (speedSheet != nil) {
            [speedSheet dismissWithClickedButtonIndex:0 animated:NO];
            speedSheet = nil;
        }
        
        if (speedSlider != nil) {
            [speedSlider removeFromSuperview];
            [speedImageView removeFromSuperview];
        }
    }
    
    if (presetSheet != nil) {
		[presetSheet dismissWithClickedButtonIndex:0 animated:NO];
		presetSheet = nil;
	}
    if (presetController != nil) {
        [presetController dismissViewControllerAnimated:NO completion:nil];
        presetController = nil;
    }
    if (snapshotSheet != nil) {
		[snapshotSheet dismissWithClickedButtonIndex:0 animated:NO];
		snapshotSheet = nil;
	}
    if (snapshotController != nil) {
        [snapshotController dismissViewControllerAnimated:NO completion:nil];
        snapshotController = nil;
    }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (dualRecordSheet != nil) {
        [dualRecordSheet dismissWithClickedButtonIndex:0 animated:NO];
        dualRecordSheet = nil;
    }
    if (dualRecordController != nil) {
        [dualRecordController dismissViewControllerAnimated:NO completion:nil];
        dualRecordController = nil;
    }
#endif
    //adjust navigationBar, toolbar and status bar translucent
    [[[self navigationController] toolbar] setTranslucent:NO];
    
    //apply to full screen layout
    [self setWantsFullScreenLayout:FALSE];
    
    [self stopHideTimer];
    [self hideUIWithValue:NO];
    
    if ([m_checkPlaybackStatusThread isExecuting]) {
        [m_checkPlaybackStatusThread cancel]; 
        while (![m_checkPlaybackStatusThread isFinished]) 
        {
            [NSThread sleepForTimeInterval:0.5];
        }
    }
    [m_checkPlaybackStatusThread release];
    m_checkPlaybackStatusThread = nil;
    
    if ([m_checkLiveViewStatusThread isExecuting]) {
        [m_checkLiveViewStatusThread cancel]; 
        while (![m_checkLiveViewStatusThread isFinished]) 
        {
            [NSThread sleepForTimeInterval:0.5];
        }
    }
    [m_checkLiveViewStatusThread release];
    m_checkLiveViewStatusThread = nil;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (IS_IPAD) {
            [pPlaybackPopover dismissPopoverAnimated:YES];
            [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
            [pPlaybackMenuPopover dismissPopoverAnimated:YES];
            [pLiveViewMenuPopover dismissPopoverAnimated:YES];
        }
        
        if (presetSheet != nil) {
            [presetSheet dismissWithClickedButtonIndex:0 animated:YES];
            presetSheet = nil;
        }
        if (presetController != nil) {
            [presetController dismissViewControllerAnimated:YES completion:nil];
            presetController = nil;
        }
        if (snapshotSheet != nil) {
            [snapshotSheet dismissWithClickedButtonIndex:0 animated:YES];
            snapshotSheet = nil;
        }
        if (snapshotController != nil) {
            [snapshotController dismissViewControllerAnimated:YES completion:nil];
            snapshotController = nil;
        }
        if (speedSlider != nil && [speedSlider superview] != nil) {
            [speedSlider removeFromSuperview];
            [speedImageView removeFromSuperview];
        }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
        if (dualRecordSheet != nil) {
            [dualRecordSheet dismissWithClickedButtonIndex:0 animated:YES];
            dualRecordSheet = nil;
        }
        if (dualRecordController != nil) {
            [dualRecordController dismissViewControllerAnimated:YES completion:nil];
            dualRecordController = nil;
        }
#endif
        // jerrylu, 2012/04/30, refresh toolbar
        [self refreshToolBar];
        [self refreshLayoutToolbar];
        
        bNoChangePage = YES;
        
        [self changeUILayout];
        
        [parent.slideView setImageUpdateOfCamIndex:iCurrentCamera flag:YES];
        
        [self.view setNeedsLayout];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
    {
        [parent.slideView setImageUpdateOfCamIndex:iCurrentCamera flag:YES];
    }];
    
    return;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    if (IS_IPAD) {
        [pPlaybackPopover dismissPopoverAnimated:YES];
        [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
        [pPlaybackMenuPopover dismissPopoverAnimated:YES];
        [pLiveViewMenuPopover dismissPopoverAnimated:YES];
    }
    
	if (presetSheet != nil) {
		[presetSheet dismissWithClickedButtonIndex:0 animated:YES];
		presetSheet = nil;
	}
    if (presetController != nil) {
        [presetController dismissViewControllerAnimated:YES completion:nil];
        presetController = nil;
    }
    if (snapshotSheet != nil) {
		[snapshotSheet dismissWithClickedButtonIndex:0 animated:YES];
		snapshotSheet = nil;
	}
    if (snapshotController != nil) {
        [snapshotController dismissViewControllerAnimated:YES completion:nil];
        snapshotController = nil;
    }
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (dualRecordSheet != nil) {
        [dualRecordSheet dismissWithClickedButtonIndex:0 animated:YES];
        dualRecordSheet = nil;
    }
    if (dualRecordController != nil) {
        [dualRecordController dismissViewControllerAnimated:YES completion:nil];
        dualRecordController = nil;
    }
#endif
    // jerrylu, 2012/04/30, refresh toolbar
    [self refreshToolBar];
    [self refreshLayoutToolbar];
    
    bNoChangePage = YES;
    
    [self changeUILayout];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)changeUILayout {
    UIInterfaceOrientation to;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            to = UIInterfaceOrientationLandscapeLeft;
        }
        else {
            to = UIInterfaceOrientationPortrait;
        }
        
        flashView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        
        if (UIInterfaceOrientationIsPortrait(to)) {
            pagecontrol.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - PAGECONTROL_WIDTH/2, [[UIScreen mainScreen] bounds].size.height - 120, PAGECONTROL_WIDTH, PAGECONTROL_HEIGHT);
            
            if (IS_IPAD) {
                arrowUp.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 24, [[UIScreen mainScreen] bounds].size.height/2 - 250, 48, 48);
                arrowDown.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 24, [[UIScreen mainScreen] bounds].size.height/2 + 202, 48, 48);
                arrowLeft.frame = CGRectMake(50, [[UIScreen mainScreen] bounds].size.height/2 - 24, 48, 48);
                arrowRight.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 98, [[UIScreen mainScreen] bounds].size.height/2 - 24, 48, 48);
            }
            else {
                arrowUp.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 16, [[UIScreen mainScreen] bounds].size.height/2 - 110, 32, 32);
                arrowDown.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 16, [[UIScreen mainScreen] bounds].size.height/2 + 78, 32, 32);
                arrowLeft.frame = CGRectMake(20, [[UIScreen mainScreen] bounds].size.height/2 - 16, 32, 32);
                arrowRight.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 52, [[UIScreen mainScreen] bounds].size.height/2 - 16, 32, 32);
            }
        }
        else {
            int offset;
            if (IS_IPAD) {
                offset = 130;
                arrowUp.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 24, 60, 48, 48);
                arrowDown.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 24, [[UIScreen mainScreen] bounds].size.height - 108, 48, 48);
                arrowLeft.frame = CGRectMake(80, [[UIScreen mainScreen] bounds].size.height/2 - 24, 48, 48);
                arrowRight.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 128, [[UIScreen mainScreen] bounds].size.height/2 - 24, 48, 48);
            }
            else {
                offset = 90;
                arrowUp.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 16, 20, 32, 32);
                arrowDown.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 16, [[UIScreen mainScreen] bounds].size.height - 52, 32, 32);
                arrowLeft.frame = CGRectMake(70, [[UIScreen mainScreen] bounds].size.height/2 - 16, 32, 32);
                arrowRight.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 102, [[UIScreen mainScreen] bounds].size.height/2 - 16, 32, 32);
            }
            pagecontrol.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - PAGECONTROL_WIDTH/2, [[UIScreen mainScreen] bounds].size.height - offset, PAGECONTROL_WIDTH, PAGECONTROL_HEIGHT);
        }
    }
    else {
        to = self.interfaceOrientation;
        
        if (UIInterfaceOrientationIsPortrait(to)) {
            flashView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
            pagecontrol.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - PAGECONTROL_WIDTH/2, [[UIScreen mainScreen] bounds].size.height - 120, PAGECONTROL_WIDTH, PAGECONTROL_HEIGHT);
            
            if (IS_IPAD) {
                arrowUp.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 24, [[UIScreen mainScreen] bounds].size.height/2 - 250, 48, 48);
                arrowDown.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 24, [[UIScreen mainScreen] bounds].size.height/2 + 202, 48, 48);
                arrowLeft.frame = CGRectMake(50, [[UIScreen mainScreen] bounds].size.height/2 - 24, 48, 48);
                arrowRight.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 98, [[UIScreen mainScreen] bounds].size.height/2 - 24, 48, 48);
            }
            else {
                arrowUp.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 16, [[UIScreen mainScreen] bounds].size.height/2 - 110, 32, 32);
                arrowDown.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 16, [[UIScreen mainScreen] bounds].size.height/2 + 78, 32, 32);
                arrowLeft.frame = CGRectMake(20, [[UIScreen mainScreen] bounds].size.height/2 - 16, 32, 32);
                arrowRight.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 52, [[UIScreen mainScreen] bounds].size.height/2 - 16, 32, 32);
            }
        }
        else {
            flashView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width);
            int offset;
            if (IS_IPAD) {
                offset = 130;
                arrowUp.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height/2 - 24, 60, 48, 48);
                arrowDown.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height/2 - 24, [[UIScreen mainScreen] bounds].size.width - 108, 48, 48);
                arrowLeft.frame = CGRectMake(80, [[UIScreen mainScreen] bounds].size.width/2 - 24, 48, 48);
                arrowRight.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height - 128, [[UIScreen mainScreen] bounds].size.width/2 - 24, 48, 48);
            }
            else {
                offset = 90;
                arrowUp.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height/2 - 16, 20, 32, 32);
                arrowDown.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height/2 - 16, [[UIScreen mainScreen] bounds].size.width - 52, 32, 32);
                arrowLeft.frame = CGRectMake(70, [[UIScreen mainScreen] bounds].size.width/2 - 16, 32, 32);
                arrowRight.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height - 102, [[UIScreen mainScreen] bounds].size.width/2 - 16, 32, 32);
            }
            pagecontrol.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height/2 - PAGECONTROL_WIDTH/2, [[UIScreen mainScreen] bounds].size.width - offset, PAGECONTROL_WIDTH, PAGECONTROL_HEIGHT);
        }
    }
    
    int width = scrollView.frame.size.width;
	int height = scrollView.frame.size.height;
	for (int idx = 0; idx < iNumberOfCamera; idx++) {
		SinglePageViewController_1X1 * controller = [viewControllers objectAtIndex:idx];
		
        if ((NSNull *) controller != [NSNull null]) {
            controller.view.frame = CGRectMake(width * idx, 0, width, height);
            scrollView.contentSize = CGSizeMake(width * iNumberOfCamera, height);
			[controller changeLayout:to];
		}
	}
    
    //important, set offset
    CGPoint offsetPoint = CGPointZero;
    offsetPoint.x = iCurrentCamera * width;
    [scrollView setContentOffset:offsetPoint];
}

- (void)deallocSingleCam:(BOOL)withAnimation {
    liveviewaudioenable = NO; //jerrylu, 2012/09/05
    
    //reset bar status here to get correct frame width height later
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setAlpha:1.000];
    
	[parent setSingleCam:nil];
    [parent backFromSingleView];
    
    if (audioWaitTimer != nil && [audioWaitTimer isValid]) {
        [audioWaitTimer invalidate];
        audioWaitTimer = nil;
    }
    if (talkWaitTimer != nil && [talkWaitTimer isValid]) {
        [talkWaitTimer invalidate];
        talkWaitTimer = nil;
    }
    if (profileWaitTimer != nil && [profileWaitTimer isValid]) {
        [profileWaitTimer invalidate];
        profileWaitTimer = nil;
    }
    if (playbackWaitTimer != nil && [playbackWaitTimer isValid]) {
        [playbackWaitTimer invalidate];
        playbackWaitTimer = nil;
    }
    
    [serverMgr setPresetDelegateByChIndex:iCurrentCamera delegate:nil]; // jerrylu, 2012/12/14, fix bug10777
    [serverMgr setPlaybackDelegate:nil]; // jerrylu, 2012/11/29, fix bug7531
    
#if SHOW_FRAMERATE_FLAG
    if (showFRTimer != nil && [showFRTimer isValid]) {
        [showFRTimer invalidate];
        showFRTimer = nil;
    }
#endif
    [self.navigationController popViewControllerAnimated:withAnimation];
}

- (void)onBackBtnTap {
    [self deallocSingleCam:NO];
    
    if (changeLayoutDelegate != nil) {
        if ([changeLayoutDelegate respondsToSelector:@selector(backFromBackBtn:)]) {
            [changeLayoutDelegate backFromBackBtn:iCurrentCamera];
        }
    }
}

- (void)onEditMyViewTap {
    [self deallocSingleCam:NO];
    
    if (changeLayoutDelegate != nil) {
        if ([changeLayoutDelegate respondsToSelector:@selector(singleViewEditBtnPressed)]) {
            [changeLayoutDelegate singleViewEditBtnPressed];
        }
    }
}

- (void)changeTONonePTZ {
#if DEBUG_LOG
    NSLog(@"changeTONonePTZ");
#endif
    [scrollView setScrollEnabled:YES];
    [pagecontrol setHidden:NO]; // jerrylu, 2012/10/01
    
    if (![imageview.mIndicator isAnimating])
        [imageview.mIndicator startAnimating];
#if RENDER_BY_OPENGL
    [imageview.image clearFrameBuffer];
    [imageview.image RenderToHardware:nil];
    [imageview.image setDefaultPic];
#else
    UIImage * imgDefault = [UIImage imageNamed:@"novideo320x240_noword.png"];
    CGImageRef imgRef = imgDefault.CGImage;
    imgSize.width = CGImageGetWidth(imgRef);
    imgSize.height = CGImageGetHeight(imgRef);
    [imageview.image setImage:imgDefault];
#endif
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(onBackBtnTap) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(onBackBtnTap)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
	}
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
	[backButton release];
    
#if FUNC_PUSH_NOTIFICATION_SUPPORT
    UIBarButtonItem *menuButton;
    if (IS_IPAD)
        menuButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Menu", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showLiveViewMenuPopover)];
    else
        menuButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Menu", nil) style:UIBarButtonItemStylePlain target:self action:@selector(liveviewMenuPressed)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [menuButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [menuButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    if (!serverMgr.isMyViewMode) {
        UIImage *cameraListImage;
        UIButton *cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        else
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        cButton.bounds = CGRectMake(0, 0, cameraListImage.size.width, cameraListImage.size.height);
        [cButton setImage:cameraListImage forState:UIControlStateNormal];
        [cButton addTarget:self action:@selector(liveviewCameraListPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * cameraListButton = [[UIBarButtonItem alloc] initWithCustomView:cButton];
        NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects:menuButton, cameraListButton, nil];
        [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
        [cameraListButton release];
    }
    else {
        UIBarButtonItem * addButton = [[UIBarButtonItem alloc]
                                         initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                         target:self
                                         action:@selector(onEditMyViewTap)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [addButton setTintColor:COLOR_NAVBUTTONTINT];
        }
        else {
            [addButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
        }
        NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects:menuButton, addButton, nil];
        [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
        
    }
    
    [menuButton release];
#else
    //set DIO Page Button
    UIImage * image;
    NSMutableArray *diDeviceList = [serverMgr getDIDeviceNameByChIndex:iCurrentCamera];
    NSMutableArray *doDeviceList = [serverMgr getDODeviceNameByChIndex:iCurrentCamera];
    if ([diDeviceList count] != 0 || [doDeviceList count] != 0) {
        if (IS_IPAD)
            image = [UIImage imageNamed:@"Btn_DIO_iPad.png"];
        else
            image = [UIImage imageNamed:@"Btn_DIO.png"];
    }
    else {
        if (IS_IPAD)
            image = [UIImage imageNamed:@"Btn_DIO_Dis_iPad.png"];
        else
            image = [UIImage imageNamed:@"Btn_DIO_Dis.png"];
    }
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
    [button setImage:image forState:UIControlStateNormal];
    
    if (IS_IPAD)
        [button addTarget:self action:@selector(showLiveViewMenuPopover) forControlEvents:UIControlEventTouchUpInside];
    else
        [button addTarget:self action:@selector(liveviewMenuPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * Btn_DIO = [[UIBarButtonItem alloc] initWithCustomView:button];
    if (!serverMgr.isMyViewMode) {
        UIImage *cameraListImage;
        UIButton *cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        else
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        cButton.bounds = CGRectMake(0, 0, cameraListImage.size.width, cameraListImage.size.height);
        [cButton setImage:cameraListImage forState:UIControlStateNormal];
        [cButton addTarget:self action:@selector(liveviewCameraListPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * cameraListButton = [[UIBarButtonItem alloc] initWithCustomView:cButton];
        NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects:Btn_DIO, cameraListButton, nil];
        [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
        [cameraListButton release];
    }
    else {
        self.navigationItem.rightBarButtonItem = Btn_DIO;
    }
    [Btn_DIO release];
    
    if ([diDeviceList count] != 0 || [doDeviceList count] != 0) {
        Btn_DIO.enabled = YES;
    }
    else {
        Btn_DIO.enabled = NO;
    }
#endif // FUNC_PUSH_NOTIFICATION_SUPPORT
    
	[imageview.mLabel setText:NSLocalizedString(@"Digital PTZ Mode", nil)];
	eViewModeType = eViewMode_None;
    eActionType = eAct_None;
    [self refreshToolBar];
    
    [self hideArrow:YES];
}

- (void)changeTOPhyPTZ {
#if DEBUG_LOG
    NSLog(@"changeTOPhyPTZ");
#endif
    [scrollView setScrollEnabled:NO];
    [pagecontrol setHidden:YES];
#if RENDER_BY_OPENGL
    [imageview.image enablePanSwipe];
#endif
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(changeTODigitalPTZ) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil)
																	style:UIBarButtonItemStylePlain 
																   target:self 
																   action:@selector(changeTODigitalPTZ)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
	}
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
	[backButton release];    
    
	[imageview.mLabel setText:NSLocalizedString(@"PTZ Mode", nil)];
#if RENDER_BY_OPENGL
    [imageview.image lockZoom];
#endif
	eViewModeType = eViewMode_PhysicalPTZ;
    eActionType = eAct_None;
    [self refreshToolBar];
    
    [self hideArrow:NO];
}

- (void)changeTODigitalPTZ {
#if DEBUG_LOG
    NSLog(@"changeTODigitalPTZ");
#endif
#if RENDER_BY_OPENGL
    if (zoomLevel > 1.0) // jerrylu, 2012/10/17
    {
        [scrollView setScrollEnabled:NO];
        [pagecontrol setHidden:YES];
        [imageview.image enablePanSwipe];
    }
    else {
        [scrollView setScrollEnabled:YES];
        [pagecontrol setHidden:NO];
        [imageview.image disablePanSwipe];
    }
#else
    if (zoomLevel > 0) // jerrylu, 2012/10/17
    {
        [scrollView setScrollEnabled:NO];
        [pagecontrol setHidden:YES];
    }
    else {
        [scrollView setScrollEnabled:YES];
        [pagecontrol setHidden:NO];
    }
#endif
    
    if (![imageview.mConnectionLabel isHidden])
        [imageview.mConnectionLabel setHidden:YES];
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(onBackBtnTap) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(onBackBtnTap)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
	}
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
	[backButton release];
    
#if FUNC_PUSH_NOTIFICATION_SUPPORT
    UIBarButtonItem *menuButton;
    if (IS_IPAD)
        menuButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Menu", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showLiveViewMenuPopover)];
    else
        menuButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Menu", nil) style:UIBarButtonItemStylePlain target:self action:@selector(liveviewMenuPressed)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [menuButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [menuButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    if (!serverMgr.isMyViewMode) {
        UIImage *cameraListImage;
        UIButton *cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        else
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        cButton.bounds = CGRectMake(0, 0, cameraListImage.size.width, cameraListImage.size.height);
        [cButton setImage:cameraListImage forState:UIControlStateNormal];
        [cButton addTarget:self action:@selector(liveviewCameraListPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * cameraListButton = [[UIBarButtonItem alloc] initWithCustomView:cButton];
        NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects:menuButton, cameraListButton, nil];
        [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
        [cameraListButton release];
    }
    else {
        self.navigationItem.rightBarButtonItem = menuButton;
    }
    
    [menuButton release];
#else
    //set DIO Page Button
    UIImage * image;
    NSMutableArray *diDeviceList = [serverMgr getDIDeviceNameByChIndex:iCurrentCamera];
    NSMutableArray *doDeviceList = [serverMgr getDODeviceNameByChIndex:iCurrentCamera];
    if ([diDeviceList count] != 0 || [doDeviceList count] != 0) {
        if (IS_IPAD)
            image = [UIImage imageNamed:@"Btn_DIO_iPad.png"];
        else
            image = [UIImage imageNamed:@"Btn_DIO.png"];
    }
    else {
        if (IS_IPAD)
            image = [UIImage imageNamed:@"Btn_DIO_Dis_iPad.png"];
        else
            image = [UIImage imageNamed:@"Btn_DIO_Dis.png"];
    }
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
    [button setImage:image forState:UIControlStateNormal];
    
    if (IS_IPAD)
        [button addTarget:self action:@selector(showLiveViewMenuPopover) forControlEvents:UIControlEventTouchUpInside];
    else
        [button addTarget:self action:@selector(liveviewMenuPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * Btn_DIO = [[UIBarButtonItem alloc] initWithCustomView:button];
    if (!serverMgr.isMyViewMode) {
        UIImage *cameraListImage;
        UIButton *cButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        else
            cameraListImage = [UIImage imageNamed:@"Switch_32x32_nor.png"];
        cButton.bounds = CGRectMake(0, 0, cameraListImage.size.width, cameraListImage.size.height);
        [cButton setImage:cameraListImage forState:UIControlStateNormal];
        [cButton addTarget:self action:@selector(liveviewCameraListPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * cameraListButton = [[UIBarButtonItem alloc] initWithCustomView:cButton];
        NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects:Btn_DIO, cameraListButton, nil];
        [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
        [cameraListButton release];
    }
    else {
        self.navigationItem.rightBarButtonItem = Btn_DIO;
    }
    [Btn_DIO release];
    
    if ([diDeviceList count] != 0 || [doDeviceList count] != 0) {
        Btn_DIO.enabled = YES;
    }
    else {
        Btn_DIO.enabled = NO;
    }
#endif // FUNC_PUSH_NOTIFICATION_SUPPORT
    
	[imageview.mLabel setText:NSLocalizedString(@"Digital PTZ Mode", nil)];
#if RENDER_BY_OPENGL
    [imageview.image unlockZoom];
#endif
	
    eViewModeType = eViewMode_DigitalPTZ;
    eActionType = eAct_None;
    [self refreshToolBar];
    
    [self hideArrow:YES];
}

- (void)changeTOPlaybackMode {
#if DEBUG_LOG
    NSLog(@"changeTOPlaybackMode");
#endif
    [scrollView setScrollEnabled:NO];
    [pagecontrol setHidden:YES]; // jerrylu, 2012/10/01
    
    if (![imageview.mConnectionLabel isHidden])
        [imageview.mConnectionLabel setHidden:YES];
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(playbackEnd) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(playbackEnd)];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    UIBarButtonItem *menuButton;
    if (IS_IPAD)
        menuButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Menu", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showPlaybackMenuPopover)];
    else
        menuButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Menu", nil) style:UIBarButtonItemStylePlain target:self action:@selector(playbackMenuPressed)];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [menuButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [menuButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.rightBarButtonItem = menuButton;
	[backButton release];
    [menuButton release];
    
    eViewModeType = eViewMode_Playback;
    eActionType = eAct_None;
    [self refreshToolBar];
    
    if (playbackWaitTimer != nil) {
        [playbackWaitTimer invalidate];
        playbackWaitTimer = nil;
    }
}

- (void)changeTOPlaybackDefaultMode {
#if DEBUG_LOG
    NSLog(@"changeTOPlaybackDefaultMode");
#endif
    [scrollView setScrollEnabled:NO];
    [pagecontrol setHidden:YES]; // jerrylu, 2012/10/01
    
    if (![imageview.mIndicator isAnimating]) {
        [imageview.mIndicator startAnimating];
    }
    
    if (![imageview.mConnectionLabel isHidden])
        [imageview.mConnectionLabel setHidden:YES];

#if RENDER_BY_OPENGL
    [imageview.image clearFrameBuffer];
    [imageview.image RenderToHardware:nil];
    [imageview.image setDefaultPic];
#else
    UIImage * imgDefault = [UIImage imageNamed:@"novideo320x240_noword.png"];
    CGImageRef imgRef = imgDefault.CGImage;
    imgSize.width = CGImageGetWidth(imgRef);
    imgSize.height = CGImageGetHeight(imgRef);
    [imageview.image setImage:imgDefault];
#endif
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(playbackEnd) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(playbackEnd)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
    [backButton release];
    self.navigationItem.rightBarButtonItem = nil;
    
    [imageview.mLabel setText:NSLocalizedString(@"Playback Mode", nil)];
    eViewModeType = eViewMode_PlaybackDefault;
    eActionType = eAct_None;
    [self refreshToolBar];
    
    if (playbackWaitTimer == nil) {
        playbackWaitTimer = [NSTimer scheduledTimerWithTimeInterval:WAIT_FEEDBACK_INTERVAL
                                                              target:self
                                                            selector:@selector(playbackWaitingAlert)
                                                            userInfo:nil
                                                             repeats:NO];
    }
    
    if (IS_IPAD)
        [pPlaybackPopover dismissPopoverAnimated:YES];
}

// Brosso - TimeLine
- (void)showTimeLine {
    [pTimeLineViewController.view setFrame:CGRectMake(0, mLayoutToolbar.frame.origin.y - 30, self.view.frame.size.width, 60)];
    if (self.view != pTimeLineViewController.view.superview) {
        [self.view addSubview:pTimeLineViewController.view];
    }
    if (self != pTimeLineViewController.parentViewController) {
        [self addChildViewController:pTimeLineViewController];
    }
}

- (void)hideTimeLine {
    if (self.view == pTimeLineViewController.view.superview) {
        [pTimeLineViewController.view removeFromSuperview];
    }
    if (self == pTimeLineViewController.parentViewController) {
        [pTimeLineViewController removeFromParentViewController];
    }
}

- (void)refreshToolBar {
    NSArray *buttonArray;
    UIImage *image;
    
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    switch (eViewModeType) {  
        case eViewMode_None:
        {
            if (IS_IPAD)
                image = [UIImage imageNamed:@"folder_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"folder_32x32_nor.png"];
            
            UIButton *playbackbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            playbackbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playbackbutton setImage:image forState:UIControlStateNormal];
            
            if (IS_IPAD)
                [playbackbutton addTarget:self action:@selector(showPlaybackPopover) forControlEvents:UIControlEventTouchUpInside];
            else
                [playbackbutton addTarget:self action:@selector(playbackPressed) forControlEvents:UIControlEventTouchUpInside];
            playbackButton = [[UIBarButtonItem alloc] initWithCustomView:playbackbutton];
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"E01_PTZ_48x48_disable.png"];
            else
                image = [UIImage imageNamed:@"E01_PTZ_32x32_disable.png"];
            UIButton *ptzbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            ptzbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [ptzbutton setImage:image forState:UIControlStateNormal];
            [ptzbutton addTarget:self action:@selector(ptzPressed) forControlEvents:UIControlEventTouchUpInside];
            ptzButton = [[UIBarButtonItem alloc] initWithCustomView:ptzbutton];
            ptzButton.enabled = NO;

            if (IS_IPAD)
                image = [UIImage imageNamed:@"audio_disable_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"audio_disable_32x32_nor.png"];
            UIButton *audiobutton = [UIButton buttonWithType:UIButtonTypeCustom];
            audiobutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [audiobutton setImage:image forState:UIControlStateNormal];
            [audiobutton addTarget:self action:@selector(audioPressed) forControlEvents:UIControlEventTouchUpInside];
            audioButton = [[UIBarButtonItem alloc] initWithCustomView:audiobutton];
            audioButton.enabled = NO;
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"mic_disable_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"mic_disable_32x32_nor.png"];
            UIButton *talkbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            talkbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [talkbutton setImage:image forState:UIControlStateNormal];
            [talkbutton addTarget:self action:@selector(talkPressed) forControlEvents:UIControlEventTouchUpInside];
            talkButton = [[UIBarButtonItem alloc] initWithCustomView:talkbutton];
            talkButton.enabled = NO;
            
            if (eProfile == eOriginal) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"Resolution_H_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"Resolution_H_32x32_nor.png"];
            }
            else {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"Resolution_L_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"Resolution_L_32x32_nor.png"];
            }
            UIButton *profilebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            profilebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [profilebutton setImage:image forState:UIControlStateNormal];
            [profilebutton addTarget:self action:@selector(profilechange) forControlEvents:UIControlEventTouchUpInside];
            megapixelButton = [[UIBarButtonItem alloc] initWithCustomView:profilebutton];
            if (![serverMgr isSupportSecondProfile:iCurrentCamera])
                megapixelButton.enabled = NO;

            if (IS_IPAD)
                image = [UIImage imageNamed:@"E02_Snapshot_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"E02_Snapshot_32x32_nor.png"];
            UIButton *snapshotbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            snapshotbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [snapshotbutton setImage:image forState:UIControlStateNormal];
            [snapshotbutton addTarget:self action:@selector(snapshot) forControlEvents:UIControlEventTouchUpInside];
            snapshotButton = [[UIBarButtonItem alloc] initWithCustomView:snapshotbutton];
            snapshotbutton.enabled = NO;
            
            buttonArray = [[NSArray alloc] initWithObjects:flexItem, 
#if FUNC_PLAYBACK_SUPPORT
                           playbackButton, flexItem, 
#endif
                           ptzButton, flexItem, 
#if FUNC_AUDIO_SUPPORT
                           audioButton, flexItem,
                           talkButton, flexItem,
#endif
#if FUNC_MEGAPIXEL_SUPPORT
                           megapixelButton, flexItem,
#endif
                           snapshotButton, flexItem, nil];
            
            [self setToolbarItems:buttonArray];
            
            [buttonArray release];
            
            [playbackButton release];
            [ptzButton release];
            [audioButton release];
            [talkButton release];
            [megapixelButton release];
            [snapshotButton release];
            break;
        }
        case eViewMode_DigitalPTZ:
        {
            if (IS_IPAD)
                image = [UIImage imageNamed:@"folder_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"folder_32x32_nor.png"];
            UIButton *playbackbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            playbackbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playbackbutton setImage:image forState:UIControlStateNormal];
            
            if (IS_IPAD)
                [playbackbutton addTarget:self action:@selector(showPlaybackPopover) forControlEvents:UIControlEventTouchUpInside];
            else
                [playbackbutton addTarget:self action:@selector(playbackPressed) forControlEvents:UIControlEventTouchUpInside];
            playbackButton = [[UIBarButtonItem alloc] initWithCustomView:playbackbutton];
            
            if (ptzCap) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"E01_PTZ_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"E01_PTZ_32x32_nor.png"];
            }
            else {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"E01_PTZ_48x48_disable.png"];
                else
                    image = [UIImage imageNamed:@"E01_PTZ_32x32_disable.png"];
            }
            UIButton *ptzbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            ptzbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [ptzbutton setImage:image forState:UIControlStateNormal];
            [ptzbutton addTarget:self action:@selector(ptzPressed) forControlEvents:UIControlEventTouchUpInside];
            ptzButton = [[UIBarButtonItem alloc] initWithCustomView:ptzbutton];
            ptzButton.enabled = ptzCap;
            
            EServerType eServerType = [serverMgr getServerTypeByChIndex:iCurrentCamera];
            
            if (eServerType == eServer_Titan ||
                eServerType == eServer_NVRmini ||
                eServerType == eServer_NVRsolo ||
                eServerType == eServer_Crystal) {
                    audioCap = YES;
            }
            
            if (audioCap == NO) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"audio_disable_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"audio_disable_32x32_nor.png"];
            }
            else {
                if (liveviewaudioenable) {
                    if (IS_IPAD)
                        image = [UIImage imageNamed:@"audio_on_48x48_nor.png"];
                    else
                        image = [UIImage imageNamed:@"audio_on_32x32_nor.png"];
                }
                else {
                    if (IS_IPAD)
                        image = [UIImage imageNamed:@"audio_off_48x48_nor.png"];
                    else
                        image = [UIImage imageNamed:@"audio_off_32x32_nor.png"];
                }
            }
            UIButton *audiobutton = [UIButton buttonWithType:UIButtonTypeCustom];
            audiobutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [audiobutton setImage:image forState:UIControlStateNormal];
            [audiobutton addTarget:self action:@selector(audioPressed) forControlEvents:UIControlEventTouchUpInside];
            audioButton = [[UIBarButtonItem alloc] initWithCustomView:audiobutton];
            audioButton.enabled = audioCap;
            
            if (talkCap == NO) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"mic_disable_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"mic_disable_32x32_nor.png"];
            }
            else {
                if (talkenable) {
                    if (IS_IPAD)
                        image = [UIImage imageNamed:@"mic_on_48x48_active.png"];
                    else
                        image = [UIImage imageNamed:@"mic_on_32x32_active.png"];
                }
                else {
                    if (IS_IPAD)
                        image = [UIImage imageNamed:@"mic_off_48x48_nor.png"];
                    else
                        image = [UIImage imageNamed:@"mic_off_32x32_nor.png"];
                }
            }
            UIButton *talkbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            talkbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [talkbutton setImage:image forState:UIControlStateNormal];
            [talkbutton addTarget:self action:@selector(talkPressed) forControlEvents:UIControlEventTouchUpInside];
            talkButton = [[UIBarButtonItem alloc] initWithCustomView:talkbutton];
            talkButton.enabled = talkCap;
            
            if (eProfile == eOriginal) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"Resolution_H_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"Resolution_H_32x32_nor.png"];
            }
            else {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"Resolution_L_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"Resolution_L_32x32_nor.png"];
            }
            UIButton *profilebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            profilebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [profilebutton setImage:image forState:UIControlStateNormal];
            [profilebutton addTarget:self action:@selector(profilechange) forControlEvents:UIControlEventTouchUpInside];
            megapixelButton = [[UIBarButtonItem alloc] initWithCustomView:profilebutton];
            if (![serverMgr isSupportSecondProfile:iCurrentCamera])
                megapixelButton.enabled = NO;
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"E02_Snapshot_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"E02_Snapshot_32x32_nor.png"];
            UIButton *snapshotbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            snapshotbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [snapshotbutton setImage:image forState:UIControlStateNormal];
            [snapshotbutton addTarget:self action:@selector(snapshot) forControlEvents:UIControlEventTouchUpInside];
            snapshotButton = [[UIBarButtonItem alloc] initWithCustomView:snapshotbutton];
            
            if (isHoldButton) {
                playbackButton.enabled = NO;
                ptzButton.enabled = NO;
                audioButton.enabled = NO;
                talkButton.enabled = NO;
                megapixelButton.enabled = NO;
                snapshotButton.enabled = NO;
            }
            
            buttonArray = [[NSArray alloc] initWithObjects:flexItem, 
#if FUNC_PLAYBACK_SUPPORT
                           playbackButton, flexItem, 
#endif
                           ptzButton, flexItem, 
#if FUNC_AUDIO_SUPPORT
                           audioButton, flexItem,
                           talkButton, flexItem,
#endif
#if FUNC_MEGAPIXEL_SUPPORT
                           megapixelButton, flexItem, 
#endif
                           snapshotButton, flexItem, nil];
            
            [self setToolbarItems:buttonArray];

            [buttonArray release];
            
            [playbackButton release];
            [ptzButton release];
            [audioButton release];
            [talkButton release];
            [megapixelButton release];
            [snapshotButton release];
            break;
        }
        case eViewMode_PhysicalPTZ:
        {
            presetButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Preset", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(presetPressed)];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                [presetButton setTintColor:COLOR_TOOLBUTTONTINT];
            }
            else {
                [presetButton setTintColor:COLOR_TOOLBUTTONTINTIOS6];
            }
            buttonArray = [[NSArray alloc] initWithObjects:flexItem, presetButton, flexItem, nil];
            presetButton.enabled = readyToPreset;
            
            [self setToolbarItems:buttonArray];
            [buttonArray release];
            [presetButton release];
            
            [mLayoutToolbar setHidden:true];
            
            break;
        }
        case eViewMode_PlaybackDefault: // jerry, 2012/06/18
        {
            if (IS_IPAD)
                image = [UIImage imageNamed:@"folder_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"folder_32x32_nor.png"];
            UIButton *playbackbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            playbackbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playbackbutton setImage:image forState:UIControlStateNormal];
            [playbackbutton addTarget:self action:@selector(playbackPressed) forControlEvents:UIControlEventTouchUpInside];
            playbackButton = [[UIBarButtonItem alloc] initWithCustomView:playbackbutton];
            playbackButton.enabled = NO;
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"Previous_Frame_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"Previous_Frame_32x32_nor.png"];
            UIButton *previousframebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            previousframebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [previousframebutton setImage:image forState:UIControlStateNormal];
            [previousframebutton addTarget:self action:@selector(pbControlPreviousFrame) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *pfButton = [[UIBarButtonItem alloc] initWithCustomView:previousframebutton];
            pfButton.enabled = NO;
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"Next_Frame_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"Next_Frame_32x32_nor.png"];
            UIButton *nextframebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            nextframebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [nextframebutton setImage:image forState:UIControlStateNormal];
            [nextframebutton addTarget:self action:@selector(pbControlNextFrame) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *nfButton = [[UIBarButtonItem alloc] initWithCustomView:nextframebutton];
            nfButton.enabled = NO;
            
            UIButton *playbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (IS_IPAD)
                image = [UIImage imageNamed:@"play_Playback_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"play_Playback_32x32_nor.png"];
            
            // Brosso - TimeLine
            [self showTimeLine];
            
            playbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playbutton setImage:image forState:UIControlStateNormal];
            [playbutton addTarget:self action:@selector(pbControlPlay) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *playButton = [[UIBarButtonItem alloc] initWithCustomView:playbutton];
            playButton.enabled = NO;
            
            UIButton *playspeedbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (IS_IPAD)
                image = [UIImage imageNamed:@"speed_1x_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"speed_1x_32x32_nor.png"];
            playspeedbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playspeedbutton setImage:image forState:UIControlStateNormal];
            [playspeedbutton addTarget:self action:@selector(playbackSpeedPressed) forControlEvents:UIControlEventTouchUpInside];
            speedButton = [[UIBarButtonItem alloc] initWithCustomView:playspeedbutton];
            speedButton.enabled = NO;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            UIButton *dualrecordbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (playbackRecordFile == eRecordFile1) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"dual_record_1_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"dual_record_1_32x32_nor.png"];
            }
            else {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"dual_record_2_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"dual_record_2_32x32_nor.png"];
            }
            dualrecordbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [dualrecordbutton setImage:image forState:UIControlStateNormal];
            [dualrecordbutton addTarget:self action:@selector(playbackDualRecordPressed) forControlEvents:UIControlEventTouchUpInside];
            dualRecButton = [[UIBarButtonItem alloc] initWithCustomView:dualrecordbutton];
            dualRecButton.enabled = NO;
#endif
            buttonArray = [[NSArray alloc] initWithObjects:flexItem,
                           playbackButton, flexItem,
                           pfButton, flexItem,
                           playButton, flexItem,
                           nfButton, flexItem,
                           speedButton, flexItem,
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
                           dualRecButton, flexItem,
#endif
                           nil];
            [self setToolbarItems:buttonArray];
            
            [buttonArray release];
            
            [playbackButton release];
            [pfButton release];
            [nfButton release];
            [playButton release];
            [speedButton release];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            [dualRecButton release];
#endif
            [mLayoutToolbar setHidden:true];
            break;
        }
        case eViewMode_Playback:
        {
            if (IS_IPAD)
                image = [UIImage imageNamed:@"folder_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"folder_32x32_nor.png"];
            UIButton *playbackbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            playbackbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playbackbutton setImage:image forState:UIControlStateNormal];
            if (IS_IPAD)
                [playbackbutton addTarget:self action:@selector(showPlaybackPopover) forControlEvents:UIControlEventTouchUpInside];
            else
                [playbackbutton addTarget:self action:@selector(playbackPressed) forControlEvents:UIControlEventTouchUpInside];
            playbackButton = [[UIBarButtonItem alloc] initWithCustomView:playbackbutton];
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"Previous_Frame_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"Previous_Frame_32x32_nor.png"];
            UIButton *previousframebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            previousframebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [previousframebutton setImage:image forState:UIControlStateNormal];
            [previousframebutton addTarget:self action:@selector(pbControlPreviousFrame) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *pfButton = [[UIBarButtonItem alloc] initWithCustomView:previousframebutton];
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"Next_Frame_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"Next_Frame_32x32_nor.png"];
            UIButton *nextframebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            nextframebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [nextframebutton setImage:image forState:UIControlStateNormal];
            [nextframebutton addTarget:self action:@selector(pbControlNextFrame) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *nfButton = [[UIBarButtonItem alloc] initWithCustomView:nextframebutton];
            
            UIButton *playbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (ePlayType == ePLAY_PLAY) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"pause_Playback_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"pause_Playback_32x32_nor.png"];
                playbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
                [playbutton setImage:image forState:UIControlStateNormal];
                [playbutton addTarget:self action:@selector(pbControlPause) forControlEvents:UIControlEventTouchUpInside];
            }
            else {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"play_Playback_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"play_Playback_32x32_nor.png"];
                playbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
                [playbutton setImage:image forState:UIControlStateNormal];
                [playbutton addTarget:self action:@selector(pbControlPlay) forControlEvents:UIControlEventTouchUpInside];
            }
            UIBarButtonItem *playButton = [[UIBarButtonItem alloc] initWithCustomView:playbutton];
            
            UIButton *playspeedbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (playbackSpeed == 1.0) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_1x_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_1x_32x32_nor.png"];
            }
            else if (playbackSpeed == 2.0) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_2x_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_2x_32x32_nor.png"];
            }
            else if (playbackSpeed == 4.0) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_4x_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_4x_32x32_nor.png"];
            }
            else if (playbackSpeed == 0.5) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_half_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_half_32x32_nor.png"];
            }
            else if (playbackSpeed == 0.25) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_quarter_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_quarter_32x32_nor.png"];
            }
            playspeedbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playspeedbutton setImage:image forState:UIControlStateNormal];
            [playspeedbutton addTarget:self action:@selector(playbackSpeedPressed) forControlEvents:UIControlEventTouchUpInside];
            speedButton = [[UIBarButtonItem alloc] initWithCustomView:playspeedbutton];
            
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            UIButton *dualrecordbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (playbackRecordFile == eRecordFile1) {
                if (IS_IPAD)
                image = [UIImage imageNamed:@"dual_record_1_48x48_nor.png"];
                else
                image = [UIImage imageNamed:@"dual_record_1_32x32_nor.png"];
            }
            else {
                if (IS_IPAD)
                image = [UIImage imageNamed:@"dual_record_2_48x48_nor.png"];
                else
                image = [UIImage imageNamed:@"dual_record_2_32x32_nor.png"];
            }
            dualrecordbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [dualrecordbutton setImage:image forState:UIControlStateNormal];
            [dualrecordbutton addTarget:self action:@selector(playbackDualRecordPressed) forControlEvents:UIControlEventTouchUpInside];
            dualRecButton = [[UIBarButtonItem alloc] initWithCustomView:dualrecordbutton];
            if (   [serverMgr getServerTypeByChIndex:iCurrentCamera] != eServer_MainConsole
                && [serverMgr getServerTypeByChIndex:iCurrentCamera] != eServer_NVRmini
                && [serverMgr getServerTypeByChIndex:iCurrentCamera] != eServer_NVRsolo)
            {
                dualRecButton.enabled = NO;
            }
            else
            {
                dualRecButton.enabled = isDualRecording;
            }
#endif
            if (isHoldButton) {
                playbackButton.enabled = NO;
                pfButton.enabled = NO;
                playButton.enabled = NO;
                nfButton.enabled = NO;
                speedButton.enabled = NO;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
                dualRecButton.enabled = NO;
#endif
            }
            
            buttonArray = [[NSArray alloc] initWithObjects:flexItem,
                           playbackButton, flexItem,
                           pfButton, flexItem,
                           playButton, flexItem,
                           nfButton, flexItem,
                           speedButton, flexItem,
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
                           dualRecButton, flexItem,
#endif
                           nil];
            [self setToolbarItems:buttonArray];
            
            [buttonArray release];
            
            [playbackButton release];
            [pfButton release];
            [nfButton release];
            [playButton release];
            [speedButton release];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            [dualRecButton release];
#endif
            [mLayoutToolbar setHidden:true];
            break;
        }
        default:
            break;
    }
	[flexItem release];
}

- (void)refreshLayoutToolbar {
    float toolbar_h = self.navigationController.toolbar.frame.size.height;
    float offset_h;
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if (IS_IPAD && SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"8.0")) {
        if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
            offset_h = 7.8;
        } else {
            offset_h = 2;
        }
    }else {
        offset_h = 2;
    }
    mLayoutToolbar.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height - toolbar_h * offset_h, self.navigationController.toolbar.frame.size.width, toolbar_h);
    [self addLayoutBtn];
    
    // Brosso - TimeLine
    [pTimeLineViewController.view setFrame:CGRectMake(0, mLayoutToolbar.frame.origin.y - 30, self.view.frame.size.width, 60)];
}

- (void)liveviewMenuPressed {
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/06
    pLvMenuController = [[[SlideViewMenuController alloc] initWithStyle:UITableViewStyleGrouped parentview:self] autorelease];
    pLvMenuController.serverMgr = serverMgr;
    [self.navigationController pushViewController:pLvMenuController animated:YES];
#else
    DOTable = [[[DOTableViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
    [DOTable setParent:self];
    [DOTable setServerMgr:serverMgr];
    [DOTable setTitle:NSLocalizedString(@"Digital Output", nil)];
    [self.navigationController pushViewController:DOTable animated:YES];
#endif // FUNC_PUSH_NOTIFICATION_SUPPORT
}

- (void)liveviewCameraListPressed {
    pLvCameraListController = [[[MyCameraListController alloc] initWithStyle:UITableViewStyleGrouped] autorelease];
    pLvCameraListController.switchControlDelegate = self;
    pLvCameraListController.serverMgr = serverMgr;
    [self.navigationController pushViewController:pLvCameraListController animated:YES];
}

- (void)addLayoutToolbar {
    [self addLayoutBtn];
    [self.view addSubview:mLayoutToolbar];
}

- (void)addLayoutBtn {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (IS_IPAD) {
        UIImage * image = [UIImage imageNamed:@"Btn_2X3_iPad.png"];
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_2X3) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_2X3 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)  // jerrylu, 2012/06/15
            image = [UIImage imageNamed:@"D06_5x3_h_48x48_nor.png"];
        else
            image = [UIImage imageNamed:@"Btn_3X5.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_3X5) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * Btn_3X5 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)  // jerrylu, 2012/06/15
            image = [UIImage imageNamed:@"D06_6x4_h_48x48_nor.png"];
        else
            image = [UIImage imageNamed:@"Btn_4X6.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_4X6) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * Btn_4X6 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)  // jerrylu, 2012/06/15
            image = [UIImage imageNamed:@"D06_8x5_h_48x48_nor.png"];
        else
            image = [UIImage imageNamed:@"Btn_5X8.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_5X8) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * Btn_5X8 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"D06_1x2_48x48_nor.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_1X2) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_1X2 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"D06_2x2_48x48_nor.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeLayout_2X2) forControlEvents:UIControlEventTouchUpInside];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_2X2 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        image = [UIImage imageNamed:@"D06_1x1_48x48_nor.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
        [button setImage:image forState:UIControlStateNormal];
        if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
            button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
        UIBarButtonItem * Btn_1X1 = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        
        UIBarButtonItem * flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                   target:nil
                                                                                   action:nil];
        
        NSArray * buttonArray = [[NSArray alloc] initWithObjects:
                                 flexItem, Btn_1X1,
                                 flexItem, Btn_1X2,
                                 flexItem, Btn_2X2,
                                 flexItem, Btn_2X3,
                                 flexItem, Btn_3X5,
                                 //flexItem, Btn_4X6,
                                 //flexItem, Btn_5X8,
                                 flexItem, nil];
        
        [mLayoutToolbar setItems:buttonArray];
        [buttonArray release];
        [Btn_1X1 release];
        [Btn_2X3 release];
        [Btn_3X5 release];
        [Btn_4X6 release];
        [Btn_5X8 release];
        [Btn_2X2 release];
        [Btn_1X2 release];
        [flexItem release];
    }
    else {
	    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    	
	    UIImage *image = [UIImage imageNamed:@"Btn_1X1.png"];
	    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	    button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
	    [button setImage:image forState:UIControlStateNormal];
	    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
	        button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
	    UIBarButtonItem * Btn_1X1 = [[UIBarButtonItem alloc] initWithCustomView:button];
    	
	    image = [UIImage imageNamed:@"Btn_1X2.png"];  
	    button = [UIButton buttonWithType:UIButtonTypeCustom];
	    button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
	    [button setImage:image forState:UIControlStateNormal];
	    [button addTarget:self action:@selector(changeLayout_1X2) forControlEvents:UIControlEventTouchUpInside];
	    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
	        button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
	    UIBarButtonItem * Btn_1X2 = [[UIBarButtonItem alloc] initWithCustomView:button];
	    
	    image = [UIImage imageNamed:@"Btn_1X3.png"];
	    button = [UIButton buttonWithType:UIButtonTypeCustom];
	    button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
	    [button setImage:image forState:UIControlStateNormal];
	    [button addTarget:self action:@selector(changeLayout_1X3) forControlEvents:UIControlEventTouchUpInside];
	    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
	        button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
	    UIBarButtonItem * Btn_1X3 = [[UIBarButtonItem alloc] initWithCustomView:button];
	    
	    image = [UIImage imageNamed:@"Btn_2X2.png"];
	    button = [UIButton buttonWithType:UIButtonTypeCustom];
	    button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
	    [button setImage:image forState:UIControlStateNormal];
	    [button addTarget:self action:@selector(changeLayout_2X2) forControlEvents:UIControlEventTouchUpInside];
	    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
	        button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
	    UIBarButtonItem * Btn_2X2 = [[UIBarButtonItem alloc] initWithCustomView:button];
	    
	    image = [UIImage imageNamed:@"Btn_2X3.png"];
	    button = [UIButton buttonWithType:UIButtonTypeCustom];
	    button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
	    [button setImage:image forState:UIControlStateNormal];
	    [button addTarget:self action:@selector(changeLayout_2X3) forControlEvents:UIControlEventTouchUpInside];
	    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
	        button.transform = CGAffineTransformMakeRotation(M_PI*270/180);
	    UIBarButtonItem * Btn_2X3 = [[UIBarButtonItem alloc] initWithCustomView:button];
	    
	    NSArray *buttonArray = [[NSArray alloc] initWithObjects:flexItem,
	                            Btn_1X1, flexItem,
	                            Btn_1X2, flexItem,
	                            Btn_1X3, flexItem,
	                            Btn_2X2, flexItem,
	                            Btn_2X3, flexItem,
	                            nil];
	    
	    [mLayoutToolbar setItems:buttonArray];
	    
	    [buttonArray release];
	    [Btn_1X1 release];
	    [Btn_1X2 release];
	    [Btn_1X3 release];
	    [Btn_2X2 release];
	    [Btn_2X3 release];
	}
}

#pragma mark -
#pragma mark PTZ action methods
- (void)ptzPressed {
    readyToPreset = NO;
    [self changeTOPhyPTZ];
	[serverMgr setPresetDelegateByChIndex:iCurrentCamera delegate:self];
    [serverMgr getPresetByChIndex:iCurrentCamera];
    [presetButton performSelectorOnMainThread:@selector(setEnabled:) withObject:NO waitUntilDone:NO];
}

- (void)sendPTZWithOp:(NSString *) operationCommand {
	if (!readyToPTZ) {
		return;
	}
	if (!ptzCap) {
		return;
	}
    [serverMgr setPTZByChIndex:iCurrentCamera withDirection:operationCommand];
}

- (void)sendPresetAtNO:(int) no {
    [serverMgr gotoPTZPresetByChIndex:iCurrentCamera withIdx:no];
}

- (void)presetPressed {
    [self stopHideTimer]; // jerrylu, 2012/11/19, fix bug10161
    [self hideUIWithValue:NO];
    
    // jerrylu, 2012/06/19, fix Bug7544
    while(1)
    {
        if ([serverMgr isGetPresetByChIndex:iCurrentCamera])
            break;
    }
    
	[self showPreset];
}

- (void)showPreset {
	eActionType = eAct_Preset;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController * actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Preset", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        if (IS_IPAD) {
            UIAlertAction* defaultAct = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"DefaultCancel", nil)
                                         style:UIAlertActionStyleCancel
                                         handler:^(UIAlertAction * action)
                                         {
                                             if (eViewModeType == eViewMode_PhysicalPTZ) {
                                                 [self resetHideTimer];
                                             }
                                             presetController = nil;
                                             eActionType = eAct_None;
                                         }];
            [actionSheet addAction:defaultAct];
        }
        UIAlertAction* cancelAct = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                    style:UIAlertActionStyleDestructive
                                    handler:^(UIAlertAction * action)
                                    {
                                        if (eViewModeType == eViewMode_PhysicalPTZ) {
                                            [self resetHideTimer];
                                        }
                                        presetController = nil;
                                        eActionType = eAct_None;
                                    }];
        [actionSheet addAction:cancelAct];
        
        NSMutableArray *presetNameArray =  [serverMgr getPresetNameArrayByChIndex:iCurrentCamera];
        for(id innerObj in presetNameArray){
            UIAlertAction* presetAct = [UIAlertAction
                                      actionWithTitle:innerObj
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          int iPresetNO = [presetNameArray indexOfObject:innerObj];
                                          [self sendPresetAtNO:iPresetNO];
                                          
                                          if (eViewModeType == eViewMode_PhysicalPTZ) {
                                              [self resetHideTimer];
                                          }
                                          
                                          [presetController dismissViewControllerAnimated:YES completion:nil];
                                          presetController = nil;
                                          eActionType = eAct_None;
                                      }];
            [actionSheet addAction:presetAct];
        }
        
        presetController = actionSheet;
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popover = actionSheet.popoverPresentationController;
            if (popover) {
                [popover setPermittedArrowDirections:0];
                popover.sourceView = self.view;
                popover.sourceRect = self.view.bounds;
            }
        }
        [self presentViewController:actionSheet animated:YES completion:nil];
    }else {
        UIActionSheet * action = [[UIActionSheet alloc] init];
        action.title = NSLocalizedString(@"Preset", nil);
        action.delegate = self;
        action.destructiveButtonIndex = [action addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
        
        NSMutableArray *presetNameArray =  [serverMgr getPresetNameArrayByChIndex:iCurrentCamera];
        for(id innerObj in presetNameArray){
            [action addButtonWithTitle:innerObj];
        }
        
        [action showInView:self.view];
        presetSheet = action;
        [action release];
    }
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ) {
        if( IS_IPAD ) {
            // fix for iOS7 iPad UIActionSheet presentation when content need to scroll
            // and scrolled view appears with unnecessary copies, remove not needed ones
            // and set proper tableview height too
            int count = 0;
            for (UIView *subview in actionSheet.subviews) {
                if( [subview isMemberOfClass:[UIView class]] ) {
                    if( ++count == 1 ) {
                        // process only first view
                        for( UIView *subsubview in subview.subviews ) {
                            if( [subsubview isKindOfClass:[UITableView class]] ) {
                                // fix table view height
                                UITableView *tableView = (UITableView*)subsubview;
                                
                                CGRect tableViewFrame = tableView.frame;
                                tableViewFrame.size.height -= subview.frame.origin.y;
                                
                                tableView.frame = tableViewFrame;
                            }
                        }
                    } else {
                        // remove unnecessary view
                        [subview removeFromSuperview];
                    }
                }
            }
        }
    }
}

#pragma mark -
#pragma mark Snapshot action methods
- (void)snapshot {
    if (IS_IPAD) {
        if (eViewModeType == eViewMode_Playback)
        {
            [self pbControlPause];
            [pPlaybackMenuPopover dismissPopoverAnimated:YES];
        }
    }
    
    if (eViewModeType == eViewMode_DigitalPTZ) {
        [self stopHideTimer]; // jerrylu, 2012/11/19, fix bug10161
        [self hideUIWithValue:NO];
    }
    
    eActionType = eAct_Snapshot;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController * actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Snapshot", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        if (IS_IPAD) {
            UIAlertAction* defaultAct = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"DefaultCancel", nil)
                                         style:UIAlertActionStyleCancel
                                         handler:^(UIAlertAction * action)
                                         {
                                             if (eViewModeType == eViewMode_DigitalPTZ) {
                                                 [self resetHideTimer];
                                             }
                                             snapshotController = nil;
                                             eActionType = eAct_None;
                                         }];
            [actionSheet addAction:defaultAct];
        }
        UIAlertAction* cancelAct = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Cancel", nil) 
                                  style:UIAlertActionStyleDestructive
                                  handler:^(UIAlertAction * action)
                                  {
                                      if (eViewModeType == eViewMode_DigitalPTZ) {
                                          [self resetHideTimer];
                                      }
                                      snapshotController = nil;
                                      eActionType = eAct_None;
                                  }];
        UIAlertAction* saveAct = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Save", nil)
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [self snapshotToFile];
                                      if (eViewModeType == eViewMode_DigitalPTZ) {
                                          [self resetHideTimer];
                                      }
                                      [snapshotController dismissViewControllerAnimated:YES completion:nil];
                                      snapshotController = nil;
                                      eActionType = eAct_None;
                                  }];
        UIAlertAction* emailAct = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Send Email", nil)
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [self snapshotToEmail];
                                      if (eViewModeType == eViewMode_DigitalPTZ) {
                                          [self resetHideTimer];
                                      }
                                      [snapshotController dismissViewControllerAnimated:YES completion:nil];
                                      snapshotController = nil;
                                      eActionType = eAct_None;
                                  }];
        
        [actionSheet addAction:cancelAct];
        [actionSheet addAction:saveAct];
        [actionSheet addAction:emailAct];
        
        snapshotController = actionSheet;
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popover = actionSheet.popoverPresentationController;
            if (popover) {
                [popover setPermittedArrowDirections:0];
                popover.sourceView = self.view;
                popover.sourceRect = self.view.bounds;
            }
        }
        [self presentViewController:actionSheet animated:YES completion:nil];
    }else {
        UIActionSheet * action = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Snapshot", nil) delegate:self cancelButtonTitle:nil destructiveButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Save", nil), NSLocalizedString(@"Send Email", nil), nil];
        
        [action showInView:self.navigationController.view];
        snapshotSheet = action;
        [action release];
    }
}

- (void)snapshotToFile {
#if RENDER_BY_OPENGL
    BOOL isUpdate = FALSE;
    MyVideoFrameBuf *imgbuf = [parent.slideView getImageBufOfCamIndex:iCurrentCamera isUpdate:&isUpdate];
    UIImage *img = imageFromYUVBuffer((void *)[imgbuf.data bytes], imgbuf.width, imgbuf.height);
    NSData * photo = UIImageJPEGRepresentation(img, 1.0f);
#else
	NSData * photo = UIImageJPEGRepresentation([imageview.image image], 0.4);
#endif
	NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString * documentsDirectory = [paths objectAtIndex:0];
	NSLog(@"%@", [documentsDirectory stringByAppendingPathComponent:@"screenshot.jpg"]);
	NSError * error = nil;
	[photo writeToFile:[documentsDirectory stringByAppendingPathComponent:@"screenshot.jpg"] options:NSAtomicWrite error:&error];

#if RENDER_BY_OPENGL
    UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil) ;
#else
	UIImageWriteToSavedPhotosAlbum([imageview.image image], nil, nil, nil) ;
#endif
	[flashView setHidden:NO];
	[self.view addSubview:flashView];
	NSValue * contextPoint = [[NSValue valueWithCGPoint:flashView.center] retain];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView beginAnimations:nil context:contextPoint];
	[UIView setAnimationDuration:1];
	[UIView setAnimationRepeatAutoreverses:NO];
	[UIView setAnimationRepeatCount:1];
	flashView.alpha = 1;
	[UIView commitAnimations];
	[UIView beginAnimations:nil context:contextPoint];
	[UIView setAnimationDuration:1];
	[UIView setAnimationRepeatAutoreverses:NO];
	[UIView setAnimationRepeatCount:1];
	flashView.alpha = 0;
	[UIView commitAnimations];
	[contextPoint release];
}

- (NSString *)getSnapshotFileName {
	NSDate * now = [NSDate date];
	NSDateFormatter * formatter = [[[NSDateFormatter alloc] init] autorelease];
	[formatter setDateFormat:@"yyyyMMddHHmmss"];
	NSString * camName = [[self title] stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
	NSString * strDate = [[[NSString alloc] initWithFormat:@"%@%@.jpg", camName, [formatter stringFromDate:now]]autorelease];
	return strDate; 
}

- (void)snapshotToEmail {
	if ([MFMailComposeViewController canSendMail]) {	
		MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
		controller.mailComposeDelegate = self;
		//[controller setSubject:@"Subject Goes Here."];
		NSString * body = @"";
		[controller setMessageBody:body isHTML:YES];
		NSLog(@"%@", [self getSnapshotFileName]);
#if RENDER_BY_OPENGL
        BOOL isUpdate = FALSE;
        MyVideoFrameBuf *imgbuf = [parent.slideView getImageBufOfCamIndex:iCurrentCamera isUpdate:&isUpdate];
        UIImage *img = imageFromYUVBuffer((void *)[imgbuf.data bytes], imgbuf.width, imgbuf.height);
        [controller addAttachmentData:UIImageJPEGRepresentation(img, 1.0f) mimeType:@"image/jpeg" fileName:[self getSnapshotFileName]];
#else
		[controller addAttachmentData:UIImageJPEGRepresentation([imageview.image image], 1.0f) mimeType:@"image/jpeg" fileName:[self getSnapshotFileName]];
#endif
		[self presentViewController:controller animated:YES completion:nil];
		[controller release];
	}
}

- (void)mailComposeController:(MFMailComposeViewController *) controller didFinishWithResult:(MFMailComposeResult) result error:(NSError *) error {
	[self dismissViewControllerAnimated:YES completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
	
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
	if ([appDelegate bDelayMsg] == TRUE)
	{
		[appDelegate setBDelayMsg:FALSE];
		[parent setServerMgr:nil];
		[appDelegate showServerDownMessage:Np_EVENT_CONNECTION_LOST];
	}
}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context{
	flashView.alpha = 0;
	[flashView removeFromSuperview];
	[flashView setHidden:YES];
}

#pragma mark -
#pragma mark Mega-Pixel action methods
- (void)profilechange {
    isHoldButton = YES;
    if (![imageview.mIndicator isAnimating]) {
        [imageview.mIndicator startAnimating];
    }
    
    if (eProfile == eOriginal) {
            eProfile = eLow;
    }
    else {
        eProfile = eOriginal;
    }
    
    if (profileWaitTimer == nil) {
        profileWaitTimer = [NSTimer scheduledTimerWithTimeInterval:WAIT_FEEDBACK_INTERVAL
                                                          target:self
                                                        selector:@selector(resetProfileStatus)
                                                        userInfo:nil
                                                         repeats:NO];
    }
    
    [self refreshToolBar];
}

- (void)resetProfileStatus {
    [profileWaitTimer invalidate];
    profileWaitTimer = nil;
    
    isHoldButton = NO;
    if (eProfile == eOriginal) {
            eProfile = eLow;
    }
    else {
        eProfile = eOriginal;
    }
    
    [self refreshToolBar];
    
    NSString *msg;
    if (eProfile == eOriginal)
        msg = [NSString stringWithFormat:NSLocalizedString(@"Unable to access to low camera stream - insufficient bandwidth", nil)];
    else
        msg = [NSString stringWithFormat:NSLocalizedString(@"Unable to access to high camera stream - insufficient bandwidth", nil)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
}

#pragma mark -
#pragma mark Audio action methods
- (void)audioPressed {
	[self audiochange];
    [self refreshToolBar];
}

- (void)audiochange {
    if (eViewModeType == eViewMode_DigitalPTZ) {
        isHoldButton = YES;
        if (![imageview.mIndicator isAnimating]) {
            [imageview.mIndicator startAnimating];
        }
        
        if (liveviewaudioenable) {
            [serverMgr setLiveviewAudioStateByChIndex:iCurrentCamera state:NO];
            liveviewaudioenable = NO;
        }
        else {
            [serverMgr setLiveviewAudioStateByChIndex:iCurrentCamera state:YES];
            liveviewaudioenable = YES;
        }
        
        if (audioWaitTimer == nil) {
            audioWaitTimer = [NSTimer scheduledTimerWithTimeInterval:WAIT_FEEDBACK_INTERVAL
                                                              target:self
                                                            selector:@selector(resetAudioStatus)
                                                            userInfo:nil
                                                             repeats:NO];
        }
    }else if (eViewModeType == eViewMode_Playback) {
        if (playbackaudioenable) {
            [serverMgr setPlaybackAudioStateByChIndex:iCurrentCamera state:NO];
        }
        else {
            [serverMgr setPlaybackAudioStateByChIndex:iCurrentCamera state:YES];
        }
        playbackaudioenable = [serverMgr getPlaybackAudioStateByChIndex:iCurrentCamera];
    }
}

- (void)resetAudioStatus {
    if (eViewModeType == eViewMode_DigitalPTZ) {
        [audioWaitTimer invalidate];
        audioWaitTimer = nil;
        
        isHoldButton = NO;
        if ([imageview.mIndicator isAnimating]) {
            [imageview.mIndicator stopAnimating];
        }
        
        if (liveviewaudioenable) {
            [serverMgr setLiveviewAudioStateByChIndex:iCurrentCamera state:NO];
            liveviewaudioenable = NO;
        }
        else {
            [serverMgr setLiveviewAudioStateByChIndex:iCurrentCamera state:YES];
            liveviewaudioenable = YES;
        }
    }
    [self refreshToolBar];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Unable to load audio - insufficient bandwidth", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Unable to load audio - insufficient bandwidth", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
}

- (void)talkPressed {
    [self talkchange];
    [self refreshToolBar];
}

- (void)talkchange {
    isHoldButton = YES;
    if (![imageview.mIndicator isAnimating]) {
        [imageview.mIndicator startAnimating];
    }
    
    if (talkenable) {
        [serverMgr setTalkStateByChIndex:iCurrentCamera state:NO];
        talkenable = NO;
    }
    else {
        [serverMgr setTalkStateByChIndex:iCurrentCamera state:YES];
        talkenable = YES;
    }
    
    if (talkWaitTimer == nil) {
        talkWaitTimer = [NSTimer scheduledTimerWithTimeInterval:WAIT_FEEDBACK_INTERVAL
                                                          target:self
                                                        selector:@selector(resetTalkStatus)
                                                        userInfo:nil
                                                         repeats:NO];
    }
}

- (void)resetTalkStatus {
    if (eViewModeType == eViewMode_DigitalPTZ) {
        [talkWaitTimer invalidate];
        talkWaitTimer = nil;
        
        isHoldButton = NO;
        if ([imageview.mIndicator isAnimating]) {
            [imageview.mIndicator stopAnimating];
        }
        
        if (talkenable) {
            [serverMgr setTalkStateByChIndex:iCurrentCamera state:NO];
            talkenable = NO;
        }
        else {
            [serverMgr setTalkStateByChIndex:iCurrentCamera state:YES];
            talkenable = YES;
        }
    }
    [self refreshToolBar];
}

- (void)getTalkReserved:(NSString *)servername user:(NSString *)username {
    NSString *msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"The talk connection has been occupied.", nil)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert release];
    }
    [msg release];
    
    if (talkenable) {
        talkenable = NO;
        [self performSelectorOnMainThread:@selector(refreshToolBar) withObject:nil waitUntilDone:YES];
    }
}

- (void)getTalkSessionError {
    if (talkenable) {
        talkenable = NO;
        [self performSelectorOnMainThread:@selector(refreshToolBar) withObject:nil waitUntilDone:YES];
    }
}

#pragma mark -
#pragma mark Playback action methods

- (void)playbackPressed {
    [self pbControlPause]; // jerrylu, 2012/06/21
    
    // Comment out - Brosso
//    [self.navigationController pushViewController:pPlaybackController animated:YES];
    [self PbViewControllerOpenRecord];
}

- (void)playbackPreStart
{
    if (pPlaybackController == nil)
        return;

    [serverMgr disconnectAllCam];

    int newTimeInterval = kDefaultTimeInterval; // 30 secs //[pPlaybackController getPickTimeInterval];
    int newRevTimeInterval = -newTimeInterval;
    
    // Change to today - Brosso
    NSDate *newDate = [[NSDate alloc] initWithTimeInterval:0 sinceDate:[NSDate date]/*[pPlaybackController getPickDate]*/];
    NSDate *newEndDate  = [[NSDate alloc] initWithTimeInterval:newTimeInterval sinceDate:[NSDate date]/*[pPlaybackController getPickDate]*/];
    NSDate *newRevEndDate = [[NSDate alloc] initWithTimeInterval:newRevTimeInterval sinceDate:[NSDate date]/*[pPlaybackController getPickDate]*/];

    if (playbackStartTime != nil)
        [playbackStartTime release];

    playbackStartTime = [newDate copy];

    if (playbackEndTime != nil)
        [playbackEndTime release];

    playbackEndTime = [newEndDate copy];

    if (playbackReverseEndTime != nil)
        [playbackReverseEndTime release];

    playbackReverseEndTime = [newRevEndDate copy]; // jerrylu, 2012/06/13

    NSDate2NpDateTime(playbackStartTime, &rec1_starttime);
    NSDate2NpDateTime(playbackStartTime, &rec2_starttime);
    NSDate2NpDateTime(playbackEndTime, &rec1_endtime);
    NSDate2NpDateTime(playbackEndTime, &rec2_endtime);
    NSDate2NpDateTime(playbackReverseEndTime, &endrevtime);

    [newDate release];
    [newEndDate release];
    [newRevEndDate release];

    memset(&(currenttime), 0, sizeof(Np_DateTime));
    playbackSpeed = kDefaultPlaybackSpeed;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    playbackRecordFile = (EDualRecordFile)[pPlaybackController getSelectedRecordfile];
#endif
#if RENDER_BY_OPENGL
    zoomLevel = 1.0;
    [imageview.image resetScaleFactors];
#else
    zoomLevel = 0;
#endif        
    [self playbackStart];
}

- (void)queryRecordByDate:(NSDate*)date {
    [serverMgr queryRecordByIndex:iCurrentCamera andDate:date];
}

- (void)playbackStart {
#if DEBUG_LOG
    NSLog(@"Playback Start");
#endif
    isPlaybackError = NO;
    
    // Brosso -Timeline
    [serverMgr setTimeLineDelegate:pTimeLineViewController];
    
    BOOL ret = NO;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (   [serverMgr getServerTypeByChIndex:iCurrentCamera] == eServer_MainConsole
        || [serverMgr getServerTypeByChIndex:iCurrentCamera] == eServer_NVRmini
        || [serverMgr getServerTypeByChIndex:iCurrentCamera] == eServer_NVRsolo) {
        ret = [serverMgr playbackStartFromDate:iCurrentCamera recordfile:playbackRecordFile startdate:playbackStartTime enddate:playbackEndTime];
    }
    else {
#endif
        ret = [serverMgr playbackStartFromDate:iCurrentCamera startdate:playbackStartTime enddate:playbackEndTime];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    }
#endif
    if (ret) {
        ePlayType = ePLAY_PAUSE;
        [serverMgr setPlaybackDelegate:self];
        
        playbackaudioenable = NO;
        [serverMgr setPlaybackAudioStateByChIndex:iCurrentCamera state:NO];
        
        // clean the image buffer on Page
        [parent.slideView resetImageOfCamIndex:iCurrentCamera];
        
        //[self performSelectorOnMainThread:@selector(changeTOPlaybackDefaultMode) withObject:nil waitUntilDone:NO];
    }
    else {
        if ([serverMgr isServerLogoutingByIndex:iCurrentCamera]) { // check if logouting
            return;
        }
        
        // jerrylu, 2012/11/29, fix bug7531
        if ([serverMgr getServerTypeByChIndex:iCurrentCamera] == eServer_Titan ||
            [serverMgr getServerTypeByChIndex:iCurrentCamera] == eServer_Crystal) {
            // use delegate control for Titan server
        }
        else {
            if ([serverMgr isPlaybackStreaming:iCurrentCamera] == NO) {
                [self performSelectorOnMainThread:@selector(playbackErrorHandleForConnectionError) withObject:nil waitUntilDone:NO];
            }else {
                [self performSelectorOnMainThread:@selector(playbackErrorHandleForNoData) withObject:nil waitUntilDone:NO];
            }
        }
    }
}

- (void)playbackEnd {
#if DEBUG_LOG
    NSLog(@"******* End Playback *******");
#endif
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
    if (playbackWaitTimer != nil) {
        [playbackWaitTimer invalidate];
        playbackWaitTimer = nil;
    }
    [serverMgr playbackStop:iCurrentCamera];
#if RENDER_BY_OPENGL
    zoomLevel = 1.0;
    [imageview.image resetScaleFactors];
#else
    zoomLevel = 0;
#endif
    [self performSelectorOnMainThread:@selector(changeTONonePTZ) withObject:nil waitUntilDone:nil];
    [mLayoutToolbar setHidden:false];
    
    // Brosso - Hide TimeLine
    [self hideTimeLine];
}

- (void)playbackReopenRecord {
    Np_DateTime reEndTime;
    memset(&reEndTime, 0, sizeof(Np_DateTime));
    
    NSDate2NpDateTime(playbackEndTime, &reEndTime);
    reEndTime.millisecond = 0;
    
    EServerType eServerType = [serverMgr getServerTypeByChIndex:iCurrentCamera];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (   (   eServerType == eServer_MainConsole
            || eServerType == eServer_NVRmini
            || eServerType == eServer_NVRsolo)
        && playbackRecordFile == eRecordFile2) {
        [serverMgr playbackOpenRecord:iCurrentCamera startdate:rec2_starttime enddate:reEndTime];
        [self pbControlPause];
    }
    else if (   eServerType == eServer_MainConsole
             || eServerType == eServer_NVRmini
             || eServerType == eServer_NVRsolo) {
        [serverMgr playbackOpenRecord:iCurrentCamera startdate:rec1_starttime enddate:reEndTime];
        [self pbControlPause];
    }
    else {
        [self pbControlPause];
        [self pbControlSeek:rec1_starttime];
    }
#else
    if (   eServerType == eServer_MainConsole
        || eServerType == eServer_NVRmini
        || eServerType == eServer_NVRsolo) {
        [serverMgr playbackOpenRecord:iCurrentCamera startdate:rec1_starttime enddate:reEndTime];
        [self pbControlPause];
    }
    else {
        [self pbControlPause];
        [self pbControlSeek:rec1_starttime];
    }
#endif
}

- (void)playbackSpeedPressed {
    ePlayType = ePLAY_PAUSE;
    [serverMgr playbackPause:iCurrentCamera];
    [self refreshToolBar];
    
    if (IS_IPAD) {
        if (pPlaybackSpeedPopover.popoverVisible == NO) {
            UIViewController* popoverContent = [[UIViewController alloc] init];
            UIView* popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
                popoverView.backgroundColor = [UIColor blackColor];
            }
            CGRect pickerFrame = CGRectMake(0, 0, 0, 0);
            UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            for (int i = 0; i < [playbackSpeedValueList count]; i++) {
                if (playbackSpeed == [[playbackSpeedValueList objectAtIndex:i] floatValue]) {
                    [pickerView selectRow:i inComponent:0 animated:NO];
                    break;
                }
            }
            speedPicker = pickerView;
            [popoverView addSubview:pickerView];
            
            UIBarButtonItem * closeButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"OK", nil) style:UIBarButtonItemStylePlain target:self action:@selector(changePlaybackSpeed)];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                [closeButton setTintColor:[UIColor darkTextColor]];
            }
            popoverContent.view = popoverView;
            popoverContent.preferredContentSize = CGSizeMake(PopoverSizeWidth, 216);
            
            UINavigationController * navigation = [[UINavigationController alloc] initWithRootViewController:popoverContent];
            popoverContent.navigationItem.leftBarButtonItem = closeButton;
            pPlaybackSpeedPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
            [pPlaybackSpeedPopover presentPopoverFromBarButtonItem:speedButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
            [pickerView release];
            [navigation release];
            [closeButton release];
            [popoverView release];
            [popoverContent release];
        }
        else {
            [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
        }
    }
    else { // iPhone version
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if (speedSlider == nil) {
                speedSlider = [[UISlider alloc] init];
                [speedSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
                [speedSlider addTarget:self action:@selector(sliderTouchEnded:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
                speedImageView = [[UIImageView alloc] init];
                [speedImageView setImage:[UIImage imageNamed:@"speed_1x_32x32_nor.png"]];
            }
            
            if ([speedSlider superview] == nil) {
                for (int i = 0; i < [playbackSpeedValueList count]; i++) {
                    if (playbackSpeed == [[playbackSpeedValueList objectAtIndex:i] floatValue]) {
                        float speedslidervalue = i * 1.0 / ([playbackSpeedValueList count] - 1);
                        [speedSlider setValue:speedslidervalue];
                        
                        switch (i) {
                            case 0:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_quarter_32x32_nor.png"]];
                                break;
                            case 1:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_half_32x32_nor.png"]];
                                break;
                            case 2:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_1x_32x32_nor.png"]];
                                break;
                            case 3:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_2x_32x32_nor.png"]];
                                break;
                            case 4:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_4x_32x32_nor.png"]];
                                break;
                            default:
                                break;
                        }
                    }
                }
                [speedSlider removeFromSuperview];
                [speedImageView removeFromSuperview];
                
                speedSlider.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 210.0, [[UIScreen mainScreen] bounds].size.height - 80.0, 200.0, 32.0);
                speedImageView.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 250.0, [[UIScreen mainScreen] bounds].size.height - 80.0, 32.0, 32.0);
                
                [self.view addSubview:speedSlider];
                [self.view addSubview:speedImageView];
            }
            else {
                [speedSlider removeFromSuperview];
                [speedImageView removeFromSuperview];
            }
        }
        else {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
            [actionSheet setActionSheetStyle:UIActionSheetStyleDefault];
            
            CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
            UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            for (int i = 0; i < [playbackSpeedValueList count]; i++) {
                if (playbackSpeed == [[playbackSpeedValueList objectAtIndex:i] floatValue]) {
                    [pickerView selectRow:i inComponent:0 animated:NO];
                    break;
                }
            }
            speedPicker = pickerView;
            [actionSheet addSubview:pickerView];
            
            UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:NSLocalizedString(@"OK", nil)]];
            closeButton.momentary = YES;
            closeButton.tintColor = [UIColor blackColor];
            [closeButton addTarget:self action:@selector(changePlaybackSpeed) forControlEvents:UIControlEventValueChanged];
            [actionSheet addSubview:closeButton];
            [actionSheet setBackgroundColor:[UIColor whiteColor]];
            [actionSheet showInView:self.view];
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
                closeButton.frame = CGRectMake(10, 7.0f, 50.0f, 30.0f);
                [actionSheet setBounds:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 485)];
            }
            else {
                closeButton.frame = CGRectMake(10, 7.0f, 50.0f, 30.0f);
                [actionSheet setBounds:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height, 325)];
            }
            speedSheet = actionSheet;
            
            [pickerView release];
            [closeButton release];
            [actionSheet release];
        }
    }
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    float step = 0.5 / ([playbackSpeedValueList count] - 1);
    int speedIndex = ((sender.value / step) + 1) / 2;
    playbackSpeed = [[playbackSpeedValueList objectAtIndex:speedIndex] floatValue];
    
    switch (speedIndex) {
        case 0:
            [speedImageView setImage:[UIImage imageNamed:@"speed_quarter_32x32_nor.png"]];
            break;
        case 1:
            [speedImageView setImage:[UIImage imageNamed:@"speed_half_32x32_nor.png"]];
            break;
        case 2:
            [speedImageView setImage:[UIImage imageNamed:@"speed_1x_32x32_nor.png"]];
            break;
        case 3:
            [speedImageView setImage:[UIImage imageNamed:@"speed_2x_32x32_nor.png"]];
            break;
        case 4:
            [speedImageView setImage:[UIImage imageNamed:@"speed_4x_32x32_nor.png"]];
            break;
        default:
            break;
    }
    
    [self refreshToolBar];
}

- (IBAction)sliderTouchEnded:(UISlider *)sender {
    for (int i = 0; i < [playbackSpeedValueList count]; i++) {
        if (playbackSpeed == [[playbackSpeedValueList objectAtIndex:i] floatValue]) {
            float speedslidervalue = i * 1.0 / ([playbackSpeedValueList count] - 1);
            [speedSlider setValue:speedslidervalue];
        }
    }
    [self changePlaybackSpeed];
}

- (void)changePlaybackSpeed {
    if (speedPicker != nil) {
        NSInteger row = [speedPicker selectedRowInComponent:0];
        playbackSpeed = [[playbackSpeedValueList objectAtIndex:row] floatValue];
    }
    [serverMgr playbackSetSpeed:iCurrentCamera speed:playbackSpeed];

    if (IS_IPAD) {
        [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
    }
    else {
        [speedSheet dismissWithClickedButtonIndex:0 animated:YES];
        speedSheet = nil;
    }
    [self refreshToolBar];
}

- (void)pbControlPlay {
    ePlayType = ePLAY_PLAY;
    
    Np_DateTime datetime = [serverMgr playbackGetTime:iCurrentCamera];
    if ([self checkPlaybackTimeline:datetime]) {
        [serverMgr playbackPlay:iCurrentCamera];
    }
    else {
        ePlayType = ePLAY_PAUSE;
        [serverMgr playbackPause:iCurrentCamera];
    }
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)playbackDualRecordPressed {
    ePrePlayType = ePlayType;
    
    [self pbControlPause];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
    
    eActionType = eAct_DualRecord;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController * actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Select Record File", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        if (IS_IPAD) {
            UIAlertAction* defaultAct = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"DefaultCancel", nil)
                                         style:UIAlertActionStyleCancel
                                         handler:^(UIAlertAction * action)
                                         {
                                             dualRecordController = nil;
                                             eActionType = eAct_None;
                                             
                                             if (ePrePlayType == ePLAY_PLAY) {
                                                 [self pbControlPlay];
                                             }
                                         }];
            [actionSheet addAction:defaultAct];
        }
        UIAlertAction* cancelAct = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                    style:UIAlertActionStyleDestructive
                                    handler:^(UIAlertAction * action)
                                    {
                                        dualRecordController = nil;
                                        eActionType = eAct_None;
                                        
                                        if (ePrePlayType == ePLAY_PLAY) {
                                            [self pbControlPlay];
                                        }
                                    }];
        UIAlertAction* record1Act = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Record File 1", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if (playbackRecordFile != eRecordFile1) {
                                             playbackRecordFile = eRecordFile1;
                                             [self pbControlChangeDualRecord:playbackRecordFile];
                                         }
                                         else {
                                             if (ePrePlayType == ePLAY_PLAY) {
                                                 [self pbControlPlay];
                                             }
                                         }
                                         [dualRecordController dismissViewControllerAnimated:YES completion:nil];
                                         dualRecordController = nil;
                                         eActionType = eAct_None;
                                     }];
        UIAlertAction* record2Act = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Record File 2", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if (playbackRecordFile != eRecordFile2) {
                                             playbackRecordFile = eRecordFile2;
                                             [self pbControlChangeDualRecord:eRecordFile2];
                                         }
                                         else {
                                             if (ePrePlayType == ePLAY_PLAY) {
                                                 [self pbControlPlay];
                                             }
                                         }
                                         [dualRecordController dismissViewControllerAnimated:YES completion:nil];
                                         dualRecordController = nil;
                                         eActionType = eAct_None;
                                     }];
        
        Np_DateTime datetime = [serverMgr playbackGetTime:iCurrentCamera];
        record1Act.enabled = [serverMgr checkPlaybackLogTime:iCurrentCamera recordfile:eRecordFile1 datetime:&datetime];
        record2Act.enabled = [serverMgr checkPlaybackLogTime:iCurrentCamera recordfile:eRecordFile2 datetime:&datetime];
        
        [actionSheet addAction:cancelAct];
        [actionSheet addAction:record1Act];
        [actionSheet addAction:record2Act];
        
        dualRecordController = actionSheet;
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popover = actionSheet.popoverPresentationController;
            if (popover) {
                [popover setPermittedArrowDirections:0];
                popover.sourceView = self.view;
                popover.sourceRect = self.view.bounds;
            }
        }
        [self presentViewController:actionSheet animated:YES completion:nil];
    }else {
        UIActionSheet * action = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Record File", nil) delegate:self cancelButtonTitle:nil destructiveButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:nil];
        
        Np_DateTime datetime = [serverMgr playbackGetTime:iCurrentCamera];
        if ([serverMgr checkPlaybackLogTime:iCurrentCamera recordfile:eRecordFile1 datetime:&datetime]) {
            [action addButtonWithTitle:NSLocalizedString(@"Record File 1", nil)];
        }
        if ([serverMgr checkPlaybackLogTime:iCurrentCamera recordfile:eRecordFile2 datetime:&datetime]) {
            [action addButtonWithTitle:NSLocalizedString(@"Record File 2", nil)];
        }
        
        [action showInView:self.navigationController.view];
        dualRecordSheet = action;
        [action release];
    }
}
#endif
- (void)pbControlPause {
    ePlayType = ePLAY_PAUSE;
    [serverMgr playbackPause:iCurrentCamera];
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}

- (void)pbControlReverse {
    ePlayType = ePLAY_REVERSE;
    
    Np_DateTime datetime = [serverMgr playbackGetTime:iCurrentCamera];
    if ([self checkPlaybackTimeline:datetime]) {
        [serverMgr playbackReversePlay:iCurrentCamera];
    }
    else {
        ePlayType = ePLAY_PAUSE;
        [serverMgr playbackPause:iCurrentCamera];
    }
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}

- (void)pbControlPreviousFrame {
    [self resetHideTimer];
    
    if (ePlayType != ePLAY_PAUSE) {
        ePlayType = ePLAY_PAUSE;
        [serverMgr playbackPause:iCurrentCamera];
    }
    
    Np_DateTime datetime = [serverMgr playbackGetTime:iCurrentCamera];
    if ([self checkPlaybackTimeline:datetime]) {
        [serverMgr playbackPreviousFrame:iCurrentCamera];
    }
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}
- (void)pbControlNextFrame {
    [self resetHideTimer];
    
    if (ePlayType != ePLAY_PAUSE) {
        ePlayType = ePLAY_PAUSE;
        [serverMgr playbackPause:iCurrentCamera];
    }
    
    Np_DateTime datetime = [serverMgr playbackGetTime:iCurrentCamera];
    if ([self checkPlaybackTimeline:datetime]) {
        [serverMgr playbackNextFrame:iCurrentCamera];
    }
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}

// Brosso - time line helper
- (void)pbControlSeekByDate:(NSDate*)seekDate {
    Np_DateTime seekTime;
    memset(&seekTime, 0, sizeof(Np_DateTime));
    NSDate2NpDateTime(seekDate, &seekTime);
    
    [serverMgr playbackSeek:iCurrentCamera seektime:seekTime];
}

// jerrylu, 2013/02/01, fix bug11855
- (void)pbControlSeek:(Np_DateTime)seektime {
    [serverMgr playbackSeek:iCurrentCamera seektime:seektime];
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)pbControlChangeDualRecord:(EDualRecordFile) recordfile {
    if (playbackWaitTimer == nil) {
        playbackWaitTimer = [NSTimer scheduledTimerWithTimeInterval:WAIT_FEEDBACK_INTERVAL
                                                             target:self
                                                           selector:@selector(playbackWaitingAlert)
                                                           userInfo:nil
                                                            repeats:NO];
    }
    isHoldButton = YES;
    if (![imageview.mIndicator isAnimating]) {
        [imageview.mIndicator startAnimating];
    }
    
    [self refreshToolBar];
    
    Np_DateTime seektime = [serverMgr playbackGetTime:iCurrentCamera];
    if (recordfile == eRecordFile1) {
        [serverMgr playbackChangeRecordFile:iCurrentCamera recordfile:recordfile startdate:rec1_starttime enddate:rec1_endtime seektime:seektime];
    }
    else {
        [serverMgr playbackChangeRecordFile:iCurrentCamera recordfile:recordfile startdate:rec2_starttime enddate:rec2_endtime seektime:seektime];
    }
}
#endif
- (BOOL)checkPlaybackTimeline:(Np_DateTime) datetime {
    // Reverse Play
    EServerType eServerType = [serverMgr getServerTypeByChIndex:iCurrentCamera];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (   (   eServerType == eServer_MainConsole
            || eServerType == eServer_NVRmini
            || eServerType == eServer_NVRsolo)
        && playbackRecordFile == eRecordFile2) {
        if (rec2_starttime.minute != 0) {
            if (datetime.hour == rec2_starttime.hour && datetime.minute <= (rec2_starttime.minute+59)%60)
                return NO;
        }
        else {
            if (datetime.hour == (rec2_starttime.hour+23)%24 && datetime.minute >= rec2_starttime.minute)
                return NO;
        }
    }
    else if (   eServerType == eServer_MainConsole
             || eServerType == eServer_NVRmini
             || eServerType == eServer_NVRsolo) {
        if (rec1_starttime.minute != 0) {
            if (datetime.hour == rec1_starttime.hour && datetime.minute <= (rec1_starttime.minute+59)%60)
                return NO;
        }
        else {
            if (datetime.hour == (rec1_starttime.hour+23)%24 && datetime.minute >= rec1_starttime.minute)
                return NO;
        }
    }
    else {
        // jerrylu, 2012/06/14
        if (endrevtime.second == 0) {
            if (endrevtime.minute != 0) {
                if (datetime.hour == endrevtime.hour && datetime.minute <= (endrevtime.minute+59)%60)
                    return NO;
            }
            else {
                if (datetime.hour == (endrevtime.hour+23)%24 && datetime.minute >= endrevtime.minute)
                    return NO;
            }
        }
        else { // jerrylu, 2012/11/08, time interval is less than 1 min
            if (datetime.hour == endrevtime.hour && datetime.minute == endrevtime.minute && datetime.second <= endrevtime.second)
                return NO;
        }
    }
    
    // Play
    if (   (   eServerType == eServer_MainConsole
            || eServerType == eServer_NVRmini
            || eServerType == eServer_NVRsolo)
        && playbackRecordFile == eRecordFile2) {
        if (datetime.hour == rec2_endtime.hour && datetime.minute == rec2_endtime.minute && datetime.second > rec2_endtime.second)
            return NO;
    }
    else {
        if (datetime.hour == rec1_endtime.hour && datetime.minute == rec1_endtime.minute && datetime.second > rec1_endtime.second)
            return NO;
    }
#else
    if (   eServerType == eServer_MainConsole
        || eServerType == eServer_NVRmini
        || eServerType == eServer_NVRsolo) {
        if (rec1_starttime.minute != 0) {
            if (datetime.hour == rec1_starttime.hour && datetime.minute <= (rec1_starttime.minute+59)%60)
                return NO;
        }
        else {
            if (datetime.hour == (rec1_starttime.hour+23)%24 && datetime.minute >= rec1_starttime.minute)
                return NO;
        }
    }
    else {
        // jerrylu, 2012/06/14
        if (endrevtime.second == 0) {
            if (endrevtime.minute != 0) {
                if (datetime.hour == endrevtime.hour && datetime.minute <= (endrevtime.minute+59)%60)
                    return NO;
            }
            else {
                if (datetime.hour == (endrevtime.hour+23)%24 && datetime.minute >= endrevtime.minute)
                    return NO;
            }
        }
        else { // jerrylu, 2012/11/08, time interval is less than 1 min
            if (datetime.hour == endrevtime.hour && datetime.minute == endrevtime.minute && datetime.second <= endrevtime.second)
                return NO;
        }
    }
    
    // Play
    if (datetime.hour == rec1_endtime.hour && datetime.minute == rec1_endtime.minute && datetime.second > rec1_endtime.second)
        return NO;
#endif
    return YES;
}

// jerrylu, 2012/06/04
- (void)playbackMenuPressed {
    [self pbControlPause];
    [self.navigationController pushViewController:pPbMenuController animated:YES];
}

- (void)playbackErrorHandle:(Np_Error) error {
    [m_errorCondition lock];
    
    if (isPlaybackError) {
        return;
    }
    
    [self playbackEnd];
    [self showAlert:error];
    isPlaybackError = YES;
    
    [m_errorCondition unlock];
}

- (void)playbackErrorHandleForNoData {
    [self playbackErrorHandle:Np_ERROR_SESSION_NODATA];
}

- (void)playbackErrorHandleForConnectionError {
    [self playbackErrorHandle:Np_ERROR_FATAL_ERROR];
}

- (void)playbackDisconnection {
    [self performSelectorOnMainThread:@selector(playbackErrorHandleForConnectionError) withObject:nil waitUntilDone:NO];
}

// jerrylu, 2012/10/09, fix bug7993
- (void)playbackWaitingAlert
{
    [playbackWaitTimer invalidate];
    playbackWaitTimer = nil;
    
    NSString *errorMsg;
    
    if (serverMgr.isUnsupportedFormat)
        errorMsg = @"Unable to load recorded video - unsupported video format";
    else
        errorMsg = @"Unable to load recorded video - insufficient bandwidth";
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        playbackWaitAlertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(errorMsg, nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [playbackWaitAlertController dismissViewControllerAnimated:YES completion:nil];
                                 playbackWaitAlertController = nil;
                                 playbackButton.enabled = YES;
                                 dualRecButton.enabled = YES;
                                 if ([imageview.mIndicator isAnimating])
                                 {
                                     [imageview.mIndicator stopAnimating];
                                 }

                                 if (serverMgr.isUnsupportedFormat)
                                     [self performSelectorOnMainThread:@selector(playbackEnd) withObject:nil waitUntilDone:NO];
                             }];
        [playbackWaitAlertController addAction:ok];
        [self presentViewController:playbackWaitAlertController animated:YES completion:nil];
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(errorMsg, nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        [alert release];
        playbackWaitAlert = alert;

        if (serverMgr.isUnsupportedFormat)
            [self performSelectorOnMainThread:@selector(playbackEnd) withObject:nil waitUntilDone:NO];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if ([alertView.message hasPrefix:NSLocalizedString(@"Unable to load recorded video - insufficient bandwidth", nil)]) {
        playbackWaitAlert = nil;
    }
}

- (void)dismissPlaybackWaitingAlert {
    if((NSNull *)playbackWaitAlert != [NSNull null])
        [playbackWaitAlert dismissWithClickedButtonIndex:-1 animated:NO];
    
    if((NSNull *)playbackWaitAlertController != [NSNull null]) {
        [playbackWaitAlertController dismissViewControllerAnimated:NO completion:nil];
        playbackWaitAlertController = nil;
    }
}

- (void)playbackHoldScreen:(BOOL) isHoldScreen {
    if (isHoldScreen) {
        isHoldButton = YES;
        if (![imageview.mIndicator isAnimating]) {
            [imageview.mIndicator performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:NO];
        }
    }
    else {
        isHoldButton = NO;
        if ([imageview.mIndicator isAnimating]) {
            [imageview.mIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
        }
    }
    [self performSelectorOnMainThread:@selector(refreshToolBar) withObject:nil waitUntilDone:NO];
}

#pragma mark -
#pragma mark General action methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger) buttonIndex {
	if (eActionType == eAct_Snapshot) {
		switch (buttonIndex) {
			case 1:
				[self snapshotToFile];
				break;
			case 2:
				[self snapshotToEmail];
				break;
			default:
				break;
		}
        snapshotSheet = nil;
        
        if (eViewModeType == eViewMode_DigitalPTZ) { // jerrylu, 2012/11/19, fix bug10161
            [self resetHideTimer];
        }
	}
	else if (eActionType == eAct_Preset) {
		int iPresetNO = buttonIndex - 1;
		if (iPresetNO >= 0 && iPresetNO < [[serverMgr getPresetNameArrayByChIndex:iCurrentCamera] count]) {
			[self sendPresetAtNO:iPresetNO];
		}
		presetSheet = nil;
        
        if (eViewModeType == eViewMode_PhysicalPTZ) { // jerrylu, 2012/11/19, fix bug10161
            [self resetHideTimer];
        }
	}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    else if (eActionType == eAct_DualRecord) {
        switch (buttonIndex) {
            case 1:
                if (playbackRecordFile != eRecordFile1) {
                    playbackRecordFile = eRecordFile1;
                    [self pbControlChangeDualRecord:playbackRecordFile];
                }
                else {
                    if (ePrePlayType == ePLAY_PLAY) {
                        [self pbControlPlay];
                    }
                }
                break;
            case 2:
                if (playbackRecordFile != eRecordFile2) {
                    playbackRecordFile = eRecordFile2;
                    [self pbControlChangeDualRecord:playbackRecordFile];
                }
                else {
                    if (ePrePlayType == ePLAY_PLAY) {
                        [self pbControlPlay];
                    }
                }
                break;
            default:
                if (ePrePlayType == ePLAY_PLAY) {
                    [self pbControlPlay];
                }
                break;
        }
        
        dualRecordSheet = nil;
    }
#endif
	eActionType = eAct_None;
}

- (void)hideArrow:(BOOL) hide {
    [arrowUp setHidden:hide];
    [arrowDown setHidden:hide];
    [arrowLeft setHidden:hide];
    [arrowRight setHidden:hide];
}

- (void)hideUI{
    [self hideUIWithValue:TRUE];
}

- (void)hideUIWithValue:(BOOL) hide {
    UIViewController *lastview = (UIViewController *)[[self.navigationController viewControllers] lastObject];
    if (![lastview isKindOfClass:[SingleCamViewController class]])
        return;
    
    [[UIApplication sharedApplication] setStatusBarHidden:hide];
    [self.navigationController setNavigationBarHidden:hide animated:YES];
#if SHOW_FRAMERATE_FLAG
    [imageview.mLabel setHidden:NO];
#else
    [imageview.mLabel setHidden:hide];
#endif
    [self.navigationController setToolbarHidden:hide animated:hide];
    
    if (eViewModeType == eViewMode_DigitalPTZ || eViewModeType == eViewMode_None) {
#if RENDER_BY_OPENGL
        if (zoomLevel > 1.0)
#else
        if (zoomLevel > 0)
#endif
        {
            [pagecontrol setHidden:YES];
        }
        else {
            [pagecontrol setHidden:hide]; // jerrylu, 2012/10/01
        }
        [mLayoutToolbar setHidden:hide];
        
        // Brosso - Hide TimeLine
        if (!hide) {
            [self hideTimeLine];
        }
        
    }
}

- (void)stopHideTimer {
    if (hideUITimer != nil && [hideUITimer isValid]) {
        [hideUITimer invalidate];
        while ([hideUITimer isValid]) {
            [NSThread sleepForTimeInterval:0.5];
        }
        hideUITimer = nil;
    }
}

- (void)resetHideTimer { // Comment out for testing - Brosso
//    [self stopHideTimer];
//    
//    if (hideUITimer == nil) {
//        hideUITimer = [[NSTimer scheduledTimerWithTimeInterval:HIDE_UI_INTERVAL
//                                                       target:self
//                                                     selector:@selector(hideUI)
//                                                     userInfo:nil
//                                                      repeats:YES] retain];
//    }
}

- (void)showAlert:(Np_Error) error {
    NSString *msg = nil;
    
    switch (error) {
        case Np_ERROR_SESSION_NODATA:
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            if (   [serverMgr getServerTypeByChIndex:iCurrentCamera] == eServer_MainConsole
                || [serverMgr getServerTypeByChIndex:iCurrentCamera] == eServer_NVRmini
                || [serverMgr getServerTypeByChIndex:iCurrentCamera] == eServer_NVRsolo) {
                if (playbackRecordFile == eRecordFile1)
                    msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"Record 1: No Data", nil)];
                else
                    msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"Record 2: No Data", nil)];
            }
            else {
#endif
                msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"No Data", nil)];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            }
#endif
            break;
        case Np_ERROR_FATAL_ERROR:
            msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"Please check the playback service is on and you have the permission", nil)];
            break;
        case Np_ERROR_CONNECT_SUCCESS:
        case Np_ERROR_DISCONNECT_SUCCESS:
        case Np_ERROR_SESSION_LOST:
        default:
            break;
    }
    
    if (msg != nil) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
            [alert release];
        }
        [msg release];
    }
}

#if RENDER_BY_OPENGL
- (void)imageBufDidRefreshed:(MyVideoFrameBuf *) pic {
    if (eViewModeType == eViewMode_None) {
        if ([serverMgr getLiveViewConnection:iCurrentCamera]) {
            [self changeTODigitalPTZ];
        }else {
            return;
        }
    }
    else if (eViewModeType == eViewMode_PlaybackDefault) {
        if ([serverMgr getPlaybackConnection:iCurrentCamera]) {
            [self dismissPlaybackWaitingAlert];
            [self changeTOPlaybackMode];
        }
        else {
            return;
        }
    }
    else if (eViewModeType == eViewMode_Playback) {
        if (isHoldButton) {
            [self playbackHoldScreen:NO];
            
            if (ePrePlayType == ePLAY_PLAY) {
                [self pbControlPlay];
            }
        }
    }
    
    if (!isHoldButton && [imageview.mIndicator isAnimating]) {
		[imageview.mIndicator stopAnimating];
	}
    
	readyToPTZ = YES;
    
    if (ePreViewModeType != eViewModeType) {
        ePreViewModeType = eViewModeType;
        [self refreshToolBar];
    }
    
    [imageview.image clearFrameBuffer];
    [imageview.image setFrameBuf:(uint8_t *)[pic.data bytes] width:[pic width] height:[pic height]];
    [imageview.image RenderToHardware:nil];
}
#else
- (void)imageDidRefreshed:(UIImage *) pic {
    if (eViewModeType == eViewMode_None) {
        if ([serverMgr getLiveViewConnection:iCurrentCamera]) {
            [self changeTODigitalPTZ];
        }else {
            return;
        }
    }
    else if (eViewModeType == eViewMode_PlaybackDefault) {
        if ([serverMgr getPlaybackConnection:iCurrentCamera]) {
            [self dismissPlaybackWaitingAlert];
            [self changeTOPlaybackMode];
        }
        else {
            return;
        }
    }
    
    if (!isHoldButton && [imageview.mIndicator isAnimating]) {
		[imageview.mIndicator stopAnimating];
	}
    
	readyToPTZ = YES;    
    
    if (ePreViewModeType != eViewModeType) {
        ePreViewModeType = eViewModeType;
        [self refreshToolBar];
    }
    
	CGImageRef imgRef = pic.CGImage;
	imgSize.width = CGImageGetWidth(imgRef);
	imgSize.height = CGImageGetHeight(imgRef);
#if DEBUG_LOG
	NSLog(@"img size %f %f", imgSize.width, imgSize.height);
#endif
	if (zoomLevel == 0) {
		offsetPos = CGPointZero;
		[imageview.image setImage:pic];
		return;
	}
    
	// 1 check the boundary
	if (offsetPos.x < 0) {
		offsetPos.x = 0;
	}
	if (offsetPos.y < 0) {
		offsetPos.y = 0;
	}
#if DEBUG_LOG
	NSLog(@"change to %f %f", offsetPos.x, offsetPos.y);
#endif
	float zoomValue = pow(ZOOM_RATIO, zoomLevel);
	int overSize = offsetPos.x + imgSize.width - (zoomValue * imgSize.width);
	if (overSize > 0) {
		offsetPos.x -= overSize;
	}
    
	overSize = offsetPos.y + imgSize.height - (zoomValue * imgSize.height);
	if (overSize > 0) {
		offsetPos.y -= overSize;
	}
	// 2 make the translation
#if DEBUG_LOG
	NSLog(@"%f %f", -offsetPos.x, -offsetPos.y);
#endif
    CGAffineTransform transform = CGAffineTransformMakeTranslation(-offsetPos.x, -offsetPos.y);
	// 3 make the scale
	transform = CGAffineTransformScale(transform, zoomValue, zoomValue);
    CGRect bounds = CGRectMake(0, 0, imgSize.width, imgSize.height);
	
	UIGraphicsBeginImageContext(bounds.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextScaleCTM(context, 1.0, -1.0);
	CGContextTranslateCTM(context, 0, -imgSize.height);
	
	CGContextConcatCTM(context, transform);
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, imgSize.width, imgSize.height), imgRef);
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	[imageview.image setImage:imageCopy];
}
#endif

- (void)imageDidnotRefreshed {
    if (![imageview.mIndicator isAnimating]) {
        [imageview.mIndicator startAnimating];
    }
    // jerrylu, 2012/07/05, fix bug7763
    if (ePreViewModeType != eViewMode_None && eViewModeType != eViewMode_PlaybackDefault && eViewModeType != eViewMode_Playback) {
#if RENDER_BY_OPENGL
        [imageview.image clearFrameBuffer];
        [imageview.image RenderToHardware:nil];
        [imageview.image setDefaultPic];
#else
        UIImage * imgDefault = [UIImage imageNamed:@"novideo320x240_noword.png"];
        CGImageRef imgRef = imgDefault.CGImage;
        imgSize.width = CGImageGetWidth(imgRef);
        imgSize.height = CGImageGetHeight(imgRef);
        [imageview.image setImage:imgDefault];
#endif
        [self changeTONonePTZ];
        ePreViewModeType = eViewMode_None;
    }
}

// jerrylu, 2012/07/05
- (void)checkLiveViewStatus {
    // refresh time and status of the player
    while (TRUE) {
        [NSThread sleepForTimeInterval:1];
        
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
        
        if (serverMgr != nil) {
            if ([serverMgr isServerLogoutingByIndex:iCurrentCamera]) {
                [NSThread exit];
                return;
            }
        }
        else {
            [NSThread exit];
            return;
        }
        
        if (eViewModeType == eViewMode_DigitalPTZ || eViewModeType == eViewMode_None) {
            BOOL isNeedChangeSetting = NO;
            
            if (eViewModeType == eViewMode_DigitalPTZ) {
                if([serverMgr getLiveViewAudioStateByChIndex:iCurrentCamera] != liveviewaudioenable) {
                    isNeedChangeSetting = YES;
#if DEBUG_LOG
                    NSLog(@"-------getLiveViewAudioStatusOnCam != liveviewaudioenable");
                    NSLog(@"-------getLiveViewAudioStatusOnCam %d, liveviewaudioenable %d",[serverMgr getLiveViewAudioStateByChIndex:iCurrentCamera], liveviewaudioenable);
#endif
                    if (liveviewaudioenable) {
                        [serverMgr setLiveviewAudioStateByChIndex:iCurrentCamera state:YES];
                    }else {
                        [serverMgr setLiveviewAudioStateByChIndex:iCurrentCamera state:NO];
                    }
                }
                else if (audioWaitTimer != nil) {
                    [audioWaitTimer invalidate];
                    audioWaitTimer = nil;
                }
                
                if([serverMgr getTalkStateByChIndex:iCurrentCamera] == talkenable) {
                    if (talkWaitTimer != nil) {
                        [talkWaitTimer invalidate];
                        talkWaitTimer = nil;
                    }
                }
            }
            
            // jerrylu, 2012/09/06, fix bug8410 
            if([serverMgr getConnectProfileTypeWithChIndex:iCurrentCamera] != eProfile) {
                isNeedChangeSetting = YES;
#if DEBUG_LOG
                NSLog(@"-------getProfileTypeWithCam != eProfile");
                NSLog(@"-------getProfileTypeWithCam %d, eProfile %d",[serverMgr getConnectProfileTypeWithChIndex:iCurrentCamera], eProfile);
#endif
                [serverMgr setConnectProfileTypeWithChIndex:iCurrentCamera profiletype:eProfile];
            }
            else if (profileWaitTimer != nil) {
                [profileWaitTimer invalidate];
                profileWaitTimer = nil;
            }
            
            if (!isNeedChangeSetting && isHoldButton) {
                isHoldButton = NO;
                if ([imageview.mIndicator isAnimating]) {
                    [imageview.mIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
                }
                [self performSelectorOnMainThread:@selector(refreshToolBar) withObject:nil waitUntilDone:NO];
            }
        }
    }
}

- (void)checkPlaybackStatus {
    // refresh time and status of the player
    while (TRUE) {
        [NSThread sleepForTimeInterval:0.2];
        
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
        
        if (serverMgr != nil) {
            if ([serverMgr isServerLogoutingByIndex:iCurrentCamera]) {
                [NSThread exit];
                return;
            }
        }
        else {
            [NSThread exit];
            return;
        }
        
        if (eViewModeType == eViewMode_Playback && !isHoldButton) {
            currenttime = [serverMgr playbackGetTime:iCurrentCamera];
            Np_DateTime datetime = [serverMgr playbackGetTime:iCurrentCamera];
            NSString *datestring = [[NSString alloc] initWithFormat:@"%d/%02d/%02d %02d:%02d:%02d", datetime.year, datetime.month, datetime.day, datetime.hour, datetime.minute, datetime.second];
//            [imageview.mLabel performSelectorOnMainThread:@selector(setText:) withObject:datestring waitUntilDone:NO];
            
            // Brosso - update time line text as well
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"YYYY/MM/dd HH:mm:ss"];
            [df setCalendar:[NSCalendar currentCalendar]];
            NSDate *displayDate = [df dateFromString:datestring];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [imageview.mLabel setText:datestring];

                if (pTimeLineViewController && ePLAY_PAUSE != ePlayType) {
                    [pTimeLineViewController enableAutoScrollView];
                    [pTimeLineViewController syncScrollViewWithDate:displayDate];
                }
            });
            
            [datestring release];
            [df release];

            
            // Brosso - don't need auto stop
//            if ([self checkPlaybackTimeline:datetime] == NO) {
//                [self playbackReopenRecord];
//                [self performSelectorOnMainThread:@selector(refreshToolBar) withObject:nil waitUntilDone:NO];
//            }
            
            Np_PlayerState tmpstate = [serverMgr playbackGetState:iCurrentCamera];
            if (playbackstate != tmpstate) {
                playbackstate = tmpstate;
                
                if (tmpstate == kStateStopped)
                {
                    [self playbackReopenRecord];
                }
                
                [self performSelectorOnMainThread:@selector(refreshToolBar) withObject:nil waitUntilDone:NO];
            }
            
            float currentspeed = [serverMgr playbackGetSpeed:iCurrentCamera];
            if (currentspeed != playbackSpeed) {
                [serverMgr playbackSetSpeed:iCurrentCamera speed:playbackSpeed];
            }
        }
    }
}

- (int)getCurrentChIndex {
    return iCurrentCamera;
}

#if SHOW_FRAMERATE_FLAG
- (void)showframerate {
    NSString *framrate_str = [[NSString alloc] initWithFormat:@"fps:%lf", [serverMgr getLiveViewFrameRate:iCurrentCamera]];
    [imageview.mLabel setText:framrate_str];
}
#endif

#pragma mark -
#pragma mark ControlHelperPresetDelegate protocal

- (void)ControlHelper:(ControlHelper *) helper didGetPresetOnCamera:(int) index {
    if (eViewModeType != eViewMode_PhysicalPTZ) {
        return;
    }
	presetButton.enabled = YES;
    readyToPreset = YES;
}

#pragma mark -
#pragma mark serverManagerPlaybackDelegate protocal

- (void)serverManagerAlertPlaybackEvent:(ServerManager *)servermgr event:(EPlaybackEvent)event {
    switch (event) {
        case ePlayback_NoData:
            [self performSelectorOnMainThread:@selector(playbackErrorHandleForNoData) withObject:nil waitUntilDone:NO];
            break;
        case ePlayback_ConnectError:
            [self performSelectorOnMainThread:@selector(playbackErrorHandleForConnectionError) withObject:nil waitUntilDone:NO];
            break;            
        default:
            break;
    }
}

- (void)serverManagerUnsupportedFormat {
    [self playbackWaitingAlert];
}

#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)serverManagerRefreshPlaybackTime:(ServerManager *)servermgr recordfile:(EDualRecordFile)rec startTime:(Np_DateTime *)newStartTime endTime:(Np_DateTime *)newEndTime {
    if (rec == eRecordFile2) {
        rec2_starttime.year = newStartTime->year;
        rec2_starttime.month = newStartTime->month;
        rec2_starttime.day = newStartTime->day;
        rec2_starttime.hour = newStartTime->hour;
        rec2_starttime.minute= newStartTime->minute;
        rec2_starttime.second = newStartTime->second;
        rec2_starttime.millisecond = newStartTime->millisecond;
        
        rec2_endtime.year = newEndTime->year;
        rec2_endtime.month = newEndTime->month;
        rec2_endtime.day = newEndTime->day;
        rec2_endtime.hour = newEndTime->hour;
        rec2_endtime.minute= newEndTime->minute;
        rec2_endtime.second = newEndTime->second;
        rec2_endtime.millisecond = newEndTime->millisecond;
    }
    else {
        rec1_starttime.year = newStartTime->year;
        rec1_starttime.month = newStartTime->month;
        rec1_starttime.day = newStartTime->day;
        rec1_starttime.hour = newStartTime->hour;
        rec1_starttime.minute= newStartTime->minute;
        rec1_starttime.second = newStartTime->second;
        rec1_starttime.millisecond = newStartTime->millisecond;
        
        rec1_endtime.year = newEndTime->year;
        rec1_endtime.month = newEndTime->month;
        rec1_endtime.day = newEndTime->day;
        rec1_endtime.hour = newEndTime->hour;
        rec1_endtime.minute= newEndTime->minute;
        rec1_endtime.second = newEndTime->second;
        rec1_endtime.millisecond = newEndTime->millisecond;
    }
}
#endif
- (void)serverManagerRefreshPlaybackTime:(ServerManager *)servermgr startTime:(Np_DateTime *)newStartTime endTime:(Np_DateTime *)newEndTime {
    rec1_starttime.year = newStartTime->year;
    rec1_starttime.month = newStartTime->month;
    rec1_starttime.day = newStartTime->day;
    rec1_starttime.hour = newStartTime->hour;
    rec1_starttime.minute= newStartTime->minute;
    rec1_starttime.second = newStartTime->second;
    rec1_starttime.millisecond = newStartTime->millisecond;
    
    rec1_endtime.year = newEndTime->year;
    rec1_endtime.month = newEndTime->month;
    rec1_endtime.day = newEndTime->day;
    rec1_endtime.hour = newEndTime->hour;
    rec1_endtime.minute= newEndTime->minute;
    rec1_endtime.second = newEndTime->second;
    rec1_endtime.millisecond = newEndTime->millisecond;
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)serverManagerSupportDualRecordPlayback:(ServerManager *)servermgr {
    isDualRecording = YES;
    [self refreshToolBar];
}
#endif

#pragma mark -
#pragma mark PlaybackViewController delegate methods
- (void)PbViewControllerOpenRecord {
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    isDualRecording = NO;
#endif
    NSThread *playbackThread = [[NSThread alloc] initWithTarget:self selector:@selector(playbackPreStart) object:NULL];
    [playbackThread start];
    [playbackThread release];
    
    [parent.slideView resetImageOfCamIndex:iCurrentCamera];
    [self performSelectorOnMainThread:@selector(changeTOPlaybackDefaultMode) withObject:nil waitUntilDone:NO];
    
    if (talkenable) {
        [serverMgr setTalkStateByChIndex:iCurrentCamera state:NO];
        talkenable = NO;
    }
}

#pragma mark -
#pragma mark PlaybackMenuViewController delegate methods
- (void)PbMenuViewControllerAudioChange {
    [self audiochange];
}

- (void)PbMenuViewControllerSnapshot {
    [self snapshot];
}

#pragma mark -
#pragma mark PinchSlideImageViewDelegate protocal

- (void) onPSImageView: (PinchSlideImageView*)view Zooming: (BOOL) isZoomIn Center:(CGPoint) center {
#if DEBUG_LOG
	NSLog(@"zoom 1 = in 0 = out %d", isZoomIn);
#endif
    // jerrylu, 2012/06/28, support zooming in Playbackmode
	if (eViewModeType == eViewMode_DigitalPTZ || eViewModeType == eViewMode_Playback) {
		if (!isZoomIn && zoomLevel + 1 < MAX_ZOOM_RATIO_NUM) {
			zoomLevel += 1;
		}
		else if (isZoomIn && zoomLevel > 0) {
			zoomLevel -= 1;
		}
		else {
			return;
		}
        
        if (eViewModeType == eViewMode_DigitalPTZ) {
            if (zoomLevel > 0) { // jerrylu, 2012/10/17, couldn't flip the 1x1 view when zoom-in
                self.scrollView.scrollEnabled = NO;
                [pagecontrol setHidden:YES];
            }
            else {
                self.scrollView.scrollEnabled = YES;
                [pagecontrol setHidden:NO];
            }
        }
		offsetPos.x = center.x;
		offsetPos.y = center.y;
	}
	else if (eViewModeType == eViewMode_PhysicalPTZ) {
		NSString * cmd;
        bool bNeedSend = true;
		if (!isZoomIn) {
			cmd = @"ZoomTele";
            if ([stopCmd isEqual:@"ZoomTeleStop"]) {
                bNeedSend = false;
            }
            stopCmd = @"ZoomTeleStop";
		}
		else {
			cmd = @"ZoomWide";
            if ([stopCmd isEqual:@"ZoomWideStop"]) {
                bNeedSend = false;
            }
            stopCmd = @"ZoomWideStop";
		}
        
        if (bNeedSend) {
            [self sendPTZWithOp:cmd];
        }
        
        [self hideArrow:YES];
	}
}

- (void)onPSImageView:(PinchSlideImageView *) view Zooming:(BOOL) isZoomIn {
#if DEBUG_LOG
	NSLog(@"zoom 1 = in 0 = out %d", isZoomIn);
#endif
    // jerrylu, 2012/06/28, support zooming in Playbackmode
	if (eViewModeType == eViewMode_DigitalPTZ || eViewModeType == eViewMode_Playback) {
		float preValue = pow((double) ZOOM_RATIO, (double) zoomLevel);
		if (!isZoomIn && zoomLevel + 1 < MAX_ZOOM_RATIO_NUM) {
			zoomLevel += 1;
		}
		else if (isZoomIn && zoomLevel > 0) {
			zoomLevel -= 1;
		}
		else {
			return;
		}
        
        if (eViewModeType == eViewMode_DigitalPTZ) {
            if (zoomLevel > 0) { // jerrylu, 2012/10/17, couldn't flip the 1x1 view when zoom-in
                self.scrollView.scrollEnabled = NO;
                [pagecontrol setHidden:YES];
            }
            else {
                self.scrollView.scrollEnabled = YES;
                [pagecontrol setHidden:NO];
            }
        }

		float zoomValue = pow((double) ZOOM_RATIO, (double) zoomLevel);

		int offsetX = (zoomValue - preValue) * imgSize.width / 2;
		int offsetY = (zoomValue - preValue) * imgSize.height / 2;
		offsetPos.x += offsetX;
		offsetPos.y += offsetY;
	}
	else if (eViewModeType == eViewMode_PhysicalPTZ) {
		NSString * cmd;
        bool bNeedSend = true;
		if (!isZoomIn) {
			cmd = @"ZoomTele";
            if ([stopCmd isEqual:@"ZoomTeleStop"]) {
                bNeedSend = false;
            }
            stopCmd = @"ZoomTeleStop";
		}
		else {
			cmd = @"ZoomWide";
            if ([stopCmd isEqual:@"ZoomWideStop"]) {
                bNeedSend = false;
            }
            stopCmd = @"ZoomWideStop";
		}
        
        if (bNeedSend) {
            [self sendPTZWithOp:cmd];
        }
        
        [self hideArrow:YES];
	}
}

- (void)onPSImageView:(PinchSlideImageView *) view SlideOnDirection:(int) dir offset:(CGPoint) offsetValue {
    // jerrylu, 2012/06/28, support slide in Playbackmode
	if (eViewModeType == eViewMode_DigitalPTZ || eViewModeType == eViewMode_Playback) {
		int shiftX = offsetValue.x * imgSize.width / imageview.image.frame.size.width;
		int shiftY = offsetValue.y * imgSize.height / imageview.image.frame.size.height;
		offsetPos.x -= shiftX;
		offsetPos.y += shiftY;
	}
	else if (eViewModeType == eViewMode_PhysicalPTZ) {
		NSString * cmd;
		switch (dir) {
			case eSlideTiltUp:
				cmd	= @"TiltUp";
				stopCmd = @"TiltUpStop";
				break;
			case eSlideTiltDown:
				cmd = @"TiltDown";
				stopCmd = @"TiltDownStop";
				break;
			case eSlidePanRight:
				cmd = @"PanRight";
				stopCmd = @"PanRightStop";
				break;
			case eSlidePanLeft:
				cmd = @"PanLeft";
				stopCmd = @"PanLeftStop";
				break;
			case eSlideUpRight:
				cmd	= @"UpRight";
				stopCmd = @"UpRightStop";
				break;
			case eSlideUpLeft:
				cmd = @"UpLeft";
				stopCmd = @"UpLeftStop";
				break;
			case eSlideDownLeft:
				cmd = @"DownLeft";
				stopCmd = @"DownLeftStop";
				break;
			case eSlideDownRight:
				cmd = @"DownRight";
				stopCmd = @"DownRightStop";
				break;
			case eSlideHome:
				cmd = @"Home";
				stopCmd = @"HomeStop";
				break;		
			default:
				cmd = nil;
				break;
		}
		if (cmd != nil) {
			[self sendPTZWithOp:cmd];
		}
        
        [self hideArrow:YES];
	}
}

- (void)onPSImageViewSingleTap:(PinchSlideImageView *) view {
    [self hideUIWithValue:FALSE];
}

- (void)onPSImageViewDoubleTap:(PinchSlideImageView *) view {
	if (eViewModeType == eViewMode_DigitalPTZ || eViewModeType == eViewMode_Playback) { // jerrylu, 2012/11/21, fix bug9800
		offsetPos = CGPointZero;
		zoomLevel = 0;
        
        if (eViewModeType == eViewMode_DigitalPTZ) { // jerrylu, 2012/11/23
            self.scrollView.scrollEnabled = YES;
            [pagecontrol setHidden:NO];
        }
	}
	else if (eViewModeType == eViewMode_PhysicalPTZ) {
		NSString * cmd = @"Home";
		[self sendPTZWithOp:cmd];
	}
}

- (void)onPSImageViewDidBeginTouch:(PinchSlideImageView *) view {
	[self stopHideTimer];
}

- (void)onPSImageViewDidEndTouch:(PinchSlideImageView *) view {
	if (eViewModeType == eViewMode_DigitalPTZ) {
		
	}
	else if (eViewModeType == eViewMode_PhysicalPTZ) {
		if (stopCmd != nil && ![stopCmd isEqual:@""]) {
#if DEBUG_LOG
			NSLog(@"stop");
#endif
			[self sendPTZWithOp:stopCmd];
            stopCmd = @"";
		}
        
        [self hideArrow:NO];
	}
    
    [self resetHideTimer]; // jerrylu, 2012/10/18
}

#pragma mark -
#pragma mark LiveViewEventListContorlDelegate Methods

- (void)lvctrlFromEventListToLiveView {
    for (int i=0; i<iNumberOfCamera;i++)
        [parent.slideView resetImageOfCamIndex:i];
    [serverMgr connectToCamWithHighResolution:iCurrentCamera];
}

#pragma mark -
#pragma mark LiveViewEventViewContorlDelegate Methods

- (void)lvctrlFromEventViewToLiveView:(int) camIndex {
    iCurrentCamera = camIndex;
    [self loadScrollViewWithCameraIndex:camIndex];
    [self scrollViewChangeCamera];
    
    //pop LiveViewEventView page
    [self.navigationController popToViewController:self animated:NO];
}

#pragma mark -
#pragma mark Picker Data Source Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    return [playbackSpeedList count];
}

#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    return [playbackSpeedList objectAtIndex:row];
}

#pragma mark -
#pragma mark Popover Delegate Method
- (void)dismissAllPopover {
    if (IS_IPAD) {
        [pPlaybackPopover dismissPopoverAnimated:YES];
        [pPlaybackMenuPopover dismissPopoverAnimated:YES];
        [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
        [pLiveViewMenuPopover dismissPopoverAnimated:YES]; // jerrylu, 2012/08/27
    }
    return;
}

- (void)showPlaybackPopover {
    if (IS_IPAD) {
        [self pbControlPause]; // jerrylu, 2012/11/26, fix bug10244
        
        if (pPlaybackPopover.popoverVisible == NO) {
            [pPlaybackPopover presentPopoverFromBarButtonItem:playbackButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else {
            [pPlaybackPopover dismissPopoverAnimated:YES];
        }
    }
    return;
}

- (void)dismissPlaybackPopover {
    if (IS_IPAD) {
        [pPlaybackPopover dismissPopoverAnimated:YES];
    }
    return;
}

- (void)showPlaybackMenuPopover {
    if (IS_IPAD) {
        if (pPlaybackMenuPopover.popoverVisible == NO) {
            [pPlaybackMenuPopover presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else {
            [pPlaybackMenuPopover dismissPopoverAnimated:YES];
        }
    }
    return;
}

- (void)dismissPlaybackMenuPopover {
    if (IS_IPAD) {
        [pPlaybackMenuPopover dismissPopoverAnimated:YES];
    }
    return;
}

- (void)showLiveViewMenuPopover {
    if (IS_IPAD) {
#if FUNC_PUSH_NOTIFICATION_SUPPORT
        if (pLiveViewMenuPopover.popoverVisible == NO) {
            pLvMenuController = [[[SlideViewMenuController alloc] initWithStyle:UITableViewStyleGrouped parentview:self] autorelease];
            pLvMenuController.serverMgr = serverMgr;
            UINavigationController * navigation = [[UINavigationController alloc] initWithRootViewController:pLvMenuController];
            pLiveViewMenuPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
            [pLiveViewMenuPopover setContentViewController:navigation];
            pLiveViewMenuPopover.delegate = self;
            [navigation release];
            
            [pLiveViewMenuPopover presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else {
            [pLiveViewMenuPopover dismissPopoverAnimated:YES];
        }
#else
        if (pLiveViewMenuPopover.popoverVisible == NO) {
            DOTable = [[[DOTableViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
            [DOTable setParent:self];
            [DOTable setServerMgr:serverMgr];
            [DOTable setTitle:NSLocalizedString(@"Digital Output", nil)];
            UINavigationController * navigation = [[UINavigationController alloc] initWithRootViewController:DOTable];
            pLiveViewMenuPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
            [pLiveViewMenuPopover setContentViewController:navigation];
            pLiveViewMenuPopover.delegate = self;
            [navigation release];
            
            [pLiveViewMenuPopover presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else {
            [pLiveViewMenuPopover dismissPopoverAnimated:YES];
        }
#endif
    }
    return;
}

- (void)dismissLiveViewMenuPopover {
    if (IS_IPAD) {
        [pLiveViewMenuPopover dismissPopoverAnimated:YES];
    }
    return;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *) popoverController {
    if (IS_IPAD) {
        [pPlaybackController.navigationController popViewControllerAnimated:NO];
        [pPbMenuController.navigationController popViewControllerAnimated:NO];
#if FUNC_PUSH_NOTIFICATION_SUPPORT
        [pLvMenuController.navigationController popViewControllerAnimated:NO];
#else
        [DOTable.navigationController popViewControllerAnimated:NO];
#endif
    }
    return;
}

#pragma mark -
#pragma mark scrollView methods
- (void)loadScrollViewWithCameraIndex:(int) index {
    if (index < 0 ||
		index >= iNumberOfCamera)
		return;

    SinglePageViewController_1X1 *controller = [viewControllers objectAtIndex:index];
    if ((NSNull *) controller == [NSNull null]) {
        controller = [[[SinglePageViewController_1X1 alloc] initwithCamIndex:index SingleCamParent:self] autorelease];
        
        // replace the placeholder if necessary
        [viewControllers replaceObjectAtIndex:index withObject:controller];
    }
    
    if (controller.view.superview == nil) {
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * index;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [scrollView addSubview:controller.view];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *) sender {
    [self hideUIWithValue:FALSE];
    
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    // If user just rotate, then we don't need to count scroll view change.
    if (bNoChangePage == NO) { // jerrylu, 2013/01/04
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        if (page != iCurrentCamera) {
            needChangeCams = YES;
        }
        iCurrentCamera = page;
        pPbMenuController.chIndex = iCurrentCamera;
	}
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithCameraIndex:iCurrentCamera - 1];
    [self loadScrollViewWithCameraIndex:iCurrentCamera];
	if (iCurrentCamera >= 0 && iCurrentCamera < iNumberOfCamera) {
#if RENDER_BY_OPENGL
        SinglePageViewController_1X1 * preController = [viewControllers objectAtIndex:iCurrentCamera];
        [preController.image clearFrameBuffer];
        [preController.image RenderToHardware:nil];
        [preController.image setDefaultPic];
#else
        SinglePageViewController_1X1 * preController = [viewControllers objectAtIndex:iCurrentCamera];
        UIImage * imgDefault = [UIImage imageNamed:@"novideo320x240_noword.png"];
        [preController.image setImage:imgDefault];
#endif
	}
    [self loadScrollViewWithCameraIndex:iCurrentCamera + 1];
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *) scrollView {
    bNoChangePage = NO; // jerrylu, 2013/01/04
    
    // jerrylu, 2013/01/24, fix bug11747
    [playbackButton setEnabled:NO];
    [ptzButton setEnabled:NO];
    [megapixelButton setEnabled:NO];
    [snapshotButton setEnabled:NO];
    [audioButton setEnabled:NO];
    [talkButton setEnabled:NO];
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *) scrollView {
	if (needChangeCams) {
		[self scrollViewChangeCamera];
        [self changeUILayout];
        needChangeCams = NO;
	}
    bNoChangePage = YES; // jerrylu, 2013/01/04
    
    [self refreshToolBar]; // jerrylu, 2013/01/24, fix bug11747
}

- (void)scrollViewChangeCamera {
    // Disconnect All connections
    [serverMgr disconnectAllCam];
    [serverMgr disconnectAllPlaybackConnection];
    
    // Change SingleCamViewController Settings
    NSString * camName = [serverMgr getDeviceNameByChIndex:iCurrentCamera];
    [self setTitle:camName];
    [serverMgr updateAudioAndPtzDeviceByChIndex:iCurrentCamera];
    [self setPtzCap:[serverMgr isSupportPTZ:iCurrentCamera]];
    [self setAudioCap:[serverMgr isSupportAudio:iCurrentCamera]];
    [self setTalkCap:[serverMgr isSupportTalk:iCurrentCamera]];
    
    liveviewaudioenable = NO;
    playbackaudioenable = NO;
    talkenable = NO;
    [serverMgr setTalkStateByChIndex:iCurrentCamera state:NO];
    
    ePlayType = ePLAY_PAUSE;
    if ([serverMgr isSupportSecondProfile:iCurrentCamera]) {
            eProfile = eLow;
    }
    else
        eProfile = eOriginal;
    
#if RENDER_BY_OPENGL
    zoomLevel = 1.0;
#else
    zoomLevel = 0;
#endif
	offsetPos = CGPointZero;
	imgSize = CGSizeZero;
	
    imageview = (SinglePageViewController_1X1 *)[viewControllers objectAtIndex:iCurrentCamera];
    [self changeTONonePTZ];
    
    // Change SlideView/SinglePAgeViewController_Base Settings
    [parent.slideView changePagebyCameraIndexbySingleView:iCurrentCamera SingleCam:self];
    
    // Connect the new Camera
    [serverMgr connectToCam:iCurrentCamera layout:eLayout_1X1];
    [serverMgr connectToCamWithHighResolution:iCurrentCamera];
    
    // jerrylu, 2012/10/01, change Page Control
    if (iNumberOfCamera > 2) {
        if (iCurrentCamera == 0)
            pagecontrol.currentPage = 0;
        else if (iCurrentCamera == iNumberOfCamera-1)
            pagecontrol.currentPage = 2;
        else
            pagecontrol.currentPage = 1;
    }
    else if (iNumberOfCamera == 2) {
        if (iCurrentCamera == 0)
            pagecontrol.currentPage = 0;
        else
            pagecontrol.currentPage = 1;
    }
    
    isHoldButton = NO;
}

#pragma mark - 
#pragma mark MyGLViewGestureDelegate
- (void)myGLViewDelegateSingleTap:(MyGLView *) view {
    [self hideUIWithValue:FALSE];
    
    [self resetHideTimer];
}

- (void)myGLViewDelegateDoubleTap:(MyGLView *) view {
    if (eViewModeType == eViewMode_DigitalPTZ || eViewModeType == eViewMode_Playback) { // jerrylu, 2012/11/21, fix bug9800
		//offsetPos = CGPointZero;
		zoomLevel = 1.0;
        
        if (eViewModeType == eViewMode_DigitalPTZ) { // jerrylu, 2012/11/23
            self.scrollView.scrollEnabled = YES;
            [pagecontrol setHidden:NO];
        }
        
        [parent.slideView setImageUpdateOfCamIndex:iCurrentCamera flag:YES];
	}
	else if (eViewModeType == eViewMode_PhysicalPTZ) {
		NSString * cmd = @"Home";
		[self sendPTZWithOp:cmd];
	}
    
    [self resetHideTimer];
}

- (void)myGLViewDelegatePinch:(MyGLView *) view zooming:(BOOL) isZoomIn zoomScale:(float) scale {
    // jerrylu, 2012/06/28, support zooming in Playbackmode
	if (eViewModeType == eViewMode_DigitalPTZ || eViewModeType == eViewMode_Playback) {
        zoomLevel = (scale > 1.0)? scale : 1.0;
        
        //NSLog(@"zoom : %f", zoomLevel);
        if (scale > 1.0) {
            [imageview.image enablePanSwipe];
            
            if (eViewModeType == eViewMode_DigitalPTZ) {
                self.scrollView.scrollEnabled = NO;
                [pagecontrol setHidden:YES];
            }
        }
        else {
            [imageview.image disablePanSwipe];
            
            if (eViewModeType == eViewMode_DigitalPTZ) {
                self.scrollView.scrollEnabled = YES;
                [pagecontrol setHidden:NO];
            }
        }
        
        [parent.slideView setImageUpdateOfCamIndex:iCurrentCamera flag:YES];
	}
	else if (eViewModeType == eViewMode_PhysicalPTZ) {
		NSString * cmd;
        bool bNeedSend = true;
		if (isZoomIn) {
			cmd = @"ZoomTele";
            if ([stopCmd isEqual:@"ZoomTeleStop"]) {
                bNeedSend = false;
            }
            stopCmd = @"ZoomTeleStop";
		}
		else {
			cmd = @"ZoomWide";
            if ([stopCmd isEqual:@"ZoomWideStop"]) {
                bNeedSend = false;
            }
            stopCmd = @"ZoomWideStop";
		}
        
        if (bNeedSend) {
#if DEBUG_LOG
            NSLog(@"Send Zoom cmd");
#endif
            [self sendPTZWithOp:cmd];
        }
        
        [self hideArrow:YES];
	}
}

- (void)myGLViewDelegatePinchEnd:(MyGLView *)view {
    if (eViewModeType == eViewMode_PhysicalPTZ) {
		if (stopCmd != nil && ![stopCmd isEqual:@""]) {
#if DEBUG_LOG
			NSLog(@"stop");
#endif
			[self sendPTZWithOp:stopCmd];
            stopCmd = @"";
		}
        
        [self hideArrow:NO];
	}
    
    [self resetHideTimer];
}

- (void)myGLViewDelegatePanSwipe:(MyGLView *) view onDirection:(ePanDirection) dir {
    if (eViewModeType == eViewMode_PhysicalPTZ) {
		NSString * cmd;
		switch (dir) {
			case ePanUp:
				cmd	= @"TiltUp";
				stopCmd = @"TiltUpStop";
				break;
			case ePanDown:
				cmd = @"TiltDown";
				stopCmd = @"TiltDownStop";
				break;
			case ePanRight:
				cmd = @"PanRight";
				stopCmd = @"PanRightStop";
				break;
			case ePanLeft:
				cmd = @"PanLeft";
				stopCmd = @"PanLeftStop";
				break;
			case ePanUpRight:
				cmd	= @"UpRight";
				stopCmd = @"UpRightStop";
				break;
			case ePanUpLeft:
				cmd = @"UpLeft";
				stopCmd = @"UpLeftStop";
				break;
			case ePanDownLeft:
				cmd = @"DownLeft";
				stopCmd = @"DownLeftStop";
				break;
			case ePanDownRight:
				cmd = @"DownRight";
				stopCmd = @"DownRightStop";
				break;
			case ePanHome:
				cmd = @"Home";
				stopCmd = @"HomeStop";
				break;
			default:
				cmd = nil;
				break;
		}
		if (cmd != nil) {
			[self sendPTZWithOp:cmd];
		}
        
        [self hideArrow:YES];
	}
}

- (void)myGLViewDelegatePanSwipeEnd:(MyGLView *) view {
    if (eViewModeType == eViewMode_DigitalPTZ || eViewModeType == eViewMode_Playback) {
        [parent.slideView setImageUpdateOfCamIndex:iCurrentCamera flag:YES];
    }
    else if (eViewModeType == eViewMode_PhysicalPTZ) {
		if (stopCmd != nil && ![stopCmd isEqual:@""]) {
#if DEBUG_LOG
			NSLog(@"stop");
#endif
			[self sendPTZWithOp:stopCmd];
            stopCmd = @"";
		}
        
        [self hideArrow:NO];
	}
    
    [self resetHideTimer];
}

#pragma mark -
#pragma mark MyCameraListControllerDelegate
- (void)switchToCameraIndex:(int) ch_index {
    iCurrentCamera = ch_index;
    
    [self loadScrollViewWithCameraIndex:ch_index - 1];
    [self loadScrollViewWithCameraIndex:ch_index];
    [self loadScrollViewWithCameraIndex:ch_index + 1];
    
    int width = scrollView.frame.size.width;
    //important, set offset
    CGPoint offsetPoint = CGPointZero;
    offsetPoint.x = ch_index * width;
    [scrollView setContentOffset:offsetPoint];
    [self scrollViewChangeCamera];
}

- (void)changeLayout_1X2 {
    [self deallocSingleCam:YES];
    if (changeLayoutDelegate != nil) {
        if ([changeLayoutDelegate respondsToSelector:@selector(changeToSpecificLayout:)]) {
            [changeLayoutDelegate changeToSpecificLayout:@"1X2"];
        }
    }
}

- (void)changeLayout_1X3 {
    [self deallocSingleCam:YES];
    if (changeLayoutDelegate != nil) {
        if ([changeLayoutDelegate respondsToSelector:@selector(changeToSpecificLayout:)]) {
            [changeLayoutDelegate changeToSpecificLayout:@"1X3"];
        }
    }
}

- (void)changeLayout_2X2 {
    [self deallocSingleCam:YES];
    if (changeLayoutDelegate != nil) {
        if ([changeLayoutDelegate respondsToSelector:@selector(changeToSpecificLayout:)]) {
            [changeLayoutDelegate changeToSpecificLayout:@"2X2"];
        }
    }
}

- (void)changeLayout_2X3 {
    [self deallocSingleCam:YES];
    if (changeLayoutDelegate != nil) {
        if ([changeLayoutDelegate respondsToSelector:@selector(changeToSpecificLayout:)]) {
            [changeLayoutDelegate changeToSpecificLayout:@"2X3"];
        }
    }
}

- (void)changeLayout_3X5 {
    [self deallocSingleCam:YES];
    if (changeLayoutDelegate != nil) {
        if ([changeLayoutDelegate respondsToSelector:@selector(changeToSpecificLayout:)]) {
            [changeLayoutDelegate changeToSpecificLayout:@"3X5"];
        }
    }
}

@end


