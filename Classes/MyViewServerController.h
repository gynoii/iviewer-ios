//
//  MyViewCameraController.h
//  iViewer
//
//  Created by NUUO on 13/8/19.
//
//

#import <UIKit/UIKit.h>
#import "SiteInfoController.h"
#import "myviewInfo.h"
#import "MyViewInfoController.h"

@interface MyViewServerController : UITableViewController {
    SiteListController * _favoriteSite;
    MyViewInfo * _myviewInfo;
    MyViewInfoController * _myViewInfoController;
    UIBarButtonItem * saveButton;
    UIBarButtonItem * backButton;
    UIBarButtonItem * cancelButton;
    
    NSMutableArray * serverInfoArray;
    NSMutableArray * ctrlHlprArray;
    NSMutableArray * isShowRecordingServerArray;
    
    BOOL _isAdding;
    
    UIActivityIndicatorView *spinner;
    UIView *overlayView;
    int select_SrvIndex;
}

@property (nonatomic, assign) SiteListController * favoriteSite;
@property (nonatomic, assign) MyViewInfo * myviewInfo;
@property (assign) BOOL isAdding;
@property (nonatomic, assign) MyViewInfoController * myViewInfoController;

@end
