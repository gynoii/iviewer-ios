#import "LiveViewAppDelegate.h"
#import "UserForget.h"

@implementation UserForgetController

@synthesize accountCell = _accountCell;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"User Forget Password";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                      target:self
                                      action:@selector(cancel)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
	[[self navigationItem] setRightBarButtonItem:cancelButton];
	[cancelButton release];
    
    EditableDetailCell *accountCell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:0 detailcell:accountCell];
    [self setAccountCell:accountCell];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        }
        else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
    }
    else {
        [self resizeViewWithOrientation:self.interfaceOrientation];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_accountCell release];

    [super dealloc];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (size.width > size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        } else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self resizeViewWithOrientation:toInterfaceOrientation];
}

-(void)resizeViewWithOrientation:(UIInterfaceOrientation) orientation {
    CGRect rect;
    UIFontDescriptor *currentDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleBody];
    CGFloat bodyTextSize = [currentDescriptor pointSize];
    
    if (IS_IPAD) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
                rect = CGRectMake(20.0, 10.0, 900.0, bodyTextSize+8.0);
            }
            else {
                rect = CGRectMake(20.0, 10.0, 640.0, bodyTextSize+8.0);
            }
        }
        else {
            if (UIInterfaceOrientationIsPortrait(orientation)) {
                rect = CGRectMake(20.0, 10.0, 640.0, 24.0);
            }
            else {
                rect = CGRectMake(20.0, 10.0, 900.0, 24.0);
            }
        }
    }
    else {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, bodyTextSize+8.0);
        }
        else {
            if (UIInterfaceOrientationIsPortrait(orientation)) {
                rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, 24.0);
            }
            else {
                rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.height - 60, 24.0);
            }
        }
    }
    _accountCell.textField.frame = rect;
   
    [self.view setNeedsDisplay];
}

- (BOOL) validateEmail: (NSString *) email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    
    return isValid;
}

- (void) showMessage: (NSString *) content {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:content preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                        message:content
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (BOOL)checkData {
    if ([[[_accountCell textField] text] length] == 0) {
        [self showMessage:@"Please input Email field."];
        
        return FALSE;
    }
    
    if ([self validateEmail:[[_accountCell textField] text]] == FALSE)
    {
        [self showMessage:@"Email format wrong."];
        
        return FALSE;

    }
    
    return TRUE;
}

- (void)cancel {
    [self dismissViewControllerAnimated:YES completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
}

- (BOOL)forgetPassowrd
{
    // Check email
    if ([self validateEmail:[[_accountCell textField] text]] == FALSE)
    {
        [self showMessage:@"Email format wrong."];
        
        return FALSE;
    }

    NSString *url = [[[NSString alloc] initWithFormat:@"http://54.251.107.11/UserAccount/nuuo/forget.php"] autorelease];
    NSString* post = [NSString stringWithFormat:@"e-mail=%s",[[[_accountCell textField] text] UTF8String]];
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSData* postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLen = [NSString stringWithFormat:@"%d", [postData length]];
    [urlRequest setValue:postLen forHTTPHeaderField:@"Content-Length"];
    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error;
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    BOOL bReuslt = FALSE;

    if (serverReply != nil)
    {
        NSDictionary* jsonObj = 
            [NSJSONSerialization JSONObjectWithData:serverReply
            options:NSJSONReadingMutableContainers
            error:nil];
        
        if ([jsonObj objectForKey:@"error"] == nil)
        {
            [self showMessage:@"A Server error occur."];
        }
        else
        {
            bool error = [[jsonObj objectForKey:@"error"] boolValue];
            if (error == false)
            {
                [self showMessage:@"Password has been sent to your mail account, please check."];
                bReuslt = TRUE;
            }
            else
            {
                NSString* str = [jsonObj objectForKey:@"error_msg"];
                
                if (str == nil)
                    [self showMessage:@"This Account is not exist, please try another again."];
                else
                    [self showMessage:str];
            }
        }
    }
    else
    {
        //replyString = [[[NSString alloc] initWithFormat:@"Unconnection"] autorelease];
        [self showMessage:@"Can not access to the internet, please check the Internet status."];
    }

    return bReuslt;
}

- (void)newDetailCellWithTag:(NSInteger) tag detailcell:(EditableDetailCell *) detailcell {
    [[detailcell textField] setDelegate:self];
    [[detailcell textField] setTag:tag];
}

#pragma mark - UITextFieldDelegate Protocol

//  Sets the label of the keyboard's return key to 'Done' when the insertion
//  point moves to the table view's last field.
//
- (BOOL)textFieldShouldBeginEditing:(UITextField *) textField {
    UITableViewCell *cell = (UITableViewCell*) [[textField superview] superview];
    [[self tableView] scrollToRowAtIndexPath:[[self tableView] indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [textField setReturnKeyType:UIReturnKeyDone];
    
    return YES;
}
//  UITextField sends this message to its delegate after resigning
//  firstResponder status. Use this as a hook to save the text field's
//  value to the corresponding property of the model object.
//

//  UITextField sends this message to its delegate when the return key
//  is pressed. Use this as a hook to navigate back to the list view
//  (by 'popping' the current view controller, or dismissing a modal nav
//  controller, as the case may be).
//
//  If the user is adding a new item rather than editing an existing one,
//  respond to the return key by moving the insertion point to the next cell's
//  textField, unless we're already at the last cell.
//
- (BOOL)textFieldShouldReturn:(UITextField *) textField {
	
    if ([textField returnKeyType] != UIReturnKeyDone) {
        //  If this is not the last field (in which case the keyboard's
        //  return key label will currently be 'Next' rather than 'Done'),
        //  just move the insertion point to the next field.
        //
        //  (See the implementation of -textFieldShouldBeginEditing: above.)
        //
        NSInteger nextTag = [textField tag] + 1;
        
        UIView *nextTextField = [[self tableView] viewWithTag:nextTag];
        
        [nextTextField becomeFirstResponder];
    }
	else {
		[textField resignFirstResponder];
    }
	
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 2;
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        EditableDetailCell * cell = [self accountCell];
        
        UITextField * textField = [cell textField];
        [textField setTag:0];
        [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [textField setPlaceholder:NSLocalizedString(@"Account/Email", nil)];
        return cell;
    }
    else if (indexPath.section == 1) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.textLabel.text = NSLocalizedString(@"OK", nil);
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            [cell setBackgroundColor:[UIColor blueColor]];
        }
        
        return cell;
    }
    
    return nil;
}

- (void)changeToSyncPage {
    [self dismissViewControllerAnimated:YES completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
        [self forgetPassowrd];
    }
}

// Method description

@end
