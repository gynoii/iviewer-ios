//
//  serverInfo.h
//  TabView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright 2010 com.debug. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kPushNotificationEnable 1
#define kPushNotificationDisable 0
#define kDefaultPushNotification kPushNotificationEnable
/*
typedef enum  E_Protocol_Type {
	eTypeMainConsole = 0,
	eTypeTitan,
} EProtocolType;
*/

typedef enum E_MachineType {
    eAndroid = 0,
    eiOS
} EMachineType;

typedef enum E_SERVER_TYPE {
    eServer_Default = 0,
    eServer_MainConsole,
    eServer_NVRmini,
    eServer_Titan,
    eServer_NVRsolo,
    eServer_Crystal,
    eServer_Max
} EServerType;

// jerrylu, 2012/07/17
typedef enum  E_CONNECTION_TYPE {
	eIPConnection = 0,
	eP2PConnection,
    eMaxConnectionType,
} EConnectionType;

typedef enum E_LAYOUT_TYPE {
    eLayout_2X3 = 0,
    eLayout_3X5,
    eLayout_4X6,
    eLayout_5X8,
    eLayout_2X2,
    eLayout_1X3,
    eLayout_1X2,
    eLayout_1X1,
    eLayout_Custom,
} ELayoutType;

@interface ServerInfo : NSObject {
	NSString * serverIP;
	NSString * serverPort;
    NSString * userName;
	NSString * userPassword;
	NSString * serverName;
	EServerType serverType;
	char * crypted;
    NSString * serverPlaybackPort; // jerrylu, 2012/04/19
    NSInteger connectionType; // jerrylu, 2012/07/16
    NSString * serverID;
    NSInteger serverPushNotificationEnable; // jerrylu, 2012/07/25
    NSString * serverID_P2P; // jerrylu, 2012/09/10
    NSString * userName_P2P; //jerrylu, 2012/09/28
	NSString * userPassword_P2P;
	NSString * serverName_P2P;
    ELayoutType layoutType;
    ELayoutType myViewLayoutType;
}

@property (nonatomic, retain) NSString * serverIP;
@property (nonatomic, retain) NSString * serverPort;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * userPassword;
@property (nonatomic, retain) NSString * serverName;
@property (nonatomic, assign) EServerType serverType;
@property (nonatomic, retain) NSString * serverPlaybackPort; // jerry, 2012/04/19
@property (nonatomic) NSInteger connectionType; // jerry, 2012/07/16
@property (nonatomic, retain) NSString * serverID;
@property (nonatomic) NSInteger serverPushNotificationEnable; // jerry, 2012/07/25
@property (nonatomic, retain) NSString * serverID_P2P; // jerrylu, 2012/09/10
@property (nonatomic, retain) NSString * userName_P2P; // jerrylu, 2012/09/28
@property (nonatomic, retain) NSString * userPassword_P2P;
@property (nonatomic, retain) NSString * serverName_P2P;
@property (nonatomic, assign) ELayoutType layoutType;
@property (nonatomic, assign) ELayoutType myViewLayoutType;

- (id)initWithDictionary:(NSDictionary *) dictionary;
- (NSArray *)getKeys;
- (id)getPropertyDic;

@end
