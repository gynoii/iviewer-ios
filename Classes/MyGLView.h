#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface MyVideoFrame :NSObject
@property (readwrite, nonatomic, strong) NSData *luma;
@property (readwrite, nonatomic, strong) NSData *chromaB;
@property (readwrite, nonatomic, strong) NSData *chromaR;
@property (readwrite, nonatomic) NSUInteger width;
@property (readwrite, nonatomic) NSUInteger height;
@end

@interface MyVideoFrameBuf :NSObject
@property (readwrite, nonatomic, strong) NSData *data;
@property (readwrite, nonatomic) NSUInteger width;
@property (readwrite, nonatomic) NSUInteger height;
@end

typedef enum ePanDirection {
	ePanNone = 0,
    ePanUp,
	ePanDown,
	ePanLeft,
	ePanRight,
	ePanUpRight,
	ePanUpLeft,
	ePanDownLeft,
	ePanDownRight,
	ePanHome,
} ePanDirection;

@protocol MyGLViewGestureDelegate;

@interface MyGLView : GLKView {
    id <MyGLViewGestureDelegate> gDelegate;
    
    BOOL isLockZoom;
    UITapGestureRecognizer *gSingleTapper;
    UITapGestureRecognizer *gDoubleTapper;
    UILongPressGestureRecognizer *gLongPress;
    UIPinchGestureRecognizer *gPincher;
    UIPanGestureRecognizer *gPanSwiper;
}
@property (nonatomic, assign) id <MyGLViewGestureDelegate> gDelegate;

- (id) initWithFrame:(CGRect)frame frameWidth:(float) w frameHeight:(float) h;
- (void)setContentMode:(UIViewContentMode)contentMode;
- (void)RenderToHardware:(NSTimer *)timer;
- (void)clearFrameBuffer;
- (void)setDefaultPic;
- (void)resetDefaultPicFrame;
- (void)setMyVideoFrame:(MyVideoFrame *) frame;
- (void)setFrameBuf:(uint8_t *) frame width:(int) vWidth height:(int) vHeight;
- (void)enableSingleTap;
- (void)disableSingleTap;
- (void)enableDoubleTap;
- (void)disableDoubleTap;
- (void)enableLongPress;
- (void)disableLongPress;
- (void)enablePinch;
- (void)disablePinch;
- (void)enablePanSwipe;
- (void)disablePanSwipe;
- (void)lockZoom;
- (void)unlockZoom;
- (void)resetScaleFactors;

- (UIImage *)getImageOnView;

+ (MyVideoFrame *) CopyFullFrameToVideoFrame: (uint8_t *) pFrameIn withWidth :(int) vWidth withHeight:(int) vHeight;
+ (MyVideoFrameBuf *)CopyFullFrameToVideoFrameBuf: (uint8_t *) pFrameIn withWidth:(int) vWidth withHeight:(int) vHeight;
@end

@protocol MyGLViewGestureDelegate <NSObject>
@optional
- (void)myGLViewDelegateSingleTap:(MyGLView *) view;
- (void)myGLViewDelegateDoubleTap:(MyGLView *) view;
- (void)myGLViewDelegateLongPress:(MyGLView *) view;
- (void)myGLViewDelegatePinch:(MyGLView *) view zooming:(BOOL) isZoomIn zoomScale:(float) scale;
- (void)myGLViewDelegatePinchEnd:(MyGLView *) view;
- (void)myGLViewDelegatePanSwipe:(MyGLView *) view onDirection:(ePanDirection) dir;
- (void)myGLViewDelegatePanSwipeEnd:(MyGLView *) view;
@end
