#import <UIKit/UIKit.h>

@interface Basicinfo : NSObject {
    NSString *email;
    NSString *password;
    BOOL autoLogin;
}

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, assign) BOOL autoLogin;

- (id)initWithDictionary:(NSDictionary *) dictionary;
- (NSArray *)getKeys;
- (id)getPropertyDic;

@end
