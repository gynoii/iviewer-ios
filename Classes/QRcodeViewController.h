//
//  ViewController.h
//  QRCodeReader
//
//  Created by Gabriel Theodoropoulos on 27/11/13.
//  Copyright (c) 2013 Gabriel Theodoropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SiteInfoController.h"

@interface QRcodeViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate, UIAlertViewDelegate>

@property (nonatomic, assign) IBOutlet UIView *viewPreview;
@property (nonatomic, assign) IBOutlet UIView *viewMask;
@property (nonatomic, assign) IBOutlet UIImageView *qrCodeSquare;
@property (nonatomic, assign) SiteListController * favoriteSite;

@end
