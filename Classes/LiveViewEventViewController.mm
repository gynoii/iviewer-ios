//
//  EventViewController.m
//  iViewer
//
//  Created by Jerry Lu on 12/7/25.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import "LiveViewEventViewController.h"
#import "LiveViewEventListViewController.h"
#import "eventinfo.h"
#import "LiveViewAppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "Utility.h"

@implementation LiveViewEventViewController
@synthesize serverMgr;
#if !RENDER_BY_OPENGL
@synthesize image = mImage;
#endif
@synthesize mLabel;
@synthesize liveViewDelegate;
@synthesize eEvType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
#if RENDER_BY_OPENGL
    mImage = [[MyGLView alloc] initWithFrame:[[UIScreen mainScreen] bounds] frameWidth:[[UIScreen mainScreen] bounds].size.width frameHeight:[[UIScreen mainScreen] bounds].size.height];
    [mImage setContentMode:UIViewContentModeScaleAspectFit];
    [mImage enableSingleTap];
    [mImage enableDoubleTap];
    [mImage enablePinch];
    mImage.gDelegate = self;
    [self.view insertSubview:mImage atIndex:0];
#endif
    
#if RENDER_BY_OPENGL
    zoomLevel = 1.0;
#else
    zoomLevel = 0;
#endif
	offsetPos = CGPointZero;
	imgSize = CGSizeZero;
    
    eEvType = eEV_PlaybackDefault;
    ePreviousEvType = eEvType;
    ePlayType = eventPLAY_PAUSE;
    
    playbackSpeed = 1.0;
    playbackSpeedList = [[NSArray alloc] initWithObjects:NSLocalizedString(@"playback speed 1/4X", nil), NSLocalizedString(@"playback speed 1/2X", nil), NSLocalizedString(@"playback speed 1X", nil), NSLocalizedString(@"playback speed 2X", nil), NSLocalizedString(@"playback speed 4X", nil), nil];
    playbackSpeedValueList = [[NSArray alloc] initWithObjects:@"0.25", @"0.5", @"1.0", @"2.0", @"4.0", nil];
    speedSheet = nil;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    dualRecordSheet = nil;
    dualRecordController = nil;
#endif
    playbackstate = kStateStopped;
    
    [self changeTOPlaybackDefaultMode];
    
    m_errorCondition = [[NSCondition alloc] init];
	
    [mIndicator startAnimating];
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(pressBackButton) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil)
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(pressBackButton)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
    BackButton = backButton;
	[backButton release];
    
    [mLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self refreshSecondToolBar]; // jerrylu, 2012/11/15
    [LiveViewButton setEnabled:NO];
    
    imgBuffer = [[NSMutableArray alloc] init];
	[imgBuffer addObject:[NSNull null]];
    isUpdateImg= NO;
    
    isHoldButton = NO;
	
    [super viewDidLoad];
    
    m_imgCondition = [[NSCondition alloc] init];
    displayTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0/30
                                                    target:self
                                                  selector:@selector(displayNextFrame:)
                                                  userInfo:nil
                                                   repeats:YES] retain];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopDisplayFrame)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopDisplayFrame)
                                                 name:NOTIFICATION_STOP_QUERY_THREAD
                                               object:[UIApplication sharedApplication]];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    [self hideUIWithValue:NO];
    [self resetHideTimer];
    
    m_checkPlaybackStatusThread = [[NSThread alloc] initWithTarget:self selector:@selector(checkPlaybackStatus) object:NULL];
    [m_checkPlaybackStatusThread start];
    
    //adjust navigationBar, toolbar and status bar translucent
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    [self.navigationController.navigationBar setTranslucent:YES];
    [self.navigationController setToolbarHidden:NO animated:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [self.navigationController.toolbar setTranslucent:YES];
        
    //apply to full screen layout
    //not use this will make gap 
    [self setWantsFullScreenLayout:YES];
    
    [self.view endEditing:TRUE];
    
    [self refreshSecondToolBar];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            [self changeLayout:UIInterfaceOrientationLandscapeLeft];
        }
        else {
            [self changeLayout:UIInterfaceOrientationPortrait];
        }
    }
    else {
        [self changeLayout:self.interfaceOrientation];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [serverMgr setEventDelegate:nil];
    liveViewDelegate = nil;
    
    [self stopHideTimer];
    [self hideUIWithValue:NO];
    
    [self playbackstopTimeoutHandle];
    
    if ([m_checkPlaybackStatusThread isExecuting]) {
        [m_checkPlaybackStatusThread cancel];
        while (![m_checkPlaybackStatusThread isFinished])
        {
            [NSThread sleepForTimeInterval:0.5];
        }
    }
    [m_checkPlaybackStatusThread release];
    
    if (IS_IPAD) { // jerrylu, 2012/06/08
        [pPlaybackSpeedPopover release];
        pPlaybackSpeedPopover = nil;
    }
    else {
        if (speedSlider != nil) {
            [speedSlider removeFromSuperview];
            [speedImageView removeFromSuperview];
        }
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidDisappear:animated];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            [self changeLayout:UIInterfaceOrientationLandscapeLeft];
        }
        else {
            [self changeLayout:UIInterfaceOrientationPortrait];
        }
        
        if (IS_IPAD) {
            [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
        }
        if (speedSheet != nil) {
            [speedSheet dismissWithClickedButtonIndex:0 animated:NO];
            speedSheet = nil;
        }
        if (speedSlider != nil && [speedSlider superview] != nil) {
            [speedSlider removeFromSuperview];
            [speedImageView removeFromSuperview];
        }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
        if (dualRecordSheet != nil) {
            [dualRecordSheet dismissWithClickedButtonIndex:0 animated:NO];
            dualRecordSheet = nil;
        }
        if (dualRecordController != nil) {
            [dualRecordController dismissViewControllerAnimated:NO completion:nil];
            dualRecordController = nil;
        }
#endif
        [self refreshToolBar];
        isUpdateImg = YES;
        [self.view setNeedsLayout];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
    {
        isUpdateImg = YES;
    }];
    
    return;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [self changeLayout:self.interfaceOrientation];
    
    if (IS_IPAD) {
        [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
    }
    if (speedSheet != nil) {
        [speedSheet dismissWithClickedButtonIndex:0 animated:NO];
        speedSheet = nil;
    }
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (dualRecordSheet != nil) {
        [dualRecordSheet dismissWithClickedButtonIndex:0 animated:NO];
        dualRecordSheet = nil;
    }
    if (dualRecordController != nil) {
        [dualRecordController dismissViewControllerAnimated:NO completion:nil];
        dualRecordController = nil;
    }
#endif
    [self refreshToolBar];
    isUpdateImg = YES;
}

- (void)changeLayout:(UIInterfaceOrientation)interfaceOrientation {
    float toolbar_h = self.navigationController.toolbar.frame.size.height;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        mImage.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        mIndicator.center = mImage.center;
        mPlaybackToolbar.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height - toolbar_h*2, [[UIScreen mainScreen] bounds].size.width, toolbar_h);
    }
    else {
        if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
            mImage.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
            mIndicator.center = mImage.center;
            mPlaybackToolbar.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height - toolbar_h*2, [[UIScreen mainScreen] bounds].size.width, toolbar_h);
        }
        else {
            mImage.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width);
            mIndicator.center = mImage.center;
            mPlaybackToolbar.frame = CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.width - toolbar_h*2, [[UIScreen mainScreen] bounds].size.height, toolbar_h);
        }
    }
#if RENDER_BY_OPENGL
    [mImage resetDefaultPicFrame];
#endif
    return;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_STOP_QUERY_THREAD object:[UIApplication sharedApplication]];
    
    [self stopDisplayFrame];
    
    [m_errorCondition release];
    m_errorCondition = nil;
    
    [m_imgCondition release];
    m_imgCondition = nil;
    [imgBuffer release];
    imgBuffer = nil;
#if RENDER_BY_OPENGL
    [mImage removeFromSuperview];
    [mImage release];
    mImage = nil;
#endif
    [mLabel release];
    mLabel =nil;
    
    [playbackSpeedList release];
    playbackSpeedList = nil;
    [playbackSpeedValueList release];
    playbackSpeedValueList = nil;
    speedPicker = nil;
    
    if (speedSheet != nil) {
        [speedSheet dismissWithClickedButtonIndex:0 animated:NO];
        speedSheet = nil;
    }
    if (speedSlider != nil) {
        [speedSlider release];
        speedSlider = nil;
        [speedImageView release];
        speedImageView = nil;
    }
    
    if (playbackStartTime != nil) {
        [playbackStartTime release];
        playbackStartTime = nil;
    }
    if (playbackEndTime != nil) {
        [playbackEndTime release];
        playbackEndTime = nil;
    }
    if (playbackReverseEndTime != nil) {
        [playbackReverseEndTime release];
        playbackReverseEndTime = nil;
    }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (dualRecordSheet != nil) {
        [dualRecordSheet dismissWithClickedButtonIndex:0 animated:NO];
        dualRecordSheet = nil;
    }
    if (dualRecordController != nil) {
        [dualRecordController dismissViewControllerAnimated:NO completion:nil];
        dualRecordController = nil;
    }
#endif
    [super dealloc];
}

#pragma mark -
#pragma mark General Method
- (void)refreshSecondToolBar {
    // refresh eventlist button
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    bool liveViewButton_enable; // jerrylu, 2013.01.24, fix bug11760
    if (LiveViewButton != nil)
        liveViewButton_enable = LiveViewButton.enabled;
    LiveViewButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Go Live", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(pressLiveViewButton)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [LiveViewButton setTintColor:COLOR_TOOLBUTTONTINT];
    }
    else {
        [LiveViewButton setTintColor:COLOR_TOOLBUTTONTINTIOS6];
    }
    [LiveViewButton setEnabled:liveViewButton_enable];
    
    NSArray *buttonArray = [[NSArray alloc] initWithObjects:flexItem, LiveViewButton, flexItem, nil];
    [self setToolbarItems:buttonArray];
    [buttonArray release];
    [LiveViewButton release];
    [flexItem release];
}

- (void)refreshToolBar {
    NSArray *buttonArray;
    UIImage *image;
    
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    switch (eEvType) {
        case eEV_PlaybackDefault: // jerry, 2012/06/18
        {
            if (IS_IPAD)
                image = [UIImage imageNamed:@"Previous_Frame_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"Previous_Frame_32x32_nor.png"];
            UIButton *previousframebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            previousframebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [previousframebutton setImage:image forState:UIControlStateNormal];
            [previousframebutton addTarget:self action:@selector(pbControlPreviousFrame) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *pfButton = [[UIBarButtonItem alloc] initWithCustomView:previousframebutton];
            pfButton.enabled = NO;
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"Next_Frame_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"Next_Frame_32x32_nor.png"];
            UIButton *nextframebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            nextframebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [nextframebutton setImage:image forState:UIControlStateNormal];
            [nextframebutton addTarget:self action:@selector(pbControlNextFrame) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *nfButton = [[UIBarButtonItem alloc] initWithCustomView:nextframebutton];
            nfButton.enabled = NO;
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"play_Playback_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"play_Playback_32x32_nor.png"];
            UIButton *playbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            playbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playbutton setImage:image forState:UIControlStateNormal];
            [playbutton addTarget:self action:@selector(pbControlPlay) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *playButton = [[UIBarButtonItem alloc] initWithCustomView:playbutton];
            playButton.enabled = NO;
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"speed_1x_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"speed_1x_32x32_nor.png"];
            UIButton *playspeedbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            playspeedbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playspeedbutton setImage:image forState:UIControlStateNormal];
            [playspeedbutton addTarget:self action:@selector(playbackSpeedPressed) forControlEvents:UIControlEventTouchUpInside];
            speedButton = [[UIBarButtonItem alloc] initWithCustomView:playspeedbutton];
            speedButton.enabled = NO;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            UIButton *dualrecordbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (playbackRecordFile == eRecordFile1) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"dual_record_1_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"dual_record_1_32x32_nor.png"];
            }
            else {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"dual_record_2_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"dual_record_2_32x32_nor.png"];
            }
            dualrecordbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [dualrecordbutton setImage:image forState:UIControlStateNormal];
            [dualrecordbutton addTarget:self action:@selector(playbackDualRecordPressed) forControlEvents:UIControlEventTouchUpInside];
            dualRecButton = [[UIBarButtonItem alloc] initWithCustomView:dualrecordbutton];
            dualRecButton.enabled = NO;
#endif
            buttonArray = [[NSArray alloc] initWithObjects:flexItem,
                           pfButton, flexItem,
                           playButton, flexItem,
                           nfButton, flexItem,
                           speedButton, flexItem,
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
                           dualRecButton, flexItem,
#endif
                           nil];
            
            [mPlaybackToolbar setItems:buttonArray];
            
            [buttonArray release];
            
            [pfButton release];
            [nfButton release];
            [playButton release];
            [speedButton release];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            [dualRecButton release];
#endif
            break;
        }
        case eEV_Playback:
        {
            if (IS_IPAD)
                image = [UIImage imageNamed:@"Previous_Frame_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"Previous_Frame_32x32_nor.png"];
            UIButton *previousframebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            previousframebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [previousframebutton setImage:image forState:UIControlStateNormal];
            [previousframebutton addTarget:self action:@selector(pbControlPreviousFrame) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *pfButton = [[UIBarButtonItem alloc] initWithCustomView:previousframebutton];
            
            if (IS_IPAD)
                image = [UIImage imageNamed:@"Next_Frame_48x48_nor.png"];
            else
                image = [UIImage imageNamed:@"Next_Frame_32x32_nor.png"];
            UIButton *nextframebutton = [UIButton buttonWithType:UIButtonTypeCustom];
            nextframebutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [nextframebutton setImage:image forState:UIControlStateNormal];
            [nextframebutton addTarget:self action:@selector(pbControlNextFrame) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *nfButton = [[UIBarButtonItem alloc] initWithCustomView:nextframebutton];
            
            UIButton *playbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (ePlayType == eventPLAY_PLAY) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"pause_Playback_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"pause_Playback_32x32_nor.png"];
                playbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
                [playbutton setImage:image forState:UIControlStateNormal];
                [playbutton addTarget:self action:@selector(pbControlPause) forControlEvents:UIControlEventTouchUpInside];
            }
            else {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"play_Playback_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"play_Playback_32x32_nor.png"];
                playbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
                [playbutton setImage:image forState:UIControlStateNormal];
                [playbutton addTarget:self action:@selector(pbControlPlay) forControlEvents:UIControlEventTouchUpInside];
            }
            UIBarButtonItem *playButton = [[UIBarButtonItem alloc] initWithCustomView:playbutton];
            
            UIButton *playspeedbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (playbackSpeed == 1.0) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_1x_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_1x_32x32_nor.png"];
            }
            else if (playbackSpeed == 2.0) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_2x_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_2x_32x32_nor.png"];
            }
            else if (playbackSpeed == 4.0) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_4x_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_4x_32x32_nor.png"];
            }
            else if (playbackSpeed == 0.5) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_half_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_half_32x32_nor.png"];
            }
            else if (playbackSpeed == 0.25) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"speed_quarter_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"speed_quarter_32x32_nor.png"];
            }
            playspeedbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [playspeedbutton setImage:image forState:UIControlStateNormal];
            [playspeedbutton addTarget:self action:@selector(playbackSpeedPressed) forControlEvents:UIControlEventTouchUpInside];
            speedButton = [[UIBarButtonItem alloc] initWithCustomView:playspeedbutton];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            UIButton *dualrecordbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (   [serverMgr getServerTypeByChIndex:CameraIndex] != eServer_MainConsole
                && [serverMgr getServerTypeByChIndex:CameraIndex] != eServer_NVRmini
                && [serverMgr getServerTypeByChIndex:CameraIndex] != eServer_NVRsolo) {
                if (IS_IPAD)
                    image = [UIImage imageNamed:@"dual_record_1_48x48_nor.png"];
                else
                    image = [UIImage imageNamed:@"dual_record_1_32x32_nor.png"];
            }
            else {
                if (playbackRecordFile == eRecordFile1) {
                    if (IS_IPAD)
                        image = [UIImage imageNamed:@"dual_record_1_48x48_nor.png"];
                    else
                        image = [UIImage imageNamed:@"dual_record_1_32x32_nor.png"];
                }
                else {
                    if (IS_IPAD)
                        image = [UIImage imageNamed:@"dual_record_2_48x48_nor.png"];
                    else
                        image = [UIImage imageNamed:@"dual_record_2_32x32_nor.png"];
                }
            }
            dualrecordbutton.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
            [dualrecordbutton setImage:image forState:UIControlStateNormal];
            [dualrecordbutton addTarget:self action:@selector(playbackDualRecordPressed) forControlEvents:UIControlEventTouchUpInside];
            dualRecButton = [[UIBarButtonItem alloc] initWithCustomView:dualrecordbutton];
            if (   [serverMgr getServerTypeByChIndex:CameraIndex] != eServer_MainConsole
                && [serverMgr getServerTypeByChIndex:CameraIndex] != eServer_NVRmini
                && [serverMgr getServerTypeByChIndex:CameraIndex] != eServer_NVRsolo)
            {
                dualRecButton.enabled = NO;
            }
            else {
                dualRecButton.enabled = isDualRecording;
            }
#endif
            if (isHoldButton) {
                pfButton.enabled = NO;
                playButton.enabled = NO;
                nfButton.enabled = NO;
                speedButton.enabled = NO;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
                dualRecButton.enabled = NO;
#endif
            }
            
            buttonArray = [[NSArray alloc] initWithObjects:flexItem,
                           pfButton, flexItem,
                           playButton, flexItem,
                           nfButton, flexItem,
                           speedButton, flexItem,
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
                           dualRecButton, flexItem,
#endif
                           nil];
            
            [mPlaybackToolbar setItems:buttonArray];
            
            [buttonArray release];
            
            [pfButton release];
            [nfButton release];
            [playButton release];
            [speedButton release];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            [dualRecButton release];
#endif
            
            
            
            break;
        }
        default:
            break;
    }

    LiveViewButton.enabled = YES;
    [self refreshSecondToolBar];
	[flexItem release];
}

- (void)changeTOPlaybackDefaultMode {
#if DEBUG_LOG
    NSLog(@"changeTOPlaybackDefaultMode");
#endif
    if (![mIndicator isAnimating]) {
        [mIndicator startAnimating];
    }
    
    [self resetImageBuffer];
#if RENDER_BY_OPENGL
    [mImage setDefaultPic];
#else
    UIImage * imgDefault = [UIImage imageNamed:@"novideo320x240_noword.png"];
    CGImageRef imgRef = imgDefault.CGImage;
    imgSize.width = CGImageGetWidth(imgRef);
    imgSize.height = CGImageGetHeight(imgRef);
    [mImage setImage:imgDefault];
#endif
    [mLabel setText:NSLocalizedString(@"Event View Mode", nil)];
    eEvType = eEV_PlaybackDefault;
    [self refreshToolBar];
}

- (void)changeTOPlaybackMode {
#if DEBUG_LOG
    NSLog(@"changeTOPlaybackMode");
#endif
    if ([mIndicator isAnimating]) {
        [mIndicator stopAnimating];
    }
    
    eEvType = eEV_Playback;
    [self refreshToolBar];
}

#if RENDER_BY_OPENGL
- (void)imageBufDidRefreshed:(MyVideoFrameBuf *) pic {
    if (eEvType == eEV_PlaybackDefault) {
        if ([serverMgr getPlaybackConnection:CameraIndex]) {
            [self changeTOPlaybackMode];
            
            // jerrylu, 2012/09/04, fix bug8519
            [self playbackstopTimeoutHandle];
        }
        else {
            return;
        }
    }
    else if (eEvType == eEV_Playback) {
        if (isHoldButton) {
            [self playbackHoldScreen:NO];
            
            if (ePrePlayType == eventPLAY_PLAY) {
                [self pbControlPlay];
            }
        }
    }
    
    if (!isHoldButton && [mIndicator isAnimating]) {
		[mIndicator stopAnimating];
	}
    
    if (ePreviousEvType != eEvType) {
        ePreviousEvType = eEvType;
        [self refreshToolBar];
    }
    
    [mImage clearFrameBuffer];
    [mImage setFrameBuf:(uint8_t *)[pic.data bytes] width:[pic width] height:[pic height]];
    [mImage RenderToHardware:nil];
}
#else
- (void)imageDidRefreshed:(UIImage *) pic {
    if (eEvType == eEV_PlaybackDefault) {
        if ([serverMgr getPlaybackConnection:CameraIndex]) {
            [self changeTOPlaybackMode];
            
            // jerrylu, 2012/09/04, fix bug8519
            [self playbackstopTimeoutHandle];
        }
        else {
            return;
        }
    }
    
    if (!isHoldButton && [mIndicator isAnimating]) {
		[mIndicator stopAnimating];
	}
    
    if (ePreviousEvType != eEvType) {
        ePreviousEvType = eEvType;
        [self refreshToolBar];
    }
    
	CGImageRef imgRef = pic.CGImage;
	imgSize.width = CGImageGetWidth(imgRef);
	imgSize.height = CGImageGetHeight(imgRef);
	
	if (zoomLevel == 0) {
		offsetPos = CGPointZero;
		[mImage setImage:pic];
		return;
	}
	
	// 1 check the boundary
	if (offsetPos.x < 0) {
		offsetPos.x = 0;
	}
	if (offsetPos.y < 0) {
		offsetPos.y = 0;
	}
	//NSLog(@"change to %d %d %d %d %f %f", offsetX, shiftX, offsetY, shiftY, offsetPos.x, offsetPos.y);
	float zoomValue = pow(ZOOM_RATIO, zoomLevel);
	int overSize = offsetPos.x + imgSize.width - (zoomValue * imgSize.width);
	if (overSize > 0) {
		offsetPos.x -= overSize;
	}
	overSize = offsetPos.y + imgSize.height - (zoomValue * imgSize.height);
	if (overSize > 0) {
		offsetPos.y -= overSize;
	}
	// 2 make the translation
	//NSLog(@"%f %f", -offsetPos.x, -offsetPos.y);
	CGAffineTransform transform = CGAffineTransformMakeTranslation(-offsetPos.x, -offsetPos.y);
	// 3 make the scale
	transform = CGAffineTransformScale(transform, zoomValue, zoomValue);
	CGRect bounds = CGRectMake(0, 0, imgSize.width, imgSize.height);
	
	UIGraphicsBeginImageContext(bounds.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextScaleCTM(context, 1.0, -1.0);
	CGContextTranslateCTM(context, 0, -imgSize.height);
	
	CGContextConcatCTM(context, transform);
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, imgSize.width, imgSize.height), imgRef);
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	[mImage setImage:imageCopy];
}
#endif
- (void)showAlert:(Np_Error) error {
    NSString *msg = nil;
    
    switch (error) {
        case Np_ERROR_SESSION_NODATA:
            msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"No Data", nil)];
            break;
        case Np_ERROR_FATAL_ERROR:
            msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"Please check the playback service is on and you have the permission", nil)];
            break;
        case Np_ERROR_UNSUPPORTED_FORMAT:
        {
            NSString *msg = [NSString stringWithFormat:NSLocalizedString(@"Unable to load recorded video - unsupported video format", nil)];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"OK", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self playbackstopTimeoutHandle];
                                         [self performSelectorOnMainThread:@selector(UnsupportedFormatButton) withObject:nil waitUntilDone:NO];
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
                [alert release];
            }
            
            break;
        }
        case Np_ERROR_CONNECT_SUCCESS:
        case Np_ERROR_DISCONNECT_SUCCESS:
        case Np_ERROR_SESSION_LOST:
        default:
            break;
    }
    
    if (msg != nil) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            timeoutAlertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     if ([mIndicator isAnimating]) { // jerrylu, 2012/11/16, fix bug10054
                                         [mIndicator stopAnimating];
                                     }
                                     timeoutAlertController = nil;
                                     [timeoutAlertController dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [timeoutAlertController addAction:ok];
            [self presentViewController:timeoutAlertController animated:YES completion:nil];
        }
        else {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            timeoutAlert = alert;
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
            [alert release];
        }
        [msg release];
    }
}

- (void)checkPlaybackStatus {
    // refresh time and status of the player
    while (TRUE) {
        [NSThread sleepForTimeInterval:0.2];
        
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
        
        if (serverMgr != nil) {
            if ([serverMgr isServerLogouting]) {
                [NSThread exit];
                return;
            }
        }
        else {
            [NSThread exit];
            return;
        }
        
        if (eEvType == eEV_Playback && !isHoldButton) {
            currenttime = [serverMgr playbackGetTime:CameraIndex];
            Np_DateTime datetime = [serverMgr playbackGetTime:CameraIndex];
            NSString *datestring = [[NSString alloc] initWithFormat:@"%d/%02d/%02d %02d:%02d:%02d", datetime.year, datetime.month, datetime.day, datetime.hour, datetime.minute, datetime.second];
            [mLabel performSelectorOnMainThread:@selector(setText:) withObject:datestring waitUntilDone:NO];
            [datestring release];
            
            if ([self checkPlaybackTimeline:datetime] == NO) {
                [self playbackReopenRecord];
            }
            
            Np_PlayerState tmpstate = [serverMgr playbackGetState:CameraIndex];
#if DEBUG_LOG
            if (tmpstate == kStatePaused)
                NSLog(@"State : Pause");
            else if (tmpstate == kStateRunning)
                NSLog(@"State : Running");
            else
                NSLog(@"State : Stopped");
#endif
            if (playbackstate != tmpstate) {
                playbackstate = tmpstate;
                
                if (tmpstate == kStateStopped)
                {
                    [self playbackReopenRecord];
                }
                
                [self performSelectorOnMainThread:@selector(refreshToolBar) withObject:nil waitUntilDone:NO];
            }
            
            float currentspeed = [serverMgr playbackGetSpeed:CameraIndex];
            if (currentspeed != playbackSpeed) {
                [serverMgr playbackSetSpeed:CameraIndex speed:playbackSpeed];
            }
        }
    }
}

-(void)displayNextFrame:(NSTimer *)timer {
    if (!timer.isValid)
        return;
    
    [m_imgCondition lock];
    
#if RENDER_BY_OPENGL
    MyVideoFrameBuf *imgbuf = [imgBuffer objectAtIndex:0];
    if (isUpdateImg && (NSNull *)imgbuf != [NSNull null]) {
        [self imageBufDidRefreshed:imgbuf];
        isUpdateImg = NO;
    }
#else
    UIImage *img = [imgBuffer objectAtIndex:0];
    if (isUpdateImg && (NSNull *)img != [NSNull null]) {
        [self imageDidRefreshed:img];
        isUpdateImg = NO;
    }
#endif
    
    [m_imgCondition unlock];
}

- (void)resetImageBuffer {
    [m_imgCondition lock];
    
    // remove image object from _imageDataArray
    [imgBuffer removeObjectAtIndex:0];
    [imgBuffer insertObject:[NSNull null] atIndex:0];
    isUpdateImg = NO;
    
    [m_imgCondition unlock];
}

- (void)stopDisplayFrame {
    if (displayTimer != nil && [displayTimer isValid]) {
        [displayTimer invalidate];
        while ([displayTimer isValid]) {
            [NSThread sleepForTimeInterval:0.5];
        }
        displayTimer = nil;
    }
}

- (void)hideUI{
    [self hideUIWithValue:TRUE];
}

- (void)hideUIWithValue:(BOOL) hide {
    UIViewController *lastview = (UIViewController *)[[self.navigationController viewControllers] lastObject];
    if (![lastview isKindOfClass:[LiveViewEventViewController class]])
        return;
    
    [[UIApplication sharedApplication] setStatusBarHidden:hide];
    [self.navigationController setNavigationBarHidden:hide animated:YES];
    [self.navigationController setToolbarHidden:hide animated:hide];

    [mLabel setHidden:hide];
    [mPlaybackToolbar setHidden:hide];
}

- (void)stopHideTimer {
    if (hideUITimer != nil && [hideUITimer isValid]) {
        [hideUITimer invalidate];
        while ([hideUITimer isValid]) {
            [NSThread sleepForTimeInterval:0.5];
        }
        hideUITimer = nil;
    }
}

- (void)resetHideTimer {
    [self stopHideTimer];
    
    if (hideUITimer == nil) {
        hideUITimer = [[NSTimer scheduledTimerWithTimeInterval:HIDE_UI_INTERVAL
                                                       target:self
                                                     selector:@selector(hideUI)
                                                     userInfo:nil
                                                      repeats:YES] retain];
    }
}

#pragma mark -
#pragma mark Playback Function Methods
- (void)playbackPreStart:(Eventinfo *)einfo {
    if (!isPlaybackError) {
        int cam_index = -1;

        // Match cam id
        for (int i = 0; i < [serverMgr getCameraTotalNum]; i++) {
            int local_id = [serverMgr getDeviceLidByChIndex:i];
            if (local_id == [einfo.cameraID intValue])
            {
                cam_index = i;
                break;
            }
        }
        
        if (cam_index < 0) {
            [self performSelectorOnMainThread:@selector(playbackErrorHandleForNoData) withObject:nil waitUntilDone:NO];
            return;
        }
        
        //[self setEventInfo:einfo];
        eventInfo = einfo;
        
        [serverMgr disconnectAllPlaybackConnection];
        
        CameraIndex = cam_index;
        NSString *camName = [serverMgr getDeviceNameByChIndex:CameraIndex];
        [self setTitle:camName];
        
        int newTimeInterval = kDefaultEvnetTimeInterval;
        int newRevTimeInterval = -newTimeInterval;
        NSDate *newDate = [[NSDate alloc] initWithTimeInterval:0 sinceDate:[eventInfo eventDate]];
        NSDate *newEndDate  = [[NSDate alloc] initWithTimeInterval:newTimeInterval sinceDate:[eventInfo eventDate]];
        NSDate *newRevEndDate = [[NSDate alloc] initWithTimeInterval:newRevTimeInterval sinceDate:[eventInfo eventDate]];
        
        if (playbackStartTime != nil) {
            [playbackStartTime release];
        }
        playbackStartTime = [newRevEndDate copy];
        if (playbackEndTime != nil) {
            [playbackEndTime release];
        }
        playbackEndTime = [newEndDate copy];
        if (playbackReverseEndTime != nil) {
            [playbackReverseEndTime release];
        }
        playbackReverseEndTime = [newRevEndDate copy]; // jerrylu, 2012/06/13
        
        NSDate2NpDateTime(playbackStartTime, &rec1_starttime);
        NSDate2NpDateTime(playbackStartTime, &rec2_starttime);
        NSDate2NpDateTime(playbackEndTime, &rec1_endtime);
        NSDate2NpDateTime(playbackEndTime, &rec2_endtime);
        NSDate2NpDateTime(playbackReverseEndTime, &endrevtime);
        
        [newDate release];
        [newEndDate release];
        [newRevEndDate release];
        
        playbackSpeed = kDefaultPlaybackSpeed;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
        playbackRecordFile = eRecordFile2;
#endif
        // jerrylu, 2012/06/28
#if RENDER_BY_OPENGL
        zoomLevel = 1.0;
        [mImage resetScaleFactors];
#else
        zoomLevel = 0;
#endif
        
        // jerrylu, 2012/09/04, fix bug8519
        if ([m_checkTimeoutThread isExecuting]) {
            [m_checkTimeoutThread cancel];
            [m_checkTimeoutThread release];
            m_checkTimeoutThread = nil;
        }
        
        if ([self playbackStart]) {
            m_checkTimeoutThread = [[NSThread alloc] initWithTarget:self selector:@selector(playbackTimeoutHandle) object:nil];
            [m_checkTimeoutThread start];
        }
    }
}

- (BOOL)playbackStart {
#if DEBUG_LOG
    NSLog(@"Playback Start");
#endif
    // jerrylu, 2012/07/12
    memset(&(currenttime), 0, sizeof(Np_DateTime));
    isPlaybackError = NO;
    
    BOOL ret = NO;
    
    [self performSelectorOnMainThread:@selector(changeTOPlaybackDefaultMode) withObject:nil waitUntilDone:NO];
    
    [serverMgr setPlaybackDelegate:self];
    ret = [serverMgr playbackStartFromDateByEvent:CameraIndex startdate:playbackStartTime enddate:playbackEndTime];
    if (ret) {
        ePlayType = eventPLAY_PAUSE;
    }
    else {
        if ([serverMgr isServerLogouting] == NO) {
            if (![serverMgr isPlaybackStreaming:CameraIndex]) {
                [self performSelectorOnMainThread:@selector(playbackErrorHandleForConnectionError) withObject:nil waitUntilDone:NO];
            }
            else {
                [self performSelectorOnMainThread:@selector(playbackErrorHandleForNoData) withObject:nil waitUntilDone:NO];
            }
        }
    }
    
    return ret;
}

- (void)playbackEnd {
#if DEBUG_LOG
    NSLog(@"******* End Playback *******");
#endif
    
    [serverMgr playbackStopByEvent:CameraIndex]; // jerrylu, 2012/11/29
    
    // jerrylu, 2012/06/28
#if RENDER_BY_OPENGL
    zoomLevel = 1.0;
    [mImage resetScaleFactors];
#else
    zoomLevel = 0;
#endif
    
    [self performSelectorOnMainThread:@selector(changeTOPlaybackDefaultMode) withObject:nil waitUntilDone:nil];
}

- (void)playbackReopenRecord {
    Np_DateTime reEndTime;
    memset(&reEndTime, 0, sizeof(Np_DateTime));
    
    NSDate2NpDateTime(playbackEndTime, &reEndTime);
    reEndTime.millisecond = 0;
    
    EServerType eServerType = [serverMgr getServerTypeByChIndex:CameraIndex];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (   (   eServerType == eServer_MainConsole
            || eServerType == eServer_NVRmini
            || eServerType == eServer_NVRsolo)
        && playbackRecordFile == eRecordFile2) {
        [serverMgr playbackOpenRecord:CameraIndex startdate:rec2_starttime enddate:reEndTime];
        [self pbControlPause];
    }
    else if (   eServerType == eServer_MainConsole
             || eServerType == eServer_NVRmini
             || eServerType == eServer_NVRsolo) {
        [serverMgr playbackOpenRecord:CameraIndex startdate:rec1_starttime enddate:reEndTime];
        [self pbControlPause];
    }
    else {
        [self pbControlPause];
        [self pbControlSeek:rec1_starttime];
    }
#else
    if (   eServerType == eServer_MainConsole
        || eServerType == eServer_NVRmini
        || eServerType == eServer_NVRsolo) {
        [serverMgr playbackOpenRecord:CameraIndex startdate:rec1_starttime enddate:reEndTime];
        [self pbControlPause];
    }
    else {
        [self pbControlPause];
        [self pbControlSeek:rec1_starttime];
    }
#endif
}

- (void)playbackSpeedPressed {
    ePlayType = eventPLAY_PAUSE;
    [serverMgr playbackPause:CameraIndex];
    [self refreshToolBar];
    
    if (IS_IPAD) {
        if (pPlaybackSpeedPopover.popoverVisible == NO) {
            UIViewController* popoverContent = [[UIViewController alloc] init];
            UIView* popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
                popoverView.backgroundColor = [UIColor blackColor];
            }
            CGRect pickerFrame = CGRectMake(0, 0, 0, 0);
            UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            for (int i = 0; i < [playbackSpeedValueList count]; i++) {
                if (playbackSpeed == [[playbackSpeedValueList objectAtIndex:i] floatValue]) {
                    [pickerView selectRow:i inComponent:0 animated:NO];
                    break;
                }
            }
            speedPicker = pickerView;
            
            [popoverView addSubview:pickerView];
            
            UIBarButtonItem * closeButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"OK", nil) style:UIBarButtonItemStylePlain target:self action:@selector(changePlaybackSpeed)];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                [closeButton setTintColor:[UIColor darkTextColor]];
            }
            popoverContent.view = popoverView;
            popoverContent.preferredContentSize = CGSizeMake(PopoverSizeWidth, 216);
            
            UINavigationController * navigation = [[UINavigationController alloc] initWithRootViewController:popoverContent];
            popoverContent.navigationItem.leftBarButtonItem = closeButton;
            pPlaybackSpeedPopover = [[UIPopoverController alloc] initWithContentViewController:navigation];
            [pPlaybackSpeedPopover presentPopoverFromBarButtonItem:speedButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
            [pickerView release];
            [navigation release];
            [closeButton release];
            [popoverView release];
            [popoverContent release];
        }
        else {
            [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
        }
    }
    else { // iPhone version
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if (speedSlider == nil) {
                speedSlider = [[UISlider alloc] init];
                [speedSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
                [speedSlider addTarget:self action:@selector(sliderTouchEnded:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
                speedImageView = [[UIImageView alloc] init];
                [speedImageView setImage:[UIImage imageNamed:@"speed_1x_32x32_nor.png"]];
            }
            
            if ([speedSlider superview] == nil) {
                for (int i = 0; i < [playbackSpeedValueList count]; i++) {
                    if (playbackSpeed == [[playbackSpeedValueList objectAtIndex:i] floatValue]) {
                        float speedslidervalue = i * 1.0 / ([playbackSpeedValueList count] - 1);
                        [speedSlider setValue:speedslidervalue];
                        
                        switch (i) {
                            case 0:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_quarter_32x32_nor.png"]];
                                break;
                            case 1:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_half_32x32_nor.png"]];
                                break;
                            case 2:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_1x_32x32_nor.png"]];
                                break;
                            case 3:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_2x_32x32_nor.png"]];
                                break;
                            case 4:
                                [speedImageView setImage:[UIImage imageNamed:@"speed_4x_32x32_nor.png"]];
                                break;
                            default:
                                break;
                        }
                    }
                }
                [speedSlider removeFromSuperview];
                [speedImageView removeFromSuperview];
                
                speedSlider.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 210.0, [[UIScreen mainScreen] bounds].size.height - 130.0, 200.0, 32.0);
                speedImageView.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width - 250.0, [[UIScreen mainScreen] bounds].size.height - 130.0, 32.0, 32.0);
                
                [self.view addSubview:speedSlider];
                [self.view addSubview:speedImageView];
            }
            else {
                [speedSlider removeFromSuperview];
                [speedImageView removeFromSuperview];
            }
        }
        else {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
            [actionSheet setActionSheetStyle:UIActionSheetStyleDefault];
            
            CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
            UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
            pickerView.showsSelectionIndicator = YES;
            pickerView.dataSource = self;
            pickerView.delegate = self;
            for (int i = 0; i < [playbackSpeedValueList count]; i++) {
                if (playbackSpeed == [[playbackSpeedValueList objectAtIndex:i] floatValue]) {
                    [pickerView selectRow:i inComponent:0 animated:NO];
                    break;
                }
            }
            speedPicker = pickerView;
            [actionSheet addSubview:pickerView];
            
            UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:NSLocalizedString(@"OK", nil)]];
            closeButton.momentary = YES;
            closeButton.tintColor = [UIColor blackColor];
            [closeButton addTarget:self action:@selector(changePlaybackSpeed) forControlEvents:UIControlEventValueChanged];
            [actionSheet addSubview:closeButton];
            [actionSheet setBackgroundColor:[UIColor whiteColor]];
            [actionSheet showInView:self.view];
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
                closeButton.frame = CGRectMake(10, 7.0f, 50.0f, 30.0f);
                [actionSheet setBounds:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 485)];
            }
            else {
                closeButton.frame = CGRectMake(10, 7.0f, 50.0f, 30.0f);
                [actionSheet setBounds:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height, 325)];
            }
            speedSheet = actionSheet;
            
            [pickerView release];
            [closeButton release];
            [actionSheet release];
        }
    }
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    float step = 0.5 / ([playbackSpeedValueList count] - 1);
    int speedIndex = ((sender.value / step) + 1) / 2;
    playbackSpeed = [[playbackSpeedValueList objectAtIndex:speedIndex] floatValue];
    
    switch (speedIndex) {
        case 0:
            [speedImageView setImage:[UIImage imageNamed:@"speed_quarter_32x32_nor.png"]];
            break;
        case 1:
            [speedImageView setImage:[UIImage imageNamed:@"speed_half_32x32_nor.png"]];
            break;
        case 2:
            [speedImageView setImage:[UIImage imageNamed:@"speed_1x_32x32_nor.png"]];
            break;
        case 3:
            [speedImageView setImage:[UIImage imageNamed:@"speed_2x_32x32_nor.png"]];
            break;
        case 4:
            [speedImageView setImage:[UIImage imageNamed:@"speed_4x_32x32_nor.png"]];
            break;
        default:
            break;
    }
    
    [self refreshToolBar];
}

- (IBAction)sliderTouchEnded:(UISlider *)sender {
    for (int i = 0; i < [playbackSpeedValueList count]; i++) {
        if (playbackSpeed == [[playbackSpeedValueList objectAtIndex:i] floatValue]) {
            float speedslidervalue = i * 1.0 / ([playbackSpeedValueList count] - 1);
            [speedSlider setValue:speedslidervalue];
        }
    }
    [self changePlaybackSpeed];
}

- (void)changePlaybackSpeed {
    if (speedPicker != nil) {
        NSInteger row = [speedPicker selectedRowInComponent:0];
        playbackSpeed = [[playbackSpeedValueList objectAtIndex:row] floatValue];
    }
    [serverMgr playbackSetSpeed:CameraIndex speed:playbackSpeed];
    
    if (IS_IPAD) {
        [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
    }
    else {
        [speedSheet dismissWithClickedButtonIndex:0 animated:YES];
        speedSheet = nil;
    }
    [self refreshToolBar];
}

#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)playbackDualRecordPressed {
    ePrePlayType = ePlayType;
    
    [self pbControlPause];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController * actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Select Record File", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        if (IS_IPAD) {
            UIAlertAction* defaultAct = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"DefaultCancel", nil)
                                         style:UIAlertActionStyleCancel
                                         handler:^(UIAlertAction * action)
                                         {
                                             dualRecordController = nil;
                                             
                                             if (ePrePlayType == eventPLAY_PLAY) {
                                                 [self pbControlPlay];
                                             }
                                         }];
            [actionSheet addAction:defaultAct];
        }
        UIAlertAction* cancelAct = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                    style:UIAlertActionStyleDestructive
                                    handler:^(UIAlertAction * action)
                                    {
                                        dualRecordController = nil;
                                        
                                        if (ePrePlayType == eventPLAY_PLAY) {
                                            [self pbControlPlay];
                                        }
                                        [self UnsupportedFormatButton];
                                    }];
        UIAlertAction* record1Act = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Record File 1", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if (playbackRecordFile != eRecordFile1) {
                                             playbackRecordFile = eRecordFile1;
                                             [self pbControlChangeDualRecord:playbackRecordFile];
                                         }
                                         else {
                                             if (ePrePlayType == eventPLAY_PLAY) {
                                                 [self pbControlPlay];
                                             }
                                             [self UnsupportedFormatButton];
                                         }
                                         [dualRecordController dismissViewControllerAnimated:YES completion:nil];
                                         dualRecordController = nil;
                                     }];
        UIAlertAction* record2Act = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Record File 2", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if (playbackRecordFile != eRecordFile2) {
                                             playbackRecordFile = eRecordFile2;
                                             [self pbControlChangeDualRecord:eRecordFile2];
                                         }
                                         else {
                                             if (ePrePlayType == eventPLAY_PLAY) {
                                                 [self pbControlPlay];
                                             }
                                             [self UnsupportedFormatButton];
                                         }
                                         [dualRecordController dismissViewControllerAnimated:YES completion:nil];
                                         dualRecordController = nil;
                                     }];
        
        Np_DateTime datetime = [serverMgr playbackGetTime:CameraIndex];
        record1Act.enabled = [serverMgr checkPlaybackLogTime:CameraIndex recordfile:eRecordFile1 datetime:&datetime];
        record2Act.enabled = [serverMgr checkPlaybackLogTime:CameraIndex recordfile:eRecordFile2 datetime:&datetime];
        
        [actionSheet addAction:cancelAct];
        [actionSheet addAction:record1Act];
        [actionSheet addAction:record2Act];
        
        dualRecordController = actionSheet;
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popover = actionSheet.popoverPresentationController;
            if (popover) {
                [popover setPermittedArrowDirections:0];
                popover.sourceView = self.view;
                popover.sourceRect = self.view.bounds;
            }
        }
        [self presentViewController:actionSheet animated:YES completion:nil];
    }else {
        UIActionSheet * action = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Record File", nil) delegate:self cancelButtonTitle:nil destructiveButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:nil];
        
        Np_DateTime datetime = [serverMgr playbackGetTime:CameraIndex];
        if ([serverMgr checkPlaybackLogTime:CameraIndex recordfile:eRecordFile1 datetime:&datetime]) {
            [action addButtonWithTitle:NSLocalizedString(@"Record File 1", nil)];
        }
        if ([serverMgr checkPlaybackLogTime:CameraIndex recordfile:eRecordFile2 datetime:&datetime]) {
            [action addButtonWithTitle:NSLocalizedString(@"Record File 2", nil)];
        }
        
        [action showInView:self.navigationController.view];
        dualRecordSheet = action;
        [action release];
    }
}
#endif

- (void)pbControlPlay {
    ePlayType = eventPLAY_PLAY;
    
    Np_DateTime datetime = [serverMgr playbackGetTime:CameraIndex];
    if ([self checkPlaybackTimeline:datetime]) {
        [serverMgr playbackPlay:CameraIndex];
    }
    else {
        ePlayType = eventPLAY_PAUSE;
        [serverMgr playbackPause:CameraIndex];
    }
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}

- (void)pbControlPause {
    ePlayType = eventPLAY_PAUSE;
    [serverMgr playbackPause:CameraIndex];
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}

- (void)pbControlReverse {
    ePlayType = eventPLAY_REVERSE;
    
    Np_DateTime datetime = [serverMgr playbackGetTime:CameraIndex];
    if ([self checkPlaybackTimeline:datetime]) {
        [serverMgr playbackReversePlay:CameraIndex];
    }
    else {
        ePlayType = eventPLAY_PAUSE;
        [serverMgr playbackPause:CameraIndex];
    }
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}

- (void)pbControlPreviousFrame {
    if (ePlayType != eventPLAY_PAUSE) {
        ePlayType = eventPLAY_PAUSE;
        [serverMgr playbackPause:CameraIndex];
    }
    
    Np_DateTime datetime = [serverMgr playbackGetTime:CameraIndex];
    if ([self checkPlaybackTimeline:datetime]) {
        [serverMgr playbackPreviousFrame:CameraIndex];
    }
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}
- (void)pbControlNextFrame {
    if (ePlayType != eventPLAY_PAUSE) {
        ePlayType = eventPLAY_PAUSE;
        [serverMgr playbackPause:CameraIndex];
    }
    
    Np_DateTime datetime = [serverMgr playbackGetTime:CameraIndex];
    if ([self checkPlaybackTimeline:datetime]) {
        [serverMgr playbackNextFrame:CameraIndex];
    }
    [self refreshToolBar];
    
    if (speedSlider != nil && [speedSlider superview] != nil) {
        [speedSlider removeFromSuperview];
        [speedImageView removeFromSuperview];
    }
}

- (void)pbControlSeek:(Np_DateTime)seektime {
    [serverMgr playbackSeek:CameraIndex seektime:seektime];
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT

- (void)serverManagerUnsupportedFormat {
    [self playbackErrorHandleForUnsupportedFormat];
}

- (void)UnsupportedFormatButton {
    if ([mIndicator isAnimating]) {
        [mIndicator stopAnimating];
    }
    [LiveViewButton setEnabled:YES];
    [dualRecButton setEnabled:YES];
}

- (void)pbControlChangeDualRecord:(EDualRecordFile) recordfile {
    isHoldButton = YES;
    isPlaybackError = NO;
    if (![mIndicator isAnimating]) {
        [mIndicator startAnimating];
    }
    
    [self refreshToolBar];
    
    Np_DateTime seektime = [serverMgr playbackGetTime:CameraIndex];
    if (recordfile == eRecordFile1) {
        [serverMgr playbackChangeRecordFile:CameraIndex recordfile:recordfile startdate:rec1_starttime enddate:rec1_endtime seektime:seektime];
    }
    else {
        [serverMgr playbackChangeRecordFile:CameraIndex recordfile:recordfile startdate:rec2_starttime enddate:rec2_endtime seektime:seektime];
    }
}
#endif
- (BOOL)checkPlaybackTimeline:(Np_DateTime) datetime {
    // Reverse Play
    EServerType serverType = [serverMgr getServerTypeByChIndex:CameraIndex];
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    if (   (   serverType == eServer_MainConsole
            || serverType == eServer_NVRmini
            || serverType == eServer_NVRsolo)
        && playbackRecordFile == eRecordFile2) {
        if (rec2_starttime.minute != 0) {
            if (datetime.hour == rec2_starttime.hour && datetime.minute <= (rec2_starttime.minute+59)%60)
                return NO;
        }
        else {
            if (datetime.hour == (rec2_starttime.hour+23)%24 && datetime.minute >= rec2_starttime.minute)
                return NO;
        }
    }
    else if (   serverType == eServer_MainConsole
             || serverType == eServer_NVRmini
             || serverType == eServer_NVRsolo) {
        if (rec1_starttime.minute != 0) {
            if (datetime.hour == rec1_starttime.hour && datetime.minute <= (rec1_starttime.minute+59)%60)
                return NO;
        }
        else {
            if (datetime.hour == (rec1_starttime.hour+23)%24 && datetime.minute >= rec1_starttime.minute)
                return NO;
        }
    }
    else {
        // jerrylu, 2012/06/14
        if (endrevtime.minute != 0) {
            if (datetime.hour == endrevtime.hour && datetime.minute <= (endrevtime.minute+59)%60)
                return NO;
        }
        else {
            if (datetime.hour == (endrevtime.hour+23)%24 && datetime.minute >= endrevtime.minute)
                return NO;
        }
    }
    
    // Play
    if (   (   serverType == eServer_MainConsole
            || serverType == eServer_NVRmini
            || serverType == eServer_NVRsolo)
        && playbackRecordFile == eRecordFile2) {
        if (datetime.hour == rec2_endtime.hour && datetime.minute == rec2_endtime.minute && datetime.second == rec2_endtime.second)
            return NO;
    }
    else {
        if (datetime.hour == rec1_endtime.hour && datetime.minute == rec1_endtime.minute && datetime.second == rec1_endtime.second)
            return NO;
    }
#else
    if (   serverType == eServer_MainConsole
        || serverType == eServer_NVRmini
        || serverType == eServer_NVRsolo) {
        if (rec1_starttime.minute != 0) {
            if (datetime.hour == rec1_starttime.hour && datetime.minute <= (rec1_starttime.minute+59)%60)
                return NO;
        }
        else {
            if (datetime.hour == (rec1_starttime.hour+23)%24 && datetime.minute >= rec1_starttime.minute)
                return NO;
        }
    }
    else {
        // jerrylu, 2012/06/14
        if (endrevtime.minute != 0) {
            if (datetime.hour == endrevtime.hour && datetime.minute <= (endrevtime.minute+59)%60)
                return NO;
        }
        else {
            if (datetime.hour == (endrevtime.hour+23)%24 && datetime.minute >= endrevtime.minute)
                return NO;
        }
    }
    
    // Play
    if (datetime.hour == rec1_endtime.hour && datetime.minute == rec1_endtime.minute && datetime.second == rec1_endtime.second)
        return NO;
#endif
    return YES;
}

- (void)playbackErrorHandle:(Np_Error) error {
    [m_errorCondition lock];
    if (isPlaybackError) {
        return;
    }
    
    [self playbackEnd];
    [self showAlert:error];
    
    isPlaybackError = YES;
    [m_errorCondition unlock];
}

// jerrylu, 2012/09/04, fix bug8519
- (void)playbackTimeoutHandle {
    if ((NSNull *)timeoutAlert != [NSNull null]) {
        [timeoutAlert release];
        timeoutAlert = nil;
    }
    
    if ((NSNull *)timeoutAlertController != [NSNull null]) {
        timeoutAlertController = nil;
    }
    
    NSDate *startTime = [[[NSDate alloc] init] autorelease];
    NSTimeInterval waitingTime = -[startTime timeIntervalSinceNow];
    while (waitingTime < kDefaultPlaybackTimeout) {
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
        
        [NSThread sleepForTimeInterval:0.5];
        waitingTime = -[startTime timeIntervalSinceNow];
    }
    
    if ([[NSThread currentThread] isCancelled]) {
        [NSThread exit];
        return;
    }
    
    if (eEvType != eEV_Playback) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            timeoutAlertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Mobile App is retrieving the recorded video.", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* close = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Close", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self playbackEnd];
                                     
                                     if ([mIndicator isAnimating]) { // jerrylu, 2012/11/16, fix bug10054
                                         [mIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
                                     }
                                     [timeoutAlertController dismissViewControllerAnimated:YES completion:nil];
                                 }];
            UIAlertAction* ok = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"OK", nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [timeoutAlertController dismissViewControllerAnimated:YES completion:nil];
                                    }];
            [timeoutAlertController addAction:close];
            [timeoutAlertController addAction:ok];
            [self presentViewController:timeoutAlertController animated:YES completion:nil];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Mobile App is retrieving the recorded video.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Close", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
            timeoutAlert = alert;
        }
    }
    
    [NSThread exit];
    return;
}

- (void)playbackstopTimeoutHandle {
    // jerrylu, 2012/09/04, fix bug8519
    if ([m_checkTimeoutThread isExecuting]) {
        [m_checkTimeoutThread cancel];
        [m_checkTimeoutThread release];
        m_checkTimeoutThread = nil;
    }
    if ((NSNull *)timeoutAlert != [NSNull null]) {
        [timeoutAlert dismissWithClickedButtonIndex:-1 animated:NO];
        [timeoutAlert release];
        timeoutAlert = nil;
    }
    if ((NSNull *)timeoutAlertController != [NSNull null]) {
        [timeoutAlertController dismissViewControllerAnimated:NO completion:nil];
        timeoutAlertController = nil;
    }
}

- (void)playbackHoldScreen:(BOOL) isHoldScreen {
    if (isHoldScreen) {
        isHoldButton = YES;
        if (![mIndicator isAnimating]) {
            [mIndicator performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:NO];
        }
    }
    else {
        isHoldButton = NO;
        if ([mIndicator isAnimating]) {
            [mIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
        }
    }
    [self performSelectorOnMainThread:@selector(refreshToolBar) withObject:nil waitUntilDone:NO];
}

- (void)playbackErrorHandleForNoData {
    [self playbackErrorHandle:Np_ERROR_SESSION_NODATA];
}

- (void)playbackErrorHandleForConnectionError {
    [self playbackErrorHandle:Np_ERROR_FATAL_ERROR];
}

- (void)playbackErrorHandleForUnsupportedFormat {
    [self playbackErrorHandle:Np_ERROR_UNSUPPORTED_FORMAT];
}

#pragma mark -
#pragma mark Go LiveView Methods
- (void)pressLiveViewButton {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"You are going to leave push notification mode.", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* cancel = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                }];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self stopDisplayFrame];
                                 
                                 [self pbControlPause];
                                 [self playbackstopTimeoutHandle];
                                 
                                 [serverMgr disconnectAllPlaybackConnection];
                                 
                                 // Not Refresh the view
                                 serverMgr.eventDelegate = nil;
                                 
                                 if (liveViewDelegate != nil && [liveViewDelegate respondsToSelector:@selector(lvctrlFromEventViewToLiveView:)]) {
                                     [liveViewDelegate lvctrlFromEventViewToLiveView:CameraIndex];
                                 }
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"You are going to leave push notification mode.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
        [alert release];
    }
}

#pragma mark -
#pragma mark Back Methods
- (void)pressBackButton {
    [self playbackEnd];
    
    [serverMgr setEventDelegate:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Start Methods
- (void)EventListContorlPlay:(Eventinfo *) einfo {
    isPlaybackError = NO;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    isDualRecording = NO;
#endif
    [self playbackPreStart:einfo];
    
    [self resetImageBuffer];
}

#pragma mark -
#pragma mark ServerManagerEventDelegate Methods
- (void)serverManagerStartEventViewByIndex:(ServerManager *)servermgr camIndex:(int)index {
    return;
}

- (void)serverManagerEventDidRefreshImages:(ServerManager *)servermgr camIndex:(int)index camImg:(UIImage *)img {
    [m_imgCondition lock];
    
    if ((NSNull *)img != [NSNull null]) {
        [imgBuffer replaceObjectAtIndex:0 withObject:img];
        isUpdateImg = YES;
    }
    
    [m_imgCondition unlock];
}

- (void)serverManagerEventDidRefreshImageBuf:(ServerManager *)servermgr camIndex:(int)index img:(NSData *)imgData width:(int)width height:(int)height {
    [m_imgCondition lock];
    
    if ([NSNull null] != (NSNull *) imgData) {
		MyVideoFrameBuf *myframebuf = [MyGLView CopyFullFrameToVideoFrameBuf:(uint8_t *)[imgData bytes] withWidth :width withHeight:height];
        [imgBuffer replaceObjectAtIndex:0 withObject:myframebuf];
        
        isUpdateImg = YES;
	}
    
    [m_imgCondition unlock];
}

- (void)serverManagerDidPbNextPreviosFrameSuccess:(ServerManager *)servermgr {
    [self playbackHoldScreen:NO];
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)serverManagerRefreshPlaybackTime:(ServerManager *)servermgr recordfile:(EDualRecordFile)rec startTime:(Np_DateTime *)newStartTime endTime:(Np_DateTime *)newEndTime {
    if (rec == eRecordFile2) {
        rec2_starttime.year = newStartTime->year;
        rec2_starttime.month = newStartTime->month;
        rec2_starttime.day = newStartTime->day;
        rec2_starttime.hour = newStartTime->hour;
        rec2_starttime.minute= newStartTime->minute;
        rec2_starttime.second = newStartTime->second;
        rec2_starttime.millisecond = newStartTime->millisecond;
        
        rec2_endtime.year = newEndTime->year;
        rec2_endtime.month = newEndTime->month;
        rec2_endtime.day = newEndTime->day;
        rec2_endtime.hour = newEndTime->hour;
        rec2_endtime.minute= newEndTime->minute;
        rec2_endtime.second = newEndTime->second;
        rec2_endtime.millisecond = newEndTime->millisecond;
    }
    else {
        rec1_starttime.year = newStartTime->year;
        rec1_starttime.month = newStartTime->month;
        rec1_starttime.day = newStartTime->day;
        rec1_starttime.hour = newStartTime->hour;
        rec1_starttime.minute= newStartTime->minute;
        rec1_starttime.second = newStartTime->second;
        rec1_starttime.millisecond = newStartTime->millisecond;
        
        rec1_endtime.year = newEndTime->year;
        rec1_endtime.month = newEndTime->month;
        rec1_endtime.day = newEndTime->day;
        rec1_endtime.hour = newEndTime->hour;
        rec1_endtime.minute= newEndTime->minute;
        rec1_endtime.second = newEndTime->second;
        rec1_endtime.millisecond = newEndTime->millisecond;
    }
}
#endif
- (void)serverManagerRefreshPlaybackTime:(ServerManager *)servermgr startTime:(Np_DateTime *)newStartTime endTime:(Np_DateTime *)newEndTime {
    rec1_starttime.year = newStartTime->year;
    rec1_starttime.month = newStartTime->month;
    rec1_starttime.day = newStartTime->day;
    rec1_starttime.hour = newStartTime->hour;
    rec1_starttime.minute= newStartTime->minute;
    rec1_starttime.second = newStartTime->second;
    rec1_starttime.millisecond = newStartTime->millisecond;
    
    rec1_endtime.year = newEndTime->year;
    rec1_endtime.month = newEndTime->month;
    rec1_endtime.day = newEndTime->day;
    rec1_endtime.hour = newEndTime->hour;
    rec1_endtime.minute= newEndTime->minute;
    rec1_endtime.second = newEndTime->second;
    rec1_endtime.millisecond = newEndTime->millisecond;
}

- (void)serverManagerDidServerDown:(ServerManager *)servermgr eventID:(int)eventID; {
    return;
}

- (void)serverManagerDidPlaybackDisconnection:(ServerManager *)servermgr {
    [self performSelectorOnMainThread:@selector(playbackErrorHandleForConnectionError) withObject:nil waitUntilDone:NO];
}

#if FUNC_P2P_SAT_SUPPORT
- (void)serverManagerQuotaTimeout:(ServerManager *)servermgr isOutOfQuota:(BOOL)outofquota {
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
	[appDelegate showQuotaTimeoutMessage:outofquota];
}
#endif

- (void)serverManagerEventLoginFailed:(ServerManager *)servermgr {
    return;
}
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)serverManagerEventSupportDualRecordPlayback:(ServerManager *)servermgr {
    isDualRecording = YES;
    [self refreshToolBar];
}

- (void)serverManagerEventChangePlaybackRecordFile:(ServerManager *)servermgr recordFile:(EDualRecordFile)recordfile {
    playbackRecordFile = recordfile;
    [self refreshToolBar];
}
#endif

#pragma mark -
#pragma mark ActionSheet Methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger) buttonIndex {
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    switch (buttonIndex) {
        case 1:
            if (playbackRecordFile != eRecordFile1) {
                playbackRecordFile = eRecordFile1;
                [self pbControlChangeDualRecord:playbackRecordFile];
            }
            else {
                if (ePrePlayType == eventPLAY_PLAY) {
                    [self pbControlPlay];
                }
            }
            break;
        case 2:
            if (playbackRecordFile != eRecordFile2) {
                playbackRecordFile = eRecordFile2;
                [self pbControlChangeDualRecord:playbackRecordFile];
            }
            else {
                if (ePrePlayType == eventPLAY_PLAY) {
                    [self pbControlPlay];
                }
            }
            break;
        default:
            if (ePrePlayType == eventPLAY_PLAY) {
                [self pbControlPlay];
            }
            break;
    }
    
    dualRecordSheet = nil;
#endif
}

#pragma mark -
#pragma mark AlertView Methods
- (void)alertView:(UIAlertView *) alertView didDismissWithButtonIndex:(NSInteger) buttionIndex {
    if ([alertView.message hasPrefix:NSLocalizedString(@"You are going to leave push notification mode.", nil)]) {
        if (buttionIndex == 1) {
            [self stopDisplayFrame];
            
            [self pbControlPause];
            [self playbackstopTimeoutHandle];
            
            [serverMgr disconnectAllPlaybackConnection];
            
            // Not Refresh the view
            serverMgr.eventDelegate = nil;
            
            if (liveViewDelegate != nil && [liveViewDelegate respondsToSelector:@selector(lvctrlFromEventViewToLiveView:)])
                [liveViewDelegate lvctrlFromEventViewToLiveView:CameraIndex];
        }
    }
    else if ([alertView.message hasPrefix:NSLocalizedString(@"Mobile App is retrieving the recorded video.", nil)]) {
        if (buttionIndex == 0) {
            [self playbackEnd];
            
            if ([mIndicator isAnimating]) { // jerrylu, 2012/11/16, fix bug10054
                [mIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
            }
        }
    }
    else {
        if ([mIndicator isAnimating]) { // jerrylu, 2012/11/16, fix bug10054
            [mIndicator stopAnimating];
        }
        timeoutAlert = nil;
    }
    return;
}

#pragma mark -
#pragma mark Picker Data Source Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    return [playbackSpeedList count];
}

#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    return [playbackSpeedList objectAtIndex:row];
}

#pragma mark -
#pragma mark Popover Delegate Method
- (void)dismissPlaybackPopover {
    if (IS_IPAD)
        [pPlaybackSpeedPopover dismissPopoverAnimated:YES];
    return;
}

#pragma mark -
#pragma mark PinchSlideImageViewDelegate protocal
- (void) onPSImageView: (PinchSlideImageView*)view Zooming: (BOOL) isZoomIn Center:(CGPoint) center {
#if DEBUG_LOG
	NSLog(@"zoom 1 = in 0 = out %d", isZoomIn);
#endif
    // jerrylu, 2012/06/28, support zooming in Playbackmode
	if (eEvType == eEV_Playback) {
		if (!isZoomIn && zoomLevel + 1 < MAX_ZOOM_RATIO_NUM) {
			zoomLevel += 1;
		}
		else if (isZoomIn && zoomLevel > 0) {
			zoomLevel -= 1;
		}
		else {
			return;
		}
        
		offsetPos.x = center.x;
		offsetPos.y = center.y;
	}
}

- (void)onPSImageView:(PinchSlideImageView *) view Zooming:(BOOL) isZoomIn {
#if DEBUG_LOG
	NSLog(@"zoom 1 = in 0 = out %d", isZoomIn);
#endif
    // jerrylu, 2012/06/28, support zooming in Playbackmode
	if (eEvType == eEV_Playback) {
		float preValue = pow((double) ZOOM_RATIO, (double) zoomLevel);
		if (!isZoomIn && zoomLevel + 1 < MAX_ZOOM_RATIO_NUM) {
			zoomLevel += 1;
		}
		else if (isZoomIn && zoomLevel > 0) {
			zoomLevel -= 1;
		}
		else {
			return;
		}
        
		float zoomValue = pow((double) ZOOM_RATIO, (double) zoomLevel);
        
		int offsetX = (zoomValue - preValue) * imgSize.width / 2;
		int offsetY = (zoomValue - preValue) * imgSize.height / 2;
		offsetPos.x += offsetX;
		offsetPos.y += offsetY;
	}
}

- (void)onPSImageView:(PinchSlideImageView *) view SlideOnDirection:(int) dir offset:(CGPoint) offsetValue {
    // jerrylu, 2012/06/28, support slide in Playbackmode
	if (eEvType == eEV_Playback) {
		int shiftX = offsetValue.x * imgSize.width / mImage.frame.size.width;
		int shiftY = offsetValue.y * imgSize.height / mImage.frame.size.height;
		offsetPos.x -= shiftX;
		offsetPos.y += shiftY;
	}
}

- (void)onPSImageViewSingleTap:(PinchSlideImageView *) view {
    return;
}

- (void)onPSImageViewDoubleTap:(PinchSlideImageView *) view {
    offsetPos = CGPointZero;
    zoomLevel = 0;
	return;
}

- (void)onPSImageViewDidBeginTouch:(PinchSlideImageView *) view {
	return;
}

- (void)onPSImageViewDidEndTouch:(PinchSlideImageView *) view {
	return;
}

#pragma mark -
#pragma mark MyGLViewGestureDelegate
- (void)myGLViewDelegateSingleTap:(MyGLView *) view {
    [self hideUIWithValue:FALSE];
    [self resetHideTimer];
}

- (void)myGLViewDelegateDoubleTap:(MyGLView *) view {
    if (eEvType == eEV_Playback) {
        zoomLevel = 1.0;
        isUpdateImg = YES;
    }
    
    [self resetHideTimer];
}

- (void)myGLViewDelegatePinch:(MyGLView *) view zooming:(BOOL) isZoomIn zoomScale:(float) scale {
    zoomLevel = (scale > 1.0)? scale : 1.0;
    
    if (eEvType == eEV_Playback) {
        if (scale > 1.0) {
            [mImage enablePanSwipe];
        }
        else {
            [mImage disablePanSwipe];
        }
        isUpdateImg = YES;
    }
    
    [self resetHideTimer];
}

- (void)myGLViewDelegatePanSwipeEnd:(MyGLView *) view {
    if (eEvType == eEV_Playback) {
        isUpdateImg = YES;
    }
    
    [self resetHideTimer];
}
@end
