//
//  ALplayer.h
//  iViewer
//
//  Created by Toby Huang on 12/5/23.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenAL/al.h>
#import <OpenAL/alc.h>
#import "CameraHelper.h"

@interface ALplayer : NSObject{
    ALCcontext *mContext;
    ALCdevice *mDevice;
    ALuint outSourceID;
    
    NSCondition* ticketCondition;
    NSMutableArray *AudioBufferList;
    UInt32 ALFormat;
    int ALSampleRate;
}

@property (nonatomic) ALCcontext *mContext;
@property (nonatomic) ALCdevice *mDevice;

- (void)initOpenAL;
- (void)updateAudioDataToQueue:(AudioData) audiodata;
- (void)cleanQueueBuffer;
- (void)playSound;
- (void)stopSound;
- (void)cleanUpOpenALID;
- (void)cleanUpOpenAL;
@end
