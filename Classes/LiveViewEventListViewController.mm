//
//  LiveViewEventListViewController.m
//  iViewer
//
//  Created by NUUO on 13/8/9.
//
//

#import "LiveViewEventListViewController.h"
#import "LiveViewAppDelegate.h"

#define FONT_SIZE 16.0

@implementation LiveViewEventListViewController

@synthesize liveViewDelegate;
@synthesize serverMgr;
@synthesize filterServerID;
@synthesize filterUserName;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        filterServerID = @"";
        filterUserName = @"";
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style serverID:(NSString *)sid userName:(NSString *)uname
{
    self = [super initWithStyle:style];
    if (self) {
        filterServerID = sid;
        filterUserName = uname;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    eventview = nil;
    
    [self setTitle:NSLocalizedString(@"Events", nil)];
#if 0//def IPAD
    self.contentSizeForViewInPopover = CGSizeMake(480, 640);
#endif
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(backToEventView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil)
																	style:UIBarButtonItemStylePlain
																   target:self
																   action:@selector(backToEventView)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
	[backButton release];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    //adjust navigationBar, toolbar and status bar translucent
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    [self.navigationController.navigationBar setTranslucent:YES];
    [self.navigationController setToolbarHidden:YES animated:NO];
    
    [[self tableView] reloadData];
    [[self tableView] setNeedsDisplay];
    
    if (eventview != nil) {
        [eventview release];
        eventview = nil;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    liveViewDelegate = nil;
    [eventList release];
    eventList = nil;
    [super dealloc];
    //[filterServerID release];
    //[filterUserName release];
}

- (void)backToEventView {
    if (liveViewDelegate != nil && [liveViewDelegate respondsToSelector:@selector(lvctrlFromEventListToLiveView)])
        [liveViewDelegate lvctrlFromEventListToLiveView];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)eventListUpdate {
    [[self tableView] reloadData];
    [[self tableView] setNeedsDisplay];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    
    if ([filterServerID isEqualToString:@""] || [filterUserName isEqualToString:@""]) {
        return 0;
    }
    else {
        if (eventList != nil) {
            [eventList release];
            eventList = nil;
        }
        
        eventList = [[appDelegate getEventListFromServer:filterServerID User:filterUserName] retain];
        return [eventList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([filterServerID isEqualToString:@""] || [filterUserName isEqualToString:@""])
        return nil;
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:@"MyCell"] autorelease];

        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
        Eventinfo *einfo = [eventList objectAtIndex:indexPath.row];
        cell.textLabel.text = einfo.eventMsg;
        if (einfo.isNotRead) {
            cell.textLabel.textColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
        }
        cell.textLabel.adjustsFontSizeToFitWidth = NO;
        //cell.textLabel.minimumFontSize = FONT_SIZE;
        cell.textLabel.lineBreakMode=NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines=0;
        cell.textLabel.font = [UIFont systemFontOfSize:FONT_SIZE];
        cell.textLabel.tag = 1;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)atableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([filterServerID isEqualToString:@""] || [filterUserName isEqualToString:@""])
        return 0;
    
    Eventinfo *einfo = [eventList objectAtIndex:indexPath.row];
    
    UIFont *font = [UIFont systemFontOfSize:FONT_SIZE];
    
    CGSize size  = [einfo.eventMsg sizeWithFont:font constrainedToSize:CGSizeMake(self.view.bounds.size.width -40 , MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];

    return size.height+10;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([filterServerID isEqualToString:@""] || [filterUserName isEqualToString:@""])
        return;
    
    Eventinfo *einfo = [eventList objectAtIndex:indexPath.row];
    if (einfo.isNotRead) {
        LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
        [appDelegate setReadEvent:einfo];
        [appDelegate saveEventList];
    }
    
    [serverMgr disconnectAllCam];
    
    if(eventview != nil) {
        [eventview release];
        eventview = nil;
    }
    
    LiveViewEventViewController *event;
    if (IS_IPAD)
#if RENDER_BY_OPENGL
        event = [[LiveViewEventViewController alloc] initWithNibName:@"EventViewController_iPad_gl" bundle:nil];
#else
        event = [[LiveViewEventViewController alloc] initWithNibName:@"EventViewController_iPad" bundle:nil];
#endif
    else
#if RENDER_BY_OPENGL
        event = [[LiveViewEventViewController alloc] initWithNibName:@"EventViewController_gl" bundle:nil];
#else
        event = [[LiveViewEventViewController alloc] initWithNibName:@"EventViewController" bundle:nil];
#endif
    [event setServerMgr:serverMgr];
    [serverMgr setEventDelegate:event];
    [event setLiveViewDelegate:liveViewDelegate];
    eventview = event;
    
    //push liveView page
    [self.navigationController pushViewController:event animated:NO];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [event EventListContorlPlay:einfo];
    
    [event release];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
