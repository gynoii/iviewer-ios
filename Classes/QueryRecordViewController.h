//
//  QueryRecordViewController.h
//  InfiniteScrollView
//
//  Created by BingHuan Wu on 12/12/16.
//  Copyright © 2016 Gynoii. All rights reserved.
//

#define GRAPH_GAP (94) // 54(label width) + 40(gap between labels)
#define GRAPH_CONTENT_SIZE (3626)
#define GRAPH_DAY (2256) // 24 hours width = 94 * 24

#define GRAPH_OFFSET_ORIGIN (27) // 7:00 (Left) -> half of label width
#define GRAPH_OFFSET_MIDNIGHT (1625) // ORIGIN + (24-7) * GAP -> 0:00
#define SECS_PER_DAY (86400.0)

#define EVENT_Y_POS (50)
#define EVENT_HEIGHT (20)
#define EVENT_COLOR_RED  ([UIColor colorWithRed:1.0 green:0.1 blue:0.1 alpha:0.3])
#define EVENT_COLOR_BLUE ([UIColor colorWithRed:0.1 green:0.1 blue:1.0 alpha:0.3])

#define EVENT_COLOR_GREEN ([UIColor colorWithRed:0.1 green:0.9 blue:0.1 alpha:0.3])

#import <UIKit/UIKit.h>
//#import "SingleCamViewController.h"

typedef enum ScrollDirection {
    ScrollDirectionDecrease, // 0
    ScrollDirectionIncrease  // 1
} ScrollDirection;

typedef enum ScrollMode {
    ManualScrollMode,
    AutoScrollMode
} ScrollMode;


@interface QueryRecordEvents : NSObject
{
    
}
@property (assign, nonatomic) int recordID;
@property (assign, nonatomic) NSDate *startDate;
@property (assign, nonatomic) NSDate *endDate;
@property (assign, nonatomic) CGFloat duration;
@property (retain, nonatomic) UIView *eventView;

+(instancetype)eventWithStartDate:(NSDate*)startDate andEndDate:(NSDate*)endDate;

@end

@interface QueryRecordViewController : UIViewController <UIScrollViewDelegate>
{
    IBOutlet UIScrollView *_scrollView;
    IBOutlet UIView *_queryContentView;
    IBOutlet UIView *_baseLine;
    IBOutlet UILabel *_dateTime;
    IBOutlet UIActivityIndicatorView *_spinner;
    
    CGFloat lastContentOffset;  // last offset value
    CGFloat deltaOffset;        // diff between width and height (used for rotation)
    BOOL    jumpFromRight;
    BOOL    jumpFromLeft;
    NSDate  *currentDateTime;
    ScrollDirection scrollDirection;
    ScrollMode scrollMode;
    
    NSMutableDictionary *eventDictionary; // key = date string, value = array of events
    NSMutableArray *eventViews; // array or all event views
    
    id singleViewDelegate; // view delegate - to sync with view
    BOOL canQuery; // disabled when query in ongoing
}

- (void)fetchEventsOnDate:(NSString*)dateString withEvents:(NSArray*)events;
- (void)renderEvents;
- (void)setSingleViewDeledate:(id)delegate;
- (void)enableAutoScrollView;
- (void)canQueryNewDate:(BOOL)flag;
- (void)syncScrollViewWithDate:(NSDate*)date;

@end
