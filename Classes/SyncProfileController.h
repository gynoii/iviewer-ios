//
//  AddSiteInfoController.h
//  iViewer
//
//  Created by NUUO on 13/8/14.
//
//

#import <UIKit/UIKit.h>
#import "SiteListController.h"
#import "basicInfo.h"

@interface SyncProfileController1 : UITableViewController <UINavigationControllerDelegate> {
    SiteListController * _parent;
    Basicinfo* _basicInfo;
    NSString* _tempPath;
}

@property (nonatomic, assign) SiteListController * parent;
@property (nonatomic, retain) Basicinfo * basicInfo;
@property (nonatomic, retain) NSString * tempPath;

- (NSString*) removeChar:(NSString*) original;

@end
