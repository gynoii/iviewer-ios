//
//  LiveViewEventListViewController.h
//  iViewer
//
//  Created by NUUO on 13/8/9.
//
//

#import <UIKit/UIKit.h>
#import "eventinfo.h"
#import "ServerManager.h"
#import "LiveViewEventViewController.h"

@class LiveViewEventViewController;

@interface LiveViewEventListViewController : UITableViewController {
    id liveViewDelegate;
    ServerManager *serverMgr;
    NSString *filterServerID;
    NSString *filterUserName;
    NSMutableArray *eventList;
    LiveViewEventViewController *eventview;
}

@property (nonatomic, assign) id liveViewDelegate;
@property (nonatomic, assign) ServerManager *serverMgr;
@property (nonatomic, assign) NSString *filterServerID;
@property (nonatomic, assign) NSString *filterUserName;

- (id)initWithStyle:(UITableViewStyle)style serverID:(NSString *)sid userName:(NSString *)uname;
- (void)eventListUpdate;
@end

@protocol LiveViewEventListContorlDelegate
@optional
- (void)lvctrlFromEventListToLiveView;
@end
