#import <UIKit/UIKit.h>
#import "EditableDetailCell.h"

@interface UserForgetController : UITableViewController <UITextFieldDelegate> {
    EditableDetailCell * _accountCell;
}

@property (nonatomic, retain) EditableDetailCell * accountCell;

@end
