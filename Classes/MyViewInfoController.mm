//
//  MyViewInfoController.m
//  iViewer
//
//  Created by NUUO on 13/8/14.
//
//

#import "MyViewInfoController.h"
#import "LiveViewAppDelegate.h"
#import "MyViewServerController.h"

@implementation MyViewInfoController

@synthesize favoriteSite = _favoriteSite;
@synthesize myviewInfo = _myviewInfo;
@synthesize nameCell = _nameCell;
@synthesize isAdding = _isAdding;
@synthesize backupMyviewInfo;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = NSLocalizedString(@"Add View", nil);
        _isAdding = NO;
        bSaving = YES;
        backupMyviewInfo = [[MyViewInfo alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                      target:self
                                      action:@selector(cancel)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
	[[self navigationItem] setRightBarButtonItem:cancelButton];
	[cancelButton release];
    
    if(_isAdding) {
#if 1
        UIImage *backImage;
        UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
        else
            backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
        bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
        [bButton setImage:backImage forState:UIControlStateNormal];
        [bButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(back)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [backButton setTintColor:COLOR_NAVBUTTONTINT];
        }
        else {
            [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
        }
#endif
        [[self navigationItem] setLeftBarButtonItem:backButton];
        [backButton release];
        
        NSMutableArray *serverlist = [[NSMutableArray alloc] init];
        _myviewInfo.serverList = serverlist;
        [serverlist release];
    }
    
    EditableDetailCell *namecell = [[[EditableDetailCell alloc] initWithFrame:CGRectZero] autorelease];
    [self newDetailCellWithTag:0 detailcell:namecell];
    [self setNameCell:namecell];
    
    NSMutableArray * serverInfoList = [[NSMutableArray alloc] init];
    for (MyViewServerInfo * sinfo in [_myviewInfo serverList]) {
        NSMutableArray * cameraInfoList = [[NSMutableArray alloc] init];
        for (MyViewCameraInfo * cinfo in [sinfo cameraList]) {
            MyViewCameraInfo *new_cinfo = [[[MyViewCameraInfo alloc] init] autorelease];
            [new_cinfo setCamCentralID:cinfo.camCentralID];
            [new_cinfo setCamLocalID:cinfo.camLocalID];
            [new_cinfo setRsCentralID:cinfo.rsCentralID];
            [new_cinfo setRsLocalID:cinfo.rsLocalID];
            [new_cinfo setCameraName:cinfo.cameraName];
            [new_cinfo setCameraOrder:cinfo.cameraOrder];
            [cameraInfoList addObject:new_cinfo];
        }
        MyViewServerInfo *new_sinfo = [[[MyViewServerInfo alloc] init] autorelease];
        [new_sinfo setServerID:sinfo.serverID];
        [new_sinfo setUserName:sinfo.userName];
        [new_sinfo setCameraList:cameraInfoList];
        [serverInfoList addObject:new_sinfo];
        [cameraInfoList release];
    }
    [backupMyviewInfo setViewName:_myviewInfo.viewName];
    [backupMyviewInfo setServerList:serverInfoList];
    [serverInfoList release];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        }
        else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
    }
    else {
        [self resizeViewWithOrientation:self.interfaceOrientation];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_myviewInfo release];
    [_nameCell release];
    [backupMyviewInfo release];
    [super dealloc];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (size.width > size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        } else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self resizeViewWithOrientation:toInterfaceOrientation];
}

-(void)resizeViewWithOrientation:(UIInterfaceOrientation) orientation {
    CGRect rect;
    UIFontDescriptor *currentDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleBody];
    CGFloat bodyTextSize = [currentDescriptor pointSize];
    
    if (IS_IPAD) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
                rect = CGRectMake(20.0, 10.0, 900.0, bodyTextSize+8.0);
            }
            else {
                rect = CGRectMake(20.0, 10.0, 640.0, bodyTextSize+8.0);
            }
        }
        else {
            if (UIInterfaceOrientationIsPortrait(orientation)) {
                rect = CGRectMake(20.0, 10.0, 640.0, 24.0);
            }
            else {
                rect = CGRectMake(20.0, 10.0, 900.0, 24.0);
            }
        }
    }
    else {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, bodyTextSize+8.0);
        }
        else {
            if (UIInterfaceOrientationIsPortrait(orientation)) {
                rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.width - 60, 24.0);
            }
            else {
                rect = CGRectMake(20.0, 10.0, [[UIScreen mainScreen] bounds].size.height - 60, 24.0);
            }
        }
    }
    _nameCell.textField.frame = rect;
    
    [self.view setNeedsDisplay];
}

- (void)next {
    if ([[[_nameCell textField] text] length] == 0) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"You need to have a name for your view.", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                            message:NSLocalizedString(@"You need to have a name for your view.", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        return;
    }
    
    MyViewServerController *serverListView = [[MyViewServerController alloc] initWithStyle:UITableViewStylePlain];
    serverListView.favoriteSite = _favoriteSite;
    serverListView.myviewInfo = _myviewInfo;
    serverListView.isAdding = _isAdding;
    serverListView.myViewInfoController = self;
    [serverListView setTitle:[[_nameCell textField] text]];
    [[self navigationController] pushViewController:serverListView animated:YES];
    [serverListView release];
}

- (void)back {
    bSaving = NO;
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)cancel {
    bSaving = NO;
    if (!_isAdding) {
        [self RestoreMyViewInfo];
    }
    [self dismissViewControllerAnimated:YES completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
}

- (void)newDetailCellWithTag:(NSInteger) tag detailcell:(EditableDetailCell *) detailcell {
    [[detailcell textField] setDelegate:self];
    [[detailcell textField] setTag:tag];
}

#pragma mark - UITextFieldDelegate Protocol

//  Sets the label of the keyboard's return key to 'Done' when the insertion
//  point moves to the table view's last field.
//
- (BOOL)textFieldShouldBeginEditing:(UITextField *) textField {
    UITableViewCell *cell = (UITableViewCell*) [[textField superview] superview];
    [[self tableView] scrollToRowAtIndexPath:[[self tableView] indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [textField setReturnKeyType:UIReturnKeyDone];
    
    return YES;
}
//  UITextField sends this message to its delegate after resigning
//  firstResponder status. Use this as a hook to save the text field's
//  value to the corresponding property of the model object.
//

- (void)textFieldDidEndEditing:(UITextField *) textField {
    NSString * text = [textField text];
    
    if (bSaving)
        [_myviewInfo setViewName:text];
}

//  UITextField sends this message to its delegate when the return key
//  is pressed. Use this as a hook to navigate back to the list view
//  (by 'popping' the current view controller, or dismissing a modal nav
//  controller, as the case may be).
//
//  If the user is adding a new item rather than editing an existing one,
//  respond to the return key by moving the insertion point to the next cell's
//  textField, unless we're already at the last cell.
//
- (BOOL)textFieldShouldReturn:(UITextField *) textField {
	
    if ([textField returnKeyType] != UIReturnKeyDone) {
        //  If this is not the last field (in which case the keyboard's
        //  return key label will currently be 'Next' rather than 'Done'),
        //  just move the insertion point to the next field.
        //
        //  (See the implementation of -textFieldShouldBeginEditing: above.)
        //
        NSInteger nextTag = [textField tag] + 1;
        
        UIView *nextTextField = [[self tableView] viewWithTag:nextTag];
        
        [nextTextField becomeFirstResponder];
    }
	else {
		[textField resignFirstResponder];
    }
	
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if (_isAdding)
            return NSLocalizedString(@"Add this view to My View", nil);
        else
            return NSLocalizedString(@"Edit the name", nil);
    }
    else
        return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        EditableDetailCell * cell = [self nameCell];
    
        UITextField * textField = [cell textField];
        [textField setTag:0];
        [textField setText:[_myviewInfo viewName]];
        [textField setPlaceholder:NSLocalizedString(@"View Name", nil)];
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.textLabel.text = NSLocalizedString(@"OK", nil);
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            [cell setBackgroundColor:[UIColor darkGrayColor]];
        }
        return cell;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
        [self next];
    }
}

- (void)RestoreMyViewInfo
{
    NSMutableArray * serverInfoList = [[NSMutableArray alloc] init];
    
    for (MyViewServerInfo * sinfo in [backupMyviewInfo serverList])
    {
        NSMutableArray * cameraInfoList = [[NSMutableArray alloc] init];
        
        for (MyViewCameraInfo * cinfo in [sinfo cameraList])
        {
            MyViewCameraInfo *new_cinfo = [[[MyViewCameraInfo alloc] init] autorelease];
            [new_cinfo setCamCentralID:cinfo.camCentralID];
            [new_cinfo setCamLocalID:cinfo.camLocalID];
            [new_cinfo setRsCentralID:cinfo.rsCentralID];
            [new_cinfo setRsLocalID:cinfo.rsLocalID];
            [new_cinfo setCameraName:cinfo.cameraName];
            [new_cinfo setCameraOrder:cinfo.cameraOrder];
            [cameraInfoList addObject:new_cinfo];
        }
        
        MyViewServerInfo *new_sinfo = [[[MyViewServerInfo alloc] init] autorelease];
        [new_sinfo setServerID:sinfo.serverID];
        [new_sinfo setUserName:sinfo.userName];
        [new_sinfo setCameraList:cameraInfoList];
        [serverInfoList addObject:new_sinfo];
        [cameraInfoList release];
    }
    
    [_myviewInfo setViewName:backupMyviewInfo.viewName];
    [_myviewInfo setServerList:serverInfoList];
    [serverInfoList release];
}

@end
