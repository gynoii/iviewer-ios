//
//  MyCameraListController.m
//  iViewer
//
//  Created by NUUO on 2015/6/2.
//
//

#import "MyCameraListController.h"
#import "LiveViewAppDelegate.h"
#include "Utility.h"

@implementation MyCameraListController
@synthesize switchControlDelegate;
@synthesize serverMgr;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    allCameraCount = [serverMgr getCameraTotalNum];;
    allCameraNameArray = [[NSMutableArray alloc] init];
    
    for (int i=0; i<allCameraCount; i++) {
        allCameraNameArray[i] = [serverMgr getDeviceNameByChIndex:i];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    [[self navigationItem] setLeftBarButtonItem:backButton];
    [backButton release];
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [allCameraNameArray release];
    serverMgr = nil;
    
    [super viewWillDisappear:animated];
}

- (void)back
{
    [[self navigationController] popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allCameraCount;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Cameras List", nil);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        
        NSInteger row = [indexPath row];
        cell.textLabel.text = [allCameraNameArray objectAtIndex:row];
    }
    
    return cell;
}

#pragma mark -
#pragma mark UITableViewDelegate Protocol
//
//  The table view's delegate is notified of runtime events, such as when
//  the user taps on a given row, or attempts to add, remove or reorder rows.
- (void)tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath {
    if (switchControlDelegate != nil) {
        if ([switchControlDelegate respondsToSelector:@selector(switchToCameraIndex:)]) {
            [switchControlDelegate switchToCameraIndex:[indexPath row]];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self back];
}

@end
