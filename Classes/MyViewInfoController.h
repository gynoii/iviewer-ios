//
//  MyViewInfoController.h
//  iViewer
//
//  Created by NUUO on 13/8/14.
//
//

#import <UIKit/UIKit.h>
#import "SiteInfoController.h"
#import "EditableDetailCell.h"
#import "myviewInfo.h"

@interface MyViewInfoController : UITableViewController <UITextFieldDelegate> {
    SiteListController * _favoriteSite;
    MyViewInfo * _myviewInfo;
    EditableDetailCell * _nameCell;
    MyViewInfo * backupMyviewInfo;
    BOOL _isAdding;
    BOOL bSaving;
}

@property (nonatomic, assign) SiteListController * favoriteSite;
@property (nonatomic, retain) MyViewInfo * myviewInfo;
@property (nonatomic, retain) MyViewInfo * backupMyviewInfo;
@property (nonatomic, retain) EditableDetailCell * nameCell;
@property (assign) BOOL isAdding;

- (void)RestoreMyViewInfo;

@end
