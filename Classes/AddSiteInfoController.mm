//
//  AddSiteInfoController.m
//  iViewer
//
//  Created by NUUO on 13/8/14.
//
//

#import "AddSiteInfoController.h"
#import "LiveViewAppDelegate.h"
#import "serverInfo.h"
#import "myviewInfo.h"
#import "SiteInfoController.h"
#import "MyViewInfoController.h"

@implementation AddSiteInfoController

@synthesize favoriteSite = _favoriteSite;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = NSLocalizedString(@"Add New", nil);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                      target:self
                                      action:@selector(cancel)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
	[[self navigationItem] setRightBarButtonItem:cancelButton];
    
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
	[cancelButton release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)cancel {
    [self dismissViewControllerAnimated:YES completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#if CUSTOMER_FOR_SEAGATE
    if ([_favoriteSite.displayedObjects count] == 0)
        return 1;
#endif
    return eAdd_Max;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *hearstr = NSLocalizedString(@"Add...", nil);
    return hearstr;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        switch (indexPath.row) {
            case eAdd_Server:
            {
                cell.textLabel.text = NSLocalizedString(@"Server", nil);
                break;
            }
            case eAdd_View:
            {
                cell.textLabel.text = NSLocalizedString(@"View", nil);
                break;
            }
            default:
                break;
        }
    }
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case eAdd_Server:
        {
            SiteInfoController * controller = [[SiteInfoController alloc] initWithStyle:UITableViewStyleGrouped];
            id info = [[ServerInfo alloc] init];
            [controller setServerInfo:info];
            [controller setFavoriteSite:_favoriteSite];
            [controller setIsAdding:YES];
            [[self navigationController] pushViewController:controller animated:YES];
            [controller release];
            [info release];
            break;
        }
        case eAdd_View:
        {
            MyViewInfoController *controller = [[MyViewInfoController alloc] initWithStyle:UITableViewStyleGrouped];
            id info = [[MyViewInfo alloc] init];
            [controller setMyviewInfo:info];
            [controller setFavoriteSite:_favoriteSite];
            [controller setIsAdding:YES];
            [[self navigationController] pushViewController:controller animated:YES];
            [controller release];
            [info release];
            break;
        }
        default:
            break;
    }
}

@end
