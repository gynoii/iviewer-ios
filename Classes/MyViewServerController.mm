//
//  MyViewCameraController.m
//  iViewer
//
//  Created by NUUO on 13/8/19.
//
//

#import "MyViewServerController.h"
#import "LiveViewAppDelegate.h"
#import "MyViewCameraController.h"
#import <QuartzCore/QuartzCore.h>
#include "Utility.h"

@implementation MyViewServerController

@synthesize favoriteSite = _favoriteSite;
@synthesize myviewInfo = _myviewInfo;
@synthesize isAdding = _isAdding;
@synthesize myViewInfoController = _myViewInfoController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        _isAdding = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    [[self navigationItem] setLeftBarButtonItem:backButton];
    [backButton release];
    
    cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [cancelButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    
    UIImage *calearAllImage;
    UIBarButtonItem * cleanAllButton;
    
    UIButton *caButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        calearAllImage = [UIImage imageNamed:@"Reset_48x48_nor.png"];
    else
        calearAllImage = [UIImage imageNamed:@"Reset_32x32_nor.png"];
    caButton.bounds = CGRectMake(0, 0, calearAllImage.size.width, calearAllImage.size.height);
    [caButton setImage:calearAllImage forState:UIControlStateNormal];
    [caButton addTarget:self action:@selector(checkToCleanVideInfo) forControlEvents:UIControlEventTouchUpInside];
    cleanAllButton = [[UIBarButtonItem alloc] initWithCustomView:caButton];
    
    NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects: cancelButton, cleanAllButton, nil];
    [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
    
    [cancelButton release];
    [cleanAllButton release];
    
    serverInfoArray = [[NSMutableArray alloc] init];
    ctrlHlprArray = [[NSMutableArray alloc] init];
    isShowRecordingServerArray = [[NSMutableArray alloc] init];
    
    for (int i=0; i<[[_favoriteSite displayedObjects] count]; i++)
    {
        ServerInfo *sinfo = [[_favoriteSite displayedObjects] objectAtIndex:i];
        [serverInfoArray addObject:sinfo];
        [ctrlHlprArray addObject:[NSNull null]];
        [isShowRecordingServerArray addObject:[NSNumber numberWithBool:NO]];
    }
}

- (void)checkToCleanVideInfo {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                                       message:NSLocalizedString(@"This button will reset all camera setting in each server to default, are you sure to continue?", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"YES", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self cleanVideInfo];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                         message:NSLocalizedString(@"This button will reset all camera setting in each server to default, are you sure to continue?", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
        [alert release];
    }
}

- (void)cleanVideInfo {
    for (int i = 0; i < [_myviewInfo.serverList count]; i++) {
        MyViewServerInfo *current_sinfo = [_myviewInfo.serverList objectAtIndex:i];
        [current_sinfo.cameraList removeAllObjects];
    }
    [_myviewInfo.serverList removeAllObjects];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                                       message:NSLocalizedString(@"All setting are reset.", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                         message:NSLocalizedString(@"All setting are reset.", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                               otherButtonTitles:nil];
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
        [alert release];
    }
}

- (void)dealloc
{
    [serverInfoArray release];
    serverInfoArray = nil;
    [ctrlHlprArray release];
    ctrlHlprArray = nil;
    [isShowRecordingServerArray release];
    isShowRecordingServerArray = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (overlayView != nil) {
            overlayView.frame = self.tableView.bounds;
        }
        if (spinner != nil) {
            spinner.center = self.tableView.center;
        }
        [self.tableView reloadData];
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    if (overlayView != nil) {
        overlayView.frame = self.tableView.bounds;
    }
    if (spinner != nil) {
        spinner.center = self.tableView.center;
    }
    return;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([_myviewInfo.serverList count] > 0) {
        NSArray *buttonArray;
        UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save & Finish View Editing", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(save)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [saveButton setTintColor:COLOR_TOOLBUTTONTINT];
        }
        else {
            [saveButton setTintColor:COLOR_TOOLBUTTONTINTIOS6];
        }
        buttonArray = [[NSArray alloc] initWithObjects:flexItem, saveButton, flexItem, nil];
        
        [self setToolbarItems:buttonArray];
        [buttonArray release];
        [saveButton release];
        [flexItem release];
        [self.navigationController.toolbar setBarStyle:UIBarStyleBlackOpaque];
        [self.navigationController setToolbarHidden:NO animated:YES];
    }
    else {
        [self.navigationController setToolbarHidden:YES animated:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES animated:NO];
}

- (void)startConnection {
    ServerInfo *sinfo = [serverInfoArray objectAtIndex:select_SrvIndex];
    ControlHelper *ctrlhlpr = [[ControlHelper alloc] init];
    [ctrlhlpr setServerInfo:sinfo];
    
    if (   (   sinfo.serverType == eServer_Default
            || sinfo.serverType == eServer_Crystal
            || sinfo.serverType == eServer_Titan)
        && [ctrlhlpr loginAndGetRecordingServerList])
    {
        [ctrlHlprArray replaceObjectAtIndex:select_SrvIndex withObject:ctrlhlpr];
        [isShowRecordingServerArray replaceObjectAtIndex:select_SrvIndex withObject:[NSNumber numberWithBool:YES]];
        [self.tableView reloadData];
    }
    else if ([ctrlhlpr loginAndGetCameraList]) {
        [ctrlHlprArray replaceObjectAtIndex:select_SrvIndex withObject:ctrlhlpr];
        
        MyViewCameraController *cameraListView = [[MyViewCameraController alloc] initWithStyle:UITableViewStylePlain];
        cameraListView.ctrlhelpr = ctrlhlpr;
        cameraListView.myviewInfo = _myviewInfo;
        cameraListView.isAdding = _isAdding;
        if (sinfo.connectionType == eP2PConnection)
            [cameraListView setTitle:sinfo.serverName_P2P];
        else
            [cameraListView setTitle:sinfo.serverName];
        
        UINavigationController * newNavController = [[UINavigationController alloc]
                                                     initWithRootViewController:cameraListView];
        newNavController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
        [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
        
        [newNavController release];
        [cameraListView release];
    }
    else {
        // login failed
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Can't connect to server", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                            message:NSLocalizedString(@"Can't connect to server", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    [ctrlhlpr release];
    backButton.enabled = YES;
    saveButton.enabled = YES;
    cancelButton.enabled = YES;
    [self releaseScreen];
}

- (void)stopConnection
{
    for (ControlHelper *ctrlhlpr in ctrlHlprArray) {
        if ((NSNull *)ctrlhlpr != [NSNull null]) {
            if (ctrlhlpr.isRunning)
                [ctrlhlpr logout];
            [ctrlhlpr release];
            [ctrlhlpr release]; // not sure that need to release twice
        }
    }
}

- (void)back {
    [self stopConnection];
    [_myViewInfoController RestoreMyViewInfo];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)cancel {
    [self stopConnection];
    [_myViewInfoController RestoreMyViewInfo];
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)save
{
    if ([_myviewInfo.serverList count] <= 0) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"The minimum camera number per MyView is 1 channel.", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"The minimum camera number per MyView is 1 channel.", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        return;
    }
    
    if (_isAdding) {
        [_favoriteSite addObject:_myviewInfo];
        _favoriteSite.isShowMyServer = NO;
        _favoriteSite.isShowMyView = YES;
    }
    else {
        [_favoriteSite save];
    }
    
    [self stopConnection];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)holdScreen {
    // jerrylu, 2012/10/08, fix bug7760
    [self.tableView setScrollEnabled:NO];
    
    if (overlayView == nil) {
        overlayView = [[UIView alloc] init];
        overlayView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        overlayView.frame = self.tableView.bounds;
    }
    if (spinner == nil) {
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        spinner.center = self.tableView.center;
    }
    [overlayView addSubview:spinner];
    [spinner startAnimating];
    [self.tableView addSubview:overlayView];
}

- (void)releaseScreen {
    // jerrylu, 2012/10/08, fix bug7760
    [self.tableView setScrollEnabled:YES];
    
    [overlayView removeFromSuperview];
    [spinner removeFromSuperview];
    [spinner stopAnimating];
    
    if (overlayView != nil) {
        [overlayView release];
        overlayView = nil;
    }
    if (spinner != nil) {
        [spinner release];
        spinner = nil;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [ctrlHlprArray count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    section = section - 1;
    if (section < 0) {
        return 0;
    }
    
    if (![[isShowRecordingServerArray objectAtIndex:section] boolValue])
        return 0;
    
    ControlHelper *ctrlhlpr = [ctrlHlprArray objectAtIndex:section];
    return ctrlhlpr.m_vecServerList.size();
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        ControlHelper *ctrlhlpr = [ctrlHlprArray objectAtIndex:indexPath.section-1];
        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = WChartToNSString(ctrlhlpr.m_vecServerList.at(indexPath.row).name);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *) tableView heightForHeaderInSection:(NSInteger) section
{
    return 35.0;
}

- (UIView *)tableView:(UITableView*) tableView viewForHeaderInSection:(NSInteger) section
{
    float hwidth, hheight = 35.0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        hwidth = [[UIScreen mainScreen] bounds].size.width;
    }
    else {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            hwidth = [[UIScreen mainScreen] bounds].size.width;
        else
            hwidth = [[UIScreen mainScreen] bounds].size.height;
    }
    
    section = section - 1;
    if (section < 0) {
        UIView *customView = [[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, hwidth, hheight)] autorelease];
        UILabel *headerlabel = [[[UILabel alloc] initWithFrame:CGRectMake(5, 5.0, hwidth-5.0, 25.0)] autorelease];
        UIFont* font = [UIFont systemFontOfSize:16.0];
        [headerlabel setFont:font];
        headerlabel.textColor = [UIColor grayColor];
        [headerlabel setText:NSLocalizedString(@"Select Server", nil)];
        [customView addSubview:headerlabel];
        return customView;
    }
    
    UIView *customView = [[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, hwidth, hheight)] autorelease];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (section % 2 == 0) {
            [customView setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0]];
        }
        else {
            [customView setBackgroundColor:[UIColor colorWithRed:0.75 green:0.75 blue:0.75 alpha:1.0]];
        }
    }
    else {
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = customView.bounds;
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor lightGrayColor] CGColor], nil];
        [customView.layer insertSublayer:gradient atIndex:0];
    }
    
    UIButton *clearbutton = [[[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, hwidth, hheight)] autorelease];
    [clearbutton setBackgroundColor:[UIColor clearColor]];
    [clearbutton addTarget:self action:@selector(headerTapped:) forControlEvents:UIControlEventTouchUpInside];
    clearbutton.tag = section;
    
    UILabel *headerlabel = [[[UILabel alloc] initWithFrame:CGRectMake(40.0, 5.0, hwidth - 80.0, 25.0)] autorelease];
    UIFont* font = [UIFont systemFontOfSize:20.0];
	[headerlabel setFont:font];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        [headerlabel setTextColor:[UIColor blackColor]];
	else
        [headerlabel setTextColor:[UIColor blackColor]];
	[headerlabel setBackgroundColor:[UIColor clearColor]];
    if ([[serverInfoArray objectAtIndex:section] connectionType] == eIPConnection)
        [headerlabel setText:[[serverInfoArray objectAtIndex:section] serverName]];
    else
        [headerlabel setText:[[serverInfoArray objectAtIndex:section] serverName_P2P]];
    
    UIImage *image;
    if ([[isShowRecordingServerArray objectAtIndex:section] boolValue])
        image = [UIImage imageNamed:@"indicationDown.png"];
    else
        image = [UIImage imageNamed:@"indicationUp.png"];
    UIButton *imgbutton = [[[UIButton alloc] initWithFrame: CGRectMake(5.0, 5.0, image.size.width, image.size.height)] autorelease];
    [imgbutton setImage:image forState:UIControlStateNormal];
    imgbutton.tag = section;
    [imgbutton addTarget:self action:@selector(headerTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [customView addSubview:clearbutton];
	[customView addSubview:imgbutton];
    [customView addSubview:headerlabel];
    
    return customView;
}

- (void)headerTapped:(UIButton*) sender
{
    UIButton *btnSection = (UIButton *)sender;
    int server_index = btnSection.tag;
    
    if ([NSNull null] == [ctrlHlprArray objectAtIndex:server_index]) {
        [self holdScreen];
        backButton.enabled = NO;
        saveButton.enabled = NO;
        cancelButton.enabled = NO;
        select_SrvIndex = server_index;
        NSThread *m_thread = [[NSThread alloc] initWithTarget:self selector:@selector(startConnection) object:NULL];
        [m_thread start];
    }
    else {
        ServerInfo *sinfo = [serverInfoArray objectAtIndex:server_index];
        if (sinfo.serverType == eServer_Crystal) {
            if (![[isShowRecordingServerArray objectAtIndex:server_index] boolValue])
                [isShowRecordingServerArray replaceObjectAtIndex:server_index withObject:[NSNumber numberWithBool:YES]];
            else
                [isShowRecordingServerArray replaceObjectAtIndex:server_index withObject:[NSNumber numberWithBool:NO]];
        
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:server_index+1] withRowAnimation:UITableViewRowAnimationFade];
        }
        else {
            ControlHelper *ctrlhlpr = [ctrlHlprArray objectAtIndex:server_index];
            MyViewCameraController *cameraListView = [[MyViewCameraController alloc] initWithStyle:UITableViewStylePlain];
            cameraListView.ctrlhelpr = ctrlhlpr;
            cameraListView.myviewInfo = _myviewInfo;
            cameraListView.isAdding = _isAdding;
            if (sinfo.connectionType == eP2PConnection)
                [cameraListView setTitle:sinfo.serverName_P2P];
            else
                [cameraListView setTitle:sinfo.serverName];
            
            UINavigationController * newNavController = [[UINavigationController alloc]
                                                         initWithRootViewController:cameraListView];
            newNavController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
            [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
            
            [newNavController release];
            [cameraListView release];
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyViewCameraController *cameraListView = [[MyViewCameraController alloc] initWithStyle:UITableViewStylePlain];
    ControlHelper *ctrlhlpr = [ctrlHlprArray objectAtIndex:indexPath.section-1];
    cameraListView.ctrlhelpr = ctrlhlpr;
    cameraListView.myviewInfo = _myviewInfo;
    cameraListView.recordingServerCID = ctrlhlpr.m_vecServerList.at(indexPath.row).ID.centralID;
    cameraListView.recordingServerLID = ctrlhlpr.m_vecServerList.at(indexPath.row).ID.localID;
    cameraListView.isAdding = _isAdding;
    [cameraListView setTitle:WChartToNSString(ctrlhlpr.m_vecServerList.at(indexPath.row).name)];
    
    UINavigationController * newNavController = [[UINavigationController alloc]
                                                 initWithRootViewController:cameraListView];
    newNavController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
    
    [newNavController release];
    [cameraListView release];
}

@end
