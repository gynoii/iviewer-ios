//
//  EventAlertView.h
//  iViewer
//
//  Created by NUUO on 12/8/8.
//
//

#import <UIKit/UIKit.h>
#import "eventinfo.h"

@interface EventAlertView : NSObject <UIAlertViewDelegate> {
    id handledelegate;
    
    Eventinfo *eInfo;
    UIAlertView *alertview;
    UIAlertController *alertController;
}

@property (nonatomic, assign) id handledelegate;
@property (nonatomic, retain) Eventinfo *eInfo;

- (void)showAlert;
- (void)dismissAlert;
@end

@protocol EventAlertViewDelegate
@optional
- (void)eventAlertViewShowEventView:(EventAlertView *) alert;
- (void)eventAlertViewRemoveAlertFromList:(EventAlertView *) alert;
@end
