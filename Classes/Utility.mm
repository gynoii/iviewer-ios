//
//  Utility.cpp
//  LiveView
//
//  Created by NUUO on 6/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "Utility.h"
#include <arpa/inet.h>

std::wstring NSStringToStringW ( NSString* Str )   
{   
    NSStringEncoding pEncode    =   CFStringConvertEncodingToNSStringEncoding ( kCFStringEncodingUTF32LE );   
    NSData* pSData              =   [ Str dataUsingEncoding : pEncode ];    
    
    return std::wstring ( (wchar_t*) [ pSData bytes ], [ pSData length] / sizeof ( wchar_t ) );   
}

NSString* StringWToNSString ( const std::wstring& Str )   
{   
    NSString* pString = [ [ NSString alloc ]    
                         initWithBytes : (wchar_t*)Str.data()   
                         length : Str.size() * sizeof(wchar_t)   
                         encoding : CFStringConvertEncodingToNSStringEncoding ( kCFStringEncodingUTF32LE ) ];   
    return pString;   
}

NSString* WChartToNSString ( const wchar_t* Str )
{
    NSString* pString = [ [ NSString alloc ]
                         initWithBytes : Str
                         length :  wcslen(Str) * sizeof(wchar_t)   
                         encoding : CFStringConvertEncodingToNSStringEncoding ( kCFStringEncodingUTF32LE ) ];
    return pString;
}

NSString* documentsPath(NSString * fileName) {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return [documentsDirectory stringByAppendingPathComponent:fileName];
}

void saveImgData(void* pData, int size, int ch, int index) {
    FILE *pFile;
	NSString *fileName;
	
	fileName = documentsPath([NSString stringWithFormat:@"image_ch%d_%04d.ppm", ch,index]);
    // Open file
    NSLog(@"write image file: %@",fileName);
    pFile=fopen([fileName cStringUsingEncoding:NSASCIIStringEncoding], "wb");
    if(pFile==NULL)
        return;
	
    // Write pixel data
    fwrite(pData, 1, size, pFile);
	
    // Close file
    fclose(pFile);
}

static int bufRV[256];
static int bufGV[256];
static int bufGU[256];
static int bufBU[256];

static int bufYR[256];
static int bufYG[256];
static int bufYB[256];
static int bufU[256];
static int bufV[256];
static bool bInit = false;

void YUV420ToRGB(BYTE* lpSrc, int width, int height, BYTE* lpDest, int lPitch, int BitCount, bool bFlip)
{
    //init
    if (!bInit) {
        for (int i=0; i<256; i++)
        {
            //[shawn:20090701]: use NTSC color space conversion
            bufRV[i] = (int)( 1.403 * (i - 128));
            bufGU[i] = (int)(-0.344 * (i - 128));
            bufGV[i] = (int)(-0.714 * (i - 128));
            bufBU[i] = (int)( 1.773 * (i - 128));
            
            bufYR[i] = (int)(0.299 * i);
            bufYG[i] = (int)(0.587 * i);
            bufYB[i] = (int)(0.114 * i);
            bufU [i] = (int)(0.492 * i);
            bufV [i] = (int)(0.877 * i);
        }    
    }
    
    
    // From YUV to RGB
    // R = Y + 1.140V
    // G = Y - 0.395U - 0.581V
    // B = Y + 2.032U
    
    int i,j;
    int c = BitCount / 8;
    
    int nY = height * width;
    int nYU = height * width + height * width / 4;
    int i1=0, i2=0, i3=0;
    
    for (i=0; i<height; i++)
    {
        for (j=0; j<width; j++)
        {
            i3 = (i / 2) * (width / 2) + (j / 2);
            
            int Y = lpSrc[i1];
            int U = lpSrc[nY + i3];
            int V = lpSrc[nYU + i3];
            
            int R = Y + bufRV[V];
            int G = Y + bufGV[V] + bufGU[U];
            int B = Y + bufBU[U];
            
            if (R>255) R=255;
            if (G>255) G=255;
            if (B>255) B=255;
            if (R<0) R=0;
            if (G<0) G=0;
            if (B<0) B=0;
            
            if (bFlip)
                i2 = (height - i - 1) * lPitch + j * c;
            else
                i2 = i * lPitch + j * c;
            
            lpDest[i2+2] = B;
            lpDest[i2+1] = G;
            lpDest[i2  ] = R;
            
            i1++;
        }
    }
}

UIImage* imageFromAVPicture(void* pData, int width, int height)
{
    UIImage *image = NULL;
    int frameSize = width * height * 3;
    //BYTE* frameBuf = new BYTE[frameSize];
    
    //YUV420ToRGB((BYTE*)pData, width, height, frameBuf, width*3, 24, false);
    
	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
	//CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, (UInt8*)frameBuf, 3*width*height,kCFAllocatorNull);
    //CFDataRef data = CFDataCreate(kCFAllocatorDefault, (UInt8*)frameBuf, 3*width*height);
    CFDataRef data = CFDataCreate(kCFAllocatorDefault, (UInt8*)pData, frameSize);
    if (data != 0) {
        CGDataProviderRef provider = CGDataProviderCreateWithCFData(data);
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGImageRef cgImage = CGImageCreate(width, 
                                           height, 
                                           8, 
                                           24, 
                                           3*width, 
                                           colorSpace, 
                                           bitmapInfo, 
                                           provider, 
                                           NULL, 
                                           NO, 
                                           kCGRenderingIntentDefault);
        CGColorSpaceRelease(colorSpace);
        image = [EonilImage imageWithCGImage:cgImage];
        CGImageRelease(cgImage);
        CGDataProviderRelease(provider);
        CFRelease(data); 
    }
    else
    {
        assert(0);
    }
    
    //delete [] frameBuf;
    
	return image;
}

UIImage* imageFromYUVBuffer(void* pData, int width, int height)
{
    UIImage *image = NULL;
    int frameSize = width * height * 3;
    BYTE* frameBuf = new BYTE[frameSize];
    
    YUV420ToRGB((BYTE*)pData, width, height, frameBuf, width*3, 24, false);
    
	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
	CFDataRef data = CFDataCreate(kCFAllocatorDefault, (UInt8*)frameBuf, frameSize);
    if (data != 0) {
        CGDataProviderRef provider = CGDataProviderCreateWithCFData(data);
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGImageRef cgImage = CGImageCreate(width,
                                           height,
                                           8,
                                           24,
                                           3*width,
                                           colorSpace,
                                           bitmapInfo,
                                           provider,
                                           NULL,
                                           NO,
                                           kCGRenderingIntentDefault);
        CGColorSpaceRelease(colorSpace);
        image = [EonilImage imageWithCGImage:cgImage];
        CGImageRelease(cgImage);
        CGDataProviderRelease(provider);
        CFRelease(data);
    }
    else
    {
        assert(0);
    }
    
    delete [] frameBuf;
    
	return image;
}

UIImage* imageScaledToSize(UIImage *image, CGSize newSize) {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            UIGraphicsBeginImageContextWithOptions(newSize, YES, 2.0);
        } else {
            UIGraphicsBeginImageContext(newSize);
        }
    } else {
        UIGraphicsBeginImageContext(newSize);
    }
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


// jerrylu, 2012/05/09
void NSDate2NpDateTime(NSDate *pDate, Np_DateTime *pNpDate) {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit;
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:pDate];
    
    pNpDate->day    = [dateComponents day];
    pNpDate->month  = [dateComponents month];
    pNpDate->year   = [dateComponents year];
    pNpDate->hour   = [dateComponents hour];
    pNpDate->minute = [dateComponents minute];
    pNpDate->second = [dateComponents second];
}

long NpDateTime2TimeInterval1970(Np_DateTime *pNpDate) {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [[[NSDateComponents alloc] init] autorelease];
    
    [dateComponents setDay:pNpDate->day];
    [dateComponents setMonth:pNpDate->month];
    [dateComponents setYear:pNpDate->year];
    [dateComponents setHour:pNpDate->hour];
    [dateComponents setMinute:pNpDate->minute];
    [dateComponents setSecond:pNpDate->second];
    
    NSDate *pDate = [calendar dateFromComponents:dateComponents];
    long time_secod = [pDate timeIntervalSince1970];
    return time_secod;
}

/*
void NpDateTime2NSDate(Np_DateTime *pNpDate, NSDate *pDate) {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit;
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:pDate];
    
    dateComponents.day   = pNpDate->day;
    dateComponents.month = pNpDate->month;
    dateComponents.year  = pNpDate->year;
    dateComponents.hour  = pNpDate->hour;
    dateComponents.minute = pNpDate->minute;
    dateComponents.second = pNpDate->second;
}
*/

UIImage *convertImageToGrayScale(UIImage *image)
{
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    // Create a new UIImage object  
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    // Return the new grayscale image
    return newImage;
}

@implementation     EonilImage

- (id)initWithCGImage:(CGImageRef)imageRef
{
    self    =   [super initWithCGImage:imageRef];
    
    if (self) 
    {
        sourceImage =   imageRef;
        CGImageRetain(imageRef);
    }
    
    return  self;
}
- (void)dealloc
{
    CGImageRelease(sourceImage);
    [super dealloc];
}
@end

bool isValidIpAddress(char *ipAddress)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
    return result != 0;
}

const char * convertIpAddress(char *ipAddress)
{
    struct sockaddr_in sa;
    inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
    return inet_ntoa(sa.sin_addr);
}
