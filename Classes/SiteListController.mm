//
//  favoriteSite.m
//  TabView
//
//  Created by johnlinvc on 10/02/04.
//  Copyright 2010 com.debug. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "LiveViewAppDelegate.h"
#import "SiteListController.h"
#import "serverInfo.h"
#import "myviewInfo.h"
#import "SiteInfoController.h"
#import "SlideView.h"
#import "LoadingPage.h"
#import "HelpViewController.h"
#import "AddSiteInfoController.h"
#import "MyViewInfoController.h"
#import "RecordingServerListController.h"
#import "QRcodeViewController.h"
#import "SyncProfileController.h"
#import "UserLoginController.h"
#include "Utility.h"

@implementation SiteListController

@synthesize displayedObjects = _displayedObjects;
@synthesize displayedMyView = _displayedMyView;
@synthesize displayedBsicinfo = _displayedBsicinfo;
@synthesize m_ServerMgr;
@synthesize logining = _logining;
@synthesize unregServerArray = _unregServerArray;
@synthesize isShowMyServer = _isShowMyServer;
@synthesize isShowMyView = _isShowMyView;
@synthesize isLoginCloud = _isLoginCloud;

#pragma mark -

- (void)dealloc{
    [_displayedObjects release];
    [_displayedMyView release];
    [_displayedBsicinfo release];
    [_unregServerArray release];
    [super dealloc];
}

+ (NSString *)pathForDocumentWithName:(NSString *) documentName {
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, 
                                                         YES);
    NSString * path = [paths objectAtIndex:0];
    
    return [path stringByAppendingPathComponent:documentName];
}

- (NSMutableArray *)displayedBsicinfo {
    if (_displayedBsicinfo == nil) {
        NSString * path = [[self class] pathForDocumentWithName:@"BasicInfo.plist"];
        NSData * savedData = [NSData dataWithContentsOfFile:path];
        NSArray * infoDicts;
        NSString * errStr;
        NSPropertyListFormat format;
        infoDicts = [NSPropertyListSerialization propertyListFromData:savedData
                                                     mutabilityOption:NSPropertyListImmutable
                                                               format:&format
                                                     errorDescription:&errStr];
        //NSLog(@"%@ Path%@",[infoDicts description], path);
        
        if (infoDicts == nil) {
            NSLog(@"Unable to read plist file at path: %@", path);
            path = [[NSBundle mainBundle] pathForResource:@"BasicInfo"
                                                   ofType:@"plist"];
            infoDicts = [NSMutableArray arrayWithContentsOfFile:path];
        }
        
        _displayedBsicinfo = [[NSMutableArray alloc] initWithCapacity:[infoDicts count]];
        
        if ([infoDicts count] == 0) {
            Basicinfo *defaultInfo = [[[Basicinfo alloc] init] autorelease];
            NSString *email = [[[NSString alloc] initWithFormat:@""] autorelease];
            NSString *password = [[[NSString alloc] initWithFormat:@""] autorelease];

            defaultInfo.email = email;
            defaultInfo.password = password;
            defaultInfo.autoLogin = FALSE;
 
            [_displayedBsicinfo addObject:defaultInfo];
        }
        else {
            for (NSDictionary * currDict in infoDicts) {
                Basicinfo * info = [[[Basicinfo alloc] initWithDictionary:currDict] autorelease];
                [_displayedBsicinfo addObject:info];
            }
        }
    }
    return _displayedBsicinfo;
}

- (NSMutableArray *)displayedObjects {
    if (_displayedObjects == nil) {
        NSString * path = [[self class] pathForDocumentWithName:@"ServerList.plist"];
		NSData * savedData = [NSData dataWithContentsOfFile:path];
		NSArray * infoDicts;
		NSString * errStr;
		NSPropertyListFormat format;
		infoDicts = [NSPropertyListSerialization propertyListFromData:savedData 
													 mutabilityOption:NSPropertyListImmutable
																		format:&format 
													 errorDescription:&errStr];
		//NSLog(@"%@ Path%@",[infoDicts description], path);

		if (infoDicts == nil) {
            NSLog(@"Unable to read plist file at path: %@", path);
            path = [[NSBundle mainBundle] pathForResource:@"ServerList"
                                                   ofType:@"plist"];
            infoDicts = [NSMutableArray arrayWithContentsOfFile:path];
        }
        
        _displayedObjects = [[NSMutableArray alloc] initWithCapacity:[infoDicts count]];
        
#if DEFAULT_DEMO_LINK // jerrylu, 2012/06/27
#if CUSTOMER_FOR_EZ
        ServerInfo *defaultInfo = [[ServerInfo alloc] init];
        NSString *dserverName    = [[NSString alloc] initWithFormat:@"EZwatch Demo"];
        NSString *dserverIP      = [[NSString alloc] initWithFormat:@"ezwatchdemo.dtdns.net"];
        NSString *dserverPort    = [[NSString alloc] initWithFormat:@"5150"];
        NSString *dsPlaybackPort = [[NSString alloc] initWithFormat:@"5150"];
        NSString *duserName      = [[NSString alloc] initWithFormat:@"demo"];
        NSString *duserPassword  = [[NSString alloc] initWithFormat:@"ezwatch123"];
        
        defaultInfo.serverName = dserverName;
        defaultInfo.serverIP = dserverIP;
        defaultInfo.serverPort = dserverPort;
        defaultInfo.serverPlaybackPort = dsPlaybackPort;
        defaultInfo.userName = duserName;
        defaultInfo.userPassword = duserPassword;
        
        [_displayedObjects addObject:defaultInfo];
        
        [dserverName release];
        [dserverIP release];
        [dserverPort release];
        [dsPlaybackPort release];
        [duserName release];
        [duserPassword release];
        
        [defaultInfo release];
        defaultInfo = nil;
#else
        if ([self checkFirstRun]) {
            ServerInfo *defaultInfo = [[ServerInfo alloc] init];
#if CUSTOMER_FOR_NORTHAMERICANCABLE
            NSString *dserverName    = [[NSString alloc] initWithFormat:@"Demo Server"];
            NSString *dserverIP      = [[NSString alloc] initWithFormat:@"palab.securitytronixddns.net"];
            NSString *dserverPort    = [[NSString alloc] initWithFormat:@"5150"];
            NSString *dsPlaybackPort = [[NSString alloc] initWithFormat:@"5160"];
            NSString *duserName      = [[NSString alloc] initWithFormat:@"demo"];
            NSString *duserPassword  = [[NSString alloc] initWithFormat:@"nace"];
#else
#if CUSTOMER_FOR_NOLOGO
            NSString *dserverName    = [[NSString alloc] initWithFormat:@"Demo Server"];
            NSString *dserverIP      = [[NSString alloc] initWithFormat:@"59.124.27.253"];
#else
            NSString *dserverName    = [[NSString alloc] initWithFormat:@"NUUO Demo Server"];
            NSString *dserverIP      = [[NSString alloc] initWithFormat:@"demosite.nuuo.com"];
#endif
            NSString *dserverPort    = [[NSString alloc] initWithFormat:@"5566"];
            NSString *dsPlaybackPort = [[NSString alloc] initWithFormat:@"5567"];
            NSString *duserName      = [[NSString alloc] initWithFormat:@"admin"];
            NSString *duserPassword  = [[NSString alloc] initWithFormat:@"admin"];
#endif
            defaultInfo.serverName = dserverName;
            defaultInfo.serverIP = dserverIP;
            defaultInfo.serverPort = dserverPort;
            defaultInfo.serverPlaybackPort = dsPlaybackPort;
            defaultInfo.userName = duserName;
            defaultInfo.userPassword = duserPassword;
            
            [_displayedObjects addObject:defaultInfo];
            
            [dserverName release];
            [dserverIP release];
            [dserverPort release];
            [dsPlaybackPort release];
            [duserName release];
            [duserPassword release];
            
            [defaultInfo release];
            defaultInfo = nil;
        }
#endif // CUSTOMER_FOR_EZ
#endif // DEFAULT_DEMO_LINK
        
		for (NSDictionary * currDict in infoDicts) {
			ServerInfo * info = [[[ServerInfo alloc] initWithDictionary:currDict] autorelease];
			[_displayedObjects addObject:info];
		}
	}
    //_logined = NO;
    return _displayedObjects;
}

- (void)addObject:(id) anObject {
    if (anObject != nil) {
        if ([anObject isKindOfClass:[ServerInfo class]]) {
            [[self displayedObjects] addObject:anObject];
        }
        else if ([anObject isKindOfClass:[MyViewInfo class]]) {
            [[self displayedMyView] addObject:anObject];
        }
        else if ([anObject isKindOfClass:[Basicinfo class]]) {
            [[self displayedBsicinfo] removeLastObject];
            [[self displayedBsicinfo] addObject:anObject];
            [self saveDisplayedBasicinfo];
            
            return;
        }
    }
    [self save];
}

- (void)clearandSaveData:(NSMutableArray*) server with:(NSMutableArray*) view
{
#if FUNC_PUSH_NOTIFICATION_SUPPORT
    NSMutableArray* oldServers = [self displayedObjects];
    for (int i = 0; i < [oldServers count]; i++)
    {
        ServerInfo* oldServer = [oldServers objectAtIndex:i];
        bool findServer = false;

        for (int j = 0; j < [server count]; j++)
        {
            ServerInfo* newServer = [server objectAtIndex:j];
            
            if (   [oldServer connectionType] == eIPConnection
                && [[oldServer serverIP] isEqualToString:[newServer serverIP]]
                && [oldServer serverPort] == [newServer serverPort]
                && [oldServer serverPlaybackPort] == [newServer serverPlaybackPort])
            {
                findServer = true;
                break;
            }
            else if (   [oldServer connectionType] == eP2PConnection
                     && [[oldServer serverID_P2P] isEqualToString:[newServer serverID_P2P]])
            {
                findServer = true;
                break;
            }
        }

        if (findServer == true)
            continue;

        // Unregister Push Notification
        NSThread *unregister_thread = [[NSThread alloc] initWithTarget:self selector:@selector(unRegPushServer:) object:oldServer];
        [unregister_thread start];
        [unregister_thread release];
        
        LiveViewAppDelegate *appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
        [appDelegate removeEventListByServerInfo:oldServer];
        [appDelegate saveEventList];
    }
    
    [oldServers removeAllObjects];
#endif
    
    for (int i = 0; i < [server count]; i++)
    {
        [[self displayedObjects] addObject:[server  objectAtIndex:i]];
    }

    [[self displayedMyView] removeAllObjects];
    for (int i = 0; i < [view count]; i++)
    {
        [[self displayedMyView] addObject:[view  objectAtIndex:i]];
    }
}

- (void)save {
    [self saveDisplayedMyView];
    [self saveDisplayedMyServer];
}

- (void)saveDisplayedMyServer {
    NSString * path = [[self class] pathForDocumentWithName:@"ServerList.plist"];
    NSMutableArray * infoList = [[NSMutableArray alloc] init];
    for (ServerInfo * info in [self displayedObjects]) {
#if DEFAULT_DEMO_LINK // jerrylu, 2012/06/27
#if CUSTOMER_FOR_EZ
        int index = [self.displayedObjects indexOfObject:info];
        if (index == DEFAULT_DEMO_ROW) {
            continue;
        }
#else
        if ([self checkFirstRun]) {
            int index = [self.displayedObjects indexOfObject:info];
            if (index == DEFAULT_DEMO_ROW) {
                continue;
            }
        }
#endif // CUSTOMER_FOR_EZ
#endif // DEFAULT_DEMO_LINK
        [infoList addObject:[info getPropertyDic]];
    }
    NSString * errStr;
    NSData * pList = [NSPropertyListSerialization dataFromPropertyList:infoList format:NSPropertyListXMLFormat_v1_0 errorDescription:&errStr];
    [infoList release];
    [pList writeToFile:path atomically:YES];
}

- (void)saveDisplayedBasicinfo {
    NSString * path = [[self class] pathForDocumentWithName:@"BasicInfo.plist"];
	NSMutableArray * infoList = [[NSMutableArray alloc] init];
	for (Basicinfo * info in [self displayedBsicinfo]) {
		[infoList addObject:[info getPropertyDic]];
	}
	NSString * errStr;
	NSData * pList = [NSPropertyListSerialization dataFromPropertyList:infoList format:NSPropertyListXMLFormat_v1_0 errorDescription:&errStr];
	[infoList release];
    if (![pList writeToFile:path atomically:YES])
        NSLog(@"Failed to write the basicinfo List");
}

// jerrylu, 2012/08/20
- (NSMutableArray *)unregServerArray {
    if (_unregServerArray == nil) {
        NSString * path = [[self class] pathForDocumentWithName:@"UnregServerList.plist"];
		NSData * savedData = [NSData dataWithContentsOfFile:path];
		NSArray * infoDicts;
		NSString * errStr;
		NSPropertyListFormat format;
		infoDicts = [NSPropertyListSerialization propertyListFromData:savedData
													 mutabilityOption:NSPropertyListImmutable
                                                               format:&format
													 errorDescription:&errStr];
		//NSLog(@"%@ Path%@",[infoDicts description], path);
        
		if (infoDicts == nil) {
            NSLog(@"Unable to read plist file at path: %@", path);
            path = [[NSBundle mainBundle] pathForResource:@"UnregServerList"
                                                   ofType:@"plist"];
            infoDicts = [NSMutableArray arrayWithContentsOfFile:path];
        }
        
        _unregServerArray = [[NSMutableArray alloc] initWithCapacity:[infoDicts count]];
        
		for (NSDictionary * currDict in infoDicts) {
			ServerInfo * info = [[[ServerInfo alloc] initWithDictionary:currDict] autorelease];
			[_unregServerArray addObject:info];
        }
	}
    
    return _unregServerArray;
}

- (void)saveUnregServerArray {
    NSMutableArray * infoList = [[NSMutableArray alloc] init];
    for (ServerInfo * info in [self unregServerArray]) {
		[infoList addObject:[info getPropertyDic]];
#if DEBUG_LOG
        NSLog(@"%s Server Name: %@", __FUNCTION__, info.serverName);
        NSLog(@"%s Server IP: %@", __FUNCTION__, info.serverIP);
#endif
	}
    
    // Save the file
    NSString * errStr;
	NSData * pList = [NSPropertyListSerialization dataFromPropertyList:infoList format:NSPropertyListXMLFormat_v1_0 errorDescription:&errStr];
    [infoList release];
    
    NSString * path = [[self class] pathForDocumentWithName:@"UnregServerList.plist"];
    if (![pList writeToFile:path atomically:YES])
        NSLog(@"Failed to write the Unreg Server List");
}

- (NSMutableArray *)displayedMyView
{
    if (_displayedMyView == nil)
    {
        NSString * path = [[self class] pathForDocumentWithName:@"MyViewList.plist"];
		NSData * savedData = [NSData dataWithContentsOfFile:path];
		NSArray * infoDicts;
		NSString * errStr;
		NSPropertyListFormat format;
		infoDicts = [NSPropertyListSerialization propertyListFromData:savedData
													 mutabilityOption:NSPropertyListImmutable
                                                               format:&format
													 errorDescription:&errStr];
        
		if (infoDicts == nil)
        {
            NSLog(@"Unable to read plist file at path: %@", path);
            path = [[NSBundle mainBundle] pathForResource:@"MyViewList"
                                                   ofType:@"plist"];
            infoDicts = [NSMutableArray arrayWithContentsOfFile:path];
        }
        
        _displayedMyView = [[NSMutableArray alloc] initWithCapacity:[infoDicts count]];
		for (NSDictionary * currDict in infoDicts)
        {
            NSDictionary * serverDicts = [currDict objectForKey:@"serverList"];
            
            MyViewInfo * info = [[[MyViewInfo alloc] initWithDictionary:currDict] autorelease];
            NSMutableArray * serverlist = [[NSMutableArray alloc] initWithCapacity:[serverDicts count]];
			info.serverList = serverlist;
            [serverlist release];
            
            for (NSDictionary * serverDict in serverDicts)
            {
                NSDictionary * cameraDicts = [serverDict objectForKey:@"cameraList"];
                
                MyViewServerInfo * sinfo = [[[MyViewServerInfo alloc] initWithDictionary:serverDict] autorelease];
                NSMutableArray * cameralist = [[NSMutableArray alloc] initWithCapacity:[cameraDicts count]];
                sinfo.cameraList = cameralist;
                [cameralist release];
                
                for (NSDictionary * cameraDict in cameraDicts)
                {
                    MyViewCameraInfo * cinfo = [[[MyViewCameraInfo alloc] initWithDictionary:cameraDict] autorelease];
                    [sinfo.cameraList addObject:cinfo];
                }
                
                [info.serverList addObject:sinfo];
            }
            [_displayedMyView addObject:info];
		}

        //for iViewer 4.3 to 4.4 or above
        //fix titan ID problem
        [self WorkAround:_displayedMyView];
    }
    return _displayedMyView;
}

- (void)saveDisplayedMyView {
    NSMutableArray * infoList = [[NSMutableArray alloc] init];
    for (MyViewInfo * info in [self displayedMyView]) {
        NSMutableArray * serverInfoList = [[NSMutableArray alloc] init];
        for (MyViewServerInfo * sinfo in [info serverList]) {
            NSMutableArray * cameraInfoList = [[NSMutableArray alloc] init];
            for (MyViewCameraInfo * cinfo in [sinfo cameraList]) {
                MyViewCameraInfo *new_cinfo = [[[MyViewCameraInfo alloc] init] autorelease];
                [new_cinfo setCamCentralID:cinfo.camCentralID];
                [new_cinfo setCamLocalID:cinfo.camLocalID];
                [new_cinfo setRsCentralID:cinfo.rsCentralID];
                [new_cinfo setRsLocalID:cinfo.rsLocalID];
                [new_cinfo setCameraName:cinfo.cameraName];
                [new_cinfo setCameraOrder:cinfo.cameraOrder];
                [cameraInfoList addObject:[new_cinfo getPropertyDic]];
            }
            MyViewServerInfo *new_sinfo = [[[MyViewServerInfo alloc] init] autorelease];
            [new_sinfo setServerID:sinfo.serverID];
            [new_sinfo setUserName:sinfo.userName];
            [new_sinfo setCameraList:cameraInfoList];
            [serverInfoList addObject:[new_sinfo getPropertyDic]];
            [cameraInfoList release];
        }
        MyViewInfo *new_info = [[[MyViewInfo alloc] init] autorelease];
        [new_info setViewName:info.viewName];
        [new_info setServerList:serverInfoList];
        [new_info setMyViewLayoutType:info.myViewLayoutType];
		[infoList addObject:[new_info getPropertyDic]];
        [serverInfoList release];
#if DEBUG_LOG
        NSLog(@"%s MyView Name: %@", __FUNCTION__, info.viewName);
#endif
	}
    
    // Save the file
    NSString * errStr;
	NSData * pList = [NSPropertyListSerialization dataFromPropertyList:infoList format:NSPropertyListXMLFormat_v1_0 errorDescription:&errStr];
    [infoList release];
    
    NSString * path = [[self class] pathForDocumentWithName:@"MyViewList.plist"];
    if (![pList writeToFile:path atomically:YES])
        NSLog(@"Failed to write the My View List");
}

- (BOOL)checkFirstRun {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"firstRun"])
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"firstRun"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    BOOL firstrun = [defaults boolForKey:@"firstRun"];
    return firstrun;
}

- (void)setFirstRun:(BOOL) status {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:status] forKey:@"firstRun"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -
#pragma mark UIViewController
- (void)viewWillAppear:(BOOL) animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController setToolbarHidden:NO animated:NO];
    
    // jerrylu, 2012/06/15
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.toolbar setTranslucent:NO];

    self.navigationController.delegate = self;
    [[self tableView] reloadData];

    // jerrylu, 2012/09/03, fix bug8403
    if ((NSNull *)aboutAlert != [NSNull null]) {
        [aboutAlert release];
        aboutAlert = nil;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // jerrylu, 2012/09/03, fix bug8403
    if ((NSNull *)aboutAlert != [NSNull null]) {
        [aboutAlert dismissWithClickedButtonIndex:-1 animated:NO];
        [aboutAlert release];
        aboutAlert = nil;
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
	[self setTitle:NSLocalizedString(@"My Sites", nil)];
    
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
    
    //  Configure the Edit button
	[[self editButtonItem] setTitle:NSLocalizedString(@"Manage", nil)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[self editButtonItem] setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [[self editButtonItem] setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    [[self navigationItem] setLeftBarButtonItem:[self editButtonItem]];
    
    //  Configure the Add button
    UIBarButtonItem * addButton = [[UIBarButtonItem alloc] 
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                  target:self
                                  action:@selector(add)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [addButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [addButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#if FUNC_QRCODE_SCAN
    // QRCode button
    UIImage *scanImage;
    UIButton *scanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        scanImage = [UIImage imageNamed:@"QRCodec_48x48_nor.png"];
    else
        scanImage = [UIImage imageNamed:@"QRCodec_32x32_nor.png"];
    scanButton.bounds = CGRectMake(0, 0, scanImage.size.width, scanImage.size.height);
    [scanButton setImage:scanImage forState:UIControlStateNormal];
    [scanButton addTarget:self action:@selector(goToScanQRcode) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * qrcodeButton = [[UIBarButtonItem alloc] initWithCustomView:scanButton];
    
    // Sync profile button
    UIImage *syncImage;
    UIButton* syncButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    _isLoginCloud = FALSE;
    if (IS_IPAD)
        syncImage = [UIImage imageNamed:@"NoSyncProfile_48x48_nor.png"];
    else
        syncImage = [UIImage imageNamed:@"NoSyncProfile_32x32_nor.png"];
    
    syncButton.bounds = CGRectMake(0, 0, syncImage.size.width, syncImage.size.height);
    [syncButton setImage:syncImage forState:UIControlStateNormal];
    [syncButton addTarget:self action:@selector(goToAccountLogin) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * syncProfileButton = [[UIBarButtonItem alloc] initWithCustomView:syncButton];
    
    // Add to button array
    NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects:addButton, qrcodeButton, syncProfileButton, nil];
    [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
    
    [rightBarButtonArray release];
    [qrcodeButton release];
    [addButton release];
    [syncButton release];
#else
    [[self navigationItem] setRightBarButtonItem:addButton];
    [addButton release];
#endif
    
	//add info button
	[[self navigationController] setToolbarHidden:NO animated:NO];
	self.navigationController.toolbar.barStyle = UIBarStyleBlackTranslucent;
	
	UIImage * image = [UIImage imageNamed:@"Btn_Inspector.png"];
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
	[button setImage:image forState:UIControlStateNormal];
	[button addTarget:self action:@selector(showAbout) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem * infoButton = [[UIBarButtonItem alloc] initWithCustomView:button];
	
	UIBarButtonItem * flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
																			   target:nil
																			   action:nil];

	NSArray * buttonArray = [[NSArray alloc] initWithObjects:flexItem, infoButton, flexItem, nil];
	[self setToolbarItems:buttonArray];
    [infoButton release];
    [flexItem release];
    [buttonArray release];
    
    if ([[self displayedMyView] count] > 0) {
        _isShowMyServer = NO;
        _isShowMyView = YES;
    }
    else {
        _isShowMyServer = YES;
        _isShowMyView = NO;
    }
    
    // jerrylu, 2012/10/04, fix bug8617
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(becomeActive) name:UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];
    
#if FUNC_PUSH_NOTIFICATION_SUPPORT
    NSThread *unregAllThread = [[NSThread alloc] initWithTarget:self selector:@selector(connectToUnRegSiteList) object:nil];
    [unregAllThread start];
    [unregAllThread release];
#endif
}

- (void)showAbout {
#if CUSTOMER_FOR_SEAGATE
    NSString *officialVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *version = [NSString stringWithFormat:NSLocalizedString(@"Version %@\n\u00A92014 Seagate Technology LLC", nil), officialVer];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        aboutAlertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"About", nil) message:version preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [aboutAlertController dismissViewControllerAnimated:YES completion:nil];
                             }];
        UIAlertAction* moreInfo = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Terms of Service", nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       HelpViewController *helpview = [[HelpViewController alloc] init];
                                       [self.navigationController pushViewController:helpview animated:NO];
                                       [helpview release];
                                       [aboutAlertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
        [aboutAlertController addAction:ok];
        [aboutAlertController addAction:moreInfo];
        [self presentViewController:aboutAlertController animated:YES completion:nil];
    }
    else {
        UIAlertView * alert;
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"About", nil)
                                           message:version
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                 otherButtonTitles:NSLocalizedString(@"Terms of Service", nil),nil];
        [alert show];
        
        if ((NSNull *)aboutAlert != [NSNull null]) {
            [aboutAlert release];
            aboutAlert = nil;
        }
        aboutAlert = alert;
    }
#else
#if CUSTOMER_FOR_NETGEAR // jerrylu, 2012/04/24
    NSString * productName = [[NSString alloc] initWithFormat:@"ReadyNAS Surveillance"];
#elif CUSTOMER_FOR_BOLIDE // jerrylu, 2012/04/24
    NSString * productName = [[NSString alloc] initWithFormat:@"BolideViewer"];
#else
    NSString * productName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
#endif // CUSTOMER_FOR_NETGEAR
    
    NSString * officialVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString * innerVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        //NSString *version = [NSString stringWithFormat:@"%@ %@\nBuild %@", productName, officialVer, innerVer];
        NSString *version = [NSString stringWithFormat:@"%@ %@", productName, officialVer];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            aboutAlertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"About", nil) message:version preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [aboutAlertController dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [aboutAlertController addAction:ok];
#if FUNC_HELP_PAGE
            UIAlertAction* moreInfo = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"HELP", nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           HelpViewController *helpview = [[HelpViewController alloc] init];
                                           [self.navigationController pushViewController:helpview animated:NO];
                                           [helpview release];
                                           [aboutAlertController dismissViewControllerAnimated:YES completion:nil];
                                       }];
            [aboutAlertController addAction:moreInfo];
#endif
            [self presentViewController:aboutAlertController animated:YES completion:nil];
        }
        else {
            UIAlertView * alert;
#if FUNC_HELP_PAGE
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"About", nil)
                                               message:version
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                     otherButtonTitles:NSLocalizedString(@"HELP", nil),nil];
#else
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"About", nil)
                                               message:version
                                              delegate:nil
                                     cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                     otherButtonTitles:nil];
#endif
            [alert show];
            
            if ((NSNull *)aboutAlert != [NSNull null]) {
                [aboutAlert release];
                aboutAlert = nil;
            }
            aboutAlert = alert;
        }
    }
    else {
        UILabel * officalLabel = [[UILabel alloc] initWithFrame:CGRectMake(25.0, 50.0, 240.0, 25.0)];
        UIFont* font = [UIFont systemFontOfSize:17.0];
        [officalLabel setFont:font];
        [officalLabel setTextColor:[UIColor whiteColor]];
        [officalLabel setBackgroundColor:[UIColor clearColor]];
        [officalLabel setTextAlignment:NSTextAlignmentCenter];
        NSString * strOffical = [NSString stringWithFormat: @"%@ %@", productName, officialVer];
        [officalLabel setText:strOffical];
        
        UILabel * buildLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 80.0, 240.0, 25.0)];
        UIFont* fontBuild = [UIFont systemFontOfSize:17.0];
        [buildLabel setFont:fontBuild];
        [buildLabel setTextColor:[UIColor darkGrayColor]];
        [buildLabel setBackgroundColor:[UIColor clearColor]];
        [buildLabel setTextAlignment:NSTextAlignmentCenter];
        NSString * strBuild = [NSString stringWithFormat: @"Build %@", innerVer];
        [buildLabel setText:strBuild];
        
        UIAlertView * alert;
#if FUNC_HELP_PAGE
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"About", nil)
                                           message:@"\n\n\n"
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                 otherButtonTitles:NSLocalizedString(@"HELP", nil),nil];
#else
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"About", nil)
                                           message:@"\n\n\n"
                                          delegate:nil
                                 cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                 otherButtonTitles:nil];
#endif
        [alert addSubview:officalLabel];
        //[alert addSubview:buildLabel];
        [alert show];

        if ((NSNull *)aboutAlert != [NSNull null]) {
            [aboutAlert release];
            aboutAlert = nil;
        }
        aboutAlert = alert;

        [buildLabel release];
        [officalLabel release];
    }
    
#if CUSTOMER_FOR_NETGEAR || CUSTOMER_FOR_BOLIDE
    [productName release];
#endif // CUSTOMER_FOR_NETGEAR
#endif // CUSTOMER_FOR_SEAGATE
}

- (void)setEditing:(BOOL)editing
          animated:(BOOL)animated {
    [super setEditing:editing
             animated:animated];
    UIBarButtonItem *editButton = [[self navigationItem] rightBarButtonItem];
    [editButton setEnabled:!editing];
	if (!editing) {
		[self save];
        [[self editButtonItem] setTitle:NSLocalizedString(@"Manage", nil)];
	}
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        [self.tableView reloadData];
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

#pragma mark -
#pragma mark Action Methods

//  Creates a new nav controller with an instance of MyDetailController as
//  its root view controller, and runs it as a modal view controller. By
//  default, that causes the detail view to be animated as sliding up from
//  the bottom of the screen. And because the detail controller is the root
//  view controller, there's no back button.
//
- (void)add {
    AddSiteInfoController * controller = [[AddSiteInfoController alloc] initWithStyle:UITableViewStyleGrouped];
    [controller setFavoriteSite:self];
    
    UINavigationController * newNavController = [[UINavigationController alloc]
                                                initWithRootViewController:controller];
    newNavController.navigationBar.barStyle = UIBarStyleBlack;
	newNavController.navigationBar.translucent = NO;
    [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
    [newNavController release];
    [controller release];
}

#if FUNC_QRCODE_SCAN
- (void)goToScanQRcode {
    QRcodeViewController *controller = [[QRcodeViewController alloc] initWithNibName:@"QRcodeView" bundle:nil];
    [controller setFavoriteSite:self];
    
    UINavigationController * newNavController = [[UINavigationController alloc]
                                                 initWithRootViewController:controller];
    newNavController.navigationBar.barStyle = UIBarStyleBlack;
    newNavController.navigationBar.translucent = NO;
    [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
    [newNavController release];
    [controller release];
}
#endif

- (BOOL)TryLogin:(Basicinfo*) info {
    NSString *url = [[[NSString alloc] initWithFormat:@"http://54.251.107.11/UserAccount/nuuo/login.php"] autorelease];
    NSString* post = [NSString stringWithFormat:@"email=%@&password=%@",
                      info.email, info.password];
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSData* postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLen = [NSString stringWithFormat:@"%d", [postData length]];
    [urlRequest setValue:postLen forHTTPHeaderField:@"Content-Length"];
    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error;
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    NSString *replyString;
    if (serverReply != nil) {
        replyString = [[[NSString alloc] initWithBytes:[serverReply bytes] length:[serverReply length] encoding: NSASCIIStringEncoding] autorelease];
        
        if ([replyString rangeOfString:@"false"].location != NSNotFound)
        {
            _isLoginCloud = TRUE;
        }
    }
    else {
        replyString = [[[NSString alloc] initWithFormat:@"Unconnection"] autorelease];
        //[self showMessage:@"Can not access to the cloud\nPlease connect to Internet."];
    }
    NSLog(@"reply string is :%@",replyString);
    
    return _isLoginCloud;
}

- (void)goToSyncProfile {
    SyncProfileController1 * controller = [[SyncProfileController1 alloc] initWithStyle:UITableViewStyleGrouped];
    [controller setParent:self];
    [controller setBasicInfo:[[self displayedBsicinfo] objectAtIndex:0]];
    [self.navigationController pushViewController:controller animated:NO];
    [controller release];
}

- (void)goToAccountLogin {
    Basicinfo * info = [[self displayedBsicinfo] objectAtIndex:0];
    // Try login first
    if (_isLoginCloud == FALSE &&
        [info.email length] != 0 &&
        info.autoLogin == TRUE) {
        _isLoginCloud = [self TryLogin:info];
    }
    
    
    /// Remove below code
    //_isLoginCloud = TRUE;
    ///////////
    
    if (_isLoginCloud == TRUE) {
        [self changeLoginStatus:TRUE];
        [self goToSyncProfile];
        
        return;
    }
    
    UserLoginController *controller = [[UserLoginController alloc] initWithStyle:UITableViewStyleGrouped];
    [controller setParent:self];
    [controller setBasicInfo:[[self displayedBsicinfo] objectAtIndex:0]];
    UINavigationController * newNavController = [[UINavigationController alloc] initWithRootViewController:controller];
    newNavController.navigationBar.barStyle = UIBarStyleBlack;
    newNavController.navigationBar.translucent = NO;
    [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
    [newNavController release];
    [controller release];
}

- (void)changeLoginStatus:(BOOL) bLogin{
    if (_isLoginCloud == bLogin) {
        return;
    }
        
    _isLoginCloud = bLogin;
    
    if (_isLoginCloud) // false -> true
    {
       
    }
    else // true -> false
    {
        Basicinfo *defaultInfo = [[[Basicinfo alloc] init] autorelease];
        NSString *email = [[[NSString alloc] initWithFormat:@""] autorelease];
        NSString *password = [[[NSString alloc] initWithFormat:@""] autorelease];
        
        defaultInfo.email = email;
        defaultInfo.password = password;
        defaultInfo.autoLogin = FALSE;
        
        [[self displayedBsicinfo] removeLastObject];
        [[self displayedBsicinfo] addObject:defaultInfo];
        [self saveDisplayedBasicinfo];
    }
}

- (void)connectToSiteAtIndexPath:(NSIndexPath *) path {
    NSUInteger index = [path row];
    ServerManager *serverMgr = [[ServerManager alloc] init];
    [self setServerManager:serverMgr];
    [serverMgr setSiteDelegate:self];
    
    NSMutableArray *serverinfo = [[NSMutableArray alloc] init];
    if (path.section == kMyServerSection) {
        [serverMgr setIsMyViewMode:NO];
        ServerInfo *info = [[self displayedObjects] objectAtIndex:index];
        [serverinfo addObject:info];
    }
    else {
        [serverMgr setIsMyViewMode:YES];
        MyViewInfo *myView = [[self displayedMyView] objectAtIndex:index];
        [serverMgr setMyViewInfo:myView];
        for (MyViewServerInfo *myViewServer in myView.serverList) {
            [serverinfo addObject:myViewServer];
        }
    }
    [serverMgr setAllServer:serverinfo];
    [serverMgr loginAllServer];
    [serverinfo release];
    [serverMgr release];
	[[self navigationController] setToolbarHidden:YES animated:YES];
}

- (void)connectToSiteAtEvent:(Eventinfo *) einfo {
    // get serverinfo, need to complete it
    NSArray *userArray = [einfo.username componentsSeparatedByString:@","];
    NSMutableArray *serverArray = [[NSMutableArray alloc] initWithCapacity:[self.displayedObjects count]];

    for (int i=0; i < [self.displayedObjects count]; i++) {
        ServerInfo *sinfo = [self.displayedObjects objectAtIndex:i];
        for (int j=0; j < [userArray count]; j++) {
            NSString *username = [userArray objectAtIndex:j];
#if FUNC_P2P_SAT_SUPPORT
            if ([sinfo connectionType] == eIPConnection) {
#endif
            if ([einfo.serverID isEqualToString:sinfo.serverID] && [username isEqualToString:sinfo.userName]) {
                [serverArray addObject:sinfo];
            }
#if FUNC_P2P_SAT_SUPPORT
            }
            else {
                if ([einfo.serverID isEqualToString:sinfo.serverID] && [username isEqualToString:sinfo.userName_P2P]) {
                    [serverArray addObject:sinfo];
                }
            }
#endif
        }
    }
    ServerManager *serverMgr = [[ServerManager alloc] init];
    [self setServerManager:serverMgr];
    
    EventViewController *eventView;
    if (IS_IPAD)
#if RENDER_BY_OPENGL
        eventView = [[EventViewController alloc] initWithNibName:@"EventViewController_iPad_gl" bundle:nil];
#else
        eventView = [[EventViewController alloc] initWithNibName:@"EventViewController_iPad" bundle:nil];
#endif
    else
#if RENDER_BY_OPENGL
        eventView = [[EventViewController alloc] initWithNibName:@"EventViewController_gl" bundle:nil];
#else
        eventView = [[EventViewController alloc] initWithNibName:@"EventViewController" bundle:nil];
#endif
    [eventView setServerMgr:serverMgr];
    [eventView setParent:self];
    [eventView setEventInfo:einfo];

    [serverMgr setEventDelegate:eventView];
    [serverMgr setSiteDelegate:self];
    [serverMgr loginAllServerByEvent:serverArray event:einfo];
    
    [serverMgr release];
    [serverArray release];
    
    //push liveView page
    [self.navigationController pushViewController:eventView animated:NO];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [eventView release];
}

- (void)setServerManager:(ServerManager *) srvmgr {
    if (m_ServerMgr != nil) {
        [m_ServerMgr deleteAllServer];
        [m_ServerMgr release];
        m_ServerMgr = nil;
    }    
	m_ServerMgr = srvmgr;
}

- (void)openSlideView:(int) camN WithServerManager:(ServerManager *) srvmgr {
	if ([self.navigationController.viewControllers count] < 2) {
		//Has too less view in stack
		//disconnect
		[srvmgr logoutAllServer];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		return;
	}
	else if (![[self.navigationController.viewControllers objectAtIndex:1] isKindOfClass:[LoadingPage class]]) {
 		[srvmgr logoutAllServer];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		return;
	}
	else {
        ELayoutType myLayout = [m_ServerMgr getLayoutType];
        SlideView * cams = [[SlideView alloc] initWithCameraNumber:camN layout:myLayout];
        [cams setParent:self];
        [cams setServerMgr:m_ServerMgr];
        [m_ServerMgr setDisplayDelegate:cams];
#if FUNC_P2P_SAT_SUPPORT
        if ([m_ServerMgr isConnectionWithP2PServer] && [m_ServerMgr isP2PConnectionWithRelayMode]) {
            NSString * new_title = [[NSString alloc] initWithFormat:NSLocalizedString(@"%@(relay)", nil), [m_ServerMgr getServerManagerTitle]];
            [cams setTitle:new_title];
            [new_title release];
        }
        else {
#endif
            [cams setTitle:[m_ServerMgr getServerManagerTitle]];
#if FUNC_P2P_SAT_SUPPORT
        }
#endif
        //pop loading page
		[self.navigationController popViewControllerAnimated:NO];
		//push liveView page
		[self.navigationController pushViewController:cams animated:NO];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		[cams release];
	}
}

- (void)openRecordingServerView:(ServerManager *) srvmgr {
	if ([self.navigationController.viewControllers count] < 2) {
		//Has too less view in stack
		//disconnect
		[srvmgr logoutAllServer];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		return;
	}
	else if (![[self.navigationController.viewControllers objectAtIndex:1] isKindOfClass:[LoadingPage class]]) {
 		[srvmgr logoutAllServer];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		return;
	}
	else {
		RecordingServerListController *rsView = [[RecordingServerListController alloc] initWithStyle:UITableViewStylePlain];
        [rsView setParent:self];
        [rsView setServerMgr:m_ServerMgr];
        [rsView setTitle:[m_ServerMgr getServerManagerTitle]];
        
        //pop loading page
		[self.navigationController popViewControllerAnimated:NO];
		//push recordingserverview page
		[self.navigationController pushViewController:rsView animated:YES];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		[rsView release];
	}
}

- (void)failToConnect {
	if([self.navigationController.viewControllers count] != 2) {
		return;
	}
	
    [self.navigationController popViewControllerAnimated:NO];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Can't connect to server", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                        message:NSLocalizedString(@"Can't connect to server", nil)
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

// jerrylu, 2012/04/30, bug6848: zero camera crash
- (void)failToConnectZeroCamera {
	if([self.navigationController.viewControllers count] != 2) {
		return;
	}

	[self.navigationController popViewControllerAnimated:NO];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil)
#if CUSTOMER_FOR_DLINK
                                                                       message:NSLocalizedString(@"no camera connected", nil)
#else
                                                                       message:NSLocalizedString(@"No camera", nil)
#endif
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
#if CUSTOMER_FOR_DLINK
                                                        message:NSLocalizedString(@"no camera connected", nil)
#else
                                                        message:NSLocalizedString(@"No camera", nil)
#endif
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)openServerView:(ServerInfo *) sinfo {
    if ([sinfo.userName_P2P isEqualToString:@""]) {
        SiteInfoController * controller = [[SiteInfoController alloc] initWithStyle:UITableViewStyleGrouped];
        [controller setServerInfo:sinfo];
        [controller setFavoriteSite:self];
        [controller setIsAdding:YES];
        UINavigationController * newNavController = [[UINavigationController alloc]
                                                     initWithRootViewController:controller];
        newNavController.navigationBar.barStyle = UIBarStyleBlack;
        newNavController.navigationBar.translucent = NO;
        [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
        [newNavController release];
        [controller release];
    }
    else {
        NSString *msg_str;
        NSIndexPath *indexPath;
        
        if ([self IsDuplicateP2PServer:sinfo]) {
            int server_index = 0;
            for (ServerInfo *current_sinfo in _displayedObjects) {
                if (current_sinfo.connectionType == eP2PConnection &&
                    [current_sinfo.serverID_P2P isEqualToString:sinfo.serverID_P2P] &&
                    [current_sinfo.userName_P2P isEqualToString:sinfo.userName_P2P]) {
                    server_index = [_displayedObjects indexOfObject:current_sinfo];
                    current_sinfo.userPassword_P2P = sinfo.userPassword_P2P;
                    break;
                }
            }
            [self setIsShowMyServer:YES];
            [self setIsShowMyView:NO];
            [self save];
            
            indexPath = [NSIndexPath indexPathForRow:server_index inSection:kMyServerSection];
            msg_str = [NSString stringWithFormat:NSLocalizedString(@"This server has already existed", nil)];
        }
        else {
            [self addObject:sinfo];
            [self setIsShowMyServer:YES];
            [self setIsShowMyView:NO];
            [self save];
            
            int server_index = [_displayedObjects count] - 1;
            indexPath = [NSIndexPath indexPathForRow:server_index inSection:kMyServerSection];
            msg_str = [NSString stringWithFormat:NSLocalizedString(@"Server list is created.", nil)];
        }
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:msg_str preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                     [self performSelectorOnMainThread:@selector(openServerViewAtIndexPath:) withObject:indexPath waitUntilDone:NO];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                            message:msg_str
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
            
            [self performSelectorOnMainThread:@selector(openServerViewAtIndexPath:) withObject:indexPath waitUntilDone:NO];
        }
    }
}

- (BOOL)IsDuplicateP2PServer:(ServerInfo *)input_sinfo {
    // Check duplicate account
    for (ServerInfo *sinfo in [self displayedObjects]) {
        if (sinfo.connectionType == eP2PConnection &&
            [sinfo.serverID_P2P isEqualToString:input_sinfo.serverID_P2P] &&
            [sinfo.userName_P2P isEqualToString:input_sinfo.userName_P2P]) {
            return YES;
        }
    }
    return NO;
}

- (void)openServerViewAtIndexPath:(NSIndexPath *) path {
    ServerManager *serverMgr = [[ServerManager alloc] init];
    [self setServerManager:serverMgr];
    [serverMgr setSiteDelegate:self];
    
    NSMutableArray *serverinfo = [[NSMutableArray alloc] init];
    [serverMgr setIsMyViewMode:NO];
    [serverinfo addObject:[_displayedObjects objectAtIndex:[path row]]];
    [serverMgr setAllServer:serverinfo];
    [serverMgr loginAllServer];
    [serverinfo release];
    [serverMgr release];
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    LoadingPage * loadingPage = [[LoadingPage alloc] init];
    [loadingPage setParent:self];
    [loadingPage setSelectedIndex:path];
    [m_ServerMgr setLoadingDelegate:loadingPage]; // jerrylu, 2012/09/12
    [self.navigationController pushViewController:loadingPage animated:NO];
    [loadingPage release];
    
    _logining = YES;
}

- (void)openMyView:(MyViewInfo *) vinfo {
    ServerManager *serverMgr = [[ServerManager alloc] init];
    [self setServerManager:serverMgr];
    [serverMgr setSiteDelegate:self];
    
    NSMutableArray *serverinfo = [[NSMutableArray alloc] init];
    [serverMgr setIsMyViewMode:YES];
    [serverMgr setMyViewInfo:vinfo];
    for (MyViewServerInfo *myViewServer in vinfo.serverList) {
        [serverinfo addObject:myViewServer];
    }
    [serverMgr setAllServer:serverinfo];
    [serverMgr loginAllServer];
    [serverinfo release];
    [serverMgr release];
	[[self navigationController] setToolbarHidden:YES animated:YES];
    
    LoadingPage * loadingPage = [[LoadingPage alloc] init];
    [loadingPage setParent:self];
    [m_ServerMgr setLoadingDelegate:loadingPage]; // jerrylu, 2012/09/12
	[self.navigationController pushViewController:loadingPage animated:NO];
	[loadingPage release];
    
    _logining = YES;
}

// jerrylu, 2012/07/26
- (void)openEventView:(Eventinfo *) einfo {
    if ([self.navigationController.viewControllers count] >= 2) {
        [m_ServerMgr setLoadingDelegate:nil];
        [m_ServerMgr setSiteDelegate:nil];
        [m_ServerMgr setDisplayDelegate:nil];
        [m_ServerMgr setPlaybackDelegate:nil];
        [m_ServerMgr setEventDelegate:nil];
        
        if ([m_ServerMgr isServerLogouting] == NO) {
            [m_ServerMgr logoutAllServer];
        }
	}
    
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    [appDelegate popToRootView];
    
    _logining = YES;
    
    [NSThread sleepForTimeInterval:1];
    
    [self connectToSiteAtEvent:einfo];
}

- (void)openLiveViewFromEventView:(int) CameraIndex {
    ELayoutType myLayout = [m_ServerMgr getLayoutType];
    SlideView * cams = [[SlideView alloc] initWithCameraNumber:[m_ServerMgr getCameraTotalNum] layout:myLayout];
    [cams setServerMgr:m_ServerMgr];
    [cams setParent:self];
    
    if ([m_ServerMgr isConnectionWithP2PServer] == NO) {
        [cams setTitle:[m_ServerMgr getServerManagerTitle]];
    }
    else {
        if ([m_ServerMgr isP2PConnectionWithRelayMode]) {
             NSString * new_title = [[NSString alloc] initWithFormat:NSLocalizedString(@"%@(relay)", nil), [m_ServerMgr getServerManagerTitle]];
            [cams setTitle:new_title];
            [new_title release];
        }
        else {
            [cams setTitle:[m_ServerMgr getServerManagerTitle]];
        }
    }
    [m_ServerMgr goLiveViewFromEventView:cams];

    // automatically change page by camera
    [cams changePagebyCameraIndexbyEvent:CameraIndex];
    
    //pop EventView page
    [self.navigationController popViewControllerAnimated:NO];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.toolbar setTranslucent:NO];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    //push LiveView page
    [self.navigationController pushViewController:cams animated:NO];
    
    [cams release];
}

#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/20
- (void)unRegPushServer:(ServerInfo *) sinfo {
    if (sinfo.serverType != eServer_MainConsole &&
        sinfo.serverType != eServer_NVRmini &&
        sinfo.serverType != eServer_NVRsolo)
        return;
    
    if (sinfo.serverPushNotificationEnable == kPushNotificationDisable || [sinfo.serverID isEqualToString:@""])
        return;
    
    NSLog(@"-------------------------");
    if (sinfo.connectionType == eIPConnection) {
        NSLog(@"server name: %@", sinfo.serverName);
        NSLog(@"server ip: %@", sinfo.serverIP);
        NSLog(@"server port: %@ %@", sinfo.serverPort, sinfo.serverPlaybackPort);
        NSLog(@"server user: %@ %@", sinfo.userName, sinfo.userPassword);
    }
    else {
        NSLog(@"server name: %@", sinfo.serverName_P2P);
        NSLog(@"server id: %@", sinfo.serverID_P2P);
        NSLog(@"server user: %@ %@", sinfo.userName_P2P, sinfo.userPassword_P2P);
    }
    NSLog(@"-------------------------");
    
    for (ServerInfo * info in [self unregServerArray]) {
        if (sinfo.connectionType == info.connectionType) {
            if (sinfo.connectionType == eP2PConnection &&
                [sinfo.serverID_P2P isEqualToString:info.serverID_P2P] &&
                [sinfo.userName_P2P isEqualToString:info.userName_P2P] &&
                [sinfo.userPassword_P2P isEqualToString:info.userPassword_P2P])
            {
                return;
            }
            else if (sinfo.connectionType == eIPConnection &&
                     [sinfo.serverIP isEqualToString:info.serverIP] &&
                     [sinfo.serverPort isEqualToString:info.serverPort] &&
                     [sinfo.userName isEqualToString:info.userName] &&
                     [sinfo.userPassword isEqualToString:info.userPassword]) {
                return;
            }
        }
    }
    
    // Add the new un-reg server
    [self.unregServerArray addObject:sinfo];
    [self saveUnregServerArray];
    
    ControlHelper *pnctrhelper = [[ControlHelper alloc] init];
    //[pnctrhelper setPushNotificationDelegate:self];
    if ([pnctrhelper pnUnregisterToServerWithoutLogin:sinfo]) {
        // Unregister Push Notification successfully
        [self.unregServerArray removeObject:sinfo];
        [self saveUnregServerArray];
    }
    [pnctrhelper release];
}

- (void)connectToUnRegSiteList {
#if DEBUG_LOG
    NSLog(@"UnRegister the previous site list.");
#endif
    if ((NSNull *)[self unregServerArray] == [NSNull null])
        return;
    
    if ([[self unregServerArray] count] == 0)
        return;
    
    // Unregister Push Notification
    ControlHelper *pnctrhelper = [[ControlHelper alloc] init];
    NSMutableArray *new_unregServerArray = [[NSMutableArray alloc] init];
    for (ServerInfo *info in _unregServerArray) {
        NSLog(@"-------------------------");
        if (info.connectionType == eIPConnection) {
            NSLog(@"server name: %@", info.serverName);
            NSLog(@"server ip: %@", info.serverIP);
            NSLog(@"server port: %@ %@", info.serverPort, info.serverPlaybackPort);
            NSLog(@"server user: %@ %@", info.userName, info.userPassword);
        }
        else {
            NSLog(@"server name: %@", info.serverName_P2P);
            NSLog(@"server id: %@", info.serverID_P2P);
            NSLog(@"server user: %@ %@", info.userName_P2P, info.userPassword_P2P);
        }
        NSLog(@"-------------------------");
        
        BOOL ret = [pnctrhelper pnUnregisterToServerWithoutLogin:info];
        if (!ret) {
            [new_unregServerArray addObject:info];
        }
    }
    [pnctrhelper release];
    
    [_unregServerArray release];
    _unregServerArray = new_unregServerArray;
    
    [self saveUnregServerArray];
    
    return;
}
#endif

- (void)becomeActive {
#if FUNC_PUSH_NOTIFICATION_SUPPORT
    // jerrylu, 2012/08/20, Success to connect site, so un-reg other sites
    NSThread *unregAllThread = [[NSThread alloc] initWithTarget:self selector:@selector(connectToUnRegSiteList) object:nil];
    [unregAllThread start];
    [unregAllThread release];
#endif
}

#pragma mark -
#pragma mark ServerManagerLoginDelegate Protocol

- (void)serverManagerDidGetCameraCount:(ServerManager *)servermgr CameraCount:(int)count {    
	_logining = NO;
    
    if (count > 0)
        [self openSlideView:count WithServerManager:servermgr];
    else
        [self failToConnectZeroCamera];
}

- (void)serverManagerCantConnectToServer:(ServerManager *)servermgr {
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/12/17
    if ([[self.navigationController.viewControllers objectAtIndex:1] isKindOfClass:[EventViewController class]]) {
        return;
    }
#endif
    
	if (_logining) {
		[self failToConnect];
	}
}

-(void)serverManagerloginCrystalServer:(ServerManager *)servermgr {
    _logining = NO;
    [self openRecordingServerView:servermgr];
}

- (void)serverManagerGetServerlistByServerID:(NSString *) sid userName:(NSString *)uname serverlist:(NSMutableArray *)slist {
    for (int i = 0; i < [[self displayedObjects] count]; i++) {
        ServerInfo *info = [[self displayedObjects] objectAtIndex:i];
        if ([info.serverID isEqualToString:sid] &&
            ((info.connectionType == eIPConnection && [info.userName isEqualToString:uname]) ||
             (info.connectionType == eP2PConnection && [info.userName_P2P isEqualToString:uname]))
            ) {
            [slist addObject:info];
        }
    }
}

#pragma mark -
#pragma mark ServerManagerSiteListDelegate Protocol
- (void)serverManagerSaveInfo:(ServerManager *)servermgr {
    [self save];
}

#pragma mark -
#pragma mark UITableViewDelegate Protocol
//
//  The table view's delegate is notified of runtime events, such as when
//  the user taps on a given row, or attempts to add, remove or reorder rows.

//  Notifies the delegate when the user selects a row.
//
- (void)tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath {
	//do connection
	[self connectToSiteAtIndexPath:indexPath];
	LoadingPage * loadingPage = [[LoadingPage alloc] init];
	[self.navigationController pushViewController:loadingPage animated:NO];
	[loadingPage setParent:self];
    [loadingPage setSelectedIndex:indexPath]; // jerrylu, 2012/10/02
    [m_ServerMgr setLoadingDelegate:loadingPage]; // jerrylu, 2012/09/12
	[loadingPage release];
	//NSTimeInterval waitTime = 100000000;
	_logining = YES;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *) tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *) indexPath {
    [self showSiteInfoDetail:indexPath]; // jerrylu, 2012/10/02
}

- (void) showSiteInfoDetail:(NSIndexPath *) indexPath {
    if (indexPath.section == kMyServerSection)
    {
        SiteInfoController * controller = [[SiteInfoController alloc]
									   initWithStyle:UITableViewStyleGrouped];
        NSUInteger index = [indexPath row];
        id info = [[self displayedObjects] objectAtIndex:index];
	
        [controller setServerInfo:info];
        [controller setFavoriteSite:self];
#if FUNC_P2P_SAT_SUPPORT
        if ([info connectionType] == eIPConnection) {
#endif
            [controller setTitle:[info serverName]];
#if FUNC_P2P_SAT_SUPPORT
        }
        else {
            [controller setTitle:[info serverName_P2P]];
        }
#endif
        UINavigationController * newNavController = [[UINavigationController alloc]
                                                 initWithRootViewController:controller];
        newNavController.navigationBar.barStyle = UIBarStyleBlack;
        newNavController.navigationBar.translucent = NO;
        [[self navigationController] presentViewController:newNavController animated:YES completion:nil];
        [newNavController release];
        [controller release];
	}
    else
    {
        MyViewInfoController * controller = [[MyViewInfoController alloc] initWithStyle:UITableViewStyleGrouped];
        NSUInteger index = [indexPath row];
        id info = [[self displayedMyView] objectAtIndex:index];
        
        [controller setMyviewInfo:info];
        [controller setFavoriteSite:self];
        
        UINavigationController * newNavController = [[UINavigationController alloc]
                                                     initWithRootViewController:controller];
        newNavController.navigationBar.barStyle = UIBarStyleBlack;
        newNavController.navigationBar.translucent = NO;
        [[self navigationController] presentViewController:newNavController animated:YES completion:nil];        
        [newNavController release];
        [controller release];
    }
	_logining = NO;
}

#pragma mark -
#pragma mark UITableViewDataSource Protocol
//
//  By default, UITableViewController makes itself the delegate of its own
//  UITableView instance, so we can implement data source protocol methods here.
//  You can move these methods to another class if you prefer -- just be sure 
//  to send a -setDelegate: message to the table view if you do.

//  Returns the number of sections in the table view. The style of this
//  table view is 'plain' rather than 'grouped', so there's only one section.
//
- (NSInteger)numberOfSectionsInTableView:(UITableView *) tableView {
#if CUSTOMER_FOR_SEAGATE
    if ([[self displayedObjects] count] == 0)
        return 1;
    else
#endif
    return 2;
}

//  Returns the number of rows in the current section.
//
- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger) section {
    if (section == kMyServerSection && _isShowMyServer)
        return [[self displayedObjects] count];
    else if(section == kMyViewSection && _isShowMyView)
        return [[self displayedMyView] count];
    
    return 0;
}

//  Return YES to allow the user to reorder table view rows
//
- (BOOL)tableView:(UITableView *) tableView canMoveRowAtIndexPath:(NSIndexPath *) indexPath {
#if DEFAULT_DEMO_LINK // jerrylu, 2012/06/27
    if (indexPath.section == kMyServerSection) {
#if CUSTOMER_FOR_EZ
        if (indexPath.row == DEFAULT_DEMO_ROW) {
            return NO;
        }
#else
        if ([self checkFirstRun]) {
            if (indexPath.row == DEFAULT_DEMO_ROW) {
                return NO;
            }
        }
#endif // CUSTOMER_FOR_EZ
    }
#endif // DEFAULT_DEMO_LINK
    return YES;
}

- (NSIndexPath *) tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    if (sourceIndexPath.section != proposedDestinationIndexPath.section)
        return sourceIndexPath;
    
#if DEFAULT_DEMO_LINK // jerrylu, 2012/06/27
    if (sourceIndexPath.section == kMyServerSection) {
#if CUSTOMER_FOR_EZ
    if (proposedDestinationIndexPath.row == DEFAULT_DEMO_ROW) {
        return sourceIndexPath;
    }
#else
    if ([self checkFirstRun]) {
        if (proposedDestinationIndexPath.row == DEFAULT_DEMO_ROW)
            return sourceIndexPath;
    }
#endif // CUSTOMER_FOR_EZ
    }
#endif // DEFAULT_DEMO_LINK
    return proposedDestinationIndexPath;
}

//  Invoked when the user drags one of the table view's cells. Mirror the
//  change in the user interface by updating the array of displayed objects.
//
- (void) tableView:(UITableView *) tableView
moveRowAtIndexPath:(NSIndexPath *) sourceIndexPath
       toIndexPath:(NSIndexPath *) targetIndexPath {
    NSUInteger sourceSection = [sourceIndexPath section];
    NSUInteger targetSection = [targetIndexPath section];
    NSUInteger sourceIndex = [sourceIndexPath row];
    NSUInteger targetIndex = [targetIndexPath row];
    
    if (sourceSection != targetSection)
        return;
    
    if (sourceIndex != targetIndex) {
        if (sourceIndex < targetIndex) {
            for (int i = sourceIndex; i < targetIndex; i++) {
                if (sourceSection == kMyServerSection)
                    [[self displayedObjects] exchangeObjectAtIndex:i withObjectAtIndex:i+1];
                else
                    [[self displayedMyView] exchangeObjectAtIndex:i withObjectAtIndex:i+1];
            }
        }
        else {
            for (int i = sourceIndex; i > targetIndex; i--) {
                if (sourceSection == kMyServerSection)
                    [[self displayedObjects] exchangeObjectAtIndex:i withObjectAtIndex:i-1];
                else
                    [[self displayedMyView] exchangeObjectAtIndex:i withObjectAtIndex:i-1];
            }
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
#if DEFAULT_DEMO_LINK // jerrylu, 2012/06/27
#if CUSTOMER_FOR_EZ
    if (indexPath.row == DEFAULT_DEMO_ROW)
        return NO;
#endif // CUSTOMER_FOR_EZ
#endif // DEFAULT_DEMO_LINK
    return YES;
}

//  Update array of displayed objects by inserting/removing objects as necessary.
//
- (void)tableView:(UITableView *) tableView commitEditingStyle:(UITableViewCellEditingStyle) editingStyle 
 forRowAtIndexPath:(NSIndexPath *) indexPath {
    if (delIndexPath != nil)
        [delIndexPath release];
	delIndexPath = [indexPath copy];
    
    NSString *msg;
    if (indexPath.section == kMyServerSection)
        msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"Do you want to delete this server?", nil)];
    else
        msg = [[NSString alloc] initWithFormat:NSLocalizedString(@"Do you want to delete this view?", nil)];
        
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                                           message:msg
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
            [alert addAction:cancel];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"OK", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     if (delIndexPath.section == kMyServerSection) {
                                         NSIndexPath  *pIndexPath0;
                                         pIndexPath0 = [NSIndexPath indexPathForRow:delIndexPath.row inSection:delIndexPath.section];
                                         ServerInfo *sinfo = [self.displayedObjects objectAtIndex:[pIndexPath0 row]];
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/15, delete the row and unregister PN
                                         // Unregister Push Notification
                                         NSThread *unregister_thread = [[NSThread alloc] initWithTarget:self selector:@selector(unRegPushServer:) object:sinfo];
                                         [unregister_thread start];
                                         [unregister_thread release];
                                         
                                         LiveViewAppDelegate *appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
                                         [appDelegate removeEventListByServerInfo:sinfo];
                                         [appDelegate saveEventList];
#endif
                                         [self removeMyServerInMyView:sinfo];
                                         
                                         [[self displayedObjects] removeObjectAtIndex:[pIndexPath0 row]];
                                         [[self tableView] reloadData];
                                         /*
                                          //  Animate deletion
                                          NSArray * indexPaths = [NSArray arrayWithObject:pIndexPath0];
                                          NSLog(@"%d %d", [pIndexPath0 section], [pIndexPath0 row]);
                                          [[self tableView] deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                                          */
#if DEFAULT_DEMO_LINK
                                         if (pIndexPath0.row == DEFAULT_DEMO_ROW)
                                         {
                                             if ([self checkFirstRun]) {
                                                 [self setFirstRun:NO];
                                             }
                                         }
#endif // DEFAULT_DEMO_LINK
                                     }
                                     else {
                                         NSIndexPath  *pIndexPath0;
                                         pIndexPath0 = [NSIndexPath indexPathForRow:delIndexPath.row inSection:delIndexPath.section];
                                         [[self displayedMyView] removeObjectAtIndex:[pIndexPath0 row]];
                                         
                                         //  Animate deletion
                                         NSArray * indexPaths = [NSArray arrayWithObject:pIndexPath0];
                                         [[self tableView] deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                                     }
                                     [self save];
                                     
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                             message:msg
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                   otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
            [alert release];
        }
	}
    [msg release];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex {
#if FUNC_HELP_PAGE
    if ([alertView.title hasPrefix:NSLocalizedString(@"About", nil)]) {
        if (buttonIndex == 1) {
            HelpViewController *helpview = [[HelpViewController alloc] init];
            [self.navigationController pushViewController:helpview animated:NO];
            [helpview release];
        }
    }
    else {
#endif // FUNC_HELP_PAGE
    // Delete My Server or My View
	if (buttonIndex == 1) {
        if (delIndexPath.section == kMyServerSection) {
            NSIndexPath  *pIndexPath0;
            pIndexPath0 = [NSIndexPath indexPathForRow:delIndexPath.row inSection:delIndexPath.section];
            ServerInfo *sinfo = [self.displayedObjects objectAtIndex:[pIndexPath0 row]];
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/15, delete the row and unregister PN
            // Unregister Push Notification
            NSThread *unregister_thread = [[NSThread alloc] initWithTarget:self selector:@selector(unRegPushServer:) object:sinfo];
            [unregister_thread start];
            [unregister_thread release];
            
            LiveViewAppDelegate *appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
            [appDelegate removeEventListByServerInfo:sinfo];
            [appDelegate saveEventList];
#endif
            [self removeMyServerInMyView:sinfo];
                        
            [[self displayedObjects] removeObjectAtIndex:[pIndexPath0 row]];
            [[self tableView] reloadData];
            /*
            //  Animate deletion
            NSArray * indexPaths = [NSArray arrayWithObject:pIndexPath0];
            NSLog(@"%d %d", [pIndexPath0 section], [pIndexPath0 row]);
            [[self tableView] deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
             */
#if DEFAULT_DEMO_LINK
            if (pIndexPath0.row == DEFAULT_DEMO_ROW)
            {
                if ([self checkFirstRun]) {
                    [self setFirstRun:NO];
                }
            }
#endif // DEFAULT_DEMO_LINK
        }
        else {
            NSIndexPath  *pIndexPath0;
            pIndexPath0 = [NSIndexPath indexPathForRow:delIndexPath.row inSection:delIndexPath.section];
            [[self displayedMyView] removeObjectAtIndex:[pIndexPath0 row]];
            
            //  Animate deletion
            NSArray * indexPaths = [NSArray arrayWithObject:pIndexPath0];
            [[self tableView] deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        }
        [self save];
	}
#if FUNC_HELP_PAGE
    }
#endif // FUNC_HELP_PAGE
}

- (void)removeMyServerInMyView:(ServerInfo *) sinfo {
    for (MyViewInfo *myVInfo in self.displayedMyView) {
        for (int i = 0; i < [myVInfo.serverList count]; i++) {
            MyViewServerInfo *mySInfo = [myVInfo.serverList objectAtIndex:i];
            if ([mySInfo.serverID isEqualToString:sinfo.serverID] &&
                ((sinfo.connectionType == eIPConnection && [mySInfo.userName isEqualToString:sinfo.userName]) ||
                 (sinfo.connectionType == eP2PConnection && [mySInfo.userName isEqualToString:sinfo.userName_P2P]))
                ) {
                [myVInfo.serverList removeObject:mySInfo];
                [self resetMyViewCameraOrder:myVInfo];
                i--;
            }
        }
    }
    
    for (int i=0; i < [self.displayedMyView count]; i++)
    {
        MyViewInfo *vinfo = [self.displayedMyView objectAtIndex:i];
        if ([vinfo.serverList count] == 0)
        {
            [self.displayedMyView removeObject:vinfo];
            i--; // keep the index
        }
    }
}

- (void)resetMyViewCameraOrder:(MyViewInfo *) myVInfo {
    int c_count = 0;
    for (MyViewServerInfo *mySInfo in myVInfo.serverList)
        c_count += [mySInfo.cameraList count];
    
    for (int i = 0; i < c_count; i++) {
        BOOL isfoundorder = NO;
        MyViewCameraInfo *target_cinfo;
        int mini_order = -1;
        for (MyViewServerInfo *sinfo in myVInfo.serverList) {
            for (MyViewCameraInfo *cinfo in sinfo.cameraList) {
                if (cinfo.cameraOrder == i) {
                    isfoundorder = YES;
                    break;
                }
                else if (cinfo.cameraOrder > i && (mini_order == -1 || cinfo.cameraOrder < mini_order)) {
                    target_cinfo = cinfo;
                    mini_order = cinfo.cameraOrder;
                }
            }
        }
        if (isfoundorder)
            continue;
        else
            target_cinfo.cameraOrder = i;
    }
}

// Return a cell containing the text to display at the provided row index.
//
- (UITableViewCell *)tableView:(UITableView *) tableView
         cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell"];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:nil] autorelease]; // jerrylu, 2013/02/06, fix bug11990, reuseIdentifier set nil
#if DEFAULT_DEMO_LINK // jerrylu, 2012/06/27
#if CUSTOMER_FOR_EZ
        if (indexPath.section != kMyServerSection || indexPath.row != DEFAULT_DEMO_ROW)
#else
        if (![self checkFirstRun] || indexPath.section != kMyServerSection || indexPath.row != DEFAULT_DEMO_ROW)
#endif // CUSTOMER_FOR_EZ
#endif // DEFAULT_DEMO_LINK
        [cell setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];
        
#if !CUSTOMER_FOR_SEAGATE
        UIFont * titleFont = [UIFont fontWithName:@"Georgia-BoldItalic" size:18.0];
        [[cell textLabel] setFont:titleFont];
        
        UIFont * detailFont = [UIFont fontWithName:@"Georgia" size:16.0];
        [[cell detailTextLabel] setFont:detailFont];
#endif
    }
    
    NSUInteger index = [indexPath row];
    
    if (indexPath.section == kMyServerSection) {
        id info = [[self displayedObjects] objectAtIndex:index];

#if FUNC_P2P_SAT_SUPPORT
        if ([info connectionType] == eIPConnection) {
#endif
            NSString * title = [info serverName];
            [[cell textLabel] setText:(title == nil || [title length] < 1 ? [info serverIP] : title)];
    
            NSString * detailText = [NSString stringWithFormat:
                                     @"%@    %@",
                                     [info serverIP],
                                     [info serverPort]];
            [[cell detailTextLabel] setText:detailText];
#if FUNC_P2P_SAT_SUPPORT
        }
        else {
            NSString * title = [info serverName_P2P];
            [[cell textLabel] setText:(title == nil || [title length] < 1 ? [info serverID_P2P] : title)];
            
            NSString * detailText = [NSString stringWithFormat:
                                     @"%@",
                                     [info serverID_P2P]]; // jerrylu, 2012/09/10
            [[cell detailTextLabel] setText:detailText];
        }
#endif
    }
    else {
        id info = [[self displayedMyView] objectAtIndex:index];
        NSString * title = [info viewName];
        [[cell textLabel] setText:title];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *) tableView heightForHeaderInSection:(NSInteger) section
{
    return 35.0;
}

- (UIView *)tableView:(UITableView*) tableView viewForHeaderInSection:(NSInteger) section
{
    float hwidth, hheight = 35.0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        hwidth = [[UIScreen mainScreen] bounds].size.width;
    }
    else {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            hwidth = [[UIScreen mainScreen] bounds].size.width;
        else
            hwidth = [[UIScreen mainScreen] bounds].size.height;
    }
    
    UIView *customView = [[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, hwidth, hheight)] autorelease];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (section == kMyServerSection)
            customView.backgroundColor = COLOR_MYSERVERSECHEADER;
        else
            customView.backgroundColor = COLOR_MYVIEWSECHEADER;
    }
    else {
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = customView.bounds;
        gradient.colors = [NSArray arrayWithObjects:(id)[COLOR_IOS6SITESECTION1 CGColor], (id)[COLOR_IOS6SITESECTION2 CGColor], nil];
        [customView.layer insertSublayer:gradient atIndex:0];
    }
    
    /* make button one pixel less high than customView above, to account for separator line */
    UIButton *clearbutton = [[[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, hwidth, hheight)] autorelease];
    [clearbutton setBackgroundColor:[UIColor clearColor]];
    [clearbutton addTarget:self action:@selector(headerTapped:) forControlEvents: UIControlEventTouchUpInside];
    clearbutton.tag = section;
    
    UILabel *headerlabel = [[[UILabel alloc] initWithFrame:CGRectMake(40.0, 5.0, hwidth - 40.0, 25.0)] autorelease];
    UIFont* font = [UIFont systemFontOfSize:20.0];
	[headerlabel setFont:font];
	[headerlabel setTextColor:[UIColor whiteColor]];
	[headerlabel setBackgroundColor:[UIColor clearColor]];

    UIImage *image;
    if (section == kMyServerSection) {
        [headerlabel setText:NSLocalizedString(@"My Server", nil)];
        if (_isShowMyServer)
            image = [UIImage imageNamed:@"indicationDown.png"];
        else
            image = [UIImage imageNamed:@"indicationUp.png"];
    }else {
        [headerlabel setText:NSLocalizedString(@"My View", nil)];
        if (_isShowMyView)
            image = [UIImage imageNamed:@"indicationDown.png"];
        else
            image = [UIImage imageNamed:@"indicationUp.png"];
    }
    UIButton *imgbutton = [[[UIButton alloc] initWithFrame: CGRectMake(5.0, 5.0, image.size.width, image.size.height)] autorelease];
    [imgbutton setImage:image forState:UIControlStateNormal];
    imgbutton.tag = section;
    [imgbutton addTarget:self action:@selector(headerTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [customView addSubview:clearbutton];
	[customView addSubview:imgbutton];
    [customView addSubview:headerlabel];
    
    return customView;
}

- (void)headerTapped: (UIButton*) sender
{
    UIButton *btnSection = (UIButton *)sender;
    if (btnSection.tag == kMyServerSection)
        _isShowMyServer = !_isShowMyServer;
    else
        _isShowMyView = !_isShowMyView;
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:btnSection.tag] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark -
#pragma mark navigationController delegate protocol
- (void)navigationController:(UINavigationController *)navigationController
	   didShowViewController:(UIViewController *)viewController 
					animated:(BOOL)animated{

	if ([viewController isKindOfClass:[SiteListController class]] ) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	}
}

- (void)WorkAround: (NSMutableArray*) myViewInfos
{
    for (MyViewInfo* myViewInfo in myViewInfos)
    {
        void* sdk = NULL;

        for (MyViewServerInfo* myViewServerInfo in myViewInfo.serverList)
        {
            NSMutableArray * objects = [self displayedObjects];
            ServerInfo* findServerInfo = nil;

            for (ServerInfo* serverInfo in objects)
            {
                if ([myViewServerInfo.serverID isEqualToString:serverInfo.serverID] == FALSE)
                    continue;

                if (   [serverInfo serverType] != eServer_Crystal
                    && [serverInfo serverType] != eServer_Titan)
                {
                    continue;
                }

                findServerInfo = serverInfo;
                break;
            }

            if (findServerInfo == nil)
                continue;

            for (MyViewCameraInfo* myViewCameraInfo in myViewServerInfo.cameraList)
            {
                if (   [myViewCameraInfo.rsCentralID isEqualToString:@"0"] == FALSE
                    || [myViewCameraInfo.rsLocalID isEqualToString:@"0"] == FALSE)
                {
                    continue;
                }

                Np_Result_t ret = Np_Result_OK;
                Np_ServerList serverList;
                
                if (NULL == sdk)
                {
                    ret |= Create_Lite_Handle(&sdk,
                                              kCrystal,
                                              NSStringToStringW([findServerInfo userName]).c_str(),
                                              NSStringToStringW([findServerInfo userPassword]).c_str(),
                                              NSStringToStringW([findServerInfo serverIP]).c_str(),
                                              [[findServerInfo serverPort] intValue]);
                }

                if (NULL == sdk)
                    break;
            
                if (ret != 0)
                    break;
            
                ret |= Info_GetRecordingServerList(sdk, serverList);
                
                if (ret != 0)
                    break;
                
                for (int i=0;i<serverList.size;i++)
                {
                    Np_Server& item = serverList.items[i];
                    if (item.uiOrder != 0)
                        continue;
                    
                    myViewCameraInfo.rsCentralID = [NSString stringWithFormat: @"%llu", item.ID.centralID];
                    myViewCameraInfo.rsLocalID = [NSString stringWithFormat: @"%llu", item.ID.localID];
                    myViewCameraInfo.camCentralID = [NSString stringWithFormat: @"%llu", item.ID.centralID];

                    break;
                }
                
                Info_ReleaseRecordingServerList(sdk, serverList);
            }
        }

        if (NULL != sdk)
            Destroy_Handle(sdk);
    }
}

@end

