//
//  ControlHelper.h
//  objcKernal
//
//  Created by johnlinvc on 10/01/29.
//  Copyright 2010. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CameraHelper.h"
#import "myviewInfo.h"
#import "AudioController.h"

#include "NpClient.h"
#if FUNC_P2P_SAT_SUPPORT
#include "sat_sdk_lib_config.h"
#include "sat_sdk_lib_debug.h"
#include "sat_sdk_lib_client.h"
#include "sat_sdk_lib_license.h"
#endif

#if FUNC_P2P_SAT_SUPPORT
//#define CONTROLPORT	7381
#define MAXBUF		68
#define MAXBUFDATA	64
#define RECVTIMEOUT	2
#define MAXHELLO_RETRY	2
#define MAXACK_RETRY	10

typedef struct _CONTROL_MSG
{
	unsigned int type;
	char data[MAXBUFDATA];
}CONTROL_MSG;

typedef enum _MSG_TYPE
{
	TYPE_HELLO	= 1,
	TYPE_QUOTA,
	TYPE_ALIVE,
	TYPE_ACK,
	TYPE_DISCONNECT,
	TYPE_UPDATE,
}MSG_TYPE;
#endif

typedef enum E_PLAYBACK_EVENT {
    ePlayback_NoData = 0,
    ePlayback_ConnectError
} EPlaybackEvent;

@class CameraHelper;
@class ServerInfo;
@class Eventinfo;

@interface ControlHelper : NSObject <CameraHelperDelegate, AudioControllerHandleDelegate> {
	// for server info
	ServerInfo * serverInfo;
    Eventinfo * eventInfo;
    MyViewServerInfo * myViewServerInfo;
    
	// for Login
	NSString * userSessionNo;
	int iAllowChannels;
	int cameraCount;
	int IOStateIndex;
	NSMutableArray * ptzSupportArray;
	NSMutableArray * cameraNameArray;
	NSMutableArray * cameraIDArray;
	// for getIOinfo
	int IODeviceCount;
	NSMutableArray * IOInputDevIDArray;
	NSMutableArray * IOOutputDevIDArray;
	NSMutableArray * IOInputPinNameArray;
	NSMutableArray * IOOutputPinNameArray;
	NSMutableArray * IOInputPinCountArray;
	NSMutableArray * IOOutputPinCountArray;
	NSMutableArray * IODevNameArray;
	NSMutableArray * IOPinDOAccessArray;
	NSMutableArray * IOModuleIDArray;
	// for GetPreset
	NSMutableArray * presetNameArray;
	NSMutableArray * presetIdArray;
    
	id serverManagerDelegate;
	id presetDelegate;
    id loadingDelegate;
    // Brosso - TimeLine
    id timelineDelegate;
    
	NSMutableArray * cameraHelperArray;
	
	//uncheck
    BOOL isLogouting;
	BOOL isRunning;
    BOOL isServerDisconnected;
    BOOL isGetPresent; // jerrylu, 2012/06/19
    BOOL bAllCamSupportSecondProfile;
    BOOL playbackEnable; // jerrylu, 2012/05/24
    BOOL playbackAudioEnable;
    
    //NSThread*  pLoginThread; // jerrylu, 2012/08/29
    NSThread*  m_checkConnectionThread;
    NSThread*  m_checkPlaybackConnectionThread; // jerrylu, 2012/05/09

    NSCondition* m_Condition;

    //SDK
    void* m_hNuSDK;
    void* m_hNuSDKPlayback;
    void* m_hLVPlayer;
    void* m_hPBPlayer;
    void* m_hEventSession;
    Np_DeviceList              m_stDeviceList;
    Np_DeviceList              m_pbDeviceList;
    Np_ServerList              m_recordingServerList;
    Np_DeviceList_Ext          m_extDeviceList;
    std::vector<Np_Server>     m_vecServerList;
    std::vector<Np_SubDevice_Ext> m_vecExtCheckedDevice;
    std::vector<Np_Device>     m_vecCheckedDevice;
    std::vector<Np_Device_Ext> m_vecExtDIOMasterDevice;
    std::vector<Np_Device>     m_vecDIOMasterDevice;
    std::vector<BOOL>          m_vecCamSupportSecondProfile;
    std::vector<Np_SubDevice_Ext> m_vecExtDIDevice;
    std::vector<Np_SubDevice>     m_vecDIDevice;
    std::vector<BOOL>             m_vecDIStatus;
    std::vector<Np_SubDevice_Ext> m_vecExtDODevice;
    std::vector<Np_SubDevice>     m_vecDODevice;
    std::vector<BOOL>             m_vecDOStatus;
    Np_PTZPresetList           m_stPTZPresetList;
    Np_PTZPreset_CS_List       m_stExtPTZPresetList;
    Np_ID                      m_idPreset;
    Np_ID_Ext                  m_idExtPreset;
    Np_Event                   m_event;
    Np_Event_Ext               m_event_ext;
    Np_RecordLogListListExt    m_recordLogListList;
    Np_ScheduleLogListList     m_scheduleLogListList;
    
#if FUNC_P2P_SAT_SUPPORT
    // P2P
    ServerInfo* p2pServerInfo;
    IP2PTunnel* p_tunnel;
    NSThread* m_quotaControlThread;
    std::string nattype;
    unsigned short controlport;
    unsigned short webport;
    BOOL quota_control;
    unsigned short p2pErrorCode;
    int relayTimeout;
    BOOL isRelayMode;
    int serverMtuSize;
    NSDate *p2pStartTime;
    NSString *local_ip;
    int loginEznuuoFailedTime;
#endif
}

//check
@property (nonatomic, assign) ServerInfo * serverInfo;
@property (nonatomic, retain) Eventinfo * eventInfo;
@property (nonatomic, assign) MyViewServerInfo * myViewServerInfo;

// for GetPreset
@property (nonatomic, retain) NSMutableArray * presetNameArray;
@property (nonatomic, retain) NSMutableArray * presetIdArray;

@property (nonatomic, retain) NSMutableArray * cameraHelperArray;
//deletate
@property (nonatomic, assign) id serverManagerDelegate;
@property (nonatomic, assign) id presetDelegate;
@property (nonatomic, assign) id loadingDelegate;
//timeline
@property (nonatomic, assign) id timeLineDelegate;

@property (assign) int cameraCount; // jerrylu, 2012/08/16
@property (assign) BOOL bAllCamSupportSecondProfile;
@property (assign) BOOL isRunning;
@property (assign) BOOL isLogouting;
@property (assign) BOOL isServerDisconnected;
@property (assign) BOOL isGetPresent; // jerrylu, 2012/06/19
@property (assign) BOOL playbackEnable; // jerrylu, 2012/05/24

#if FUNC_P2P_SAT_SUPPORT
@property (assign) BOOL quota_control; // jerrylu, 2012/09/14
@property (assign) BOOL isRelayMode;
#endif

//SDK
@property (nonatomic) void*                      m_hNuSDK;
@property (nonatomic) void*                      m_hNuSDKPlayback; // jerrylu, 2012/05/08
@property (nonatomic) std::vector<Np_Server>     m_vecServerList;
@property (nonatomic) std::vector<Np_SubDevice_Ext> m_vecExtCheckedDevice;
@property (nonatomic) std::vector<Np_Device>     m_vecCheckedDevice;
@property (nonatomic) std::vector<Np_Device>     m_vecDIOMasterDevice;
@property (nonatomic) std::vector<BOOL>          m_vecCamSupportSecondProfile;

@property (nonatomic, assign) Np_Event m_event;
@property (nonatomic, assign) Np_Event_Ext m_event_ext;

- (void)login;
- (void)loginByEvent:(NSMutableArray *)serverArray;
- (void)loginMyView:(NSMutableArray *)serverArray;
- (BOOL)loginAndGetCameraList;
- (BOOL)loginAndGetRecordingServerList;
- (BOOL)updateCameraListFromRecordingServer:(Np_ID_Ext) serverID;
- (BOOL)updateDeviceByMyView;
- (NSString *)getCameraNameFromIDExt:(Np_ID_Ext) sensorID;
- (NSString *)getUserName;
- (void)create_FakeCameraList;
- (std::vector<Np_Device> *)getAllCameraList;
- (std::vector<Np_Device_Ext> *)getAllCameraListFromRecordingServer:(Np_ID_Ext) serverID;

- (void)setPTZOnCamera:(int) index withDirection:(NSString *) dir;
- (void)gotoPTZPreset:(int) index withIdx:(int) idx;
- (void)connectToCam:(int) position eLayout:(ELayoutType) eType;
- (void)connectToAllCam;
- (void)stopconnectToCam;
- (void)disconnectAllCam;
- (void)reloadAllDevices;
- (void)connectToCamWithHighResolution:(int) position;
- (BOOL)getLiveViewConnection:(int) index;
- (void)setProfileTypeWithCam:(int) position profiletype:(EProfileType) profiletype;
- (EProfileType)getProfileTypeWithCam:(int) position;
- (EProfileType)getConnectProfileTypeWithCam:(int) position; // jerrylu, 2012/09/06, fix bug8410
- (BOOL)isCamStreaming:(int) idx;
- (void)logout;

- (void)update_AudioAndPtzDeviceInfoOnCam:(int) index;

// PTZ
- (BOOL)getPTZCapOnCam:(int) index;
- (void)getPresetToCam:(int) index;

// DI/O
- (void)forceOutPut:(int) index state:(BOOL) state;
- (BOOL)queryDOState:(int) index;
- (BOOL)queryDIState:(int) index;
- (BOOL)getDOPrivilege:(int) index;
- (void)forceOutPutOnCam:(int) index doIndex:(int) do_index state:(BOOL) state;
- (BOOL)queryDOStateOnCam:(int) index doIndex:(int) do_index;
- (BOOL)queryDIStateOnCam:(int) index diIndex:(int) di_index;
- (BOOL)getDOPrivilegeOnCam:(int) index doIndex:(int) do_index;
- (NSMutableArray *)getDODeviceNameList;
- (NSMutableArray *)getDODeviceNameOnCam:(int) index;
- (NSMutableArray *)getDIDeviceNameList;
- (NSMutableArray *)getDIDeviceNameOnCam:(int) index;

// Audio
- (BOOL)getLiveViewAudioCapOnCam:(int) index;
- (BOOL)getLiveViewAudioStatusOnCam:(int) index;
- (void)setLiveviewAudioStateOnCam:(int) index state:(BOOL) state;
- (BOOL)getPlaybackAudioStatus;
- (void)setPlaybackAudioState:(BOOL) state;
- (BOOL)getTalkCapOnCam:(int) index;
- (BOOL)setTalkStateOnCam:(int) index state:(BOOL) state audioFormat:(Np_TalkAudioFormat *) audiofmt;
- (void)handelReservedTalk:(Np_Event)event;

// Playback
- (void)disconnectAllPlaybackConnection;
- (BOOL)playbackStartFromDate:(int) index startdate:(NSDate *) startdate enddate:(NSDate *) enddate;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (BOOL)playbackStartFromDate:(int) index recordfile:(EDualRecordFile) recordfile startdate:(NSDate *) startdate enddate:(NSDate *) enddate;
#endif
- (BOOL)playbackStartFromDateByEvent:(int) index startdate:(NSDate *) startdate enddate:(NSDate *) enddate;
- (BOOL)playbackOpenRecord:(Np_DateTime) starttime enddate:(Np_DateTime) endtime;
- (void)playbackStop:(int) index;
- (void)playbackStopByEvent:(int) index;
- (void)playbackPlay;
- (void)playbackReversePlay;
- (void)playbackPause;
- (void)playbackPreviousFrame:(int) index;
- (void)playbackNextFrame:(int) index;
- (void)playbackSeek:(Np_DateTime)seektime; // jerrylu, 2013/02/01, fix bug11855
- (float)playbackGetSpeed;
- (void)playbackSetSpeed:(float) speed;
- (Np_DateTime)playbackGetTime;
- (Np_PlayerState)playbackGetState;
- (BOOL)getPlaybackConnection:(int) index;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (BOOL)playbackChangeRecordFile:(int) index recordfile:(EDualRecordFile) recordfile startdate:(Np_DateTime)starttime enddate:(Np_DateTime)endtime seektime:(Np_DateTime)seektime;
- (BOOL)checkPlaybackLogTime:(int) ch_index recordfile:(EDualRecordFile) rf_index datetime:(Np_DateTime *) datetime;
#endif

#if FUNC_PUSH_NOTIFICATION_SUPPORT
// Push Notification
- (NSString *)getServerID;
- (BOOL)pnRegisterToServer;
- (BOOL)pnUnregisterToServer;
- (BOOL)pnUnregisterToServerWithoutLogin:(ServerInfo *) sinfo;
#endif

#if FUNC_P2P_SAT_SUPPORT
- (void)quotaReset;
#endif

#if SHOW_FRAMERATE_FLAG
- (double)getLiveViewFrameRate:(int) index;
#endif

// Brosso - time line
- (BOOL)checkPlaybackTimeByNSDate:(int) index startdate:(NSDate *) startdate enddate:(NSDate *) enddate;

@end


@protocol ControlHelperLoginDelegate
@optional
- (void)controlHelperConnectToServer:(ControlHelper *)helper cameraCount:(int) count;
- (void)controlHelperConnectToCrystalServer:(ControlHelper *)helper;
- (void)controlHelperCantConnectToServer:(ControlHelper *)helper;
- (void)controlHelperEventHandleFromServer:(ControlHelper *)helper event:(Np_Event)event;
- (void)controlHelperEventHandleFromServer:(ControlHelper *)helper event_ext:(Np_Event_Ext)event;
- (void)controlHelperDidRefreshImages:(ControlHelper *) helper camIndex:(int)index camImg:(UIImage *)img;
- (void)controlHelperDidRefreshImagesBuf:(ControlHelper *) helper camIndex:(int)index img:(NSData *)img imgWidth:(int)width imgHeight:(int)height;
- (void)controlHelperDidCameraSessionLost:(ControlHelper *) helper camIndex:(int)index;
- (void)controlHelperDidPlaybackDisconnection:(ControlHelper *) helper;
- (void)controlHelperDidUnsupportedFormat;
- (void)controlHelperQuotaTimeout:(ControlHelper *) helper; // jerrylu, 2012/09/14
- (void)controlHelperDidPbNextPreviosFrameSuccess:(ControlHelper *) helper;
- (void)controlHelperAlertPlaybackEvent:(ControlHelper *) helper camIndex:(int) index event:(EPlaybackEvent)event;
- (void)controlHelperRefreshPlaybackTime:(ControlHelper *) helper starttime:(Np_DateTime *)newStartTime endtime:(Np_DateTime *)newEndTime;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)controlHelperRefreshPlaybackTime:(ControlHelper *) helper recordfile:(EDualRecordFile)rec startTime:(Np_DateTime *)newStartTime endTime:(Np_DateTime *)newEndTime;
- (void)controlHelperSupportDualRecordPlayback:(ControlHelper *) helper camIndex:(int) index;
- (void)controlHelperChangePlaybackRecordFile:(ControlHelper *) helper camIndex:(int) index recordFile:(EDualRecordFile) recordfile;
#endif
- (void)controlHelperSave:(ControlHelper *) helper;
- (void)controlHelperStartEventViewByIndex:(ControlHelper *) helper index:(int) index;
- (void)controlHelperDidRefreshAudio:(ControlHelper *) helper camIndex:(int) index WithAudio:(AudioData) audiodata;
- (void)controlHelperDidTalkReserved:(ControlHelper *) helper srv:(NSString *)servername usr:(NSString *)username;
- (void)controlHelperDidTalkSessionError:(ControlHelper *) helper;
- (void)controlHelperShowP2PControlDialog:(ControlHelper *) helper msg:(NSString *)dialogMsg;
@end

@protocol ControlHelperPresetDelegate
@optional
- (void) ControlHelper:(ControlHelper *) helper didGetPresetOnCamera:(int) index;
@end

@protocol ControlHelperLoadingDelegate
@optional
- (void)controlHelperSetLoadingProgressState:(int) progressstate;
- (void)controlHelperP2PConnectionFailed:(int) errorcode;
@end
