//
//  PlaybackIntervalViewController.m
//  iViewer
//
//  Created by Jerry Lu on 12/4/24.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import "PlaybackRecordFileViewController.h"
#import "LiveViewAppDelegate.h"

@implementation PlaybackRecordFileViewController

@synthesize playbackView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        playback_recordfile = 0;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style withDefaultRecordFile:(int) defaultrecordfile;
{
    self = [super initWithStyle:style];
    if (self) {
        playback_recordfile = defaultrecordfile;
    }
    return self;
}

- (void)viewDidLoad
{
    if (IS_IPAD) {
        self.preferredContentSize = CGSizeMake(PopoverSizeWidth, 360);
    }
    
	// Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController setToolbarHidden:YES animated:NO];
    [self setTitle:NSLocalizedString(@"Record File", nil)];
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(popview) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(popview)];
    
    if (IS_IPAD)
        [backButton setTintColor:COLOR_NAVBUTTONTINTPAD];
    else
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
#endif
    self.navigationItem.leftBarButtonItem = backButton;
    [backButton release];
    
    [super viewDidLoad];
}

- (void)popview {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    playbackView = nil;
    [super dealloc];
}

#pragma mark - Table view data source
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *hearstr = NSLocalizedString(@"Select record file", nil);
    return hearstr;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RecordFileCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    NSInteger row = [indexPath row];
    cell.textLabel.text = [NSString stringWithFormat:@"Record %d", row+1];
    cell.accessoryType = (row == playback_recordfile) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = (NSInteger)[indexPath row];
    playback_recordfile = row;
    [playbackView setSelectedRecordfile:playback_recordfile];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
