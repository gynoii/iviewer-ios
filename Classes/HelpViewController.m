//
//  HelpViewController.m
//  iViewer
//
//  Created by NUUO on 12/12/6.
//
//

#import "HelpViewController.h"
#import "LiveViewAppDelegate.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:[self.view bounds]];
    webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    webView.contentMode = UIViewContentModeScaleAspectFit;
    webView.scalesPageToFit = YES;
#if CUSTOMER_FOR_SEAGATE
    NSString *path = [[NSBundle mainBundle] pathForResource:@"EULA_Multiple User_US_English_FINAL (2012)" ofType:@"pdf"];
#else
    NSString *path = [[NSBundle mainBundle] pathForResource:@"D-ViewCam Mobile_user_manual_V1_1 _iOS_20121206" ofType:@"pdf"];
#endif
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [webView loadRequest:request];
    [self.view addSubview:webView];
    [webView release];
    
    [self.navigationController setToolbarHidden:YES];
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(popview) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(popview)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
    [backButton release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)popview {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
