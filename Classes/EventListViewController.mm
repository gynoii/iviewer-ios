//
//  EventListViewController.m
//  iViewer
//
//  Created by NUUO on 12/8/9.
//
//

#import "EventListViewController.h"
#import "LiveViewAppDelegate.h"

#define FONT_SIZE 16.0

@implementation EventListViewController

@synthesize eventviewDelegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:NSLocalizedString(@"Events", nil)];
#if 0//def IPAD
    self.contentSizeForViewInPopover = CGSizeMake(480, 640);
#endif
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(backToEventView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil)
																	style:UIBarButtonItemStylePlain
																   target:self
																   action:@selector(backToEventView)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
	[backButton release];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    //adjust navigationBar, toolbar and status bar translucent
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    [self.navigationController.navigationBar setTranslucent:YES];
    [self.navigationController setToolbarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    [eventList release];
    eventList = nil;
    [super dealloc];
}

- (void)backToEventView {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)eventListUpdate {
    [[self tableView] reloadData];
    [[self tableView] setNeedsDisplay];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (eventList != nil) {
        [eventList release];
        eventList = nil;
    }
    
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
    eventList = [[appDelegate getEventListFromServer:@"" User:@""] retain];
    return [eventList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:@"MyCell"] autorelease];

        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
        Eventinfo *einfo = [eventList objectAtIndex:indexPath.row];
        
        cell.textLabel.text = einfo.eventMsg;
        if (einfo.isNotRead) {
            cell.textLabel.textColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
        }
        cell.textLabel.adjustsFontSizeToFitWidth = NO;
        //cell.textLabel.minimumFontSize = FONT_SIZE;
        cell.textLabel.lineBreakMode=NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines=0;
        cell.textLabel.font = [UIFont systemFontOfSize:FONT_SIZE];
        cell.textLabel.tag = 1;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)atableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Eventinfo *einfo = [eventList objectAtIndex:indexPath.row];
    
    UIFont *font = [UIFont systemFontOfSize:FONT_SIZE];
    
    CGSize size  = [einfo.eventMsg sizeWithFont:font constrainedToSize:CGSizeMake(self.view.bounds.size.width -40 , MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];

    return size.height+10;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.navigationController popViewControllerAnimated:NO];
    
    Eventinfo *einfo = [eventList objectAtIndex:indexPath.row];
    if (einfo.isNotRead) {
        LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
        [appDelegate setReadEvent:einfo];
        [appDelegate saveEventList];
    }
    
    if ([eventviewDelegate respondsToSelector:@selector(EventListContorlPlay:)]) {
        [eventviewDelegate EventListContorlPlay:einfo];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
