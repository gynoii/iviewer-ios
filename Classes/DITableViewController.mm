//
//  DITableViewController.m
//  LiveView
//
//  Created by johnlinvc on 10/05/06.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DITableViewController.h"
#import "LiveViewAppDelegate.h"
#import "SlideViewMenuController.h"
#import "SingleCamViewController.h"
#include "Utility.h"

@implementation DITableViewController

@synthesize parent;
@synthesize serverMgr;
@synthesize readDITimer;

#pragma mark -
#pragma mark View lifecycle
 
- (id)initWithDIDevice:(NSMutableArray *) pDeviceName; {
	if ((self = [super init])) {
		m_vecDIDeviceName = pDeviceName;
        for (int i=0; i < [m_vecDIDeviceName count]; i++) {
            m_vecDIStatus.push_back(FALSE);        
        }
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = YES;
	
    if (IS_IPAD) {
        self.preferredContentSize = CGSizeMake(PopoverSizeWidth, PopoverSizeHeight);
    }
    
    UIBarButtonItem * DOButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"DO", nil) style:UIBarButtonItemStylePlain target:self action:@selector(filpToDO)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (IS_IPAD)
            [DOButton setTintColor:COLOR_NAVBUTTONTINTPAD];
        else
            [DOButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        if (IS_IPAD)
            [DOButton setTintColor:COLOR_NAVBUTTONTINTPADIOS6];
        else
            [DOButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
    self.navigationItem.leftBarButtonItem = DOButton;
    [DOButton release];
    
    if ([parent isKindOfClass:[SingleCamViewController class]])
        singlecam = (SingleCamViewController *)parent;
    else
        singlecam = nil;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.tableView reloadData];
    
    readDITimer = [NSTimer scheduledTimerWithTimeInterval:3
                                                   target:self
                                                 selector:@selector(updateUI)
                                                 userInfo:nil
                                                  repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    for (int i=0; i < [m_vecDIDeviceName count]; i++) {
        if (singlecam != nil)
            m_vecDIStatus[i] = [serverMgr getDIState:i chIndex:[singlecam getCurrentChIndex]];
        else
            m_vecDIStatus[i] = [serverMgr getDIState:i];
    }    
    
    diThread = [[NSThread alloc] initWithTarget:self selector:@selector(readDI) object:nil];
    [diThread start];
    
    [self updateUI];
}

- (void)dismissPopover {
    if (IS_IPAD) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [parent dismissPopover];
    }
    return;
}

- (void)filpToDO {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)updateUI {
    for (int i=0; i < [m_vecDIDeviceName count]; i++) {
        BOOL bStatus = m_vecDIStatus[i];
        
        if (i < [lableArray count]) {
            if (bStatus) {
                ((UIImageView *)[lableArray objectAtIndex:i]).image = [UIImage imageNamed:@"lightred.png"];
            }
            else {
                ((UIImageView *)[lableArray objectAtIndex:i]).image = [UIImage imageNamed:@"lightblue.png"];
            }
        }
    }
}

- (void)readDI {
    while (TRUE) {
        [NSThread sleepForTimeInterval:1];
        for (int i=0; i < [m_vecDIDeviceName count]; i++) {
            if (singlecam != nil)
                m_vecDIStatus[i] = [serverMgr getDIState:i chIndex:[singlecam getCurrentChIndex]];
            else
                m_vecDIStatus[i] = [serverMgr getDIState:i];
            if ([[NSThread currentThread] isCancelled]) {
                [NSThread exit];
                return;
            }        
        }
        
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
        }
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [m_vecDIDeviceName count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        
		UIImageView * status = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lightblue.png"]] autorelease];
		if (m_vecDIStatus.size() > indexPath.row) {
			if (m_vecDIStatus[indexPath.row]) {
				status.image = [UIImage imageNamed:@"lightred.png"];
			}
		}
		if (lableArray == nil) {
			lableArray = [[NSMutableArray alloc] init];
		}
        if (indexPath.row < [lableArray count]) {
            [lableArray replaceObjectAtIndex:indexPath.row withObject:status];
        }
        else {
            [lableArray addObject:status];
        }
		cell.accessoryView = status;
    }
    
    // Configure the cell...
	cell.textLabel.text = [m_vecDIDeviceName objectAtIndex:indexPath.row];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewWillDisappear:(BOOL) animated {
    if ([diThread isExecuting]) {
        [diThread cancel];
        while (![diThread isFinished]) {
            [NSThread sleepForTimeInterval:0.5];
        }
    }
    [diThread release];
	
    [statArray release];
    statArray = nil;
	[lableArray release];
	lableArray = nil;
    [readDITimer invalidate];
    readDITimer = nil;
}

- (void)dealloc {
    [serverMgr release];
    serverMgr = nil;
	[super dealloc];
}


@end

