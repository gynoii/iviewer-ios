//
//  RecordingServerListController.h
//  iViewer
//
//  Created by NUUO on 13/10/7.
//
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "SiteListController.h"

@interface RecordingServerListController : UITableViewController<ServerManagerDisplayDelegate> {
    SiteListController * parent;
    ServerManager *serverMgr;
    
    std::vector<Np_Server> rserver_list;
}

@property (nonatomic, assign) ServerManager * serverMgr;
@property (nonatomic, assign) SiteListController * parent;

@end
