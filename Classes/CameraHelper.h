//
//  CameraHelper.h
//  LiveView
//
//  Created by LIN YU HSIANG on 10/05/21.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import	"serverInfo.h"
#include "NpClient.h"

typedef enum E_PROFILE_TYPE {
	eNone = -1,
	eMinimum = 0,
	eLow,
	eMedium,
    eOriginal, // jerrylu, 2012/05/22
} EProfileType;

#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
typedef enum E_DUAL_RECORD_FILE {
    eRecordFile1 = 0,
    eRecordFile2,
} EDualRecordFile;
#endif

struct AudioData {
    unsigned char *data;
    int len;
    int samplerate;
    int bitspersample;
    int channels;
};

@interface CameraHelper : NSObject {
	ServerInfo * serverInfo;
	id ctrlHelperDelegate;
	EProfileType eProfileType;
    EProfileType eMaxProfile; // jerrylu, 2012/10/16, fix bug9456
	int cameraIndex;
    
    //SDK
    void* m_lvPlayer;
    void* m_lvSession;
    Np_Device m_lvDevice;
    Np_SubDevice_Ext m_lvDeviceExt;
    BOOL  m_lvConnect;
    EProfileType connectProfileType;
    // jerrylu, 2012/05/07, playback port session
    void* m_pbPlayer;
    void* m_pbSession;
    Np_Device m_pbDevice;
    Np_SubDevice_Ext m_pbDeviceExt;
    BOOL  m_pbConnect;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    EDualRecordFile eRecordFile;
#endif
    BOOL isPbConnectSuccess;
    
    BOOL bLiveviewAudio; // jerrylu, 2012/09/06
    BOOL bIsDetachLiveView; // jerrylu, 2013/05/15
    
#if SHOW_FRAMERATE_FLAG
    BOOL isReceiveFirstFrame;
    NSDate *default_date;
    int frame_count;
    double frame_rate;
#endif
}

@property (nonatomic, assign) int cameraIndex;
@property (nonatomic, assign) ServerInfo * serverInfo;
@property (nonatomic, assign) id ctrlHelperDelegate;
@property (nonatomic, assign) EProfileType eProfileType;
@property (nonatomic, assign) EProfileType eMaxProfile; // jerrylu, 2012/10/16, fix bug9456
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
@property (nonatomic, assign) EDualRecordFile eRecordFile;
#endif

- (void)initValue;
- (Np_Result_t)checkLiveViewConnection;
- (Np_Result_t)liveViewStopConnection;
- (void)liveViewConnect;
- (void)liveViewDisconnect;
- (BOOL)isLiveViewConnectionCreated;
- (void)setLvPlayer:(void*) lvPlayer;

- (Np_Result_t)checkPlaybackConnection;
- (Np_Result_t)playbackStopConnection;
- (void)playbackConnect;
- (void)playbackDisconnect;
- (BOOL)isPlaybackConnectionCreated;
- (void)setPbPlayer:(void*) pbPlayer;

- (void)playbackGetCurrentFrame:(Np_Frame &)frame;
- (BOOL)getLiveViewAudioStatus;
- (Np_Result_t)setLiveviewAudioState:(BOOL) state;
- (EProfileType)getConnectProfile; // jerrylu, 2012/09/06, fix bug8410 
- (void)setNpDevice:(Np_Device) device;
- (Np_Device)getNpDevice;
- (void)setNpDeviceExt:(Np_SubDevice_Ext) device;
- (Np_SubDevice_Ext)getNpDeviceExt;
- (void)setPbNpDevice:(Np_Device) device;
- (Np_Device)getPbNpDevice;
- (void)setPbNpDeviceExt:(Np_SubDevice_Ext) device;
- (Np_SubDevice_Ext)getPbNpDeviceExt;
- (void)setPbOpenRecordSuccess;
- (BOOL)isPbOpenRecordSuccess;

#if SHOW_FRAMERATE_FLAG
- (double)getFrameRate;
#endif

@end

@protocol CameraHelperDelegate
@optional
- (void)cameraHelperDidRefreshImage:(int) index WithUIImage:(UIImage *) img;
- (void)cameraHelperDidRefreshImage:(int) index WithRaw:(NSData *)data Length:(int)len Width:(int)width Hieght:(int)height;
- (void)cameraHelperDidRefreshAudio:(int) index WithAudio:(AudioData) audiodata;
- (void)cameraHelperErrorHandle:(int) index error:(Np_Error)error;
- (void)cameraHelperPlaybackFeedbackHandle:(int) index;
@end


