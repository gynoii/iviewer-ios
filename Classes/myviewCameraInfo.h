//
//  myviewCameraInfo.h
//  TabView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright 2010 com.debug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyViewCameraInfo : NSObject {
    NSString * rsCentralID;
    NSString * rsLocalID;
    NSString * camCentralID;
    NSString * camLocalID;
    int cameraOrder;
    NSString * cameraName;
}
@property (nonatomic, retain) NSString * rsCentralID;
@property (nonatomic, retain) NSString * rsLocalID;
@property (nonatomic, retain) NSString * camCentralID;
@property (nonatomic, retain) NSString * camLocalID;
@property (nonatomic, assign) int cameraOrder;
@property (nonatomic, retain) NSString * cameraName;

- (id)initWithDictionary:(NSDictionary *) dictionary;
- (NSArray *)getKeys;
- (id)getPropertyDic;

@end
