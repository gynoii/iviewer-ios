//
//  LogoPage.h
//  iViewer
//
//  Created by Toby Huang on 12/7/18.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogoPage : UIViewController {
    UIImageView *_imageview;
}

@property (nonatomic, retain) IBOutlet UIImageView *imageview;

@end
