//
//  SlideViewMenuController.h
//  iViewer
//
//  Created by Jerry Lu on 12/8/1.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"

typedef enum E_SLIDEVIEW_FUNCTION_TYPE {
	eSlideViewFunctionNone = -1,
    eSlideViewFunctionDIDO,
	eSlideViewFunctionPushNotification,
    eSlideViewFunctionMax
} eSlideViewFunctionType;

@class SlideView;

@interface SlideViewMenuController : UITableViewController {
    ServerManager *serverMgr;
    id parent;
    UISwitch *pnServerSwitch;
    BOOL isShowDIO;
    
    UIActivityIndicatorView *spinner;
    UIView *overlayView;
}

@property (nonatomic, assign) ServerManager *serverMgr;

- (id)initWithStyle:(UITableViewStyle)style parentview:(id)parentview;

@end
