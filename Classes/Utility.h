//
//  Utility.h
//  LiveView
//
//  Created by NUUO on 6/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#include <string>
#include <vector>
#include "NpClient.h" // jerrylu, 2012/05/09

#ifndef BYTE
typedef unsigned char BYTE;
#endif // BYTE

#ifndef WORD
typedef unsigned short WORD;
#endif // WORD

#if 0 // jerrylu, 2012/04/18, redefine
#ifndef DWORD
typedef unsigned long DWORD;
#endif // DWORD
#endif

std::wstring NSStringToStringW ( NSString* Str );

NSString* StringWToNSString (const  std::wstring& Str );

NSString* WChartToNSString ( const wchar_t* Str );

NSString* documentsPath(NSString * fileName);

void saveImgData(void* pData, int size, int ch, int index);

void YUV420ToRGB(BYTE* lpSrc, int width, int height, BYTE* lpDest, int lPitch, int BitCount, bool bFlip);

UIImage* imageFromAVPicture(void* pData, int width, int height);
UIImage* imageFromYUVBuffer(void* pData, int width, int height);
UIImage* imageScaledToSize(UIImage *image, CGSize newSize);

void NSDate2NpDateTime(NSDate *pDate, Np_DateTime *pNpDate); // jerrylu, 2012/05/09
long NpDateTime2TimeInterval1970(Np_DateTime *pNpDate);

UIImage *convertImageToGrayScale(UIImage *image); // jerrylu, 2012/05/31

@interface  EonilImage : UIImage
{
@private
    CGImageRef      sourceImage;
}
- (id)initWithCGImage:(CGImageRef)imageRef;

bool isValidIpAddress(char *ipAddress);
const char * convertIpAddress(char *ipAddress);

@end