//
//  QueryRecordViewController.m
//  InfiniteScrollView
//
//  Created by BingHuan Wu on 12/12/16.
//  Copyright © 2016 Gynoii. All rights reserved.
//

#import "QueryRecordViewController.h"

@interface QueryRecordViewController ()

@end

@implementation QueryRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Dictionary to store events
    eventDictionary = [[NSMutableDictionary alloc] init];
    eventViews = [[NSMutableArray alloc] init];
    
    // Landscape / Portrait offset handle
    CGRect frame = self.view.frame;
    deltaOffset = fabs((frame.size.height - frame.size.width)) / 2;

    // Set scroll view property
    _scrollView.contentSize = CGSizeMake(GRAPH_CONTENT_SIZE, 0);
    
    // Init param
    scrollMode = ManualScrollMode;
    canQuery = YES;
    
    // Set dateTime label
    currentDateTime = [[NSDate date] retain];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd  HH:mm:00"];
    [_dateTime setText:[df stringFromDate:currentDateTime]];
    [df release];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set initial scroll point
    lastContentOffset = [self viewOffsetByDate:[NSDate date]];
    [_scrollView setContentOffset:CGPointMake(lastContentOffset, 0)];

}

- (void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (currentDateTime) {
        [currentDateTime release];
    }
    
    if (eventViews) {
        [eventViews removeAllObjects];
        [eventViews release];
    }
    
    if (eventDictionary) {
        [eventDictionary removeAllObjects];
        [eventDictionary release];
    }
    
    singleViewDelegate = nil;
    
    [super dealloc];
}

- (void)setSingleViewDeledate:(id)delegate {
    if (nil != delegate) {
        singleViewDelegate = delegate;
    }
}

#pragma mark - UI update related
- (void)enableAutoScrollView {
    scrollMode = AutoScrollMode;
}

- (void)canQueryNewDate:(BOOL)flag {
    canQuery = flag;
}

- (CGFloat)minimumScrollOffset:(NSDate*)date {
    CGFloat offset = [self viewOffsetByDate:date];
    
    if (lastContentOffset > offset + GRAPH_DAY / 2) {
        return offset + GRAPH_DAY;
    }
    else if (lastContentOffset < offset - GRAPH_DAY / 2) {
        return offset - GRAPH_DAY;
    }
    else {
        return offset;
    }
}

- (void)syncScrollViewWithDate:(NSDate*)date {
    if (ManualScrollMode == scrollMode || !date) { // Once interrupted then disable auto sync
        return;
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd  HH:mm:ss"];
    NSString *newDateTime = [df stringFromDate:date];

    dispatch_async(dispatch_get_main_queue(), ^{
        // Use minimum valuse to avoid sudden jump
        [_scrollView setContentOffset:CGPointMake([self minimumScrollOffset:date], 0)];
        [_dateTime setText:newDateTime];
    });
    
    [df release];
}

- (CGFloat)viewOffsetByDate:(NSDate*)date { // Return scroll offset for given time (date ignored)
    
    // (1 hour = 94 pixel = 60min, 1 pixel = 0.63829787min ~=38.3sec, 1min ~=1.5 pixel)
    CGFloat offset = 0.0;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH"];
    int hour = [[df stringFromDate:date] intValue];
    offset = [self getViewOffsetByHour:hour];
    
    [df setDateFormat:@"mm"];
    int minute = [[df stringFromDate:date] intValue];
    offset += 1.5 * minute;
    
    [df release];

     return offset;
}

- (CGFloat)eventOffsetByDate:(NSDate*)date { // Return x pos for event view
    
    // (1 hour = 94 pixel = 60min, 1 pixel = 0.63829787min ~=38.3sec, 1min ~=1.5 pixel)
    CGFloat offset = 0.0;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];

    NSDate *displayDate = [self getCurrentDisplayDate];
    int displaydayDiff = [self daysBetween:displayDate and:date];
    
    [df setDateFormat:@"HH"];
    int hour = [[df stringFromDate:date] intValue];
    offset = [self getViewOffsetByHour:hour];
    
    // Hour
    if (lastContentOffset < GRAPH_OFFSET_MIDNIGHT && hour < 8) { // Looking left -> move AM to left
        offset -= GRAPH_DAY; // 94 * 24 hours 
    }
    else if (lastContentOffset >= GRAPH_OFFSET_MIDNIGHT && hour >= 8) { // Looking right -> move PM to right
        offset += GRAPH_DAY; // 94 * 24 hours
    }
    
    // Minute
    [df setDateFormat:@"mm"];
    int minute = [[df stringFromDate:date] intValue];
    offset += 1.567 * minute;
    
    // Day
    offset += (displaydayDiff) * GRAPH_DAY;
    
    [df release];
    
    return offset + 2 * GRAPH_GAP; // 2 * self.gap for actual view difference
}

- (CGFloat)getViewOffsetByHour:(int)hour {
    
    // Landscape pixel offset
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    int delta = (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)? deltaOffset : 0;
    
    CGFloat offset = 0.0;
    
    if (hour >= 8) { // After 8:00 (use left side)
        offset = GRAPH_OFFSET_ORIGIN + (hour - 7) * GRAPH_GAP;
    }
    else { // 0:00 ~ 7:00 (use right side)
        offset = GRAPH_OFFSET_MIDNIGHT + hour * GRAPH_GAP;
    }
    return offset - delta;
}

- (NSDate*)getCurrentDisplayDate {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd  HH:mm:ss"];
    NSDate *displayDate = [df dateFromString:_dateTime.text];
    [df release];
    
    return displayDate;
}

- (void)updateDateTimeLabel:(CGFloat)offsetX withDate:(NSString*)newDate {
    
    // Landscape pixel offset
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    int delta = (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)? deltaOffset : 0;

    NSString *date = (nil != newDate)? newDate: [[_dateTime.text componentsSeparatedByString:@" "] firstObject];
    int hour = 0;
    int minute = 0;
    int base = (offsetX >= GRAPH_OFFSET_MIDNIGHT - delta)? GRAPH_OFFSET_MIDNIGHT - delta : 121 - delta; // 0:00(mid) or 8:00(left)
    int paddingHour = (offsetX >= GRAPH_OFFSET_MIDNIGHT - delta)? 0:8; // 08:00(L) at 121, 00:00 at 1625
    
    // (1 hour = 94 pixel = 60min, 1 pixel = 0.63829787min ~=38.3sec, 1min ~=1.56 pixel)
    hour = (offsetX - base) / GRAPH_GAP;
    minute = (offsetX - base - hour * GRAPH_GAP) / 1.56;
    if (minute >= 59) minute = 59; // This is crap... xd
    
    NSString *newDateTime = [NSString stringWithFormat:@"%@ %02d:%02d:00",date, paddingHour+hour, minute];
    [_dateTime setText:newDateTime];
}

- (void)renderEvents {
    
    // Hide spinner
    dispatch_async(dispatch_get_main_queue(), ^{
        [_spinner setHidden:YES];
    });
    
    // Render all days
    dispatch_async(dispatch_get_main_queue(), ^{
        for (NSArray *eventAry in [eventDictionary allValues]) {
            if (eventAry && ((QueryRecordEvents *)[eventAry firstObject]).eventView.superview) {
                continue;
            }
            for (QueryRecordEvents *event in eventAry) {
                if (nil == [event.eventView superview]) {
                    [_queryContentView addSubview:event.eventView];
                    [eventViews addObject:event.eventView];
                }
            }
        }
    });
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    [UIView animateWithDuration:0.2f animations:^{
        if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
            [_scrollView setContentOffset:CGPointMake([_scrollView contentOffset].x - deltaOffset, 0)];
        }
        else {
            [_scrollView setContentOffset:CGPointMake([_scrollView contentOffset].x + deltaOffset, 0)];
        }
    } completion:nil];
}

#pragma mark - Query logs
- (void)queryNewDate:(NSDate*)date {
    
    int gap = [self daysBetween:currentDateTime and:date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    NSString *dayKey = [df stringFromDate:date];
    [df release];
    
    if (nil != singleViewDelegate && canQuery && gap <= 0) { // Today or past
        if (gap < 0) { // Past, check previous record
            NSArray *eventArray = [eventDictionary objectForKey:dayKey];
            if (nil != eventArray) { // Already queried
                return;
            }
        }
        
        // Show spinner
        dispatch_async(dispatch_get_main_queue(), ^{
            [_spinner setHidden:NO];
        });
        
        canQuery = NO;
        [singleViewDelegate queryRecordByDate:date];
    }
}

#pragma mark - ScrollView delagate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView { // Manual interaction quarantined
    scrollMode = ManualScrollMode;
    if (nil != singleViewDelegate) {
        [singleViewDelegate pbControlPause];
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    NSDate *displayDate = [self getCurrentDisplayDate];
    NSArray *displayDateEvents = [eventDictionary objectForKey:[df stringFromDate:displayDate]];
    
    if (nil == displayDateEvents) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self queryNewDate:displayDate];
        });
    }
    
    [df release];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSDate *displayDate = [self getCurrentDisplayDate];
    if ([self checkEventExists:displayDate] && nil != singleViewDelegate) {
        [singleViewDelegate pbControlSeekByDate:displayDate];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView { // Manual interaction quarantined
    NSDate *displayDate = [self getCurrentDisplayDate];
    if ([self checkEventExists:displayDate] && [singleViewDelegate respondsToSelector:@selector(pbControlSeekByDate:)]) {
        [singleViewDelegate pbControlSeekByDate:displayDate];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // Get scroll direction
    if (lastContentOffset > scrollView.contentOffset.x) {
        scrollDirection = ScrollDirectionDecrease; // 0
    }
    else {
        scrollDirection = ScrollDirectionIncrease; // 1
    }
    
    // Infinite scroll loop
    if (scrollView.contentOffset.x >= 2941) { // Jump from 14:00(R) to 14:00(L)
        jumpFromRight = YES;
        [_scrollView setContentOffset:CGPointMake(685, 0)];
    }
    if (scrollView.contentOffset.x <= 121) { // Jump from 8:00(L) to 8:00(R)
        jumpFromLeft = YES;
        [_scrollView setContentOffset:CGPointMake(2377, 0)];
    }
    
    // Check for boundary cross
    NSString *newDate = nil;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    NSDate *today = [df dateFromString:[[_dateTime.text componentsSeparatedByString:@" "] firstObject]];
    NSDate *yesterday = [today dateByAddingTimeInterval: -SECS_PER_DAY];
    NSDate *tomorrow = [today dateByAddingTimeInterval: SECS_PER_DAY];
    NSString *yesterdayKey = [df stringFromDate:yesterday];
    NSString *tomorrowKey = [df stringFromDate:tomorrow];
    NSArray *yesterdayEvents = [eventDictionary objectForKey:yesterdayKey];
    NSArray *tomorrowEvents = [eventDictionary objectForKey:tomorrowKey];
    
    BOOL ifCrossed = ((lastContentOffset >= GRAPH_OFFSET_MIDNIGHT) && (scrollView.contentOffset.x < GRAPH_OFFSET_MIDNIGHT)) || ((lastContentOffset <= GRAPH_OFFSET_MIDNIGHT) && (scrollView.contentOffset.x > GRAPH_OFFSET_MIDNIGHT));
    
    if (ifCrossed) { // Crossed midnight
        
        if (jumpFromRight) { // Shift events, jump from right (14:00)
            // Shift all event views
            for (UIView *views in eventViews) {
                views.frame = CGRectOffset( views.frame, -GRAPH_DAY, 0 ); // Left shift 24 hours
            }

            // Query next day in background
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [self queryNewDate:tomorrow];
            });
            
            // Subviews handle
            dispatch_async(dispatch_get_main_queue(), ^{
                // Hide view from yesterday
                for (QueryRecordEvents *events in yesterdayEvents) {
                    [events.eventView setHidden:YES];
                }
                // Show view from tomorrow
                for (QueryRecordEvents *events in tomorrowEvents) {
                    [events.eventView setHidden:NO];
                }
            });
        }
        else if (jumpFromLeft) { // Shift events, jump from left (08:00)
            // Shift all event views
            for (UIView *views in eventViews) {
                views.frame = CGRectOffset( views.frame, GRAPH_DAY, 0 ); // Right shift 24 hours
            }
            
            // Query prev day in background
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [self queryNewDate:yesterday];
            });
            
            // Subviews handle
            dispatch_async(dispatch_get_main_queue(), ^{
                // Show view from yesterday
                for (QueryRecordEvents *events in yesterdayEvents) {
                    [events.eventView setHidden:NO];
                }
                // Hide view from tomorrow
                for (QueryRecordEvents *events in tomorrowEvents) {
                    [events.eventView setHidden:YES];
                }
            });
        }
        else { // Load new events, crossed midnight (00:00)
            if (ScrollDirectionDecrease == scrollDirection) { // Yesterday
                newDate = [df stringFromDate:yesterday];
            }
            else { // Tomorrow
                newDate = [df stringFromDate:tomorrow];
            }
        }
        jumpFromRight = jumpFromLeft = NO;
    }
    
    [df release];
    
    // Query handle and UI refresh
    [self updateDateTimeLabel:scrollView.contentOffset.x withDate:newDate];
    
    // Set lastContentOffset
    lastContentOffset = scrollView.contentOffset.x;
    
}

#pragma mark - Events related
- (BOOL)checkEventExists:(NSDate*)givenDate {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    NSString *dayKey = [df stringFromDate:givenDate];
    [df release];
    
    for (QueryRecordEvents *event in [eventDictionary objectForKey:dayKey]) {
        if ([self givenDate:givenDate isBetweenDate:event.startDate andDate:event.endDate]) {
            return YES;
        }
    }
    return NO;
}

- (void)fetchEventsOnDate:(NSString*)dateString withEvents:(NSArray*)events { // Fill event array
    
    // Generate view first
    for (QueryRecordEvents *event in events) {
        [self generateEventViewFromEvent:event];
    }
    
    // Update eventDictionary
    NSMutableArray *eventArray = [eventDictionary objectForKey:dateString];
    if (nil == eventArray) { // New entry
        eventArray = [NSMutableArray array];
    }
    else {
        // Merge with existed data (should be today)
        for (QueryRecordEvents *event in eventArray) {
            [event.eventView removeFromSuperview];
            [eventViews removeObject:event.eventView];
        }
        [eventArray removeAllObjects];
    }
    
    [eventArray addObjectsFromArray:events];
    [eventDictionary setObject:eventArray forKey:dateString];
}

- (void)generateEventViewFromEvent:(QueryRecordEvents*)event { // Generate view for events
    
//    static BOOL colorFlag = NO;
    
    UIView *eventView = [[UIView alloc] initWithFrame:CGRectMake([self eventOffsetByDate:event.startDate], EVENT_Y_POS, event.duration, EVENT_HEIGHT)];
//    [eventView setBackgroundColor:colorFlag?EVENT_COLOR_RED:EVENT_COLOR_BLUE];
    [eventView setBackgroundColor:EVENT_COLOR_BLUE];
    event.eventView = eventView;
    [eventView release];
    
//    colorFlag = !colorFlag;
}

#pragma mark NSDate helper
- (NSDate *)beginningOfDay:(NSDate *)date {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    return [cal dateFromComponents:components];
}

- (int)daysBetween:(NSDate *)dt1 and:(NSDate *)dt2 {
    NSUInteger unitFlags = NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:[self beginningOfDay:dt1] toDate:[self beginningOfDay:dt2] options:0];
    return (int)[components day];
}

- (BOOL)givenDate:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate {
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}

@end

#pragma mark - Event Class
@implementation QueryRecordEvents

+ (instancetype)eventWithStartDate:(NSDate*)startDate andEndDate:(NSDate*)endDate {
    if (!startDate || !endDate) {
        return nil;
    }
    
    NSTimeInterval duration = [endDate timeIntervalSinceDate:startDate];
    if (duration <= 0) {
        return nil;
    }
    
    QueryRecordEvents *newEvent = [[super alloc] init];
    if (newEvent) {
        // (1 hour = 94 pixel = 60min, 1 pixel = 0.63829787min ~=38.3sec, 1min ~=1.5 pixel)
        newEvent.duration = duration / 38.3;
        newEvent.startDate = startDate;
        newEvent.endDate = endDate;
    }
    return [newEvent autorelease];
}

- (void)dealloc {
    [super dealloc];
}

@end


