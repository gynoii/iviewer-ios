//
//  LoadingPage.m
//  LiveView
//
//  Created by johnlinvc on 10/03/16.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "LoadingPage.h"
#import "LiveViewAppDelegate.h"

@implementation LoadingPage

@synthesize parent;
@synthesize selectedIndex;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    if ([parent.m_ServerMgr isConnectionWithP2PServer] == NO) {
        [progresslabel setHidden:YES];
        [progressbar setHidden:YES];
    }
    else {
        if ([indicator isAnimating])
            [indicator stopAnimating];
        
        [progressbar setProgress:0.0];
        
        m_progressstate = eProgressNone;// eProgressStart;
        
        [NSTimer scheduledTimerWithTimeInterval:0.2f target:self selector:@selector(goProgress:) userInfo:nil repeats:YES];
    }
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(backFromLoading) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil)
																	style:UIBarButtonItemStylePlain 
																   target:self 
																   action:@selector(backFromLoading)];
	if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
    [backButton release];
    
    [self.view setBackgroundColor:COLOR_BACKGROUND];
    
	
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 2.0f);
    progressbar.transform = transform;
}

- (void)backFromLoading {
    [parent.m_ServerMgr abortLoginAllServer];
    [parent setServerManager:nil];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        progressbar.transform = transform;
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    progressbar.transform = transform;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 2.0f);
    progressbar.transform = transform;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)dealloc {
    [indicator release];
    indicator = nil;
    [progressbar release];
    progressbar = nil;
    [progresslabel release];
    progresslabel = nil;
    [selectedIndex release];
    selectedIndex = nil;
    
    [super dealloc];
}

- (void)goProgress:(NSTimer *)theTimer {
    float progress = progressbar.progress;
    progress = progress * 10 + 1;
    
    float progressmax = 0.0;
    switch (m_progressstate) {
        case eProgressStart:
            progressmax = 3.5;
            break;
        case eProgressAuthenticate:
            progressmax = 5.5;
            break;
        case eProgressConnect:
            progressmax = 6.5;
            break;            
        case eProgressConnectTunnel:
            progressmax = 10.0;
            break;
        case eProgressConnectToServer:
            progressmax = 10.0;
            break;
        case eProgressSuccess:
            progressmax = 10.0;
        default:
            break;
    }

    if (progress > progressmax)
        return;
    
    [progressbar setProgress:progress * 0.1];
    
    if (progress >= 10) {
        // stop NSTimer
        [theTimer invalidate];
    }
}

- (void)changeProgressLabel:(NSString *)state {
    [progresslabel setText:state];
}

- (void)resetProgressBar {
    [progressbar setProgress:0.0];
    [NSTimer scheduledTimerWithTimeInterval:0.2f target:self selector:@selector(goProgress:) userInfo:nil repeats:YES];
}

#pragma mark -
#pragma mark ControlHelperLoadingDelegate methods
- (void)controlHelperSetLoadingProgressState:(int) progressstate {
    switch (progressstate) {
        case eProgressStart:
            m_progressstate = eProgressStart;
            [self performSelectorOnMainThread:@selector(changeProgressLabel:) withObject:NSLocalizedString(@"Start", nil) waitUntilDone:YES];
            [self performSelectorOnMainThread:@selector(resetProgressBar) withObject:nil waitUntilDone:YES];
            break;
        case eProgressAuthenticate:
            m_progressstate = eProgressAuthenticate;
            [self performSelectorOnMainThread:@selector(changeProgressLabel:) withObject:NSLocalizedString(@"Authenticate", nil) waitUntilDone:YES];
            break;
        case eProgressConnect:
            m_progressstate = eProgressConnect;
            [self performSelectorOnMainThread:@selector(changeProgressLabel:) withObject:NSLocalizedString(@"Connect", nil) waitUntilDone:YES];
            break;
        case eProgressConnectTunnel:
            m_progressstate = eProgressConnectTunnel;
            [self performSelectorOnMainThread:@selector(changeProgressLabel:) withObject:NSLocalizedString(@"ConnectTunnel", nil) waitUntilDone:YES];
            break;
        case eProgressConnectToServer:
            m_progressstate = eProgressConnectToServer;
            [self performSelectorOnMainThread:@selector(changeProgressLabel:) withObject:NSLocalizedString(@"Connect to Server", nil) waitUntilDone:YES];
            break;
        case eProgressSuccess:
            m_progressstate = eProgressSuccess;
            [self performSelectorOnMainThread:@selector(changeProgressLabel:) withObject:NSLocalizedString(@"Finish", nil) waitUntilDone:YES];
            break;
        default:
            break;
    }
}

- (void)controlHelperP2PConnectionFailed:(int) errorcode {
    switch (errorcode) {
        case eP2pConnectFailed:
        {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"This server can't be connected.", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [self backFromLoading];
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                         }];
                UIAlertAction* retry = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"Retry", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            [self resetProgressBar];
                                            m_progressstate = eProgressStart;
                                            [[parent m_ServerMgr] loginAllServer];
                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                        }];
                [alert addAction:cancel];
                [alert addAction:retry];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"This server can't be connected.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Retry", nil), nil];
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
                [alert release];
            }
            break;
        }
        case eP2pConnectIdError:
        {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"This Server ID isn't available", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"OK", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self backFromLoading];
                                         [parent showSiteInfoDetail:selectedIndex];
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"This Server ID isn't available", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
                [alert release];
            }
            break;
        }
        case eP2pConnectAccountError:
        {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Username/Password is incorrect.", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"OK", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self backFromLoading];
                                         [parent showSiteInfoDetail:selectedIndex];
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Username/Password is incorrect.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:nil];
                [alert release];
            }
            break;
        }
        default:
            break;
    }
}


- (void)alertView:(UIAlertView *) alertView didDismissWithButtonIndex:(NSInteger) buttionIndex {
    if ([alertView.message hasPrefix:NSLocalizedString(@"This server can't be connected.", nil)]) {
        if (buttionIndex == 0) {
            [self backFromLoading];
        }
        else if (buttionIndex == 1) {
            [self resetProgressBar];
            m_progressstate = eProgressStart;
            [[parent m_ServerMgr] loginAllServer];
        }
    }
    else
    {
        [self backFromLoading];
        [parent showSiteInfoDetail:selectedIndex];
    }
    
    return;
}
                 
- (void)mailComposeController:(MFMailComposeViewController *) controller didFinishWithResult:(MFMailComposeResult) result error:(NSError *) error {
	[self dismissViewControllerAnimated:YES completion:nil]; // dismissModalViewControllerAnimated: is deprecated in iOS 6.0
	
    LiveViewAppDelegate * appDelegate = (LiveViewAppDelegate *) [[UIApplication sharedApplication] delegate];
	if ([appDelegate bDelayMsg] == TRUE)
	{
		[appDelegate setBDelayMsg:FALSE];
		[parent setServerManager:nil];
		[appDelegate showServerDownMessage:Np_EVENT_CONNECTION_LOST];
	}
}
@end
