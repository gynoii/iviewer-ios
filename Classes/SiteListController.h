//
//  favoriteSite.h
//  TabView
//
//  Created by johnlinvc on 10/02/04.
//  Copyright 2010 com.debug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "ControlHelper.h"
#import "EventViewController.h"
#import "basicInfo.h"

#if DEFAULT_DEMO_LINK
#define DEFAULT_DEMO_ROW 0
#endif

typedef enum K_SHOW_SEC {
    kMyServerSection = 0,
    kMyViewSection
} kShowSection;

@class SlideView;
@class ServerInfo;

@interface SiteListController : UITableViewController <ServerManagerLoginDelegate, ServerManagerSiteListDelegate, UINavigationControllerDelegate, UIAlertViewDelegate>{
	NSMutableArray *_displayedObjects;
    NSMutableArray *_displayedMyView;
    NSMutableArray *_displayedBsicinfo;
    NSMutableArray *_Basicinfo;
    ServerManager *m_ServerMgr;
	IBOutlet UIActivityIndicatorView *indicator;
	BOOL _logining;
	NSIndexPath  *delIndexPath;
    NSMutableArray * _unregServerArray;
    UIAlertView *aboutAlert;
    UIAlertController *aboutAlertController;
    
    BOOL _isShowMyServer;
    BOOL _isShowMyView;
    BOOL _isLoginCloud;
}

@property (nonatomic, retain) NSMutableArray * displayedObjects;
@property (nonatomic, retain) NSMutableArray * displayedMyView;
@property (nonatomic, retain) NSMutableArray * displayedBsicinfo;
@property (nonatomic, retain) ServerManager * m_ServerMgr;
@property (nonatomic, assign) BOOL logining;
@property (nonatomic, retain) NSMutableArray * unregServerArray; // jerrylu, 2012/08/20
@property (nonatomic) BOOL isShowMyServer;
@property (nonatomic) BOOL isShowMyView;
@property (nonatomic) BOOL isLoginCloud;
@property (nonatomic, assign) UIButton * syncButton;

+ (NSString *)pathForDocumentWithName:(NSString *) documentName;

- (void)save;
- (void)addObject:(id) anObject;

- (void)showSiteInfoDetail:(NSIndexPath *) indexPath;
- (void)openServerView:(ServerInfo *) sinfo;
- (void)openMyView:(MyViewInfo *) vinfo;
- (void)openEventView:(Eventinfo *) einfo;
- (void)openLiveViewFromEventView:(int) CameraIndex;
- (void)setServerManager:(ServerManager *) srvmgr;
- (void)removeMyServerInMyView:(ServerInfo *) sinfo;
- (void)saveDisplayedBasicinfo;
- (void)clearandSaveData:(NSMutableArray*) server with:(NSMutableArray*) view;
#if FUNC_PUSH_NOTIFICATION_SUPPORT
- (void)unRegPushServer:(ServerInfo *) sinfo;
#endif
- (void)changeLoginStatus:(BOOL) bLogin;
- (void)goToSyncProfile;
@end
