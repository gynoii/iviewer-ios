//
//  EventAlertView.m
//  iViewer
//
//  Created by NUUO on 12/8/8.
//
//

#import "EventAlertView.h"
#import "LiveViewAppDelegate.h"

@implementation EventAlertView
@synthesize handledelegate;
@synthesize eInfo;

- (void)dealloc {
    [eInfo release];
    eInfo = nil;
    [alertview release];
    alertview = nil;
    alertController = nil;
    
    [super dealloc];
}

- (void)showAlert {
    /*
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        alertController = [UIAlertController alertControllerWithTitle:@"" message:eInfo.eventMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* close = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Close", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if (handledelegate != nil &&
                                     [handledelegate respondsToSelector:@selector(eventAlertViewRemoveAlertFromList:)])
                                     [handledelegate eventAlertViewRemoveAlertFromList:self];
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                             }];
        UIAlertAction* view = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"View", nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    eInfo.isNotRead = 0;
                                    
                                    if (handledelegate != nil &&
                                        [handledelegate respondsToSelector:@selector(eventAlertViewShowEventView:)])
                                        [handledelegate eventAlertViewShowEventView:self];
                                    
                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                }];
        [alertController addAction:close];
        [alertController addAction:view];
        
        LiveViewAppDelegate *appdelegate = (LiveViewAppDelegate *)handledelegate;
        [appdelegate.navigationController presentViewController:alertController animated:YES completion:nil];
    }
    else {
     */
        alertview = [[UIAlertView alloc] initWithTitle:@"" message:eInfo.eventMsg delegate:self cancelButtonTitle:NSLocalizedString(@"Close", nil) otherButtonTitles:NSLocalizedString(@"View", nil), nil];
        
        NSArray *subviewArray = alertview.subviews;
        for(int x = 1; x < [subviewArray count]; x++){
            if([[[subviewArray objectAtIndex:x] class] isSubclassOfClass:[UILabel class]]) {
                UILabel *label = [subviewArray objectAtIndex:x];
                label.textAlignment = NSTextAlignmentLeft;
            }
        }
        
//        [alertview show]; // Annoying - Brosso
    //}
}

- (void)dismissAlert {
    if ((NSNull *)alertview != [NSNull null])
        [alertview dismissWithClickedButtonIndex:0 animated:NO];
    
    if ((NSNull *)alertController != [NSNull null])
        [alertController dismissViewControllerAnimated:NO completion:nil];
}

# pragma mark - UIAlertViewDelegate Method

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        eInfo.isNotRead = 0; // jerrylu, 2012/11/15
        
        if (handledelegate != nil &&
            [handledelegate respondsToSelector:@selector(eventAlertViewShowEventView:)])
            [handledelegate eventAlertViewShowEventView:self];
    }
    else {
        if (handledelegate != nil &&
            [handledelegate respondsToSelector:@selector(eventAlertViewRemoveAlertFromList:)])
            [handledelegate eventAlertViewRemoveAlertFromList:self];
    }
}

@end
