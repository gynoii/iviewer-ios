//
//  oneViewControl.m
//  PageView
//
//  Created by johnlinvc on 09/12/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//


#import "SinglePageViewController_4X6.h"

#define LABEL_HEIGHT_P 18 //Portrait
#define LABEL_HEIGHT_L 18 //Landscape
#define IMG_HEIGHT_P 132
#define IMG_HEIGHT_L 148
#define START_HEIGHT_P 0
#define START_HEIGHT_L 0
#define COL 6
#define ROW 4
#define IND_SIZE 10

@implementation SinglePageViewController_4X6

@synthesize image0 = _image0;
@synthesize image1 = _image1;
@synthesize image2 = _image2;
@synthesize image3 = _image3;
@synthesize image4 = _image4;
@synthesize image5 = _image5;
@synthesize image6 = _image6;
@synthesize image7 = _image7;
@synthesize image8 = _image8;
@synthesize image9 = _image9;
@synthesize image10 = _image10;
@synthesize image11 = _image11;
@synthesize image12 = _image12;
@synthesize image13 = _image13;
@synthesize image14 = _image14;
@synthesize image15 = _image15;
@synthesize image16 = _image16;
@synthesize image17 = _image17;
@synthesize image18 = _image18;
@synthesize image19 = _image19;
@synthesize image20 = _image20;
@synthesize image21 = _image21;
@synthesize image22 = _image22;
@synthesize image23 = _image23;

- (id) initwithPage:(int) page {
	if((self = [super initWithNibName:@"SinglePageViewController_4X6" bundle:nil])) {
		iPageIdx = page;
	}
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    //label.text =[NSString stringWithFormat:@"this page is %d",n+1];
	//self.view.backgroundColor = [UIColor colorWithRed:(CGFloat) n*20 green:(CGFloat) n*20 blue:(CGFloat) n*20 alpha:0];
	
	self.view.backgroundColor = [UIColor blackColor];
	_imageArray = [[NSMutableArray alloc] initWithObjects:_image0, _image1, _image2, _image3, _image4, _image5, 
														  _image6, _image7, _image8, _image9, _image10, _image11,
														  _image12, _image13, _image14, _image15, _image16, _image17, 
														  _image18, _image19, _image20, _image21, _image22, _image23, nil];
	
	_labelArray = [[NSMutableArray alloc] initWithObjects:_label0, _label1, _label2, _label3, _label4, _label5, 
														  _label6, _label7, _label8, _label9, _label10, _label11,
														  _label12, _label13, _label14, _label15, _label16, _label17, 
														  _label18, _label19, _label20, _label21, _label22, _label23, nil];
	
    _labelConnectionArray = [[NSMutableArray alloc] initWithObjects:_labelconnect0, _labelconnect1, _labelconnect2, _labelconnect3, _labelconnect4, _labelconnect5, _labelconnect6, _labelconnect7, _labelconnect8, _labelconnect9, _labelconnect10, _labelconnect11, _labelconnect12, _labelconnect13, _labelconnect14, _labelconnect15, _labelconnect16, _labelconnect17, _labelconnect18, _labelconnect19, _labelconnect20, _labelconnect21, _labelconnect22, _labelconnect23, nil];
	for (int i = 0 ; i < [_labelConnectionArray count]; i++) {
        UIView * clabel = [_labelConnectionArray objectAtIndex:i];
        [clabel setHidden:YES];
    }
    
	_indicatorArray = [[NSMutableArray alloc] initWithObjects:_indicator0, _indicator1, _indicator2, _indicator3, _indicator4, _indicator5, 
															  _indicator6, _indicator7, _indicator8, _indicator9, _indicator10, _indicator11, 
															  _indicator12, _indicator13, _indicator14, _indicator15, _indicator16, _indicator17, 
															  _indicator18, _indicator19, _indicator20, _indicator21, _indicator22, _indicator23, nil];
	
 	[super viewDidLoad];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if (UIInterfaceOrientationIsPortrait(fromInterfaceOrientation)) {
		int width = self.view.frame.size.width / ROW;
		for (int col = 0; col < COL; col++) {
			for (int row = 0; row < ROW; row++) {
				UIView * label = [_labelArray objectAtIndex:col * ROW + row];
                UIView * clabel = [_labelConnectionArray objectAtIndex:col * ROW + row];
				UIView * img = [_imageArray objectAtIndex:col * ROW + row];
				UIView * ind = [_indicatorArray objectAtIndex:col * ROW + row];
				label.frame = CGRectMake(row * width, (LABEL_HEIGHT_P + IMG_HEIGHT_P) * col + START_HEIGHT_P, width, LABEL_HEIGHT_P);
				img.frame = CGRectMake(row * width, (LABEL_HEIGHT_P + IMG_HEIGHT_P) * col + LABEL_HEIGHT_P + START_HEIGHT_P, width, IMG_HEIGHT_P);
				CGPoint center = img.center;
				ind.frame = CGRectMake(center.x - IND_SIZE, center.y - 10, 20, 20);
                clabel.center = center;
			}
		}
	}
	else {
		int width = self.view.frame.size.width / COL;
		for (int col = 0; col < ROW; col++) {
			for (int row = 0; row < COL; row++) {
				UILabel * label = [_labelArray objectAtIndex:col * COL + row];
                UIView * clabel = [_labelConnectionArray objectAtIndex:col * COL + row];
				UIView * img = [_imageArray objectAtIndex:col * COL + row];
				UIView * ind = [_indicatorArray objectAtIndex:col * COL + row];
				label.frame = CGRectMake(row * width, (LABEL_HEIGHT_L + IMG_HEIGHT_L) * col + START_HEIGHT_L, width, LABEL_HEIGHT_L);
				img.frame = CGRectMake(row * width, (LABEL_HEIGHT_L + IMG_HEIGHT_L) * col + LABEL_HEIGHT_L + START_HEIGHT_L, width, IMG_HEIGHT_L);
				CGPoint center = img.center;
				ind.frame = CGRectMake(center.x - IND_SIZE, center.y - IND_SIZE, 20, 20);
                clabel.center = center;
			}
		}
	}
    
    if (isScaling) {
        for (UIView *view in _imageArray) {
            view.transform = CGAffineTransformScale(view.transform, scale_size, scale_size);
        }
    }
}

- (void) didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void) dealloc {
    [super dealloc];
}

- (int)getCameraIndexByTouchPoint:(CGPoint) tappoint {
    UIInterfaceOrientation to;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            to = UIInterfaceOrientationLandscapeLeft;
        }
        else {
            to = UIInterfaceOrientationPortrait;
        }
    }
    else {
        to = self.interfaceOrientation;
    }
    
    if (UIInterfaceOrientationIsPortrait(to)) {
        for (int col = 0; col < COL; col++) {
            for (int row = 0; row < ROW; row++) {
                int index = col * ROW + row;
                UIView * label = [_labelArray objectAtIndex:index];
                UIView * img = [_imageArray objectAtIndex:index];
                
                if (CGRectContainsPoint(label.frame, tappoint) ||
                    CGRectContainsPoint(img.frame, tappoint)) {
                    return iPageIdx * [slideView getPreviewNum] + index;
                }
            }
        }
    }
    else {
        for (int col = 0; col < ROW; col++) {
			for (int row = 0; row < COL; row++) {
				int index = col * COL + row;
                UILabel * label = [_labelArray objectAtIndex:index];
				UIView * img = [_imageArray objectAtIndex:index];
				
                if (CGRectContainsPoint(label.frame, tappoint) ||
                    CGRectContainsPoint(img.frame, tappoint)) {
                    return iPageIdx * [slideView getPreviewNum] + index;
                }
			}
		}
    }
    return -1;
}

@end
