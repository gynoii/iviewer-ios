//
//  oneViewControl.h
//  PageView
//
//  Created by johnlinvc on 09/12/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "SinglePageViewController_Base.h"

@interface SinglePageViewController_4X6 : SinglePageViewController_Base {

	IBOutlet TapDetectingImageView * _image0;
	IBOutlet TapDetectingImageView * _image1;
	IBOutlet TapDetectingImageView * _image2;
	IBOutlet TapDetectingImageView * _image3;
	IBOutlet TapDetectingImageView * _image4;
	IBOutlet TapDetectingImageView * _image5;
	IBOutlet TapDetectingImageView * _image6;
	IBOutlet TapDetectingImageView * _image7;
	IBOutlet TapDetectingImageView * _image8;
	IBOutlet TapDetectingImageView * _image9;
	IBOutlet TapDetectingImageView * _image10;
	IBOutlet TapDetectingImageView * _image11;
	IBOutlet TapDetectingImageView * _image12;
	IBOutlet TapDetectingImageView * _image13;
	IBOutlet TapDetectingImageView * _image14;
	IBOutlet TapDetectingImageView * _image15;
	IBOutlet TapDetectingImageView * _image16;
	IBOutlet TapDetectingImageView * _image17;
	IBOutlet TapDetectingImageView * _image18;
	IBOutlet TapDetectingImageView * _image19;
	IBOutlet TapDetectingImageView * _image20;
	IBOutlet TapDetectingImageView * _image21;
	IBOutlet TapDetectingImageView * _image22;
	IBOutlet TapDetectingImageView * _image23;
	
	IBOutlet UILabel * _label0;
	IBOutlet UILabel * _label1;
	IBOutlet UILabel * _label2;
	IBOutlet UILabel * _label3;
	IBOutlet UILabel * _label4;
	IBOutlet UILabel * _label5;
	IBOutlet UILabel * _label6;
	IBOutlet UILabel * _label7;
	IBOutlet UILabel * _label8;
	IBOutlet UILabel * _label9;
	IBOutlet UILabel * _label10;
	IBOutlet UILabel * _label11;
	IBOutlet UILabel * _label12;
	IBOutlet UILabel * _label13;
	IBOutlet UILabel * _label14;
	IBOutlet UILabel * _label15;
	IBOutlet UILabel * _label16;
	IBOutlet UILabel * _label17;
	IBOutlet UILabel * _label18;
	IBOutlet UILabel * _label19;
	IBOutlet UILabel * _label20;
	IBOutlet UILabel * _label21;
	IBOutlet UILabel * _label22;
	IBOutlet UILabel * _label23;
	
	IBOutlet UIActivityIndicatorView * _indicator0;
	IBOutlet UIActivityIndicatorView * _indicator1;
	IBOutlet UIActivityIndicatorView * _indicator2;
	IBOutlet UIActivityIndicatorView * _indicator3;
	IBOutlet UIActivityIndicatorView * _indicator4;
	IBOutlet UIActivityIndicatorView * _indicator5;
	IBOutlet UIActivityIndicatorView * _indicator6;
	IBOutlet UIActivityIndicatorView * _indicator7;
	IBOutlet UIActivityIndicatorView * _indicator8;
	IBOutlet UIActivityIndicatorView * _indicator9;
	IBOutlet UIActivityIndicatorView * _indicator10;
	IBOutlet UIActivityIndicatorView * _indicator11;
	IBOutlet UIActivityIndicatorView * _indicator12;
	IBOutlet UIActivityIndicatorView * _indicator13;
	IBOutlet UIActivityIndicatorView * _indicator14;
	IBOutlet UIActivityIndicatorView * _indicator15;
	IBOutlet UIActivityIndicatorView * _indicator16;
	IBOutlet UIActivityIndicatorView * _indicator17;
	IBOutlet UIActivityIndicatorView * _indicator18;
	IBOutlet UIActivityIndicatorView * _indicator19;
	IBOutlet UIActivityIndicatorView * _indicator20;
	IBOutlet UIActivityIndicatorView * _indicator21;
	IBOutlet UIActivityIndicatorView * _indicator22;
	IBOutlet UIActivityIndicatorView * _indicator23;
    
    IBOutlet UILabel * _labelconnect0;
	IBOutlet UILabel * _labelconnect1;
    IBOutlet UILabel * _labelconnect2;
	IBOutlet UILabel * _labelconnect3;
	IBOutlet UILabel * _labelconnect4;
	IBOutlet UILabel * _labelconnect5;
	IBOutlet UILabel * _labelconnect6;
	IBOutlet UILabel * _labelconnect7;
	IBOutlet UILabel * _labelconnect8;
	IBOutlet UILabel * _labelconnect9;
	IBOutlet UILabel * _labelconnect10;
	IBOutlet UILabel * _labelconnect11;
	IBOutlet UILabel * _labelconnect12;
	IBOutlet UILabel * _labelconnect13;
	IBOutlet UILabel * _labelconnect14;
	IBOutlet UILabel * _labelconnect15;
	IBOutlet UILabel * _labelconnect16;
	IBOutlet UILabel * _labelconnect17;
	IBOutlet UILabel * _labelconnect18;
	IBOutlet UILabel * _labelconnect19;
	IBOutlet UILabel * _labelconnect20;
	IBOutlet UILabel * _labelconnect21;
	IBOutlet UILabel * _labelconnect22;
	IBOutlet UILabel * _labelconnect23;
}

@property (nonatomic, retain) TapDetectingImageView * image0;
@property (nonatomic, retain) TapDetectingImageView * image1;
@property (nonatomic, retain) TapDetectingImageView * image2;
@property (nonatomic, retain) TapDetectingImageView * image3;
@property (nonatomic, retain) TapDetectingImageView * image4;
@property (nonatomic, retain) TapDetectingImageView * image5;
@property (nonatomic, retain) TapDetectingImageView * image6;
@property (nonatomic, retain) TapDetectingImageView * image7;
@property (nonatomic, retain) TapDetectingImageView * image8;
@property (nonatomic, retain) TapDetectingImageView * image9;
@property (nonatomic, retain) TapDetectingImageView * image10;
@property (nonatomic, retain) TapDetectingImageView * image11;
@property (nonatomic, retain) TapDetectingImageView * image12;
@property (nonatomic, retain) TapDetectingImageView * image13;
@property (nonatomic, retain) TapDetectingImageView * image14;
@property (nonatomic, retain) TapDetectingImageView * image15;
@property (nonatomic, retain) TapDetectingImageView * image16;
@property (nonatomic, retain) TapDetectingImageView * image17;
@property (nonatomic, retain) TapDetectingImageView * image18;
@property (nonatomic, retain) TapDetectingImageView * image19;
@property (nonatomic, retain) TapDetectingImageView * image20;
@property (nonatomic, retain) TapDetectingImageView * image21;
@property (nonatomic, retain) TapDetectingImageView * image22;
@property (nonatomic, retain) TapDetectingImageView * image23;

- (id)initwithPage:(int) page;

@end
