//
//  oneViewControl.m
//  PageView
//
//  Created by johnlinvc on 09/12/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//


#import "SinglePageViewController_Base.h"
#import <QuartzCore/CALayer.h>
#import "SingleCamViewController.h"
#import "SlideView.h"
#import "TapDetectingImageView.h"
#import "MyGLView.h"
#import "CameraHelper.h"
#import "LiveViewAppDelegate.h"

#include "Utility.h"

@implementation SinglePageViewController_Base

@synthesize slideView;
@synthesize singleCam;
@synthesize serverMgr;
@synthesize iPageIdx;
@synthesize singleCamPos;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	singleCam = nil;
    isScaling = NO;
    scale_size = 1.0;

    [self.view setBackgroundColor:COLOR_BACKGROUND];
    
	int count = [serverMgr getCameraTotalNum];
	int initPosition = iPageIdx * [slideView getPreviewNum];
	camNumInPage = (initPosition + [slideView getPreviewNum]) > count ? count - initPosition : [slideView getPreviewNum];	
    
	//set label
	for (int i = 0; i < camNumInPage; i++) {
		UILabel * label = [_labelArray objectAtIndex:i];
        [label setTextColor:COLOR_CAMERANAMECOLOR];
		NSString * camName = [serverMgr getDeviceNameByChIndex:i+initPosition];
		[label setText:camName];
	}
	[self setDefaultPic];
	[self startAnimatingIndicator];
	[super viewDidLoad];
    
    _myCondition = [[NSCondition alloc] init];
    
    /* Repaint Timer */
    displayTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0/30
                                                    target:self
                                                  selector:@selector(displayNextFrame:)
                                                  userInfo:nil
                                                   repeats:YES] retain];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopDisplayFrame)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopDisplayFrame)
                                                 name:NOTIFICATION_STOP_QUERY_THREAD
                                               object:[UIApplication sharedApplication]];
}

- (void) didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_STOP_QUERY_THREAD object:[UIApplication sharedApplication]];
    
    [self stopDisplayFrame];
    
    [_indicatorArray release];
	[_labelArray release];
    [_labelConnectionArray release];
	for (TapDetectingImageView * obj in _imageArray) {
		[obj setDelegate:nil];
	}
	[_imageArray release];
    [_myCondition release];
    singleCam = nil;
    [slideView release];
    slideView = nil;
    
    [serverMgr release];
    serverMgr = nil;
    
    [super dealloc];
}

- (void)setDefaultPic {
	for (TapDetectingImageView * imgView in _imageArray) {
		UIImage * imgDefault = [UIImage imageNamed:@"novideo320x240_noword.png"];
		[imgView setImage:imgDefault];
	}
}

- (void)changeImageViewScale:(float) scale {
    if (isScaling)
        return;
    
    scale_size = scale;
    isScaling = YES;
    for (UIView *view in _imageArray) {
        view.transform = CGAffineTransformScale(view.transform, scale, scale);
    }
}

- (void)restoreImageViewScale {
    if (!isScaling)
        return;
    
    isScaling = NO;
    float scale = 1.0 / scale_size;
    for (UIView *view in _imageArray) {
        view.transform = CGAffineTransformScale(view.transform, scale, scale);
    }
}

- (void)reloadThisPage {
    int initPosition = iPageIdx * [slideView getPreviewNum];
	
    for (int i = 0; i < camNumInPage; i++) {
		UILabel * label = [_labelArray objectAtIndex:i];
		NSString * camName = [serverMgr getDeviceNameByChIndex:i+initPosition];
		[label setText:camName];
        UILabel * clabel = [_labelConnectionArray objectAtIndex:i];
        if ([serverMgr isSupportSecondProfile:i+initPosition])
            [clabel setHidden:YES];
        else
            [clabel setHidden:NO];
	}
	[self setDefaultPic];
	[self startAnimatingIndicator];
}

- (void)stop
{
    [self stopDisplayFrame];
}

- (void)backFromSingleView {
    if ([slideView getLayoutType] != eLayout_1X1) {
        [slideView reconnectCurrentPage];
    }
    if(serverMgr.isSingleCam){
        [slideView stopStreamAndBackToSiteList];
    }
}

- (void)startAnimatingIndicator {
	for (int i = 0; i < camNumInPage; i++) {
        UIActivityIndicatorView * indicator = [_indicatorArray objectAtIndex:i];
        [indicator startAnimating];
        [indicator setColor:COLOR_CAMERASPINNERCOLOR];
	}
}

- (int)getCameraIndexByTouchPoint:(CGPoint) tappoint {
    return 0;
}

- (void)getImageViewByCameraIndex:(UIImageView *)imageView index:(int)c_index {
    int index = c_index - iPageIdx * [slideView getPreviewNum];
    
    UIImageView * img = [_imageArray objectAtIndex:index];
    [imageView setFrame:img.frame];
    [imageView setImage:img.image];
}

- (void)clearSelectImageView {
    for (UIView *img in _imageArray) {
        img.layer.borderColor = [UIColor clearColor].CGColor;
        img.layer.borderWidth = 0;
    }
}

- (void)setSelectedImageView:(int)c_index {
    [self clearSelectImageView];
    
    int index = c_index - iPageIdx * [slideView getPreviewNum];
    UIImageView * img = [_imageArray objectAtIndex:index];
    img.layer.borderColor = [UIColor greenColor].CGColor;
    img.layer.borderWidth = 1;
}

#pragma mark -
#pragma mark method called by SlideView

- (void)playbackDisconnection {
    UINavigationController * navi = slideView.navigationController;
	UIViewController * visi = navi.visibleViewController;
	if (visi == singleCam) { // 1x1 View
        [singleCam playbackDisconnection];
    }
}

-(void)playbackNextPreviousFrameSucess {
    UINavigationController * navi = slideView.navigationController;
	UIViewController * visi = navi.visibleViewController;
	if (visi == singleCam) {
        [singleCam playbackHoldScreen:NO];
    }
}

- (void)getTalkReserved:(NSString *)servername user:(NSString *)username {
    UINavigationController * navi = slideView.navigationController;
	UIViewController * visi = navi.visibleViewController;
	if (visi == singleCam) { // 1x1 View
        [singleCam getTalkReserved:servername user:username];
    }
}

- (void)getTalkSessionError {
    UINavigationController * navi = slideView.navigationController;
	UIViewController * visi = navi.visibleViewController;
	if (visi == singleCam) { // 1x1 View
        [singleCam getTalkSessionError];
    }
}

#pragma mark -
#pragma mark TapDetectingImageViewDelegate interface
- (void)imageViewTaped: (TapDetectingImageView *) view  {
    if (slideView.isLockSingleView)
        return;
    
    TapDetectingImageView * thisView = nil;
		
    for (id innerObj in _imageArray) 
    {
        if (innerObj == view) 
        {
            thisView = view;
        }
    }
		
	if (thisView != nil) {
		int idx = [_imageArray indexOfObject:thisView];
		int initPosition = iPageIdx * [slideView getPreviewNum];	
		BOOL bStreaming = [serverMgr isCamStreaming:initPosition + idx];
		if (bStreaming == TRUE && idx < camNumInPage) {
			[self openSingleViewAtPosition:idx];
		}
    }
}
	
- (void)tapDetectingImageView:(TapDetectingImageView *) view gotSingleTapAtPoint:(CGPoint) tapPoint {
	[self imageViewTaped:view];
	
}
- (void)tapDetectingImageView:(TapDetectingImageView *) view gotDoubleTapAtPoint:(CGPoint) tapPoint {
	[self imageViewTaped:view];
}

- (void)tapDetectingImageView:(TapDetectingImageView *) view gotTwoFingerTapAtPoint:(CGPoint) tapPoint {
}

- (void)openSingleViewAtPosition:(int) pos {
    if (self.slideView != nil) {
        if ([self.slideView.navigationController.visibleViewController isKindOfClass:[SlideView class]]) {
            singleCamPos = pos;
            int camInArray = iPageIdx * [slideView getPreviewNum] + singleCamPos;
            
            SingleCamViewController * mCam = [[SingleCamViewController alloc] initWithServerManager:serverMgr CurrentCam:camInArray];
            [mCam setParent:self];
			[mCam setTitle:[[_labelArray objectAtIndex:pos] text]];
            mCam.changeLayoutDelegate = self.slideView;
            
            int ch_index = iPageIdx * [slideView getPreviewNum] + pos;
            [serverMgr updateAudioAndPtzDeviceByChIndex:ch_index];
            [mCam setPtzCap:[serverMgr isSupportPTZ:ch_index]];
			[mCam setAudioCap:[serverMgr isSupportAudio:ch_index]];
            [mCam setTalkCap:[serverMgr isSupportTalk:ch_index]];
			[self setSingleCam:mCam];
            [serverMgr setPlaybackDelegate:mCam];
			[self.slideView.navigationController pushViewController:mCam animated:YES];
			singleCam = mCam;
			//clear to default picture
			[self setDefaultPic];
			[serverMgr disconnectAllCam];
			[serverMgr connectToCamWithHighResolution:ch_index];
            [mCam release];
		}
		else {
			return;
		}
	}
}

#pragma mark -
#pragma mark MyGLViewGestureDelegate interface

- (void)myGLViewDelegateSingleTap:(MyGLView *) view {
    if (slideView.isLockSingleView)
        return;
    
    MyGLView * thisView = nil;
    
    for (id innerObj in _imageArray)
    {
        if (innerObj == view)
        {
            thisView = view;
        }
    }
    
	if (thisView != nil) {
		int idx = [_imageArray indexOfObject:thisView];
		int initPosition = iPageIdx * [slideView getPreviewNum];
		BOOL bStreaming = [serverMgr isCamStreaming:initPosition + idx];
		if (bStreaming == TRUE && idx < camNumInPage) {
			[self openSingleViewAtPosition:idx];
		}
    }
}

#pragma mark -
#pragma mark Render method
-(void)displayNextFrame:(NSTimer *)timer {
    if (!timer.isValid)
        return;
    
    [_myCondition lock];
    
    if (singleCam) { // 1x1 View
        int camInArray = iPageIdx * [slideView getPreviewNum] + singleCamPos;
        BOOL isUpdate = FALSE;
        
#if RENDER_BY_OPENGL
        MyVideoFrameBuf *img = [slideView getImageBufOfCamIndex:camInArray isUpdate:&isUpdate];
#else
        UIImage *img = [slideView getImageOfCamIndex:camInArray isUpdate:&isUpdate];
#endif
        if ((NSNull *)img == [NSNull null]) {
            [singleCam imageDidnotRefreshed];
        }
        else if (isUpdate) {
#if RENDER_BY_OPENGL
            [singleCam imageBufDidRefreshed:img];
#else
            [singleCam imageDidRefreshed:img];
#endif
        }
	}
	else { // Multi layout
        if (slideView.pageControl.currentPage == iPageIdx) {
            // jerrylu, 2012/10/01, [slideView getPreviewNum] => camNumInPage, avoid empty camera to run the indicator
            for (int indexMapping=0; indexMapping < camNumInPage; indexMapping++) {
                if (!timer.isValid)
                    break;
                
                if (indexMapping < [_imageArray count]) {
                    int camIndex = iPageIdx * [slideView getPreviewNum] + indexMapping;
                    BOOL isUpdate = FALSE;

                    UIImage * img = [slideView getImageOfCamIndex:camIndex isUpdate:&isUpdate];
                    if ([serverMgr isSupportSecondProfile:camIndex]) {
                        if ([NSNull null] != (NSNull *) img) {
                            if (isUpdate && [serverMgr isSupportSecondProfile:camIndex]) {
                                UIActivityIndicatorView * indicator = [_indicatorArray objectAtIndex:indexMapping];
                                [indicator stopAnimating];
                                TapDetectingImageView * view = [_imageArray objectAtIndex:indexMapping];
                                [view setImage:img];
                            }
                        }
                        else { // jerrylu, 2012/06/05, session lost => reset layout
                            UIActivityIndicatorView * indicator = [_indicatorArray objectAtIndex:indexMapping];
                            if (![indicator isAnimating]) {
                                [indicator startAnimating];
                            }
                            UIImage * imgDefault = [UIImage imageNamed:@"novideo320x240_noword.png"];
                            TapDetectingImageView *view = [_imageArray objectAtIndex:indexMapping];
                            [view setImage:imgDefault];
                        }
                    }
                    else {
                        UILabel * clabel = [_labelConnectionArray objectAtIndex:indexMapping];
                        [clabel setHidden:NO];
                        UIActivityIndicatorView * indicator = [_indicatorArray objectAtIndex:indexMapping];
                        [indicator stopAnimating];
                        
                        UIImage * imgDefault = [UIImage imageNamed:@"novideo320x240_noword.png"];
                        TapDetectingImageView *view = [_imageArray objectAtIndex:indexMapping];
                        [view setImage:imgDefault];
                    }
                }
            }
        }
	}
    
    [_myCondition unlock];
}

- (void) stopDisplayFrame {
    if (displayTimer != nil && [displayTimer isValid]) {
        [displayTimer invalidate];
        while ([displayTimer isValid]) {
            [NSThread sleepForTimeInterval:0.5];
        }
        displayTimer = nil;
    }
}

@end
