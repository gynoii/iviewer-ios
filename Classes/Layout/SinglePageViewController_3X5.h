//
//  oneViewControl.h
//  PageView
//
//  Created by johnlinvc on 09/12/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "SinglePageViewController_Base.h"

@interface SinglePageViewController_3X5 : SinglePageViewController_Base {

	IBOutlet TapDetectingImageView * _image0;
	IBOutlet TapDetectingImageView * _image1;
	IBOutlet TapDetectingImageView * _image2;
	IBOutlet TapDetectingImageView * _image3;
	IBOutlet TapDetectingImageView * _image4;
	IBOutlet TapDetectingImageView * _image5;
	IBOutlet TapDetectingImageView * _image6;
	IBOutlet TapDetectingImageView * _image7;
	IBOutlet TapDetectingImageView * _image8;
	IBOutlet TapDetectingImageView * _image9;
	IBOutlet TapDetectingImageView * _image10;
	IBOutlet TapDetectingImageView * _image11;
	IBOutlet TapDetectingImageView * _image12;
	IBOutlet TapDetectingImageView * _image13;
	IBOutlet TapDetectingImageView * _image14;

	IBOutlet UILabel * _label0;
	IBOutlet UILabel * _label1;
	IBOutlet UILabel * _label2;
	IBOutlet UILabel * _label3;
	IBOutlet UILabel * _label4;
	IBOutlet UILabel * _label5;
	IBOutlet UILabel * _label6;
	IBOutlet UILabel * _label7;
	IBOutlet UILabel * _label8;
	IBOutlet UILabel * _label9;
	IBOutlet UILabel * _label10;
	IBOutlet UILabel * _label11;
	IBOutlet UILabel * _label12;
	IBOutlet UILabel * _label13;
	IBOutlet UILabel * _label14;
	
	IBOutlet UIActivityIndicatorView * _indicator0;
	IBOutlet UIActivityIndicatorView * _indicator1;
	IBOutlet UIActivityIndicatorView * _indicator2;
	IBOutlet UIActivityIndicatorView * _indicator3;
	IBOutlet UIActivityIndicatorView * _indicator4;
	IBOutlet UIActivityIndicatorView * _indicator5;
	IBOutlet UIActivityIndicatorView * _indicator6;
	IBOutlet UIActivityIndicatorView * _indicator7;
	IBOutlet UIActivityIndicatorView * _indicator8;
	IBOutlet UIActivityIndicatorView * _indicator9;
	IBOutlet UIActivityIndicatorView * _indicator10;
	IBOutlet UIActivityIndicatorView * _indicator11;
	IBOutlet UIActivityIndicatorView * _indicator12;
	IBOutlet UIActivityIndicatorView * _indicator13;
	IBOutlet UIActivityIndicatorView * _indicator14;
    
    IBOutlet UILabel * _labelconnect0;
	IBOutlet UILabel * _labelconnect1;
    IBOutlet UILabel * _labelconnect2;
	IBOutlet UILabel * _labelconnect3;
    IBOutlet UILabel * _labelconnect4;
	IBOutlet UILabel * _labelconnect5;
    IBOutlet UILabel * _labelconnect6;
	IBOutlet UILabel * _labelconnect7;
    IBOutlet UILabel * _labelconnect8;
	IBOutlet UILabel * _labelconnect9;
    IBOutlet UILabel * _labelconnect10;
	IBOutlet UILabel * _labelconnect11;
    IBOutlet UILabel * _labelconnect12;
	IBOutlet UILabel * _labelconnect13;
    IBOutlet UILabel * _labelconnect14;
}

- (id)initwithPage:(int) page;

@end
