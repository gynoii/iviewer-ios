//
//  SinglePageViewController_1X1.h
//  iViewer
//
//  Created by NUUO on 12/9/14.
//
//

#import <UIKit/UIKit.h>
#import "PinchSlideImageView.h"
#import "MyGLView.h"

#define IND_SIZE 10

@class SingleCamViewController;

@interface SinglePageViewController_1X1 : UIViewController {
#if RENDER_BY_OPENGL
    MyGLView * mImage;
#else
    IBOutlet PinchSlideImageView * mImage;
#endif
	IBOutlet UILabel * mLabel;
	IBOutlet UIActivityIndicatorView * mIndicator;
	IBOutlet UILabel * mConnectionLabel;
    
    int iCamIdx;
    SingleCamViewController *mParent;
}
#if RENDER_BY_OPENGL
@property (nonatomic, retain) MyGLView * image;
#else
@property (nonatomic, retain) PinchSlideImageView * image;
#endif
@property (nonatomic, retain) UILabel * mLabel;
@property (nonatomic, retain) UIActivityIndicatorView *mIndicator;
@property (nonatomic, retain) UILabel * mConnectionLabel;
@property (nonatomic, assign) int iCamIdx;

- (id)initwithCamIndex:(int) index SingleCamParent:(SingleCamViewController *) singleCam;
- (void)changeLayout:(UIInterfaceOrientation)interfaceOrientation;

@end
