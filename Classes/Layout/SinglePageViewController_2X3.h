//
//  oneViewControl.h
//  PageView
//
//  Created by johnlinvc on 09/12/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "SinglePageViewController_Base.h"

@interface SinglePageViewController_2X3 : SinglePageViewController_Base {

	IBOutlet TapDetectingImageView * _image0;
	IBOutlet TapDetectingImageView * _image1;
	IBOutlet TapDetectingImageView * _image2;
	IBOutlet TapDetectingImageView * _image3;
	IBOutlet TapDetectingImageView * _image4;
	IBOutlet TapDetectingImageView * _image5;
	IBOutlet UILabel * _label0;
	IBOutlet UILabel * _label1;
	IBOutlet UILabel * _label2;
	IBOutlet UILabel * _label3;
	IBOutlet UILabel * _label4;
	IBOutlet UILabel * _label5;
	IBOutlet UIActivityIndicatorView * _indicator0;
	IBOutlet UIActivityIndicatorView * _indicator1;
	IBOutlet UIActivityIndicatorView * _indicator2;
	IBOutlet UIActivityIndicatorView * _indicator3;
	IBOutlet UIActivityIndicatorView * _indicator4;
	IBOutlet UIActivityIndicatorView * _indicator5;
    IBOutlet UILabel * _labelconnect0;
	IBOutlet UILabel * _labelconnect1;
    IBOutlet UILabel * _labelconnect2;
	IBOutlet UILabel * _labelconnect3;
    IBOutlet UILabel * _labelconnect4;
	IBOutlet UILabel * _labelconnect5;
}

- (id)initwithPage:(int) page;

@end
