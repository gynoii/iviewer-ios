//
//  oneViewControl.m
//  PageView
//
//  Created by johnlinvc on 09/12/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//


#import "SinglePageViewController_1X3.h"

#define LABEL_HEIGHT 18
#define BORDER 124
#define BORDER_4 40

#define COL 3
#define ROW 1
#define IND_SIZE 10

@implementation SinglePageViewController_1X3

- (id) initwithPage:(int) page {
	if(self = [super initWithNibName:@"SinglePageViewController_1X3" bundle:nil]) {
		iPageIdx = page;
	}
	return self;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if (UIInterfaceOrientationIsPortrait(fromInterfaceOrientation)) {
		int width = self.view.frame.size.width / ROW;
        int img_height = (self.view.frame.size.height - COL * LABEL_HEIGHT) / COL;
		for (int col = 0; col < COL; col++) {
			for (int row = 0; row < ROW; row++) {
				UIView * label = [_labelArray objectAtIndex:col * ROW + row];
                UIView * clabel = [_labelConnectionArray objectAtIndex:col * ROW + row];
				UIView * img = [_imageArray objectAtIndex:col * ROW + row];
				UIView * ind = [_indicatorArray objectAtIndex:col * ROW + row];
                if(CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size,CGSizeMake(640, 1136))) { // support 4 inch screen
                    label.frame = CGRectMake(row * width,
                                             (LABEL_HEIGHT + img_height) * col,
                                             width, LABEL_HEIGHT);
                    img.frame = CGRectMake(row * width + BORDER_4 / 2,
                                           (LABEL_HEIGHT + img_height) * col + LABEL_HEIGHT,
                                           width - BORDER_4, img_height);
                }
                else {
                    label.frame = CGRectMake(row * width,
                                             (LABEL_HEIGHT + img_height) * col,
                                             width, LABEL_HEIGHT);
                    img.frame = CGRectMake(row * width + BORDER / 2,
                                           (LABEL_HEIGHT + img_height) * col + LABEL_HEIGHT,
                                           width - BORDER, img_height);
                }
				CGPoint center = img.center;
				ind.frame = CGRectMake(center.x - IND_SIZE, center.y - 10, 20, 20);
                clabel.center = img.center;
			}
		}
	}
	else {
		int width = self.view.frame.size.width / COL;
        int img_height = (self.view.frame.size.height - ROW * LABEL_HEIGHT) / ROW;
		for (int col = 0; col < ROW; col++) {
			for (int row = 0; row < COL; row++) {
				UILabel * label = [_labelArray objectAtIndex:col * COL + row];
                UIView * clabel = [_labelConnectionArray objectAtIndex:col * COL + row];
				UIView * img = [_imageArray objectAtIndex:col * COL + row];
				UIView * ind = [_indicatorArray objectAtIndex:col * COL + row];
				label.frame = CGRectMake(row * width,
										 (LABEL_HEIGHT + img_height) * col,
										 width, LABEL_HEIGHT);
				img.frame = CGRectMake(row * width,
									   (LABEL_HEIGHT + img_height) * col + LABEL_HEIGHT,
									   width, img_height);
				CGPoint center = img.center;
				ind.frame = CGRectMake(center.x - IND_SIZE, center.y - IND_SIZE, 20, 20);
                clabel.center = center;
			}
		}
	}
    
    if (isScaling) {
        for (UIView *view in _imageArray) {
            view.transform = CGAffineTransformScale(view.transform, scale_size, scale_size);
        }
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    //label.text =[NSString stringWithFormat:@"this page is %d",n+1];
	//self.view.backgroundColor = [UIColor colorWithRed:(CGFloat) n*20 green:(CGFloat) n*20 blue:(CGFloat) n*20 alpha:0];
	
	self.view.backgroundColor = [UIColor blackColor];
	_imageArray = [[NSMutableArray alloc] initWithObjects:_image0, _image1, _image2, nil];
	
	_labelArray = [[NSMutableArray alloc] initWithObjects:_label0, _label1, _label2, nil];
	
    _labelConnectionArray = [[NSMutableArray alloc] initWithObjects:_labelconnect0, _labelconnect1, _labelconnect2, nil];
    for (int i = 0 ; i < [_labelConnectionArray count]; i++) {
        UIView * clabel = [_labelConnectionArray objectAtIndex:i];
        [clabel setHidden:YES];
    }
    
	_indicatorArray = [[NSMutableArray alloc] initWithObjects:_indicator0, _indicator1, _indicator2, nil];
	   
 	[super viewDidLoad];
}

- (void) didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void) dealloc {
    [super dealloc];
}

- (int)getCameraIndexByTouchPoint:(CGPoint) tappoint {
    UIInterfaceOrientation to;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            to = UIInterfaceOrientationLandscapeLeft;
        }
        else {
            to = UIInterfaceOrientationPortrait;
        }
    }
    else {
        to = self.interfaceOrientation;
    }
    
    if (UIInterfaceOrientationIsPortrait(to)) {
        for (int col = 0; col < COL; col++) {
            for (int row = 0; row < ROW; row++) {
                int index = col * ROW + row;
                UIView * label = [_labelArray objectAtIndex:index];
                UIView * img = [_imageArray objectAtIndex:index];
                
                if (CGRectContainsPoint(label.frame, tappoint) ||
                    CGRectContainsPoint(img.frame, tappoint)) {
                    return iPageIdx * [slideView getPreviewNum] + index;
                }
            }
        }
    }
    else {
        for (int col = 0; col < ROW; col++) {
			for (int row = 0; row < COL; row++) {
				int index = col * COL + row;
                UILabel * label = [_labelArray objectAtIndex:index];
				UIView * img = [_imageArray objectAtIndex:index];
				
                if (CGRectContainsPoint(label.frame, tappoint) ||
                    CGRectContainsPoint(img.frame, tappoint)) {
                    return iPageIdx * [slideView getPreviewNum] + index;
                }
			}
		}
    }
    return -1;
}

@end
