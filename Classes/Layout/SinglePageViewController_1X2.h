//
//  oneViewControl.h
//  PageView
//
//  Created by johnlinvc on 09/12/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "SinglePageViewController_Base.h"

@interface SinglePageViewController_1X2 : SinglePageViewController_Base {

	IBOutlet TapDetectingImageView * _image0;
	IBOutlet TapDetectingImageView * _image1;
		
	IBOutlet UILabel * _label0;
	IBOutlet UILabel * _label1;
		
	IBOutlet UIActivityIndicatorView * _indicator0;
	IBOutlet UIActivityIndicatorView * _indicator1;
    
    IBOutlet UILabel * _labelconnect0;
	IBOutlet UILabel * _labelconnect1;
}

- (id)initwithPage:(int) page;

@end
