//
//  oneViewControl.h
//  PageView
//
//  Created by johnlinvc on 09/12/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveViewAppDelegate.h"
#import "SlideView.h"
#import "TapDetectingImageView.h"
#import "MyGLView.h"

@class SingleCamViewController;
@class TapDetectingImageView;

@interface SinglePageViewController_Base : UIViewController <TapDetectingImageViewDelegate, MyGLViewGestureDelegate> {
	SlideView * slideView;
	SingleCamViewController * singleCam;
	ServerManager * serverMgr;
	int singleCamPos;
	int camNumInPage;
	int iPageIdx;
	NSMutableArray *_indicatorArray;
	NSMutableArray *_labelArray;
	NSMutableArray *_labelConnectionArray;
	NSMutableArray *_imageArray;
	NSCondition *_myCondition;
    
    BOOL isScaling;
    float scale_size;
    
    NSTimer *displayTimer;
}

@property (nonatomic, retain) SlideView * slideView;
@property (nonatomic, assign) SingleCamViewController * singleCam;
@property (nonatomic, retain) ServerManager * serverMgr;
@property (nonatomic, assign) int singleCamPos;
@property (nonatomic, assign) int iPageIdx;

- (void)openSingleViewAtPosition:(int) pos;
- (void)startAnimatingIndicator;
- (void)setDefaultPic;
- (void)imageViewTaped:(TapDetectingImageView *) view;
- (void)backFromSingleView;
- (void)changeImageViewScale:(float) scale;
- (void)restoreImageViewScale;
- (void)reloadThisPage;
- (void)stop;

#pragma mark -
#pragma mark method called by SlideView
- (void)playbackDisconnection;
- (void)playbackNextPreviousFrameSucess;
- (int)getCameraIndexByTouchPoint:(CGPoint) tappoint;
- (void)getImageViewByCameraIndex:(UIImageView *) imageView index:(int)c_index;
- (void)clearSelectImageView;
- (void)setSelectedImageView:(int)c_index;
- (void)getTalkReserved:(NSString *)servername user:(NSString *)username;
- (void)getTalkSessionError;

#pragma mark -
#pragma mark TapDetectingImageViewDelegate interface
- (void)tapDetectingImageView:(TapDetectingImageView *) view gotSingleTapAtPoint:(CGPoint) tapPoint;
- (void)tapDetectingImageView:(TapDetectingImageView *) view gotDoubleTapAtPoint:(CGPoint) tapPoint;
- (void)tapDetectingImageView:(TapDetectingImageView *) view gotTwoFingerTapAtPoint:(CGPoint) tapPoint;

@end
