//
//  SinglePageViewController_1X1.m
//  iViewer
//
//  Created by NUUO on 12/9/14.
//
//

#import "SinglePageViewController_1X1.h"
#import "LiveViewAppDelegate.h"

@implementation SinglePageViewController_1X1

@synthesize image = mImage;
@synthesize mLabel;
@synthesize mIndicator;
@synthesize mConnectionLabel;
@synthesize iCamIdx;

- (id)initwithCamIndex:(int) index SingleCamParent:(SingleCamViewController *) singleCam {
    if (IS_IPAD) {
#if RENDER_BY_OPENGL
        if(self = [super initWithNibName:@"SinglePageViewController_1x1_iPad_gl" bundle:nil]) {
            iCamIdx = index;
            mParent = singleCam;
        }
#else
        if(self = [super initWithNibName:@"SinglePageViewController_1x1_iPad" bundle:nil]) {
            iCamIdx = index;
            mParent = singleCam;
        }
#endif
    }
    else {
#if RENDER_BY_OPENGL
        if(self = [super initWithNibName:@"SinglePageViewController_1x1_gl" bundle:nil]) {
            iCamIdx = index;
            mParent = singleCam;
        }
#else
        if(self = [super initWithNibName:@"SinglePageViewController_1x1" bundle:nil]) {
            iCamIdx = index;
            mParent = singleCam;
        }
#endif
    }
    
	return self;
}

- (void)viewDidLoad
{
#if RENDER_BY_OPENGL
    mImage = [[MyGLView alloc] initWithFrame:[[UIScreen mainScreen] bounds] frameWidth:[[UIScreen mainScreen] bounds].size.width frameHeight:[[UIScreen mainScreen] bounds].size.height];
    [mImage setContentMode:UIViewContentModeScaleAspectFit];
    [mImage enableSingleTap];
    [mImage enableDoubleTap];
    [mImage enablePinch];
    mImage.gDelegate = (id)mParent;
    [self.view insertSubview:mImage atIndex:0];
#else
    mImage.delegate = mParent;
#endif
    [mConnectionLabel setHidden:YES];
    [mLabel setTextColor:COLOR_CAMERANAMECOLOR];
    [mIndicator setColor:COLOR_CAMERASPINNERCOLOR];
    [self.view setBackgroundColor:COLOR_BACKGROUND];
    
    [super viewDidLoad];
    
    //[self enablePhyPTZmode:YES];
}

-(void)dealloc {
#if RENDER_BY_OPENGL
    [mImage removeFromSuperview];
    [mImage release];
#else
    [mImage release];    
#endif
    [mLabel release];
    [mIndicator release];
    [mConnectionLabel release];
    [super dealloc];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self changeLayout:toInterfaceOrientation];
}

- (void)changeLayout:(UIInterfaceOrientation)interfaceOrientation {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        mImage.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        CGPoint center = mImage.center;
        mIndicator.frame = CGRectMake(center.x - IND_SIZE, center.y - IND_SIZE, 20, 20);
        mConnectionLabel.center = mImage.center;
    }
    else {
        if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
            mImage.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
            CGPoint center = mImage.center;
            mIndicator.frame = CGRectMake(center.x - IND_SIZE, center.y - IND_SIZE, 20, 20);
            mConnectionLabel.center = mImage.center;
        }
        else {
            mImage.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width);
            CGPoint center = mImage.center;
            mIndicator.frame = CGRectMake(center.x - IND_SIZE, center.y - IND_SIZE, 20, 20);
            mConnectionLabel.center = mImage.center;
        }
    }
#if RENDER_BY_OPENGL
    [mImage resetDefaultPicFrame];
#endif
    return;
}

@end
