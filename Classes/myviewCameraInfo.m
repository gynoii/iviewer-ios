//
//  myviewCameraInfo.m
//  TabView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright 2010 com.debug. All rights reserved.
//

#import "myviewCameraInfo.h"


@implementation MyViewCameraInfo

@synthesize rsCentralID;
@synthesize rsLocalID;
@synthesize camCentralID;
@synthesize camLocalID;
@synthesize cameraOrder;
@synthesize cameraName;

- (id)init {
	if ((self = [super init])) {
        [self setRsCentralID:@""];
        [self setRsLocalID:@""];
        [self setCamCentralID:@""];
        [self setCamLocalID:@""];
        [self setCameraOrder:0];
        [self setCameraName:@""];
	}
	return self;
}

- (id)initWithDictionary:(NSDictionary *) dictionary {
	if ((self = [self init])) {
		[self setValuesForKeysWithDictionary:dictionary];
	}
	return self;
}

- (NSArray *) getKeys {
    NSArray * keys = [NSArray arrayWithObjects:
                      @"rsCentralID",
                      @"rsLocalID",
                      @"camCentralID",
                      @"camLocalID",
                      @"cameraOrder",
                      @"cameraName",
                      nil];
    return keys;
}

- (id)getPropertyDic {
    return [self dictionaryWithValuesForKeys:[self getKeys]];
}

- (NSString *)description {
	return [[self getPropertyDic] description];
}

- (void)dealloc {
    if ([NSNull null] != (NSNull *)rsCentralID) {
        [rsCentralID release];
        rsCentralID = nil;
    }
    if ([NSNull null] != (NSNull *)rsLocalID) {
        [rsLocalID release];
        rsLocalID = nil;
    }
    if ([NSNull null] != (NSNull *)camCentralID) {
        [camCentralID release];
        camCentralID = nil;
    }
    if ([NSNull null] != (NSNull *)camLocalID) {
        [camLocalID release];
        camLocalID = nil;
    }
    if ([NSNull null] != (NSNull *)cameraName) {
        [cameraName release];
        cameraName = nil;
    }
    [super dealloc];
}

@end
