//
//  singlecam.h
//  TabView
//
//  Created by johnlinvc on 10/02/04.
//  Copyright 2010 com.debug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PinchSlideImageView.h"
#import "MyGLView.h"
#import "ServerManager.h"
#import "PlaybackViewController.h"
#import "PlaybackMenuViewController.h"
#import "DOTableViewController.h"
#import "SlideViewMenuController.h"
#import "MyCameraListController.h"
#import "LiveViewEventListViewController.h"
#import "LiveViewEventViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "QueryRecordViewController.h" // Brosso

#define kDefaultPlaybackSpeed 1.0

#define PAGECONTROL_WIDTH 38.0
#define PAGECONTROL_HEIGHT 36.0

typedef enum E_ACTION_TYPE {
	eAct_None = -1,
	eAct_Snapshot,
	eAct_Preset,
    eAct_Audio,
    eAct_DualRecord,
    eAct_Max
} EActionType; 

typedef enum E_VIEW_MODE_TYPE {
	eViewMode_None = -1,
	eViewMode_DigitalPTZ,
	eViewMode_PhysicalPTZ,
    eViewMode_PlaybackDefault, // jerrylu, 2012/06/18
    eViewMode_Playback, // jerrylu, 2012/04/20
} EViewModeType;

typedef enum E_PLAY_TYPE {
	ePLAY_PAUSE = 0,
	ePLAY_PLAY,
	ePLAY_REVERSE,
} EPlayType; 

@class SinglePageViewController_Base;
@class ControlHelper;
@class PinchSlideImageView;
@class MyGLView;
@class PlaybackViewController; // jerrylu, 2012/04/20
@class PlaybackMenuViewController;
@class SinglePageViewController_1X1;
@class MyCameraListController;

@interface SingleCamViewController : UIViewController <UIScrollViewDelegate, PinchSlideImageViewDelegate, MyGLViewGestureDelegate,ControlHelperPresetDelegate, PlaybackMenuViewControllerDelegate, PlaybackViewControllerDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverControllerDelegate, LiveViewEventListContorlDelegate, LiveViewEventViewContorlDelegate, MyCameraListControllerDelegate> {
    
    id changeLayoutDelegate;
    
	UIScrollView * scrollView;
    UIPageControl * pagecontrol;
    UIView * flashView;
    
    int iNumberOfCamera;
	int iCurrentCamera;
    BOOL bNoChangePage;
    BOOL needChangeCams;
    NSMutableArray * viewControllers;
	SinglePageViewController_Base * parent;
    SinglePageViewController_1X1 * imageview;
	ServerManager * serverMgr;
    
    UIBarButtonItem * playbackButton;
    UIBarButtonItem * ptzButton;
    UIBarButtonItem * presetButton;
    UIBarButtonItem * audioButton;
    UIBarButtonItem * talkButton;
    UIBarButtonItem * megapixelButton;
    UIBarButtonItem * snapshotButton;
    UIBarButtonItem * speedButton;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    UIBarButtonItem * dualRecButton;
#endif
    
    IBOutlet UIToolbar * mLayoutToolbar;
    
    EProfileType eProfile;
    EActionType eActionType;
	EViewModeType eViewModeType;
    EViewModeType ePreViewModeType;
    EPlayType ePlayType;
    EPlayType ePrePlayType;
    Np_PlayerState playbackstate;
    
    bool connectstatus;
    CGPoint offsetPos;
	CGSize imgSize;
    NSTimer* hideUITimer;
    
    bool ptzCap;
	bool readyToPTZ;
    bool readyToPreset;
#if RENDER_BY_OPENGL
    float zoomLevel;
#else
    int zoomLevel;
#endif
	UIActionSheet * presetSheet;
    UIAlertController *presetController;
    NSString * stopCmd;
    
    bool audioCap; // jerrylu, 2012/04/20
    bool liveviewaudioenable;
    bool playbackaudioenable;
    bool talkCap;
    bool talkenable;
    
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/27, Pop SlideView Menu
    SlideViewMenuController *pLvMenuController;
#else
	// This variable is used to pop DI to DO table when popover dismiss not regular.
	DOTableViewController * DOTable;
#endif
    
    MyCameraListController *pLvCameraListController;
    
    // jerrylu, 2012/06/08, for iPad version
    UIPopoverController *pLiveViewMenuPopover; // jerrylu, 2012/08/27
	UIPopoverController *pPlaybackPopover;
    UIPopoverController *pPlaybackMenuPopover;
    UIPopoverController *pPlaybackSpeedPopover; // for iPad version

    PlaybackViewController *pPlaybackController; // jerrylu, 2012/04/20
    PlaybackMenuViewController *pPbMenuController;  // jerrylu, 2012/06/04
    QueryRecordViewController *pTimeLineViewController; // Brosso
    NSDate *playbackStartTime;
    NSDate *playbackEndTime;
    NSDate *playbackReverseEndTime; // jerrylu, 2012/06/13
    Np_DateTime rec1_starttime;
    Np_DateTime rec1_endtime;
    Np_DateTime rec2_starttime;
    Np_DateTime rec2_endtime;
    Np_DateTime endrevtime;
    Np_DateTime currenttime; // jerrylu, 2012/07/12
    
    NSArray *playbackSpeedList;
    NSArray *playbackSpeedValueList;
    UIPickerView *speedPicker;
    UIActionSheet *speedSheet;
    UISlider *speedSlider;
    UIImageView *speedImageView;
    float playbackSpeed;
    UIAlertView *playbackWaitAlert;
    UIAlertController *playbackWaitAlertController;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
    EDualRecordFile playbackRecordFile;
    BOOL isDualRecording;
    UIActionSheet *dualRecordSheet;
    UIAlertController *dualRecordController;
#endif
    UIActionSheet *snapshotSheet; // jerrylu, 2012/08/28
    UIAlertController *snapshotController;
    
    NSThread *m_checkLiveViewStatusThread;
    NSThread *m_checkPlaybackStatusThread;
    
    BOOL isPlaybackError;
    NSCondition *m_errorCondition;
    
    BOOL isHoldButton;
    NSTimer *audioWaitTimer;
    NSTimer *talkWaitTimer;
    NSTimer *profileWaitTimer;
    NSTimer *playbackWaitTimer;
    
    UIImageView *arrowUp;
    UIImageView *arrowDown;
    UIImageView *arrowRight;
    UIImageView *arrowLeft;
    
#if SHOW_FRAMERATE_FLAG
    NSTimer *showFRTimer;
#endif
}
@property (nonatomic, assign) id changeLayoutDelegate;

@property (nonatomic, retain) IBOutlet UIScrollView * scrollView;
@property (nonatomic, retain) UIPageControl * pagecontrol;

@property (nonatomic, assign) SinglePageViewController_Base * parent;
@property (nonatomic, assign) ServerManager * serverMgr;

@property (nonatomic, assign) bool ptzCap;
@property (nonatomic, assign) bool audioCap;
@property (nonatomic, assign) bool talkCap;

@property (nonatomic, retain) IBOutlet UIImageView *arrowUp;
@property (nonatomic, retain) IBOutlet UIImageView *arrowDown;
@property (nonatomic, retain) IBOutlet UIImageView *arrowRight;
@property (nonatomic, retain) IBOutlet UIImageView *arrowLeft;

@property (nonatomic, assign) EViewModeType eViewModeType;

- (id)initWithServerManager:(ServerManager *)smgr CurrentCam:(int)chIndex;

#if RENDER_BY_OPENGL
- (void)imageBufDidRefreshed:(MyVideoFrameBuf *) pic;
#else
- (void)imageDidRefreshed:(UIImage *) pic;
#endif
- (void)imageDidnotRefreshed;
- (void)sendPresetAtNO:(int) no;
- (void)playbackHoldScreen:(BOOL) isHoldScreen;
- (void)dismissPlaybackPopover;
- (void)dismissPlaybackMenuPopover;
- (void)dismissLiveViewMenuPopover;
- (int)getCurrentChIndex;
- (void)pbControlPause;
- (void)playbackDisconnection;
- (void)getTalkReserved:(NSString *)servername user:(NSString *)username;
- (void)getTalkSessionError;

//Brosso - time line related
- (void)pbControlSeekByDate:(NSDate*)seekDate;
- (void)pbControlPlay;
- (void)queryRecordByDate:(NSDate*)date;
@end

@protocol MySingleCamViewControllerDelegate
@optional
- (void)changeToSpecificLayout:(NSString *) layout;
- (void)backFromBackBtn:(int) ch_index;
- (void)singleViewEditBtnPressed;
@end
