//
//  SlideView.h
//  LiveView
//
//  Created by johnlinvc on 10/02/26.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "SiteListController.h"
#import "LiveViewEventListViewController.h"
#import "LiveViewEventViewController.h"
#import "MyCameraListController.h"
#import "SingleCamViewController.h"

#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/27, Pop SlideView Menu
@class SlideViewMenuController;
#else
@class DOTableViewController;
#endif
@class SingleCamViewController; // jerrylu, 2012/09/21
@class MyCameraListController;

@interface SlideView : UIViewController <UIScrollViewDelegate, ServerManagerDisplayDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, LiveViewEventListContorlDelegate, LiveViewEventViewContorlDelegate, MyCameraListControllerDelegate, MySingleCamViewControllerDelegate> {
    //check
	SiteListController * parent;
	UIScrollView * scrollView;
    ServerManager *serverMgr;
	int iNumberOfPages;
	int iCurrentPage;
	ELayoutType eLayoutType;
	BOOL pageControlUsed;
    NSMutableArray * viewControllers;
    NSMutableArray *imageDataArray;
    NSMutableArray *imageRawDataArray;
    NSMutableArray *imageUpdateStatusArray;
    NSMutableArray *imageConditionArray;
    // To be used when scrolls originate from the UIPageControl
	BOOL needChangeCams;
    
	UIPopoverController * DIDOPopover; // for iPad version
#if FUNC_PUSH_NOTIFICATION_SUPPORT // jerrylu, 2012/08/27, Pop SlideView Menu
    SlideViewMenuController *SlideMenuTable;
#else
	// This variable is used to pop DI to DO table when popover dismiss not regular.
	DOTableViewController * DOTable;
#endif
    MyCameraListController *pLvCameraListController;
    
    IBOutlet UIPageControl * pageControl;
    BOOL bNoChangePage;
    
    int iCameraOfChange;
    BOOL isFromCameraChangebyEvent;
    BOOL isFromCameraChangebySingleView;
    BOOL isLockSingleView;
    
    BOOL backFrom1x1;
    BOOL changeto1x1ByEvent;
    
    UITapGestureRecognizer *singleTap;
    UILongPressGestureRecognizer *longPress;
    UIImageView * singleMoveImage;
    int swap_begin_cindex;
    int swap_end_cindex;
    NSTimer *changePageTimer;
    
    UIBarButtonItem *saveButton;
    
#if FUNC_P2P_SAT_SUPPORT
    UIAlertView *relayAlert;
    UIAlertController *relayAlertController;
#endif
    
    UIAlertController *popAlertController;
}

//check
@property (nonatomic, retain) ServerManager * serverMgr;
@property (nonatomic, assign) SiteListController * parent;
@property (nonatomic, retain) IBOutlet UIScrollView * scrollView;
@property (nonatomic) int iNumberOfPages;
@property (nonatomic, retain) UIPopoverController * DIDOPopover;
@property (nonatomic, retain) IBOutlet UIPageControl * pageControl;
@property (assign) BOOL bNoChangePage;
@property (assign) BOOL isLockSingleView;

- (IBAction)changePage:(id) sender;
- (void)changeUILayout;
- (void)addLayoutBtn;
- (void)changeLayout;
- (void)changeLayout_2X2;
- (void)changeLayout_1X3;
- (void)changeLayout_1X2;
- (void)changeLayout_Custom;
- (void)changeLayout_2X3;
- (void)changeLayout_3X5;
- (void)changeLayout_4X6;
- (void)changeLayout_5X8;
- (id)initWithCameraNumber:(int) cam layout:(ELayoutType) layout;
- (void)reconnectCurrentPage;
- (void)stopStreamAndBackToSiteList;
- (int)getPreviewNum;
- (void)dismissPopover;
- (void)changePagebyCameraIndexbyEvent:(int) camIndex;
- (void)changePagebyCameraIndexbySingleView:(int) camIndex SingleCam:(SingleCamViewController *) singleCam;
- (UIImage *)getImageOfCamIndex:(int) index isUpdate:(BOOL*) isUpdate;
- (MyVideoFrameBuf *)getImageBufOfCamIndex:(int) index isUpdate:(BOOL*) isUpdate;
- (void)setImageUpdateOfCamIndex:(int) index flag:(BOOL) flag;
- (void)resetImageOfCamIndex:(int) index;
- (ELayoutType)getLayoutType;
@end
