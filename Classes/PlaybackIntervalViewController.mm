//
//  PlaybackIntervalViewController.m
//  iViewer
//
//  Created by Jerry Lu on 12/4/24.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import "PlaybackIntervalViewController.h"
#import "LiveViewAppDelegate.h"

@implementation PlaybackIntervalViewController

@synthesize playbackView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        playback_timeinterval = ePlaybackTimeIntervalDefault;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style withDefaultInterval:(EPlaybackTimeInterval) defaultinterval
{
    self = [super initWithStyle:style];
    if (self) {
        playback_timeinterval = defaultinterval;
    }
    return self;
}

- (void)viewDidLoad
{
    if (IS_IPAD) {
        self.preferredContentSize = CGSizeMake(PopoverSizeWidth, 360);
    }
    
	// Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController setToolbarHidden:YES animated:NO];
    [self setTitle:NSLocalizedString(@"Duration", nil)];
    
    pIntervalStringList = [[NSArray alloc] initWithObjects:NSLocalizedString(@"30 Seconds", nil), NSLocalizedString(@"1 Minute", nil), NSLocalizedString(@"2 Minutes", nil), NSLocalizedString(@"5 Minutes", nil), NSLocalizedString(@"10 Minutes", nil), nil];
    
#if 1
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(popview) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(popview)];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (IS_IPAD)
            [backButton setTintColor:COLOR_NAVBUTTONTINTPAD];
        else
            [backButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else {
        if (IS_IPAD)
            [backButton setTintColor:COLOR_NAVBUTTONTINTPADIOS6];
        else
            [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
#endif
    self.navigationItem.leftBarButtonItem = backButton;
    [backButton release];
    
    [super viewDidLoad];
}

- (void)popview {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [pIntervalStringList release];
    pIntervalStringList = nil;
    playbackView = nil;
    [super dealloc];
}

#pragma mark - Table view data source
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *hearstr = NSLocalizedString(@"Select duration", nil);
    return hearstr;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ePlaybackTimeIntervalMax;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"IntervalCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    NSInteger row = [indexPath row];
    cell.textLabel.text = [pIntervalStringList objectAtIndex:[indexPath row]];
    cell.accessoryType = (row == playback_timeinterval) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = (NSInteger)[indexPath row];
    playback_timeinterval = (EPlaybackTimeInterval)row;
    [playbackView setPlaybackTimeInterval:playback_timeinterval];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
