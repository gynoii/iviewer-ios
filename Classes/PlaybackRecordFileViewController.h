//
//  PlaybackIntervalViewController.h
//  iViewer
//
//  Created by Jerry Lu on 12/4/24.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaybackViewController.h"

@class PlaybackViewController;

@interface PlaybackRecordFileViewController : UITableViewController {
    PlaybackViewController *playbackView;
    int playback_recordfile;
}

@property (nonatomic, assign) PlaybackViewController *playbackView;

- (id)initWithStyle:(UITableViewStyle)style withDefaultRecordFile:(int) defaultrecordfile;

@end
