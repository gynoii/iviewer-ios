//
//  PresetTableViewController.h
//  LiveView
//
//  Created by johnlinvc on 10/04/02.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SingleCamViewController.h"

@class SingleCamViewController;

@interface PresetTableViewController : UITableViewController {
	NSMutableArray * presetNameArray;
	NSMutableArray * presetIdArray;
	SingleCamViewController * cam;
}

@property (nonatomic, retain) NSMutableArray * presetNameArray;
@property (nonatomic, retain) NSMutableArray * presetIdArray;
@property (nonatomic, retain) SingleCamViewController * cam;

@end
