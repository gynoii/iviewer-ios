//
//  CameraHelper.m
//  LiveView
//
//  Created by LIN YU HSIANG on 10/05/21.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
#import "LiveViewAppDelegate.h"
#import "CameraHelper.h"
#include "Utility.h"

#define LoggingPacket NO
#define DebugPacket NO
#define FORMAT(format, ...) [NSString stringWithFormat:(format), ##__VA_ARGS__]

enum cameraSteps {
	DESCRIBESTEP,
	SETUPSTEP,
	PLAYSTEP,
	TEARDOWNSTEP,
	STREAMINGASTEP,
	STREAMINGBSTEP,
	STREAMINGHEADERSTEP,
	STREAMINGACKSTEP,
	STREAMINGDATASTEP
};

struct H264Packet{//total 56 bytes
	UInt32	lpBuffer;//4
	UInt32	nLength;//4
	unsigned int dwTimestamp;//8
	bool	bSyncPoint;//1
	bool	bAutoDelete;//1
	UInt16	packetType;//2
	UInt32	seq;//4
	//SYSTEMTIME	sysTime;//should be 34byte
	//spilt into 8+8+8+8
	int timea;
	int timeb;
	int timec;
	int timed;
};

@implementation CameraHelper

@synthesize serverInfo;
@synthesize ctrlHelperDelegate;
@synthesize eProfileType;
@synthesize eMaxProfile; // jerrylu, 2012/10/16, fix bug9456
@synthesize cameraIndex;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
@synthesize eRecordFile;
#endif

#pragma mark -
#pragma mark public methods

void LvVideoHandle(Np_DateTime /*time*/,
                 char* buffer,
                 int len,
                 int width, 
                 int height, 
                 void* ctx)
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc]init];
    CameraHelper* pCamHelper = (CameraHelper*) ctx;
    if (pCamHelper->m_lvConnect == FALSE || pCamHelper->bIsDetachLiveView == TRUE) {
        [pool release];
        return;
    }
    
    UIImage* ImageA = imageFromAVPicture(buffer, width, height);
    [pCamHelper readStreamingDataResponse:ImageA];
    
    [pool release];
}

#if RENDER_BY_OPENGL
void LvRawVideoHandle(Np_DateTime /*time*/,
                   char* buffer,
                   int len,
                   int width,
                   int height,
                   void* ctx)
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc]init];
    CameraHelper* pCamHelper = (CameraHelper*) ctx;
    if (pCamHelper->m_lvConnect == FALSE || pCamHelper->bIsDetachLiveView == TRUE) {
        [pool release];
        return;
    }
    
    NSData* data = [NSData dataWithBytes:(void *)buffer length:sizeof(char)*len];
    [pCamHelper readStreamingDataResponse:data Length:len Width:width Hieght:height];
    
    [pool release];
}
#endif

void PbVideoHandle(Np_DateTime time,
                 char* buffer, 
                 int len,
                 int width, 
                 int height, 
                 void* ctx)
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc]init];
    CameraHelper* pCamHelper = (CameraHelper*) ctx;
    if (pCamHelper->m_pbConnect == FALSE) {
        [pool release];
        return;
    }
    
    UIImage* ImageA = imageFromAVPicture(buffer, width, height);
    [pCamHelper readStreamingDataResponse:ImageA];
    
    [pool release];
}

#if RENDER_BY_OPENGL
void PbRawVideoHandle(Np_DateTime time,
                   char* buffer,
                   int len,
                   int width,
                   int height,
                   void* ctx)
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc]init];
    CameraHelper* pCamHelper = (CameraHelper*) ctx;
    if (pCamHelper->m_pbConnect == FALSE) {
        [pool release];
        return;
    }
    
    NSData* data = [NSData dataWithBytes:(void *)buffer length:sizeof(char)*len];
    [pCamHelper readStreamingDataResponse:data Length:len Width:width Hieght:height];
    
    [pool release];
}
#endif

void LvAudioHandle(Np_DateTime /*time*/,
                 char* buffer, 
                 int len, 
                 int bitsPerSample, 
                 int samplesPerSec, 
                 int channels, 
                 void* ctx)
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc]init];
    // jerrylu, 2012/05/23, OpenAL to handleg audio data
    CameraHelper* pCamHelper = (CameraHelper*) ctx;
    if (pCamHelper->m_lvConnect == FALSE || pCamHelper->bIsDetachLiveView == TRUE) {
        [pool release];
        return;
    }
    
    AudioData audiobuffer;
    audiobuffer.data = (unsigned char *)buffer;
    audiobuffer.len = len;
    audiobuffer.samplerate = samplesPerSec;
    audiobuffer.bitspersample = bitsPerSample;
    audiobuffer.channels = channels;
    
    [pCamHelper readAudioStreamingDataResponse: audiobuffer];
    [pool release];
}

void PbAudioHandle(Np_DateTime time,
                   char* buffer, 
                   int len, 
                   int bitsPerSample, 
                   int samplesPerSec, 
                   int channels, 
                   void* ctx)
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc]init];
    // jerrylu, 2012/05/23, OpenAL to handleg audio data
    CameraHelper* pCamHelper = (CameraHelper*) ctx;
    if (pCamHelper->m_pbConnect == FALSE) {
        [pool release];
        return;
    }
    
    AudioData audiobuffer;
    audiobuffer.data = (unsigned char *)buffer;
    audiobuffer.len = len;
    audiobuffer.samplerate = samplesPerSec;
    audiobuffer.bitspersample = bitsPerSample;
    audiobuffer.channels = channels;
    
    [pCamHelper readAudioStreamingDataResponse: audiobuffer];
    [pool release];
}

void LvErrorHandle(Np_Error error, void* ctx)
{
    CameraHelper* pCamHelper = (CameraHelper*) ctx;
    [pCamHelper errorHandleResponse:error];
}

void PbErrorHandle(Np_Error error, void* ctx)
{
    CameraHelper* pCamHelper = (CameraHelper*) ctx;
    [pCamHelper errorHandleResponse:error];
    
    if (error == Np_ERROR_CONNECT_SUCCESS) {
        [pCamHelper setPbOpenRecordSuccess];
    }
}

- (id)init {
	if ((self = [super init])) {
		eProfileType = eNone;
        eMaxProfile = eOriginal;
        m_lvSession = NULL;
        m_lvConnect = FALSE;
        m_pbSession = NULL; // jerrylu, 20120/05/09
        m_pbConnect = FALSE;
    }
#if SHOW_FRAMERATE_FLAG
    isReceiveFirstFrame = FALSE;
    frame_rate = 0;
#endif
	return self;
}

- (void)dealloc {
    ctrlHelperDelegate = nil;
    
    [super dealloc];
}

- (void)initValue {
}

- (void)setNpDevice:(Np_Device) device {
    m_lvDevice = device;
}

- (Np_Device)getNpDevice {
    return m_lvDevice;
}

- (void)setNpDeviceExt:(Np_SubDevice_Ext)device {
    m_lvDeviceExt = device;
}

- (Np_SubDevice_Ext)getNpDeviceExt {
    return m_lvDeviceExt;
}

- (void)setPbNpDevice:(Np_Device) device {
    m_pbDevice = device;
}

- (Np_Device)getPbNpDevice {
    return m_pbDevice;
}

- (void)setPbNpDeviceExt:(Np_SubDevice_Ext) device {
    m_pbDeviceExt = device;
}

- (Np_SubDevice_Ext)getPbNpDeviceExt {
    return m_pbDeviceExt;
}

#pragma mark -
#pragma mark LiveView methods
- (void)setLvPlayer:(void*) lvPlayer {
    m_lvPlayer = lvPlayer;
}

- (Np_Result_t)liveViewStopConnection{
    Np_Result_t ret = Np_Result_OK;
    if(m_lvSession)
    {
        if (serverInfo.serverType == eServer_Crystal)
            ret = LiveView_DetachSessionExtExt( m_lvPlayer, m_lvSession);
        else
            ret = LiveView_DetachSession( m_lvPlayer, m_lvSession);
        m_lvSession = NULL;
    }
    return ret;
}

- (Np_Result_t)checkLiveViewConnection {
    Np_StreamProfile profile = kProfileNormal;
    Np_Result_t ret = Np_Result_OK;
    
    if (m_lvConnect && !m_lvSession) {
#if DEBUG_LOG
        NSLog(@"LiveView_AttachSession %d", index);
#endif
        switch (eProfileType) {
            case eLow:
                profile = kProfileLow;
                break;
            case eMinimum:
                profile = kProfileMinimum;
                break;
            case eMedium:
                profile = kProfileNormal;
                break;
            case eOriginal:
                if (eMaxProfile == eOriginal) { // jerrylu, 2012/10/16. fix bug9456
                    profile = kProfileOriginal;
                }
                else {
                    profile = kProfileNormal;
                }
                break;
            default:
                break;
        }
        
        if (serverInfo.serverType == eServer_Crystal) {
#if RENDER_BY_OPENGL
            if (profile == kProfileMinimum) {
                ret = LiveView_AttachSessionExtExt(m_lvPlayer, &m_lvSession, m_lvDeviceExt.ID, profile, kPixelFormatRGB24, LvVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
            }
            else {
                ret = LiveView_AttachSessionExtExt(m_lvPlayer, &m_lvSession, m_lvDeviceExt.ID, profile, kPixelFormatYUV420P, LvRawVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
            }
#else
            ret = LiveView_AttachSessionExtExt(m_lvPlayer, &m_lvSession, m_lvDeviceExt.ID, profile, kPixelFormatRGB24, LvVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
#endif
        }
        else {
#if RENDER_BY_OPENGL
            if (profile == kProfileMinimum) {
                ret = LiveView_AttachSessionExt(m_lvPlayer, &m_lvSession, m_lvDevice.ID, profile, kPixelFormatRGB24, LvVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
            }
            else {
                ret = LiveView_AttachSessionExt(m_lvPlayer, &m_lvSession, m_lvDevice.ID, profile, kPixelFormatYUV420P, LvRawVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
            }
#else
            ret = LiveView_AttachSessionExt(m_lvPlayer, &m_lvSession, m_lvDevice.ID, profile, kPixelFormatRGB24, LvVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
#endif
        }
        bLiveviewAudio = NO; // jerrylu, 2012/09/06
        bIsDetachLiveView = NO; // jerrylu, 2013/05/15
#if DEBUG_LOG
        NSLog(@"LiveView_AttachSession result: %d", ret);
#endif
        if (ret == Np_Result_OK) {
            connectProfileType = eProfileType;
        }
    }
    else if (!m_lvConnect && m_lvSession) {
#if DEBUG_LOG
        NSLog(@"LiveView_DetachSession %d", index);
#endif
        bIsDetachLiveView = YES; // jerrylu, 2013/05/15
        
        if (serverInfo.serverType == eServer_Crystal)
            ret = LiveView_DetachSessionExtExt(m_lvPlayer, m_lvSession);
        else
            ret = LiveView_DetachSession(m_lvPlayer, m_lvSession);
        m_lvSession = NULL;
        
        bLiveviewAudio = NO; // jerrylu, 2012/09/06
        bIsDetachLiveView = NO; // jerrylu, 2013/05/15
#if DEBUG_LOG
        NSLog(@"LiveView_DetachSession result: %d", ret);
#endif
#if SHOW_FRAMERATE_FLAG
        frame_rate = 0;
        frame_count = 0;
        isReceiveFirstFrame = FALSE;
#endif
    }
    else if (m_lvConnect && eProfileType != connectProfileType)
    {
#if DEBUG_LOG
        NSLog(@"Profile change: now:%d pre:%d", eProfileType, connectProfileType);
        NSLog(@"LiveView_DetachSession %d", index);
#endif
        bIsDetachLiveView = YES; // jerrylu, 2013/05/15

        if (serverInfo.serverType == eServer_Crystal)
            ret = LiveView_DetachSessionExtExt(m_lvPlayer, m_lvSession);
        else
            ret = LiveView_DetachSession(m_lvPlayer, m_lvSession);
        m_lvSession = NULL;
#if DEBUG_LOG
        NSLog(@"LiveView_DetachSession result: %d", ret);
#endif
        switch (eProfileType) {
            case eLow:
                profile = kProfileLow;
                break;
            case eMinimum:
                profile = kProfileMinimum;
                break;
            case eMedium:
                profile = kProfileNormal;
                break;
            case eOriginal:
                if (eMaxProfile == eOriginal) { // jerrylu, 2012/10/16. fix bug9456
                    profile = kProfileOriginal;
                }
                else {
                    profile = kProfileNormal;
                }
                break;
            default:
                break;
        }
#if DEBUG_LOG
        NSLog(@"LiveView_AttachSession %d", index);
#endif
        if (serverInfo.serverType == eServer_Crystal) {
#if RENDER_BY_OPENGL
            if (profile == kProfileMinimum) {
                ret = LiveView_AttachSessionExtExt(m_lvPlayer, &m_lvSession, m_lvDeviceExt.ID, profile, kPixelFormatRGB24, LvVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
            }
            else {
                ret = LiveView_AttachSessionExtExt(m_lvPlayer, &m_lvSession, m_lvDeviceExt.ID, profile, kPixelFormatYUV420P, LvRawVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
            }
#else
            ret = LiveView_AttachSessionExtExt(m_lvPlayer, &m_lvSession, m_lvDeviceExt.ID, profile, kPixelFormatRGB24, LvVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
#endif
        }
        else {
#if RENDER_BY_OPENGL
            if (profile == kProfileMinimum) {
                ret = LiveView_AttachSessionExt(m_lvPlayer, &m_lvSession, m_lvDevice.ID, profile, kPixelFormatRGB24, LvVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
            }
            else {
                ret = LiveView_AttachSessionExt(m_lvPlayer, &m_lvSession, m_lvDevice.ID, profile, kPixelFormatYUV420P, LvRawVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
            }
#else
            ret = LiveView_AttachSessionExt(m_lvPlayer, &m_lvSession, m_lvDevice.ID, profile, kPixelFormatRGB24, LvVideoHandle, self, LvAudioHandle, self, LvErrorHandle, self);
#endif
        }
        bIsDetachLiveView = NO; // jerrylu, 2013/05/15
#if DEBUG_LOG
        NSLog(@"LiveView_AttachSession result: %d", ret);
#endif
        // jerrylu, 2012/06/05, if user change profile between low & original, keep the audio status.
        if (ret == Np_Result_OK) { // jerrylu, 2012/09/04
            connectProfileType = eProfileType;
            if ((eProfileType != eNone && eProfileType != eMinimum) && bLiveviewAudio == YES) {
                LiveView_SetAudioOn(m_lvPlayer, m_lvSession);
            }
            else
                LiveView_SetAudioOff(m_lvPlayer);
        }
        else
        {
            m_lvSession = NULL;
        }
#if SHOW_FRAMERATE_FLAG
        frame_rate = 0;
        frame_count = 0;
        isReceiveFirstFrame = FALSE;
#endif
    }
    
    return ret;
}

- (void)liveViewConnect {
    m_lvConnect = TRUE;
}

- (void)liveViewDisconnect {
    m_lvConnect = FALSE;
}

- (BOOL)isLiveViewConnectionCreated {
    if (m_lvSession)
        return YES;
    else
        return NO;
}

// jerrylu, 2012/09/06, fix bug8410 
- (EProfileType)getConnectProfile {
    return connectProfileType;
}

#if SHOW_FRAMERATE_FLAG
- (double)getFrameRate {
    if (isReceiveFirstFrame)
    {
        NSTimeInterval timeToAlert = -[default_date timeIntervalSinceNow];
        if (timeToAlert >= 30.0)
        {
            isReceiveFirstFrame = FALSE;
            frame_count = 0;
            NSLog(@"Reset fps");
        }
        frame_rate = (double)frame_count / timeToAlert;
        
        NSLog(@"frame_count: %d time: %lf", frame_count, timeToAlert);
        NSLog(@"frame_rate: %lf", frame_rate);
    }
    return frame_rate;
}
#endif

#pragma mark -
#pragma mark LiveView Audio methods
- (BOOL)getLiveViewAudioStatus {
    if (bLiveviewAudio)
        return kAUDIO_ON;
    
    return kAUDIO_OFF;
}

- (Np_Result_t)setLiveviewAudioState:(BOOL) state {
    Np_Result_t ret = Np_Result_OK;
    if (m_lvSession) {
        if (state)
            ret = LiveView_SetAudioOn(m_lvPlayer, m_lvSession);
        else
            ret = LiveView_SetAudioOff(m_lvPlayer);
    }

    if (ret == Np_Result_OK)
        bLiveviewAudio = state;
    
    return ret;
}

#pragma mark -
#pragma mark Playback methods
- (void)setPbPlayer:(void *)pbPlayer {
    m_pbPlayer = pbPlayer;
}

- (Np_Result_t)checkPlaybackConnection {
    Np_Result_t ret = Np_Result_OK;
    
    if (m_pbConnect && !m_pbSession) {
#if DEBUG_LOG
        NSLog(@"PlayBack_AttachSession %d", index);
#endif
        if (serverInfo.serverType == eServer_Crystal) {
#if RENDER_BY_OPENGL
            ret = PlayBack_AttachSessionExtExt(m_pbPlayer, &m_pbSession, m_pbDeviceExt.ID, kPixelFormatYUV420P, PbRawVideoHandle, self, PbAudioHandle, self, PbErrorHandle, self);
#else
            ret = PlayBack_AttachSessionExtExt(m_pbPlayer, &m_pbSession, m_pbDeviceExt.ID, kPixelFormatRGB24, PbVideoHandle, self, PbAudioHandle, self, PbErrorHandle, self);
#endif
            if (m_pbSession != NULL)
                PlayBack_SetAudioOn(m_pbPlayer, m_pbSession);
            
            isPbConnectSuccess = YES;
        }
        else {
#if RENDER_BY_OPENGL
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            if (   serverInfo.serverType == eServer_MainConsole
                || serverInfo.serverType == eServer_NVRmini
                || serverInfo.serverType == eServer_NVRsolo) {
                ret = PlayBack_AttachSession1Ext(m_pbPlayer, &m_pbSession, m_pbDevice.ID, kPixelFormatYUV420P, eRecordFile, PbRawVideoHandle, self, PbAudioHandle, self, PbErrorHandle, self);
                isPbConnectSuccess = NO;
            }
            else {
#endif
                ret = PlayBack_AttachSessionExt(m_pbPlayer, &m_pbSession, m_pbDevice.ID, kPixelFormatYUV420P, PbRawVideoHandle, self, PbAudioHandle, self, PbErrorHandle, self);
                 isPbConnectSuccess = YES;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            }
#endif // FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
#else
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            if (   serverInfo.serverType == eServer_MainConsole
                || serverInfo.serverType == eServer_NVRmini
                || serverInfo.serverType == eServer_NVRsolo) {
                ret = PlayBack_AttachSession1Ext(m_pbPlayer, &m_pbSession, m_pbDevice.ID, kPixelFormatRGB24, eRecordFile, PbVideoHandle, self, PbAudioHandle, self, PbErrorHandle, self);
                 isPbConnectSuccess = NO;
            }
            else {
#endif
                ret = PlayBack_AttachSessionExt(m_pbPlayer, &m_pbSession, m_pbDevice.ID, kPixelFormatRGB24, PbVideoHandle, self, PbAudioHandle, self, PbErrorHandle, self);
                 isPbConnectSuccess = YES;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            }
#endif // FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
#endif
        }
    }
    else if (!m_pbConnect && m_pbSession) {
#if DEBUG_LOG
        NSLog(@"PlayBack_DetachSession %d", index);
#endif
        if (serverInfo.serverType == eServer_Crystal)
            ret = PlayBack_DetachSessionExtExt(m_pbPlayer, m_pbSession);
        else
            ret = PlayBack_DetachSession(m_pbPlayer, m_pbSession);
        m_pbSession = NULL;
        isPbConnectSuccess = NO;
    }
    return ret;
}

- (Np_Result_t)playbackStopConnection {
    Np_Result_t ret = Np_Result_OK;
    if(m_pbSession) {
        if (serverInfo.serverType == eServer_Crystal)
            ret = PlayBack_DetachSessionExtExt(m_pbPlayer, m_pbSession);
        else
            ret = PlayBack_DetachSession(m_pbPlayer, m_pbSession);
        m_pbSession = NULL;
        isPbConnectSuccess = NO;
    }
    return ret;
}

- (void)playbackConnect {	
    m_pbConnect = TRUE;
}

- (void)playbackDisconnect {
    m_pbConnect = FALSE;
}

- (BOOL)isPlaybackConnectionCreated {
    if (m_pbSession)
        return YES;
    else
        return NO;
}

- (void)playbackGetCurrentFrame:(Np_Frame &)frame {
    PlayBack_GetSessionCurrentImage(m_pbPlayer, m_pbSession, frame);
}

- (void)setPbOpenRecordSuccess {
    isPbConnectSuccess = YES;
}

- (BOOL)isPbOpenRecordSuccess {
    return isPbConnectSuccess;
}

#pragma mark -
#pragma mark PrivateMethods
- (void)readStreamingDataResponse:(UIImage *) img {
#if SHOW_FRAMERATE_FLAG
    if (isReceiveFirstFrame == FALSE) {
        if (default_date != nil)
            [default_date release];
        default_date = [[NSDate alloc] init];
        isReceiveFirstFrame = TRUE;
        frame_count = 0;
        frame_rate = 0;
    }
    frame_count++;
#endif
	if ([ctrlHelperDelegate respondsToSelector:@selector(cameraHelperDidRefreshImage:WithUIImage:)]) {
        [ctrlHelperDelegate cameraHelperDidRefreshImage:cameraIndex WithUIImage:img];
    }
}

- (void)readStreamingDataResponse:(NSData *)imgData Length:(int)len Width:(int)width Hieght:(int)height
{
#if SHOW_FRAMERATE_FLAG
    if (isReceiveFirstFrame == FALSE) {
        if (default_date != nil)
            [default_date release];
        default_date = [[NSDate alloc] init];
        isReceiveFirstFrame = TRUE;
        frame_count = 0;
        frame_rate = 0;
    }
    frame_count++;
#endif
    
    //NSLog(@"length:%d , w:%d, h:%d", len, width,height);
    
    if ([ctrlHelperDelegate respondsToSelector:@selector(cameraHelperDidRefreshImage:WithRaw:Length:Width:Hieght:)]) {
        [ctrlHelperDelegate cameraHelperDidRefreshImage:cameraIndex WithRaw:imgData Length:len Width:width Hieght:height];
    }
}

// jerrylu, 2012/05/23, OpenAL to handleg audio data
- (void)readAudioStreamingDataResponse:(AudioData) audiodata {
    if ([ctrlHelperDelegate respondsToSelector:@selector(cameraHelperDidRefreshAudio:WithAudio:)]) {
        [ctrlHelperDelegate cameraHelperDidRefreshAudio:cameraIndex WithAudio:audiodata];
    }
}

// jerrylu, 2012/05/23, Error Handler
- (void)errorHandleResponse:(Np_Error) error {
    if ([ctrlHelperDelegate respondsToSelector:@selector(cameraHelperErrorHandle:error:)]) {
        if (error == Np_ERROR_SESSION_LOST && bIsDetachLiveView == FALSE) {
            [ctrlHelperDelegate cameraHelperErrorHandle:cameraIndex error:(Np_Error)error];
        }
        else if (error == Np_ERROR_FATAL_ERROR || error == Np_ERROR_UNSUPPORTED_FORMAT) {
            [ctrlHelperDelegate cameraHelperErrorHandle:cameraIndex error:(Np_Error)error];
        }
        else if (error == Np_ERROR_SESSION_NODATA) {
            NSLog(@"Np_ERROR_SESSION_NODATA");
        }
    }
    
    if ([ctrlHelperDelegate respondsToSelector:@selector(cameraHelperPlaybackFeedbackHandle:)]) {
        if (error == Np_ERROR_CODE_NEXTFRAME_SUCCESS ||
            error == Np_ERROR_CODE_NEXTFRAME_FAIL ||
            error == Np_ERROR_CODE_PREVIOUSFRAME_SUCCESS ||
            error == Np_ERROR_CODE_PREVIOUSFRAME_FAIL) {
            [ctrlHelperDelegate cameraHelperPlaybackFeedbackHandle:cameraIndex];
        }
    }
}

@end
