//
//  ALplayer.mm
//  iViewer
//
//  Created by Toby Huang on 12/5/23.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import "ALplayer.h"

#define MAX_AUDIO_BUFFER_SIZE 50

@implementation ALplayer
@synthesize mContext;
@synthesize mDevice;

-(void)initOpenAL
{
    mDevice=alcOpenDevice(NULL);
    if (mDevice) {
        mContext=alcCreateContext(mDevice, NULL);
        alcMakeContextCurrent(mContext);
    }
    
    alGenSources(1, &outSourceID);
    alSpeedOfSound(1.0);
    alDopplerVelocity(1.0);
    alDopplerFactor(1.0);
    alSourcef(outSourceID, AL_PITCH, 1.0f);
    alSourcef(outSourceID, AL_GAIN, 1.0f);
    alSourcei(outSourceID, AL_LOOPING, AL_FALSE);
    alSourcef(outSourceID, AL_SOURCE_TYPE, AL_STREAMING);
    
    ticketCondition = [[NSCondition alloc] init];
    AudioBufferList = [[NSMutableArray alloc] init];
    
    [NSTimer scheduledTimerWithTimeInterval:.01
                                     target:self  
                                   selector:@selector(updataQueueBuffer) 
                                   userInfo:nil
                                    repeats:YES];
}

- (void)updateAudioDataToQueue:(AudioData) audiodata
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    [ticketCondition lock];
    
    if ([AudioBufferList count] >= MAX_AUDIO_BUFFER_SIZE) {
        [AudioBufferList removeAllObjects];
    }
    
    if (ALSampleRate != audiodata.samplerate) {
        [AudioBufferList removeAllObjects];
    }
    ALSampleRate = audiodata.samplerate;
    
    int inputALFormat = AL_FORMAT_MONO16;
    if (audiodata.bitspersample == 8)
    {
        if (audiodata.channels == 1)
            inputALFormat = AL_FORMAT_MONO8;
        else
            inputALFormat = AL_FORMAT_STEREO8;
    }
    else
    {
        if (audiodata.channels == 1)
            inputALFormat = AL_FORMAT_MONO16;
        else
            inputALFormat = AL_FORMAT_STEREO16;
    }
    if (inputALFormat != ALFormat) {
        [AudioBufferList removeAllObjects];
    }
    ALFormat = inputALFormat;
    
    NSData * tmpData = [NSData dataWithBytes:audiodata.data length:audiodata.len];
    [AudioBufferList addObject:tmpData];
    
    [ticketCondition unlock];
    
    [pool release];
    pool = nil;
}

- (void)getNextBuffer
{
    [ticketCondition lock];
    
    if ([AudioBufferList count] > 0) {
        NSData *audiodata = [AudioBufferList objectAtIndex:0];
        
        ALuint bufferID = 0;
        alGenBuffers(1, &bufferID);
        alBufferData(bufferID, ALFormat, (char*)[audiodata bytes], (ALsizei)[audiodata length], ALSampleRate);
        alSourceQueueBuffers(outSourceID, 1, &bufferID);
        
        [AudioBufferList removeObjectAtIndex:0];
    }
    
    [ticketCondition unlock];
}

- (BOOL)updataQueueBuffer
{
    if ([AudioBufferList count] <= 0) {
        return NO;
    }
    

    ALint stateVaue;
    alGetSourcei(outSourceID, AL_SOURCE_STATE, &stateVaue);
    if (stateVaue == AL_STOPPED ||
        stateVaue == AL_PAUSED || 
        stateVaue == AL_INITIAL) 
    {
        int processed, queued;
        alGetSourcei(outSourceID, AL_BUFFERS_PROCESSED, &processed);
        alGetSourcei(outSourceID, AL_BUFFERS_QUEUED, &queued);
        while(processed--)
        {
            ALuint buff;
            alSourceUnqueueBuffers(outSourceID, 1, &buff);
            alDeleteBuffers(1, &buff);
        }

        [self getNextBuffer];
        [self playSound];

        return NO;
    }

    [self getNextBuffer];
    int processed, queued;
    alGetSourcei(outSourceID, AL_BUFFERS_PROCESSED, &processed);
    alGetSourcei(outSourceID, AL_BUFFERS_QUEUED, &queued);
    while(processed--)
    {
        ALuint buff;
        alSourceUnqueueBuffers(outSourceID, 1, &buff);
        alDeleteBuffers(1, &buff);
    }

    return YES;
}

- (void)cleanQueueBuffer {
    [ticketCondition lock];
    [AudioBufferList removeAllObjects];
    [ticketCondition unlock];
    
    int processed, queued;
    alGetSourcei(outSourceID, AL_BUFFERS_PROCESSED, &processed);
    alGetSourcei(outSourceID, AL_BUFFERS_QUEUED, &queued);
    while(queued--)
    {
        ALuint buff;
        alSourceUnqueueBuffers(outSourceID, 1, &buff);
        alDeleteBuffers(1, &buff);
    }
}

-(void)playSound
{
    alSourcePlay(outSourceID);
}

- (void)stopSound {
    alSourceStop(outSourceID);  
}

- (void)cleanUpOpenALID {
    // delete the sources           
    alDeleteSources(1, &outSourceID);
}

- (void)cleanUpOpenAL {
    // destroy the context      
    alcDestroyContext(mContext);    
    // close the device     
    alcCloseDevice(mDevice);
}

-(void)dealloc
{
    [ticketCondition release];
    ticketCondition = nil;
    
    [AudioBufferList release];
    AudioBufferList = nil;
    
    [super dealloc];
}
@end
