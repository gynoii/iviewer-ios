//
//  LogoPage.m
//  iViewer
//
//  Created by Toby Huang on 12/7/18.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import "LogoPage.h"
#import "LiveViewAppDelegate.h"

@implementation LogoPage

@synthesize imageview = _imageview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

}

- (void)viewWillAppear:(BOOL)animated {
#if FUNC_LOGO_PAGE
    UIImage *logoimg;
    UIInterfaceOrientation to;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            to = UIInterfaceOrientationLandscapeLeft;
        }
        else {
            to = UIInterfaceOrientationPortrait;
        }
    }
    else {
        to = self.interfaceOrientation;
    }
    
    if (UIInterfaceOrientationIsPortrait(to)) {
        if (IS_IPAD) {
            logoimg = [UIImage imageNamed:LOGOICON_PAD_PORTRAIT];
        }
        else {
            if (IS_IPHONE_4_INCH)
                logoimg = [UIImage imageNamed:LOGOICON_PHONE_4INCH_PORTRAIT];
            else
                logoimg = [UIImage imageNamed:LOGOICON_PHONE_PORTRAIT];
        }
    }
    else {
        if (IS_IPAD) {
            logoimg = [UIImage imageNamed:LOGOICON_PAD_LANDSCAPE];
        }
        else {
            if (IS_IPHONE_4_INCH)
                logoimg = [UIImage imageNamed:LOGOICON_PHONE_4INCH_LANDSCAPE];
            else
                logoimg = [UIImage imageNamed:LOGOICON_PHONE_LANDSCAPE];
        }
    }
    
    [_imageview setImage:logoimg];
#if CUSTOMER_FOR_NORTHAMERICANCABLE
    _imageview.contentMode = UIViewContentModeScaleAspectFit;
#endif
    //_imageview.frame = [[UIScreen mainScreen] applicationFrame];
#endif
}

- (void)dealloc
{
    [_imageview release];
    _imageview = nil;
    [super dealloc];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
#if FUNC_LOGO_PAGE
    UIImage *logoimg;
    UIInterfaceOrientation to = self.interfaceOrientation;
    
    if (UIInterfaceOrientationIsPortrait(to)) {
        if (IS_IPAD) {
            logoimg = [UIImage imageNamed:LOGOICON_PAD_PORTRAIT];
        }
        else {
            if (IS_IPHONE_4_INCH)
                logoimg = [UIImage imageNamed:LOGOICON_PHONE_4INCH_PORTRAIT];
            else
                logoimg = [UIImage imageNamed:LOGOICON_PHONE_PORTRAIT];
        }
    }
    else {
        if (IS_IPAD) {
            logoimg = [UIImage imageNamed:LOGOICON_PAD_LANDSCAPE];
        }
        else {
            if (IS_IPHONE_4_INCH)
                logoimg = [UIImage imageNamed:LOGOICON_PHONE_4INCH_LANDSCAPE];
            else
                logoimg = [UIImage imageNamed:LOGOICON_PHONE_LANDSCAPE];
        }
    }
    
    [_imageview setImage:logoimg];
#endif
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
#if FUNC_LOGO_PAGE
        UIImage *logoimg;
        
        if (IS_IPAD) {
            if (size.width > size.height) {
                logoimg = [UIImage imageNamed:LOGOICON_PAD_LANDSCAPE];
            }
            else {
                logoimg = [UIImage imageNamed:LOGOICON_PAD_PORTRAIT];
            }
        }
        else {
            if (size.width > size.height) {
                if (IS_IPHONE_4_INCH)
                    logoimg = [UIImage imageNamed:LOGOICON_PHONE_4INCH_LANDSCAPE];
                else
                    logoimg = [UIImage imageNamed:LOGOICON_PHONE_LANDSCAPE];
            } else {
                if (IS_IPHONE_4_INCH)
                    logoimg = [UIImage imageNamed:LOGOICON_PHONE_4INCH_PORTRAIT];
                else
                    logoimg = [UIImage imageNamed:LOGOICON_PHONE_PORTRAIT];
            }
        }
        [_imageview setImage:logoimg];
#endif
        [self.view setNeedsLayout];
    } completion:nil];
}

@end
