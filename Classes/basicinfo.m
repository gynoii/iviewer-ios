#import "basicinfo.h"

@implementation Basicinfo

@synthesize email;
@synthesize password;
@synthesize autoLogin;

- (id)init {
	if ((self = [super init])) {
		[self setEmail:@""];
        [self setPassword:@""];
        [self setAutoLogin:FALSE];
    }
	return self;
}

- (id)initWithDictionary:(NSDictionary *) dictionary {
	if ((self = [self init])) {
		[self setValuesForKeysWithDictionary:dictionary];
	}
	return self;
}

- (NSArray *) getKeys {
    NSArray * keys = [NSArray arrayWithObjects:
                      @"email",
                      @"password",
                      @"autoLogin",
                      nil];
    return keys;
}

- (id)getPropertyDic {
    return [self dictionaryWithValuesForKeys:[self getKeys]];
}

- (NSString *)description {
	return [[self getPropertyDic] description];
}

- (void)dealloc {
    // Couldn't release directly
    if ((NSNull *)email != [NSNull null])
       [email release];
    email = nil;
    if ((NSNull *)password != [NSNull null])
        [password release];
    password = nil;
    [super dealloc];
}

@end
