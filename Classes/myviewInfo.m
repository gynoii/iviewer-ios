//
//  serverInfo.m
//  TabView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright 2010 com.debug. All rights reserved.
//

#import "myviewInfo.h"


@implementation MyViewInfo

@synthesize viewName;
@synthesize serverList;
@synthesize myViewLayoutType;

- (id)init {
	if ((self = [super init])) {
		[self setViewName:@""];
        [self setServerList:nil];
        [self setMyViewLayoutType:eLayout_2X3];
	}
	return self;
}

- (id)initWithDictionary:(NSDictionary *) dictionary {
	if ((self = [self init])) {
		[self setValuesForKeysWithDictionary:dictionary];
	}
	return self;
}

- (NSArray *) getKeys {
  NSArray * keys = [NSArray arrayWithObjects:
                     @"viewName",
					 @"serverList",
                     @"myViewLayoutType",
					 nil];
  return keys;
}

- (id)getPropertyDic {
  return [self dictionaryWithValuesForKeys:[self getKeys]];
}

- (NSString *)description {
	return [[self getPropertyDic] description];
}

- (void)dealloc {
    if ([NSNull null] != (NSNull *)viewName) {
        [viewName release];
        viewName = nil;
    }
    if ([NSNull null] != (NSNull *)serverList) {
        [serverList release];
        serverList = nil;
    }
    
    [super dealloc];
}

@end
