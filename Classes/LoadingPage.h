//
//  LoadingPage.h
//  LiveView
//
//  Created by johnlinvc on 10/03/16.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SiteListController.h"
#import "ServerManager.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

typedef enum E_Progress_Status {
    eProgressNone = -1,
    eProgressStart = 0,
    eProgressAuthenticate,
    eProgressConnect,
    eProgressConnectTunnel,
    eProgressConnectToServer,
    eProgressSuccess
} eProgressStatus;

typedef enum E_P2P_CONNECTION_ERROR {
    eP2pConnectSuccess = 0,
    eP2pConnectFailed,
    eP2pConnectIdError,
    eP2pConnectAccountError,
    eP2pConnectErrorMax
} eP2pConnectErrorCode;

@interface LoadingPage : UIViewController <ControlHelperLoadingDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate> {
	IBOutlet UIActivityIndicatorView * indicator;
    IBOutlet UILabel * progresslabel;
    IBOutlet UIProgressView * progressbar;
	SiteListController * parent;
    eProgressStatus m_progressstate;
    NSIndexPath *selectedIndex;
}

@property (nonatomic, assign) SiteListController * parent;
@property (nonatomic, retain) NSIndexPath *selectedIndex;

@end
