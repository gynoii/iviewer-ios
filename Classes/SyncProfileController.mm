//
//  SyncProfileController.m
//  iViewer
//
//  Created by NUUO on 13/8/14.
//
//

#import "SyncProfileController.h"
#import "LiveViewAppDelegate.h"
#import "serverInfo.h"
#import "myviewInfo.h"
#import "SiteInfoController.h"
#import "MyViewInfoController.h"
#import "xmlReader.h"
#import "serverInfo.h"

@implementation SyncProfileController1
@synthesize basicInfo = _basicInfo;
@synthesize parent = _parent;
@synthesize tempPath = _tempPath;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = NSLocalizedString(@"My Account", nil);
    }
    
    //Get folder path
    NSArray *pathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* path =[pathList objectAtIndex:0];
    //add filename
    path = [path stringByAppendingPathComponent:@"userData.xml"];
    
    [self setTempPath:path];

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Sync profile button
    UIImage *logoutImage;
    UIButton* syncButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (IS_IPAD)
        logoutImage = [UIImage imageNamed:@"logout_48x48_nor.png"];
    else
        logoutImage = [UIImage imageNamed:@"logout_32x32_nor.png"];
    
    syncButton.bounds = CGRectMake(0, 0, logoutImage.size.width, logoutImage.size.height);
    [syncButton setImage:logoutImage forState:UIControlStateNormal];
    [syncButton addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithCustomView:syncButton];
    // Add to button array
    NSArray * rightBarButtonArray = [[NSArray alloc] initWithObjects: logoutButton, nil];
    [[self navigationItem] setRightBarButtonItems:rightBarButtonArray];
    
    [rightBarButtonArray release];
    [logoutButton release];
    
    UIImage *backImage;
    UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPAD)
        backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
    else
        backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
    bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
    [bButton setImage:backImage forState:UIControlStateNormal];
    [bButton addTarget:self action:@selector(backPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
    self.navigationItem.leftBarButtonItem = backButton;
    [backButton release];
    
    [self.navigationController.navigationBar setBarTintColor:COLOR_NAVIGATIONBAR];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_NAVIGATIONBARTITLE}];
    [self.navigationController.toolbar setBarTintColor:COLOR_TOOLBAR];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)logout
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Are you sure to logout current account?", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                [_parent changeLoginStatus:FALSE];
                                [self.navigationController popViewControllerAnimated:NO];
                             
                                [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Close", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                        message:@"Are you sure to logout current account?"
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Close", nil)
                                              otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert setTag:0];
        [alert show];
        [alert release];
    }
}

- (void)backPreviousPage {
    [self.navigationController popViewControllerAnimated:NO];
}

- (int)ChangeServerType:(int) originalType{
    if (originalType == 2) {
        // from android Tian to iOS Titan
        return 3;
    }
    else if (originalType == 3) {
        // from android mini to iOS mini
        return 2;
    }
    
    return originalType;
}

- (ELayoutType)ChangeLayoutType:(int) originalType WithIsIOS:(bool)isIOS
{
    ELayoutType iChangType = (ELayoutType)originalType;
    
    if (isIOS == TRUE)
    { // iOS
        // do nothing
    }
    else
    { // Android
        switch (originalType)
        {
            case 1:
                // from android 1x1 to iOS 1x1
                iChangType = eLayout_1X1;
                break;
            case 2:
                // from android 1x2 to iOS 1x2
                iChangType = eLayout_1X2;
                break;
            case 3:
                // from android 1x3 to iOS 1x3
                iChangType = eLayout_1X3;
                break;
            case 4:
                // from android 2x2 to iOS 2x2
                iChangType = eLayout_2X2;
                break;
            case 5:
                // from android 2x3 to iOS 2x3
                iChangType = eLayout_2X3;
                break;
            case 6:
                // from android 3x5 to iOS 3x5
                iChangType = eLayout_3X5;
                break;
            default:
                iChangType = eLayout_3X5;
                break;
        }
    }
    
    if (IS_IPAD == FALSE)
    {
        if (   iChangType == eLayout_5X8
            || iChangType == eLayout_4X6
            || iChangType == eLayout_3X5
            || iChangType == eLayout_Custom)
        {
            iChangType = eLayout_2X3;
        }
    }
    else
    {
        if (   iChangType == eLayout_5X8
            || iChangType == eLayout_4X6
            || iChangType == eLayout_1X3
            || iChangType == eLayout_Custom)
        {
            iChangType = eLayout_2X3;
        }
    }
    
    return iChangType;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        switch (indexPath.row) {
            case 0:
            {
                cell.textLabel.text = NSLocalizedString(@"Upload Setting to the cloud", nil);
                break;
            }
            case 1:
            {
                cell.textLabel.text = NSLocalizedString(@"Download Setting to device", nil);
                break;
            }
            default:
                break;
        }
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (void) showMessage: (NSString *) content {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:content preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"OK", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                        message:content
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (BOOL)uploadData {
    NSString* boundary = @"*****";
    NSString *url = [[[NSString alloc] initWithFormat:@"http://54.251.107.11/UserAccount/nuuo/upload.php"] autorelease];
    NSURL *theURL = [NSURL URLWithString:url];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:theURL
                                                              cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                          timeoutInterval:20.0f];
    [theRequest setHTTPMethod:@"POST"];
    NSString* myMail = _basicInfo.email;
    NSString *boundaryString = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theRequest setValue:boundaryString forHTTPHeaderField:@"Content-Type"];
    [theRequest setValue:myMail forHTTPHeaderField:@"From"];
    // define boundary separator...
    NSString *boundarySeparator = [NSString stringWithFormat:@"--%@\r\n", boundary];
    
    //adding the body...
    NSMutableData *postBody = [NSMutableData data];
    
    [postBody appendData:[boundarySeparator dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploaded_file\"; filename=\"%@\"\r\n", @"userData.xml"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    NSData* binary = [NSData dataWithContentsOfFile:_tempPath options:0 error:nil];
    [postBody appendData:binary];
    
    [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary]
                          dataUsingEncoding:NSUTF8StringEncoding]];
    [theRequest setHTTPBody:postBody];
    NSString* postLen = [NSString stringWithFormat:@"%d", [postBody length]];
    [theRequest setValue:postLen forHTTPHeaderField:@"Content-Length"];
    
    NSURLResponse *response;
    NSError *error;
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
    BOOL bReuslt = FALSE;
    NSString *replyString = nil;
    if (serverReply != nil) {
        replyString = [[[NSString alloc] initWithBytes:[serverReply bytes] length:[serverReply length] encoding: NSASCIIStringEncoding] autorelease];
        
        if ([replyString rangeOfString:@"false"].location != NSNotFound)
        {
            bReuslt = TRUE;
            [self showMessage:@"upload profile success."];
        }
        else
        {
            [self showMessage:@"This Account is not exist, please try another again."];
        }
    }
    else {
        [self showMessage:@"Can not access to the cloud\nPlease connect to Internet."];
    }
    NSLog(@"reply string is :%@",replyString);
    
    return bReuslt;
}

- (BOOL)downloadData {
    NSString *url = [[NSString alloc] initWithFormat:@"http://54.251.107.11/UserAccount/nuuo/download.php?email=%@", _basicInfo.email];
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLResponse *response;
    NSError *error;
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    NSString *replyString;
    if (serverReply != nil)
    {
        if ([serverReply length] == 0)
        {
            [self showMessage:@"No data, please try to sync the data again."];
        }
        else
        {
            replyString = [[[NSString alloc] initWithBytes:[serverReply bytes] length:[serverReply length] encoding: NSASCIIStringEncoding] autorelease];
            [self ParseXMLFile:replyString];
            [_parent save];
            [_parent.tableView reloadData];
            [self showMessage:@"Download setting Susccessfully!"];
        }
    }
    else
    {
        [self showMessage:@"Can not access to the internet, please check the Internet status."];
        
        return FALSE;
    }
   
    return TRUE;
}

- (NSString*) removeChar:(NSString*) original {
    original = [original stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    original = [original stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    return original;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Are you sure to upload current settings to cloud?", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"OK", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self GenerateXMLFile];
                                         [self uploadData];
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"Close", nil)
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                         }];
                [alert addAction:cancel];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                                message:@"Are you sure to upload current settings to cloud?"
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"Close", nil)
                                                      otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                [alert setTag:1];
                [alert show];
                [alert release];
            }

            break;
        }
        case 1:
        {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning Message", nil) message:NSLocalizedString(@"Are you sure to download cloud settings to current device?", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"OK", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self downloadData];
                                         
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:NSLocalizedString(@"Close", nil)
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                         }];
                [alert addAction:cancel];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning Message", nil)
                                                                message:@"Are you sure to download cloud settings to current device?"
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"Close", nil)
                                                      otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                [alert setTag:2];
                [alert show];
                [alert release];
            }

            break;
        }
        default:
            break;
    }
}

- (void)ParseXMLFile:(NSString*) body {
    NSDictionary* dict = [[NSDictionary alloc] initWithDictionary:[XMLReader dictionaryForXMLString:body error:nil]];
 
    NSString* platformStr = [[[dict valueForKey:@"Config"] valueForKey:@"Platform"] valueForKey:@"text"];
    int iPlatform = [[self removeChar:platformStr] intValue];
    
    // My server
    NSMutableArray * serverList = [[NSMutableArray alloc] init];
    NSArray *myServer = [[[dict valueForKey:@"Config"] valueForKey:@"MyServer"] valueForKey:@"ServerInfo"];
    BOOL bSingleObj = FALSE;
    if ([myServer isKindOfClass:[NSDictionary class]])
    {
        bSingleObj = TRUE;
    }
    
    // Parse Single obj
    if (bSingleObj == TRUE) {
        ServerInfo *sinfo = [[[ServerInfo alloc] init] autorelease];
        int connectType = [[[myServer valueForKey:@"ConnectionType"] valueForKey:@"text"] intValue];
        
        NSString* platformStr = [[[dict valueForKey:@"Config"] valueForKey:@"Platform"] valueForKey:@"text"];
        int iPlatform = [[self removeChar:platformStr] intValue];
        
        if (connectType == 0)
        {
            sinfo.connectionType = 0;
            sinfo.serverName = [[myServer valueForKey:@"Name"] valueForKey:@"text"];
            sinfo.serverName = [self removeChar:sinfo.serverName];
            sinfo.serverIP = [[myServer valueForKey:@"Host"] valueForKey:@"text"];
            sinfo.serverIP = [self removeChar:sinfo.serverIP];
            sinfo.userName = [[myServer valueForKey:@"UserName"] valueForKey:@"text"];
            sinfo.userName = [self removeChar:sinfo.userName];
            sinfo.userPassword = [[myServer valueForKey:@"Password"] valueForKey:@"text"];
            sinfo.userPassword = [self removeChar:sinfo.userPassword];
            sinfo.serverPort = [[myServer valueForKey:@"LivePort"] valueForKey:@"text"];
            sinfo.serverPort = [self removeChar:sinfo.serverPort];
            sinfo.serverPlaybackPort = [[myServer valueForKey:@"PlaybackPort"] valueForKey:@"text"];
            sinfo.serverPlaybackPort = [self removeChar:sinfo.serverPlaybackPort];
        }
        else
        {
            sinfo.connectionType = 1;
            sinfo.serverName_P2P = [[myServer valueForKey:@"P2pServerName"] valueForKey:@"text"];
            sinfo.serverName_P2P = [self removeChar:sinfo.serverName_P2P];
            sinfo.serverID_P2P = [[myServer valueForKey:@"P2pServerID"] valueForKey:@"text"];
            sinfo.serverID_P2P = [self removeChar:sinfo.serverID_P2P];
            sinfo.userName_P2P = [[myServer valueForKey:@"P2pUserName"] valueForKey:@"text"];
            sinfo.userName_P2P = [self removeChar:sinfo.userName_P2P];
            sinfo.userPassword_P2P = [[myServer valueForKey:@"P2pUserPasswd"] valueForKey:@"text"];
            sinfo.userPassword_P2P = [self removeChar:sinfo.userPassword_P2P];
        }
        sinfo.serverID = [[myServer valueForKey:@"ServerId"] valueForKey:@"text"];
        sinfo.serverID = [self removeChar:sinfo.serverID];
        
        NSString* pushStr = [[myServer valueForKey:@"PushNotificationEnable"] valueForKey:@"text"];
        sinfo.serverPushNotificationEnable = [[self removeChar:pushStr] boolValue];
        
        NSString* typeStr = [[myServer valueForKey:@"ServerType"] valueForKey:@"text"];
        sinfo.serverType = (EServerType)[[self removeChar:typeStr] intValue];
        if (iPlatform == 1) { // iOS
            // do nothing
        }
        else { // Android
            sinfo.serverType = (EServerType) [self ChangeServerType:sinfo.serverType];
        }
        
        NSString* layoutStr = [[myServer valueForKey:@"ServerLayout"] valueForKey:@"text"];
        sinfo.layoutType = [self ChangeLayoutType:[[self removeChar:layoutStr] intValue] WithIsIOS:iPlatform == 1];
        [serverList addObject:sinfo];
    }
    // Parse mulitple obj
    else {
        for (int i = 0; i < myServer.count; i++)
        {
            ServerInfo *sinfo = [[[ServerInfo alloc] init] autorelease];
            NSString* connectTypeStr = [[[myServer objectAtIndex:i] valueForKey:@"ConnectionType"] valueForKey:@"text"];
            int connectType = [[self removeChar:connectTypeStr] intValue];
            if (connectType == 0)
            {
                sinfo.connectionType = 0;
                sinfo.serverName = [[[myServer objectAtIndex:i] valueForKey:@"Name"] valueForKey:@"text"];
                sinfo.serverName = [self removeChar:sinfo.serverName];
                sinfo.serverIP = [[[myServer objectAtIndex:i] valueForKey:@"Host"] valueForKey:@"text"];
                sinfo.serverIP = [self removeChar:sinfo.serverIP];
                sinfo.userName = [[[myServer objectAtIndex:i] valueForKey:@"UserName"] valueForKey:@"text"];
                sinfo.userName = [self removeChar:sinfo.userName];
                sinfo.userPassword = [[[myServer objectAtIndex:i] valueForKey:@"Password"] valueForKey:@"text"];
                sinfo.userPassword = [self removeChar:sinfo.userPassword];
                sinfo.serverPort = [[[myServer objectAtIndex:i] valueForKey:@"LivePort"] valueForKey:@"text"];
                sinfo.serverPort = [self removeChar:sinfo.serverPort];
                sinfo.serverPlaybackPort = [[[myServer objectAtIndex:i] valueForKey:@"PlaybackPort"] valueForKey:@"text"];
                sinfo.serverPlaybackPort = [self removeChar:sinfo.serverPlaybackPort];
            }
            else
            {
                sinfo.connectionType = 1;
                sinfo.serverName_P2P = [[[myServer objectAtIndex:i] valueForKey:@"P2pServerName"] valueForKey:@"text"];
                sinfo.serverName_P2P = [self removeChar:sinfo.serverName_P2P];
                sinfo.serverID_P2P = [[[myServer objectAtIndex:i] valueForKey:@"P2pServerID"] valueForKey:@"text"];
                sinfo.serverID_P2P = [self removeChar:sinfo.serverID_P2P];
                sinfo.userName_P2P = [[[myServer objectAtIndex:i] valueForKey:@"P2pUserName"] valueForKey:@"text"];
                sinfo.userName_P2P = [self removeChar:sinfo.userName_P2P];
                sinfo.userPassword_P2P = [[[myServer objectAtIndex:i] valueForKey:@"P2pUserPasswd"] valueForKey:@"text"];
                sinfo.userPassword_P2P = [self removeChar:sinfo.userPassword_P2P];
            }
            sinfo.serverID = [[[myServer objectAtIndex:i] valueForKey:@"ServerId"] valueForKey:@"text"];
            sinfo.serverID = [self removeChar:sinfo.serverID];
            
            NSString* pushStr = [[[myServer objectAtIndex:i] valueForKey:@"PushNotificationEnable"] valueForKey:@"text"];
            sinfo.serverPushNotificationEnable = [[self removeChar:pushStr] boolValue];
        
            NSString* typeStr = [[[myServer objectAtIndex:i] valueForKey:@"ServerType"] valueForKey:@"text"];
            sinfo.serverType = (EServerType)[[self removeChar:typeStr] intValue];
            if (iPlatform == 1) { // iOS
                // do nothing
            }
            else { // Android
                sinfo.serverType = (EServerType) [self ChangeServerType:sinfo.serverType];
            }
        
            NSString* layoutStr = [[[myServer objectAtIndex:i] valueForKey:@"ServerLayout"] valueForKey:@"text"];
            sinfo.layoutType = [self ChangeLayoutType:[[self removeChar:layoutStr] intValue] WithIsIOS:iPlatform == 1];
            [serverList addObject:sinfo];
        }
    }
    
    // My view
    NSMutableArray * myViewist = [[NSMutableArray alloc] init];
    NSArray *myView = [[[dict valueForKey:@"Config"] valueForKey:@"MyView"] valueForKey:@"MyViewInfo"];
    bSingleObj = FALSE;
    if ([myView isKindOfClass:[NSDictionary class]])
    {
        bSingleObj = TRUE;
    }
    
    if (bSingleObj == TRUE) { // single viewinfo
        MyViewInfo *viewinfo = [[[MyViewInfo alloc] init] autorelease];
        viewinfo.viewName = [[myView valueForKey:@"MyViewName"] valueForKey:@"text"];
        viewinfo.viewName = [self removeChar:viewinfo.viewName];
        NSString* layoutStr = [[myView valueForKey:@"MyViewLayout"] valueForKey:@"text"];
        viewinfo.myViewLayoutType = [self ChangeLayoutType:[[self removeChar:layoutStr] intValue] WithIsIOS:iPlatform == 1];
        NSArray *myCam = [myView valueForKey:@"MyViewCameraInfo"];
        
        BOOL bCamSingleObj = FALSE;
        if ([myCam isKindOfClass:[NSDictionary class]])
        {
            bCamSingleObj = TRUE;
        }
        
        if (bCamSingleObj == TRUE) {
            NSMutableArray * cameraInfoList = [[[NSMutableArray alloc] init] autorelease];
            MyViewCameraInfo *vcinfo = [[[MyViewCameraInfo alloc] init] autorelease];
            vcinfo.rsCentralID = [[[myCam valueForKey:@"RecordingServerID"] valueForKey:@"RSCentralID"] valueForKey:@"text"];
            vcinfo.rsCentralID = [self removeChar:vcinfo.rsCentralID];
            vcinfo.rsLocalID = [[[myCam valueForKey:@"RecordingServerID"] valueForKey:@"RSLocalID"] valueForKey:@"text"];
            vcinfo.rsLocalID = [self removeChar:vcinfo.rsLocalID];
            vcinfo.camCentralID = [[[myCam valueForKey:@"CameraID"] valueForKey:@"CameraCentralID"] valueForKey:@"text"];
            vcinfo.camCentralID = [self removeChar:vcinfo.camCentralID];
            vcinfo.camLocalID = [[[myCam valueForKey:@"CameraID"] valueForKey:@"CameraLocalID"] valueForKey:@"text"];
            vcinfo.camLocalID = [self removeChar:vcinfo.camLocalID];
            vcinfo.cameraName = [[myCam valueForKey:@"MyViewCameraName"] valueForKey:@"text"];
            vcinfo.cameraName = [self removeChar:vcinfo.cameraName];
            NSString* typeStr = [[myCam valueForKey:@"CameraOrder"] valueForKey:@"text"];
            vcinfo.cameraOrder = [[self removeChar:typeStr] intValue];
            [cameraInfoList addObject:vcinfo];
            
            NSMutableArray * serverInfoList = [[[NSMutableArray alloc] init] autorelease];
            MyViewServerInfo *vsinfo = [[[MyViewServerInfo alloc] init] autorelease];
            vsinfo.serverID = [[myCam valueForKey:@"ManageServerID"] valueForKey:@"text"];
            vsinfo.serverID = [self removeChar:vsinfo.serverID];
            vsinfo.userName = [[myCam valueForKey:@"UserNameOfServer"] valueForKey:@"text"];
            vsinfo.userName = [self removeChar:vsinfo.userName];
            
            [vsinfo setCameraList:cameraInfoList];
            [serverInfoList addObject:vsinfo];
            
            [viewinfo setServerList:serverInfoList];
            [myViewist addObject:viewinfo];
        }
        else {
            // Get server count
            NSMutableArray * serverInfoList = [[[NSMutableArray alloc] init] autorelease];
            for (int j = 0; j < myCam.count; j++)
            {
                NSString* tempID = [[[myCam objectAtIndex:j] valueForKey:@"ManageServerID"] valueForKey:@"text"];
                tempID = [self removeChar:tempID];
                BOOL bFind = FALSE;
                for (int idIdx = 0; idIdx < [serverInfoList count]; idIdx++)
                {
                    // Do not count the same server ID object
                    MyViewServerInfo *vsinfo = [serverInfoList objectAtIndex:idIdx];
                    if ([tempID isEqualToString:vsinfo.serverID] == TRUE) {
                        bFind = TRUE;
                    }
                }
                
                if (bFind == TRUE) {
                    continue;
                }
                MyViewServerInfo *vsinfo = [[[MyViewServerInfo alloc] init] autorelease];
                vsinfo.serverID = [[[myCam objectAtIndex:j] valueForKey:@"ManageServerID"] valueForKey:@"text"];
                vsinfo.serverID = [self removeChar:vsinfo.serverID];
                vsinfo.userName = [[[myCam objectAtIndex:j] valueForKey:@"UserNameOfServer"] valueForKey:@"text"];
                vsinfo.userName = [self removeChar:vsinfo.userName];
                NSMutableArray * cameraInfoList = [[[NSMutableArray alloc] init] autorelease];
                [vsinfo setCameraList:cameraInfoList];
                [serverInfoList addObject:vsinfo];
            }
            [viewinfo setServerList:serverInfoList];
            
            for (int j = 0; j < myCam.count; j++)
            {
                MyViewCameraInfo *vcinfo = [[[MyViewCameraInfo alloc] init] autorelease];
                vcinfo.rsCentralID = [[[[myCam objectAtIndex:j] valueForKey:@"RecordingServerID"] valueForKey:@"RSCentralID"] valueForKey:@"text"];
                vcinfo.rsCentralID = [self removeChar:vcinfo.rsCentralID];
                vcinfo.rsLocalID = [[[[myCam objectAtIndex:j] valueForKey:@"RecordingServerID"] valueForKey:@"RSLocalID"] valueForKey:@"text"];
                vcinfo.rsLocalID = [self removeChar:vcinfo.rsLocalID];
                vcinfo.camCentralID = [[[[myCam objectAtIndex:j] valueForKey:@"CameraID"] valueForKey:@"CameraCentralID"] valueForKey:@"text"];
                vcinfo.camCentralID = [self removeChar:vcinfo.camCentralID];
                vcinfo.camLocalID = [[[[myCam objectAtIndex:j] valueForKey:@"CameraID"] valueForKey:@"CameraLocalID"] valueForKey:@"text"];
                vcinfo.camLocalID = [self removeChar:vcinfo.camLocalID];
                vcinfo.cameraName = [[[myCam objectAtIndex:j] valueForKey:@"MyViewCameraName"] valueForKey:@"text"];
                vcinfo.cameraName = [self removeChar:vcinfo.cameraName];
                NSString* typeStr = [[[myCam objectAtIndex:j] valueForKey:@"CameraOrder"] valueForKey:@"text"];
                vcinfo.cameraOrder = [[self removeChar:typeStr] intValue];
                
                NSString* tempID = [[[myCam objectAtIndex:j] valueForKey:@"ManageServerID"] valueForKey:@"text"];
                tempID = [self removeChar:tempID];
                for (int idIdx = 0; idIdx < [serverInfoList count]; idIdx++)
                {
                    MyViewServerInfo *vsinfo = [serverInfoList objectAtIndex:idIdx];
                    if ([tempID isEqualToString:vsinfo.serverID] == TRUE) {
                        [vsinfo.cameraList addObject:vcinfo];
                        break;
                    }
                }
            }
            [myViewist addObject:viewinfo];
        }
    }
    else { // multiple viewinfo
        for (int i = 0; i < myView.count; i++)
        {
            MyViewInfo *viewinfo = [[[MyViewInfo alloc] init] autorelease];
            viewinfo.viewName = [[[myView objectAtIndex:i] valueForKey:@"MyViewName"] valueForKey:@"text"];
            viewinfo.viewName = [self removeChar:viewinfo.viewName];
            NSString* layoutStr = [[[myView objectAtIndex:i] valueForKey:@"MyViewLayout"] valueForKey:@"text"];
            viewinfo.myViewLayoutType = [self ChangeLayoutType:[[self removeChar:layoutStr] intValue] WithIsIOS:iPlatform == 1];
            NSArray *myCam = [[myView objectAtIndex:i] valueForKey:@"MyViewCameraInfo"];
            
            BOOL bCamSingleObj = FALSE;
            if ([myCam isKindOfClass:[NSDictionary class]])
            {
                bCamSingleObj = TRUE;
            }

            if (bCamSingleObj == TRUE) {
                NSMutableArray * cameraInfoList = [[[NSMutableArray alloc] init] autorelease];
                MyViewCameraInfo *vcinfo = [[[MyViewCameraInfo alloc] init] autorelease];
                vcinfo.rsCentralID = [[[myCam valueForKey:@"RecordingServerID"] valueForKey:@"RSCentralID"] valueForKey:@"text"];
                vcinfo.rsCentralID = [self removeChar:vcinfo.rsCentralID];
                vcinfo.rsLocalID = [[[myCam valueForKey:@"RecordingServerID"] valueForKey:@"RSLocalID"] valueForKey:@"text"];
                vcinfo.rsLocalID = [self removeChar:vcinfo.rsLocalID];
                vcinfo.camCentralID = [[[myCam valueForKey:@"CameraID"] valueForKey:@"CameraCentralID"] valueForKey:@"text"];
                vcinfo.camCentralID = [self removeChar:vcinfo.camCentralID];
                vcinfo.camLocalID = [[[myCam valueForKey:@"CameraID"] valueForKey:@"CameraLocalID"] valueForKey:@"text"];
                vcinfo.camLocalID = [self removeChar:vcinfo.camLocalID];
                vcinfo.cameraName = [[myCam valueForKey:@"MyViewCameraName"] valueForKey:@"text"];
                vcinfo.cameraName = [self removeChar:vcinfo.cameraName];
                NSString* typeStr = [[myCam valueForKey:@"CameraOrder"] valueForKey:@"text"];
                vcinfo.cameraOrder = [[self removeChar:typeStr] intValue];
                
                [cameraInfoList addObject:vcinfo];
                
                NSMutableArray * serverInfoList = [[[NSMutableArray alloc] init] autorelease];
                MyViewServerInfo *vsinfo = [[[MyViewServerInfo alloc] init] autorelease];
                vsinfo.serverID = [[myCam valueForKey:@"ManageServerID"] valueForKey:@"text"];
                vsinfo.userName = [[myCam valueForKey:@"UserNameOfServer"] valueForKey:@"text"];
                
                [vsinfo setCameraList:cameraInfoList];
                [serverInfoList addObject:vsinfo];
                
                [viewinfo setServerList:serverInfoList];
                [myViewist addObject:viewinfo];
            }
            else {
                
                // Get server count
                NSMutableArray * serverInfoList = [[[NSMutableArray alloc] init] autorelease];
                for (int j = 0; j < myCam.count; j++)
                {
                    NSString* tempID = [[[myCam objectAtIndex:j] valueForKey:@"ManageServerID"] valueForKey:@"text"];
                    tempID = [self removeChar:tempID];
                    BOOL bFind = FALSE;
                    for (int idIdx = 0; idIdx < [serverInfoList count]; idIdx++)
                    {
                        // Do not count the same server ID object
                        MyViewServerInfo *vsinfo = [serverInfoList objectAtIndex:idIdx];
                        if ([tempID isEqualToString:vsinfo.serverID] == TRUE) {
                            bFind = TRUE;
                        }
                    }
                    
                    if (bFind == TRUE) {
                        continue;
                    }
                    MyViewServerInfo *vsinfo = [[[MyViewServerInfo alloc] init] autorelease];
                    vsinfo.serverID = [[[myCam objectAtIndex:j] valueForKey:@"ManageServerID"] valueForKey:@"text"];
                    vsinfo.serverID = [self removeChar:vsinfo.serverID];
                    vsinfo.userName = [[[myCam objectAtIndex:j] valueForKey:@"UserNameOfServer"] valueForKey:@"text"];
                    vsinfo.userName = [self removeChar:vsinfo.userName];
                    NSMutableArray * cameraInfoList = [[[NSMutableArray alloc] init] autorelease];
                    [vsinfo setCameraList:cameraInfoList];
                    [serverInfoList addObject:vsinfo];
                }
                [viewinfo setServerList:serverInfoList];
                
                for (int j = 0; j < myCam.count; j++)
                {
                    MyViewCameraInfo *vcinfo = [[[MyViewCameraInfo alloc] init] autorelease];
                    vcinfo.rsCentralID = [[[[myCam objectAtIndex:j] valueForKey:@"RecordingServerID"] valueForKey:@"RSCentralID"] valueForKey:@"text"];
                    vcinfo.rsCentralID = [self removeChar:vcinfo.rsCentralID];
                    vcinfo.rsLocalID = [[[[myCam objectAtIndex:j] valueForKey:@"RecordingServerID"] valueForKey:@"RSLocalID"] valueForKey:@"text"];
                    vcinfo.rsLocalID = [self removeChar:vcinfo.rsLocalID];
                    vcinfo.camCentralID = [[[[myCam objectAtIndex:j] valueForKey:@"CameraID"] valueForKey:@"CameraCentralID"] valueForKey:@"text"];
                    vcinfo.camCentralID = [self removeChar:vcinfo.camCentralID];
                    vcinfo.camLocalID = [[[[myCam objectAtIndex:j] valueForKey:@"CameraID"] valueForKey:@"CameraLocalID"] valueForKey:@"text"];
                    vcinfo.camLocalID = [self removeChar:vcinfo.camLocalID];
                    vcinfo.cameraName = [[[myCam objectAtIndex:j] valueForKey:@"MyViewCameraName"] valueForKey:@"text"];
                    vcinfo.cameraName = [self removeChar:vcinfo.cameraName];
                    NSString* typeStr = [[[myCam objectAtIndex:j] valueForKey:@"CameraOrder"] valueForKey:@"text"];
                    vcinfo.cameraOrder = [[self removeChar:typeStr] intValue];
                    
                    NSString* tempID = [[[myCam objectAtIndex:j] valueForKey:@"ManageServerID"] valueForKey:@"text"];
                    tempID = [self removeChar:tempID];
                    for (int idIdx = 0; idIdx < [serverInfoList count]; idIdx++)
                    {
                        MyViewServerInfo *vsinfo = [serverInfoList objectAtIndex:idIdx];
                        if ([tempID isEqualToString:vsinfo.serverID] == TRUE) {
                            [vsinfo.cameraList addObject:vcinfo];
                            break;
                        }
                    }
                }
                [myViewist addObject:viewinfo];
            }
        }
    }
    NSLog(@"%@", myViewist);

    [_parent clearandSaveData:serverList with:myViewist];
}

- (NSString*) GenerateMyView {
    NSMutableString* body = [[NSMutableString alloc] init];
    [body appendString:@"<MyView>\n"];
    int index = 0;
    
    for (MyViewInfo * info in [_parent displayedMyView]) {
        [body appendString:@"<MyViewInfo>\n"];
        [body appendFormat:@"<MyViewIndex>%d</MyViewIndex>\n", index++];
        [body appendFormat:@"<MyViewName>%@</MyViewName>\n", info.viewName];
        for (MyViewServerInfo * sinfo in [info serverList]) {
            for (MyViewCameraInfo * cinfo in [sinfo cameraList]) {
                [body appendString:@"<MyViewCameraInfo>\n"];
                [body appendFormat:@"<MyViewCameraName>%@</MyViewCameraName>\n", cinfo.cameraName];
                [body appendString:@"<RecordingServerID>\n"];
                [body appendFormat:@"<RSCentralID>%@</RSCentralID>\n", cinfo.rsCentralID];
                [body appendFormat:@"<RSLocalID>%@</RSLocalID>\n", cinfo.rsLocalID];
                [body appendString:@"</RecordingServerID>\n"];
                [body appendString:@"<CameraID>\n"];
                [body appendFormat:@"<CameraCentralID>%@</CameraCentralID>\n", cinfo.camCentralID];
                [body appendFormat:@"<CameraLocalID>%@</CameraLocalID>\n", cinfo.camLocalID];
                [body appendString:@"</CameraID>\n"];
                [body appendFormat:@"<CameraOrder>%d</CameraOrder>\n", cinfo.cameraOrder];
                [body appendFormat:@"<UserNameOfServer>%@</UserNameOfServer>\n", sinfo.userName];
                [body appendFormat:@"<ManageServerID>%@</ManageServerID>\n", sinfo.serverID];
                [body appendString:@"</MyViewCameraInfo>\n"];
            }
        }
        [body appendFormat:@"<MyViewLayout>%u</MyViewLayout>\n", info.myViewLayoutType];
        [body appendString:@"</MyViewInfo>\n"];
    }
    [body appendString:@"</MyView>\n"];
    
    return [body autorelease];
}

- (NSString*) GenerateMyServer {
    NSMutableString* body = [[NSMutableString alloc] init];
    [body appendString:@"<MyServer>\n"];
    int index = 0;
    for (ServerInfo *info in [_parent displayedObjects]) {
        [body appendString:@"<ServerInfo>\n"];
        [body appendFormat:@"<Index>%d</Index>\n", index++];
        if (info.connectionType == eIPConnection) {
            [body appendFormat:@"<Name>%@</Name>\n", info.serverName];
            [body appendFormat:@"<Host>%@</Host>\n", info.serverIP];
            [body appendFormat:@"<UserName>%@</UserName>\n", info.userName];
            [body appendFormat:@"<Password>%@</Password>\n", info.userPassword];
            [body appendFormat:@"<LivePort>%@</LivePort>\n", info.serverPort];
            [body appendFormat:@"<PlaybackPort>%@</PlaybackPort>\n", info.serverPlaybackPort];
            [body appendString:@"<P2pServerName />\n<P2pServerID />\n<P2pUserName />\n<P2pUserPasswd />\n<ConnectionType>0</ConnectionType>\n"];
            
        }
        else if (info.connectionType == eP2PConnection) {
            [body appendString:@"<Name />\n<Host />\n<UserName />\n<Password />\n<LivePort />\n<PlaybackPort />\n"];
            [body appendFormat:@"<P2pServerName>%@</P2pServerName>\n", info.serverName_P2P];
            [body appendFormat:@"<P2pServerID>%@</P2pServerID>\n", info.serverID_P2P];
            [body appendFormat:@"<P2pUserName>%@</P2pUserName>\n", info.userName_P2P];
            [body appendFormat:@"<P2pUserPasswd>%@</P2pUserPasswd>\n", info.userPassword_P2P];
            [body appendString:@"<ConnectionType>1</ConnectionType>\n"];
        }
        [body appendFormat:@"<ServerId>%@</ServerId>\n", info.serverID];
        if (info.serverPushNotificationEnable == kPushNotificationEnable) {
            [body appendString:@"<PushNotificationEnable>true</PushNotificationEnable>\n"];
        }
        else {
            [body appendString:@"<PushNotificationEnable>false</PushNotificationEnable>\n"];
        }
        
        [body appendFormat:@"<ServerType>%u</ServerType>\n", info.serverType];
        [body appendFormat:@"<ServerLayout>%u</ServerLayout>\n", info.layoutType];
        [body appendString:@"</ServerInfo>\n"];
    }
    
    [body appendString:@"</MyServer>\n"];
    
    return [body autorelease];
}

- (void)GenerateXMLFile {
    NSMutableString* body = [[[NSMutableString alloc] init] autorelease];
    [body appendString:@"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n"];
    NSString* myServer = [self GenerateMyServer];
    NSString* myView = [self GenerateMyView];
    [body appendFormat:@"<Config><Platform>%d</Platform>\n%@%@</Config>\n", eiOS, myServer, myView];
    
    NSLog(@"%@", body);
    
        // Test file is exist or not
    if ([[NSFileManager defaultManager] fileExistsAtPath:_tempPath]) {
        // Delete file
        [[NSFileManager defaultManager] removeItemAtPath:_tempPath error:nil];
    }
    // write to file
    [body writeToFile:_tempPath atomically:NO encoding:NSUTF8StringEncoding error:nil];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == 0)
    {
        if (buttonIndex == 1)
        {
            [_parent changeLoginStatus:FALSE];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
    else if ([alertView tag] == 1)
    {
        if (buttonIndex == 1)
        {
            [self GenerateXMLFile];
            [self uploadData];
        }
    }
    else if ([alertView tag] == 2)
    {
        if (buttonIndex == 1)
        {
            [self downloadData];
        }
    }
}

@end
