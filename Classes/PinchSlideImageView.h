//
//  PinchSlideImageView.h
//  pinchSlide
//
//  Created by johnlinvc on 10/03/03.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum E_SlideDirection {
	eSlideTiltUp,
	eSlideTiltDown,
	eSlidePanLeft,
	eSlidePanRight,
	eSlideUpRight,
	eSlideUpLeft,
	eSlideDownLeft,
	eSlideDownRight,
	eSlideHome,
} eSlideDir;

@protocol PinchSlideImageViewDelegate;

@interface PinchSlideImageView : UIImageView {
	id mDelegate;
	int mode;
	BOOL isUsed;
	CGFloat initDistance;
	CGPoint initPosition;
}
- (CGFloat)distanceBetweenTwoPoints:(CGPoint)fromPoint toPoint:(CGPoint)toPoint;
@property (nonatomic,assign) id delegate;
@end

@protocol PinchSlideImageViewDelegate

@optional
- (void) onPSImageView:(PinchSlideImageView *) view SlideOnDirection:(int) dir offset:(CGPoint) offsetValue;
- (void) onPSImageView: (PinchSlideImageView*)view Zooming: (BOOL) isZoomIn;
- (void) onPSImageView: (PinchSlideImageView*)view Zooming: (BOOL) isZoomIn Center:(CGPoint) center;
- (void) onPSImageViewDidBeginTouch:(PinchSlideImageView *)view;
- (void) onPSImageViewDoubleTap:(PinchSlideImageView *) view;
- (void) onPSImageViewSingleTap:(PinchSlideImageView *) view;
- (void) onPSImageViewDidEndTouch:(PinchSlideImageView*)view ;

@end
