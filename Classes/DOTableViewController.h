//
//  DOTableViewController.h
//  LiveView
//
//  Created by johnlinvc on 10/05/06.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"

@class SlideView;
@class SingleCamViewController;

@interface DOTableViewController : UITableViewController {
    id parent;
    ServerManager * serverMgr;
    NSThread* doThread;

	NSMutableArray * stateArray;
	NSMutableArray * switchArray;
    NSMutableArray * m_vecDODeviceName;
    std::vector<BOOL> m_vecDOStatus;
	NSMutableArray * updateWaitArray;
    
    int UINumber;
    NSCondition* m_soapCondition;
    
    UIActivityIndicatorView *spinner;
    UIView *overlayView;
    int forceDOIndex;
    BOOL forceState;
    
    SingleCamViewController *singlecam;
}

- (void)dismissPopover;
- (void)filpToDI;
- (void)updateAllUI;
- (void)updateUI;
- (void)readDO;
- (id)initWithDODevice:(NSMutableArray *)pVecDeviceName;
- (void)forceOutput:(id) sender;

@property (nonatomic, retain) ServerManager *serverMgr;
@property (nonatomic, assign) id parent;

@end
