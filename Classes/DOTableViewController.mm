//
//  DOTableViewController.m
//  LiveView
//
//  Created by johnlinvc on 10/05/06.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DOTableViewController.h"
#import "LiveViewAppDelegate.h"
#import "DITableViewController.h"
#import "SingleCamViewController.h"
#import "SlideViewMenuController.h"

#include "Utility.h"

#define MAX_INPUT_NUM 8
#define READ_DO_TIME_INTERVAL 3
#define WAIT_FORCE_DO_INTERVAL 1
#define WAIT_FORCE_DO_TIMEOUT 10

@implementation DOTableViewController

@synthesize serverMgr;
@synthesize parent;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem * DOButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"DI", nil)
                                                                  style:UIBarButtonItemStylePlain
																 target:self 
																 action:@selector(filpToDI)];
	if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (IS_IPAD)
            [DOButton setTintColor:COLOR_NAVBUTTONTINTPAD];
        else
            [DOButton setTintColor:COLOR_NAVBUTTONTINT];
    }
    else
    {
        if (IS_IPAD)
            [DOButton setTintColor:COLOR_NAVBUTTONTINTPADIOS6];
        else
            [DOButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
    }
	self.navigationItem.rightBarButtonItem = DOButton;
	[DOButton release];
    if (IS_IPAD) {
#if FUNC_PUSH_NOTIFICATION_SUPPORT
        //set back button title
#if 1
        UIImage *backImage;
        UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
        else
            backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
        bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
        [bButton setImage:backImage forState:UIControlStateNormal];
        [bButton addTarget:self action:@selector(popview) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(popview)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [backButton setTintColor:COLOR_NAVBUTTONTINTPAD];
        }
        else {
            [backButton setTintColor:COLOR_NAVBUTTONTINTPADIOS6];
        }
#endif
        self.navigationItem.leftBarButtonItem = backButton;
        [backButton release];
#else
        UIBarButtonItem * CloseButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(dismissPopover)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [CloseButton setTintColor:COLOR_NAVBUTTONTINTPAD];
        }
        else {
            [CloseButton setTintColor:COLOR_NAVBUTTONTINTPADIOS6];
        }
        self.navigationItem.leftBarButtonItem = CloseButton;
        [CloseButton release];
#endif
        self.preferredContentSize = CGSizeMake(PopoverSizeWidth, PopoverSizeHeight);
    }
    else { // iPhone version
        //set back button title
#if 1
        UIImage *backImage;
        UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
        else
            backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
        bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
        [bButton setImage:backImage forState:UIControlStateNormal];
        [bButton addTarget:self action:@selector(popview) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(popview)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [backButton setTintColor:COLOR_NAVBUTTONTINT];
        }
        else {
            [backButton setTintColor:COLOR_NAVBUTTONTINTPADIOS6];
        }
#endif
        self.navigationItem.leftBarButtonItem = backButton;
        [backButton release];
    }
    
    forceDOIndex = -1;
}

- (void)popview {
    if (IS_IPAD) {
        self.preferredContentSize = CGSizeMake(PopoverSizeWidth, PopoverSizeHeight);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	[self.tableView reloadData];
    
    m_soapCondition = [[NSCondition alloc] init];
    
    stateArray = [[NSMutableArray alloc] init];
    [stateArray retain];
    
    updateWaitArray = [[NSMutableArray alloc] init];
    [updateWaitArray retain];
    
    if ([parent isKindOfClass:[SingleCamViewController class]]) {
        singlecam = (SingleCamViewController *)parent;
        m_vecDODeviceName = [serverMgr getDODeviceNameByChIndex:[singlecam getCurrentChIndex]];
        for (int i=0; i < [m_vecDODeviceName count]; i++) {
            m_vecDOStatus.push_back(FALSE);
        }
    }
    else {
        singlecam = nil;
        m_vecDODeviceName = [serverMgr getDODeviceNameList];
        for (int i=0; i < [m_vecDODeviceName count]; i++) {
            m_vecDOStatus.push_back(FALSE);
        }
    }
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    for (int i=0; i < [m_vecDODeviceName count]; i++) {
        if (singlecam != nil)
            m_vecDOStatus[i] = [serverMgr getDOState:i chIndex:[singlecam getCurrentChIndex]];
        else
            m_vecDOStatus[i] = [serverMgr getDOState:i];
    }
    
    [self performSelectorOnMainThread:@selector(updateAllUI) withObject:NULL waitUntilDone:YES];
    
    doThread = [[NSThread alloc] initWithTarget:self selector:@selector(readDO) object:nil];
    [doThread start];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (overlayView != nil) {
            overlayView.frame = self.tableView.bounds;
        }
        if (spinner != nil) {
            spinner.center = self.tableView.center;
        }
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    if (overlayView != nil) {
        overlayView.frame =  self.tableView.bounds;
    }
    if (spinner != nil) {
        spinner.center = self.tableView.center;
    }
    return;
}

- (void)updateAllUI{
    for (int i=0; i < [m_vecDODeviceName count]; i++)
    {
        BOOL bStatus = m_vecDOStatus[i];
        [stateArray addObject:[NSNumber numberWithBool:bStatus]];
        [updateWaitArray addObject:[NSNumber numberWithInt:0]]; // jerrylu, 2012/07/11
        
        if (i < [switchArray count]) {
            if (bStatus) 
            {
                ((UISwitch *)[switchArray objectAtIndex:i]).on = YES;
            }
            else 
            {
                ((UISwitch *)[switchArray objectAtIndex:i]).on = NO;
            }         
        }
    }
}

- (void)updateUI{
    BOOL bStatus = m_vecDOStatus[UINumber];
    
    if (UINumber < [switchArray count]) {
        if (bStatus) 
        {
            ((UISwitch *)[switchArray objectAtIndex:UINumber]).on = YES;
        }
        else 
        {
            ((UISwitch *)[switchArray objectAtIndex:UINumber]).on = NO;
        }         
    }
}

- (void)readDO{
    int forcecount = 0;
    
    while (TRUE) {
        if ([[NSThread currentThread] isCancelled]) {
            [NSThread exit];
            return;
        }
        
        if (forceDOIndex >= 0)
            [NSThread sleepForTimeInterval:WAIT_FORCE_DO_INTERVAL];
        else
            [NSThread sleepForTimeInterval:READ_DO_TIME_INTERVAL];
        
        for (int i=0; i < [m_vecDODeviceName count]; i++)
        {
            if ([[NSThread currentThread] isCancelled]) 
            {
                [NSThread exit];
                return;
            }
            
            // jerrylu, 2012/10/09, fix bug7760
            // User poll the switch
            if (forceDOIndex >= 0) {
                if (singlecam != nil) {
                    if (![serverMgr getDOPrivilege:i chIndex:[singlecam getCurrentChIndex]])
                        continue;
                }
                else {
                    if (![serverMgr getDOPrivilege:i])
                        continue;
                }
                //update DO
                [m_soapCondition lock];
                if (singlecam != nil)
                    m_vecDOStatus[i] = [serverMgr getDOState:i chIndex:[singlecam getCurrentChIndex]];
                else
                    m_vecDOStatus[i] = [serverMgr getDOState:i];
                [m_soapCondition unlock];
                
                // check the status of forced switch only
                if (i == forceDOIndex) {
                    if (forceState == m_vecDOStatus[i]) {
                        [self performSelectorOnMainThread:@selector(releaseScreen) withObject:NULL waitUntilDone:NO];
                        forcecount = 0;
                        forceDOIndex = -1;
                    }
                    else {
                        forcecount += WAIT_FORCE_DO_INTERVAL;
                    }
                    
                    if (forcecount >= WAIT_FORCE_DO_TIMEOUT) {
                        [self performSelectorOnMainThread:@selector(releaseScreen) withObject:NULL waitUntilDone:NO];
                        forcecount = 0;
                        forceDOIndex = -1;
                        UINumber = i;
                        [self performSelectorOnMainThread:@selector(updateUI) withObject:NULL waitUntilDone:NO];
                    }
                }
                
                if ([serverMgr isServerLogouting]) {
                    [NSThread exit];
                    return;
                }
            }
            else {
                // jerrylu, 2012/07/11
                int updatewaitcount = [[updateWaitArray objectAtIndex:i] intValue];
                if (updatewaitcount > 0) {
                    updatewaitcount--;
                    [updateWaitArray replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:updatewaitcount]];
                    continue;
                }
                
                // jerrylu, 2012/08/24
                if (singlecam != nil) {
                    if (![serverMgr getDOPrivilege:i chIndex:[singlecam getCurrentChIndex]])
                        continue;
                }
                else {
                    if (![serverMgr getDOPrivilege:i])
                        continue;
                }
                
                //update DO
                [m_soapCondition lock];
                if (singlecam != nil)
                    m_vecDOStatus[i] = [serverMgr getDOState:i chIndex:[singlecam getCurrentChIndex]];
                else
                    m_vecDOStatus[i] = [serverMgr getDOState:i];
                [m_soapCondition unlock];
                
                //check if its differ form oldStatArray
                //NSLog(@"statArray has size of  %d", [statArray count]);
                BOOL bStatus = m_vecDOStatus[i];
                if(bStatus != [[stateArray objectAtIndex:i] boolValue])
                {
                    if (![serverMgr isServerLogouting]) // jerrylu, 2012/06/28
                    {
                        UINumber = i;
                        
                        //Change UI
                        [self performSelectorOnMainThread:@selector(updateUI) withObject:NULL waitUntilDone:NO];
                    }
                    else { // jerrylu, 2012/07/02
                        [NSThread exit];
                        return;
                    }
                }
                [stateArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:bStatus]];
            }
        }
    }
}

- (id)initWithDODevice:(NSMutableArray *) pVecDeviceName {
	if ((self = [super init])) {
		m_vecDODeviceName = pVecDeviceName;
        for (int i=0; i < [m_vecDODeviceName count]; i++) {
            m_vecDOStatus.push_back(FALSE);
        }    
    }
	return self;
}

- (void)dismissPopover {
    if (IS_IPAD) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        if ([parent isKindOfClass:[SingleCamViewController class]])
            [parent dismissLiveViewMenuPopover];
        else
            [parent dismissPopover];
    }
    return;
}

- (void)filpToDI {
    if ([parent isKindOfClass:[SingleCamViewController class]]) {
        DITableViewController * diTable = [[DITableViewController alloc] initWithDIDevice:[serverMgr getDIDeviceNameByChIndex:[singlecam getCurrentChIndex]]];
        [diTable setTitle:NSLocalizedString(@"Digital Input", nil)];
        [diTable setParent:parent];
        [diTable setServerMgr:serverMgr];
        [self.navigationController pushViewController:diTable animated:YES];
        [diTable release];
    }
    else {
        DITableViewController * diTable = [[DITableViewController alloc] initWithDIDevice:[serverMgr getDIDeviceNameList]];
        [diTable setTitle:NSLocalizedString(@"Digital Input", nil)];
        [diTable setParent:parent];
        [diTable setServerMgr:serverMgr];
        [self.navigationController pushViewController:diTable animated:YES];
        [diTable release];
    }
}

- (void)forceOutput:(id) sender {
    int iIdx = [switchArray indexOfObject:sender];
    if ( 0 <= iIdx && iIdx < [m_vecDODeviceName count]) {
        BOOL bState = ((UISwitch *)sender).on == TRUE ? YES : NO;
        
        if (singlecam != nil) {
            if (![serverMgr getDOPrivilege:iIdx chIndex:[singlecam getCurrentChIndex]]) {
                ((UISwitch *)sender).enabled = FALSE;
                ((UISwitch *)sender).on = FALSE;
                return;
            }
        }
        else {
            if (![serverMgr getDOPrivilege:iIdx]) {
                ((UISwitch *)sender).enabled = FALSE;
                ((UISwitch *)sender).on = FALSE;
                return;
            }
        }
        
        [m_soapCondition lock];
        if (singlecam != nil)
            [serverMgr setDOState:iIdx chIndex:[singlecam getCurrentChIndex] state:bState];
        else
            [serverMgr setDOState:iIdx state:bState];
        [m_soapCondition unlock];
        
        m_vecDOStatus[iIdx] = bState;
        [stateArray replaceObjectAtIndex:iIdx withObject:[NSNumber numberWithBool:bState]];
        [updateWaitArray replaceObjectAtIndex:iIdx withObject:[NSNumber numberWithInt:1]];
        
        // jerrylu, 2012/10/08, fix bug7760
        [self holdScreen];
        forceDOIndex = iIdx;
        forceState = bState;
    }
}

- (void)holdScreen {
    // jerrylu, 2012/10/08, fix bug7760
    [self.tableView setScrollEnabled:NO];
    
    if (overlayView == nil) {
        overlayView = [[UIView alloc] init];
        overlayView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        overlayView.frame = self.tableView.bounds;
    }
    if (spinner == nil) {
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        spinner.center = self.tableView.center;
    }
    [overlayView addSubview:spinner];
    [spinner startAnimating];
    [self.tableView addSubview:overlayView];
}

- (void)releaseScreen {
    // jerrylu, 2012/10/08, fix bug7760
    [self.tableView setScrollEnabled:YES];
    
    [overlayView removeFromSuperview];
    [spinner removeFromSuperview];
    [spinner stopAnimating];
    
    if (overlayView != nil) {
        [overlayView release];
        overlayView = nil;
    }
    if (spinner != nil) {
        [spinner release];
        spinner = nil;
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [m_vecDODeviceName count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        
        UISwitch * cellSwitch = [[[UISwitch alloc] init] autorelease];
        if (switchArray == nil) {
            switchArray = [[NSMutableArray alloc] init];
        }
        
        if (indexPath.row < [switchArray count]) {
            [switchArray replaceObjectAtIndex:indexPath.row withObject:cellSwitch];
        }
        else {
            [switchArray addObject:cellSwitch];
        }
        
        cellSwitch.on = NO;
        [cellSwitch addTarget:self action:@selector(forceOutput:) forControlEvents:UIControlEventValueChanged];
        if (m_vecDOStatus.size() > indexPath.row) {
            if (m_vecDOStatus[indexPath.row]) {
                cellSwitch.on = YES;
            }
            
            if (singlecam != nil) {
                if (![serverMgr getDOPrivilege:indexPath.row chIndex:[singlecam getCurrentChIndex]]) {
                    cellSwitch.enabled = FALSE;
                }
            }
            else {
                if (![serverMgr getDOPrivilege:indexPath.row]) {
                 cellSwitch.enabled = FALSE;
                }
            }
        }
        
        cell.accessoryView = cellSwitch;
        
    }

    if (indexPath.row < [m_vecDODeviceName count]) {
        cell.textLabel.text = [m_vecDODeviceName objectAtIndex:indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the cell...
    //ell.textLabel.text = @"%d",indexPath.row;
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewWillDisappear:(BOOL) animated {
	//NSLog(@"do viewwilldisappear !!! %d %d", [switchArray retainCount], [statArray retainCount]);
    
	if ([doThread isExecuting]) {
        [doThread cancel];
        while (![doThread isFinished]) {
            [NSThread sleepForTimeInterval:0.5];
        }
    }
    [doThread release];
    
    [m_soapCondition release];
    m_soapCondition = nil;
    
	[switchArray release];
	switchArray = nil;
	[stateArray release];
    stateArray = nil;
    [updateWaitArray release]; // jerrylu, 2012/07/11
    updateWaitArray = nil;
}

- (void)dealloc {
    [serverMgr release];
    serverMgr = nil;
    [super dealloc];
}

@end

