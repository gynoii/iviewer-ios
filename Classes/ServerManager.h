//
//  ControlHelper.h
//  objcKernal
//
//  Created by johnlinvc on 10/01/29.
//  Copyright 2010. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ControlHelper.h"
#import "myviewInfo.h"
#import "AudioController.h"
#import "ALplayer.h"

typedef enum E_EVENTVIEW_TYPE {
	eEV_None = -1,
	eEV_PlaybackDefault, // jerrylu, 2012/06/18
    eEV_Playback, // jerrylu, 2012/04/20
} EEVType;

typedef enum E_EVENTPLAY_TYPE {
	eventPLAY_PAUSE = 0,
	eventPLAY_PLAY,
	eventPLAY_REVERSE,
} EventPlayType;

@class ControlHelper;

@interface ServerManager : NSObject<ControlHelperLoginDelegate, UIAlertViewDelegate> {
    int m_ControlHelperLoginNum;
    int m_ControlHelperLoginFailedNum;
    int m_CameraTotalNum;
    NSMutableArray* m_ControlHelperArray;
    NSMutableArray* m_ControlHelperLoginFailedArray;
	NSMutableArray* m_CameraCountArray;
 	NSMutableArray* m_DigitalInputCountArray;
 	NSMutableArray* m_DigitalInputDeviceNameArray;
 	NSMutableArray* m_DigitalOutputCountArray;
 	NSMutableArray* m_DigitalOutputDeviceNameArray;
    
    id siteDelegate; //SiteListCtrl
    id displayDelegate;
    id playbackDelegate;
    id eventDelegate;
    // Brosso
    id timelineDelegate;
	
    BOOL isSingleCam;
    BOOL isMyViewMode;
    BOOL isChangeMyViewLayout;
    BOOL isUnsupportedFormat;
    MyViewInfo * _myViewInfo;
    MyViewInfo * _myViewBackupInfo;
    NSThread * m_checkServerConnectionThread;
    
    NSMutableArray *m_AlertViewArray;
    
    BOOL talkEnable;
    AudioController *m_AudioController; //Handle Talk
    ALplayer *audioplayer; //Handle Audio
}

@property (nonatomic, assign) id siteDelegate;
@property (nonatomic, assign) id displayDelegate;
@property (nonatomic, assign) id playbackDelegate;
@property (nonatomic, assign) id eventDelegate;

//Check Single Cam
@property (nonatomic, assign) BOOL isSingleCam;

@property (nonatomic, assign) BOOL isMyViewMode;
@property (nonatomic, assign) BOOL isChangeMyViewLayout;
@property (nonatomic, assign) BOOL isUnsupportedFormat;
@property (nonatomic, assign) MyViewInfo * myViewInfo;
@property (nonatomic, assign) MyViewInfo * myViewBackupInfo;

- (void)setAllServer:(NSMutableArray *)serverlist;
- (void)deleteAllServer;
- (void)loginAllServer;
- (void)logoutAllServer;
- (void)reLoginMyView;
- (void)abortLoginAllServer;
- (void)loginAllServerByEvent:(NSMutableArray *)serverList event:(Eventinfo *)eventInfo;
- (void)loginSuccessByIndex:(int)index CameraCount:(int)count;
- (void)loginFailedByIndex:(int)index;
- (BOOL)isServerLogouting;
- (BOOL)isServerLogoutingByIndex:(int)index;
- (void)goLiveViewFromEventView:(id)displaydelegate;

- (int)getCameraTotalNum;
- (NSString*)getServerManagerTitle;
- (ELayoutType)getLayoutType;
- (EServerType)getServerTypeByChIndex:(int)index;
- (EServerType)getServerTypeBySrvIndex:(int)index;
- (BOOL)isAllSupportSecondStream;
- (BOOL)isSupportSecondStreamByChIndex:(int)index;
- (BOOL)isConnectionWithP2PServer;
#if FUNC_P2P_SAT_SUPPORT
- (BOOL)isP2PConnectionWithRelayMode;
- (void)resetP2PRelayQuota;
#endif
- (void)updateAudioAndPtzDeviceByChIndex:(int)index;

- (void)connectToCam:(int)index layout:(ELayoutType)layout;
- (void)setLayoutType:(ELayoutType)layout;
- (void)connectToCamWithHighResolution:(int)index;
- (void)stopconnectToCam;
- (void)disconnectAllCam;
- (void)disconnectAllCamExceptFrom:(int)start_cam To:(int)end_cam;
- (void)disconnectAllPlaybackConnection;
- (BOOL)isCamStreaming:(int)index;
//- (Np_Device)getDeviceByChIndex:(int)index;
- (NSString *)getDeviceNameByChIndex:(int)index;
- (int)getDeviceCidByChIndex:(int)index;
- (int)getDeviceLidByChIndex:(int)index;
- (unsigned long long)getExtDeviceCidByChIndex:(int)index;
- (unsigned long long)getExtDeviceLidByChIndex:(int)index;
- (void)reloadAllDevices;
- (BOOL)getLiveViewConnection:(int)index;
- (BOOL)getPlaybackConnection:(int)index;
- (void)changeCameraOrderFrom:(int)pre_index to:(int)post_index;
- (void)saveMyViewLayout;

// DI/O
- (NSMutableArray *)getDODeviceNameList;
- (NSMutableArray *)getDODeviceNameByChIndex:(int)index;
- (NSMutableArray *)getDIDeviceNameList;
- (NSMutableArray *)getDIDeviceNameByChIndex:(int)index;
- (BOOL)getDOState:(int)index;
- (BOOL)getDIState:(int)index;
- (void)setDOState:(int)index state:(BOOL)state;
- (BOOL)getDOPrivilege:(int)index;
- (BOOL)getDOState:(int)do_index chIndex:(int)ch_index;
- (BOOL)getDIState:(int)di_index chIndex:(int)ch_index;
- (void)setDOState:(int)do_index chIndex:(int)ch_index state:(BOOL)state;
- (BOOL)getDOPrivilege:(int)do_index chIndex:(int)ch_index;

// PTZ
- (BOOL)isSupportPTZ:(int)index;
- (void)getPresetByChIndex:(int)index;
- (BOOL)isGetPresetByChIndex:(int)index;
- (NSMutableArray *)getPresetNameArrayByChIndex:(int)index;
- (void)setPTZByChIndex:(int)index withDirection:(NSString *)dir;
- (void)gotoPTZPresetByChIndex:(int)index withIdx:(int)idx;
- (void)setPresetDelegateByChIndex:(int)index delegate:(id)theDelegate;
- (void)setLoadingDelegate:(id)theDelegate;

// Audio
- (BOOL)isSupportAudio:(int)index;
- (BOOL)getLiveViewAudioStateByChIndex:(int)index;
- (void)setLiveviewAudioStateByChIndex:(int)index state:(BOOL) state;
- (BOOL)getPlaybackAudioStateByChIndex:(int)index;
- (void)setPlaybackAudioStateByChIndex:(int)index state:(BOOL) state;
- (BOOL)isSupportTalk:(int)index;
- (BOOL)getTalkStateByChIndex:(int)index;
- (void)setTalkStateByChIndex:(int)index state:(BOOL) state;
- (void)disableTalkFunc;

// Mega-Pixel
- (BOOL)isSupportSecondProfile:(int)index;
- (EProfileType)getConnectProfileTypeWithChIndex:(int)index;
- (void)setConnectProfileTypeWithChIndex:(int)index profiletype:(EProfileType)profiletype;

#if FUNC_PUSH_NOTIFICATION_SUPPORT
// Push Notification
- (NSString *)getFilterPushNotificationServerID;
- (NSString *)getFilterPushNotificationUserName;
- (BOOL)getPushNotificationEnableByChIndex:(int)index;
- (void)setPushNotificationEnableByChIndex:(int)index enable:(BOOL)enable;
#endif

// Playback
- (BOOL)isSupportPlayback:(int)index;
- (BOOL)isPlaybackStreaming:(int)index;
- (BOOL)playbackStartFromDate:(int)index startdate:(NSDate *)startdate enddate:(NSDate *)enddate;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (BOOL)playbackStartFromDate:(int)index recordfile:(EDualRecordFile) recordfile startdate:(NSDate *)startdate enddate:(NSDate *)enddate;
#endif
- (BOOL)playbackStartFromDateByEvent:(int) index startdate:(NSDate *)startdate enddate:(NSDate *)enddate;
- (BOOL)playbackOpenRecord:(int) index startdate:(Np_DateTime)starttime enddate:(Np_DateTime)endtime;
- (void)playbackStop:(int)index;
- (void)playbackStopByEvent:(int)index; // jerrylu, 2012/11/29
- (void)playbackPlay:(int)index;
- (void)playbackReversePlay:(int)index;
- (void)playbackPause:(int)index;
- (void)playbackPreviousFrame:(int) index;
- (void)playbackNextFrame:(int) index;
- (void)playbackSeek:(int)index seektime:(Np_DateTime)seektime;
- (float)playbackGetSpeed:(int)index;
- (void)playbackSetSpeed:(int)index speed:(float)speed;
- (Np_DateTime)playbackGetTime:(int)index;
- (Np_PlayerState)playbackGetState:(int)index;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (BOOL)playbackChangeRecordFile:(int) index recordfile:(EDualRecordFile) recordfile startdate:(Np_DateTime)starttime enddate:(Np_DateTime)endtime seektime:(Np_DateTime)seektime;
- (BOOL)checkPlaybackLogTime:(int) index recordfile:(EDualRecordFile) rf_index datetime:(Np_DateTime *) datetime;

// Brosso
- (void)queryRecordByIndex:(int)index andDate:(NSDate*)date;
#endif

//Crystal Server
- (std::vector<Np_Server>)getRecordingServerByMSIndex:(int)index;
- (BOOL)getCameraListFromByMSIndex:(int)index rsID:(Np_ID_Ext) serverID;

//MyView Edit
- (void)matchServerHandler:(NSMutableArray *) targetHlprArray refServerList:(NSMutableArray *) serverInfoList;
- (void)updateServerHandlerForNewView:(MyViewInfo *) newViewInfo refServerHandler:(NSMutableArray *) targetHlprArray;
- (void)removeServerHandlerInCurrentView:(NSMutableArray *) targetHlprArray;

#if SHOW_FRAMERATE_FLAG
- (double)getLiveViewFrameRate:(int) index;
#endif
@end

@protocol ServerManagerLoginDelegate
@optional
- (void)serverManagerDidGetCameraCount:(ServerManager *)servermgr CameraCount:(int)count;
- (void)serverManagerloginCrystalServer:(ServerManager *)servermgr;
- (void)serverManagerCantConnectToServer:(ServerManager *)servermgr;
- (void)serverManagerGetServerlistByServerID:(NSString *)sid userName:(NSString *)uname serverlist:(NSMutableArray *)slist;
@end

@protocol ServerManagerDisplayDelegate
@optional
- (void)serverManagerDidServerDown:(ServerManager *)servermgr serverIndex:(int)index eventID:(int)eventID;
- (void)serverManagerDidPlaybackDisconnection:(ServerManager *)servermgr;
- (void)serverManagerDidRefreshImages:(ServerManager *)servermgr camIndex:(int)index camImg:(UIImage *)img;
- (void)serverManagerDidRefreshImageBuf:(ServerManager *)servermgr camIndex:(int)index img:(NSData *)imgData width:(int)width height:(int)height;
- (void)serverManagerDidCameraSessionLost:(ServerManager *)servermgr camIndex:(int)index;
- (void)serverManagerDidPbNextPreviosFrameSuccess:(ServerManager *)servermgr;
#if FUNC_P2P_SAT_SUPPORT
- (void)serverManagerQuotaTimeout:(ServerManager *)servermgr serverIndex:(int)index isOutOfQuota:(BOOL)outofquota;
- (void)serverManagerShowP2PControlDialog:(ServerManager *)servermgr msg:(NSString *)dialogMsg;
#endif
- (void)serverManagerDidSaveLayout:(ServerManager *)servermgr;
- (void)serverManagerDidTalkReserved:(ServerManager *)servermgr srv:(NSString *)servername usr:(NSString *)username;
- (void)serverManagerDidTalkSessionError:(ServerManager *)servermgr;
- (void)serverManagerShowDialog:(ServerManager *)servermgr msg:(NSString *)dialogMsg;
- (void)serverManagerRecordingServerAvailable:(ServerManager *)servermgr msg:(NSString *)dialogMsg;
- (void)serverManagerRefreshAllLayout:(ServerManager *)servermgr;
@end

@protocol ServerManagerPlaybackDelegate
@optional
- (void)serverManagerAlertPlaybackEvent:(ServerManager *)servermgr event:(EPlaybackEvent)event;
- (void)serverManagerRefreshPlaybackTime:(ServerManager *)servermgr startTime:(Np_DateTime *)newStartTime endTime:(Np_DateTime *)newEndTime;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)serverManagerUnsupportedFormat;
- (void)serverManagerSupportDualRecordPlayback:(ServerManager *)servermgr;
- (void)serverManagerRefreshPlaybackTime:(ServerManager *)servermgr recordfile:(EDualRecordFile)rec startTime:(Np_DateTime *)newStartTime endTime:(Np_DateTime *)newEndTime;
#endif
@end

@protocol ServerManagerSiteListDelegate
- (void)serverManagerSaveInfo:(ServerManager *)servermgr;
@end 

@protocol ServerManagerEventDelegate
@optional
- (void)serverManagerStartEventViewByIndex:(ServerManager *)servermgr camIndex:(int)index;
- (void)serverManagerEventDidRefreshImages:(ServerManager *)servermgr camIndex:(int)index camImg:(UIImage *)img;
- (void)serverManagerEventDidRefreshImageBuf:(ServerManager *)servermgr camIndex:(int)index img:(NSData *)imgData width:(int)width height:(int)height;
- (void)serverManagerDidServerDown:(ServerManager *)servermgr eventID:(int)eventID;
- (void)serverManagerDidPlaybackDisconnection:(ServerManager *)servermgr;
#if FUNC_P2P_SAT_SUPPORT
- (void)serverManagerQuotaTimeout:(ServerManager *)servermgr  isOutOfQuota:(BOOL)outofquota;
- (void)serverManagerEventShowP2PControlDialog:(ServerManager *)servermgr msg:(NSString *)dialogMsg;
#endif
- (void)serverManagerEventLoginFailed:(ServerManager *)servermgr;
- (void)serverManagerEventSupportDualRecordPlayback:(ServerManager *)servermgr;
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
- (void)serverManagerEventChangePlaybackRecordFile:(ServerManager *)servermgr recordFile:(EDualRecordFile)recordfile;
#endif
@end
