//
//  PlaybackViewController.m
//  iViewer
//
//  Created by Jerry Lu on 12/4/20.
//  Copyright (c) 2012年 NUUO. All rights reserved.
//

#import "PlaybackViewController.h"
#import "LiveViewAppDelegate.h"
#import "SingleCamViewController.h"
#import "PlaybackIntervalViewController.h"
#import "PlaybackRecordFileViewController.h"

@implementation PlaybackViewController
@synthesize singleViewDelegate;
@synthesize pickupdate;
@synthesize pDatePicker;
@synthesize pTimePicker;
@synthesize pActionTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    NSDate *date = [[NSDate alloc] init];
    pickupdate = [date copy];
    [date release];
    
    if (IS_IPAD) {
        self.preferredContentSize = CGSizeMake(PopoverSizeWidth, 480);
        
        UIBarButtonItem * CloseButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(dismissPopover)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [CloseButton setTintColor:COLOR_NAVBUTTONTINTPAD];
        }
        else {
            [CloseButton setTintColor:COLOR_NAVBUTTONTINTPADIOS6];
        }
        self.navigationItem.leftBarButtonItem = CloseButton;
        [CloseButton release];
    }
    else {
#if 1
        UIImage *backImage;
        UIButton *bButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            backImage = [UIImage imageNamed:@"Back_48x48_nor.png"];
        else
            backImage = [UIImage imageNamed:@"Back_32x32_nor.png"];
        bButton.bounds = CGRectMake(0, 0, backImage.size.width, backImage.size.height);
        [bButton setImage:backImage forState:UIControlStateNormal];
        [bButton addTarget:self action:@selector(popview) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:bButton];
#else
        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(popview)];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [backButton setTintColor:COLOR_NAVBUTTONTINT];
        }
        else {
            [backButton setTintColor:COLOR_NAVBUTTONTINTIOS6];
        }
#endif
        self.navigationItem.leftBarButtonItem = backButton;
        [backButton release];
    }
    
    kPlaybackTimeInterval = ePlaybackTimeIntervalDefault;
    kPlaybackRecordfile = 0;
    
    SingleCamViewController *camview = (SingleCamViewController *)singleViewDelegate;
    serverType = [camview.serverMgr getServerTypeByChIndex:[camview getCurrentChIndex]];
}

- (void)popview {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dismissPopover {
    if (IS_IPAD) {
        SingleCamViewController *camview = (SingleCamViewController *)singleViewDelegate;
        [camview dismissPlaybackPopover];
    }
    return;
}

- (void)dealloc
{
    [super dealloc];
    
    // Release any retained subviews of the main view.
    [pickupdate release];
    pickupdate = nil;
    pDatePicker = nil;
    pTimePicker = nil;
    [pActionTableView release];
    pActionTableView = nil;
    singleViewDelegate = nil;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext> context) {
        if (size.width > size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        } else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
        [self.view setNeedsLayout];
    } completion:nil];
    
    return;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        return;
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self resizeViewWithOrientation:toInterfaceOrientation];
}

-(void)resizeViewWithOrientation:(UIInterfaceOrientation) orientation
{
    [self.pDatePicker removeFromSuperview];
    [self.pDatePicker removeTarget:self action:@selector(refreshPickDate) forControlEvents:UIControlEventValueChanged];
    self.pDatePicker = nil;
    [self.pTimePicker removeFromSuperview];
    [self.pTimePicker removeTarget:self action:@selector(refreshPickDate) forControlEvents:UIControlEventValueChanged];
    self.pTimePicker = nil;

    //(Re)initialize the datepicker, thanks to Apple's buggy UIDatePicker implementation
    UIDatePicker *dummyDatePicker = [[UIDatePicker alloc] init];
    self.pDatePicker = dummyDatePicker;
    self.pDatePicker.datePickerMode = UIDatePickerModeDate;
    [dummyDatePicker release];
    UIDatePicker *dummyTimePicker = [[UIDatePicker alloc] init];
    self.pTimePicker = dummyTimePicker;
    self.pTimePicker.datePickerMode = UIDatePickerModeTime;
    NSLocale* locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    [dummyTimePicker setLocale:locale];
    [locale release];
    [dummyTimePicker release];

    [self.pDatePicker setDate:self.pickupdate animated:YES];
    [self.pDatePicker addTarget:self action:@selector(refreshPickDate) forControlEvents:UIControlEventValueChanged];
    [self.pTimePicker setDate:self.pickupdate animated:YES];
    [self.pTimePicker addTarget:self action:@selector(refreshPickDate) forControlEvents:UIControlEventValueChanged];
    
    CGRect screensize = [[UIScreen mainScreen] bounds];
    int width = (screensize.size.width < screensize.size.height)? screensize.size.width : screensize.size.height;
    int height = (screensize.size.width < screensize.size.height)? screensize.size.height : screensize.size.width;
    
    if (IS_IPAD)
        self.pDatePicker.frame = CGRectMake(0, 20, 320, 150);
    else
    {
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            self.pDatePicker.frame = CGRectMake(0.0, 44.0, width/10.0*7.0, 216.0);
        }
        else
        {
            self.pDatePicker.frame = CGRectMake(0.0, 25.0, height/10.0*7.0, 162.0);
        }
    }
    [self.view addSubview:self.pDatePicker];

    if (IS_IPAD)
        self.pTimePicker.frame = CGRectMake(0, 170, 320, 150);
    else
    {
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            self.pTimePicker.frame = CGRectMake(width/10.0*6.0, 44.0, width/10.0*5.0, 216.0);
        }
        else
        {
            self.pTimePicker.frame = CGRectMake(height/10.0*6.0, 25.0, height/10.0*5.0, 162.0);
        }
    }
    [self.view addSubview:self.pTimePicker];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        if (IS_IPHONE)
        {
            [self.pActionTableView removeFromSuperview];
            self.pActionTableView = nil;
            
            if (UIInterfaceOrientationIsPortrait(orientation))
            {
                self.pActionTableView = [[[UITableView alloc] initWithFrame:CGRectMake(0.0, 280.0, width, 200.0) style:UITableViewStyleGrouped] autorelease];
            }
            else
            {
                self.pActionTableView = [[[UITableView alloc] initWithFrame:CGRectMake(0.0, 145.0, height, 200.0) style:UITableViewStyleGrouped] autorelease];
            }
            [self.pActionTableView setDelegate:self];
            [self.pActionTableView setDataSource:self];
            [self.pActionTableView setScrollEnabled:NO];
            
            [self.view addSubview:self.pActionTableView];
        }
        else
        {
            [self.pActionTableView setFrame:CGRectMake(0.0, 320.0, 320.0, 200.0)];
        }
    }
    
    [self.view setNeedsDisplay];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
        
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController setToolbarHidden:YES animated:NO];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([[UIScreen mainScreen] bounds].size.width > [[UIScreen mainScreen] bounds].size.height) {
            [self resizeViewWithOrientation:UIInterfaceOrientationLandscapeLeft];
        }
        else {
            [self resizeViewWithOrientation:UIInterfaceOrientationPortrait];
        }
    }
    else {
        [self resizeViewWithOrientation:self.interfaceOrientation];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
#pragma mark -
#pragma mark Public Methods
- (NSDate *)getPickDate {
    // Set the seconds and milliseconds to zero
    NSTimeInterval time = floor([pickupdate timeIntervalSinceReferenceDate] / 60.0) * 60.0;
    return [NSDate dateWithTimeIntervalSinceReferenceDate:time];
}

- (void)refreshPickDate
{
    NSDateFormatter* timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"yyyy"];
    NSString* yearStr = [timeFormat stringFromDate: pDatePicker.date];
    [timeFormat setDateFormat:@"MM"];
    NSString* monthStr = [timeFormat stringFromDate: pDatePicker.date];
    [timeFormat setDateFormat:@"dd"];
    NSString* dayStr = [timeFormat stringFromDate: pDatePicker.date];

    [timeFormat setDateFormat:@"HH"];
    NSString* hourStr = [timeFormat stringFromDate: pTimePicker.date];
    [timeFormat setDateFormat:@"mm"];
    NSString* minuteStr = [timeFormat stringFromDate: pTimePicker.date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components: NSYearCalendarUnit|
                                                         NSMonthCalendarUnit|
                                                         NSDayCalendarUnit
                                                         fromDate:[NSDate date]];
    [components setYear  :[yearStr integerValue]];
    [components setMonth :[monthStr integerValue]];
    [components setDay   :[dayStr integerValue]];
    [components setHour  :[hourStr integerValue]];
    [components setMinute:[minuteStr integerValue]];
    pickupdate = [[calendar dateFromComponents:components] copy];
    [timeFormat release];
}

- (int)getPickTimeInterval {
    switch (kPlaybackTimeInterval) {
        case ePlaybackTimeInterval1:
            return kPlaybackTimeInterval1;
        case ePlaybackTimeInterval2:
            return kPlaybackTimeInterval2;
        case ePlaybackTimeInterval3:
            return kPlaybackTimeInterval3;
        case ePlaybackTimeInterval4:
            return kPlaybackTimeInterval4;
        case ePlaybackTimeInterval5:
            return kPlaybackTimeInterval5;
        default:
            break;
    }
    return kDefaultTimeInterval;
}

- (void)setPlaybackTimeInterval:(EPlaybackTimeInterval) timeinterval {
    kPlaybackTimeInterval = timeinterval;
}

- (int)getSelectedRecordfile {
    return kPlaybackRecordfile;
}

- (void)setSelectedRecordfile:(int) recordfile {
    kPlaybackRecordfile = recordfile;
}

#pragma mark -
#pragma mark Table View Data Source Method
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return ePlaybackOptionMax;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        NSUInteger section = [indexPath section];
        switch (section) {
            case ePlaybackOptionPeriod:
            {
                cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                cell.textLabel.text = NSLocalizedString(@"Duration", nil);
                
                NSString *timestring;
                switch (kPlaybackTimeInterval) {
                    case ePlaybackTimeInterval1:
                        timestring = [NSString stringWithFormat:NSLocalizedString(@"30 Seconds", nil)];
                        break;
                    case ePlaybackTimeInterval2:
                        timestring = [NSString stringWithFormat:NSLocalizedString(@"1 Min", nil)];
                        break;
                    case ePlaybackTimeInterval3:
                        timestring = [NSString stringWithFormat:NSLocalizedString(@"2 Mins", nil)];
                        break;
                    case ePlaybackTimeInterval4:
                        timestring = [NSString stringWithFormat:NSLocalizedString(@"5 Mins", nil)];
                        break;
                    case ePlaybackTimeInterval5:
                        timestring = [NSString stringWithFormat:NSLocalizedString(@"10 Mins", nil)];
                        break;
                    default:
                        timestring = [NSString stringWithFormat:NSLocalizedString(@"30 Seconds", nil)];
                        break;
                }
                cell.detailTextLabel.text = timestring;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
            case ePlaybackOptionRecordFile:
            {
                cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                cell.textLabel.text = NSLocalizedString(@"Record File", nil);
                switch (kPlaybackRecordfile) {
                    case 0:
                        cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Record 1", nil)];
                        break;
                    case 1:
                        cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Record 2", nil)];
                        break;
                    default:
                        cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Record 1", nil)];
                        break;
                }
                if (   serverType != eServer_MainConsole
                    && serverType != eServer_NVRmini
                    && serverType != eServer_NVRsolo)
                {
                    cell.userInteractionEnabled = NO;
                    cell.textLabel.enabled = NO;
                    cell.detailTextLabel.enabled = NO;
                }
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            }
#endif
            case ePlaybackOptionPlay:
            {
                cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.textLabel.text = NSLocalizedString(@"Play", nil);
                cell.textLabel.textAlignment = NSTextAlignmentCenter;
                break;
            }
            default:
                break;
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *) tableView heightForHeaderInSection:(NSInteger) section
{
    return 5.0;
}

#pragma mark -
#pragma mark Table Delegate Method
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSUInteger section = [indexPath section];    
    switch (section) {
        case ePlaybackOptionPeriod:
        {
            PlaybackIntervalViewController *intervalViewController = [[PlaybackIntervalViewController alloc] initWithStyle:UITableViewStyleGrouped withDefaultInterval:kPlaybackTimeInterval];
            intervalViewController.playbackView = self;
            [self.navigationController pushViewController:intervalViewController animated:YES];
            [intervalViewController release];
            
            break;
        }
#if FUNC_PLAYBACK_DUAL_STREAM_SUPPORT
        case ePlaybackOptionRecordFile:
        {
            PlaybackRecordFileViewController *recordViewController = [[PlaybackRecordFileViewController alloc] initWithStyle:UITableViewStyleGrouped withDefaultRecordFile:kPlaybackRecordfile];
            recordViewController.playbackView = self;
            [self.navigationController pushViewController:recordViewController animated:YES];
            [recordViewController release];
            break;
        }
#endif
        case ePlaybackOptionPlay:
        {
            [self.navigationController popViewControllerAnimated:NO];
            if ([singleViewDelegate respondsToSelector:@selector(PbViewControllerOpenRecord)]) {                    [singleViewDelegate PbViewControllerOpenRecord];
            }
            break;
        }
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
