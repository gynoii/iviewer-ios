//
//  myviewInfo.h
//  TabView
//
//  Created by johnlinvc on 10/02/25.
//  Copyright 2010 com.debug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myviewServerInfo.h"
#import "myviewCameraInfo.h"
#import "serverInfo.h"

#define kMaxNumOfCamera 64

@interface MyViewInfo : NSObject {
    NSString * viewName;
    ELayoutType myViewLayoutType;
    NSMutableArray * serverList;
}

@property (nonatomic, retain) NSString * viewName;
@property (nonatomic, retain) NSMutableArray * serverList;
@property (nonatomic, assign) ELayoutType myViewLayoutType;

- (id)initWithDictionary:(NSDictionary *) dictionary;
- (NSArray *)getKeys;
- (id)getPropertyDic;

@end
