//
//  QuickLinkController.h
//  TabView
//
//  Created by johnlinvc on 10/02/04.
//  Copyright 2010 com.debug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableDetailCell.h"
#import "serverInfo.h"
#import "SiteListController.h"
#import "ControlHelper.h"

@class ServerInfo;
@class EditableDetailCell;
@class ControlHelper;

typedef enum E_ERROR_MESSAGE {
	eError_Port = 0,
	eError_Username,
	eError_Blank,
    eError_InValidPort,
    eError_BlankPlaybackPort,
    eError_DuplicateIpAccount,
    eError_DuplicateP2pAccount,
    eError_InValidInputStr,
    eError_InValidQRcode
} EErrorType;

typedef enum  E_FIELD_TYPE {
	eName = 0,
	eIP,
	ePort,
#if FUNC_PLAYBACK_SUPPORT
    ePlaybackPort, // jerrylu, 2012/04/19
#endif
	eAccount,
	ePassword,
    eMaxFieldType, // jerrylu, 2012/04/19
} EFieldType;

typedef enum  E_FIELD_P2P_TYPE {
    eP2pName = 0,
	eP2pId,
	eP2pAccount,
	eP2pPassword,
    eMaxFieldP2pType,
} EFieldP2pType;

typedef enum  E_SECTION_TYPE {
#if FUNC_P2P_SAT_SUPPORT
    eTypeControlSection = 0,
	eNameSection,
#else
	eNameSection = 0,
#endif
	eIPSection,
	ePortSection,
#if FUNC_PLAYBACK_SUPPORT
    ePlaybackPortSection, // jerrylu, 2012/04/19
#endif
	eAccountSection,
	ePasswordSection,
    eMaxSectionType,
} ESectionP2pType;

typedef enum  E_SECTION_P2P_TYPE {
#if FUNC_P2P_SAT_SUPPORT
    eP2pTypeControlSection = 0,
	eP2pNameSection,
#else
	eP2pNameSection = 0,
#endif
	eP2pIdSection,
	eP2pAccountSection,
	eP2pPasswordSection,
    eMaxSectionP2pType,
} ESectionType;

@interface SiteInfoController : UITableViewController <UITextFieldDelegate> {
    UISegmentedControl *_ConnectionTypeControl;
	ServerInfo * _serverInfo;
	SiteListController * _favoriteSite;
	EditableDetailCell * _IPCell;
	EditableDetailCell * _portCell;
#if FUNC_PLAYBACK_SUPPORT
    EditableDetailCell * _playbackportCell; // jerrylu, 2012/04/19
#endif
	EditableDetailCell * _accountCell;
	EditableDetailCell * _passwordCell;
	EditableDetailCell * _nameCell;
    EditableDetailCell * _idCell; // jerrylu, 2012/07/17
    EditableDetailCell * _p2pNameCell; // jerrylu, 2012/09/28
    EditableDetailCell * _p2pAccountCell;
    EditableDetailCell * _p2pPasswordCell;
	NSMutableArray * _cellArray;
	BOOL _isAdding;
	BOOL bSaving;
	ServerInfo* backupInfo;
    
    UITextField *activeTextField;
    
    BOOL isChangeConnectionType; // jerrylu, 2012/09/19
    BOOL isChangeSettings; // jerrylu, 2012/11/15
}

@property (nonatomic, retain) ServerInfo * serverInfo;
@property (nonatomic, retain) ServerInfo * backupInfo;
@property (nonatomic, assign) SiteListController * favoriteSite;
@property (nonatomic, retain) EditableDetailCell * IPCell;
@property (nonatomic, retain) EditableDetailCell * portCell;
#if FUNC_PLAYBACK_SUPPORT
@property (nonatomic, retain) EditableDetailCell * playbackportCell; // jerrylu, 2012/04/19
#endif
@property (nonatomic, retain) EditableDetailCell * accountCell;
@property (nonatomic, retain) EditableDetailCell * passwordCell;
@property (nonatomic, retain) EditableDetailCell * nameCell;
@property (nonatomic, retain) EditableDetailCell * idCell;
@property (nonatomic, retain) EditableDetailCell * p2pNameCell;
@property (nonatomic, retain) EditableDetailCell * p2pAccountCell;
@property (nonatomic, retain) EditableDetailCell * p2pPasswordCell;
@property (assign) BOOL isAdding;

//- (EditableDetailCell *)newDetailCellWithTag:(NSInteger) tag;
- (void)newDetailCellWithTag:(NSInteger) tag detailcell:(EditableDetailCell *) detailcell;
- (BOOL)checkInfo;
- (void)save;
- (void)cancel;
- (void)popWarning:(EErrorType) eErrorType;
- (void)loadCellInfos;

@end
